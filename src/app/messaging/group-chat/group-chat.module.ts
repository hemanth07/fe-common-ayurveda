import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GroupChatPage } from './group-chat.page';
import { ObservationComponentsModule } from './../../components/observation-components/observation-components.module';
import { MessagingcomponentsModule } from './../../components/messaging-components/messagingcomponents.module';

const routes: Routes = [
  {
    path: '',
    component: GroupChatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    MessagingcomponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GroupChatPage]
})
export class GroupChatPageModule {}
