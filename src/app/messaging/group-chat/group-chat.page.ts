import { Component, ViewChild, NgZone, OnInit, OnDestroy } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from "@angular/router";
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { OtherService } from '../../languages/others.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { ChatMenuOptionsComponent } from '../../components/messaging-components/chat-menu-options/chat-menu-options.component';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';
import { Platform } from '@ionic/angular';
import { GroupDetailsComponent } from '../../components/messaging-components/group-details/group-details.component';


@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.page.html',
  styleUrls: ['./group-chat.page.scss'],
})
export class GroupChatPage implements OnInit {
  @ViewChild('content') private content: any;
  @ViewChild('textarea') private textarea: any;
  headerData: any = [];
  groupDetails: any = [];
  messageText: any = "";
  messageList: any = [];
  user_id: any;
  makeActive: boolean = false;
  audioFile: any ;
  imageFile:any ;
  videoFile:any ;
  isImage:boolean = false;
  isVideo:boolean = false;
  audio:any;
  isPlaying:any = [];
  isSelected:any = [];
  isPause:boolean = false;
  showAudio:boolean = false;
  isReply:boolean = false;
  isShowAttachments:boolean = false;
  showAtTheRateMentions: boolean = false;
  replyObject : any = {}
  userList:any = [];
  originalUserList:any = [];
  colors:any = [];
  message_users:any = [];
  date:any;
  progress: number = 0;
  pressState: string = "released";
  interval: any;
  showCustomHeader:boolean = false;
  selectedIndex:any = [];
  messageCount:number = 0;
  previousCount:number = 0;
  count:number = 15;
  updatedMessages:any = [];
  deletedMessageCount: number = 0;
  // deletedMessages:any = [];
  constructor(private storage: Storage,
    private sg: SimpleGlobal,
    private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation: FarmeasyTranslate,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public zone: NgZone,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private file: File,
    private media: Media,
    private mediaCapture: MediaCapture,
    public platform: Platform,
    private camera: Camera,
    private otherService:OtherService,
    private clipboard: Clipboard) { }

  ngOnInit() {
    // this.textarea.setFocus();
    this.route.params.subscribe(params => {
      this.groupDetails = JSON.parse(params['group']);
      console.log(this.groupDetails);
      firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key).on('value',resp => {
        let data = resp.val();
        this.groupDetails.members = data.members;
        this.groupDetails.description = data.description;
        this.groupDetails.group_icon = data.group_icon;
        this.groupDetails.name = data.name;
        this.groupDetails.group_icon = data.group_icon;
        this.getTitle(this.groupDetails.name, this.groupDetails.group_icon);
      });
      // this.sg['group'] = this.groupDetails;
      this.getTitle(this.groupDetails.name, this.groupDetails.group_icon);
      // this.findActiveMember();
      this.getGroupMessages();
      this.getUsersList();
      console.log(this.groupDetails);
      this.user_id = this.sg['userdata'].farmeasy_id;
      this.colors = this.otherService.getColors();
      // console.log(this.colors);
      // this.getLastAddedMessages();
      this.content.scrollToBottom();
      this.triggerOnChildAdded();
      this.messageChangesTrigger();


    });
  }
  ngOnChanges(){
    if(this.messageList.length>0){
      this.content.scrollToBottom();
    }
  }
  // findActiveMember() {
  //   let activelist = this.groupDetails.active_members;
  //   let index = activelist.indexOf(this.sg['userdata'].farmeasy_id);
  //   if (index == -1) {
  //     this.makeActive = true;
  //     activelist.push(this.sg['userdata'].farmeasy_id);
  //     this.groupDetails.active_members = activelist;
  //     console.log(this.groupDetails.active_members);
  //   }
  // }
  getTitle(title, group_icon) {
    if(this.sg['userdata'].role != 'employee'){
      this.headerData = {
        color: 'green',
        title: title,
        avatar: group_icon?group_icon:'placeholder',
        button1: 'more',
      };
    } else {
      this.headerData = {
        color: 'green',
        title: title,
        avatar: group_icon?group_icon:'placeholder'
      };
    }
  
  }
 
  convertTime(milli) {
      if(milli){
        let date = new Date(milli);
      let hours = date.getHours()>12 ? date.getHours()-12:(date.getHours()<10?"0"+date.getHours():date.getHours());
      let min = date.getMinutes().toString().length == 1? "0"+date.getMinutes().toString():date.getMinutes().toString();
      let mar = date.getHours() >= 12 ? 'pm' : 'am';
    
      return hours+":"+min+" "+mar;
      }   
  }
  async getGroupMessages() {
    const loading = await this.loadingCtrl.create({
              spinner: 'bubbles',
              message: 'Please wait...',
              translucent: true,
              cssClass: 'custom-class custom-loading'
            });
            loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).limitToLast(this.count).once('value', resp => {
      this.messageList = [];
      let messageList = snapshotToArray(resp);
      this.isPlaying = [];
      this.previousCount = messageList.length;

      // console.log(this.messageList);
      for(let i=0;i<messageList.length;i++){
        this.isPlaying.push(false);
        this.isSelected.push(false);
        if(messageList[i].delete_from_me){
          if(messageList[i].reply_for){
            console.log(messageList[i]);
            let reply = this.findReplyMessage(messageList,messageList[i].reply_for);
            messageList[i]['reply_for'] = reply;
          }
          let index = messageList[i].delete_from_me.indexOf(this.user_id);
          if(index == -1){
            this.messageList.push(messageList[i]);
          }
        } else {
          if(messageList[i].reply_for){
            console.log(messageList[i]);
            let reply = this.findReplyMessage(messageList,messageList[i].reply_for);
            messageList[i]['reply_for'] = reply;
          }
          this.messageList.push(messageList[i]);
        }
       
        // this.content.scrollToBottom();
      }
      setTimeout(() => {
        this.content.scrollToBottom();
        loading.dismiss();
      }, 500);
      setTimeout(() => {
        this.content.scrollToBottom();
      }, 1000);
    });
  }
  findReplyMessage(list,message){
     return list.find(msg => msg.key == message.key);
  }
  // getLastAddedMessages(){
  //   firebase.database().ref('chatroom/group/' + this.groupDetails.id).orderByChild("time").limitToLast(1).on('child_added', resp => {
  //     let message = snapshotToArray(resp);
  //     console.log(message);
  //   });
  // }
 async getUsersList(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').once('value',resp =>{
      this.userList = snapshotToArray(resp);
      this.originalUserList = this.userList;
    });
  }
 async openAttachmentsMenu(){
    this.isShowAttachments = !this.isShowAttachments;
    this.content.scrollToBottom();

  }

  captureAudio() {
    let options: CaptureImageOptions = { limit: 1 }
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    this.mediaCapture.captureAudio(options)
      .then(
        (data: MediaFile[]) => {
          // alert('data' + JSON.stringify(data))
          let that;
          that = this;
          const index = data[0].fullPath.lastIndexOf('/');
          let finalPath = data[0].fullPath.substr(0, index);
          if (this.platform.is('ios')) {
              finalPath = 'file://' + finalPath;
          }
          this.file.readAsArrayBuffer(finalPath, data[0].name).then(async (file) => {
            const blob = new Blob([file], { type: data[0].type });
            const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
                 'source':'Audio'
             },
            });
            await popover.present();
            popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
              }
            });
      this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/audio/' + data[0].name);
            firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/audio/' + data[0].name).put(blob).then(function (snapshot) {
              firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/audio/' + data[0].name).getDownloadURL().then(function (url) {
                popover.dismiss();
                that.audioFile = { type: 'audio', url: url, path: that.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/audio/' + data[0].name };
                that.showAudio = true;
                // that.sendAttachments(that.audioFile);
              });
            });
            that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/audio/' + data[0].name).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
          });
        },
        (err: CaptureError) => console.error('error' + JSON.stringify(err))
      );
  }
captureImage() {
let  urls;
let body=[];
const options: CameraOptions = {
quality: 20,
destinationType: this.camera.DestinationType.DATA_URL,
encodingType: this.camera.EncodingType.JPEG,
mediaType: this.camera.MediaType.PICTURE
}
this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
this.camera.getPicture(options).then(async (imageData) => {
           this.date = new Date().getTime();
            const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
                 'source':'Image'
             },
            });
            await popover.present();
            popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
              }
            });
           let that;
           that = this;
                  let img_url  = 'data:image/jpeg;base64,' + imageData;
                  fetch(img_url)
                  .then(res => res.blob())
                  .then(blob => {
                    urls = {'name' : this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + this.date};
      this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + this.date);
                    firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + this.date).put(blob).then(function(snapshot) {
                      setTimeout(() => {
                        firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                         popover.dismiss();
                         that.imageFile = { 'type': 'image', 'url' : url  , 'path': urls.name } ;
                         that.isImage = true;
                         });
                      }, 1000);
                    });
          that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + this.date).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                 });
                }, (err) => {
                  // Handle error
         alert(JSON.stringify(err));
    }); 
  }
  // captureVedio() {
  //   const options: CaptureVideoOptions = {
  //     limit: 1,
  //     duration: 30
  //   };
  //   this.mediaCapture.captureVideo(options)
  //   .then(
  //     (data: MediaFile[]) => {
  //       let that;
  //       that = this;
  //       const index = data[0].fullPath.lastIndexOf('/');
  //       const finalPath = data[0].fullPath.substr(0, index);
  //       this.file.readAsArrayBuffer(finalPath, data[0].name).then( async(file) => {
  //       const blob = new Blob([file], {type: data[0].type});
  //       const loading = await this.loadingCtrl.create({
  //         spinner: 'bubbles',
  //         message: 'Please wait...',
  //         translucent: true,
  //         cssClass: 'custom-class custom-loading'
  //       });
  //       loading.present();
  //       firebase.storage().ref('farmeasy/chatAttachments/video/' + data[0].name).put(blob).then(function(snapshot) {
  //         firebase.storage().ref('farmeasy/chatAttachments/video/' + data[0].name).getDownloadURL().then(function(url) {
  //           loading.dismiss();
  //          that.videoFile.push( { type: 'video', url : url,  path : 'farmeasy/chatAttachments/video/' + data[0].name });
  //          that.isVideo = true;
  //         });
  //        });
  //       });
  //     },
  //     (err: CaptureError) => console.error(err)
  //   );
  //  }
  messageTapped(event){
    console.log(event);
  }
 reply(message){
   console.log(message);
   this.replyObject = {};
    this.isReply = true;
    this.replyObject['message'] = message.message?message.message:null;
    this.replyObject['sender_name'] = message.sender_name;
    this.replyObject['sender_id'] = message.sender_id;
    this.replyObject['key'] = message.key;
    if(message.media){
      this.replyObject['media'] = message.media;
    }
    if(message.message_users){
      this.replyObject['message_users'] = message.message_users;
    }
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
 }
 removeReply(){
   this.isReply = false;
   this.replyObject = {};
 }
 triggerOnChildAdded(){
   firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).limitToLast(1).on('value',resp=>{
    let messageList = snapshotToArray(resp);
    if(messageList[0].message != "This Message has been Deleted" && this.messageList[this.messageList.length-1].key != messageList[0].key){
      this.previousCount+=messageList.length;
      for(let i=0;i<messageList.length;i++){
        this.isPlaying.push(false);
        this.isSelected.push(false);
        if(messageList[i].delete_from_me){
          // if(messageList[i].reply_for){
          //   console.log(messageList[i]);
          //   let reply = this.findReplyMessage(messageList,messageList[i].reply_for);
          //   messageList[i]['reply_for'] = reply;
          // }
          let index = messageList[i].delete_from_me.indexOf(this.user_id);
          if(index == -1){
            this.messageList.push(messageList[i]);
          }
        } else {
          // if(messageList[i].reply_for){
          //   console.log(messageList[i]);
          //   let reply = this.findReplyMessage(messageList,messageList[i].reply_for);
          //   messageList[i]['reply_for'] = reply;
          // }
          this.messageList.push(messageList[i]);
          // console.log(this.messageList);
         
        }
        setTimeout(() => {
          this.content.scrollToBottom();
        }, 500);
      }
    }
 
   });
 }
 messageChangesTrigger(){
  firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).on('child_changed',resp=>{
    // console.log(resp);
    // let key = resp.key
    // console.log(key);
    // let data = resp.val();
    let message = resp.val();
    let index;
    message['key'] = resp.key;
    // if(this.selectedIndex){
      for(let idx=0;idx<this.messageList.length;idx++){
        if(this.messageList[idx].key == message.key){
          this.messageList[idx] = message;
            this.isSelected[idx] = false;
            this.showCustomHeader = false;

        }
      }
    // }
    // this.updatedMessages.push(data);
    // console.log(data);
    // console.log(this.updatedMessages);
    // console.log(this.selectedIndex);
  });
  console.log("outSide of Function");

 }
  sendMessage() {
    if(this.messageText != " "){
      this.textarea.setFocus();
    let media = {};
    let messageLabel = "";
    if(this.audioFile){
      media = this.audioFile;
      messageLabel = 'Audio';
    } else if(this.imageFile){
      media = this.imageFile;
      messageLabel = 'Image';
    } else if(this.videoFile){
      media = this.videoFile;
      messageLabel = 'Video';
    } 
    let body = { 
                 message_users:this.message_users?this.message_users:null,
                 reply_for:this.replyObject?this.replyObject : null,
                 message: this.messageText?this.messageText:null,
                 media: media?media:null,
                 sender_name: this.sg['userdata'].name,
                 sender_id: this.sg['userdata'].farmeasy_id,
                 time: new Date().getTime()
                }
    let last_message = ""
    if(this.message_users){
    last_message = this.messageText.split('[').join("").split("]").join("");
    } else {
    last_message = this.messageText?this.messageText:messageLabel;
    }
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/' + this.groupDetails.key).update({ last_message:last_message,
                                                                                      time: new Date().getTime(),
                                                                                      sender_name: this.sg['userdata'].name });
    // if (this.makeActive) {
    //   firebase.database().ref('messaging/groups/group' + this.groupDetails.id).update({ active_members: this.groupDetails.active_members });
    // }
    firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).push(body);
    this.messageText = "";
    this.showAudio = false;
    this.isImage = false;
    this.isReply = false;
    this.audioFile = null;
    this.imageFile = null;
    this.replyObject = null;
    } else {
      this.messageText = "";
    }
  }

  listeningText(event){
    console.log(event.detail.value);
    console.log(event.detail.value.slice(-1));
    if(event.detail.value.slice(-1) === "@"){
      this.showAtTheRateMentions = true;
    }
    let index = event.detail.value.lastIndexOf("@")+1;
    let name = event.detail.value.substr(index,event.detail.value.length);
    if(!event.detail.value){
      this.showAtTheRateMentions = false;
    }
    this.searchPersons(name);
    }
    searchPersons(name){
      this.userList = this.originalUserList.filter(person => person.name.toLowerCase().includes(name));
      if(this.userList.length == 0){
      this.showAtTheRateMentions = false;
        this.userList = this.originalUserList;
      }
    }
  
  mentionName(person){

    this.message_users.push({"id" : person.farmeasy_id, "name" : person.name}) ;
    var lastIndex = this.messageText.lastIndexOf(" ");
    this.messageText = this.messageText.substring(0, lastIndex);      
    this.messageText = this.messageText + " " + "@[" + person.name + "]";
    this.showAtTheRateMentions = false;
    this.textarea.setFocus();

  }

  dispalyMessage(message,index){
    let chat = message;
    let new_message = ""
    let messageArray = message.message.split("@");
    let message_users = message.message_users;
    for(let i=0;i<messageArray.length;i++){
      if(messageArray[i].indexOf('[') != -1){
       let name = messageArray[i].split('[').pop().split(']')[0];
        for(var j=0;j<message_users.length;j++){
          if(message_users[j].name == name){
            new_message = new_message+" "+messageArray[i].replace("["+name+"]",'<span style="color:#65A0E6" (click)="openProfile('+message_users[j].id+')">'+"@"+message_users[j].name+'</span>');
            break;
          }
        }    
      }
      else{
        new_message = new_message+""+messageArray[i];
      }
    }
    return document.getElementById(index).innerHTML = new_message;
  }
  dispalyReplyMessage(message,index){
    let chat = message;
    let new_message = ""
    let messageArray = message.message.split("@");
    let message_users = message.message_users;
    for(let i=0;i<messageArray.length;i++){
      if(messageArray[i].indexOf('[') != -1){
       let name = messageArray[i].split('[').pop().split(']')[0];
        for(var j=0;j<message_users.length;j++){
          if(message_users[j].name == name){
            new_message = new_message+" "+messageArray[i].replace("["+name+"]",'<span style="color:#65A0E6" (click)="openProfile('+message_users[j].id+')">'+"@"+message_users[j].name+'</span>');
            break;
          }
        }    
      }
      else{
        new_message = new_message+""+messageArray[i];
      }
    }
    return document.getElementById('reply'+index).innerHTML = new_message;
  }
  async presentRemoveAlert(url){
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Are you sure want to delete this attachment?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.removeAttachment(url);
            // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
    removeAttachment(url) {
      let that;
      that = this;
        firebase.storage().ref(url).delete().then(function(snapshot) {
          that.audioFile = {};
          that.showAudio = false;
          that.imageFile = {};
          that.isImage = false;
          that.videoFile = {};
          that.isVideo = false;
        });
     }
     playAudio(media,idx){
      // alert(media.url)
      this.audio  = new Audio();
      this.audio.src = media.url;
      this.audio.load();
      this.isPlaying[idx] = true;
      this.audio.play();
      this.audio.addEventListener('ended', event =>{
        this.isPlaying[idx] = false;
        this.audio = null;
      });
    }
  stopAudio(idx){
    this.audio.pause(); 
    this.isPlaying[idx] = false;
    this.audio = null;
  }
  ngOnDestroy(){
    if(this.audio){
    this.audio.pause(); 
    this.audio = null;
    }
  }
  viewMedia(media){
    const data = {media: JSON.stringify(media)};
    this.navCtrl.navigateForward(['/view-media', data]);
  }
  
  openInfo(){
    const data = {group: JSON.stringify(this.groupDetails)};
    this.navCtrl.navigateForward(['/edit-group-description',data]);
  }
  back(){
    this.showCustomHeader = false;
    if(this.selectedIndex.length != 0){
      for(let idx of this.selectedIndex){
        this.isSelected[idx] = false;
      }
    }
    setTimeout(()=>{
      this.selectedIndex = [];
    },3000);
  }
  async deleteMessage(){
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Are you sure want to delete?',
      buttons: [
        {
          text: 'Me',
          handler: async (blah) => {
            let that = this;
            for(let idx of that.selectedIndex){
              let delete_from_me = [];
              if(that.messageList[idx].delete_from_me){
                delete_from_me = that.messageList[idx].delete_from_me;
                delete_from_me.push(that.user_id);
              } else {
                delete_from_me.push(that.user_id);
              }
             firebase.database().ref(that.sg['userdata'].vendor_id+'/chatroom/group/' + that.groupDetails.id+'/'+that.messageList[idx].key).update({delete_from_me:delete_from_me});
            // this.getGroupMessages();

              that.messageList.splice(idx,1);
              that.isSelected.splice(idx,1);
            }
            const toast = await this.toastCtrl.create({
              message: 'Message has been deleted from you',
              duration: 2000
            });
            toast.present();
             this.back();

          }
        }, {
          text: 'Everyone',
          handler: () => {
            let that = this;
            for(let idx of this.selectedIndex){
              if(this.messageList[idx].sender_id == this.user_id){
                if(idx == this.messageList.length-1){
                firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/' + this.groupDetails.key).update({last_message:"This Message has been Deleted",
                  time: new Date().getTime(),
                  sender_name: this.sg['userdata'].name });
                }
                firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id +'/'+this.messageList[idx].key+"/media").remove();
                firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id +'/'+this.messageList[idx].key+"/reply_for").remove();
                firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id +'/'+this.messageList[idx].key).update({delete_from_everyone:true,message:'This Message has been Deleted'});
                // this.getGroupMessages();
              } else {
                this.presentAlert();
              //   let delete_from_me = [];
              //   if(that.messageList[idx].delete_from_me){
              //     delete_from_me = that.messageList[idx].delete_from_me;
              //     delete_from_me.push(that.user_id);
              //   } else {
              //     delete_from_me.push(that.user_id);
              //   }
              //   firebase.database().ref(that.sg['userdata'].vendor_id+'/chatroom/group/' + that.groupDetails.id+'/'+that.messageList[idx].key).update({delete_from_me:delete_from_me});
               }
            }
          this.back();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, 
      ]
    });

    await alert.present();
    // this.getGroupMessages();
  }
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      message: 'you do not have permission to delete this message',
      buttons: ['OK']
    });
    await alert.present();
  }

  copyMessage(){
    let mediaFlag:boolean = false;
    console.log(this.content);
    this.clipboard.clear().then(async ()=>{
      let copyContent ="";
      for(let idx of this.selectedIndex){
        copyContent = copyContent+""+this.messageList[idx].sender_name+":"+this.messageList[idx].message+"\n";
         if(this.messageList[idx].media){
          mediaFlag = true;
        }
      }
      if(!mediaFlag){
        this.clipboard.copy(copyContent).then(async()=>{
          const toast = await this.toastCtrl.create({
            message: 'Copied',
            duration: 2000
          });
          toast.present();
          this.back();
          this.selectedIndex = [];
        });
      } else {
        const alert = await this.alertCtrl.create({
          header: 'Error',
          message: 'Not able to copy media file, Please unselect media messages',
          buttons: ['OK']
        });
    
        await alert.present();
      }

    },err => alert(err));
  }
  async viewGroupDescription(){
    let popover = await this.popoverController.create({
      component: GroupDetailsComponent,
      cssClass: "group-description",
      componentProps: {'group': this.groupDetails }
    });
    popover.onDidDismiss().then((detail) => {
      console.log(detail);
    });
    await popover.present();
  }

 async more(){
    console.log("More");
    let popover = await this.popoverController.create({
      component: ChatMenuOptionsComponent,
      cssClass: "group-chat-menu",
      componentProps: {'receipt': JSON.stringify("param")},
    });
    popover.onDidDismiss().then((detail) => {
      console.log(detail);
    });
    await popover.present();
  }

  loadData(event) {
    // console.log(this.content);
      this.count = this.count + 10;
      this.loadMoreMessages(this.count);
      setTimeout(() => {
      console.log('Done');
      event.target.complete();
      }, 500);
  }
loadMoreMessages(count){
  firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).limitToLast(count).once('value', resp => {
    let data = snapshotToArray(resp);
    this.messageCount = data.length;
    if(this.previousCount<this.messageCount){
      let messageList =  data.slice(0 ,10).reverse();
      for(let i=0;i<messageList.length;i++){
        this.isPlaying.unshift(false);
        this.isSelected.unshift(false);
        if(messageList[i].delete_from_me){
          if(messageList[i].reply_for){
            console.log(messageList[i]);
            let reply = this.findReplyMessage(messageList,messageList[i].reply_for);
            messageList[i]['reply_for'] = reply;
          }
          let index = messageList[i].delete_from_me.indexOf(this.user_id);
          if(index == -1){
            this.messageList.unshift(messageList[i]);
          }
        } else {
          if(messageList[i].reply_for){
            console.log(messageList[i]);
            let reply = this.findReplyMessage(messageList,messageList[i].reply_for);
            messageList[i]['reply_for'] = reply;
          }
          this.messageList.unshift(messageList[i]);
        }
       }
       this.previousCount = this.messageCount;
    }
    console.log(this.messageList.length);
    // console.log(this.content);
  });
}

onPress($event) {
    console.log("onPress", $event);
    this.pressState = 'pressing';
    this.startInterval();
}

onPressUp($event) {
    console.log("onPressUp", $event);
    this.pressState = 'released';
    this.stopInterval();
}

startInterval() {
  this.progress = 0;
  let that = this;
    this.interval = setInterval(function () {
        that.progress = that.progress + 1;
        if(that.progress == 5){
          that.showCustomHeader = true;
          that.stopInterval();
        }
        console.log(this.progress);
    }, 100);
}

stopInterval() {
    clearInterval(this.interval);
}
selectMessage(idx,chat){
  if(!this.isSelected[idx]){
    this.selectedIndex.push(idx);
    this.isSelected[idx] = true;
    if(chat.message == 'This Message has been Deleted'){
      this.deletedMessageCount++;
    }
  } else {
    this.isSelected[idx] = false;
    let index = this.selectedIndex.indexOf(idx);
    this.selectedIndex.splice(index,1);
    if(chat.message == 'This Message has been Deleted'){
      this.deletedMessageCount--;
    }
  }
}

}

export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};


