import { Component, ViewChild, NgZone, OnInit } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from "@angular/router";
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-media',
  templateUrl: './view-media.page.html',
  styleUrls: ['./view-media.page.scss'],
})
export class ViewMediaPage implements OnInit {
  media:any = {};
  title:any = ""
  constructor(private storage: Storage,
    private sg: SimpleGlobal,
    private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation: FarmeasyTranslate,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public zone: NgZone,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private location:Location) {
      this.route.params.subscribe(params => {
      this.media = JSON.parse(params['media']);
      console.log(this.media);
      });
     }

  ngOnInit() {
    this.getTitle();
  }
    getTitle(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.title = 'View Media';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.title = 'ಮಾಧ್ಯಮವನ್ನು ವೀಕ್ಷಿಸಿ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.title = 'மீடியாவைக் காண்க';
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.title = 'మీడియాను చూడండి';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.title = 'मीडिया देखें';
    }
  }
  back() {
    this.location.back();
  }

}
