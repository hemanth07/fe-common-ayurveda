import { Component, ViewChild, NgZone, OnInit } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from "@angular/router";
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Location } from '@angular/common';
import { AddToGroupComponent } from '../../components/messaging-components/add-to-group/add-to-group.component';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.page.html',
  styleUrls: ['./create-group.page.scss'],
})
export class CreateGroupPage implements OnInit {
  headerData:any = [];
  footerData:any = [];
  groupDetails:any = [];
  isGroupNameEdit:boolean = false;
  isGroupDescriptionEdit: boolean = false;
  users:any = [];
  usersList:any = [];
  existingUserIdList:any = [];
  existingUserIdListKeys:any = [];
  allusers:any = [];
  lastGroup:any = [];
  lastGroupId:number;
  groupNameLen:number = 0;
  groupDescLen:number = 0;
  rows:number =1;
  constructor(private storage: Storage,
    private sg: SimpleGlobal,
    private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation: FarmeasyTranslate,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public zone: NgZone,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private location:Location) {
      this.groupDetails['group_icon'] = "";
      this.groupDetails['name'] = "";
      this.groupDetails['description'] = "";
    }
    
  ngOnInit() {
    this.getTitle();
    this.getParticipants();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups').limitToLast(1).once('value',resp =>{
      let data = snapshotToArray(resp)[0];
      this.lastGroup = data;
      if(!this.lastGroup){
        this.lastGroupId = 1;
      } else {
        this.lastGroupId = this.lastGroup.id+1;
      }
      console.log(this.lastGroup);
    });
  }
  
  saveGroupName(){
    console.log(this.groupDetails.name);
    this.isGroupNameEdit = false;
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key).update({name:this.groupDetails.name});
  }
  editGroupDescription(){
    this.isGroupDescriptionEdit = true;
  }
  saveGroupDescription(){
    console.log(this.groupDetails.description);
    this.isGroupDescriptionEdit = false;
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key).update({description:this.groupDetails.description});
  }
  extractExistingUsers(users){
    this.existingUserIdList = [];
      Object.keys(users).forEach((key)=>{
        this.existingUserIdList.push(users[key].id);
      });
    this.existingUserIdListKeys = Object.keys(users);
    console.log(this.existingUserIdList);
  }
    getTitle(){
    let title
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Create Group';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಗುಂಪು ಮಾಹಿತಿ ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'குழு தகவல்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'సమూహ సమాచారం';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'समूह जानकारी';
    }
    this.headerData = {
      color: 'green',
      title: title,
      button2: 'home',
      button2_action: '/farm-dashboard',
      };
    this.footerData ={
                        'footerColor':'rgba(255, 255, 255)',
                        'middleButtonName':'Create Group',
                        'middleButtonColor':'green'
                        };
  }

  listenGroupName(event){
    // console.log(event);
    this.groupNameLen = event.detail.value.length;
  }
  listenGroupDesc(event){
    // console.log(event);
    this.groupDescLen = event.detail.value.length;
    if(this.groupDescLen == 40){
      this.rows = 2
    } else if(this.groupDescLen == 80){
      this.rows = 3
    } else if(this.groupDescLen == 120){
      this.rows = 4
    } else if(this.groupDescLen == 160){
      this.rows = 5
    } else if(this.groupDescLen == 200){
      this.rows = 6
    }
  }
async getParticipants(){
    const loading = await this.loadingCtrl.create({
    spinner: 'bubbles',
    message: 'Please wait...',
    translucent: true,
    cssClass: 'custom-class custom-loading'
  });
  loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').once('value',resp =>{
      let users = snapshotToArray(resp);
      this.allusers = snapshotToArray(resp);
      this.users = [];
      for(let user of users){
         this.users.push(user);
      }
      loading.dismiss();
    });
    // this.groupDetails.group_icon="https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2Fdefault.jpeg?alt=media&token=07684228-cd2b-4b0b-9a8c-7fbf4560e766";

  }
 async addParticipants(){
   let eligibleusers = [];
   if(this.usersList.length>0){
     let list = this.users;
     for(let unselecteduser of this.users){
       let count = 0;
      for(let user of this.usersList){
       if(user.id == unselecteduser.farmeasy_id){
         count++;
       }
      }
      if(count == 0){
        eligibleusers.push(unselecteduser);
      }
     }
   }
   if(this.usersList.length == 0){
     eligibleusers = this.users;
   }
    let popover = await this.popoverController.create({
      component: AddToGroupComponent,
      cssClass: "add-users-popup",
      componentProps: {'existingList':{ids:this.existingUserIdList,list:eligibleusers} }
    });
    popover.onDidDismiss().then((detail) => {
      console.log(detail);
       if(detail.data.length>0){
         for(let user of detail.data){
        this.usersList.push(user);
         }
      }
    });
    await popover.present();
  }
  chooseFile(id) {
    document.getElementById(id).click();
  }
  async upload(event,id) {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
        const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
            'source':'Image'
            },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
      this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + data.name);
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + data.name).put(data).then(function(snapshot) {
        popover.dismiss();
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' +  data.name).getDownloadURL().then(function(url) {
        if(id === 'profile'){
          that.groupDetails.group_icon = url;
        }
        });
      });
      that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + data.name).put(data).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
    } else {
      const alert = await this.alertCtrl.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
  }
  removeuser(user,index){
    this.usersList.splice(index,1);
   }
   async validateGroupDetails(){
     let validations = [];
     if(!this.groupDetails.name){
      validations.push("Group Name");
     }
     if(!this.groupDetails.description){
      validations.push("Group Description");
     }
    //  if(!this.groupDetails.group_icon){
    //   validations.push("Group Icon");
    //  }
     if(this.usersList.length == 0){
      validations.push("Group Members");
     }
     if(validations.length != 0){
      const alert = await this.alertCtrl.create({
        subHeader: 'Please fill manditory fields',
        message: validations.join(','),
        buttons: ['OK']
      });
      return await alert.present();
     } else {
      this.createGroup();
     }
     
   }
   createGroup(){
     let members = [];
     for(let user of this.usersList){
      members.push({id:user.id,time: new Date().getTime()});
     }
     members.push({id:this.sg['userdata'].farmeasy_id,time: new Date().getTime()});
     let body = {description: this.groupDetails.description,
                name:this.groupDetails.name,
                id:this.lastGroupId,
                group_icon:this.groupDetails.group_icon,
                members:members}
     firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups').push(body);
    firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' +this.lastGroupId).push({display_message: this.groupDetails.name +" is Created By "+ this.sg['userdata'].name,time: new Date().getTime()});

     this.location.back()
   }




 }


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
