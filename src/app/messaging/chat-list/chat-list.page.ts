import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from "@angular/router";
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { PopoverController,LoadingController } from '@ionic/angular';
import { GroupDetailsComponent } from '../../components/messaging-components/group-details/group-details.component';
@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.page.html',
  styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage implements OnInit {
headerData:any = [];
groupList:any = [];

  constructor(private storage: Storage, 
    private sg: SimpleGlobal,
    private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation:FarmeasyTranslate,
    private loadingCtrl:LoadingController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getTitle();
    this.getLabours();
  }
    getTitle(){
    let title
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Group chat';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಗುಂಪು ಚಾಟ್';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'குழு அரட்டை';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'సమూహ చాట్';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'समूह बातचीत';
    }
    if(this.sg['userdata'].role != 'employee'){
      this.headerData = {
        color: 'green',
        title: title,
        button1:'add-circle-outline',
        button1_action: '/create-group',
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    } else {
      this.headerData = {
        color: 'green',
        title: title,
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    }
    
  }
  convertTime(milli){
    if(milli){
      let date = new Date(milli);
    let hours = date.getHours()>12 ? date.getHours()-12:(date.getHours()<10?"0"+date.getHours():date.getHours());
    let min = date.getMinutes().toString().length == 1? "0"+date.getMinutes().toString():date.getMinutes().toString();
    let mar = date.getHours() >= 12 ? 'PM' : 'AM';
  
    return hours+":"+min+" "+mar;
    }     
  }
  async viewGroupDescription(group){
    let popover = await this.popoverController.create({
      component: GroupDetailsComponent,
      cssClass: "group-description",
      componentProps: {'group': group }
    });
    popover.onDidDismiss().then((detail) => {
      console.log(detail);
    });
    await popover.present();
  }
  async getLabours(){
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
       firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups').on('value', resp => {
        let grouplist= snapshotToArray(resp);
        this.groupList = [];
        for(let group of grouplist){
          let activelist = [];
          if(group.members){
            Object.keys(group.members).forEach((key)=>{
              activelist.push(group.members[key].id);
            });
            if(activelist){
              let index = activelist.indexOf(this.sg['userdata'].farmeasy_id);
              if(index != -1){
                this.groupList.push(group);
              }
            }
          }

        }
        loading.dismiss();
      });
    }
    createGroup(){
      this.navCtrl.navigateForward('/create-group');
    }
    naviagateToGropuChat(group){
      const data = {data: 'group', group: JSON.stringify(group)};
      this.navCtrl.navigateForward(['/group-chat', data]);
    }
  }
  
  export const snapshotToArray = snapshot => {
    const returnArr = [];
    snapshot.forEach(childSnapshot => {
      const item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });
    return returnArr;
  };