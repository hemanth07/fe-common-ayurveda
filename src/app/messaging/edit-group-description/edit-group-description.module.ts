import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditGroupDescriptionPage } from './edit-group-description.page';
import { ObservationComponentsModule } from './../../components/observation-components/observation-components.module';
import { MessagingcomponentsModule } from '../../components/messaging-components/messagingcomponents.module';
const routes: Routes = [
  {
    path: '',
    component: EditGroupDescriptionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessagingcomponentsModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditGroupDescriptionPage]
})
export class EditGroupDescriptionPageModule {}
