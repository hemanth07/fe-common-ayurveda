import { Component, ViewChild, NgZone, OnInit } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from "@angular/router";
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Location } from '@angular/common';
import { AddToGroupComponent } from '../../components/messaging-components/add-to-group/add-to-group.component';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-edit-group-description',
  templateUrl: './edit-group-description.page.html',
  styleUrls: ['./edit-group-description.page.scss'],
})
export class EditGroupDescriptionPage implements OnInit {
  headerData:any = [];
  groupDetails:any = [];
  isGroupNameEdit:boolean = false;
  isGroupDescriptionEdit: boolean = false;
  users:any = [];
  usersList:any = [];
  existingUserIdList:any = [];
  existingUserIdListKeys:any = [];
  allusers:any = [];
  groupNameLen:number = 0;
  groupDescLen:number = 0;
  rows:number = 1;
  constructor(private storage: Storage,
    private sg: SimpleGlobal,
    private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation: FarmeasyTranslate,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public zone: NgZone,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private location:Location) { 
      this.route.params.subscribe(params => {
      this.groupDetails = JSON.parse(params['group']);
      if(this.groupDetails){
        this.groupNameLen = this.groupDetails.name.length;
        this.groupDescLen = this.groupDetails.description.length;
      }
      console.log(this.groupDetails);
      this.extractExistingUsers(this.groupDetails.members);
      });
    }
    
  ngOnInit() {
    this.getTitle();
    this.getParticipants();
  }
  listenGroupName(event){
    // console.log(event);
    this.groupNameLen = event.detail.value.length;
  }
  listenGroupDesc(event){
    // console.log(event);
    this.groupDescLen = event.detail.value.length;
    if(this.groupDescLen == 40){
      this.rows = 2
    } else if(this.groupDescLen == 80){
      this.rows = 3
    } else if(this.groupDescLen == 120){
      this.rows = 4
    } else if(this.groupDescLen == 160){
      this.rows = 5
    } else if(this.groupDescLen == 200){
      this.rows = 6
    }
  }
  editGroupName(){
    this.isGroupNameEdit = true;
  }
  saveGroupName(){
    console.log(this.groupDetails.name);
    this.isGroupNameEdit = false;
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key).update({name:this.groupDetails.name});
  }
  editGroupDescription(){
    this.isGroupDescriptionEdit = true;
  }
  saveGroupDescription(){
    console.log(this.groupDetails.description);
    this.isGroupDescriptionEdit = false;
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key).update({description:this.groupDetails.description});
  }
  extractExistingUsers(users){
    this.existingUserIdList = [];
      Object.keys(users).forEach((key)=>{
        this.existingUserIdList.push(users[key].id);
      });
    this.existingUserIdListKeys = Object.keys(users);
    console.log(this.existingUserIdList);
  }
    getTitle(){
    let title
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Group Info';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಗುಂಪು ಮಾಹಿತಿ ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'குழு தகவல்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'సమూహ సమాచారం';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'समूह जानकारी';
    }
    this.headerData = {
      color: 'green',
      title: title,
      button2: 'home',
      button2_action: '/farm-dashboard',
      };
  }

async getParticipants(){
    const loading = await this.loadingCtrl.create({
    spinner: 'bubbles',
    message: 'Please wait...',
    translucent: true,
    cssClass: 'custom-class custom-loading'
  });
  loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').on('value',resp =>{
      let users = snapshotToArray(resp);
      this.allusers = snapshotToArray(resp);
      this.usersList = [];
      this.users = [];
      for(let user of users){
        let index  = this.existingUserIdList.indexOf(user.farmeasy_id);
        if(index != -1){
          let body = { farmeasy_id :user.farmeasy_id, profile_url:user.profile_url, name:user.name, role:user.role}
          this.usersList.push(body);
        } else {
          this.users.push(user);
        }
      }
      loading.dismiss();
    });
  }
 async addParticipants(){
    let popover = await this.popoverController.create({
      component: AddToGroupComponent,
      cssClass: "add-users-popup",
      componentProps: {'existingList':{ids:this.existingUserIdList,list:this.users} }
    });
    popover.onDidDismiss().then((detail) => {
      console.log(detail);
      if(detail.data.length>0){
        for(let user of detail.data){
        firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key+'/members').push({id:user.id,time: new Date().getTime()});
        firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).push({display_message:user.name +" was added by "+ this.sg['userdata'].name,time: new Date().getTime()});
        this.existingUserIdList = [];
        this.existingUserIdListKeys = [];
        firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key+'/members').once('value',resp =>{
          let exist = snapshotToArray(resp);
          for(let user of exist){
            this.existingUserIdListKeys.push(user.key);
            this.existingUserIdList.push(user.id);
          }
        });
        this.usersList.push(user);
        // this.existingUserIdList.push(user.id);
        }
      }
    });
    await popover.present();
  }
  chooseFile(id) {
    document.getElementById(id).click();
  }
  async upload(event,id) {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
       const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
      this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + data.name);
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + data.name).put(data).then(function(snapshot) {
        popover.dismiss();
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' +  data.name).getDownloadURL().then(function(url) {
        if(id === 'profile'){
          that.groupDetails.group_icon = url;
          firebase.database().ref(that.sg['userdata'].vendor_id+'/messaging/groups/'+that.groupDetails.key).update({group_icon:that.groupDetails.group_icon});
        }
        });
      });
      that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/chatAttachments/image/' + data.name).put(data).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
    } else {
      const alert = await this.alertCtrl.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
  }
  removeuser(user,index){
    let id = user.farmeasy_id?user.farmeasy_id:user.id;
    let idx =  this.existingUserIdList.indexOf(id);
    let key = this.existingUserIdListKeys[idx];
    firebase.database().ref(this.sg['userdata'].vendor_id+'/messaging/groups/'+this.groupDetails.key+'/members/'+key).remove();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/chatroom/group/' + this.groupDetails.id).push({display_message:user.name +" was removed by "+ this.sg['userdata'].name,time: new Date().getTime()});
    this.existingUserIdListKeys.splice(idx,1);
    this.existingUserIdList.splice(idx,1);
    this.usersList.splice(index,1);
    for(let user of this.allusers){
      if(user.farmeasy_id == id){
        this.users.push(user);
      }
    }
    console.log(user);
  }
}


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
