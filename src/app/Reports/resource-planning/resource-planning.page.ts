import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, PopoverController, LoadingController} from '@ionic/angular';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';
import { snapshotToArray } from 'src/app/languages/others.service';

@Component({
  selector: 'app-resource-planning',
  templateUrl: './resource-planning.page.html',
  styleUrls: ['./resource-planning.page.scss'],
})
export class ResourcePlanningPage implements OnInit {
  headerData:any;
  date:any;
  resource_data:any = [];
  label:any ={};
  @ViewChild('datePicker') datePicker;
  constructor(
    private popoverController: PopoverController,
    private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService
  ) { }

  ngOnInit() {
    this.createHeader();
    this.prepareData();
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Labour Work assignment';
      this.label['month'] = "Month & Year";
      this.label['resource'] = "Resources";
      this.label['week'] = "Week"
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'లేబర్ వర్క్ అసైన్‌మెంట్';
      this.label['month'] = "నెల & ఇయర్";
      this.label['resource'] = "రిసోర్స్";
      this.label['week'] = "వారం"
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'தொழிலாளர் பணி நியமனம்';
      this.label['month'] = "மாதம் வருடம்";
      this.label['resource'] = "வள";
      this.label['week'] = "வாரம்"
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಕಾರ್ಮಿಕ ಕೆಲಸದ ನಿಯೋಜನೆ';
      this.label['month'] = "ತಿಂಗಳು ವರ್ಷ";
      this.label['resource'] = "ಸಂಪನ್ಮೂಲ";
      this.label['week'] = "ವಾರ"
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'लेबर वर्क असाइनमेंट';
      this.label['month'] = "महीना वर्ष";
      this.label['resource'] = "संसाधन";
      this.label['week'] = "सप्ताह"
    } 
    this.headerData = {
        color: 'blue',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        };
  }
  ionViewDidEnter(){
   this.labourAssignment();
  }
  labourAssignment(){
    let Assignment = [];
    firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table').once('value',resp =>{
      let data = snapshotToArray(resp);
      for(let task of data){
        if(task.assignee.farmeasy_id){
          let index = parseInt(task.assignee.farmeasy_id.split("-")[1]);
          if(!Assignment[index]){
            Assignment[index] = [];
            Assignment[index]['completed'] = [];
            Assignment[index]['not_completed'] = [];
            // Assignment[index]['task'] = [];
          }
          if(task.status.name == "Task completed"){
            Assignment[index]['completed'].push(task);
          } else {
            Assignment[index]['not_completed'].push(task);
          }
          // Assignment[index]['task'].push(task);
        }
 
      }
      console.log(data);
      console.log(Assignment)
    });
  }
  prepareData(){
    this.resource_data = [{img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2FDan-Singh.jpeg?alt=media&token=56936fa5-42ca-4eed-8c14-26f0e650e6a1",
                          week1:100,
                          week2:75,
                          week3:50,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2FChellaiah.jpeg?alt=media&token=dedfd4ab-a2cc-48aa-884b-3c0b82cb3de2",
                          week1:100,
                          week2:90,
                          week3:50,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2FSudhakar.jpeg?alt=media&token=988c8b57-04cd-4825-b7eb-49188d9e4267",
                          week1:100,
                          week2:75,
                          week3:50,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2Fdefault.jpeg?alt=media&token=07684228-cd2b-4b0b-9a8c-7fbf4560e766",
                          week1:100,
                          week2:75,
                          week3:80,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2FDan-Singh.jpeg?alt=media&token=56936fa5-42ca-4eed-8c14-26f0e650e6a1",
                          week1:100,
                          week2:75,
                          week3:50,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2FChellaiah.jpeg?alt=media&token=dedfd4ab-a2cc-48aa-884b-3c0b82cb3de2",
                          week1:100,
                          week2:90,
                          week3:50,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2FSudhakar.jpeg?alt=media&token=988c8b57-04cd-4825-b7eb-49188d9e4267",
                          week1:100,
                          week2:75,
                          week3:50,
                          week4:25},
                          {img:"https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2Fdefault.jpeg?alt=media&token=07684228-cd2b-4b0b-9a8c-7fbf4560e766",
                          week1:100,
                          week2:75,
                          week3:80,
                          week4:25}
                        ]
  }
  async pickDate() {
    const popover = await this.popoverController.create({
         component: CalendercomponentComponent,
         cssClass: 'popoverclass',
     });
     popover.onDidDismiss().then((data) => {
         console.log(data);
          this.date = data.data;
     });
     await popover.present();
}


}
