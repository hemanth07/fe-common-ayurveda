import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResourcePlanningPage } from './resource-planning.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';

const routes: Routes = [
  {
    path: '',
    component: ResourcePlanningPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    HarvestcomponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResourcePlanningPage]
})
export class ResourcePlanningPageModule {}
