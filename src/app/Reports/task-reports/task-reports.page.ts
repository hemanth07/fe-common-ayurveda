import { FarmeasyTranslate } from './../../translate.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController,Events, PopoverController,AlertController, LoadingController,IonInfiniteScroll } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import { OverlayEventDetail } from '@ionic/core';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import * as firebase from 'firebase';
import { PopupTaskPlannerDeleteComponent } from '../../components/task-manger-components/popup-task-planner-delete/popup-task-planner-delete.component'
import { PopupTaskPlannerUpdateComponent } from '../../components/task-manger-components/popup-task-planner-update/popup-task-planner-update.component'
import { PopupTaskPlannerFilterComponent } from '../../components/task-manger-components/popup-task-planner-filter/popup-task-planner-filter.component'
import { KanadaService } from '../../languages/kanada.service';
import { TeluguService } from '../../languages/telugu.service';
import { TamilService } from '../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';

@Component({
  selector: 'app-task-reports',
  templateUrl: './task-reports.page.html',
  styleUrls: ['./task-reports.page.scss'],
})
export class TaskReportsPage implements OnInit {
  @ViewChild('infi') infi: ElementRef;
  @ViewChild('content') content : ElementRef;
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable');
  headerData: any;
  fromOrTodate: any;
  toDate: any;
  fromDate: any;selected_block:any;selected_category:any;selected_user:any;selected_status:any;
  totalTaskData: any = [];
  filterTasksByDate:any=[];
  tasksData:any=[];
  task_logData:any=[];delteTaskData:any=[];
  loadComponent:any;
  refreshData:boolean=false;
  color:any = '#ff9800';searchedValue:any;
  updateTaskData:any = [];selectAllTasksData:boolean=false;
  clearFiltersData:boolean = false;
  users:any = [];
  blocks:any = [];
  categories:any = [];
  categoryName: any;
  statusValue:any;
  blockName:any;
  labourName:any;
  tasks:any = [];
  totalTasksData:any = [];
  displayTaskData:any = [];
  deletedTasksData:any = [];
  showCompleted:any = [];
  check:any =[];
  labels:any;
  count = 10;
  previousLen:number;
  wantToFilter:boolean = false;
  selectedfilterData:any;
  searchplaceholder:any
  dueby_label:any;
  overDueby_label:any;
  dueTomorrow_label:any;
  dueToday_label:any;
  days_label:any;
  no_task_label:any;
  isLabour:boolean = false;
  wait:string;
  constructor(public sg: SimpleGlobal, 
    private navCtrl: NavController, 
    private popoverController: PopoverController,
    private alertController:AlertController,
    private loadingController:LoadingController,
    private translate: FarmeasyTranslate,
    public events: Events,
    private en: EnglishService, 
    private hi: HindiService, 
    private ta: TamilService, 
    private te: TeluguService,
    private kn: KanadaService) { }

  ngOnInit() {
      let data
     if(this.sg['userdata'].primary_language === 'en'){
     data = 'Task Reports';
     this.searchplaceholder = 'Enter to Search'
      this.dueby_label = "Due By";
      this.overDueby_label = "Overdue by";
      this.dueToday_label = "Due Today";
      this.dueTomorrow_label = "Due Tomorrow";
      this.days_label = "Days";
      this.no_task_label ="No Task Available"
      this.wait = "Please Wait ..."
    } else if(this.sg['userdata'].primary_language === 'te'){
     data = 'టాస్క్ రిపోర్ట్స్';
     this.searchplaceholder = 'శోధనకు నమోదు చేయండి'
     this.dueby_label = "Due By";
     this.overDueby_label = "ఆలస్యం";
     this.dueToday_label = "ఈ రోజు గడువు";
     this.dueTomorrow_label = "రేపు డ్యూ";
     this.days_label = "రోజులు";
     this.no_task_label ="టాస్క్ అందుబాటులో లేదు"
     this.wait = "దయచేసి వేచి ఉండండి ..."
    } else if(this.sg['userdata'].primary_language === 'ta'){
     this.searchplaceholder = 'தேடலில் உள்ளிடவும்'
     data = 'பணி அறிக்கைகள்';
     this.dueby_label = "Due By";
     this.overDueby_label = "தாமதமாக";
     this.dueToday_label = "இன்று டியூ";
     this.dueTomorrow_label = "காரணமாக நாளை";
     this.days_label = "நாட்கள்";
     this.no_task_label ="பணி கிடைக்கவில்லை"
     this.wait = "தயவுசெய்து காத்திருங்கள் ..."
    } else if(this.sg['userdata'].primary_language === 'kn'){
     this.searchplaceholder = 'ಹುಡುಕಾಟಕ್ಕೆ ನಮೂದಿಸಿ'
     data = 'ಕಾರ್ಯ ವರದಿಗಳು';
     this.dueby_label = "Due By";
     this.overDueby_label = "ತಡವಾಗಿ";
     this.dueToday_label = "ಇಂದು";
     this.dueTomorrow_label = "ನಾಳೆ ಕಾರಣ";
     this.days_label = "ದಿನಗಳು";
     this.no_task_label ="ಯಾವುದೇ ಕಾರ್ಯ ಲಭ್ಯವಿಲ್ಲ"
     this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
    }  else if(this.sg['userdata'].primary_language === 'hi'){
     this.searchplaceholder = 'खोज करने के लिए दर्ज करें'
     data = 'कार्य रिपोर्ट';
     this.dueby_label = "Due By";
     this.overDueby_label = "देर से";
     this.dueToday_label = "आज दवे";
     this.dueTomorrow_label = "कल के लिए नियत";
     this.days_label = "दिन";
     this.no_task_label ="कोई टास्क उपलब्ध नहीं है"
     this.wait = "कृपया प्रतीक्षा करें ..."
    }  
    if(this.sg['userdata'].role!='employee') {
      this.headerData = {
        'title': data, 
        'color': 'blue',
        'button1': 'home', 
        'button1_action': '/farm-dashboard',
        'button3': './assets/images/Recycle_Bin_white.svg',
        'button4': 'create',
        'button5': 'ios-funnel'
      };
    }else{
      this.headerData = {
        'title': data, 'back': '/farm-dashboard', 'color': 'green',
        button2: 'home',
        button2_action: '/farm-dashboard',
      };
    }
    this.translation();
  }
 async filterTasks(event){
      this.refreshData=false;
    const popover = await this.popoverController.create({
      component: PopupTaskPlannerFilterComponent,
      cssClass: "popover_TaskDelete",
      componentProps: {'data': {blocks:this.blocks,users:this.users,categories:this.categories,selected:this.selectedfilterData}},
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      if(detail.data != 'clear'){
        console.log(detail);
        console.log("filter");
        this.searchData(detail.data);
        this.selectedfilterData = detail.data;
      } else{
        this.selectedfilterData = {}
        this.initializeItems();

      }
    });
    await popover.present();
  }
   checked(status, data, idx) {
    if (status == true) {
      // this.deletedTasksData.push(this.displayTaskData[idx])
      this.deletedTasksData.push(this.tasksData[idx])
    } else {
      for (var i = 0; i < this.deletedTasksData.length; i++) {
        if (data.key == this.deletedTasksData[i].key) {
          this.deletedTasksData.splice(i, 1);
        }
      }
    }
    this.delteTask(this.deletedTasksData);
  }
  getTask(data) {
    console.log(this.content);
    let temp = { 'task': JSON.stringify(data) };
    this.navCtrl.navigateForward(['/task-detailed-view',temp]);
  }
  markAsDone(val, idx) {
    let status = { "name": 'Task completed' };
    var data = { 'key': val.key, 'status': status };
    // this.showCompleted[idx] = true;
    this.updateStatus(data);
  }

  translation() {
    if (this.sg['userdata'].primary_language === 'en') {
      this.labels = this.en.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'te') {
      this.labels = this.te.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'ta') {
      this.labels = this.ta.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'kn') {
      this.labels = this.kn.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'hi') {
      this.labels = this.hi.getTaskListLabels();
    }
  }
  async openDate(val) {
    this.fromOrTodate = val;
    let popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popover_class",
      componentProps: {'previous': true},
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getSelectedDate(detail.data);
    });
    await popover.present();
  }
  getSelectedDate(val) {
    console.log(this.fromOrTodate);
    if (this.fromOrTodate == 'toDate') {
      this.toDate = val;
      console.log(this.toDate);
    }
    if (this.fromOrTodate == 'fromDate') {
      this.fromDate = val;
      console.log(this.fromDate);
    }
  }
  // totalTasksList(val) {
  //   console.log(val);
  //   this.totalTaskData = val;
  //   this.taskList();
  // }
  taskList(){
    this.tasksData=this.totalTaskData
  }
  async searchData(val) { 
    console.log(val);
    var from_date = val.fromDate
    var to_date = val.toDate
    if(from_date && to_date){
      const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    let firebase_task_table;
    firebase_task_table = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table').orderByChild('date').startAt(from_date).endAt(to_date);
    firebase_task_table.on('value', resp => {
      var data1 = [];
      this.wantToFilter = true;
      data1 = snapshotToArray(resp).filter(item => item.assignee&&(item.date==from_date || item.date == to_date));
      console.log(data1);
      this.check = [];
      this.count = 10;
      this.displayTaskData = [];
      this.processTasks(data1,loading);
      this.filterData(val);
    });
    } else {
      this.filterData(val);
    }
   }
  
  ionViewDidEnter(){
   this.initializeItems();
   this.taskPlannerDataPreparation();
   }
   ngOnChanges() {
    }
  async initializeItems() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    let firebase_task_table;
    // if (this.sg['userdata'].role == 'employee'){
    //   let date = new Date(new Date().toISOString().split("T")[0]).getTime();
    //   this.isLabour = true;
    //   firebase_task_table = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table').orderByChild('assignee/farmeasy_id').equalTo(this.sg['userdata'].farmeasy_id);
    // } else {
      let date = new Date(new Date().toISOString().split("T")[0]).getTime()+604800000;
      let today = new Date(new Date().toISOString().split("T")[0]).getTime();
      firebase_task_table = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table');
    // }
    firebase_task_table.on('value', resp => {
      this.tasksData = [];
      var data1 = [];
      let today = new Date(new Date().toISOString().split("T")[0]).getTime();
      let yesterday = new Date(new Date().toISOString().split("T")[0]).getTime()-86400000;
      data1 = snapshotToArray(resp).filter(item => item.assignee);
      this.check = [];
      this.wantToFilter = false;
      this.displayTaskData = [];
      this.count = 10;
      this.processTasks(data1,loading);
      
    });
  }

  processTasks(data1,loading){
    if (data1.length > 0) {
      this.tasksData = [];
      let data = data1;
      for (var i = 0; i < data.length; i++) {
      this.check.push(false)
        if (data[i].task_type) {
          let today_date = new Date();
          var current_month = today_date.getMonth() + 1;

          let currentDate = today_date.getFullYear() + '-' + current_month + "-" + today_date.getDate();

          var one_day = 1000 * 60 * 60 * 24;
          var date1 = new Date(today_date);
          var date1_ms = date1.getTime();
          var date2_ms = data[i].date;
          var difference_ms = date2_ms - date1_ms;
          difference_ms = difference_ms / 1000;
          var seconds = Math.floor(difference_ms % 60);
          difference_ms = difference_ms / 60;
          var minutes = Math.floor(difference_ms % 60);
          difference_ms = difference_ms / 60;
          var hours = Math.floor(difference_ms % 24);
          var days = Math.floor(difference_ms / 24);
          data[i]['days'] = days;

          if(data[i].status && data[i].status.name=="Task completed"){
            data[i]['taskStatus']='Closed'
          } else {
            data[i]['taskStatus']='Open';
          }
          if (days > 0) {
            if (days >= 1) {

              data[i]["dueDate"] = this.dueby_label+' ' + (days + 1) + ' '+this.days_label;
              data[i]['color'] = "green";

            }
          }
          if (days == 0) {
            data[i]['color'] = "green";
            data[i]["dueDate"] =  this.dueTomorrow_label;
          }
          if (days < 0) {
            if (days == -1) {
              data[i]['color'] = "orange";
              data[i]["dueDate"] = this.dueToday_label;
            }
            else {
              data[i]['color'] = "red";
              data[i]["dueDate"] =  this.overDueby_label+' ' + Math.abs(days) + ' '+this.days_label;
            }
          }
        if (this.sg['userdata'].role == 'employee') {
          if(data[i]['taskStatus']=='Open'){
            if (days <= -1) {
              this.tasksData.push(data[i]);
            }
          }
        } else {
            if (days <= 6) {
              this.tasksData.push(data[i]);
            }
        }
        }
      }
      // if (this.sg['userdata'].role == 'employee') {
      //   this.tasksData = this.sortTasks(this.tasksData)
      // } else {
        this.tasksData.reverse();
        // this.tasksData = this.sortTasksByDues(this.tasksData);
      // }
       this.translating(this.tasksData);
      console.log(this.tasksData);
      this.totalTasksData =  [...this.tasksData];
      if(this.wantToFilter == false){
        if(this.tasksData.length<=this.count){
         this.count = this.tasksData.length;
        }
        for(let i=0 ;i<this.count; i++){
            this.displayTaskData.push(this.tasksData[i]);
        }
      }
      // console.log(this.displayTaskData);
    }
    else {
      
    }
     setTimeout(() => {
        loading.dismiss();
      }, 1000);
  }


  sortTasks(array) {
    var done = false;
    while (!done) {
      done = true;
      for (var i = 1; i < array.length; i += 1) {
        if (array[i - 1].days > array[i].days) {
          done = false;
          var tmp = array[i - 1];
          array[i - 1] = array[i];
          array[i] = tmp;
        }
      }
    }

    return array;
  }
  sortTasksByDues(tasks) {
    let due: any = [];
    let overDue: any = [];
    let today: any = [];
    let tommorow: any = [];
    let tasksData = [];
    this.tasksData = []; 
    let temp: any = []
    for (var i = 0; i < tasks.length; i++) {
      if (tasks[i].days == -1) {
        today.push(tasks[i]);
      }
      if (tasks[i].days == 0) {
        tommorow.push(tasks[i]);
      }
      if (tasks[i].color == 'red') {
        overDue.push(tasks[i]);
      }
      if (tasks[i].color == 'green' && tasks[i].days >= 1) {
        due.push(tasks[i]);
      }

    }
    overDue = this.sortTasks(overDue);
    due = this.sortTasks(due);
    if (overDue.length > 0) {
      tasksData.push(overDue);
    }
    if (today.length > 0) {
      tasksData.push(today);
    }
    if (tommorow.length > 0) {
      tasksData.push(tommorow);
    }
    if (due.length > 0) {
      tasksData.push(due);
    }
    for (var i = 0; i < tasksData.length; i++) {
      for (var j = 0; j < tasksData[i].length; j++) {
        temp.push(tasksData[i][j]);
      }
    }
    return temp;
  }
  translating(data) {
    let temp = data
    let menu = [];
    for (let item of data) {
      let obj = {
        description: item.description ? item.description : 'No Description',
        labour: item.assignee.name
      }
      menu.push(obj)
    }
    for (let i = 0; i < menu.length; i++) {
      if (this.sg['userdata'].primary_language !== 'en') {
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.tasksData[i]['description'] = data['description'];
          this.tasksData[i]['assignee']['name'] = data['labour']
        });
      } else {
        this.tasksData[i]['description'] = menu[i]['description'];
        this.tasksData[i]['assignee']['name'] = menu[i]['labour'];

      }
    }
  }
  taskPlannerDataPreparation(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp => {
      var data = [];
      data = snapshotToArray(resp);
      this.blocks = data;
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').once('value', resp => {
      var data = [];
      data = snapshotToArray(resp);
      this.categories = data;
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users/').once('value', resp => {
      var data = [];
      data = snapshotToArray(resp);
      data.filter(data => {
        if (data.role == 'employee')
          this.users.push(data);

      })
    });
  }
  initializeTasks(){
    this.displayTaskData = [];
    if(this.tasks.length>0){
    this.tasksData=this.tasks;
    } else {
    this.tasksData=this.totalTasksData;
    }
  }
  filterData(val) {
     this.initializeTasks();
    if (val.category) {
    //  this.initializeTasks();
      this.tasksData = this.tasksData.filter(role => role.category && role.category.name == val.category);
    }
    if (val.status) {
      //  this.initializeTasks();
      this.tasksData = this.tasksData.filter(role => role.taskStatus == val.status);
      
    }
    if (val.block) {
    //  this.initializeTasks();
      this.tasksData = this.tasksData.filter(role => role.blocks.find(group => val.block.includes(group.name)));
      
    }
    if (val.labour) {
      //  this.initializeTasks();
      this.tasksData = this.tasksData.filter(role => role.assignee.name == val.labour);
    }
    if(this.wantToFilter){
      if(this.tasksData.length<=this.count){
      this.count = this.tasksData.length;
      }
        for(let i=0 ;i<this.count; i++){
            this.displayTaskData.push(this.tasksData[i]);
        }
    }
      // console.log(this.displayTaskData);

  }
  getItems(value) { 
    // this.hideSearchIcon = false;
    let val = value.detail.value;
    if(val == ''){
      if(this.tasks.length>0){
        this.tasksData = this.tasks;
      } else {
      this.tasksData = this.totalTasksData;
      }
    }
    if (val && val.trim() != '') {
      if(this.tasks.length>0){
        this.tasksData = this.tasks;
      } else {
      this.tasksData = this.totalTasksData;
      }
      this.tasksData = this.tasksData.filter((item) => {
        return (item.description.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    this.displayTaskData = this.tasksData;
  }
  block(val){
    this.blockName = val;
    // this.filterData();
  }
  category(val){
    this.categoryName = val;
    // this.filterData();
  }
  user(val){
    this.labourName = val;
    // this.filterData();
  }
  status(val){
    this.statusValue = val;
    // this.filterData();
  }
  updateStatus(val){
    console.log(val);
    var today_date = new Date();
    var date_in_ms = today_date.getTime();
    firebase.database().ref(this.sg['userdata'].vendor_id+"/taskManager/task_table/"+val.key)
    .update({'status': val.status,'updated_date':date_in_ms});
    this.task_logData['updated'] = { "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone, 
    'role': this.sg["userdata"].role,'profile_url': this.sg["userdata"].profile_url,'date': date_in_ms};
    this.task_logData['task_id'] = val.key;
    this.task_logData['status'] = val.status;
    let logData = this.taskManager_Task_log.push();
    logData.set(this.task_logData);
  }
  nofication_count(){
    let data={"page_status":JSON.stringify("tm-mainmenu")}
    this.navCtrl.navigateForward(['/notifications',data]);
  }
  delteTask(val){
    this.updateTaskData = val;
    let keys = [];
    for(let key of val){
      keys.push(key.key);
    }
    this.delteTaskData= keys;
  }
async deleteTask(ev) {
    if(ev=='delete' && this.delteTaskData.length>0){
      this.refreshData=false;
    const popover = await this.popoverController.create({
      component: PopupTaskPlannerDeleteComponent,
      cssClass: "popover_TaskDelete",
      componentProps: {
       'count':this.delteTaskData,
      },
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      if(detail.data=='update'){
      // this.delteTaskData = [];
      // this.updateTaskData=[];
      this.refreshData=true;
      this.deletedTasksData=[];
      //this.getPlots(detail.data);
      }else if(detail.data=='cancel'){
       this.refreshData=false;
      }
    });
    await popover.present();
  } else {
    let message;
    if(this.sg['userdata'].primary_language === 'en'){
      message  = 'Select the tasks to Delete'
     } else if(this.sg['userdata'].primary_language === 'te'){
      message  =  'తొలగించడానికి పనులను ఎంచుకోండి'
     } else if(this.sg['userdata'].primary_language === 'ta'){
      message  = 'நீக்க பணிகளைத் தேர்ந்தெடுக்கவும்'
     } else if(this.sg['userdata'].primary_language === 'kn'){
      message  = 'ಅಳಿಸಲು ಕಾರ್ಯಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ'
     } else if(this.sg['userdata'].primary_language === 'hi'){
      message  ='डिलीट करने के लिए कार्यों का चयन करें'
     }
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
  }
  selectedAll(val){
    this.selectAllTasksData=val;
  }
  clearFilters(val){
      this.blockName = null;
      this.categoryName = null;
      this.statusValue = null;
      this.labourName = null;
    this.tasksData=this.totalTasksData;

    // this.clearFiltersData =val;
    setTimeout(()=>{
      this.clearFiltersData = !this.clearFiltersData;
    },1000)
  }
  selected(data){
    console.log(data);
  }
  // getItems(ev){
  //   if(ev.target.value){
  //   this.searchedValue=ev.target.value;
  //   console.log(this.searchedValue);
  //   }else{
  //     this.searchedValue='emptyData';
  //   }
  // }
  onCancel($event){

  }
  cancelSearch(){
    
  }
async updateTask(ev){
  let showError = false;
  for(let i=0;i<this.displayTaskData.length;i++){
    if(this.displayTaskData[i].status.name == 'Task completed' && this.check[i] == true){
      showError = true;
      break;
    }
  }
  if(showError){
    let message;
    if(this.sg['userdata'].primary_language === 'en'){
      message  ='Not able to update Completed Tasks, Please check selected task before updating'
     } else if(this.sg['userdata'].primary_language === 'te'){
      message  = 'పూర్తయిన పనులను నవీకరించడం సాధ్యం కాదు, దయచేసి నవీకరించడానికి ముందు ఎంచుకున్న పనిని తనిఖీ చేయండి'
     } else if(this.sg['userdata'].primary_language === 'ta'){
      message  = 'முடிக்கப்பட்ட பணிகளைப் புதுப்பிக்க முடியவில்லை, புதுப்பிப்பதற்கு முன் தேர்ந்தெடுக்கப்பட்ட பணியைச் சரிபார்க்கவும்'
     } else if(this.sg['userdata'].primary_language === 'kn'){
      message  = 'ಪೂರ್ಣಗೊಂಡ ಕಾರ್ಯಗಳನ್ನು ನವೀಕರಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ, ದಯವಿಟ್ಟು ನವೀಕರಿಸುವ ಮೊದಲು ಆಯ್ದ ಕಾರ್ಯವನ್ನು ಪರಿಶೀಲಿಸಿ'
     } else if(this.sg['userdata'].primary_language === 'hi'){
      message  ='पूर्ण कार्य अद्यतन करने में सक्षम नहीं है, कृपया अद्यतन करने से पहले चयनित कार्य की जाँच करें'
     }
    const alert = await this.alertController.create({
      header: 'Error',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  } else {
    if(ev=='update' && this.updateTaskData.length>0){
      this.refreshData=false;
      const popover = await this.popoverController.create({
        component: PopupTaskPlannerUpdateComponent,
        cssClass: "popover_class",
        componentProps: {
         'taskData':this.updateTaskData
        },
      });
      popover.onDidDismiss().then((detail: OverlayEventDetail) => {
        //this.getPlots(detail.data);
        if(detail.data=='update'){
          // this.delteTaskData = [];
          // this.updateTaskData=[];
          this.refreshData=true;
          this.deletedTasksData=[];
        }else if(detail.data=='cancel'){
          this.refreshData=false;
        }
       
      });
      await popover.present();
    } else {
      let message;
      if(this.sg['userdata'].primary_language === 'en'){
        message  = 'Select the tasks to Update';
       } else if(this.sg['userdata'].primary_language === 'te'){
        message  ='నవీకరించడానికి పనులను ఎంచుకోండి'
       } else if(this.sg['userdata'].primary_language === 'ta'){
        message  = 'புதுப்பிக்க பணிகளைத் தேர்ந்தெடுக்கவும்'
       } else if(this.sg['userdata'].primary_language === 'kn'){
        message  = 'ನವೀಕರಿಸಲು ಕಾರ್ಯಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ';
       } else if(this.sg['userdata'].primary_language === 'hi'){
        message  ='अपडेट करने के लिए कार्यों का चयन करें'
       }
      const alert = await this.alertController.create({
        header: 'Alert',
        message: message,
        buttons: ['OK']
      });
      await alert.present();
    }
  }

  }
  taskUpdate(data){
    console.log(data)
  }
 async loadData(event) {
     const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message:this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    await loading.present();
    this.previousLen = this.count;
    this.count = this.count+10;
    if(this.tasksData.length<=this.count){
      this.count = this.tasksData.length;
    }
    if(this.previousLen<this.count){
      for(let i=this.previousLen ; i< this.count;i++){
      this.displayTaskData.push(this.tasksData[i]);
      }
    }
    // console.log(this.displayTaskData);
     setTimeout(() => {
        loading.dismiss();
      }, 1000);
    setTimeout(() => {
      console.log('Done');
      event.target.complete();
      }, 500);
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};