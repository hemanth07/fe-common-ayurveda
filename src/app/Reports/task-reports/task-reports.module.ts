import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TaskReportsPage } from './task-reports.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { HarvestcomponentsModule} from '../../components/harvestcomponents/harvestcomponents.module';
import { CalendercomponentComponent} from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import { PopupTaskPlannerDeleteComponent } from '../../components/task-manger-components/popup-task-planner-delete/popup-task-planner-delete.component'
import { PopupTaskPlannerUpdateComponent } from '../../components/task-manger-components/popup-task-planner-update/popup-task-planner-update.component'
import { PopupTaskPlannerFilterComponent } from '../../components/task-manger-components/popup-task-planner-filter/popup-task-planner-filter.component'

const routes: Routes = [
  {
    path: '',
    component: TaskReportsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
     HarvestcomponentsModule,
    ObservationComponentsModule,
    TaskManagerComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TaskReportsPage],
  entryComponents: [CalendercomponentComponent,PopupTaskPlannerDeleteComponent,PopupTaskPlannerUpdateComponent,PopupTaskPlannerFilterComponent]
})
export class TaskReportsPageModule {}
