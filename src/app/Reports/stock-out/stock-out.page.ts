import { Router, Route } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { PopoverController, AlertController, LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { OtherService } from '../../languages/others.service';

@Component({
  selector: 'app-stock-out',
  templateUrl: './stock-out.page.html',
  styleUrls: ['./stock-out.page.scss'],
})
export class StockOutPage implements OnInit {
  headerData: any = [];
  footerData:any = [];
  types: any = [];
  type_label: string;
  fromdate_label: string;
  todate_label: string;
  type:string;
  from:string;
  to:string;
  location:any;
  index:any;
  completeStockOutReports:any = [];
  stockOutReports:any = [];
  stockOutReportsFilter:any = [];
  stockOutCategoryReports:any = [];
  keySet:any = [];
  viewStockOut:boolean = false;
  constructor(private sg: SimpleGlobal,
    public router: Router,
    private navCtrl:NavController,
    private translate: FarmeasyTranslate,
    private popoverController: PopoverController,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private toastController:ToastController,
    private platform:Platform,
    private socialSharing: SocialSharing,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService,
    private other: OtherService) { }

  ngOnInit() {
    this.createHeaderFooter();
    this.loadHarvestLovs();
    this.getStockOutReports();

  }
  createHeaderFooter() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Stock Out';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'స్టాక్ అవుట్';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'தீர்ந்துவிட்டது';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಸ್ಟಾಕ್ .ಟ್';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'स्टॉक से बाहर';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
    let data;
    if(this.sg['userdata'].primary_language === 'en'){
      data = this.en.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data = this.te.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data = this.ta.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data = this.kn.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data = this.hi.getHarvestReportLabels();
    }else {
       data = this.en.getHarvestReportLabels();
    }
      this.type_label = data[0];
      this.fromdate_label = data[1];
      this.todate_label = data[2];
  }
  loadHarvestLovs(){
    firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/harvest").on('value',resp =>{
      this.types = snapshotToArray(resp);
    });
  }
  categoryChange(){
    if(this.type === 'All'){
      this.stockOutCategoryReports = this.stockOutReportsFilter;
     } else {
       for(let category of this.stockOutReportsFilter){
         if(this.type == category.category){
         this.stockOutCategoryReports = [category];
         }
       }
     }
  }
  dateConverter(date){
    if(date){
      let d = new Date(date);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  }
  async toast() {

    let check = false;
    for(let item of this.stockOutCategoryReports){
      if(item.sub_category.length>0){
        check = true;
      }
    }
    if(check){
      this.other.getReports(this.stockOutCategoryReports).then(resp =>{
        let that = this;
          console.log(resp);
          let filename ="Report Stock Out "+this.from+" to "+this.to+'-'+this.type+'.xlsx';
          this.other.createExcelandOpen(filename,resp['_body']);
          console.log(resp);
        });
    }else{
      const alert = await this.alertController.create({
        header: 'Alert',
        message: 'No content to Download',
        buttons: ['OK']
      });
      return await alert.present();
    }

  
  }
 shareVia() {
  if(this.platform.is('ios')) {
    this.socialSharing.share('Stock Out Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
    .then(()=>{
       // alert('Done');
    }).catch((err)=>{
      alert('error :'+JSON.stringify(err));
    });
  } else {
    this.socialSharing.share('Stock Out Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
    .then(()=>{
   //     alert('Done');
    }).catch((err)=>{
      alert('error :'+JSON.stringify(err));
    });
  }
 
  }

  async pickDate(location,idx) {
    this.viewStockOut = false;
    this.location = location;
    this.stockOutReports = this.completeStockOutReports;
    this.index = idx;
        const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            componentProps: {'previous': true},
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
           if (this.location === 'from'){
              this.from = data.data;
            } else if (this.location === 'to') {
              this.to = data.data;
            }
        });
        await popover.present();
    }
 async viewDetailedReports(){
      if(!this.type || !this.from || !this.to || typeof this.from == "undefined" || typeof this.to == "undefined" || typeof this.type == 'undefined'){
        const alert = await this.alertController.create({
          header: 'Alert',
          message: 'Please Fill Proper Data',
          buttons: ['OK']
        });
        return await alert.present();
      } else {
        this.viewStockOut = true;
        for(let i=0;i<this.stockOutReports.length;i++){
          this.stockOutReportsFilter[i].sub_category = this.stockOutReports[i].sub_category.filter(item => {
            if(item.date>= new Date(this.from).getTime() && item.date<= new Date(this.to).getTime()){
              return true;
            }
          })
        }
        if(this.type === 'All'){
          this.stockOutCategoryReports = this.stockOutReportsFilter;
         } else {
           for(let category of this.stockOutReportsFilter){
             if(this.type == category.category){
             this.stockOutCategoryReports = [category];
             }
           }
         }
      }
     }
   async getStockOutReports(){
      const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/harvest').once('value', response =>{
        this.stockOutReports = [];
        this.stockOutReportsFilter = [];
        let lovs = snapshotToArray(response);
        for(let category of lovs){
           let temp = {category:category.cat_name,sub_category:[]}
           this.stockOutReports.push(temp);
           this.stockOutReportsFilter.push(temp);
           let subCategory=[];
           for(let sub_cat of category.subcat_names){
             subCategory.push(sub_cat.name);
           }
           this.keySet.push({category:category.cat_name,subcategory:subCategory,dates:[]});
        }
       firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/stockOut').once('value', resp =>{
         let that = this;
         let Data = snapshotToArray(resp);
         for(let stock of Data){
           let keys  = Object.keys(stock);
           for(let key of keys){
             if(key !== 'key'){
               for(let i=0;i<that.stockOutReports.length;i++){
                 if(this.stockOutReports[i].category === key){
                   Object.keys(stock[key]).forEach(function(obj_key) {
                     that.stockOutReports[i].sub_category.push(stock[key][obj_key]);
                     that.keySet[i].dates.push(stock[key][obj_key]['date']);
                 });
                 }
               }
             }
           }
         }
         this.mergeSameSubCategory();
         this.completeStockOutReports = JSON.parse(JSON.stringify( this.stockOutReports ));
         console.log(this.completeStockOutReports);
       });
      loading.dismiss();
     });
     }
     mergeSameSubCategory(){
       for(let i=0;i<this.stockOutReports.length;i++){
         let sub_category = this.stockOutReports[i].sub_category;
         let sub_category_keys = this.keySet[i].subcategory;
         let dates = Array.from(new Set(this.keySet[i].dates));
         this.keySet[i].dates = dates;
         let temp_sub_category = sub_category_keys.map(key => {
          return {...dates.map(date => {
             let sub_cat = sub_category.filter(subcat => subcat.subcategory == key && subcat.date == date);
             if(sub_cat.length>1){
               let quantity = 0;
               let uom;
               for(let item of sub_cat){
                 uom = item.UOM;
                 quantity += item.Quantity;
               }
               return ({subcategory:key,date:date,UOM:uom,Quantity:quantity})
             } else if(sub_cat.length == 1){
               return sub_cat[0];
             }
           })}
         });
         let sub_cartegory_merge = this.removeUndefinedValues(temp_sub_category);
         if(sub_cartegory_merge.length>0){
           this.stockOutReports[i].sub_category = sub_cartegory_merge;
         }
       }
     }
     removeUndefinedValues(data){
      let sub_cat_array = [];
      for(let item of data){
        Object.keys(item).forEach((key)=>{
          if(item[key]){
            sub_cat_array.push(item[key]);
          }
        });
      }
      return sub_cat_array;
    }






}


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};