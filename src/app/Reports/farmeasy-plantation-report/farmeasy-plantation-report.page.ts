import { Component, OnInit, ViewChild } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { PopoverController } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';

@Component({
  selector: 'app-farmeasy-plantation-report',
  templateUrl: './farmeasy-plantation-report.page.html',
  styleUrls: ['./farmeasy-plantation-report.page.scss'],
})
export class FarmeasyPlantationReportPage implements OnInit {
  headerData: any = [];
  type: string;
  fromdate: string;
  todate: string;
  date: string;
  harvest_report: any = [];
  report_length: number;
  location: string;
  index: number;
  types: any = [];
  @ViewChild('datePicker') datePicker;
  constructor(private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private popoverController: PopoverController,
    private en: EnglishService,
    private ta: TamilService,
    private hi: HindiService,
    private te: TeluguService,
    private kn: KanadaService ) { }

  ngOnInit() {
    this.createHeader();
    // this.date = new Date().toJSON().split('T')[0];
    this.harvest_report.push({'type':'','from':this.date,'to':this.date});
    this.report_length = this.harvest_report.length-1;
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'PLANTATION REPORT';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'ప్లాంటేషన్ రిపోర్ట్';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'தாவரத் தகவல் அறிக்கை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಸಸ್ಯ ವರದಿ';
    }  else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'प्लांटेशन रिपोर्ट';
    }
    this.types = ['vegetables','crops','pulses','dairy','oil','all'];
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
      let data;
    if(this.sg['userdata'].primary_language === 'en'){
      data = this.en.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data = this.te.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data = this.ta.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data = this.kn.getHarvestReportLabels();
    }else if(this.sg['userdata'].primary_language === 'hi'){
      data = this.hi.getHarvestReportLabels();
    }  else {
       data = this.en.getHarvestReportLabels();
    }
      this.type = data[0];
      this.fromdate = data[1];
      this.todate = data[2];
   }
  async pickDate(location,idx) {
    this.location = location;
    this.index = idx;
        const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
         //   console.log(data);
           if (this.location === 'from'){
              this.harvest_report[this.index]['from'] = data.data;
            } else if (this.location === 'to') {
              this.harvest_report[this.index]['to'] = data.data;
            }
        });
        await popover.present();
    }
  
  addMore(){
    this.harvest_report.push({'type':'','from':this.date,'to':this.date});
    this.report_length = this.harvest_report.length-1;
  }
  remove(idx){
    this.harvest_report.splice(idx,1);
    this.report_length = this.harvest_report.length-1;
  }
}
