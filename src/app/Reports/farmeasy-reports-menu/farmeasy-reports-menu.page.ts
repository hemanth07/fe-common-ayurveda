import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Router, Route } from '@angular/router';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-reports-menu',
  templateUrl: './farmeasy-reports-menu.page.html',
  styleUrls: ['./farmeasy-reports-menu.page.scss'],
})
export class FarmeasyReportsMenuPage implements OnInit {
  headerData: any = [];
  menu: any = [];
  constructor(private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private navCtrl: NavController,
    public router: Router,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService
    ) { }

  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Reports';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'నివేదికలు';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'அறிக்கைகள்';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ವರದಿಗಳು';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'रिपोर्ट';
    } 
    this.headerData = {
        color: 'blue',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        };
  }
  translation() {
    if(this.sg['userdata'].primary_language === 'en'){
      this.menu  = this.en.getReportsMenu();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.menu  = this.te.getReportsMenu();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.menu  = this.ta.getReportsMenu();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.menu  = this.kn.getReportsMenu();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.menu  = this.hi.getReportsMenu();
    }  else {
       this.menu  = this.en.getReportsMenu();
    }
  }
  selectedTask(data){
    this.navCtrl.navigateForward([data.action]);
    // this.router.navigate([data.action], { preserveFragment: true });
  }
}
