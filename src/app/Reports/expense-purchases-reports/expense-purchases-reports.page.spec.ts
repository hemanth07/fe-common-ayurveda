import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensePurchasesReportsPage } from './expense-purchases-reports.page';

describe('ExpensePurchasesReportsPage', () => {
  let component: ExpensePurchasesReportsPage;
  let fixture: ComponentFixture<ExpensePurchasesReportsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensePurchasesReportsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensePurchasesReportsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
