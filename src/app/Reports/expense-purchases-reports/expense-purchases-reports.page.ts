import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { PopoverController, AlertController, LoadingController, NavController } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';

@Component({
  selector: 'app-expense-purchases-reports',
  templateUrl: './expense-purchases-reports.page.html',
  styleUrls: ['./expense-purchases-reports.page.scss'],
})
export class ExpensePurchasesReportsPage implements OnInit {
  to:any;
  headerData:any;
  options:any;
  expenseData:any = [];
  displayList:any = [];
  salaryList:any = [];
  type:any;
  showList:boolean = false;
  category:any;
  showSalaries:any = [];
  selectedFlag:boolean = true;
  constructor(private popoverController: PopoverController,
    private loadingCtrl: LoadingController,private sg:SimpleGlobal) { }

  ngOnInit() {
    this.createHeader();
  }
  createHeader(){
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Expenses & Purchases Reports';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'ఖర్చులు & కొనుగోళ్ల నివేదికలు';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'செலவுகள் மற்றும் கொள்முதல் அறிக்கைகள்';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿ ವರದಿಗಳು';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'व्यय और खरीद रिपोर्ट';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
      this.options = [
        {label:'Food Expenses',value:"Food Expenses"},
        {label:'Farm Purchases',value:"Farm Purchases"},
        {label:'Repair and Maintanance',value:"Repair and Maintanance"},
        {label:'Vehicle Maintanance',value:"Vehicle Maintanance"},
        {label:'Live Stock Expenses',value:"Live Stock Expenses"},
        {label:'Fuel Expenses',value:"Fuel Expenses"},
        {label:'Statutory Expenses',value:"Statutory Expenses"},
        {label:'Other Expenses',value:"Other Expenses"},
        {label:'Transportation',value:"Transportation"}
      ];
  }
  async pickDate(location) {
     // this.stockOutReports = this.completeStockOutReports;
        const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            componentProps: {'previous': true},
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
           if (location === 'to') {
              this.to = data.data;
              if(this.to && this.showList){
                this.selectedFlag = false;
              }
            }
        });
        await popover.present();
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
        let day = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear().toString().substr(2,2);
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        return day+"-"+months[month]+"-"+year;
      } else {
        return 'DD-Mon-YYYY';
      }
    }
  async viewDetailedReports(){
    console.log(this.to);
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses').orderByChild('date').equalTo(this.to).once('value', resp =>{
      this.expenseData = snapshotToArray(resp);
      console.log(this.expenseData);
      this.filterData();
      loading.dismiss();
    });
  }

  filterData(){
    if(this.expenseData && this.type){
      if(this.type === 'All'){
        this.displayList = this.expenseData;
      } else {
        this.displayList = this.expenseData.filter(record => record.category == this.type);
      }
      this.showList = true;
      if(this.to && this.showList){
        this.selectedFlag = false;
      }
    }
  
  }

}

export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
