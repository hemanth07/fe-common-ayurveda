import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesCollectionsReportsPage } from './sales-collections-reports.page';

describe('SalesCollectionsReportsPage', () => {
  let component: SalesCollectionsReportsPage;
  let fixture: ComponentFixture<SalesCollectionsReportsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCollectionsReportsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCollectionsReportsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
