import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { PopoverController, AlertController, LoadingController, NavController } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';
import { snapshotToArray } from 'src/app/languages/others.service';

@Component({
  selector: 'app-sales-collections-reports',
  templateUrl: './sales-collections-reports.page.html',
  styleUrls: ['./sales-collections-reports.page.scss'],
})
export class SalesCollectionsReportsPage implements OnInit {
  headerData: any = [];
  to:any;
  displayList: any = [];
  showList:boolean = false;
  constructor(private popoverController: PopoverController,
    private loadingCtrl: LoadingController,private sg:SimpleGlobal) { }

  ngOnInit() {
    this.createHeader();
  }
  createHeader(){
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Sales and Collection Report';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'అమ్మకాలు మరియు సేకరణ నివేదిక';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'விற்பனை மற்றும் சேகரிப்பு அறிக்கை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಮಾರಾಟ ಮತ್ತು ಸಂಗ್ರಹ ವರದಿ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'बिक्री और संग्रह रिपोर्ट';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
  }
  async pickDate(location) {
    // this.stockOutReports = this.completeStockOutReports;
       const popover = await this.popoverController.create({
           component: CalendercomponentComponent,
           componentProps: {'previous': true},
           cssClass: 'popoverclass',
       });
       popover.onDidDismiss().then((data) => {
          if (location === 'to') {
             this.to = data.data;
           }
       });
       await popover.present();
   }
   dateConverter(date){
     if(date){
       let d = new Date(date);
       let day = d.getDate();
       let month = d.getMonth();
       let year = d.getFullYear().toString().substr(2,2);
       let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
       return day+"-"+months[month]+"-"+year;
     } else {
       return 'DD-Mon-YYYY';
     }
   }
 async viewDetailedReports(){
  this.displayList = [];
   const loading = await this.loadingCtrl.create({
     spinner: 'bubbles',
     message: 'Please wait...',
     translucent: true,
     cssClass: 'custom-class custom-loading'
   });
   loading.present();
   firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out').orderByChild('Date_of_Transaction').equalTo(this.to).once('value',resp=>{
    let data = snapshotToArray(resp);
    console.log(data);
    let temp
    let amount
    for(let item of data){
      amount = 0;
      for(let record of item.record){
        amount+=record.Rate
      }
      temp = { Bill_no:item.Bill_no,customer_name:item.customer_name,amount:amount,payment_status:item.payment_status,short_name:item.short_name}
      this.displayList.push(temp);
    }
    this.showList = true;
    loading.dismiss();
   });
 }

}
