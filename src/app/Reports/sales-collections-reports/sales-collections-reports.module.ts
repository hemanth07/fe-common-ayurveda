import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SalesCollectionsReportsPage } from './sales-collections-reports.page';

import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';

const routes: Routes = [
  {
    path: '',
    component: SalesCollectionsReportsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ObservationComponentsModule,
    HarvestcomponentsModule
  ],
  declarations: [SalesCollectionsReportsPage]
})
export class SalesCollectionsReportsPageModule {}
