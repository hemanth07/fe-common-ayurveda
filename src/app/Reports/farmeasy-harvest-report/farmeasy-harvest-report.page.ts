import { Router, Route } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { PopoverController, AlertController, LoadingController, NavController } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { ChartSelectEvent } from 'ng2-google-charts';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';

@Component({
  selector: 'app-farmeasy-harvest-report',
  templateUrl: './farmeasy-harvest-report.page.html',
  styleUrls: ['./farmeasy-harvest-report.page.scss'],
})
export class FarmeasyHarvestReportPage implements OnInit {
  headerData: any = [];
  footerData:any = [];
  type: string;
  fromdate: string;
  todate: string;
  report: string;
  compare: string;
  date: string;
  harvest_report: any = [];
  report_length: number;
  location: string;
  index: number;
  types: any = [];
  viewBtn: boolean = true;
  harvest_data: any = [];
  harvest_filter_data: any = [];
  selectedKey: any;
  showGraphs:boolean = true;
  completeStockInReports:any = [];
  stockInReports:any = [];
  stockInReportsFilter:any = [];
  stockInCategoryReports:any = [];
  keySet:any = [];
  public pieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [ ['Stock In', 'count']],
    options: {title: 'Stock In', 'pieSliceText': 'label', 'is3D': true},
  };
  public coloumnChart: GoogleChartInterface = {
    chartType: 'ColumnChart',
    dataTable: [ ['Stock In','count']],
    options: {title: 'Stock In', 'pieSliceText': 'label', legend: 'none', 'is3D': true},
  };
  @ViewChild('chart') chart;
  constructor(private sg: SimpleGlobal,
    public router: Router,
    private navCtrl:NavController,
    private translate: FarmeasyTranslate,
    private popoverController: PopoverController,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService ) { }

  ngOnInit() {
    this.createHeaderFooter();
    this.getHarvestData();
    this.getStockInReports();
    this.loadHarvestLovs();
    //this.date = new Date().toJSON().split('T')[0];
    this.harvest_report.push({'type':'','from':this.date,'to':this.date});
    this.report_length = this.harvest_report.length-1;
  }
  createHeaderFooter() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Stock In';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'లో స్టాక్';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'இல் பங்கு';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಇನ್ ಸ್ಟಾಕ್';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'भंडार में';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.9)',
        'middleButtonName':'VIEW DETAILED REPORT',
        'middleButtonColor':'blue',
        };
    let data;
    if(this.sg['userdata'].primary_language === 'en'){
      data = this.en.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data = this.te.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data = this.ta.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data = this.kn.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data = this.hi.getHarvestReportLabels();
    }else {
       data = this.en.getHarvestReportLabels();
    }
      this.type = data[0];
      this.fromdate = data[1];
      this.todate = data[2];
      this.report = data[3];
      this.compare = data[4];
      // this.types = ['Vegetables','Crops','Fruits','Greens','Pulses','Dairy','Oil'];
  }
  dateConverter(date){
    if(date){
      let d = new Date(date);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  }
  getHarvestData(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_in').once('value', resp =>{
      this.harvest_data = snapshotToArray(resp);
      for(let i=0;i<this.harvest_data.length;i++){
        const time = new Date(this.harvest_data[i].Date_of_Receiving).getTime();
        this.harvest_data[i]['time'] = time;
      }
     // console.log(this.harvest_data);
    });

  }
  getStockInReports(){
   firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/harvest').once('value', response =>{
     this.stockInReports = [];
     this.stockInReportsFilter = [];
     let lovs = snapshotToArray(response);
     for(let category of lovs){
        let temp = {category:category.cat_name,sub_category:[]}
        this.stockInReports.push(temp);
        this.stockInReportsFilter.push(temp);
        let subCategory=[];
        for(let sub_cat of category.subcat_names){
          subCategory.push(sub_cat.name);
        }
        this.keySet.push({category:category.cat_name,subcategory:subCategory,dates:[]});
     }
    firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/stockIn').once('value', resp =>{
      let that = this;
      let Data = snapshotToArray(resp);
      for(let stock of Data){
        let keys  = Object.keys(stock);
        for(let key of keys){
          if(key !== 'key'){
            for(let i=0;i<that.stockInReports.length;i++){
              if(this.stockInReports[i].category === key){
                Object.keys(stock[key]).forEach(function(obj_key) {
                  that.stockInReports[i].sub_category.push(stock[key][obj_key]);
                  that.keySet[i].dates.push(stock[key][obj_key]['date']);
              });
              }
            }
            console.log(key);
          }
        }
        console.log(keys);
        console.log(stock);
      }
      this.mergeSameSubCategory();
      this.completeStockInReports = JSON.parse(JSON.stringify( this.stockInReports ));
      console.log(this.completeStockInReports);
    });
  });
  }
  mergeSameSubCategory(){
    console.log(this.stockInReports);
    for(let i=0;i<this.stockInReports.length;i++){
      let sub_category = this.stockInReports[i].sub_category;
      let sub_category_keys = this.keySet[i].subcategory;
      let dates = Array.from(new Set(this.keySet[i].dates));
      this.keySet[i].dates = dates;
      let temp_sub_category = sub_category_keys.map(key => {
       return {...dates.map(date => {
          let sub_cat = sub_category.filter(subcat => subcat.subcategory == key && subcat.date == date);
          if(sub_cat.length>1){
            let quantity = 0;
            let uom;
            for(let item of sub_cat){
              uom = item.UOM;
              quantity += item.Quantity;
            }
            return ({subcategory:key,date:date,UOM:uom,Quantity:quantity})
          } else if(sub_cat.length == 1){
            return sub_cat[0];
          }
        })}
      });
      let sub_cartegory_merge = this.removeUndefinedValues(temp_sub_category);
      if(sub_cartegory_merge.length>0){
        this.stockInReports[i].sub_category = sub_cartegory_merge;
      }
    }
  }
  removeUndefinedValues(data){
    let sub_cat_array = [];
    for(let item of data){
      if(item[0]){
        sub_cat_array.push(item[0]);
      } else if(item[1]){
        sub_cat_array.push(item[1]);
      }
    }
    return sub_cat_array;
  }

  loadHarvestLovs(){
    firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/harvest").on('value',resp =>{
      this.types = snapshotToArray(resp);
    });
  }
 async pickDate(location,idx) {
  this.viewBtn = true;
  this.showGraphs = true;
  this.location = location;
  this.stockInReports = this.completeStockInReports;
  this.index = idx;
      const popover = await this.popoverController.create({
          component: CalendercomponentComponent,
          componentProps: {'previous': true},
          cssClass: 'popoverclass',
      });
      popover.onDidDismiss().then((data) => {
         // console.log(data);
         if (this.location === 'from'){
            this.harvest_report[this.index]['from'] = data.data;
          } else if (this.location === 'to') {
            this.harvest_report[this.index]['to'] = data.data;
          }
      });
      await popover.present();
  }
  addMore(){
    this.viewBtn = true;
    this.harvest_report.push({'type':'','from':this.date,'to':this.date});
    this.report_length = this.harvest_report.length-1;
  }
  remove(idx){
    this.harvest_report.splice(idx,1);
    this.report_length = this.harvest_report.length-1;
  }
 async getreport(){
    if(this.harvest_report.length !== 1 || typeof this.harvest_report[0].from === 'undefined' || typeof this.harvest_report[0].to === 'undefined'){
     // console.log("fill data");
      const alert = await this.alertController.create({
        header: 'Alert',
        message: 'Please Fill Proper Data',
        buttons: ['OK']
      });
      return await alert.present();
    } else {
      this.selectedKey = this.harvest_report[0].type;
      const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      this.pieChart.dataTable =  [ ['Stock In', 'count']];
      this.viewBtn = false;
      const from = new Date(this.harvest_report[0].from).getTime();
      const to = new Date(this.harvest_report[0].to).getTime();
      const type = this.harvest_report[0].type;
      let data = this.harvest_data.filter(item => {
      if(item.time>=from && item.time<=to)
      {
       return true;
      }});
      let data1 = [];
      for(let record of data){
        for(let item of record.record){
          data1.push({...item,Date_of_Receiving:record.Date_of_Receiving,labourer_name:record.labourer_name,time:record.time,key:record.key});
        }
      }
      this.harvest_filter_data = data1;
     // console.log(data);
      for (let items of this.types) {
        let temp = [];
          temp = data1.filter(item => {if (item.category === items.cat_name) {return true; }});
          this.pieChart.dataTable.push([items.cat_name, temp.length]);
      }

      loading.dismiss();
      this.prepareBarChart(type);

    }
  }
  onSelect(data) {
  // console.log(data);
  if(data.selectedRowValues[0]){
  this.selectedKey = data.selectedRowValues[0];
  }
   let key = data.selectedRowValues[0];
   this.prepareBarChart(key);
  }
  prepareBarChart(key){
    this.coloumnChart.options = {'pieSliceText': 'label', legend: 'none', 'is3D': true};
    this.coloumnChart.dataTable =  [ ['category', 'count']];
    let data = this.harvest_filter_data.filter(item => item.category === key);
    let subcats = [];
    for(let item of data){
      let index = subcats.indexOf(item.subcategory);
      if(index === -1){
        subcats.push(item.subcategory);
      }
    }
   // console.log(subcats);
    for(let items of subcats){
      let temp = [];
      temp = data.filter(item => {if (item.subcategory === items) {return true; }});
      if(temp.length>0){
      this.coloumnChart.dataTable.push([items, temp.length]);
      }
    }
   // console.log(this.coloumnChart);
    this.coloumnChart.component.draw();
    
   }
   viewDetailedReports(){
    this.showGraphs = false;
    for(let i=0;i<this.stockInReports.length;i++){
      this.stockInReportsFilter[i].sub_category = this.stockInReports[i].sub_category.filter(item => {
        if(item.date>= new Date(this.harvest_report[0].from).getTime() && item.date<= new Date(this.harvest_report[0].to).getTime()){
          return true;
        }
      })
    }
    if(this.harvest_report[0].type === 'All'){
      this.stockInCategoryReports = this.stockInReportsFilter;
     } else {
       for(let category of this.stockInReportsFilter){
         if(this.harvest_report[0].type == category.category){
         this.stockInCategoryReports = [category];
         }
       }
     }
    
   }


  getcompare(){
   // console.log('Compare');
   let data = {compare:JSON.stringify(this.harvest_report)}
    this.navCtrl.navigateBack(['/farmeasy-report-compare',data]);
    // this.router.navigate(['/farmeasy-report-compare'], { replaceUrl: true });
  }
  categoryChange(){
    if(this.harvest_report[0].type === 'All'){
      this.stockInCategoryReports = this.stockInReportsFilter;
     } else {
       for(let category of this.stockInReportsFilter){
         if(this.harvest_report[0].type == category.category){
         this.stockInCategoryReports = [category];
         }
       }
     }
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};