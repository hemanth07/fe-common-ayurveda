import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { FarmeasyHarvestReportPage } from './farmeasy-harvest-report.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
const routes: Routes = [
  {
    path: '',
    component: FarmeasyHarvestReportPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    HarvestcomponentsModule,
    Ng2GoogleChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyHarvestReportPage]
})
export class FarmeasyHarvestReportPageModule {}
