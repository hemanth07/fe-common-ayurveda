import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Router, Route } from '@angular/router';
import { KanadaService } from '../../languages/kanada.service';
import { TeluguService } from '../../languages/telugu.service';
import { TamilService } from '../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-stock-inventory-report-menu',
  templateUrl: './stock-inventory-report-menu.page.html',
  styleUrls: ['./stock-inventory-report-menu.page.scss'],
})
export class StockInventoryReportMenuPage implements OnInit {
  headerData: any = [];
  menu: any = [];    
  constructor(private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private navCtrl: NavController,
    public router: Router,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService
    ) { }

  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Stock Inventory Report';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'స్టాక్ ఇన్వెంటరీ రిపోర్ట్';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'பங்கு சரக்கு அறிக்கை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಸ್ಟಾಕ್ ಇನ್ವೆಂಟರಿ ವರದಿ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'स्टॉक इन्वेंटरी रिपोर्ट';
    } 
    this.headerData = {
        color: 'blue',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        };
  }
  translation() {
    if(this.sg['userdata'].primary_language === 'en'){
      this.menu  = this.en.getStockInventoryMenu();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.menu  = this.te.getStockInventoryMenu();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.menu  = this.ta.getStockInventoryMenu();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.menu  = this.kn.getStockInventoryMenu();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.menu  = this.hi.getStockInventoryMenu();
    }  else {
       this.menu  = this.en.getStockInventoryMenu();
    }
  }
  selectedTask(data){
    this.navCtrl.navigateForward([data.action]);
    // this.router.navigate([data.action], { preserveFragment: true });
  }
}
