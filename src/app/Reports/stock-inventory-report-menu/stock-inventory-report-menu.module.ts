import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StockInventoryReportMenuPage } from './stock-inventory-report-menu.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: StockInventoryReportMenuPage
  }
];

@NgModule({
  imports: [
    ObservationComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StockInventoryReportMenuPage]
})
export class StockInventoryReportMenuPageModule {}
