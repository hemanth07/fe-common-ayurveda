import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { ChartSelectEvent } from 'ng2-google-charts';
import * as firebase from 'firebase';
import { PopoverController, AlertController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-farmeasy-report-compare',
  templateUrl: './farmeasy-report-compare.page.html',
  styleUrls: ['./farmeasy-report-compare.page.scss'],
})
export class FarmeasyReportComparePage implements OnInit {
  headerData: any;
  category: string = 'ALL';
  harvest_data: any = [];
  harvest_filter_data: any = [];
  harvest_report: any = [];
  report_length: number;
  types: any = ['SR','SW','D','IC','ALL'];
  type:any;
  categories: any = [];
  harvest_categories:any;
  selected_harvest_type:any;
  showSubFlag:boolean = false;
  public pieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [ ['Harvest Types', 'count']],
    options: {title: 'Harvest Types', 'pieSliceText': 'label', 'is3D': true},
  };
  public coloumnChart: GoogleChartInterface = {
    chartType: 'ColumnChart',
    dataTable: [ ['Harvest Types','count']],
    options: {title: 'Harvest Types', 'pieSliceText': 'label', legend: 'none', 'is3D': true},
  };
  
  @ViewChild('chart') chart;
  constructor(private sg: SimpleGlobal,
    private popoverController: PopoverController,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private route:ActivatedRoute) { }

  ngOnInit() {
    this.createHeader();
    this.getHarvestData();
    this.route.params.subscribe(params => {
      this.harvest_categories = JSON.parse(params.compare);
      console.log(this.harvest_categories);
    });
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'HARVEST REPORT';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'హార్వెస్ట్ నివేదిక';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'அறுவடை அறிக்கை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಹರ್ವೆಸ್ಟ್ ವರದಿ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'सबसे बड़ी रिपोर्ट';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
  }
  
  getHarvestData(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out').once('value', resp =>{
      this.harvest_data = snapshotToArray(resp);
      for(let i=0;i<this.harvest_data.length;i++){
        const time = new Date(this.harvest_data[i].Date_of_Transaction).getTime();
        this.harvest_data[i]['time'] = time;
      }
     this.prepareData(this.harvest_data,this.harvest_categories);
    });
  }

  prepareData(categries,filterData){
    let temp = []
    let temp1 = [];
    for(let category of categries){
        for(let record of category.record){
          temp.push({...record,Bill_no:category.Bill_no,
                      Date_of_Transaction:new Date(category.Date_of_Transaction).getTime(),
                      customer_name: category.customer_name,
                      short_name:category.short_name,
                      type:category.type});
        }
    }
    for(let i=0;i<filterData.length;i++){
      temp1[i] = temp.filter((record)=>{
        if(record.Date_of_Transaction >= new Date(filterData[i].from) && record.Date_of_Transaction <= new Date(filterData[i].to)){
          if(record.category == filterData[i].type){
            return true;
          }
        }
      })
    }
    console.log(temp);
    console.log(temp1);
    this.harvest_filter_data = temp;
    this.harvest_data = temp1;
    this.getreport('ALL');
  }

  onSelect(data) {
   // console.log(data);
    this.showSubFlag = true;
    let key = data.selectedRowValues[0];
    this.selected_harvest_type = key;
    this.prepareBarChart();
   }
   selectCategory(){
     if(this.type == 'ALL'){
      this.getreport('ALL');
     } else {
      this.getreport(this.type);
     }
   }
   selectCategorySubCat(){
    this.prepareBarChart();
  }
   async getreport(key){
       const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      this.pieChart.dataTable =  [ ['Category', 'count']];
      if(key === 'ALL'){
        for(let i=0 ;i<this.harvest_data.length;i++){
          this.pieChart.dataTable.push([this.harvest_categories[i].type, this.harvest_data[i].length]);
        }
      } else {
        for(let i=0 ;i<this.harvest_data.length;i++){
          let temp = this.harvest_data[i].filter(item=> item.short_name == key);
          this.pieChart.dataTable.push([this.harvest_categories[i].type, temp.length]);
        }
      }
    loading.dismiss();
    this.pieChart.component.draw();
  }
   prepareBarChart(){
    this.coloumnChart.options = {title: this.selected_harvest_type, 'pieSliceText': 'label', legend: 'none', 'is3D': true};
    this.coloumnChart.dataTable =  [ ['category', 'count']];
    let data = this.harvest_filter_data.filter(item => {
      if(item.category == this.selected_harvest_type ) {
        if(this.category === 'ALL'){
          return true;
        } else {
          if(item.short_name == this.category) {
            return true;
            }
          }
        }
      });
    let subcats = [];
    for(let item of data){
      let index = subcats.indexOf(item.subcategory);
      if(index === -1){
        subcats.push(item.subcategory);
      }
    }
    for(let items of subcats){
      let temp = [];
      temp = data.filter(item => {if (item.subcategory === items) {return true; }});
      if(temp.length>0){
      this.coloumnChart.dataTable.push([items, temp.length]);
      }
    }
    this.coloumnChart.component.draw();
   }

}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};