import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WateringReportsPage } from './watering-reports.page';

import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: WateringReportsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WateringReportsPage]
})
export class WateringReportsPageModule {}
