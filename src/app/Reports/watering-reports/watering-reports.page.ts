import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import {SimpleGlobal} from 'ng2-simple-global';
import { snapshotToArray } from 'src/app/components/harvestcomponents/print-popup/print-popup.component';
import { PopoverController, AlertController, LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-watering-reports',
  templateUrl: './watering-reports.page.html',
  styleUrls: ['./watering-reports.page.scss'],
})
export class WateringReportsPage implements OnInit {
  headerData:any
  sources:any = [];
  constructor(private sg:SimpleGlobal,
    private popoverController: PopoverController,
    private loadingCtrl: LoadingController) {

     }

  ngOnInit() {
    this.createHeader();
  }
  createHeader(){
    let title;
    if(this.sg['page'] == 'watering-reports'){
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Watering Reports';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'నీరు త్రాగుట నివేదికలు';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'நீர்ப்பாசன அறிக்கைகள்';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ನೀರಿನ ವರದಿಗಳು';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'पानी की रिपोर्ट';
      } 
    } else {
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Fertigation Report';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'ఫెర్టిగేషన్ రిపోర్ట్';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'கருத்தரித்தல் அறிக்கை';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಲೀಕರಣ ವರದಿ';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'फर्टिगेशन रिपोर्ट';
      }
    }
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
    }
   async ionViewDidEnter(){
      const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      if(this.sg['page'] == 'watering-reports'){
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/sources').orderByChild('id').equalTo("1").once('value',resp =>{
          this.sources = snapshotToArray(resp)[0];
          console.log(this.sources);
          loading.dismiss();
        });
      } else {
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/sources').orderByChild('id').equalTo("3").once('value',resp =>{
          this.sources = snapshotToArray(resp)[0];
          console.log(this.sources);
          loading.dismiss();
        });
      }

     
    }
}
