import { Router, Route } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { PopoverController, AlertController, LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { OtherService } from 'src/app/languages/others.service';
@Component({
  selector: 'app-stock-in-hand',
  templateUrl: './stock-in-hand.page.html',
  styleUrls: ['./stock-in-hand.page.scss'],
})
export class StockInHandPage implements OnInit {
  headerData: any = [];
  footerData:any = [];
  types: any = [];
  type_label: string;
  fromdate_label: string;
  todate_label: string;
  type:string;
  from:string;
  to:string;
  location:any;
  completeStockOutReports:any = [];
  stockOutReports:any = [];
  stockOutReportsFilter:any = [];
  stockOutCategoryReports:any = [];
  keySet:any = [];
  viewStockOut:boolean = false;
  completeStockInReports:any = [];
  stockInReports:any = [];
  stockInReportsFilter:any = [];
  stockInCategoryReports:any = [];
  stockOnHandReports:any = [];
  lovs:any = [];
  stockOnHand:any = []
  constructor(private sg: SimpleGlobal,
    public router: Router,
    private navCtrl:NavController,
    private translate: FarmeasyTranslate,
    private popoverController: PopoverController,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private toastController:ToastController,
    private platform:Platform,
    private socialSharing: SocialSharing,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private other:OtherService,
    private kn:KanadaService) { }
  ngOnInit() {
    this.createHeaderFooter();
    this.loadHarvestLovs();
    this.getStockReports();
  }
  createHeaderFooter() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Stock On Hand';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'చేతిలో స్టాక్';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'கையில் பங்கு';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಕೈಯಲ್ಲಿ ಸ್ಟಾಕ್';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'हाथ में स्टॉक';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
    let data;
    if(this.sg['userdata'].primary_language === 'en'){
      data = this.en.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data = this.te.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data = this.ta.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data = this.kn.getHarvestReportLabels();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data = this.hi.getHarvestReportLabels();
    }else {
       data = this.en.getHarvestReportLabels();
    }
      this.type_label = data[0];
      this.fromdate_label = data[1];
      this.todate_label = data[2];
  }
  loadHarvestLovs(){
    firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/harvest").on('value',resp =>{
      this.types = snapshotToArray(resp);
    });
  }
  async pickDate(location) {
    this.viewStockOut = false;
    this.location = location;
     // this.stockOutReports = this.completeStockOutReports;
        const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            componentProps: {'previous': true},
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
           if (this.location === 'from'){
              this.from = data.data;
            } else if (this.location === 'to') {
              this.to = data.data;
              
            }
        });
        await popover.present();
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
        let day = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear().toString().substr(2,2);
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        return day+"-"+months[month]+"-"+year;
      } else {
        return 'DD-Mon-YYYY';
      }
    }
    async toast() {
      // const toast = await this.toastController.create({
      //   message: 'Coming Soon...',
      //   duration: 2000,
      //   position : "middle",
      //   cssClass:"my-custom-class"
      // });
      // toast.present();
      let check = false;
      for(let item of this.stockOnHandReports){
        if(item.subcategory.length>0){
          check = true;
        }
      }
      if(check){
   
        let stockOnHandReports = this.stockOnHandReports.map(x => ({...x}));
        let payload  = [];
        let payloaditem = { category : "", sub_category:[]};
        let subcategory_item;
        for(let record of stockOnHandReports){
          payloaditem['category'] = record.category;
          payloaditem['sub_category'] = [];
          subcategory_item = [];
            for(let item of record.subcategory){
              subcategory_item.push({date:this.to,...item,stock_on_hand_quantity:item.stock_in_quantity-item.stock_out_quantity});
            }
          payloaditem['sub_category'] = subcategory_item;
          payload.push({...payloaditem});
        }
        this.other.getReports(payload).then(resp =>{
            let that = this;
            console.log(resp);
            let filename ="Report Stock On Hand "+this.to+'-'+this.type+'.xlsx';
            this.other.createExcelandOpen(filename,resp['_body']);
            // console.log(resp);
          });
      }else{
        const alert = await this.alertController.create({
          header: 'Alert',
          message: 'No content to Download',
          buttons: ['OK']
        });
        return await alert.present();
      }
  
      

   }
   shareVia() {
    if(this.platform.is('ios')) {
      this.socialSharing.share('Stock On Hand Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
      .then(()=>{
         // alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    } else {
      this.socialSharing.share('Stock On Hand Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
      .then(()=>{
     //     alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    }
   
    }
   async getStockReports(){
      const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/harvest').once('value', response =>{
        this.stockOutReports = [];
        this.stockOutReportsFilter = [];
        let lovs = snapshotToArray(response);
        this.lovs = snapshotToArray(response);
        for(let category of lovs){
           let temp = JSON.parse(JSON.stringify({category:category.cat_name,sub_category:[]}));
           this.stockOutReports.push({...temp});
           this.stockOutReportsFilter.push({...temp});
           this.stockInReports.push({...temp});
           this.stockInReportsFilter.push({...temp});
           let subCategory=[];
           let subCategoryObject = [];
           for(let sub_cat of category.subcat_names){
             subCategory.push(sub_cat.name);
           }
          for(let sub_cat of category.subcat_names){
            subCategoryObject.push({name:sub_cat.name,stock_in_quantity:0,stock_out_quantity:0});
          }
           this.keySet.push({category:category.cat_name,subcategory:subCategory,dates:[]});
           this.stockOnHandReports.push({category:category.cat_name,subcategory:subCategoryObject});
        }
        firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/stockIn').once('value', resp =>{
          let that = this;
          let Data = snapshotToArray(resp);
          // console.log(Data);
          let keySet = that.keySet;
          for(let stock of Data){
            let keys  = Object.keys(stock);
            for(let key of keys){
              if(key !== 'key'){
                for(let i=0;i<that.stockInReports.length;i++){
                  if(this.stockInReports[i].category === key){
                    Object.keys(stock[key]).forEach(function(obj_key) {
                      that.stockInReports[i].sub_category.push({...stock[key][obj_key],'type':'In'});
                      keySet[i].dates.push(stock[key][obj_key]['date']);
                  });
                  }
                }
              }
            }
          }
          let temp_stockInReports = this.stockInReports.map(x => ({...x}));
          this.stockInReports.length = 0;
          for(let report of temp_stockInReports){
            report.sub_category = report.sub_category.filter(item => item.type == 'In');
            this.stockInReports.push(report);
          }
          // console.log(this.stockInReports);
          this.mergeStockInSameSubCategory(this.stockInReports,keySet);
          this.completeStockInReports = JSON.parse(JSON.stringify( this.stockInReports ));
          console.log(this.completeStockInReports);
        });
       firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/stockOut').once('value', resp =>{
         let that = this;
         let Data_out = snapshotToArray(resp);
        //  console.log(Data_out);
         let keySet = that.keySet;
         for(let stock_out of Data_out){
           let keys_out  = Object.keys(stock_out);
           for(let key_out of keys_out){
             if(key_out !== 'key'){
               for(let i=0;i<that.stockOutReports.length;i++){
                 if(this.stockOutReports[i].category === key_out){
                   Object.keys(stock_out[key_out]).forEach(function(obj_key) {
                     that.stockOutReports[i].sub_category.push({...stock_out[key_out][obj_key],'type':'Out'});
                     keySet[i].dates.push(stock_out[key_out][obj_key]['date']);
                 });
                 }
               }
             }
           }
         }
         let temp_stockOutReports = this.stockOutReports.map(x => ({...x}));
         this.stockOutReports.length = 0;
         for(let report of temp_stockOutReports){
           report.sub_category = report.sub_category.filter(item => item.type == 'Out');
           this.stockOutReports.push(report);
         }
        //  console.log(this.stockOutReports);
         this.mergeStockOutSameSubCategory(this.stockOutReports,keySet);
         this.completeStockOutReports = JSON.parse(JSON.stringify(this.stockOutReports));
         console.log(this.completeStockOutReports);
       });

      loading.dismiss();
     });
     }
     mergeStockOutSameSubCategory(stockOutReports,keySet){
      let sub_category_out
      let sub_category_keys_out
      let dates_out
      let temp_sub_category_out
      let sub_cartegory_merge_out
      let sub_cat_out
      let quantity_out = 0;
      let uom_out
       for(let i=0;i<stockOutReports.length;i++){
         sub_category_out = stockOutReports[i].sub_category;
         sub_category_keys_out = keySet[i].subcategory;
         dates_out = Array.from(new Set(keySet[i].dates));
        //  this.keySet[i].dates = dates_out;
         temp_sub_category_out = sub_category_keys_out.map(key_out => {
          return {...dates_out.map(date_out => {
             sub_cat_out = sub_category_out.filter(subcat_out => subcat_out.subcategory == key_out && subcat_out.date == date_out);
             if(sub_cat_out.length>1){
               quantity_out = 0;
               for(let item_out of sub_cat_out){
                 uom_out = item_out.UOM;
                 quantity_out += item_out.Quantity;
               }
               return ({subcategory:key_out,date:date_out,UOM:uom_out,Quantity:quantity_out,type:'Out'})
             } else if(sub_cat_out.length == 1){
               return sub_cat_out[0];
             }
           })}
         });
         sub_cartegory_merge_out = this.removeUndefinedValues(temp_sub_category_out);
         if(sub_cartegory_merge_out.length>0){
           this.stockOutReports[i].sub_category = sub_cartegory_merge_out;
         }
       }
     }
     mergeStockInSameSubCategory(stockInReports,keySet){
      let sub_category_in 
      let sub_category_keys_in
      let dates_in
      let temp_sub_category_in
      let sub_cartegory_merge_in
      let sub_cat_in
      let quantity_in = 0;
      let uom_in
       for(let i=0;i<stockInReports.length;i++){
         sub_category_in= stockInReports[i].sub_category;
         sub_category_keys_in = keySet[i].subcategory;
         dates_in = Array.from(new Set(keySet[i].dates));
        //  this.keySet[i].dates = dates_in;
         temp_sub_category_in = sub_category_keys_in.map(key_in => {
          return {...dates_in.map(date => {
             sub_cat_in = sub_category_in.filter(subcat_in => subcat_in.subcategory == key_in && subcat_in.date == date);
            //  console.log(sub_cat_in);
             if(sub_cat_in.length>1){
               quantity_in = 0;
               for(let item_in of sub_cat_in){
                 uom_in = item_in.UOM;
                 quantity_in += item_in.Quantity;
               }
               return ({subcategory:key_in,date:date,UOM:uom_in,Quantity:quantity_in,type:'In'})
             } else if(sub_cat_in.length == 1){
               return sub_cat_in[0];
             }
           })}
         });
         sub_cartegory_merge_in = this.removeUndefinedValues(temp_sub_category_in);
         if(sub_cartegory_merge_in.length>0){
          //  console.log(this.stockInReports[i]);
           this.stockInReports[i].sub_category = sub_cartegory_merge_in;
         }
       }
     }
     removeUndefinedValues(data){
      let sub_cat_array = [];
      for(let item of data){
        Object.keys(item).forEach((key)=>{
          if(item[key]){
            sub_cat_array.push(item[key]);
          }
        });
      }
      return sub_cat_array;
    }
    async viewDetailedReports(){
      if(!this.type || !this.to || typeof this.to == 'undefined' || typeof this.type == 'undefined'){
        const alert = await this.alertController.create({
          header: 'Alert',
          message: 'Please Fill Proper Data',
          buttons: ['OK']
        });
        return await alert.present();
      } else {
      this.viewStockOut = true;
      for(let cat=0;cat<this.stockOnHandReports.length;cat++){
        this.stockOutReportsFilter[cat].sub_category = this.stockOutReports[cat]['sub_category'].filter(item => item.date == new Date(this.to).getTime());
        this.stockInReportsFilter[cat].sub_category = this.stockInReports[cat]['sub_category'].filter(item => item.date == new Date(this.to).getTime()); 
      }
        for(let i=0;i<this.stockOnHandReports.length;i++){
          for(let j=0;j<this.stockOnHandReports[i].subcategory.length;j++){
            for(let insub of this.stockInReportsFilter[i].sub_category){

              if(this.stockOnHandReports[i].subcategory[j].name == insub.subcategory){
                this.stockOnHandReports[i].subcategory[j]['stock_in_quantity'] = insub.Quantity;
                this.stockOnHandReports[i].subcategory[j]['stock_in_uom'] = insub.UOM;
              }
            }
            for(let outsub of this.stockOutReportsFilter[i].sub_category){
              if(this.stockOnHandReports[i].subcategory[j].name == outsub.subcategory){
                this.stockOnHandReports[i].subcategory[j]['stock_out_quantity'] = outsub.Quantity;
                this.stockOnHandReports[i].subcategory[j]['stock_out_uom'] = outsub.UOM;
              }
            }
          }
        }
        for(let i=0;i<this.stockOnHandReports.length;i++){
          let subcat = this.stockOnHandReports[i].subcategory.filter( item => !(item.stock_in_quantity == 0 && item.stock_out_quantity == 0));
          this.stockOnHandReports[i].subcategory = subcat;
        }
        console.log(this.stockOnHandReports);
        if(this.type === 'All'){
          this.stockOnHand = this.stockOnHandReports;
         } else {
           for(let category of this.stockOnHandReports){
             if(this.type == category.category){
             this.stockOnHand = [category];
             }
           }
         }
        }
       }

       calaculateStockOnHand(stockin,stockout){
         let onHand = parseInt(stockin)-parseInt(stockout);
        return onHand;
       }

       categoryChange(){
        if(this.type === 'All'){
          this.stockOnHand = this.stockOnHandReports;
         } else {
           for(let category of this.stockOnHandReports){
             if(this.type == category.category){
             this.stockOnHand = [category];
             }
           }
         }
      }




}


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
