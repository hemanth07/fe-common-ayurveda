import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InflowOutflowReportsPage } from './inflow-outflow-reports.page';

import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
const routes: Routes = [
  {
    path: '',
    component: InflowOutflowReportsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    HarvestcomponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InflowOutflowReportsPage]
})
export class InflowOutflowReportsPageModule {}
