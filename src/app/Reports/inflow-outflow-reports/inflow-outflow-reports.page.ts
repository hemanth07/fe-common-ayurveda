import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { PopoverController, AlertController, LoadingController, NavController } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import * as firebase from 'firebase';
import { snapshotToArray } from 'src/app/languages/others.service';

@Component({
  selector: 'app-inflow-outflow-reports',
  templateUrl: './inflow-outflow-reports.page.html',
  styleUrls: ['./inflow-outflow-reports.page.scss'],
})
export class InflowOutflowReportsPage implements OnInit {
  headerData: any = [];
  to:any;
  displayList: any = [];
  flowData:any = [];
  type:any;
  showList:boolean = true;
  selectedFlag:boolean = true;
  constructor(private popoverController: PopoverController,
    private loadingCtrl: LoadingController,private sg:SimpleGlobal) { }

  ngOnInit() {

    this.createHeader();
  }
  createHeader(){
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Inflow & Outflow Report';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'ఇన్‌ఫ్లో & అవుట్ ఫ్లో రిపోర్ట్';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'வரத்து மற்றும் வெளிச்செல்லும் அறிக்கை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಒಳಹರಿವು ಮತ್ತು ಹೊರಹರಿವಿನ ವರದಿ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'इनफ्लो एंड आउटफ्लो रिपोर्ट';
    } 
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
  }
  async pickDate(location) {
     // this.stockOutReports = this.completeStockOutReports;
        const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            componentProps: {'previous': true},
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
           if (location === 'to') {
              this.to = data.data;
              if(this.to && this.showList){
                this.selectedFlag = false;
              }
            }
        });
        await popover.present();
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
        let day = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear().toString().substr(2,2);
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        return day+"-"+months[month]+"-"+year;
      } else {
        return 'DD-Mon-YYYY';
      }
    }
  async viewDetailedReports(){
    let count = 0
    this.flowData = [];
    console.log(this.to);
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses').orderByChild('date').equalTo(this.to).once('value', resp =>{
      let data = snapshotToArray(resp);
      for(let item of data){
        this.flowData.push({date:item.date,type:'OutFlow',category:item.category,amount:item.amount});
      }
      count++;
      if(count == 5){
        this.filterData();
        loading.dismiss();
      }
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/purchase_in').orderByChild('Date_of_transaction').equalTo(this.to).once('value', resp =>{
      let data = snapshotToArray(resp);
      for(let item of data){
        this.flowData.push({date:item.Date_of_transaction,type:'OutFlow',category:item.category,amount:item.total});
      }
      count++;
      if(count == 5){
        this.filterData();
        loading.dismiss();
      }
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/purchase_out').orderByChild('Date_of_transaction').equalTo(this.to).once('value', resp =>{
      let data = snapshotToArray(resp);
      for(let item of data){
        this.flowData.push({date:item.Date_of_transaction,type:'InFlow',category:item.category,amount:item.total});
      }
      count++;
      if(count == 5){
        this.filterData();
        loading.dismiss();
      }
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out').orderByChild('Date_of_Transaction').equalTo(this.to).once('value', resp =>{
      let data = snapshotToArray(resp).filter(item => item.short_name != 'IC' && item.short_name != 'D');
      let amount;
      for(let item of data){
        amount = 0;
        for(let record of item.record){
          amount+=record.Rate
        }
        this.flowData.push({date:item.Date_of_Transaction,type:'InFlow',category:item.short_name,amount:amount});
      }
      count++;
      if(count == 5){
        this.filterData();
        loading.dismiss();
      }
    });
    //     let d = new Date(this.to);
    //     let day = d.getDate();
    //     let month = d.getMonth();
    //     let year = d.getFullYear().toString().substr(2,2);
    //     let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    //     let key = months[month]+"-"+year;
    // firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/salary/'+key).once('value', resp =>{
    //   let data = snapshotToArray(resp);
    //   for(let item of data){
    //     this.flowData.push({date:item.date,type:'OutFlow',category:'Salaries',amount:item.amount});
    //   }
     
    // });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/salary').once('value', resp =>{
      let data = snapshotToArray(resp);
      let data1 = []
      for(let item of data){
        let keys = Object.keys(item);
        console.log(keys);
        keys.forEach(key =>{
          let temp = item[key];
          if(temp.amount){
            temp['date_string'] = new Date(temp.date).toISOString().split("T")[0];
            data1.push(temp);
          }
        })
      }
      console.log(data);
      let data2 = data1.filter(item => item.date_string == this.to);
      for(let item of data2){
        this.flowData.push({date:item.date,type:'OutFlow',category:'Salaries',amount:item.amount});
      }
      count++;
      if(count == 5){
        this.filterData();
        loading.dismiss();
      }
    });
  }

  filterData(){
    if(this.flowData && this.type){
      if(this.type === 'All'){
        this.displayList = this.flowData;
      } else {
        this.displayList = this.flowData.filter(record => record.type == this.type);
      }
      this.showList = true;
      if(this.to && this.showList){
        this.selectedFlag = false;
      }
    }
  
  }
}
