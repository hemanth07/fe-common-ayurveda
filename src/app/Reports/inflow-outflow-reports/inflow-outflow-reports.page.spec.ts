import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InflowOutflowReportsPage } from './inflow-outflow-reports.page';

describe('InflowOutflowReportsPage', () => {
  let component: InflowOutflowReportsPage;
  let fixture: ComponentFixture<InflowOutflowReportsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InflowOutflowReportsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InflowOutflowReportsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
