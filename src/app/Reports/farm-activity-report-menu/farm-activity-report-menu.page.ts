import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Router, Route } from '@angular/router';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-farm-activity-report-menu',
  templateUrl: './farm-activity-report-menu.page.html',
  styleUrls: ['./farm-activity-report-menu.page.scss'],
})
export class FarmActivityReportMenuPage implements OnInit {
  headerData: any = [];
  menu: any = [];
  constructor(private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private navCtrl: NavController,
    public router: Router,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService
    ) { }

  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Farm Activity Report';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'వ్యవసాయ కార్యాచరణ నివేదిక';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'பண்ணை செயல்பாட்டு அறிக்கை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಕೃಷಿ ಚಟುವಟಿಕೆ ವರದಿ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'फार्म गतिविधि रिपोर्ट';
    } 
    this.headerData = {
        color: 'blue',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        };
  }
  translation() {
    if(this.sg['userdata'].primary_language === 'en'){
      this.menu  = this.en.getFarmActivityMenu();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.menu  = this.te.getFarmActivityMenu();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.menu  = this.ta.getFarmActivityMenu();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.menu  = this.kn.getFarmActivityMenu();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.menu  = this.hi.getFarmActivityMenu();
    }  else {
       this.menu  = this.en.getFarmActivityMenu();
    }
  }
  selectedTask(data){
    this.sg['page'] = data.keyword;
    this.navCtrl.navigateForward([data.action]);
    // this.router.navigate([data.action], { preserveFragment: true });
  }
}
