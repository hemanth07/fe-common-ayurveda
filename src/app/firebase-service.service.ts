import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { OtherService } from './languages/others.service';

@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {

 
  constructor(public other:OtherService) { }

  pushDataIntoFirebase(path, object): any {
    return new Promise((resolve,reject)=>{
      firebase.database().ref(path).push(object).then(data => {
        console.log(data);
        resolve(data.key);
      }).catch(err=>{
        reject(err);
      })
    });
  }

  updateDataInFirebase(path, object): any {
      firebase.database().ref(path).update(object);
  }

  getDataFromFirebase(path) {
    return new Promise((resolve, reject) => {
      firebase.database().ref(path).once('value', resp => {
        let data = snapshotToArray(resp);
        resolve(data);
      }).catch(err => 
        {
          reject(err)
        });
    })
  }

  getDataObjectFromFirebase(path) {
    return new Promise((resolve, reject) => {
      firebase.database().ref(path).once('value', resp => {
        let data = resp.val();
        resolve(data);
      }).catch(err => reject(err));
    })
  }



  searchUserByUID(id,email) {
    return new Promise((resolve, reject) => {
      firebase.database().ref('users').orderByChild('uid').equalTo(id).once('value', resp => {
        let users = snapshotToArray(resp);
        if (users && users.length == 1) {
          resolve(users[0]);
        } else {
          if(email){
            firebase.database().ref('users').orderByChild('email').equalTo(email).once('value', Emailresp => {
              let Emailusers = snapshotToArray(Emailresp);
              if (Emailusers && Emailusers.length == 1) {
                resolve(Emailusers[0]);
              } else {
                reject({status:'Error',Message:"No user Found"});
              }
            });
          } else {
            reject({status:'Error',Message:"No user Found With that uid"});
          }

        }
      });
    });
  }

  snapshotToArray = snapshot => {
    let returnArr = [];
    snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });

    return returnArr;
  };
  
  snapshotDataToArray = snapshot => {
    let returnArr = [];
    snapshot.forEach(childSnapshot => {
      let item = childSnapshot.data();
      item.key = childSnapshot.id;
      returnArr.push(item);
    });

    return returnArr;
  };

  ObjectToArray = snapshot =>{
    let returnArr = [];
    let keys = Object.keys(snapshot);
    keys.forEach(key =>{
      let item = snapshot[key];
      item.key = key;
      returnArr.push(item);
    });
    return returnArr;
  }

}



export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};


export const ObjectToArray = snapshot =>{
  let returnArr = [];
  let keys = Object.keys(snapshot);
  keys.forEach(key =>{
    let item = snapshot[key];
    item.key = key;
    returnArr.push(item);
  });
  return returnArr;
}
