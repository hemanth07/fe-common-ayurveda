import { Component, OnInit,ViewChild , ElementRef } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { NavController, Events, AlertController, LoadingController, IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-observation-list',
  templateUrl: './farmeasy-observation-list.page.html',
  styleUrls: ['./farmeasy-observation-list.page.scss'],
})
export class FarmeasyObservationListPage implements OnInit {
  @ViewChild('infi') infi: ElementRef;
  @ViewChild('content') content;
  headerData: any = [];
  Observations: any = [];
  userData: any = [];
  showlabour: boolean;
  showadmin: boolean;
  showsupervisor: boolean;
  transObservation : string;
  priority: string;
  labour: string;
  block_label:any;
  plantHistory: any = [];
  count:any = 10;
  data = 'hello';
  previouscount:number = 0;
  not_acknowledge:any;
  acknowledged:any;
  pending:any;
  draft:any;
  wait:string;
  constructor(
    public translate: FarmeasyTranslate,
    public navCtrl: NavController,
    public events: Events,
    public storage: Storage,
    private sg: SimpleGlobal,
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    ) {
    }


    // this.wait = "Please Wait ..."
    // this.wait = "దయచేసి వేచి ఉండండి ..."
    // this.wait = "தயவுசெய்து காத்திருங்கள் ..."
    // this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
    // this.wait = "कृपया प्रतीक्षा करें ..."

  ngOnInit() {
      this.createHeader();
    if(this.sg['userdata'].primary_language === 'en'){
      this.transObservation = 'No Observations';
      this.priority = 'Priority';
      this.labour = 'Labour';
      this.block_label = 'Block';
      this.not_acknowledge = "Not Yet Acknowledged";
      this.acknowledged = 'Acknowledged';
      this.pending = "Pending";
      this.wait = "Please Wait ..."
      this.draft = "Draft";
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.transObservation = 'పరిశీలనలు లేవు';
      this.priority = 'ప్రాధాన్యత';
      this.labour = 'లేబర్';
      this.block_label = 'బ్లాక్';
      this.not_acknowledge = "ఇంకా అంగీకరించలేదు";
      this.acknowledged = 'తెలియజేసారు';
      this.pending = "పెండింగ్";
      this.wait = "దయచేసి వేచి ఉండండి ..."
      this.draft = "డ్రాఫ్ట్";
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.transObservation = 'வானிலை';
      this.priority = 'முன்னுரிமை';
      this.labour = 'தொழிலாளர்';
      this.block_label = 'பிளாக்';
      this.not_acknowledge = "இன்னும் ஒப்புக்கொள்ளப்படவில்லை";
      this.acknowledged = 'ஒப்புக்கொள்ளப்பட்டது';
      this.pending = "நிலுவையில்";
      this.wait = "தயவுசெய்து காத்திருங்கள் ..."
      this.draft = "வரைவு";
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.transObservation = 'ಯಾವುದೇ ಅವಲೋಕನಗಳಿಲ್ಲ';
      this.priority = 'ಆದ್ಯತೆ';
      this.labour = 'ಲೇಬರ್';
      this.block_label = 'ನಿರ್ಬಂಧಿಸಿ';
      this.not_acknowledge = "ಇನ್ನೂ ಅಂಗೀಕರಿಸಲಾಗಿಲ್ಲ";
      this.acknowledged = 'ಅಂಗೀಕರಿಸಲಾಗಿದೆ';
      this.pending = "ಬಾಕಿ ಉಳಿದಿದೆ";
      this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
      this.draft = "ಕರಡು";
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.transObservation = 'कोई अवलोकन नहीं';
      this.priority = 'प्राथमिकता';
      this.labour = 'श्रम';
      this.block_label = 'खंड';
      this.not_acknowledge = "अभी तक स्वीकार नहीं किया गया है";
      this.acknowledged = 'स्वीकार किया';
      this.pending = "अपूर्ण";
      this.wait = "कृपया प्रतीक्षा करें ..."
      this.draft = "प्रारूप";
    } else {
      this.transObservation = 'No Observations';
      this.priority = 'Priority';
      this.labour = 'Labour';
      this.block_label = 'Block';
      this.not_acknowledge = "Not Yet Acknowledged";
      this.acknowledged = 'Acknowledged';
      this.pending = "Pending";
      this.wait = "Please Wait ..."
      this.draft = "Draft";
    }
    this.storage.get('farmeasy_userdata').then((val) => {
      this.userData = val;
      if (this.userData.role === 'corporate') {
        this.showadmin = true;
      }
      if (this.userData.role === 'supervisor') {
        this.showsupervisor = true;
      }
      if (this.userData.role === 'employee') {
        this.showlabour = true;
      }
    });
  }
  createHeader() {
    let title
    if(this.sg['userdata'].primary_language === 'en'){
    title = 'Observation by'+" "+this.sg['userdata'].name;
     } else if(this.sg['userdata'].primary_language === 'te'){
    title = 'నా పరిశీలనలు';
     } else if(this.sg['userdata'].primary_language === 'ta'){
    title = 'என் கவனிப்புகள்';
     } else if(this.sg['userdata'].primary_language === 'kn'){
    title = 'ನನ್ನ ಅವಲೋಕನಗಳು';
     } else if(this.sg['userdata'].primary_language === 'hi'){
    title = 'मेरा अवलोकन';
     } else {
    title = 'Observation by'+" "+this.sg['userdata'].name;
    }
    this.headerData = {
        color: 'green',
        title: title,
        button1: 'add-circle-outline',
        button1_action: '/farmeasy-observation-enter',
        button2: 'home',
        button2_action: '/farm-dashboard',
    };
  }
  scrollPosition(event){
    console.log(event);
  }

  ionViewWillEnter(){
    console.log("ionViewDidEnter");
    this.getObservationList(this.count,0);
  }
  
  // viewObservation(observation) {
  //   if (typeof this.name === 'undefined') {
  //     const data = {observation: JSON.stringify(observation)};
  //     this.navCtrl.navigateForward(['/farmeasy-observation-view/' + observation.key, data]);
  //     // this.router.navigate(['/farmeasy-observation-view/' + observation.key, data], { preserveFragment: true });
  //   }  else {
  //     const data = {data: 'admin', observation: JSON.stringify(observation)};
  //     this.navCtrl.navigateForward(['/farmeasy-observation-view/' + observation.key, data]);
  //     // this.router.navigate(['/farmeasy-observation-view/' + observation.key, data], { preserveFragment: true });
  //   }
  // }
 async getObservationList(count,view) {
   this.Observations = [];
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
      firebase.database().ref(this.sg['userdata'].vendor_id+'/observations').orderByChild('created_by').equalTo(this.sg['userdata'].phone).limitToLast(count).on('value', resp => {
        let that;
        that = this;
        this.Observations = snapshotToArray(resp);
        this.storage.get('farmeasy_userdata').then((val) => {
          that.userData = val;
        });
        for (let i = 0; i < this.Observations.length; i++) {
          this.Observations[i]['days'] = this.daysbetweenDates(this.Observations[i]['date']);
        }
        this.Observations.reverse();
       this.translation(this.Observations);
       loading.dismiss();
       if(this.Observations.length == 0){
         this.Observations = 'No Observations';
       }
       setTimeout(()=>{
        this.content['el'].scrollToPoint(350,view).then((success)=>{console.log(success)},
        (err)=>{console.log(err)});
        console.log(this.content['el'].scrollToPoint(350,view));
        },100);
      });
  }
   daysbetweenDates(date) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const firstDate = new Date().getTime();
    const days = Math.round(Math.abs((firstDate - date) / (oneDay)));
    if ( days !== 0) {
      if ( days === 30) {
       return days + 'm ago';
      } else if (days === 365 ){
        return days + 'y ago';
      } else {
        return days + 'd ago';
      }
    } else {
      const hours = this.msToTime(Math.abs(firstDate - date));
      if ( hours !== 0) {
        return hours + 'h ago';
      } else {
        return 'Just Now';
      }
    }
   }
    msToTime(duration) {
    const hours: number = Math.round((duration / (1000 * 60 * 60)) % 24);
    return hours ;
  }
  viewObservationMode(observation) {
      const data = {observation: JSON.stringify(observation)};
      this.navCtrl.navigateForward(['/farmeasy-observation-view-mode/' + observation.key, data]);
      // this.router.navigate(['/farmeasy-observation-view-mode/' + observation.key, data], { preserveFragment: true });
  }
  editObservation(observation){
    const data = {data: 'admin', observation: JSON.stringify(observation)};
    this.navCtrl.navigateForward(['/farmeasy-observation-enter', data]);
  }
  async deleteObservation(observation){
      const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure want to Delete Observation...?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
           // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
          //  console.log(observation);
            for(let data of observation.address){
              firebase.storage().ref(data.path.name).delete();
            }
            firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/'+observation.key).remove();
          }
        }
      ]
    });

    await alert.present();
  }
  translation(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        description:item.description,
        labour:item.labour,
        proprity:item.proprity,
        user_role:item.user_role,
        username:item.username,
        status_label:(item.status == 'Not Yet Acknowledged') ? 'pending' : ((item.status == 'Actioned')? 'Work Assigned':((item.status == 'Draft'? 'Draft':'Reported')))
      }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.Observations[i].description = data['description'];
          this.Observations[i].labour = data['labour'];
          this.Observations[i].proprity = data['priority'];
          this.Observations[i].user_role = data['user_role'];
          this.Observations[i].username = data['username'];
          this.Observations[i]['status_label'] = data['status_label'];
          });
        // this.translate.translateObject(menu[i], 'en',this.sg['userdata'].primary_language).then(data => {
        //   this.Observations[i].description = data['description'];
        //   this.Observations[i].labour = data['labour'];
        //   this.Observations[i].proprity = data['priority'];
        //   this.Observations[i].user_role = data['user_role'];
        //   this.Observations[i].username = data['username'];
        //   this.Observations[i]['status_label'] = data['status_label'];
        //   });
      } else {
        // this.translate.reverieTranslation(menu[i],this.sg['user'])
        this.Observations[i].description = menu[i]['description'];
        this.Observations[i].labour =menu[i]['labour'];
        this.Observations[i].proprity = menu[i]['priority'];
        this.Observations[i].user_role = menu[i]['user_role'];
        this.Observations[i].username = menu[i]['username'];
        this.Observations[i]['status_label'] = menu[i]['status_label'];
      }
    }
  }

  loadData(event) {
    let view = this.infi['el'].offsetParent.scrollTop;
    console.log(this.infi['el'].offsetParent.scrollTop);
     this.count = this.count + 10;
     this.loadObservationList(this.count,view);
    setTimeout(() => {
      console.log('Done');
      event.target.complete();
      }, 500);
  }
  async loadObservationList(count,view) {
    // this.Observations = [];
     const loading = await this.loadingCtrl.create({
       spinner: 'bubbles',
       message:this.wait,
       translucent: true,
       cssClass: 'custom-class custom-loading'
     });
     loading.present();
         firebase.database().ref(this.sg['userdata'].vendor_id+'/observations').orderByChild('created_by').equalTo(this.sg['userdata'].phone).limitToLast(count).on('value', resp => {
         let that;
         that = this;
         let Observations = [];
         Observations = snapshotToArray(resp).reverse();
         let latestLength = Observations.length;
         for (let i = 0; i < Observations.length; i++) {
           Observations[i]['days'] = this.daysbetweenDates(Observations[i]['date']);
         }
        let lastObservations = Observations.slice(Observations.length -10 ,Observations.length);
        if(this.previouscount<latestLength && this.Observations.length != latestLength){ 
          for(let observation of lastObservations){
            this.Observations.push(observation);
          }
        }
        this.previouscount = latestLength;
        this.translation(this.Observations);
        loading.dismiss();
        // setTimeout(()=>{
        //  this.content['el'].scrollToPoint(350,view).then((success)=>{console.log(success)},
        //  (err)=>{console.log(err)});
        //  console.log(this.content['el'].scrollToPoint(350,view));
        //  },100);
       });
   }
  // loadData($event:any){
 
  //   $event.state = "closed";
  //   setTimeout(()=>{
  //     $event.target.hidden = true;
  //   },1000);
  //   console.log($event);
  // }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];

  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};