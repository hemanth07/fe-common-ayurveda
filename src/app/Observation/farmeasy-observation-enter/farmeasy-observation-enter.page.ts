import { Component, OnInit,ViewContainerRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Location } from '@angular/common';
import { Storage } from '@ionic/storage';
import { NavParams, NavController, PopoverController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { PopOverComponent } from '../../components/observation-components/pop-over/pop-over.component';
import { Router, Route } from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { HindiService } from './../../languages/hindi.service';
import { EnglishService } from './../../languages/english.service';
import { ActivatedRoute } from '@angular/router';
import { Device } from "@ionic-native/device/ngx";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-farmeasy-observation-enter',
  templateUrl: './farmeasy-observation-enter.page.html',
  styleUrls: ['./farmeasy-observation-enter.page.scss'],
})
export class FarmeasyObservationEnterPage implements OnInit {

  headerData: any = [];
  activatePadding: boolean;
  block: string;
  description: string;
  priority: string = 'Medium';
  labour: any;
  address: any = [];
  labours: any = [];
  temp_labours: any = [];
  date: any;
  userData: any = [];
  message: string = 'Your Observation is Recorded successfully and ' +
  'the field supervisor has been notified of the further action that needs to be taken.';
  showlabour: boolean;
  showadmin: boolean;
  showsupervisor: boolean;
  blocks: any = [];
  selectblock: string;
  enterdescription: string;
  enterdescriptionplace:string;
  indicatePriority: string;
  submit:string;
  selectFile:string;
  observation: any =[];
  prioritylist: any = [{name:'Urgent'},{name:'High'},{name:'Medium'},{name:'Low'}];
  block_label:any;
  cancel:any;
  ok:any;
  selectval:any;
  proof_label:any;
  Observation_location:any
  status:any;
  footerData:any;
  wait:string;
  constructor( public navCtrl: NavController,
    public storage: Storage,
    private resolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private sg: SimpleGlobal,
    public router: Router,
    private http: Http,
    public translate: FarmeasyTranslate,
    private popoverController: PopoverController,
    private alertController: AlertController,
    private device: Device,
    private diagnostic: Diagnostic,
    private androidPermissions: AndroidPermissions,
    private loc:Location,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService,
    private route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private toastController:ToastController,
    private geolocation: Geolocation
    ) { }
 
  ngOnInit() {
    this.createHeaderFooter();
    this.getLangLatValues();
    this.getPermission();
    this.translation();
    this.translatePriority(this.prioritylist);
    // this.Createlabours();
    this.getBlocks();
    this.date = new Date().getTime();
    this.route.params.subscribe(params => {
      let data  = JSON.parse( params['observation']);
      this.observation = data;
      this.address = data.address ? data.address : [];
      this.block = data.block;
      this.description = data.description;
      this.priority = data.priority;
      this.status = data.status;
      // console.log(data);
    });
    this.storage.get('farmeasy_userdata').then((val) => {
      this.userData = val;
      // console.log(this.userData);
      if (this.userData.role === 'corporate') {
        this.showadmin = true;
      }
      if (this.userData.role === 'supervisor') {
        this.showsupervisor = true;
      }
      if (this.userData.role === 'employee') {
        this.showlabour = true;
      }
    });
  }
   
  createHeaderFooter() {
    // color: green,orange
    let title
    if(this.sg['userdata'].primary_language === 'en'){
    title = 'Enter Observation';
    this.wait = "Please Wait ..."
    this.message= 'Your Observation is Recorded successfully and the field supervisor has been notified of the further action that needs to be taken.';
    this.footerData ={
      'footerColor':'rgba(255, 255, 255, 0.9)',
      'leftButtonName':'Submit',
      'rightButtonName':'Save Draft',
      'leftButtonColor':'green',
      'rightButtonColor':'grey',
      };
     } else if(this.sg['userdata'].primary_language === 'te'){
    title = 'ఎంటర్ పరిశీలన';
    this.wait = "దయచేసి వేచి ఉండండి ..."
    this.message= 'మీ పరిశీలన విజయవంతంగా రికార్డ్ చేయబడింది మరియు తీసుకోవలసిన తదుపరి చర్యల గురించి ఫీల్డ్ సూపర్‌వైజర్‌కు తెలియజేయబడింది.';
    this.footerData ={
      'footerColor':'rgba(255, 255, 255, 0.9)',
      'leftButtonName':'సమర్పించండి',
      'rightButtonName':'రాసినది భద్రపరచు',
      'leftButtonColor':'green',
      'rightButtonColor':'grey',
      };
     } else if(this.sg['userdata'].primary_language === 'ta'){
    title = 'அட்வென்சர் சேர்க்கவும்';
    this.wait = "தயவுசெய்து காத்திருங்கள் ..."
    this.message= 'உங்கள் அவதானிப்பு வெற்றிகரமாக பதிவுசெய்யப்பட்டு, மேற்கொண்டு எடுக்க வேண்டிய நடவடிக்கை குறித்து கள மேற்பார்வையாளருக்கு அறிவிக்கப்பட்டுள்ளது.';
    this.footerData ={
      'footerColor':'rgba(255, 255, 255, 0.9)',
      'leftButtonName':'சமர்ப்பி',
      'rightButtonName':'வரைவைச் சேமிக்கவும்',
      'leftButtonColor':'green',
      'rightButtonColor':'grey',
      };
     } else if(this.sg['userdata'].primary_language === 'kn'){
    title = 'ವೀಕ್ಷಣೆ ನಮೂದಿಸಿ';
    this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
    this.message= 'ನಿಮ್ಮ ವೀಕ್ಷಣೆಯನ್ನು ಯಶಸ್ವಿಯಾಗಿ ದಾಖಲಿಸಲಾಗಿದೆ ಮತ್ತು ಮುಂದಿನ ಕ್ರಮಗಳ ಬಗ್ಗೆ ಕ್ಷೇತ್ರ ಮೇಲ್ವಿಚಾರಕರಿಗೆ ತಿಳಿಸಲಾಗಿದೆ.';
    this.footerData ={
      'footerColor':'rgba(255, 255, 255, 0.9)',
      'leftButtonName':'ಸಲ್ಲಿಸು',
      'rightButtonName':'ಕರಡು ಉಳಿಸು',
      'leftButtonColor':'green',
      'rightButtonColor':'grey',
      };
     } else if(this.sg['userdata'].primary_language === 'hi'){
    title = 'अवलोकन दर्ज करें';
    this.wait = "कृपया प्रतीक्षा करें ..."
    this.message= 'आपका अवलोकन सफलतापूर्वक दर्ज किया गया है और क्षेत्र पर्यवेक्षक को आगे की कार्रवाई के बारे में सूचित किया गया है जिसे लेने की आवश्यकता है।';
    this.footerData ={
      'footerColor':'rgba(255, 255, 255, 0.9)',
      'leftButtonName':'प्रस्तुत',
      'rightButtonName':'लिखित को सुरक्षित करो',
      'leftButtonColor':'green',
      'rightButtonColor':'grey',
      };
     }else {
    title = 'Enter Observation';
    }
    this.headerData = {
        color: 'green',
        title: title,
        present : 'farmeasy-observation-enter',
        button1: 'home',
        button1_action: '/farm-dashboard',
        back: '/farmeasy-observation-list'
        };
   
  }
  getPermission() {
    this.diagnostic.getPermissionAuthorizationStatus(
        this.diagnostic.permission.READ_EXTERNAL_STORAGE
      ).then(
        status => {
          // console.log(`AuthorizationStatus`);
          // console.log(status);
          if (status != this.diagnostic.permissionStatus.GRANTED) {
            this.diagnostic
              .requestRuntimePermission(
                this.diagnostic.permission.READ_EXTERNAL_STORAGE
              )
              .then(data => {
                // console.log(`getCameraAuthorizationStatus`);
                // console.log(data);
              });
          } else {
            // console.log("We have the permission");
          }
        },
        statusError => {
          // console.log(`statusError`);
          // console.log(statusError);
        }
      );
    this.diagnostic
      .getPermissionAuthorizationStatus(this.diagnostic.permission.WRITE_EXTERNAL_STORAGE)
      .then(status => {
          // console.log(`AuthorizationStatus`);
          // console.log(status);
          if (status != this.diagnostic.permissionStatus.GRANTED) {
            this.diagnostic
              .requestRuntimePermission(
                this.diagnostic.permission.WRITE_EXTERNAL_STORAGE
              )
              .then(data => {
                // console.log(`getCameraAuthorizationStatus`);
                // console.log(data);
              });
          } else {
            // console.log("We have the permission");
          }
        },
        statusError => {
          // console.log(`statusError`);
          // console.log(statusError);
        });
  }
  translation(){
    let data;
    if(this.sg['userdata'].primary_language === 'en'){
     data  = this.en.getEnterObservation();
     } else if(this.sg['userdata'].primary_language === 'te'){
     data  = this.te.getEnterObservation();
     } else if(this.sg['userdata'].primary_language === 'ta'){
     data  = this.ta.getEnterObservation();
     } else if(this.sg['userdata'].primary_language === 'kn'){
     data  = this.kn.getEnterObservation();
     } else if(this.sg['userdata'].primary_language === 'hi'){
     data  = this.hi.getEnterObservation();
     }else {
      data  = this.en.getEnterObservation();
     }
        this.selectblock = data[0];
        this.enterdescription = data[1];
        this.indicatePriority = data[2];
        this.submit = data[3];
        this.message = data[4];
        this.selectFile = data[5];
        this.enterdescriptionplace = data[6];
        this.block_label = data[7];
        this.ok = data[8];
        this.cancel = data[9];
        this.selectval = data[10];
        this.proof_label = data[11];
        /* 'Select a Block',
        'Enter Problem Description',
        'Indicate Priority',
        'Submit',
        'select File',
        'Enter Problem Description' */
  }
  async getBlocks() {
    const loading = await this.loadingCtrl.create({
            spinner: 'bubbles',
            message: this.wait,
            translucent: true,
            cssClass: 'custom-class custom-loading'
          });
          loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp => {
      const data = snapshotToArray(resp);
      this.blocks = data;
      loading.dismiss();
    });
 }
//  async Createlabours() {
//     const loading = await this.loadingCtrl.create({
//       spinner: 'bubbles',
//       message: this.wait,
//       translucent: true,
//       cssClass: 'custom-class custom-loading'
//     });
//     loading.present();
//     firebase.database().ref('users').orderByChild('role').equalTo('labour').once('value', resp => {
//       // console.log(snapshotToArray(resp));
//       this.temp_labours = snapshotToArray(resp);
//       loading.dismiss();
//     //  this.translateLaboursName(this.temp_labours);
//       this.labours = this.temp_labours;
//       this.blockLabours();
//       this.reloadComponent();
//    });
//    this.labours = this.temp_labours;
//   }
  translatePriority(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        view:item.name,
         }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.prioritylist[i]['view'] = data['view'];
          });
      } else {
        this.prioritylist[i]['view'] = menu[i]['view'];
      }
    }
  }
  translateLaboursName(labourData){
   let names = [];
    for(let name of labourData){
      names.push(name.name);
    }
      // if( this.sg['userdata'].primary_language !== 'en'){
      //   this.translate.translateArray(names, 'en', this.sg['userdata'].primary_language).then(data => {
      //     let temp = []
      //     temp = new Array(data);
      //     for(let i=0;i<temp[0].length;i++){
      //       this.temp_labours[i].name = temp[0][i];
      //     }
      //   });
      //  }
  }
  blockLabours() {
    if(this.block){
      this.labours = this.temp_labours.filter( labour => {
        let blocks = [];
        blocks = labour.block_responsible;
        if(blocks){
        if (blocks.includes(this.block)) { return true; }
        } else {
          return false;
        }
      });
      // console.log(this.labours);
    }
  }
  getProofUrl(data) {
    this.getLangLatValues();
    this.address.push(data);
  }
  getLangLatValues(){
    this.geolocation.getCurrentPosition().then((resp) => {
        let location = {latitude:resp.coords.latitude,longitude:resp.coords.longitude}
        this.Observation_location = location
        }).catch((error) => {
         alert('Error getting location' + error);
       });
  }
  async presentPopover() {
    const popover = await this.popoverController.create({
      component: PopOverComponent,
      cssClass: 'popover_class',
      componentProps: {
        message: this.message
      },
    });
    await popover.present();
  }
  async createObservation() {
    let that = this;
    const error = [];
    if (typeof this.block === 'undefined') {
      error.push('Block');
    }
    if (typeof this.description === 'undefined') {
      error.push('Description');
    }
    // if (typeof this.priority === 'undefined') {
    //   error.push('Priority');
    // }
    if (this.address.length === 0) {
      error.push('Proofs');
    }
    if (error.length !== 0) {
      let message;
      if(this.sg['userdata'].primary_language === 'en'){
        message  = "Please fill all mandatory fields"
       } else if(this.sg['userdata'].primary_language === 'te'){
        message  = "దయచేసి అన్ని తప్పనిసరి ఫీల్డ్‌లను పూరించండి"
       } else if(this.sg['userdata'].primary_language === 'ta'){
        message  = "அனைத்து கட்டாய புலங்களையும் நிரப்பவும்"
       } else if(this.sg['userdata'].primary_language === 'kn'){
        message  = "ದಯವಿಟ್ಟು ಎಲ್ಲಾ ಕಡ್ಡಾಯ ಕ್ಷೇತ್ರಗಳನ್ನು ಭರ್ತಿ ಮಾಡಿ"
       } else if(this.sg['userdata'].primary_language === 'hi'){
        message  = "कृपया सभी अनिवार्य फ़ील्ड भरें"
       }
      const alert = await this.alertController.create({
        subHeader: message,
        message: error.join(', '),
        buttons: ['OK']
      });
      return await alert.present();
    } else {
      if(this.Observation_location){
        let body = {
          'location':this.Observation_location ? this.Observation_location : null,
          'created_by': this.userData.phone,
          'username': this.userData.name,
          'user_profile': this.userData.profile_url,
          'user_role': this.userData.role,
          'block': this.block,
          'description': this.description,
          'priority': this.priority? this.priority : null,
          'address': this.address,
          'date': this.date,
          'status' : 'Not Yet Acknowledged',
          'actions' : [{'action': 'Reported the observation', 'date':  new Date().toString().substring(0, 25), 'user_profile': this.userData.profile_url}]
        };
        // alert(JSON.stringify(body));
        if(this.observation.length != 0){
          let date =  new Date().toString().substring(0, 25);
          const action = {'action': this.userData.name + ' is Updated', 'date': date, 'user_profile': this.userData.profile_url};
          firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/' + this.observation.key + '/actions').push(action);
          firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/'+this.observation.key).update(body).then( () => {
            that.loc.back();
          });
        } else {
          firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/').push(body).then( () => {
            that.presentPopover();
            that.loc.back();
          }).catch((error) => {
            alert(error);
          });
        }
      } else {
        let message;
        if(this.sg['userdata'].primary_language === 'en'){
          message  = "Please On GPS Location"
         } else if(this.sg['userdata'].primary_language === 'te'){
          message  = "దయచేసి GPS లొకేషన్‌ను ఆన్ చేయండి"
         } else if(this.sg['userdata'].primary_language === 'ta'){
          message  = "ஜி.பி.எஸ் இருப்பிடத்தை மாற்றவும்"
         } else if(this.sg['userdata'].primary_language === 'kn'){
          message  = "ದಯವಿಟ್ಟು ಜಿಪಿಎಸ್ ಸ್ಥಳವನ್ನು ಬದಲಾಯಿಸಿ"
         } else if(this.sg['userdata'].primary_language === 'hi'){
          message  = "कृपया GPS लोकेशन पर स्विच करें"
         }
        const alert = await this.alertController.create({
          subHeader: message,
          buttons: ['OK']
        });
        return await alert.present();
        this.address = [];
      }
  
    }
  }
 async saveDraft(){
    if(!this.block && !this.description && this.address.length == 0){
      let message;
      if(this.sg['userdata'].primary_language === 'en'){
        message  = "Not able to save as Draft with all empty fields, enter something to save as draft"
       } else if(this.sg['userdata'].primary_language === 'te'){
        message  = "అన్ని ఖాళీ ఫీల్డ్‌లతో డ్రాఫ్ట్‌గా సేవ్ చేయలేకపోయింది, చిత్తుప్రతిగా సేవ్ చేయడానికి ఏదైనా నమోదు చేయండి"
       } else if(this.sg['userdata'].primary_language === 'ta'){
        message  = "எல்லா வெற்று புலங்களுடனும் வரைவாக சேமிக்க முடியவில்லை, வரைவாக சேமிக்க ஏதாவது உள்ளிடவும்"
       } else if(this.sg['userdata'].primary_language === 'kn'){
        message  = "ಎಲ್ಲಾ ಖಾಲಿ ಕ್ಷೇತ್ರಗಳೊಂದಿಗೆ ಡ್ರಾಫ್ಟ್ ಆಗಿ ಉಳಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ, ಡ್ರಾಫ್ಟ್ ಆಗಿ ಉಳಿಸಲು ಏನನ್ನಾದರೂ ನಮೂದಿಸಿ"
       } else if(this.sg['userdata'].primary_language === 'hi'){
        message  = "सभी खाली क्षेत्रों के साथ ड्राफ्ट के रूप में सहेजने में सक्षम नहीं, ड्राफ्ट के रूप में सहेजने के लिए कुछ दर्ज करें"
       }
      const alert = await this.alertController.create({
        header: 'Alert',
        message: message,
        buttons: ['OK']
      });
  
      await alert.present();
    } else {
      let message;
      if(this.sg['userdata'].primary_language === 'en'){
        message  = "Observation is saved as Draft"
       } else if(this.sg['userdata'].primary_language === 'te'){
        message  = "పరిశీలన డ్రాఫ్ట్ గా సేవ్ చేయబడింది"
       } else if(this.sg['userdata'].primary_language === 'ta'){
        message  = "கவனிப்பு வரைவாக சேமிக்கப்படுகிறது"
       } else if(this.sg['userdata'].primary_language === 'kn'){
        message  = "ವೀಕ್ಷಣೆಯನ್ನು ಡ್ರಾಫ್ಟ್ ಆಗಿ ಉಳಿಸಲಾಗಿದೆ"
       } else if(this.sg['userdata'].primary_language === 'hi'){
        message  ="अवलोकन को ड्राफ्ट के रूप में सहेजा जाता है"
       }
      let body = {
        'location':this.Observation_location ? this.Observation_location : null,
        'created_by': this.userData.phone,
        'username': this.userData.name,
        'user_profile': this.userData.profile_url,
        'user_role': this.userData.role,
        'block': this.block ? this.block : null,
        'description': this.description ? this.description :null,
        'priority': this.priority? this.priority : null,
        'address': this.address ? this.address : [],
        'date': this.date,
        'status' : 'Draft',
      };
      if(this.status == 'Draft'){
        firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/'+this.observation.key).update(body).then( async () => {
          const toast = await this.toastController.create({
            message: message,
            duration: 2000
          });
          toast.present();
          this.loc.back();
        });
      } else {
        firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/').push(body).then( async () => {
          const toast = await this.toastController.create({
            message: message,
            duration: 2000
          });
          toast.present();
          this.loc.back();
        });
      }
    }
  }
  async presentRemoveAlert(index,url){
    let message;
    let header;
    let no;
    let yes;
    if(this.sg['userdata'].primary_language === 'en'){
      message  = "Are you sure want to delete this attachment?"
      header = "confirm"
      no = "No"
      yes = "Yes"
     } else if(this.sg['userdata'].primary_language === 'te'){
      message  = "మీరు ఖచ్చితంగా ఈ జోడింపును తొలగించాలనుకుంటున్నారా?"
      header = "నిర్ధారించండి"
      no = "కాదు"
      yes = "అవును"
     } else if(this.sg['userdata'].primary_language === 'ta'){
      message  = "இந்த இணைப்பை நிச்சயமாக நீக்க விரும்புகிறீர்களா?"
      header = "உறுதிப்படுத்து"
      no = "இல்லை"
      yes = "ஆம்"
     } else if(this.sg['userdata'].primary_language === 'kn'){
      message  = "ಈ ಲಗತ್ತನ್ನು ಅಳಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ?"
      header = "ಖಚಿತಪಡಿಸಿ"
      no = "ಇಲ್ಲ"
      yes = "ಹೌದು"
     } else if(this.sg['userdata'].primary_language === 'hi'){
      message  = "क्या आप वाकई इस अनुलग्नक को हटाना चाहते हैं?"
      header = "पुष्टि करें"
      no = "नहीं"
      yes = "हाँ"
     }
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: no,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: yes,
          handler: () => {
            this.removeAttachment(index,url);
            // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  ngOnDestroy(){
    // prevent memory leak when component destroyed
    this.Observation_location = undefined;
    

  }
  removeAttachment(index,url) {
    let that;
    that = this;
      firebase.storage().ref(this.address[index].path.name).delete().then(function(snapshot) {
        that.address.splice(index, 1);
      });
   }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
  };
