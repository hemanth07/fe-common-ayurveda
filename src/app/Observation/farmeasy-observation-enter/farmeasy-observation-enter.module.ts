import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasyObservationEnterPage } from './farmeasy-observation-enter.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: FarmeasyObservationEnterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ObservationComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyObservationEnterPage]
})
export class FarmeasyObservationEnterPageModule {}
