import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasyObservationViewPage } from './farmeasy-observation-view.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: FarmeasyObservationViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ObservationComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyObservationViewPage]
})
export class FarmeasyObservationViewPageModule {}
