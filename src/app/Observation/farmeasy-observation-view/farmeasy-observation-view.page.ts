import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';

@Component({
  selector: 'app-farmeasy-observation-view',
  templateUrl: './farmeasy-observation-view.page.html',
  styleUrls: ['./farmeasy-observation-view.page.scss'],
})
export class FarmeasyObservationViewPage implements OnInit {
  headerData: any = [];
  data: any;
  constructor(private route: ActivatedRoute, private sg: SimpleGlobal,public translate: FarmeasyTranslate) { }

  ngOnInit() {
  this.route.params.subscribe(params => {
    this.data = params['data'];
   // console.log(this.data);
    });
    setTimeout(() => {
      this.createHeader();
      }, 100);
  }
  createHeader() {
    // color: green,orange
    let title
    if(this.sg['userdata'].primary_language === 'en'){
    title = 'View Observation';
     } else if(this.sg['userdata'].primary_language === 'te'){
    title = 'వీక్షణ పరిశీలన';
     } else if(this.sg['userdata'].primary_language === 'ta'){
    title = 'பார்வை கவனிப்பு';
     } else if(this.sg['userdata'].primary_language === 'kn'){
    title = 'ವೀಕ್ಷಣೆ ವೀಕ್ಷಿಸಿ';
     }else if(this.sg['userdata'].primary_language === 'hi'){
    title = 'अवलोकन देखें';
     } else {
    title = 'View Observation';
    }
    if (this.data === 'admin') {
      this.headerData = {
        color: 'green',
        title: title,
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    } else {
      this.headerData = {
        color: 'green',
        title: title,
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    }
  }

}
