import { Component, OnInit,ViewChild , ElementRef } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { NavController, Events, AlertController, LoadingController, IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-all-observations',
  templateUrl: './farmeasy-all-observations.page.html',
  styleUrls: ['./farmeasy-all-observations.page.scss'],
})
export class FarmeasyAllObservationsPage implements OnInit {
  headerData: any = [];
  Observations: any = [];
  userData: any = [];
  showlabour: boolean;
  showadmin: boolean;
  showsupervisor: boolean;
  transObservation : string;
  priority: string;
  labour: string;
  block_label:any;
  plantHistory: any = [];
  count:any = 10;
  data = 'hello';
  type:any;
  previouscount:number = 0;
  wait:string;
  constructor(
    public translate: FarmeasyTranslate,
    public navCtrl: NavController,
    public events: Events,
    public storage: Storage,
    private sg: SimpleGlobal,
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    ) {
    }
  ngOnInit() {
    this.createHeader();
    this.type = 'All Observations';
    if(this.sg['userdata'].primary_language === 'en'){
      this.transObservation = 'No Observations';
      this.priority = 'Priority';
      this.labour = 'employee';
      this.block_label = 'Block';
      this.wait = "Please Wait ..."
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.transObservation = 'పరిశీలనలు లేవు';
      this.priority = 'ప్రాధాన్యత';
      this.labour = 'లేబర్';
      this.block_label = 'బ్లాక్';
      this.wait = "దయచేసి వేచి ఉండండి ..."

    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.transObservation = 'வானிலை';
      this.priority = 'முன்னுரிமை';
      this.labour = 'தொழிலாளர்';
      this.block_label = 'பிளாக்';
      this.wait = "தயவுசெய்து காத்திருங்கள் ..."

    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.transObservation = 'ಯಾವುದೇ ಅವಲೋಕನಗಳಿಲ್ಲ';
      this.priority = 'ಆದ್ಯತೆ';
      this.labour = 'ಲೇಬರ್';
      this.block_label = 'ನಿರ್ಬಂಧಿಸಿ';
      this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."

    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.transObservation = 'कोई अवलोकन नहीं';
      this.priority = 'प्राथमिकता';
      this.labour = 'श्रम';
      this.block_label = 'खंड';
      this.wait = "कृपया प्रतीक्षा करें ..."

    } else {
      this.transObservation = 'No Observations';
      this.priority = 'Priority';
      this.labour = 'employee';
      this.block_label = 'Block';
      this.wait = "Please Wait ..."

    }
    this.storage.get('farmeasy_userdata').then((val) => {
      this.userData = val;
      if (this.userData.role === 'corporate') {
        this.showadmin = true;
      }
      if (this.userData.role === 'supervisor') {
        this.showsupervisor = true;
      }
      if (this.userData.role === 'labour') {
        this.showlabour = true;
      }
    });
  }
  createHeader() {
    // color: green,orange
    let title
    if(this.sg['userdata'].primary_language === 'en'){
    title = 'All Observations';
     } else if(this.sg['userdata'].primary_language === 'te'){
    title = 'అన్ని పరిశీలనలు';
     } else if(this.sg['userdata'].primary_language === 'ta'){
    title = 'அனைத்து கவனிப்புகளும்';
     } else if(this.sg['userdata'].primary_language === 'kn'){
    title = 'ಎಲ್ಲಾ ಅವಲೋಕನಗಳು';
     } else if(this.sg['userdata'].primary_language === 'hi'){
    title = 'सभी अवलोकन';
     } else {
    title = 'All Observations';
    }
     this.headerData = {
       color: 'green',
       title: title,
       button1: 'home',
       button1_action: '/farm-dashboard',
        };
   }
  scrollPosition(event){
    console.log(event);
  }

  ionViewWillEnter(){
   console.log("ionViewWillEnter");
  }
  ionViewCanEnter(){
    console.log("ionViewCanEnter");
   
  }
  ionViewDidEnter(){
    console.log("ionViewDidEnter");
    this.getObservationList(this.count,0);
  }
  
  // viewObservation(observation) {
  //   if (typeof this.name === 'undefined') {
  //     const data = {observation: JSON.stringify(observation)};
  //     this.navCtrl.navigateForward(['/farmeasy-observation-view/' + observation.key, data]);
  //     // this.router.navigate(['/farmeasy-observation-view/' + observation.key, data], { preserveFragment: true });
  //   }  else {
  //     const data = {data: 'admin', observation: JSON.stringify(observation)};
  //     this.navCtrl.navigateForward(['/farmeasy-observation-view/' + observation.key, data]);
  //     // this.router.navigate(['/farmeasy-observation-view/' + observation.key, data], { preserveFragment: true });
  //   }
  // }
 async getObservationList(count,view) {
   this.Observations = [];
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/observations').limitToLast(count).on('value', resp => {
      let that;
      that = this;
      this.Observations = snapshotToArray(resp);
      this.storage.get('farmeasy_userdata').then((val) => {
        that.userData = val;
      });
      for (let i = 0; i < this.Observations.length; i++) {
        this.Observations[i]['days'] = this.daysbetweenDates(this.Observations[i]['date']);
      }
      this.Observations.reverse();
     this.translation(this.Observations);
     loading.dismiss();
     if(this.Observations.length == 0){
       this.Observations = 'No Observations';
        }
     });
  }
   daysbetweenDates(date) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const firstDate = new Date().getTime();
    const days = Math.round(Math.abs((firstDate - date) / (oneDay)));
    if ( days !== 0) {
      if ( days === 30) {
       return days + 'm ago';
      } else if (days === 365 ){
        return days + 'y ago';
      } else {
        return days + 'd ago';
      }
    } else {
      const hours = this.msToTime(Math.abs(firstDate - date));
      if ( hours !== 0) {
        return hours + 'h ago';
      } else {
        return 'Just Now';
      }
    }
   }
    msToTime(duration) {
    const hours: number = Math.round((duration / (1000 * 60 * 60)) % 24);
    return hours ;
  }
  viewObservationMode(observation) {
    const data = {data: 'admin', observation: JSON.stringify(observation)};
    this.navCtrl.navigateForward(['/farmeasy-observation-view-mode/' + observation.key, data]);
  }
  editObservation(observation){
    const data = {data: 'admin', observation: JSON.stringify(observation)};
    this.navCtrl.navigateForward(['/farmeasy-observation-enter', data]);
  }
  async deleteObservation(observation){
      const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure want to Delete Observation...?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
           // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
          //  console.log(observation);
            for(let data of observation.address){
              firebase.storage().ref(data.path.name).delete();
              console.log(observation.description);
            }
            firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/'+observation.key).remove();
          }
        }
      ]
    });

    await alert.present();
  }
  translation(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        description:item.description,
        labour:item.labour,
        proprity:item.proprity,
        user_role:item.user_role,
        username:item.username,
        status_label:(item.status == 'Not Yet Acknowledged') ? 'pending' : ((item.status == 'Actioned')? 'Work Assigned':((item.status == 'Draft'? 'Draft':'Reported')))
      }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.Observations[i].description = data['description'];
          this.Observations[i].labour = data['employee'];
          this.Observations[i].proprity = data['priority'];
          this.Observations[i].user_role = data['user_role'];
          this.Observations[i].username = data['username'];
          this.Observations[i]['status_label'] = data['status_label'];
          });
      } else {
        this.Observations[i].description = menu[i]['description'];
        this.Observations[i].labour =menu[i]['labour'];
        this.Observations[i].proprity = menu[i]['priority'];
        this.Observations[i].user_role = menu[i]['user_role'];
        this.Observations[i].username = menu[i]['username'];
        this.Observations[i]['status_label'] = menu[i]['status_label'];
      }
    }
  }
  loadData(event) {
    // let view = this.infi['el'].offsetParent.scrollTop;
    // console.log(this.infi['el'].offsetParent.scrollTop);
     this.count = this.count + 10;
     this.loadObservationList(this.count);
    setTimeout(() => {
      console.log('Done');
      event.target.complete();                                                                                      
      }, 500);
  }
  async loadObservationList(count) {
    // this.Observations = [];
     const loading = await this.loadingCtrl.create({
       spinner: 'bubbles',
       message: this.wait,
                                                                                             translucent: true,
       cssClass: 'custom-class custom-loading'
     });
     loading.present();
     firebase.database().ref(this.sg['userdata'].vendor_id+'/observations').limitToLast(count).on('value', resp => {
         let that;
         that = this;
         let Observations = [];
         Observations = snapshotToArray(resp).reverse();
         let latestLength = Observations.length;
         for (let i = 0; i < Observations.length; i++) {
           Observations[i]['days'] = this.daysbetweenDates(Observations[i]['date']);
         }
        let lastObservations = Observations.slice(Observations.length -10 ,Observations.length);
        if(this.previouscount<latestLength && this.Observations.length != latestLength){
          for(let observation of lastObservations){
            this.Observations.push(observation);
          }
        }
        this.previouscount = latestLength;
        this.translation(this.Observations);
        loading.dismiss();
        // setTimeout(()=>{
        //  this.content['el'].scrollToPoint(350,view).then((success)=>{console.log(success)},
        //  (err)=>{console.log(err)});
        //  console.log(this.content['el'].scrollToPoint(350,view));
        //  },100);
       });
   }
  // loadData($event:any){
 
  //   $event.state = "closed";
  //   setTimeout(()=>{
  //     $event.target.hidden = true;
  //   },1000);
  //   console.log($event);
  // }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];

  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};