import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FarmeasyObservationViewModePage } from './farmeasy-observation-view-mode.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: FarmeasyObservationViewModePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ObservationComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyObservationViewModePage]
})
export class FarmeasyObservationViewModePageModule {}
