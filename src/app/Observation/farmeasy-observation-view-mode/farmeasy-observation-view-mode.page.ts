import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { PopoverController, NavController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { PopOverComponent } from '../../components/observation-components/pop-over/pop-over.component';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from './../../languages/english.service';
import { HindiService } from './../../languages/hindi.service';
import { Router, Route } from '@angular/router';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-farmeasy-observation-view-mode',
  templateUrl: './farmeasy-observation-view-mode.page.html',
  styleUrls: ['./farmeasy-observation-view-mode.page.scss'],
})
export class FarmeasyObservationViewModePage implements OnInit {

  headerData: any = [];
  data: any;
  id: any;
  observation: any;
  userData: any;
  date: string;
  date_string:string;
  message: string;
  showlabour: boolean;
  showadmin: boolean;
  showsupervisor: boolean;
  acknowledgeflag: boolean;
  images: any = [];
  videos: any = [];
  labels: any = [];
  actions: any = [];
  constructor(private route: ActivatedRoute, 
    public translate: FarmeasyTranslate,
    public storage: Storage,
    private popoverController: PopoverController,
    private navCtrl :NavController,
    public router: Router,
    private launchNavigator: LaunchNavigator,
    private sg: SimpleGlobal,
    private en: EnglishService,
    private ta: TamilService,
    private hi: HindiService,
    private te: TeluguService,
    private kn: KanadaService) { }

  ngOnInit() {
    this.createHeader();
    this.message = 'Your Observation is recorded successfully';
    this.date = new Date().toString().substring(0, 25);
    this.millitoFulldate(new Date().getTime());
    this.id = this.route.snapshot.paramMap.get('id');
    this.getLables();
    this.route.params.subscribe(params => {
      this.observation = JSON.parse( params['observation']);
      this.sg['observation'] = this.observation;
      this.observation['save_time'] = this.convertMilliToDate(this.observation.date);
      firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/'+this.sg['observation'].key).on('value', resp => {
        let data = resp.val();
        this.translation([data]);
      });
      if (this.observation.actions) {
        this.actions = [];
        const data = Object.entries(this.observation.actions).map((value) => (value));
        for (const item of data) {
          this.actions.push(item[1]);
        }
      }
      if (this.observation.address) {
        this.images = [];
        this.videos = [];
        for (const item of this.observation.address) {
            if (item.type !== 'video') {
              this.images.push(item);
            } else {
              this.videos.push(item);
            }
        }
      }
     });
    this.storage.get('farmeasy_userdata').then((val) => {
      this.userData = val;
      if (this.userData.role === 'corporate') {
        this.showadmin = true;
      }
      if (this.userData.role === 'supervisor') {
        this.showsupervisor = true;
      }
      if (this.userData.role === 'employee') {
        this.showlabour = true;
      }
    });
  }
  createHeader() {
    // color: green,orange
    let title
    if(this.sg['userdata'].primary_language === 'en'){
    title = 'View Observation';
     } else if(this.sg['userdata'].primary_language === 'te'){
    title = 'వీక్షణ పరిశీలన';
     } else if(this.sg['userdata'].primary_language === 'ta'){
    title = 'பார்வை கவனிப்பு';
     } else if(this.sg['userdata'].primary_language === 'kn'){
    title = 'ವೀಕ್ಷಣೆ ವೀಕ್ಷಿಸಿ';
     }else if(this.sg['userdata'].primary_language === 'hi'){
    title = 'अवलोकन देखें';
     } else {
    title = 'View Observation';
    }
      this.headerData = {
        color: 'green',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
       };
  }
  goToLocation(){
    if(this.observation.location){
    let latitiude = this.observation.location.latitude
    let longititude = this.observation.location.longitude
    this.launchNavigator.navigate([latitiude,longititude]).then(
    success => console.log('Launched navigator'),
    error => alert(error)
  );
    }
  }
  millitoFulldate(milli){
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var d = new Date(milli);
    var dayName = days[d.getDay()];
    let day = new Date(milli).toString().substring(0, 15).split(" ");
    let month = day[2]+"-"+day[1]+"-"+day[3]
    let date = new Date(milli);
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let sec = date.getSeconds()<10 ? '0'+date.getSeconds(): date.getSeconds();
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    let minute = minutes < 10 ? '0'+minutes : minutes;
    let strTime =dayName+", "+ month+" "+hours + ':' + minute + ':'+sec+' ' + ampm;
    this.date_string = strTime;
    // return strTime;
  }
  convertMilliToDate(milli){
  let day = new Date(milli).toString().substring(0, 15);
  let date = new Date(milli);
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let sec = date.getSeconds()<10 ? '0'+date.getSeconds(): date.getSeconds();
  let ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  let minute = minutes < 10 ? '0'+minutes : minutes;
  let strTime = day+" "+hours + ':' + minute + ':'+sec+' ' + ampm;
  return strTime;
  }
  translation(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        status_label:item.status,
        status:item.status
        }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.observation['status_label'] = data['status_label'];
          this.observation['task_created'] = temp[0].task_created;
          this.observation['status'] = temp[0].status;
          });
      } else {
        this.observation['status_label'] = menu[i]['status_label'];
        this.observation['task_created'] = temp[0].task_created;
        this.observation['status'] = temp[0].status;
      }
    }
  }
  acknowledge() {
    //this.acknowledgeflag = true;
    const body = {'action':'Observation acknowledged', 'date': this.date_string, 'user_profile': this.userData.profile_url};
    firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/' + this.id ).update({'status': 'Acknowledged'});
    firebase.database().ref(this.sg['userdata'].vendor_id+'/observations/' + this.id + '/actions').push(body);
  }
  getLables(){
    if(this.sg['userdata'].primary_language === 'en'){
    this.labels  = this.en.getViewObservationLabels();
    } else if(this.sg['userdata'].primary_language === 'te'){
  this.labels = this.te.getViewObservationLabels();
    } else if(this.sg['userdata'].primary_language === 'ta'){
  this.labels = this.ta.getViewObservationLabels();
    } else if(this.sg['userdata'].primary_language === 'kn'){
  this.labels = this.kn.getViewObservationLabels();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.labels = this.hi.getViewObservationLabels();
    } else {
   this.labels = this.en.getViewObservationLabels();
    }
 }
 converTask(){
    const data = {observation: JSON.stringify(this.observation)};
    this.navCtrl.navigateForward(['/create-task-labour',data]);
 }
  async presentPopover() {
    const popover = await this.popoverController.create({
      component: PopOverComponent,
      cssClass: 'popover_class',
      componentProps: {
        message: this.message
      },
    });
    await popover.present();
  }

}
