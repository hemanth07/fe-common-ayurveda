import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PlantsHystoryPage } from './plants-hystory.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: PlantsHystoryPage
  }
];

@NgModule({
  imports: [
    ObservationComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PlantsHystoryPage]
})
export class PlantsHystoryPageModule {}
