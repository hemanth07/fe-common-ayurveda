import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import { LoadingController } from '@ionic/angular';
import { TaskUpdateHistoryComponent } from 'src/app/components/task-manger-components/task-update-history/task-update-history.component';
@Component({
  selector: 'app-plants-hystory',
  templateUrl: './plants-hystory.page.html',
  styleUrls: ['./plants-hystory.page.scss'],
})
export class PlantsHystoryPage implements OnInit {
  plantsDetails: any=[];
  fruitsData: any = [];
  vegetablesData: any = [];
  showFruitsData: boolean = true;
  plantType:any;
  plantsHistory:any=[];
  showvegitablesData: boolean = false;
  pulsesData:any=[];
  showPulsesData:boolean=false;
  grainsData:any=[];
  showGrainsData:boolean=false;
  showTubersData:boolean=false;
  showTreenutsData:boolean=false;
  tubersData:any=[];
  treenutsData:any=[];
  labels:any;
  title:any;
  plants: string;
  myInput:any;
  constructor(public navCtrl: NavController,
    private sg: SimpleGlobal,
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController ) { }

  ngOnInit() {

    this.plants = "Fruit";
    //    for(var i=0;i<this.plantsHistory.Sheet1.length;i++){
    //   let newData = firebase.database().ref('PlantDetails/sheet1').push();
    //   newData.set(this.plantsHistory.Sheet1[i]);
    // }
    this.initializeData();
    this.createHeader();
  }
  async initializeData() {
let that=this;
let url;
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    if(this.sg['userdata'].primary_language === 'en'){
      url = 'PlantDetails/english';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      url = 'PlantDetails/kannada';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      url = 'PlantDetails/tamil';
    } else if(this.sg['userdata'].primary_language === 'te'){
      url = 'PlantDetails/telugu';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      url = 'PlantDetails/hindi';
    }
    let usersRef = firebase.database().ref(url);
    usersRef.on("value", function (Data) {
      let data = snapshotToArray(Data);
      if (data.length > 0) {
        // console.log(JSON.stringify(data));
        that.plantsInformation(data);
        loading.dismiss();
      }
    });
  }
  onCancel($event){

  }
  cancelSearch(){
    
  }
  createHeader() {
    // color: green,orange
    let data;
     if(this.sg['userdata'].primary_language === 'en'){
      this.title = 'Plants History';
      this.labels = { fruits:'Fruits',vegetables:'Vegetables', pulses:'Pulses',grains:'Grains',tubers:'Tubers',treenuts:'Treenuts',view:"View"}
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.title  = 'ಸಸ್ಯಗಳ ಇತಿಹಾಸ ';
      this.labels = { fruits:'ಹಣ್ಣುಗಳು',vegetables:'ತರಕಾರಿಗಳು', pulses:'ದ್ವಿದಳ ಧಾನ್ಯಗಳು',grains:'ಧಾನ್ಯಗಳು',tubers:'ಟ್ಯೂಬರ್ಗಳು',treenuts:'ಟ್ರೀ ನಟ್ಸ್',view:"ನೋಟ"}
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.title  = 'தாவரங்களின் வரலாறு';
      this.labels = { fruits:'பழங்கள்',vegetables:'காய்கறிகள்', pulses:'பருப்பு',grains:'தானியங்கள்',tubers:'கிழங்குகளும்',treenuts:'மரம் நட்ஸ்',view:"காண்க"}
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.title  = 'మొక్కలు చరిత్ర';
      this.labels = { fruits:'పండ్లు',vegetables:'కూరగాయలు', pulses:'పప్పులు',grains:'ధాన్యాలు',tubers:'దుంపలు',treenuts:'చెట్టు గింజలు',view:"చూడండి"}
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.title  = 'पौधों का इतिहास';
      this.labels = { fruits:'फल',vegetables:'सब्जियां', pulses:'दलहन',grains:'अनाज',tubers:'कंद',treenuts:'पेड़ की सुपारी',view:"राय"}
    }
    // if (data.length > 0) {
    //   this.plantsInformation(data);
    // }
  }

  plantsInformation(data){
    // console.log(data);
    this.plantsDetails = data;
    // console.log(this.plantsDetails);
   
    if (this.showFruitsData) {
     
      this.segmentChanged('Fruit');
    }
  }
  getSelectedFruit(data) {
    // console.log(data);
    // alert('fruit');
    let plantData = { "plantData": JSON.stringify(data) }
    this.navCtrl.navigateForward(['/view-plant-details', plantData]);
  }
  getSelectedvegetable(data) {
    // console.log(data);
    // alert('veg');
    let plantData = { "plantData": JSON.stringify(data) }
    this.navCtrl.navigateForward(['/view-plant-details', plantData]);
  }
  getSelectedTuber(data){

    // alert('nut');
    // console.log(data);
    let plantData = { "plantData": JSON.stringify(data) }
    this.navCtrl.navigateForward(['/view-plant-details', plantData]);
  }
  segmentChanged(val) {
    // console.log(this.plantsDetails);
    // console.log(val);
    this.plantType=val;
    if (val == 'Fruit') {
      this.showFruitsData = true;
      this.showvegitablesData = false;
      this.showPulsesData = false;
      this.showGrainsData = false;
      this.fruitsData = this.plantsDetails.filter(
        m => m.fruits_vegetables == "Fruit"
      );
      // console.log(this.fruitsData);
    }
    if (val == 'Vegetable') {
      this.showvegitablesData = true;
      this.showFruitsData = false;
      this.vegetablesData = this.plantsDetails.filter(
        m => m.fruits_vegetables == "Vegetable"
      );
      // console.log(this.fruitsData);
    }
    if (val == 'Pulses') {
      this.showPulsesData = true;
      this.showvegitablesData = false;
      this.showFruitsData = false;
      this.showGrainsData = false;
      this.showTubersData = false;
      this.showTreenutsData = false;
      this.pulsesData = this.plantsDetails.filter(
        m => m.fruits_vegetables == "Pulses"
      );
      // console.log(this.pulsesData);
    }
    if (val == 'Grains') {
      this.showGrainsData = true;
      this.showPulsesData = false;
      this.showvegitablesData = false;
      this.showFruitsData = false;
      this.showTubersData = false;
      this.showTreenutsData = false;
      this.grainsData = this.plantsDetails.filter(
        m => m.fruits_vegetables == "Grains"
      );
      // console.log(this.grainsData);
    }
    if (val == 'Tubers') {
      this.showGrainsData = false;
      this.showPulsesData = false;
      this.showvegitablesData = false;
      this.showFruitsData = false;
      this.showTubersData = true;
      this.showTreenutsData = false;
      this.tubersData = this.plantsDetails.filter(
        m => m.fruits_vegetables == "Tubers"
      );
      // console.log(this.grainsData);
    }
    if (val == 'Treenuts') {
      this.showGrainsData = false;
      this.showPulsesData = false;
      this.showvegitablesData = false;
      this.showFruitsData = false;
      this.showTubersData = false;
      this.showTreenutsData = true;
      this.treenutsData = this.plantsDetails.filter(
        m => m.fruits_vegetables == "Tubers"
      );
      // console.log(this.grainsData);
    }

  }
  searchByFruits(ev) {
    // this.rowsData = this.duprowsData;
    this.segmentChanged('Fruit');
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.fruitsData = this.fruitsData.filter((fruit) => {
        return (fruit.plant_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.segmentChanged('Fruit');
    }
  }
  searchByVegitables(ev) {
    // this.rowsData = this.duprowsData;
    this.segmentChanged('Vegetable');
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.vegetablesData = this.vegetablesData.filter((vegitable) => {
        return (vegitable.plant_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.segmentChanged('Vegetable');
    }
  }
  searchByGrains(ev){
    this.segmentChanged('Grains');
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.grainsData = this.grainsData.filter((grains) => {
        return (grains.plant_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.segmentChanged('Grains');
    }
  }
  searchByPulses(ev){
    this.segmentChanged('Pulses');
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.pulsesData = this.pulsesData.filter((pluse) => {
        return (pluse.plant_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.segmentChanged('Pulses');
    }
  }
 
  searchByTubers(ev){
    this.segmentChanged('Tubers');
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.tubersData = this.tubersData.filter((tuber) => {
        return (tuber.plant_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.segmentChanged('Tubers');
    }

  }
  searchByTreenuts(ev){
    this.segmentChanged('Treenuts');
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.treenutsData = this.treenutsData.filter((treenut) => {
        return (treenut.plant_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.segmentChanged('Treenuts');
    }
  }
  home(){
    // NavController.setRoot();
    this.navCtrl.navigateRoot('/farm-dashboard');
  }
  addNewPlant() {
    // console.log(this.plantType);
    let plantData = { "plantType": JSON.stringify(this.plantType) }
    this.navCtrl.navigateForward(['/add-new-plant', plantData]);
    

  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
