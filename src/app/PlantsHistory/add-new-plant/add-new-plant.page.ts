import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { File } from '@ionic-native/file/ngx';
import { LoadingController,ToastController ,NavController} from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { OverlayEventDetail } from '@ionic/core';
// import {FormBuilder,FormGroup,AbstractControl,Validators} from '@angular/forms';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import {Http, Headers, RequestOptions} from '@angular/http';
import { map } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
@Component({
  selector: 'app-add-new-plant',
  templateUrl: './add-new-plant.page.html',
  styleUrls: ['./add-new-plant.page.scss'],
})
export class AddNewPlantPage implements OnInit {
  headerData: any;
  isSubmit: boolean = true;
  plant_name: any;
  plant_depth_length: any;
  plant_diseases_without_chemicals: any;
  plant_cropping_methods: any;
  plant_life_span: any;
  plant_mature_period: any;
  plant_place_required: any;
  plant_preventions_before_diseases: any;
  plant_temparature: any;
  plant_cropping_timings: any;
  plants_energy_required: any;
  plant_organic_material: any;
  plant_water_quantity: any;
  plant_without_freeze_time: any;
  plant_with_freez_time: any;
  plantType: any;
  date: any;
  plant_link:any='';
  cameraImage: any = { 'type': 'camera', 'url': '', 'path': '' };
  plantName: any;
  DepthLength: any;
  diseasesControllingMethods: any;
  croppingMethods: any;
  lifeSpan: any;
  maturePeriod: any;
  placeRequired: any;
  preventionBeforeDiseasea: any;
  temparature: any;
  croppingTimings: any;
  energyRequiredToGrow: any;
  organicMaterialRequired: any;
  waterQuantity: any;
  withoutFreezTime: any;
  withFreezTime: any;
  link:any;
  submitAttempt: boolean = false;
  formgroup: FormGroup;
  editPlant: any;
  plantLink: any='';
  plantUdatedLength: any;
  imagePrevie:any;
  ishttpUrl:boolean=false;
  showLink: boolean=true;
  constructor(private route: ActivatedRoute, private file: File, public loadingCtrl: LoadingController, private camera: Camera,
    public formbuilder: FormBuilder,public toastController: ToastController,public navCtrl: NavController,private http: Http,private iab: InAppBrowser) {
    this.formgroup = formbuilder.group({
      plantName: ['', Validators.required],
      DepthLength: ['', Validators.required],
      diseasesControllingMethods: ['', Validators.required],
      croppingMethods: ['', Validators.required],
      lifeSpan: ['', Validators.required],
      maturePeriod: ['', Validators.required],
      placeRequired: ['', Validators.required],
      preventionBeforeDiseasea: ['', Validators.required],
      temparature: ['', Validators.required],
      croppingTimings: ['', Validators.required],
      energyRequiredToGrow: ['', Validators.required],
      waterQuantity: ['', Validators.required],
      withoutFreezTime: ['', Validators.required],
      withFreezTime: ['', Validators.required],
      organicMaterialRequired: ['', Validators.required],
      link:['',Validators]


    });
  }
 
  ngOnInit() {
    let sub = this.route.params.subscribe(params => {
      this.plantType = JSON.parse(params["plantType"]);
     // console.log(this.plantType);
    });
    let sub1 = this.route.params.subscribe(params => {
      this.editPlant = JSON.parse(params["plantData"]);
     // console.log(this.editPlant);
    });
    this.headerData = { 'title': 'Add Plant', 'back': '/plants-hystory', 'color': 'green' };
    if (this.editPlant) {
      this.editPlantDetials();
    }
  }
  editPlantDetials() {
    //console.log(this.editPlant);
    this.plant_name = this.editPlant.plant_name;
    this.plant_depth_length = this.editPlant.depth_length;
    this.plant_diseases_without_chemicals = this.editPlant.diseases_without_chemicals;
    this.plant_cropping_methods = this.editPlant.different_crops;
    this.plant_life_span = this.editPlant.life_span;
    this.plant_mature_period = this.editPlant.mature_period;
    this.plant_place_required = this.editPlant.place_required;
    this.plant_preventions_before_diseases = this.editPlant.prevention_before_diseases;
    this.plant_temparature = this.editPlant.temperature;
    this.plant_cropping_timings = this.editPlant.cropping_timing;
    this.plants_energy_required = this.editPlant.energy_required_to_grow;
    this.plant_organic_material = this.editPlant.organic_material;
    this.plant_water_quantity = this.editPlant.water_quantity;
    this.plant_without_freeze_time = this.editPlant.without_freeze_time;
    this.plant_with_freez_time = this.editPlant.with_freeze_time;
    this.cameraImage = this.editPlant.image;
    this.plantLink = this.editPlant.link;
    this.plantType = this.editPlant.fruits_vegetables;
    this.plantUdatedLength = this.editPlant.s_no;

  }
  getPlant_name(val) {
   // console.log(this.plant_name);
    // if(this.plant_name && this.plant_depth_length && this.plant_diseases_without_chemicals && this.plant_cropping_methods && 
    //   this.plant_life_span && this.plant_mature_period && )
  }
  getPlant_depth_length(val) {
    //console.log(this.plant_depth_length);
  }
  getPlant_diseases_without_chemicals(val) {
    //console.log(this.plant_diseases_without_chemicals);
  }
  getPlant_cropping_methods(val) {
    //console.log(this.plant_cropping_methods);
  }
  getPlant_life_span(val) {
    //console.log(this.plant_life_span);
  }
  getPlant_mature_period(val) {
    //console.log(this.plant_mature_period);
  }
  getPlant_place_required(val) {
    //console.log(this.plant_place_required);
  }
  getPlant_preventions_before_diseases(val) {
    //console.log(this.plant_preventions_before_diseases);
  }
  getPlant_temparature(val) {
   // console.log(this.plant_temparature);
  }
  getPlant_cropping_timings(val) {
    //console.log(this.plant_cropping_timings);
  }
  getPlant_energy_required(val) {
   // console.log(this.plants_energy_required);
  }
  getPlant_organic_material(val) {
    console.log(this.plant_organic_material);
  }
  getPlant_water_quantity(val) {
    //console.log(this.plant_water_quantity);
  }
  getPlant_without_freeze_time(val) {
    //console.log(this.plant_without_freeze_time);
  }
  getPlant_with_freez_time(val) {
   // console.log(this.plant_with_freez_time);

  }
  getPlant_link(val){
     // var uri = this.plantData.link;
    //var res = encodeURIComponent(uri);
  //   let url2="//www.google.com/search?ei=1ascXM6kFoftvgTG86rIBA&q=info+about+mango&oq=info+about+mango&gs_l=psy-ab.3.0.0l2j0i10i30j0i30l5.38495.47086..47956...1.0..0.222.1452.1j10j1......0....1..gws-wiz.......0i71j0i7i30j0i7i10i30j0i8i7i30j0i13j0i13i10i30j0i13i30.1Tl1bZTttzo"

  //  let url="https://www.google.com/search?ei=1ascXM6kFoftvgTG86rIBA&q=info+about+mango&oq=info+about+mango&gs_l=psy-ab.3.0.0l2j0i10i30j0i30l5.38495.47086..47956...1.0..0.222.1452.1j10j1......0....1..gws-wiz.......0i71j0i7i30j0i7i10i30j0i8i7i30j0i13j0i13i10i30j0i13i30.1Tl1bZTttzo"
   let httpUrl=val.search('http');
   if(httpUrl>=0){
    // var uri = this.plantData.link;
   var res = encodeURIComponent(val);
   let url1="https://opengraph.io/api/1.1/site/"+res+'?app_id=5af190119b03547407c653a0'
  //console.log(httpUrl);
   this.http.get(url1).pipe(map(res => res.json()))
   .subscribe(response => {
    this.imagePrevie=response.hybridGraph;
    this.showLink=false;
   // console.log( this.imagePrevie)
  },
  err => {
  //  console.log(JSON.stringify(err));
 
});
   }
   else{
    this.imagePrevie='';
    this.showLink=true;
   }
  }
  openLink(link){
    const browser = this.iab.create(link);
browser.on('loadstop').subscribe(event => {
   browser.insertCSS({ code: "body{color: red;" });
});

browser.close();
  }
  saveEntry() {
    if (this.formgroup.valid) {
      //alert('ok')
    }
    else {
      //alert('cancel');
    }
  }
  captureImage() {
    let urls;
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
      this.date = new Date().getTime();
      const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      let that;
      that = this;
      let img_url = 'data:image/jpeg;base64,' + imageData;
      fetch(img_url)
        .then(res => res.blob())
        .then(blob => {
          urls = { 'name': 'farmeasy/PlantsData/' + this.date };
          firebase.storage().ref('farmeasy/PlantsData/' + this.date).put(blob).then(function (snapshot) {
            setTimeout(() => {
              firebase.storage().ref(urls.name).getDownloadURL().then(function (url) {
                loading.dismiss();
                const body = { 'type': 'camera', 'url': url, 'path': urls.name };
                that.cameraImage = body;
              });
            }, 1000);
          });
        });
    }, (err) => {
      // Handle error
      alert(err);
    });
  }
  submitPlantDetails() {

    this.submitAttempt = true;
    if (this.formgroup.valid) {
      let plantsData = [];
      let plantsLength: any;
      if (!this.editPlant) {
        let that = this;
        let usersRef = firebase.database().ref('PlantDetails/sheet1');
        usersRef.once("value", function (Data) {
          plantsData = snapshotToArray(Data);
         // console.log(plantsData);
          plantsLength = plantsData.length+1;
          // let plantUdatedLength = plantsData[plantsData.length - 1].s_no;
          that.plantUdatedLength=plantsLength;
         // console.log(that.plantUdatedLength);
          if (plantsData) {
            that.submitData();
          }
        });
      }
      else {
        this.submitData();
      }
    }
    else {

    }
  }
  submitData() {

    let body = {
      "s_no": this.plantUdatedLength,
      "fruits_vegetables": this.plantType,
      "image": this.cameraImage,
      "plant_name": this.plant_name,
      "place_required": this.plant_place_required,
      "depth_length": this.plant_depth_length,
      "life_span": this.plant_life_span,
      "water_quantity": this.plant_water_quantity,
      "organic_material": this.plant_organic_material,
      "energy_required_to_grow": this.plants_energy_required,
      "cropping_timing": this.plant_cropping_timings,
      "diseases_without_chemicals": this.plant_diseases_without_chemicals,
      "temperature": this.plant_temparature,
      "mature_period": this.plant_mature_period,
      "prevention_before_diseases": this.plant_preventions_before_diseases,
      "different_crops": this.plant_cropping_methods,
      "without_freeze_time": this.plant_without_freeze_time,
      "with_freeze_time": this.plant_with_freez_time,
      "link": this.plantLink
    }
    let that=this;
    if (!this.editPlant) {
      let newData = firebase.database().ref('PlantDetails/sheet1').push();
      newData.set(body);
      that.createdPlantDetails();
    }
    if (this.editPlant) {
      let newData = firebase.database().ref('PlantDetails/sheet1/' + this.editPlant.key);
      newData.set(body);
      that.updatedPlantDetails();
    }
  }
  async updatedPlantDetails() {
    const toast = await this.toastController.create({
      
      message: 'Sucessfully updated'+this.plant_name+'details.',
      duration: 2000
    });
    toast.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.navCtrl.navigateRoot('/plants-hystory');
     
    });
    toast.present();
  }

  async createdPlantDetails(){
    const toast = await this.toastController.create({
      message: 'Sucessfully created'+this.plant_name+'details.',
      duration: 2000
    });
    toast.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.navCtrl.navigateRoot('/plants-hystory');
     
    });
    toast.present();

  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
