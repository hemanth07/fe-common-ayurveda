import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ViewPlantDetailsPage } from './view-plant-details.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HttpModule } from '@angular/http';
const routes: Routes = [
  {
    path: '',
    component: ViewPlantDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewPlantDetailsPage]
})
export class ViewPlantDetailsPageModule {}
