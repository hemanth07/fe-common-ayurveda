import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import {Http, Headers, RequestOptions} from '@angular/http';
import { map } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-view-plant-details',
  templateUrl: './view-plant-details.page.html',
  styleUrls: ['./view-plant-details.page.scss'],
})
export class ViewPlantDetailsPage implements OnInit {
  plantData: any;
  headerData: any;
  headerNames: any;
  showSubText: any=[];
  content:any;
  showDownArrow:boolean=true;
  showUpArrow:boolean=false;
  imagePrevie:any=[];
  previewImageData:any=[];
  constructor(private route: ActivatedRoute,
    public navCtrl: NavController,
    private http: Http,
    private iab: InAppBrowser,
    private sg: SimpleGlobal) { }

  ngOnInit() {
    // alert('details');
    let sub = this.route.params.subscribe(params => {
      this.plantData = JSON.parse(params["plantData"]);
     // console.log(this.plantData);
    });
    // this.getPreview();
    this.headerData = { 'title': this.plantData.plant_name,
    button2: 'home',
    button2_action: '/farm-dashboard', 'back': '/plants-hystory', 'color': 'green' };
    this.createHeaderNames();
    this.initilizeData();
   
  }

  createHeaderNames() {
     if(this.sg['userdata'].primary_language === 'en'){
      this.headerNames = [
        { name: 'Depth & Length', "text":this.plantData.depth_length},
        { name: 'Diseases controlling methods without Chemicals', "text":this.plantData.diseases_without_chemicals},
        { name: 'How to yield crop in different ways', "text":this.plantData.different_crops },
        { name: 'Life Span' ,"text":this.plantData.life_span},
        { name: 'Mature Period ' ,"text":this.plantData.mature_period},
        { name: 'Place Required ' ,"text":this.plantData.place_required},
        { name: 'Prevention to take before diseases',"text":this.plantData.prevention_before_diseases },
        { name: 'Temperature ' ,"text":this.plantData.temperature},
        { name: 'Timing of cropping' ,"text":this.plantData.cropping_timing},
        { name: 'Type of energy required to grow' ,"text":this.plantData.energy_required_to_grow},
        { name: 'Organic material Required' ,"text":this.plantData.organic_material},
        { name: 'Water Quantity' ,"text":this.plantData.water_quantity},
        { name: 'Witout freeze time' ,"text":this.plantData.without_freeze_time},
        { name: 'With freeze time' ,"text":this.plantData.with_freeze_time},
        // { name: 'Link' ,"text":this.plantData.link}
      ]
     
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.headerNames = [
        { name: 'ಆಳ ಮತ್ತು ಉದ್ದ', "text":this.plantData.depth_length},
        { name: 'ರಾಸಾಯನಿಕಗಳು ಇಲ್ಲದೆ ವಿಧಾನಗಳನ್ನು ನಿಯಂತ್ರಿಸುವ ರೋಗಗಳು', "text":this.plantData.diseases_without_chemicals},
        { name: 'ವಿವಿಧ ರೀತಿಯಲ್ಲಿ ಬೆಳೆಗಳನ್ನು ಹೇಗೆ ಪಡೆಯುವುದು', "text":this.plantData.different_crops },
        { name: 'ಆಯಸ್ಸು' ,"text":this.plantData.life_span},
        { name: 'ಪ್ರಬುದ್ಧ ಅವಧಿ' ,"text":this.plantData.mature_period},
        { name: 'ಸ್ಥಳಕ್ಕೆ ಅಗತ್ಯವಿದೆ' ,"text":this.plantData.place_required},
        { name: 'ರೋಗಗಳನ್ನು ಎದುರಿಸಲು ತಡೆಗಟ್ಟುವುದು',"text":this.plantData.prevention_before_diseases },
        { name: 'ತಾಪಮಾನ' ,"text":this.plantData.temperature},
        { name: 'ಬೆಳೆದ ಸಮಯ' ,"text":this.plantData.cropping_timing},
        { name: 'ಬೆಳೆಯಲು ಅಗತ್ಯವಾದ ಶಕ್ತಿಯ ಪ್ರಕಾರ' ,"text":this.plantData.energy_required_to_grow},
        { name: 'ಸಾವಯವ ವಸ್ತು ಅಗತ್ಯವಿದೆ' ,"text":this.plantData.organic_material},
        { name: 'ನೀರಿನ ಪ್ರಮಾಣ' ,"text":this.plantData.water_quantity},
        { name: 'ಫ್ರೀಜ್ ಸಮಯವಿಲ್ಲದೆ' ,"text":this.plantData.without_freeze_time},
        { name: 'ಫ್ರೀಜ್ ಸಮಯದೊಂದಿಗೆ' ,"text":this.plantData.with_freeze_time},
        // { name: 'ಲಿಂಕ್' ,"text":this.plantData.link}
      ]

    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.headerNames = [
        { name: 'ஆழம் & நீளம்', "text":this.plantData.depth_length},
        { name: 'இரசாயனங்கள் இல்லாமல் நோய்களைக் கட்டுப்படுத்தும் நோய்கள்', "text":this.plantData.diseases_without_chemicals},
        { name: 'பல்வேறு வழிகளில் பயிர் எவ்வாறு விளைவிக்கின்றது', "text":this.plantData.different_crops },
        { name: 'ஆயுட்காலம்' ,"text":this.plantData.life_span},
        { name: 'முதிர்ந்த காலம்' ,"text":this.plantData.mature_period},
        { name: 'இடம் தேவைப்படுகிறது' ,"text":this.plantData.place_required},
        { name: 'நோய் தடுக்கும் முன் தடுப்பு',"text":this.plantData.prevention_before_diseases },
        { name: 'வெப்ப நிலை' ,"text":this.plantData.temperature},
        { name: 'பயிர் நேரம்' ,"text":this.plantData.cropping_timing},
        { name: 'வளர தேவையான ஆற்றல் வகை' ,"text":this.plantData.energy_required_to_grow},
        { name: 'கரிம பொருள் தேவை' ,"text":this.plantData.organic_material},
        { name: 'நீர் அளவு' ,"text":this.plantData.water_quantity},
        { name: 'முடக்கம் நேரம் இல்லாமல்' ,"text":this.plantData.without_freeze_time},
        { name: 'முடக்கம் நேரம்' ,"text":this.plantData.with_freeze_time},
        // { name: 'இணைப்பு' ,"text":this.plantData.link}
      ]

    } else if(this.sg['userdata'].primary_language === 'te'){
      this.headerNames = [
        { name: 'లోతు & పొడవు', "text":this.plantData.depth_length},
        { name: 'కెమికల్స్ లేకుండా పద్ధతులను నియంత్రించటం', "text":this.plantData.diseases_without_chemicals},
        { name: 'ఎలా వివిధ మార్గాల్లో పంట ఇచ్చుటకు', "text":this.plantData.different_crops },
        { name: 'జీవితకాలం' ,"text":this.plantData.life_span},
        { name: 'పక్వత కాలం' ,"text":this.plantData.mature_period},
        { name: 'స్థలం అవసరం' ,"text":this.plantData.place_required},
        { name: 'వ్యాధుల ముందు నివారణ నివారణ',"text":this.plantData.prevention_before_diseases },
        { name: 'ఉష్ణోగ్రత' ,"text":this.plantData.temperature},
        { name: 'పంట యొక్క సమయం' ,"text":this.plantData.cropping_timing},
        { name: 'పెరగడానికి అవసరమైన శక్తి రకం' ,"text":this.plantData.energy_required_to_grow},
        { name: 'సేంద్రీయ పదార్థం అవసరం' ,"text":this.plantData.organic_material},
        { name: 'నీరు పరిమాణం' ,"text":this.plantData.water_quantity},
        { name: 'స్తంభింప లేకుండా సమయం' ,"text":this.plantData.without_freeze_time},
        { name: 'స్తంభింపచేసిన సమయంతో' ,"text":this.plantData.with_freeze_time},
        // { name: 'లింక్' ,"text":this.plantData.link}
      ]

    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.headerNames = [
        { name: 'गहराई और लंबाई', "text":this.plantData.depth_length},
        { name: 'रसायन के बिना तरीकों को नियंत्रित करने वाले रोग', "text":this.plantData.diseases_without_chemicals},
        { name: 'विभिन्न तरीकों से फसल की पैदावार कैसे करें', "text":this.plantData.different_crops },
        { name: 'जीवनकाल' ,"text":this.plantData.life_span},
        { name: 'परिपक्व काल' ,"text":this.plantData.mature_period},
        { name: 'जगह की आवश्यकता है' ,"text":this.plantData.place_required},
        { name: 'रोगों से पहले लेने के लिए रोकथाम',"text":this.plantData.prevention_before_diseases },
        { name: 'तापमान' ,"text":this.plantData.temperature},
        { name: 'फसल का समय' ,"text":this.plantData.cropping_timing},
        { name: 'बढ़ने के लिए आवश्यक ऊर्जा का प्रकार' ,"text":this.plantData.energy_required_to_grow},
        { name: 'जैविक सामग्री आवश्यक' ,"text":this.plantData.organic_material},
        { name: 'पानी की मात्रा' ,"text":this.plantData.water_quantity},
        { name: 'फ्रीज समय के बिना' ,"text":this.plantData.without_freeze_time},
        { name: 'फ्रीज समय के साथ' ,"text":this.plantData.with_freeze_time},
        // { name: 'संपर्क' ,"text":this.plantData.link}
      ]

    }
    
  }


//   getPreview(){
//     var uri = this.plantData.link;
//     var res = encodeURIComponent(uri);
//    let url="https://opengraph.io/api/1.1/site/"+res+'?app_id=5af190119b03547407c653a0'
//   //  let url="https://opengraph.io/api/1.1/site/ https%3A%2F%2Fstackoverflow.com%2Fquestions%2F29716543%2Fform-validation-using-javascript?app_id=5af190119b03547407c653a0"
  
//    this.http.get(url).pipe(map(res => res.json()))
//    .subscribe(response => {
//     this.imagePrevie=response.hybridGraph;
//    // console.log( this.imagePrevie)
//   },
//   err => {
//    // console.log(JSON.stringify(err));
 
// });
//   }

 
  openLink(link){
    const browser = this.iab.create(link);
browser.on('loadstop').subscribe(event => {
   browser.insertCSS({ code: "body{color: red;" });
});

browser.close();
  }

  initilizeData() {
   
    for (var i = 0; i < this.headerNames.length; i++) {
      this.showSubText[i] = false;
    }
  }
  showData(val, idx) {
   // console.log( this.headerNames);
   // console.log(val + " /" + idx);
    for (var i = 0; i < this.headerNames.length; i++) {
      if (i == idx) {
     
        this.showSubText[i] = true;
        if(val=='Link'){
          this.content='';
          this.previewImageData= this.imagePrevie;
        }
        else{
          this.content=this.headerNames[i].text;
        }
       
      }
      else {
        this.showSubText[i] = false;
      }
    }
  }
  hideData(val,idx){
    this.showSubText[idx]=false;
  }
  editPlant(){
    
    let plantData = { "plantData": JSON.stringify(this.plantData) }
    this.navCtrl.navigateForward(['/add-new-plant', plantData]);
  }
}
