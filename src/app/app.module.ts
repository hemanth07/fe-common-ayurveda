import { IonicGestureConfig } from './ionic-gesture-config';
import { NgModule } from '@angular/core';
import { BrowserModule ,HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule,ReactiveFormsModule,FormGroup, FormArray, FormBuilder,Validators} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { SimpleGlobal } from 'ng2-simple-global';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { Media } from '@ionic-native/media/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { FarmeasyTranslate } from './translate.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Device } from '@ionic-native/device/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
// import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ObservationComponentsModule } from './components/observation-components/observation-components.module';
import { UsercomponentsModule } from './components/usercomponents/usercomponents.module';
import { TaskManagerComponentsModule } from './components/task-manger-components/task-mananger-components.module';
import { ComponentsModule } from './components/components.module';
// import { CalanderPage } from './calander/calander.page';
// import { CalanderPageModule } from './calander/calander.module';
// import { TranslatePipe } from './translate.pipe';
// import { FileChooser } from '@ionic-native/file-chooser/ngx';
// import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { NgCalendarModule  } from 'ionic2-calendar';
import { TimePipe } from './time.pipe';

// IonicModule.forRoot(MyApp,{
//   backButtonText: '',
//   backButtonIcon: 'arrow-back',
//   iconMode: 'md',
//   scrollPadding: false,
//   scrollAssist: true, 
//   autoFocusAssist: true,
//   platforms: {
//     ios: {
//       statusbarPadding: true
//     }
//   },
//   tabsHideOnSubPages: 'true'   
// }),

@NgModule({
  declarations: [AppComponent, TimePipe, ],
  entryComponents: [],
  imports: [BrowserModule,
      CommonModule,
      NgCalendarModule,
      // CalanderPageModule,
      FormsModule,
      ReactiveFormsModule,
      UsercomponentsModule,
      HttpClientModule,
      HttpModule,
      TaskManagerComponentsModule,
      ComponentsModule,
      IonicModule.forRoot(),
      IonicStorageModule.forRoot(),
      IonicModule.forRoot({
        backButtonText: '',
        backButtonIcon: 'md-arrow-back',
        // mode:'md'
        // iconMode: 'md',
        }),
      AppRoutingModule,
      ObservationComponentsModule,
    ],
  providers: [
    StatusBar,
    MediaCapture,
    File,
    InAppBrowser,
    ImagePicker,
    Media,FileOpener,
    Firebase,
    SplashScreen,
    SimpleGlobal,
    DocumentViewer,
    FileTransfer,
    Device,
    AndroidPermissions,
    Diagnostic,
    AppVersion,
    SocialSharing,
    // FileChooser,
    // IOSFilePicker,
    FarmeasyTranslate,
    Camera,
    Geolocation,
    LaunchNavigator,
    Clipboard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
  },
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
