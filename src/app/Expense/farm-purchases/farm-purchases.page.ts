import { Component, OnInit } from "@angular/core";
import { SimpleGlobal } from "ng2-simple-global";
import { Location } from '@angular/common';
import { FarmeasyTranslate } from './../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { HindiService } from './../../languages/hindi.service';
import { EnglishService } from './../../languages/english.service';
import { CalendercomponentComponent } from './../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { PopoverController,AlertController,ToastController } from '@ionic/angular';
import { ReviewpopoverComponent } from './../../components/harvestcomponents/reviewpopover/reviewpopover.component';
import * as firebase from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-farm-purchases',
  templateUrl: './farm-purchases.page.html',
  styleUrls: ['./farm-purchases.page.scss'],
})
export class FarmPurchasesPage implements OnInit {
  headerData: any;
  labels: any = [];
  dataInTime: any;
  uomList: any = [];
  purchase_categories: any = [];
  selectedCategory: any;
  description: any;
  purchase_category: any=[];
  purchaseIn_description: any=[];
  purchase_quantity: any=[];
  purchase_uom: any=[];
  purchase_rate: any = [];
  rate:any;
  uom: any;
  quantity: any;
  subCategory:any=[{name:'',uom:'',quantity:''}];
  subCategories:any=[];
  bill_no:any;
  vendor_name:any;
  total:number;
  view_mode:boolean = false;
  address:any = [];
  edit_flag:boolean = false;
  editContent:any;
  fixed_asset:any;
  constructor(public sg: SimpleGlobal,
    public location:Location,
    private popoverController: PopoverController,
    public alertController:AlertController,
    public toastController:ToastController,
    public translate: FarmeasyTranslate,
    private route: ActivatedRoute,
    private camera: Camera,
    private en: EnglishService,
    private ta: TamilService,
    private te: TeluguService,
    private hi: HindiService,
    private kn: KanadaService) {}

  ngOnInit() {
      // color: green,orange
      let title;

      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Farm Purchases';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಖರೀದಿಗಳು';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை கொள்முதல்';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'వ్యవసాయ కొనుగోళ్లు';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'खेत खरीद';
      }
 
      this.headerData = {
          color: 'pink',
          title: title,
          button1: 'home',
          button1_action: '/farm-dashboard',
          };
          this.subCategories=this.subCategory;
          var current_date = new Date();
          var current_month:any;
          current_month= current_date.getMonth() + 1;
          if(current_month<10){
            current_month='0'+current_month;
          }
          this.dataInTime = current_date.getFullYear() + "-" + current_month + "-" + current_date.getDate()
          this.translation();
          this.harvestdata();
          this.route.params.subscribe(params => {
          this.edit_flag = params['edit'];
          this.editContent = JSON.parse( params['expense']);
          if(this.editContent.key){
            this.dataInTime = this.editContent.date
            this.subCategories = this.editContent.subCategories
            this.address = this.editContent.address?this.editContent.address: []
            this.bill_no = this.editContent.bill_no
            this.vendor_name = this.editContent.vendor
            this.total = this.editContent.amount
            for(let sub of this.subCategories){
             this.purchaseIn_description.push(sub.name)
            this.purchase_quantity.push(sub.quantity)
            this.purchase_uom.push(sub.uom)
            this.purchase_rate.push(parseInt(sub.rate))
            }
          }
          console.log(this.editContent);
          });
    }
    translation() {
      if (this.sg['userdata'].primary_language === 'en') {
        this.labels = {
                       date:"Date of Transaction",
                       bill_no:"Bill No:",
                       vendor_name:"Vendor Name",
                       description:"Description",
                       uom:"UOM",
                       qty:"QYT",
                       rate:"Rate",
                       total:"Total",
                       add_bill:"Add Bill",
                       review:"Review",
                       back:"Back",
                       save:"Save",
                       reconcile:"Reconcile",
                       fixed_assets:"Add To Fixed Asset",
                       added_fixed_assets:"Added to fixed Asset"
                      }
      } else if (this.sg['userdata'].primary_language === 'te') {
        this.labels = {
          "date": "లావాదేవీ తేదీ",
          "bill_no": "బిల్ లేవు:",
          "vendor_name": "విక్రేత యొక్క పేరు",
          "description": "వివరణ",
          "rate":"రేటు",
          "uom": "UOM",
          "qty": "QYT",
          "total": "మొత్తం",
          "add_bill": "బిల్ జోడించండి",
          "review": "సమీక్ష",
          "back": "తిరిగి",
          "save": "సేవ్",
          "reconcile": "పునరుద్దరించటానికి",
          "fixed_assets": "స్థిర ఆస్తి జోడించు",
          "added_fixed_assets": "స్థిర ఆస్తి జోడించబడింది"
        }
      } else if (this.sg['userdata'].primary_language === 'ta') {
        this.labels = {
          "date": "பரிவர்த்தனை தேதி",
          "bill_no": "பில் இல்லை:",
          "vendor_name": "விற்பனையாளர் பெயர்",
          "description": "விளக்கம்",
          "uom": "UOM",
          "rate":"மதிப்பீடு",
          "qty": "QYT",
          "total": "மொத்த",
          "add_bill": "பில் சேர்",
          "review": "விமர்சனம்",
          "back": "மீண்டும்",
          "save": "சேமி",
          "reconcile": "சரிசெய்யும்",
          "fixed_assets": "நிலையான சொத்துக்குச் செல்க சேர்",
          "added_fixed_assets": "நிலையான சொத்தின் சேர்க்கப்பட்டது"
        }
      } else if (this.sg['userdata'].primary_language === 'kn') {
        this.labels = {
          "date": "ಟ್ರಾನ್ಸಾಕ್ಷನ್ ದಿನಾಂಕ",
          "bill_no": "ಬಿಲ್ ಇಲ್ಲ:",
          "vendor_name": "ವೆಂಡರ್ ಹೆಸರು",
          "description": "ವಿವರಣೆ",
          "rate":"ದರ",
          "uom": "UOM",
          "qty": "QYT",
          "total": "ಒಟ್ಟು",
          "add_bill": "ಬಿಲ್ ಸೇರಿಸಿ",
          "review": "ಸಮೀಕ್ಷೆ",
          "back": "ಹಿಂದೆ",
          "save": "ಉಳಿಸಿ",
          "reconcile": "ಸಮನ್ವಯಗೊಳಿಸಲು",
          "fixed_assets": "ಸ್ಥಿರ ಸ್ವತ್ತು ಸೇರಿಸಿ",
          "added_fixed_assets": "ಸ್ಥಿರ ಸ್ವತ್ತಿಗೆ ಸೇರಿಸಲಾಗಿದೆ"
        }
      }else if (this.sg['userdata'].primary_language === 'hi') {
        this.labels = {
          "date": "लेन-देन की तिथि",
          "bill_no": "बिल नहीं:",
          "vendor_name": "विक्रेता का नाम",
          "description": "विवरण",
          "uom": "UOM",
          "rate":"मूल्यांकन करें",
          "qty": "QYT",
          "total": "संपूर्ण",
          "add_bill": "विधेयक में जोड़ें",
          "review": "समीक्षा",
          "back": "वापस",
          "save": "सहेजें",
          "reconcile": "समाधान करना",
          "fixed_assets": "फिक्स्ड एसेट में जोड़ें",
          "added_fixed_assets": "निश्चित परिसंपत्ति में जोड़ा गया"
        }
      } else {
        this.labels = {
          date:"Date of Transaction",
          bill_no:"Bill No:",
          vendor_name:"Vendor Name",
          description:"Description",
          uom:"UOM",
          qty:"QYT",
          rate:"Rate",
          total:"Total",
          add_bill:"Add Bill",
          review:"Review",
          back:"Back",
          save:"Save",
          reconcile:"Reconcile",
          fixed_assets:"Add To Fixed Asset",
          added_fixed_assets:"Added to fixed Asset"
         }
      }
     console.log(this.labels);
    }
    uploadBills() {
      const urls = [];
      this.sg['uploadProgress'] = 1
      this.sg['transferBytes'] = 0
      this.sg['totalBytes'] = 0
      const options: CameraOptions = {
               quality: 100,
               destinationType: this.camera.DestinationType.DATA_URL,
               encodingType: this.camera.EncodingType.JPEG,
               mediaType: this.camera.MediaType.PICTURE,
              //  sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
             }
              this.camera.getPicture(options).then(async (imageData) => {
                const popover = await this.popoverController.create({
              component: ProgressBarComponent,
              translucent: true,
              backdropDismiss:false,
              cssClass:'progress-bar-popover',
              componentProps: {
           'source':'Bills'
          },
            });
        await popover.present();
       popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
                this.sg['currentFile'].cancel();
                return true;
              }
       });
                let that;
                that = this;
                       let img_url  = 'data:image/jpeg;base64,' + imageData;
                       fetch(img_url)
                       .then(res => res.blob())
                       .then(blob => {
                         urls.push({'name' : this.sg['userdata'].vendor_id+'/expenses/images/' + this.dataInTime});
          this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.dataInTime);
                         firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.dataInTime).put(blob).then(function(snapshot) {
                           setTimeout(() => {
                             firebase.storage().ref(urls[0].name).getDownloadURL().then(function(url) {
                              popover.dismiss();
                              that.address[0]= url;
                               });
                           }, 1000);
                         });
         that.zone.run(() => {
            firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.dataInTime).put(blob).on('state_changed', (snapshot) => {
              that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
               that.sg['transferBytes'] = snapshot.bytesTransferred;
              that.sg['totalBytes'] = snapshot.totalBytes;
              // alert(that.sg['uploadProgress']);
              });
            });
  
  
                      });
                     }, (err) => {
                       alert('error');
         });
  
    }
    async openDate() {
      const popover = await this.popoverController.create({
        component: CalendercomponentComponent,
        cssClass: "popoverclass",
      });
      popover.onDidDismiss().then((data) => {
      //  console.log(data);
        this.dataInTime = data.data;
      });
      await popover.present();
  
    }
    add_another_category(){
      this.subCategories.push({name:'',uom:'',quantity:'',rate:''})
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
    }
    async review(){
      let validationerros = [];
      for (let i = 0; i < this.subCategories.length; i++) {
        if (this.subCategories[i]["name"].length == 0) {
          this.subCategories.splice(i, 1);
        }
      }
      if(this.subCategories.length == 0){
        this.add_another_category();
      }
      if (this.subCategories.length == 0) {
        validationerros.push("Fill Records");
       }
        if (!this.bill_no) {
          validationerros.push("Bill No");
        }
        if (!this.vendor_name) {
          validationerros.push("Vendor Name");
        }
        if(!this.dataInTime){
          validationerros.push("Date");
        }
      if (validationerros.length != 0) {
        const alert = await this.alertController.create({
          header: "Please enter mandatory fields",
          message: validationerros.join(","),
          cssClass: "custom-alertDanger",
          buttons: ["OK"]
        });
        await alert.present();
      } else {
        this.view_mode = true;
      }
    }
    back(){
      this.view_mode = false;
    }
    addAnotherRow(idx){
      if(this.subCategories[idx].name.length>0 && this.subCategories[idx].uom.length>0 && this.subCategories[idx].quantity != 0  && this.subCategories[idx].rate != 0 ){
        {
          if(!this.subCategories[idx+1]){
          this.add_another_category();
          }
        }
      }
    }
    harvestdata() {
      firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/purchasein").on('value',resp => {
        this.purchase_categories = snapshotToArray(resp);
      });
      // this.purchase_categories
      //   = [{
      //     "cat_name": "Vegetables",
      //     "cat_id": 0,
  
      //   }, {
      //     "cat_name": "Fruits",
      //     "cat_id": 1,
  
  
      //   }, {
      //     "cat_name": "Crops",
      //     "cat_id": 2,
  
  
      //   }, {
      //     "cat_name": "Greens",
      //     "cat_id": 3,
  
      //   }]
      this.uomList = [{ name: 'Kg' }, { name: 'No' }, { name: 'Ltr' }]
    }
    SelectCategory(val) {
      this.selectedCategory = val;
      
    //  console.log(this.SelectCategory);
    }
    getDescription(val,idx) {
      this.description = val.detail.value;
      this.subCategories[idx].name=this.description;
      this.addAnotherRow(idx);
    }
    selectUom(val,idx) {
      this.uom = val;
      this.subCategories[idx].uom=val;
      this.addAnotherRow(idx);
  
    }
    getQuantity(val,idx) {
      this.quantity = val.detail.value;
      this.subCategories[idx].quantity= this.quantity;
      this.addAnotherRow(idx);
  
    }
   async getRate(val,idx) {
      if(isNaN(val.detail.value)){
      const alert = await this.alertController.create({
          message: "Please enter Numbers",
          buttons: ["OK"]
        });
      await alert.present();
      this.rate = 0;
      this.purchase_rate[idx] = null;
      this.subCategories[idx].rate= null;
      } else {
      this.rate = Number(val.detail.value);
      this.subCategories[idx].rate= this.rate;
      }
      
      this.addAnotherRow(idx);
      this.TotalAmount();
    }
    formSentance(item){
      let sentence = [];
      for(let card of item){
        let name = card.name;
        let uom = card.uom;
        let quant = card.quantity;
        if(uom == "Kg"){
          sentence.push(quant+uom+" "+name);
        } else if(uom == "No"){
          sentence.push(quant+" "+name);
        }else if(uom == "Ltr"){
          sentence.push(quant+uom+" "+name);
        }
      }
      var c = sentence.join(",");
      var idx = c.lastIndexOf(",");
      let last;
      if(idx !== -1){
         last =  c.substr(0,idx)+' and '+c.substr(idx + 1);
      } else {
        last = c;
      }
      return ("Purchased "+last)
     }
    TotalAmount(){
      let count = 0
      for(let item of this.purchase_rate){
          count = count + Number(item);
      }
      this.total = count;
    }
    async save() {
      let harvest_data = {};
      harvest_data = {
        date: this.dataInTime,
        description: this.formSentance(this.subCategories),
        subCategories:this.subCategories,
        address:this.address?this.address:null,
        bill_no:this.bill_no,
        vendor:this.vendor_name,
        amount:this.total,
        status:'Submitted',
        category:"Farm Purchases",
        created_at: new Date().getTime(),
        created_by:{name:this.sg['userdata'].name,
                     phone:this.sg['userdata'].phone,
                     primary_language:this.sg['userdata'].primary_language,
                     role:this.sg['userdata'].role,
                     profile_url:this.sg['userdata'].profile_url
                     }
        
      };
        firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses').push(harvest_data).then(async ()=>{
          const toast = await this.toastController.create({
            message: 'Success',
            duration: 2000
          });
          toast.present();
          this.location.back();
        });
    }
    marksAsFixedAsset(){
      this.fixed_asset = true;
      firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses/'+this.editContent.key).update({fixed_asset:true});
    }
    ValidateExpense(){
      firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses/'+this.editContent.key).update({status:'Reconciled',
      last_update:new Date().getTime(),
      updated_by:{name:this.sg['userdata'].name,
      phone:this.sg['userdata'].phone,
      primary_language:this.sg['userdata'].primary_language,
      role:this.sg['userdata'].role,
      profile_url:this.sg['userdata'].profile_url
      }});
      this.location.back();
    }
  }
  
  export const snapshotToArray = snapshot => {
    let returnArr = [];
    snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });
    return returnArr;
  };
  