import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmPurchasesPage } from './farm-purchases.page';

describe('FarmPurchasesPage', () => {
  let component: FarmPurchasesPage;
  let fixture: ComponentFixture<FarmPurchasesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmPurchasesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmPurchasesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
