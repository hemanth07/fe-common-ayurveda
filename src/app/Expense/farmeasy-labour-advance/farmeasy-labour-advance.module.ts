import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasyLabourAdvancePage } from './farmeasy-labour-advance.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
import { ExpenseComponentsModule } from '../../components/expense-components/expense-components.module'
const routes: Routes = [
  {
    path: '',
    component: FarmeasyLabourAdvancePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ObservationComponentsModule,
    HarvestcomponentsModule,
    ExpenseComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyLabourAdvancePage]
})
export class FarmeasyLabourAdvancePageModule {}
