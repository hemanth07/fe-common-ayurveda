import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavController,ToastController,PopoverController,AlertController} from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { BreakupPaymentComponent } from './../../components/expense-components/breakup-payment/breakup-payment.component';

@Component({
  selector: 'app-farmeasy-labour-advance',
  templateUrl: './farmeasy-labour-advance.page.html',
  styleUrls: ['./farmeasy-labour-advance.page.scss'],
})
export class FarmeasyLabourAdvancePage implements OnInit {
  headerData: any = [];
  labourData: any = [];
  labourNames: any = [];
  category: string = "Loans & Advance";
  expense_category: string;
  labourname:string;
  selected_labour:any;
  month_sal:string;   
  adv:string;
  new_adv:string;
  submit_btn:string;
  date:any  =  new Date().toISOString().split("T")[0];
  isLaboursList:any;
  New_advance:any;
  labour_loan:any = [];
  new_loan_flag:boolean = true;
  advance:any;
  AdvanceList:any = [];
  labour_name_label:any;
  loan:any;
  date_label:any;
  labels:any;
  constructor(public navCtrl: NavController,
              public router: Router,
              private sg: SimpleGlobal,
              private location:Location,
              private translate:FarmeasyTranslate,
              private en: EnglishService,
              private hi: HindiService,
              private ta:TamilService,
              private te:TeluguService,
              private kn:KanadaService,
              public toastController: ToastController,
              private alertController:AlertController,
              public popoverController:PopoverController) {
                  this.createHeader();
                  this.createlabours();
              }
  ngOnInit() {
    this.createHeader();
    this.translations();
    this.createlabours();
  }
  createlabours() {
    this.labourData = [];
    firebase.database().ref(this.sg['userdata'].vendor_id+'/salary').on('value', resp => {
     let  temp = snapshotToArray(resp);
      this.labourNames = temp.filter( labour => {
        if(labour.role !== 'corporate' && labour.role !== 'admin') {
          return true;
        }
      });
      this.translationLabours(this.labourNames);
     });
   }
   translationLabours(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        name_label:item.name,
      }
      menu.push(obj);
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i], 'en',this.sg['userdata'].primary_language).then(data => {
          this.labourNames[i]['name_label'] = data['name_label'];
          });
      } else {
        this.labourNames[i]['name_label'] = menu[i]['name_label'];
      }
    }
  }
 
   showLaboursList(){
    this.isLaboursList=!this.isLaboursList;
  }
  closeList(){
    this.isLaboursList=false;
  }
  getLabour(val) {
    console.log(val);
    this.labourData = [val];
    this.labourname = val.name_label;
    this.selected_labour = val;
    this.isLaboursList=false;
    this.new_loan_flag = true;
    if(this.selected_labour.advance){
     this.AdvanceList =  this.getAdvancesDetails(this.selected_labour.advance);
    } else {
    this.AdvanceList = [];
    }
    if(this.selected_labour.loan){
     this.labour_loan = this.convertObjectToArray(this.selected_labour.loan);
    console.log( this.labour_loan);
    } else {
      this.labour_loan = []
    }
  }
  getAdvancesDetails(data){
    console.log(data);
    let Advances = []
    let temp
    let keys = Object.keys(data);
    keys.forEach( key =>{
      temp = data[key];
      temp['key'] = key;
     Advances.push(temp);
     
    })
    return Advances;
  }
 convertObjectToArray(obj){
    let array1 = Object.entries(obj);
   let original_array = [];
    for(let array2 of array1){
        let obj1 = array2[1];
        obj1['key'] = array2[0]
          if(obj1['balance']>1){
            this.new_loan_flag = false;
          }
          if(obj1['recovered']){
            obj1['recovered'] = this.convertObjectToArray1(obj1['recovered']);
          }
        console.log(obj1);
        original_array.push(obj1);
    }
   console.log(original_array);
   let calaculated_array = this.calaculateLoanBalance(original_array);
   return calaculated_array;
  }
  convertObjectToArray1(obj){
    let array1 = Object.entries(obj);
   let original_array = [];
    for(let array2 of array1){
        let obj1 = array2[1];
        obj1['key'] = array2[0]
           original_array.push(obj1);
     }
   console.log(original_array);
   return original_array;
  }
  calaculateLoanBalance(loan){
    for(let i=0 ;i<loan.length;i++){
      let amount = 0
      if(loan[i].recovered){
        for(let recover of loan[i].recovered){
        amount += parseInt(recover.amount);
        }
      }
      let balance = loan[i].amount-amount
      loan[i]['recover'] = amount;
      loan[i]['balance'] = balance;
    }
    return loan;
  }
  dateConverter(date){
     if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  }
  
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Labour Advance';
      this.labourname = "- Select Labour -"
      // this.labour_name_label = "Labour name"
      // this.loan = 'Loans & Advance'
      // this.date_label = "Date of Payment";
      this.labels = {labourname: "- Select Labour -",
                    labour_name_label :"Labour name",
                    loan :'Loans & Advance',
                    date_label :"Date of Payment",
                    month_sal:"Monthly Sal",
                    prior:"Prior Loan History",
                    date:"Date",
                    adv_paid:"Adv paid",
                    recovered:"Recovered",
                    balance:"Balance",
                    prior_advance_history:"Prior Advance History",
                    advance:"Advance",
                    recover:"Recover",
                    new_loan:"New Loan Paid",
                    new_advance:"New Advance",
                    no_labour:"There is no labour"
                    }
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಲೇಬರ್ ಅಡ್ವಾನ್ಸ್';
      this.labourname = "- ಲೇಬರ್ಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ -"
      // this.labour_name_label = "ಕಾರ್ಮಿಕ ಹೆಸರು"
      // this.loan = 'ಸಾಲ ಮತ್ತು ಮುಂಗಡ'
      // this.date_label = "ಪಾವತಿ ದಿನಾಂಕ";
      this.labels = {labourname: "- ಲೇಬರ್ಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ -",
                    labour_name_label :"ಕಾರ್ಮಿಕ ಹೆಸರು",
                    loan :'ಸಾಲ ಮತ್ತು ಮುಂಗಡ',
                    date_label : "ಪಾವತಿ ದಿನಾಂಕ",
                    month_sal:"ಮಾಸಿಕ ಸಾಲ್",
                    prior:"ಸಾಲದ ಮೊದಲು",
                    date:"ದಿನಾಂಕ",
                    adv_paid:"ಅಡ್ವ್ ಪಾವತಿಸಲಾಗಿದೆ",
                    recovered:"ಚೇತರಿಸಿಕೊಂಡ",
                    balance:"ಸಮತೋಲನ",
                    prior_advance_history:"ಪೂರ್ವ ಮುಂಗಡ ಇತಿಹಾಸ",
                    advance:"ಮುಂಗಡ",
                    recover:"ಗುಣಮುಖರಾಗಲು",
                    new_loan:"ಹೊಸ ಸಾಲ ಪಾವತಿಸಲಾಗಿದೆ",
                    new_advance:"ಹೊಸ ಮುಂಗಡ",
                    no_labour:"ಶ್ರಮವಿಲ್ಲ"
                    }

    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'தொழில் முனைப்பு';
      this.labourname = "- லேபர்களைத் தேர்ந்தெடுக்கவும் -"
      // this.labour_name_label = "தொழிலாளர் பெயர்"
      // this.loan = 'கடன்கள் மற்றும் அட்வான்ஸ்'
      // this.date_label = "பணம் செலுத்திய தேதி";
      this.labels = {labourname: "- லேபர்களைத் தேர்ந்தெடுக்கவும் -",
                    labour_name_label :"தொழிலாளர் பெயர்",
                    loan : 'கடன்கள் மற்றும் அட்வான்ஸ்',
                    date_label :"பணம் செலுத்திய தேதி",
                    month_sal:"மாதாந்திர சால்",
                    prior:"முன் கடன் வரலாறு",
                    date:"தேதி",
                    adv_paid:"அட்வா செலுத்தப்பட்டது",
                    recovered:"மீட்கப்பட்ட",
                    balance:"இருப்பு",
                    prior_advance_history:"முன் அட்வான்ஸ் வரலாறு",
                    advance:"அட்வான்ஸ்",
                    recover:"மீட்டெடு",
                    new_loan:"புதிய கடன் செலுத்தப்பட்டது",
                    new_advance:"புதிய முன்னேற்றம்",
                    no_labour:"உழைப்பு இல்லை"
                    }

    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'లేబర్ అడ్వాన్స్';
      this.labourname = "- లేబర్స్ ఎంచుకోండి -"
      // this.labour_name_label = "కార్మిక పేరు"
      // this.loan ='లోన్స్ & అడ్వాన్స్'
      // this.date_label = "చెల్లింపు తేదీ";
      this.labels = {labourname: "- లేబర్స్ ఎంచుకోండి -",
                    labour_name_label :"కార్మిక పేరు",
                    loan :'లోన్స్ & అడ్వాన్స్',
                    date_label :"చెల్లింపు తేదీ",
                    month_sal:"మంత్లీ సాల్",
                    prior:"ముందు రుణ చరిత్ర",
                    date:"తేదీ",
                    adv_paid:"అడ్వాన్స్ చెల్లించారు",
                    recovered:"కోలుకున్న",
                    balance:"సంతులనం",
                    prior_advance_history:"ముందు పురోగతి చరిత్ర",
                    advance:"అడ్వాన్స్",
                    recover:"పునరుద్ధరించు",
                    new_loan:"కొత్త రుణ చెల్లింపు",
                    new_advance:"కొత్త అడ్వాన్స్",
                    no_labour:"శ్రమ లేదు"
                    }

    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'श्रम अग्रिम';
      this.labourname = "- मजदूरों का चयन करें -"
      // this.labour_name_label = "श्रम का नाम"
      // this.loan = 'ऋण और अग्रिम'
      // this.date_label = "भुगतान की तिथि";
      this.labels = {labourname: "- मजदूरों का चयन करें -",
                    labour_name_label :"श्रम का नाम",
                    loan : 'ऋण और अग्रिम',
                    date_label :"भुगतान की तिथि",
                    month_sal:"मासिक वेतन",
                    prior:"पूर्व ऋण इतिहास",
                    date:"दिनांक",
                    adv_paid:"भुगतान किया गया",
                    recovered:"बरामद",
                    balance:"संतुलन",
                    prior_advance_history:"पूर्व अग्रिम इतिहास",
                    advance:"अग्रिम",
                    recover:"वसूली",
                    new_loan:"नया ऋण भुगतान किया",
                    new_advance:"नया एडवांस",
                    no_labour:"कोई श्रम नहीं है"
                    }


    }
    this.headerData = {
          color: 'blue',
          title: title,
          button1: 'home',
          button1_action: '/farm-dashboard',
          present : 'farmeasy-labour-advance',
          back: '/farmeasy-expense-manager'
          };
  }
  translations() {
    let data;
    if(this.sg['userdata'].primary_language === 'en'){
      data  = this.en.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data  = this.te.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data  = this.ta.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data  = this.kn.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data  = this.hi.getLabourAdvance();
    } else {
      data  = this.en.getLabourAdvance();
    }
      this.expense_category = data[0];
      // this.labourname = data[1];
      this.month_sal = data[2];
      this.adv = data[3];
      this.new_adv = data[4];
      this.submit_btn = data[5];
  }
  async pickDate() {
    const popover = await this.popoverController.create({
         component: CalendercomponentComponent,
         cssClass: 'popoverclass',
     });
     popover.onDidDismiss().then((data) => {
          this.date = data.data;
     });
     await popover.present();
}
async viewBreakUp(idx){
  let loan = this.labour_loan[idx]
  loan['name'] = this.selected_labour.name;
  loan['role'] = this.selected_labour.role;
  loan['profile_url'] = this.selected_labour.profile_url;
  const popover = await this.popoverController.create({
  component: BreakupPaymentComponent,
  cssClass: 'popoverclass',
  componentProps: {
        message:loan
      }
  });
  popover.onDidDismiss().then((data) => {
    
  });
  await popover.present();
}
recovered(data,idx){
console.log(data);
firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+this.selected_labour.key+'/advance/'+data.key).update({status:'recovered'});
this.AdvanceList[idx]['status'] = 'recovered';
}
recover(data,idx){
  console.log(data);
  firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+this.selected_labour.key+'/advance/'+data.key).update({status:'recover'});
  this.AdvanceList[idx]['status'] = 'recover';
}
async validateLoan(event){
if(isNaN(this.New_advance)){
  const alert = await this.alertController.create({
    message: "Please enter Numbers",
    buttons: ["OK"]
  });
  await alert.present();
  this.New_advance = null;
}
}
async validateAdvance(event){
  if(isNaN(this.advance)){
    const alert = await this.alertController.create({
      message: "Please enter Numbers",
      buttons: ["OK"]
    });
    await alert.present();
    this.advance = null;
  }
}
  async submit() {
    if(this.New_advance){
      this.New_advance = Number(this.New_advance);
      let errors = []
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      let mon = new Date().getMonth()+1;
       let year = new Date().getFullYear().toString().substr(2, 2);
      if(mon == 12){
        mon = 1;
        year = (new Date().getFullYear()+1).toString().substr(2, 2);
      }
      let month = months[mon];
      let key = month+"-"+year;
      let date = new Date(this.date).toISOString().split("T")[0];
       firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+this.selected_labour.key+'/loan').push({date:date,balance:this.New_advance,amount:this.New_advance,loan_no:this.labour_loan.length+1});
      if(this.labourData.length>0){
        const toast = await this.toastController.create({
          message: 'Labours Advances Updated',
          duration: 2000
        });
        toast.present();
         this.location.back();
      } else {
        this.createlabours()
      }
    } 
    if(this.advance) {
      this.advance = Number(this.advance);
      let errors = []
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      let mon = new Date().getMonth()+1;
       let year = new Date().getFullYear().toString().substr(2, 2);
      if(mon == 12){
        mon = 1;
        year = (new Date().getFullYear()+1).toString().substr(2, 2);
      }
      let month = months[mon];
      let key = month+"-"+year;
      let date = new Date(this.date).toISOString().split("T")[0];
       firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+this.selected_labour.key+'/advance').push({date:date,amount:this.advance,status:'recover'});
      if(this.labourData.length>0){
        const toast = await this.toastController.create({
          message: 'Labours Advances Updated',
          duration: 2000
        });
        toast.present();
         this.location.back();
      } else {
        this.createlabours()
      }
    }
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
  };

  export const objectToArray = snapshot => {
    const returnArr = [];
    snapshot.forEach(childSnapshot => {
      const item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });
    return returnArr;
    };