import { OnInit, Component, ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';
import { NavController } from '@ionic/angular';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { Router, Route } from '@angular/router';
@Component({
  selector: 'app-farmeasy-expense-manager',
  templateUrl: './farmeasy-expense-manager.page.html',
  styleUrls: ['./farmeasy-expense-manager.page.scss'],
})
export class FarmeasyExpenseManagerPage implements OnInit {
  expensemanager: any = [];
  headerData: any = [];
  translateData:any = [];
  constructor(private navCtrl: NavController,
    private sg: SimpleGlobal,
    private router: Router,
    private translate:FarmeasyTranslate,
    private resolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService
    ) { }

  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Expenses & Purchases';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿಗಳು';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'செலவுகள் மற்றும் கொள்முதல்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'ఖర్చులు & కొనుగోళ్లు';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'व्यय और खरीद';
    } 
    this.headerData = {
        color: 'orange',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        // button2: 'notifications',
        // button2_action: '/notifications',
        // notification_count: this.sg['notificationscount'],
        };
  }
  translation(){
  if(this.sg['userdata'].primary_language === 'en'){
    this.expensemanager  = this.en.getExpense();
  } else if(this.sg['userdata'].primary_language === 'te'){
    this.expensemanager  = this.te.getExpense();
  } else if(this.sg['userdata'].primary_language === 'ta'){
    this.expensemanager  = this.ta.getExpense();
  } else if(this.sg['userdata'].primary_language === 'kn'){
    this.expensemanager  = this.kn.getExpense();
  } else if(this.sg['userdata'].primary_language === 'hi'){
    this.expensemanager  = this.hi.getExpense();
  } else {
     this.expensemanager  = this.en.getExpense();
  }
}
selectedTask(task) {
  this.sg['expense_item'] = task.keyword;
  this.navCtrl.navigateForward(task.action);
}

}

