import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasyExpenseManagerPage } from './farmeasy-expense-manager.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
const routes: Routes = [
  {
    path: '',
    component: FarmeasyExpenseManagerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ObservationComponentsModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyExpenseManagerPage]
})
export class FarmeasyExpenseManagerPageModule {}
