import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { FarmeasyTranslate } from '../../translate.service';

@Component({
  selector: 'app-farm-purchases-list',
  templateUrl: './farm-purchases-list.page.html',
  styleUrls: ['./farm-purchases-list.page.scss'],
})
export class FarmPurchasesListPage implements OnInit {
  headerData: any;
  expenseList: any = [];
  validcolor:string = '#00ccff'
  page:any;
  no_data:any;
  constructor(private sg: SimpleGlobal, public translate: FarmeasyTranslate,) { }

  ngOnInit() {
    this.page = this.sg['expense_item'];
    this.createHeader();
    this.getExpense();
  }
    createHeader() {
    // color: green,orange
    let title;
    if(this.page == 'Daily Expenses'){
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Daily Expenses';
        this.no_data ="No Daily Expenses";
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ದಿನನಿತ್ಯದ ವೆಚ್ಚ ';
        this.no_data ="ದೈನಂದಿನ ವೆಚ್ಚಗಳಿಲ್ಲ";

      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'தினசரி செலவு ';
        this.no_data ="தினசரி செலவுகள் இல்லை";

      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'డైలీ ఖర్చు ';
        this.no_data ="రోజువారీ ఖర్చులు లేవు";

      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'दैनिक व्यय ';
        this.no_data ="कोई दैनिक खर्च नहीं";

      }
    } else if(this.page == 'Farm Purchases'){
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Farm Purchases';
        this.no_data ="No Farm Purchases";
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಖರೀದಿಗಳು';
        this.no_data ="ಕೃಷಿ ಖರೀದಿಗಳಿಲ್ಲ";
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை கொள்முதல்';
        this.no_data ="பண்ணை கொள்முதல் இல்லை";
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'వ్యవసాయ కొనుగోళ్లు';
        this.no_data ="వ్యవసాయ కొనుగోళ్లు లేవు";
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'खेत खरीद';
        this.no_data ="कोई खेत खरीद नहीं";
      }
    }
  
    this.headerData = {
        color: 'orange',
        title: title,
        button1: 'add-circle-outline',
        button1_action: '/farm-purchases',
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
  }
  getExpense(){
      firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses').on('value',resp=>{
        let temp = snapshotToArray(resp).reverse();
         // temp.map((item,i)=>temp[i].date = temp[i].date.split("-").reverse().join("-"));
        // temp.map((item,i)=> { temp[i]['amount'] = parseInt(temp[i]['amount']).toLocaleString();});
        let temp1;
        if(this.page == 'Daily Expenses'){
           temp1 = temp.filter((item)=>item.category !== 'Farm Purchases');
        } else if(this.page == 'Farm Purchases'){
           temp1 = temp.filter((item)=>item.category === 'Farm Purchases');
        }
        if(this.sg['userdata'].role === 'corporate' || this.sg['userdata'].role === 'accounts' || this.sg['userdata'].role === 'manager'){
          this.expenseList = temp1;
          console.log(this.expenseList);
        } else {
          this.expenseList = temp1.filter((resp)=>{
            if(resp.created_by.phone === this.sg['userdata'].phone){
              return true
            } 
          })
        }
       this.translation(this.expenseList);
      });
    }
    translation(data){
      let temp = data
      let menu = [];
      for(let item of data){
        let obj = {
          category:item.category,
          description:item.description,
          status:item.status.toLowerCase(),
          name:item.created_by.name
          }
        menu.push(obj)
      }
      for(let i=0;i<menu.length;i++){
        if(this.sg['userdata'].primary_language !== 'en'){
          this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
            this.expenseList[i].category = data['category'];
            this.expenseList[i].description = data['description'];
            this.expenseList[i]['status_label'] = data['status'];
            this.expenseList[i]['name'] = data['name'];
            });
        } else {
          this.expenseList[i].category = menu[i]['category'];
          this.expenseList[i].description = menu[i]['description'];
          this.expenseList[i]['status_label'] = menu[i]['status'];
          this.expenseList[i]['name'] = menu[i]['name'];
        }
      }
    }
    commaSeparater(amount){
      if(amount>999){
      return amount.toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
      } else {
        return amount
      }
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
    }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};