import { Location } from '@angular/common';
import { Component, OnInit, ViewChild ,NgZone } from '@angular/core';
import * as firebase from 'firebase';
import { ImagePicker,   ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { NavController, PopoverController, LoadingController,AlertController} from '@ionic/angular';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-farmeasy-daily-expense',
  templateUrl: './farmeasy-daily-expense.page.html',
  styleUrls: ['./farmeasy-daily-expense.page.scss'],
})
export class FarmeasyDailyExpensePage implements OnInit {

  headerData: any = [];
  category: string = 'Daily Expense';
  description: string;
  amount: number;
  date: string;
  justification: string;
  address: any = [];
  today: any;
  expense_category: string;
  expense_description: string;
  amountLabel: string;
  date_of_expense: string;
  justificationLabel: string;
  submit_btn: string;
  justification_place: string;
  editView:String;
  editContent:any;
  page:any;
  label:any = {}
  options:any = [];
  @ViewChild('datePicker') datePicker;
  constructor(public loadingCtrl: LoadingController,
              private alertController:AlertController,
              private route: ActivatedRoute,
              public router: Router,
              private imagePicker: ImagePicker,
              private camera: Camera,
              private popoverController: PopoverController,
              private zone:NgZone,
              private translate:FarmeasyTranslate,
              public navCtrl: NavController,
              private location:Location,
              private sg: SimpleGlobal,
              private en: EnglishService,
              private hi: HindiService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService) { }

  ngOnInit() {
    this.page = this.sg['expense_item'];
    this.createHeader();
    this.translations();
    this.date = new Date().toJSON().split('T')[0];
    this.route.params.subscribe(params => {
      this.editView = params['edit'];
      let temp = JSON.parse( params['expense']);
      this.editContent = temp;
      this.category =temp.category;
      this.description = temp.description;
      this.amount = temp.amount;
      this.date = temp.date;
      this.justification = temp.justification;
      this.address = temp.address;
      if(typeof this.address == 'undefined'){
        this.address = [];
      }
      });
  }
  createHeader() {
    // color: green,orange
    let title;
    if(this.page == 'Daily Expenses'){
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Daily expenses at Farm';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ನಲ್ಲಿ ದೈನಂದಿನ ವೆಚ್ಚಗಳು';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணையில் தினசரி செலவுகள்';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'పొలంలో రోజువారీ ఖర్చులు';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'फार्म में दैनिक खर्च';
      }
    } else if(this.page == 'Farm Purchases'){
      this.category  = 'Farm Purchases';
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Farm Purchases';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಖರೀದಿಗಳು';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை கொள்முதல்';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'వ్యవసాయ కొనుగోళ్లు';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'खेत खरीद';
      }
    }
    this.headerData = {
        color: 'orange',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        present : 'farmeasy-daily-expense',
        back: '/farmeasy-expense-manager'
        };
    this.prepareOptions();
  }
  async validateAmount(event){
    console.log(event.detail.value.length);
    if(isNaN(event.detail.value)){
      const alert = await this.alertController.create({
        message: "Please enter Numbers",
        buttons: ["OK"]
      });
      await alert.present();
      this.amount = null;
    }
    
  }
  prepareOptions(){

    if(this.sg['userdata'].primary_language === 'en'){
      this.label['validate'] = 'Validate';
      this.label['cancel'] = 'Cancel';
      this.options = [
        {label:'Food Expenses',value:"Food Expenses"},
        // {label:'Farm Purchases',value:"Farm Purchases"},
        {label:'Repair and Maintanance',value:"Repair and Maintanance"},
        {label:'Vehicle Maintanance',value:"Vehicle Maintanance"},
        {label:'Live Stock Expenses',value:"Live Stock Expenses"},
        {label:'Fuel Expenses',value:"Fuel Expenses"},
        {label:'Statutory Expenses',value:"Statutory Expenses"},
        {label:'Other Expenses',value:"Other Expenses"},
        {label:'Transportation',value:"Transportation"}
      ];

    } else if(this.sg['userdata'].primary_language === 'kn'){

      this.label['validate'] = 'ಮೌಲ್ಯೀಕರಿಸಿ';
      this.label['cancel'] = 'ರದ್ದುಮಾಡಿ';
      this.options =[
        {label:'Food Expenses',value:"ಆಹಾರ ವೆಚ್ಚಗಳು"},
        // {label:'Farm Purchases',value:"ಕೃಷಿ ಖರೀದಿಗಳು"},
        {label:'Repair and Maintanance',value:"ದುರಸ್ತಿ ಮತ್ತು ನಿರ್ವಹಣೆ"},
        {label:'Vehicle Maintanance',value:"ವಾಹನ ನಿರ್ವಹಣೆ"},
        {label:'Live Stock Expenses',value:"ಲೈವ್ ಸ್ಟಾಕ್ ವೆಚ್ಚಗಳು"},
        {label:'Fuel Expenses',value:"ಇಂಧನ ವೆಚ್ಚಗಳು"},
        {label:'Statutory Expenses',value:"ಶಾಸನಬದ್ಧ ವೆಚ್ಚಗಳು"},
        {label:'Other Expenses',value:"ಇತರ ವೆಚ್ಚಗಳು"},
        {label:'Transportation',value:"ಸಾರಿಗೆ"}
      ];

    } else if(this.sg['userdata'].primary_language === 'ta'){

      this.label['validate'] = 'சரிபார்க்கவும்';
      this.label['cancel'] = 'ரத்து';
      this.options = [
        {label:'Food Expenses',value:"உணவு செலவுகள்"},
        // {label:'Farm Purchases',value:"பண்ணை கொள்முதல்"},
        {label:'Repair and Maintanance',value:"பழுதுபார்ப்பு மற்றும் பராமரிப்பு"},
        {label:'Vehicle Maintanance',value:"வாகன பராமரிப்பு"},
        {label:'Live Stock Expenses',value:"நேரடி பங்கு செலவுகள்"},
        {label:'Fuel Expenses',value:"எரிபொருள் செலவுகள்"},
        {label:'Statutory Expenses',value:"சட்டரீதியான செலவுகள்"},
        {label:'Other Expenses',value:"பிற செலவுகள்"},
        {label:'Transportation',value:"போக்குவரத்து"}
      ]

    } else if(this.sg['userdata'].primary_language === 'te'){

      this.label['validate'] = 'ప్రమాణీకరించు';
      this.label['cancel'] = 'రద్దు';
      this.options = [
        {label:'Food Expenses',value:"ఆహార ఖర్చులు"},
        // {label:'Farm Purchases',value:"వ్యవసాయ కొనుగోళ్లు"},
        {label:'Repair and Maintanance',value:"మరమ్మత్తు మరియు నిర్వహణ"},
        {label:'Vehicle Maintanance',value:"వాహన నిర్వహణ"},
        {label:'Live Stock Expenses',value:"ప్రత్యక్ష స్టాక్ ఖర్చులు"},
        {label:'Fuel Expenses',value:"ఇంధన ఖర్చులు"},
        {label:'Statutory Expenses',value:"చట్టబద్ధమైన ఖర్చులు"},
        {label:'Other Expenses',value:"ఇతర ఖర్చులు"},
        {label:'Transportation',value:"రవాణా"}
      ];

    } else if(this.sg['userdata'].primary_language === 'hi'){

      this.label['validate'] = 'मान्य करें';
      this.label['cancel'] = 'रद्द करना';
      this.options = [
        {label:'Food Expenses',value:"खाद्य व्यय"},
        // {label:'Farm Purchases',value:"खेत खरीद"},
        {label:'Repair and Maintanance',value:"मरम्मत और देखभाल"},
        {label:'Vehicle Maintanance',value:"वाहन मनन"},
        {label:'Live Stock Expenses',value:"लाइव स्टॉक खर्च"},
        {label:'Fuel Expenses',value:"ईंधन खर्च"},
        {label:'Statutory Expenses',value:"वैधानिक व्यय"},
        {label:'Other Expenses',value:"अन्य खर्चे"},
        {label:'Transportation',value:"परिवहन"}
      ];

    }

  }
  translations(){
    let data;
    console.log(this.sg['userdata']);
    if(this.sg['userdata'].primary_language === 'en'){
      data  = this.en.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data  = this.te.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data  = this.ta.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data  = this.kn.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data  = this.hi.getDailyExpense();
    }else {
      data  = this.en.getDailyExpense();
    }
    this.expense_category = data[0];
    this.expense_description = data[1];
    this.amountLabel = data[2];
    this.date_of_expense = data[3];
    this.justificationLabel = data[4];
    this.submit_btn = data[5];
    this.justification_place = data[6];
  }
  async pickDate() {
       const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
          //  console.log(data);
             this.date = data.data;
        });
        await popover.present();
  }
  uploadBills() {
    const urls = [];
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const options: CameraOptions = { 
             quality: 100,
             destinationType: this.camera.DestinationType.DATA_URL,
             encodingType: this.camera.EncodingType.JPEG,
             mediaType: this.camera.MediaType.PICTURE,
            //  sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
           }
            this.camera.getPicture(options).then(async (imageData) => {
              const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Bills'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
              let that;
              that = this;
                     let img_url  = 'data:image/jpeg;base64,' + imageData;
                     fetch(img_url)
                     .then(res => res.blob())
                     .then(blob => {
                       urls.push({'name' : this.sg['userdata'].vendor_id+'/expenses/images/' + this.today});
        this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.today);
                       firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.today).put(blob).then(function(snapshot) {
                         setTimeout(() => {
                           firebase.storage().ref(urls[0].name).getDownloadURL().then(function(url) {
                            popover.dismiss();
                            that.address[0]= url;
                             });
                         }, 1000);
                       });
       that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.today).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });


                    });
                   }, (err) => {
                     alert('error');
       });

  }
async submitDailyExpense() {
    let validationerros = [];
      if (!this.category) {
        validationerros.push("Category");
      }
      if (!this.amount) {
        validationerros.push("Amount");
      }
      if (!this.category) {
        validationerros.push("Date");
      }
      if (!this.description) {
        validationerros.push("Description");
      }
    if (validationerros.length != 0) {
      const alert = await this.alertController.create({
        header: "Please enter mandatory fields",
        message: validationerros.join(","),
        cssClass: "custom-alertDanger",
        buttons: ["OK"]
      });
      await alert.present();
    } else {
      const body = {
        'category': this.category,
        'description': this.description,
        'amount': this.amount,
        'date': this.date,
        'address': this.address,
        status:'Submitted',
        created_at: new Date().getTime(),
         created_by:{name:this.sg['userdata'].name,
                      phone:this.sg['userdata'].phone,
                      primary_language:this.sg['userdata'].primary_language,
                      role:this.sg['userdata'].role,
                      profile_url:this.sg['userdata'].profile_url
                      }
  
      };
      firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses').push(body);
        this.location.back();
    }
  }
  dateConverter(date){
    if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  
  }
  ValidateExpense(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses/'+this.editContent.key).update({status:'Validated'});
    this.location.back();
  }
  CancelExpense(){
    this.location.back();
  }

}
