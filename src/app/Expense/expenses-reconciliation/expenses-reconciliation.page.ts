import { PopoverController } from '@ionic/angular';
import { Component, OnInit, NgZone } from '@angular/core';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import {SimpleGlobal} from 'ng2-simple-global';
import { OverlayEventDetail } from '@ionic/core';
import * as firebase from 'firebase';
import { NavController, LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-expenses-reconciliation',
  templateUrl: './expenses-reconciliation.page.html',
  styleUrls: ['./expenses-reconciliation.page.scss'],
})
export class ExpensesReconciliationPage implements OnInit {
  headerData:any;
  fromOrTodate:any;
  toDate:any;
  fromDate:any;
  color:any = '#2174E1';
  expenseList:any = [];
  TempexpenseList:any = [];
  count:number = 10;
  previousLen:number = 0;
  displayExpenseList:any = []
  constructor(private sg: SimpleGlobal,
              private popoverController:PopoverController,
              public navCtrl: NavController,
              private ngZone: NgZone,
              public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.createHeader();
    this.getExpense();
  }
  createHeader() {
    // color: green,orange
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Expenses Reconciliation';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ವೆಚ್ಚ ಸಾಮರಸ್ಯ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'செலவுகள் மீளுருவாக்கம்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'ఖర్చులు సయోధ్య';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'खर्चों का सामंजस्य';
    }
    this.headerData = {
        color: 'blue',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        present : 'expenses-reconciliation',
        back: '/farmeasy-expense-manager'
        };
  }
  dateConverter(date){
     if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  }
  async openDate(val) {
    this.fromOrTodate = val;
    let popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popover_class",
      componentProps: {'previous': true},
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getSelectedDate(detail.data);
    });
    await popover.present();
  }
  getSelectedDate(val) {
    console.log(this.fromOrTodate);
    if (this.fromOrTodate == 'toDate') {
      this.toDate = val;
      console.log(this.toDate);
    }
    if (this.fromOrTodate == 'fromDate') {
      this.fromDate = val;
      console.log(this.fromDate);
    }
  }
 async getExpense(){
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses').on('value',resp=>{
        let temp = snapshotToArray(resp).reverse();
        this.displayExpenseList = [];
       // temp.map((item,i)=>temp[i].date = temp[i].date.split("-").reverse().join("-"));
        temp.map((item,i)=> { temp[i]['time'] = new Date(temp[i].date).getTime()});
        // temp.map((item,i)=> { temp[i]['amount'] = parseInt(temp[i]['amount']).toLocaleString();});
        if(this.sg['userdata'].role === 'corporate' || this.sg['userdata'].role === 'accounts' || this.sg['userdata'].role === 'manager'){
          this.expenseList = temp;
          this.TempexpenseList = temp;
          console.log(this.expenseList);
        } else {
          this.expenseList = temp.filter((resp)=>{
            if(resp.created_by.phone === this.sg['userdata'].phone){
              return true
            } 
          })
          this.TempexpenseList = this.expenseList;
        }
        this.ngZone.run( () => {
          this.count = this.expenseList.length<10?this.expenseList.length:10;
          for(let i=0;i<this.count;i++){
            this.displayExpenseList.push(this.expenseList[i]);
          }
       });

        loading.dismiss();
      });
    }
 searchData(val) {
   if(this.toDate && this.fromDate){
     let toDate = new Date(this.toDate).getTime();
     let fromDate = new Date(this.fromDate).getTime();
     this.expenseList = this.TempexpenseList.filter((item)=> item.time>=fromDate && item.time<=toDate);
     this.displayExpenseList = [];
      this.count = this.expenseList.length<10?this.expenseList.length:10;
      for(let i=0;i<this.count;i++){
        this.displayExpenseList.push(this.expenseList[i]);
      }
   }
 }
commaSeparater(amount){
  if(amount>999){
    return amount.toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
    } else {
      return amount
    }
  // return amount.replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
}
navigateToSpecific(expense){
console.log(expense);
if(expense.category == "Farm Purchases"){
  const data = {edit: "true", expense: JSON.stringify(expense)};
  this.navCtrl.navigateForward(['/farm-purchases', data]);
} else {
  const data = {edit: "true", expense: JSON.stringify(expense)};
  this.navCtrl.navigateForward(['/daily-expense-reconciliation', data]);
}

}

}

export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};