import { Location } from '@angular/common';
import { Component, OnInit, ViewChild,NgZone } from '@angular/core';
import * as firebase from 'firebase';
import { ImagePicker,   ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { NavController, PopoverController, LoadingController,AlertController} from '@ionic/angular';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';
@Component({
  selector: 'app-daily-expense-reconciliation',
  templateUrl: './daily-expense-reconciliation.page.html',
  styleUrls: ['./daily-expense-reconciliation.page.scss'],
})
export class DailyExpenseReconciliationPage implements OnInit {

  headerData: any = [];
  category: string;
  description: string;
  amount: string;
  date: string;
  justification: string;
  address: any = [];
  today: any;
  expense_category: string;
  expense_description: string;
  amountLabel: string;
  date_of_expense: string;
  justificationLabel: string;
  submit_btn: string;
  justification_place: string;
  editView:String;
  editContent:any;
  fixed_asset:boolean;
  expense:any;
  @ViewChild('datePicker') datePicker;
  constructor(public loadingCtrl: LoadingController,
              private alertController:AlertController,
              private route: ActivatedRoute,
              public router: Router,
              private imagePicker: ImagePicker,
              private camera: Camera,
              private popoverController: PopoverController,
              private translate:FarmeasyTranslate,
              public navCtrl: NavController,
              private zone:NgZone,
              private location:Location,
              private sg: SimpleGlobal,
              private en: EnglishService,
              private hi: HindiService,
              private ta:TamilService,
              private te:TeluguService,
              private kn:KanadaService ) { }

  ngOnInit() {
    
    this.createHeader();
    this.translations();
    this.date = new Date().toJSON().split('T')[0];
    this.route.params.subscribe(params => {
      this.editView = params['edit'];
      let temp = JSON.parse( params['expense']);
      this.expense = temp;
      console.log(temp);
      this.editContent = temp;
      this.category =temp.category;
      this.description = temp.description;
      this.amount = temp.amount;
      this.date = temp.date;
      this.justification = temp.justification;
      this.address = temp.address;
      this.fixed_asset = temp.fixed_asset ? temp.fixed_asset : false;
      if(typeof this.address == 'undefined'){
        this.address = [];
      }
      });
  }
  createHeader() {
    // color: green,orange
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Daily Expenses Reconiliation';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ದೈನಂದಿನ ವೆಚ್ಚಗಳ ಸಾಮರಸ್ಯ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'தினசரி செலவுகள் நல்லிணக்கம்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'రోజువారీ ఖర్చులు సయోధ్య';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'दैनिक खर्चों का सामंजस्य';
    }
    this.headerData = {
        color: 'blue',
        title: title,
        button1: 'home',
        button1_action: '/farm-dashboard',
        };
  }
  translations(){
    let data;
    console.log(this.sg['userdata']);
    if(this.sg['userdata'].primary_language === 'en'){
      data  = this.en.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data  = this.te.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data  = this.ta.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data  = this.kn.getDailyExpense();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data  = this.hi.getDailyExpense();
    }else {
      data  = this.en.getDailyExpense();
    }
    this.expense_category = data[0];
    this.expense_description = data[1];
    this.amountLabel = data[2];
    this.date_of_expense = data[3];
    this.justificationLabel = data[4];
    this.submit_btn = data[5];
    this.justification_place = data[6];
  }
  async pickDate() {
       const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            cssClass: 'popoverclass',
        });
        popover.onDidDismiss().then((data) => {
          //  console.log(data);
             this.date = data.data;
        });
        await popover.present();
  }
  changeAmount(event){
    console.log(event.target.value);
  }
  uploadBills() {
    const urls = [];
    const options: CameraOptions = {
             quality: 100,
             destinationType: this.camera.DestinationType.DATA_URL,
             encodingType: this.camera.EncodingType.JPEG,
             mediaType: this.camera.MediaType.PICTURE,
            //  sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
           }
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
            this.camera.getPicture(options).then(async (imageData) => {
             const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Bills'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
              let that;
              that = this;
                     let img_url  = 'data:image/jpeg;base64,' + imageData;
                     fetch(img_url)
                     .then(res => res.blob())
                     .then(blob => {
                       urls.push({'name' : this.sg['userdata'].vendor_id+'/expenses/images/' + this.today});
        this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.today);
                       firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.today).put(blob).then(function(snapshot) {
                         setTimeout(() => {
                           firebase.storage().ref(urls[0].name).getDownloadURL().then(function(url) {
                            popover.dismiss();
                            that.address[0]= url;
                             });
                         }, 1000);
                       });
                               that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/expenses/images/' + this.today).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                    });
                   }, (err) => {
                     alert('error');
       });

  }
async Reconciled() {
    let validationerros = [];
      if (!this.category) {
        validationerros.push("Category");
      }
      if (!this.amount) {
        validationerros.push("Amount");
      }
      if (!this.category) {
        validationerros.push("Date");
      }
      if (!this.description) {
        validationerros.push("Description");
      }
    if (validationerros.length != 0) {
      const alert = await this.alertController.create({
        header: "Please enter mandatory fields",
        message: validationerros.join(","),
        cssClass: "custom-alertDanger",
        buttons: ["OK"]
      });
      await alert.present();
    } else {
      let body = {...this.expense};
      body['amount'] = this.amount
      body['category'] = this.category;
      body['date'] = this.date;
      body['description'] = this.description;
      body['status'] = 'Reconciled';
      body['last_update']= new Date().getTime();
      body['updated_by'] = {name:this.sg['userdata'].name,
      phone:this.sg['userdata'].phone,
      primary_language:this.sg['userdata'].primary_language,
      role:this.sg['userdata'].role,
      profile_url:this.sg['userdata'].profile_url
      }
      firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses/'+this.editContent.key).update(body);
      this.location.back();
    }
  }
  marksAsFixedAsset(){
    this.fixed_asset = true;
    firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses/'+this.editContent.key).update({fixed_asset:true});
  }
  ValidateExpense(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/expenses/'+this.editContent.key).update({status:'Reconciled'});

    this.location.back();
  }
  CancelExpense(){
    this.location.back();
  }
}
