import { Injectable } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
@Injectable({
  providedIn: 'root'
})
export class HindiService {

 constructor(private sg: SimpleGlobal) { }
 getDashboard(){
  if(this.sg['userdata'].role === 'corporate'){
    const dashboard = [{
   "title": "फार्म अवलोकन",
   "subtitle": "खेत में विशिष्ट निष्कर्षों की रिपोर्ट करें",
   "image": "assets/images/observations.jpg",
   "path": "/farmeasy-observation-list"
 },
 {
   "title": "कार्य प्रबंधन",
   "subtitle": "योजना, असाइन और पूर्ण कार्य / कार्य",
   "image": "assets/images/task.jpg",
   "path": "/task-planner" 
 },
 {
   'title': 'रिपोर्ट / विश्लेषण',
   'subtitle': 'रिपोर्ट और सभी कृषि विवरणों का विश्लेषण प्रबंधित करें',
   'image': 'assets/images/sales_report.png',
   "path": "/farmeasy-reports-menu"
 },
 {
   "title": "फार्म की जानकारी",
   "subtitle": "खेत से संबंधित संपूर्ण डेटा और उसके संचालन का प्रबंधन करें",
   "image": "assets/images/in_and_out.jpg",
   "path": "/farm-details-menu"
 },
 {
   'title': 'व्यय और खरीद',
   'subtitle': 'यहां सभी दैनिक खर्च और कृषि खरीद दर्ज करें',
   'image': 'assets/images/expense.jpg',
   "path": "/farmeasy-expense-manager"
 },
 {
   "title": "बिक्री और उत्पादन",
   "subtitle": "स्टॉक इन, स्टॉक आउट और सेल्स प्रबंधित करें",
   "image": "assets/images/management.jpg",
   "path": '/harvest'
 },
//  {
//   "title": "Asset Management",
//   "subtitle": "Manage Material In, Material Out",
//   'path': '/assets-management-menu',
//   "image": "assets/images/farmcleaning.jpg",
//  },
 {
   "title": "लेखा / वित्त",
   "subtitle": "खेत में नकदी प्रवाह का प्रबंधन करें",
   "image": "assets/images/accounts.jpg",
   "path": '/account-manager-menu'
 }];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'manager'){
  const dashboard = [{
    "title": "फार्म अवलोकन",
    "subtitle": "खेत में विशिष्ट निष्कर्षों की रिपोर्ट करें",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "कार्य प्रबंधन",
    "subtitle": "योजना, असाइन और पूर्ण कार्य / कार्य",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  },
  {
    'title': 'रिपोर्ट / विश्लेषण',
    'subtitle': 'रिपोर्ट और सभी कृषि विवरणों का विश्लेषण प्रबंधित करें',
    'image': 'assets/images/sales_report.png',
    "path": "/farmeasy-reports-menu"
  },
  {
    "title": "फार्म की जानकारी",
    "subtitle": "खेत से संबंधित संपूर्ण डेटा और उसके संचालन का प्रबंधन करें",
    "image": "assets/images/in_and_out.jpg",
    "path": "/farm-details-menu"
  },
  {
    'title': 'व्यय और खरीद',
    'subtitle': 'यहां सभी दैनिक खर्च और कृषि खरीद दर्ज करें',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  },
  {
    "title": "बिक्री और उत्पादन",
    "subtitle": "स्टॉक इन, स्टॉक आउट और सेल्स प्रबंधित करें",
    "image": "assets/images/management.jpg",
    "path": '/harvest'
  },
  {
    "title": "लेखा / वित्त",
    "subtitle": "खेत में नकदी प्रवाह का प्रबंधन करें",
    "image": "assets/images/accounts.jpg",
    "path": '/account-manager-menu'
  }];
return(dashboard);
} else if(this.sg['userdata'].role === 'accounts'){
const dashboard = [
{
  'title': 'व्यय और खरीद',
  'subtitle': 'यहां सभी दैनिक खर्च और कृषि खरीद दर्ज करें',
  'image': 'assets/images/expense.jpg',
  "path": "/farmeasy-expense-manager"
},
{
  "title": "खेतीकेउतपादन",
  "subtitle": "स्टॉक इन, स्टॉक आउट और सेल्स प्रबंधित करें",
  "image": "assets/images/management.jpg",
  "path": '/harvest'
},
{
  "title": "लेखा / वित्त",
  "subtitle": "खेत में नकदी प्रवाह का प्रबंधन करें",
  "image": "assets/images/accounts.jpg",
  "path": '/account-manager-menu'
},
{
  'title': 'रिपोर्ट / विश्लेषण',
  'subtitle': 'रिपोर्ट और सभी कृषि विवरणों का विश्लेषण प्रबंधित करें',
  'image': 'assets/images/sales_report.png',
  "path": "/farmeasy-reports-menu"
},
{
  "title": "फार्म की जानकारी",
  "subtitle": "खेत से संबंधित संपूर्ण डेटा और उसके संचालन का प्रबंधन करें",
  "image": "assets/images/in_and_out.jpg",
  "path": "/farm-details-menu"
}];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'supervisor'){
  const dashboard = [{
    "title": "फार्म अवलोकन",
    "subtitle": "खेत में विशिष्ट निष्कर्षों की रिपोर्ट करें",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "कार्य प्रबंधन",
    "subtitle": "योजना, असाइन और पूर्ण कार्य / कार्य",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  },
  {
    'title': 'व्यय और खरीद',
    'subtitle': 'यहां सभी दैनिक खर्च और कृषि खरीद दर्ज करें',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  }
];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'employee'){
  const dashboard = [{
    "title": "फार्म अवलोकन",
    "subtitle": "खेत में विशिष्ट निष्कर्षों की रिपोर्ट करें",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "कार्य प्रबंधन",
    "subtitle": "योजना, असाइन और पूर्ण कार्य / कार्य",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  }];
   return(dashboard);
 } 
}
 getHamburger(){
 const hamburger = {
 profile:'मेरा प्रोफ़ाइल',
 notification:'सूचनाएं',
 settings: 'सेटिंग',
 about: 'फ़ार्मेसी के बारे में',
 sync: 'सिंक डेटा',
 lagout: 'लॉगआउट',
 switch:'स्विच भाषा',
 dashboard: 'डैशबोर्ड',
 support:'सहायता / प्रतिक्रिया',
 plant_history: 'पौधे इतिहास',
 version:'संस्करण',
 change_pass:'पासवर्ड बदलें',
 terms:'तिथि'
 };
 return(hamburger);
 }
     
getOnBoarding(){
 const onboarding = [{
 'image': 'assets/images/admin.jpg',
 'heading': 'कर्मचारी रिकॉर्ड्स',
 'description': 'खेत में कर्मचारियों के सभी रोजगार रिकॉर्ड प्रबंधित करें',
 'action': '/farmeasy-manage-labours',
 },
 {
 'image': 'assets/images/tasks.jpg',
 'heading': 'श्रेणियाँ सेटअप',
 'description': 'खेत में सभी श्रेणियों का काम सेटअप करें',
 'action': '/farmeasy-view-category',
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'heading': 'ब्लॉक सेटअप',
 'description': 'खेत में ब्लॉक और विभाजन सेट करें',
 'action': '/farmeasy-view-blocks',
 },
 {
 'image': 'assets/images/rotavator.jpg',
 'heading': 'संसाधन सेटअप',
 'description': 'खेत में उपयोग करने के लिए लगाए गए सभी संसाधन',
 'action': '/farmeasy-view-source',
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'heading': 'फार्म का नक्शा / लेआउट',
 'description': 'खेत और उसके लेआउट को चिह्नित करें',
 'action': '/farm-map',
 }, 
 ];
 return(onboarding);
 }
 getExpense(){
   let expense = [];
  if(this.sg['userdata'].role === 'supervisor'){
    expense = [{
      'image': 'assets/images/dailyExpense.png',
      'action': 'view-daily-expense',
      'heading': 'फार्म पर दैनिक खर्च',
      'keyword':'Daily Expenses',
      'description':'दिन-प्रतिदिन का खर्च यहां लॉग इन करने के लिए'
      },
      {
      'image': 'assets/images/farm-map.jpg',
      'action': 'farm-purchases-list',
      'heading': 'फार्म खरीद',
      'keyword':'Farm Purchases',
      'description': 'किसी भी मशीनरी / उपकरण खरीद सहित खेत के लिए कोई भी खरीद'
      }];
  } else {
    expense = [{
      'image': 'assets/images/dailyExpense.png',
      'action': 'view-daily-expense',
      'heading': 'फार्म पर दैनिक खर्च',
      'keyword':'Daily Expenses',
      'description':'दिन-प्रतिदिन का खर्च यहां लॉग इन करने के लिए'
      },
      {
      'image': 'assets/images/farm-map.jpg',
      'action': 'farm-purchases-list',
      'heading': 'फार्म खरीद',
      'keyword':'Farm Purchases',
      'description': 'किसी भी मशीनरी / उपकरण खरीद सहित खेत के लिए कोई भी खरीद'
      },
      {
      'image': 'assets/images/salary_payments.jpeg',
      'action': 'salary-advances',
      'heading': 'वेतन भुगतान',
      'keyword': 'Salary Payments',
      'description': 'कर्मचारियों को दिए गए सभी वेतन भुगतान दर्ज करें'
      },
      {
      'image': 'assets/images/loan.jpeg',
      'action': 'farmeasy-labour-advance',
      'heading': 'ऋण और अग्रिम',
      'keyword': 'Loans & Advances',
      'description': 'महीने के दौरान मजदूरों को दी गई कोई अग्रिम राशि'
      }];
  }
 return(expense);
 }
 getReportsMenu(){
 const reports = [{
 'heading': 'बिक्री और संग्रह रिपोर्ट',
 'description': 'खेत में खराब होने वाली वस्तुओं सहित बिक्री',
 'image': './assets/images/sales_report.png',
 'action': '/sales-collections-reports'
 },
 {
 'heading': 'व्यय और खरीद रिपोर्ट',
 'description': 'किसी भी मशीनरी / उपकरण खरीद सहित खेत के लिए कोई भी खरीद',
 'image': './assets/images/expense.jpg',
 'action': '/expense-purchases-reports'
 },
 {
 'heading': 'इन्फ्लो एंड आउटफ्लो रिपोर्ट',
 'description': 'खेत में नकदी की आमद और बहिर्वाह को ट्रैक करें',
 'image': './assets/images/in_and_out.jpg',
 'action': '/inflow-outflow-reports'
 },
 {
 'heading': 'स्टॉक इन्वेंटरी रिपोर्ट',
 'description': 'स्टॉक विवरण, स्टॉक आउट और स्टॉक ऑन हैंड विवरण',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-inventory-report-menu'
 },
 {
 'heading': 'फार्म अवलोकन रिपोर्ट',
 'description': 'खेत में सभी वृक्षारोपण को ट्रैक करें और उन्हें प्रबंधित करें',
 'image': './assets/images/plantation_new.jpg',
 'action': '/farm-activity-report-menu'
 }];
 return(reports);
 }
 getFarmActivityMenu(){
 const reports = [{
 'heading': 'फार्म अवलोकन रिपोर्ट',
 'keyword': 'observation-reports',
 'description': 'खेत में बताई गई सभी टिप्पणियों को ट्रैक करें',
 'image': './assets/images/in_and_out.jpg',
 'action': '/farmeasy-all-observations'
 },
 {
 'heading':'पानी की रिपोर्ट',
 'keyword': 'watering-reports',
 'description': 'खेत में किए गए पानी की निगरानी करें',
 'image': './assets/images/watering_reports.jpg',
 'action': '/watering-reports'
 },
 {
 'heading': 'फर्टिगेशन रिपोर्ट',
 'keyword': 'fertigation-reports',
 'description':'खेत में किए गए उर्वरकों की निगरानी करें',
 'image': './assets/images/fertigation.jpeg',
 'action': '/watering-reports'
 },
//  {
//  'keyword': 'labour-work-assignment',
//  'heading': 'लेबर वर्क असाइनमेंट',
//  'description': 'खेत में मजदूरों को सौंपा गया कार्य देखें',
//  'image': './assets/images/male.png',
//  'action': '/resource-planning'
//  },
 {
 'heading':'कार्य रिपोर्ट',
 'keyword': 'task-reports',
 'description': 'खेत में प्रबंधित सभी कार्य देखें',
 'image': './assets/images/task.jpg',
 'action': '/task-reports'
 }
 ];
 return(reports);
 }
 getStockInventoryMenu(){
 const reports = [{
 'heading':'स्टॉक इन',
 'description': 'खेत में की गई सभी कटाई को ट्रैक करें और उन्हें प्रबंधित करें',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-in'
 },
 {
 'heading': 'स्टॉक आउट',
 'description': 'खेत में सभी वृक्षारोपण को ट्रैक करें और उन्हें प्रबंधित करें',
 'image': './assets/images/stock_out.png',
 'action': '/stock-out'
 },
 {
 'heading': 'स्टॉक ऑन हैंड',
 'description':'खेत में खराब होने वाली वस्तुओं सहित बिक्री',
 'image': './assets/images/stockonhand.jpg',
 'action': '/stock-in-hand'
 }
 ];
 return(reports);
 }
 getHarvestMenu(){
 const harvest=[{
 "title": "स्टॉक इन",
 "title1": "stock in",
 "subtitle": "खेत में की गई सभी कटाई को ट्रैक करें और उन्हें प्रबंधित करें",
 "image": "assets/images/harvest-in.png",
 "path": "/harvestin"
 },
 {
 "title":"बिक्री",
 "title1":"sales",
 "subtitle": "किसी भी मशीनरी / उपकरण खरीद सहित खेत के लिए कोई भी खरीद",
 "image": "assets/images/cultivator.png",
 "path": "/harvestout"
 },
 {
 "title": "स्टॉक का इस्तेमाल किया और नुकसान",
 "title1": "stock out",
 "subtitle": "खेत में जाने वाली सभी फ़सल की कटाई करें और उन्हें प्रबंधित करें",
 "image": "assets/images/food.jpg",
 "path": "/harvestout"
 }
 ];
 return(harvest);
 }
 getAssestsManagementMenu(){
 const purchases=[
 {
 "title":"सामग्री खरीद में",
 "subtitle": "किसी भी मशीनरी / उपकरण खरीद सहित खेत के लिए कोई भी खरीद",
 "image": "assets/images/cultivator.png",
 "keyword":"PurchasesIn",
 "path": "/purchasein-list"
 },
 {
 "title":"सामग्री खरीद बाहर",
 "subtitle": "स्टोर से बाहर जाने वाली किसी भी सामग्री को ट्रैक और प्रबंधित करें",
 "image": "assets/images/dailyExpense.png",
 "keyword":"PurchasesOut",
 "path": "/purchasein-list"
 }
 ];
 return(purchases);
 }
 getTaskManagerMenu(){
 const task = [{
 "image": 'assets/images/category.png',
 'heading':'श्रेणी द्वारा निरुपित',
 "description": 'श्रेणी के अनुसार कार्य / कार्य सौंपें। श्रेणी: पानी, सफाई, फसल आदि',
 "action":'/create-task-category'
 },
 {
 "image": 'assets/images/farm-map.jpg',
 'heading':'ब्लॉक द्वारा असाइन करें',
 "description": 'खेत में अलग किए गए ब्लॉक द्वारा कार्य निर्दिष्ट करें',
 "action":'/create-task-by-block'
 },
 {
 "image": 'assets/images/male.png',
 'heading': 'श्रम द्वारा निरुपित',
 "description": 'खेत में कार्यरत मजदूरों पर आधारित कार्य',
 "action":'/create-task-labour'
 },
 {
 "image": 'assets/images/task.jpg',
 'heading':'तिथि द्वारा निरुपित',
 "description": 'तिथि के आधार पर कार्य निर्दिष्ट करें',
 "action":'/create-task-by-date'
 }
 ];
 return(task);
 }
 getFarmDetails(){
 const labels = [{
 'image': 'assets/images/task.jpg',
 'action': '/farm-land-records-list',
 'heading': 'लैंड रिकॉर्ड्स',
 'keyword': 'Land Records',
 'description': 'सभी भूमि रिकॉर्ड और कथे विवरण प्रबंधित करें'
 },
 {
 'image': 'assets/images/cultivator.png',
 'action': '/farm-land-records-list',
 'heading': 'फार्म एसेट्स',
 'keyword': 'Farm Assets',
 'description': 'सभी अचल संपत्तियों और खेत में चल संपत्ति को स्टोर करें'
 },
 {
 'image': 'assets/images/water.jpg',
 'action': '/farm-land-records-list',
 'heading': 'फार्म उपयोगिता विवरण',
 'keyword': 'Farm Utility Details',
 'description': 'किसी भी ईबी को स्टोर करें। पानी और अन्य उपयोगिता बिल '
 },
 {
 'image': 'assets/images/certifying-clipart-banner-6.png',
 'action': '/farm-land-records-list',
 'heading':'लाइसेंस',
 'keyword': 'Licenses',
 'description': 'कोई भी लाइसेंस और खेत से संबंधित दस्तावेज'
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'action': 'farmeasy-boarding-menu',
 'heading':'फार्म ऑनबोर्डिंग / सेटअप',
 'description': 'संपूर्ण फ़ार्म लेआउट और मानचित्र प्रबंधित करें'
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'action': '/farm-land-records-list',
 'heading': 'विविध',
 'keyword': 'Miscellaneous',
 'description':'उपरोक्त श्रेणियों में फिट होने वाला कोई अन्य दस्तावेज'
 }
 ];
 return(labels);
 }
 getAccountsManagerLabels(){
 let labels = [
 // {
 // 'image': 'assets/images/salary_payments.jpeg',
 // 'action': 'salary-advances',
 // 'heading': 'Salary Payments',
 // 'description': 'Enter all salary payments made to the employees'
 // },
 // {
 // 'image': 'assets/images/loan.jpeg',
 // 'action': 'farmeasy-labour-advance',
 // 'heading': 'Loans & Advances',
 // 'description': 'Any advance given to labourers during the month'
 // },
 {
 'image': 'assets/images/sales_report.png',
 'action': 'sales-reconciliation',
 'heading': 'डेली रेककन ऑफ़ सेल',
 'description': 'दैनिक बिक्री के आंकड़े प्राप्त करें और साइन ऑफ करें',
 },
   {
 'image': 'assets/images/in_and_out.jpg',
 'action': 'expenses-reconciliation',
 'heading': 'दैनिक व्यय का प्रतिपूर्ति',
 'description':'दैनिक खर्चों को पुनः प्राप्त करें और साइन ऑफ करें',
 },
   ];
 return(labels)
 }
 getEnterObservation(){
 let message = 'आपका अवलोकन सफलतापूर्वक दर्ज किया गया है और क्षेत्र पर्यवेक्षक को आगे की कार्रवाई के बारे में सूचित किया गया है जिसे लेने की जरूरत है' ;
 let labels =['एक ब्लॉक का चयन करें', 'समस्या का विवरण दर्ज करें', 'प्राथमिकता', 'सबमिट करें', message,'फ़ाइल का चयन करें', 'समस्या का विवरण दर्ज करें', 'ब्लॉक', 'ठीक है', 'रद्द करें', 'एक मूल्य का चयन करें ',' सबूत का सबूत '];
 return(labels);
 }
 getBudgetApproval(){
 const budget = ['सलेक्ट पर्पस', 'सेलेक्ट कैटेगरी', 'अमाउंट', 'औचित्य', 'सबमिट', 'रेंज फ्रॉम', 'रेंज टू', 'एंटर जस्टिफिकेशन'];
 return(budget);
 }
 getBudgetApprovalView(){
 const budgetview = ['उद्देश्य', 'उत्पाद', 'औचित्य', 'राशि', 'चित्र', 'अनुमोदन', 'अस्वीकार', 'बजट अनुमोदन के लिए कोई अनुरोध नहीं'];
 return(budgetview);
 }
 getDailyExpense(){
 const daily = ['व्यय श्रेणी का चयन करें', 'व्यय विवरण', 'राशि [INR]', 'व्यय की तिथि', 'औचित्य', 'जमा करें', 'पाठ दर्ज करें'];
 return(daily);
 }
 getLabourAdvance(){
 const labAdvance = ['चयन व्यय श्रेणी', 'श्रम का नाम', 'मासिक वेतन', 'अग्रिम भुगतान', 'नया सलाह', 'जमा'];
 return(labAdvance);
 }
 getHarvestOutInvoice(){
 const invoice = ['हार्वेस्ट आउट - इनवॉइस', 'श्रीनी फूड पार्क', 'मल्लिक अर्जुन राव करगे, 121, चित्तूर मेन रोड', 'आंध्र प्रदेश, चित्तौड़', 'श्रेणी',
     'उपश्रेणी', 'दर', 'वेलनेस्ट नेचुरल फार्म', 'जेपी कॉर्प, बेल्लारी रोड, सदा शिवा नगर', 'बैंगलोर, कर्नाटक, भारत।', 'पंजीकरण संख्या', 'प्रिंट चालान', 'कुल'];
 return(invoice);
 }
 getHarvestIn(){
 const harvestIn =['प्राप्त करने की तिथि', 'चयनित श्रम', 'लेनदेन की तिथि', 'फसल का प्रकार','सेलेक्ट कैटेगरी', 'बिल नं', 'कस्टमर नेम', 'कॉन्टेक्ट पर्सन', 'सेलेक्ट सब-कैटेगरी', 'यूओएम', 'क्यूटीवाई', 'रेट','रिव्यू एंट्री','कृपया सभी क्षेत्रों का चयन करें ','विवरण उसकी एडीओटोरी श्रेणी'] 
  return(harvestIn);
 }
 getHarvestPrintPopup(){
 const popup = ['Bill / Invoice', '# 219/11, JP Corp, Bellary Road, Sadshiva nagar, Bangalore, Karnataka, India।', 'Category', 'Subcategory', 'UOM', 'QTY', ' मूल्यांकन करें',
     'नोट: यह एक इलेक्ट्रॉनिक रूप से उत्पन्न इनवॉइस है और इसमें हस्ताक्षर की आवश्यकता नहीं है', 'प्रिंट', 'टोटल'];
 return(popup);
 }
 getHarvestReviewPopup(){
 const reviewpopup =['की समीक्षा की पुष्टि करें', 'मजदूर का नाम:', 'श्रेणी का नाम:', 'उपश्रेणी का नाम:', 'प्राप्ति की तिथि:', 'UOM:', 'मात्रा:', 'रद्द', 'ठीक है', ' मूल्यांकन करें:'];
 return(reviewpopup);
 }
 getCreateEmployeeLabels(){
 const labels = {
 'heading':'कर्मचारी बनाएँ',
 'title':'कर्मचारी आईडी कार्ड',
 'name': 'कर्मचारी का नाम',
 'gender': '- एक लिंग चुनें -',
 'dob': 'जन्म तिथि',
 'address': 'आवासीय पता',
 'phone': 'संपर्क नंबर',
 'aadhar': 'आधार नंबर',
 'email':'ईमेल आईडी',
 'proof': 'आईडी प्रमाण जोड़ें',
 'bank': 'बैंक विवरण जोड़ें',
 'language': 'भाषा वरीयता',
 'details': 'रोजगार विवरण',
 'doj':'जुड़ने की तारीख',
 'designation': '- पदनाम -',
 'salary': 'मासिक वेतन',
 'manager': '- प्रबंधक -',
 'password':'पासवर्ड',
 'block': '- ब्लॉक का चयन करें',
 'submit':'प्रस्तुत',
 'update': 'अद्यतन',
 'delete': 'नष्ट',
 'found': 'उपयोगकर्ता पहले से मौजूद हैं',
 'known_lang': 'ज्ञात भाषाएं'
 }
 return(labels);
 }
 getViewObservationLabels(){ 
 const view = {
 'image':'छवि',
 'video':'वीडियो',
 'action':'एक्शन हिस्ट्री ऑन ऑब्जर्वेशन',
 'acknowledge':'स्वीकार',
 'convert': 'टास्क में कन्वर्ट'
 };
 return(view);
 }
 getLiveStockMenu(){
 const menu = [{
 'name': 'सब्जियां',
 'icon': 'assets/images/carrot.svg',
 'action': '/farmeasy-livestock-view',
 },
 {
 'name': 'फल',
 'icon': 'assets/images/watermelon.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'फसल',
 'icon': 'assets/images/crops.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'ग्रीन्स',
 'icon': 'assets/images/green-tea.svg',
 'action': '/farmeasy-livestock-view'
 }];
 return(menu);
 }
 getHarvestReportLabels(){
 const reports = ['प्रकार', 'तारीख से', 'आज तक', 'रिपोर्ट', 'तुलना'];
 return(reports);
 }
 getCreateTaskLabourLabels(){
 const labels = {
 labour:'श्रम',
 labour_place:'- श्रम का चयन करें -',
 date: 'तिथि',
 block: 'ब्लॉक',
 block_place: '- ब्लॉक चुनें -',
 priority: 'प्राथमिकता',
 priority_place:'- प्राथमिकता चुनें -',
 adhoc:'एडहॉक टास्क',
 adhoc_place: '- कार्य चुनें -',
 select_lab_date:'कृपया श्रम और तिथि का चयन करें'
 }
 return(labels);
 }
 getFrequencySelectionLabels(){
 const labels = {
 frequency: 'आवृत्ति',
 freq_place:'- फ़्रीक्वेंसी चुनें -',
 dayof: 'सप्ताह का दिन',
 dayofm:'महीना (दिन) का महीना',
 start: 'आरंभ तिथि',
 end: 'अंतिम तिथि',
 ok:'ठीक है',
 cancel: 'अभी नहीं'
 }
 return(labels);
 }
 getMyTaskLabels(){
 const labels = {
 title:'माई टास्क',
 place: 'खोज',
 water:'पौधों को पानी में',
 Block: 'अवरुद्ध करें',
 plots: 'भूखंडों',
 Acknowledge: 'स्वीकार',
 Acknowledged: 'स्वीकार',
 done:'मार्क ऐज़ डन',
 completed: 'पूरा',
 clean: 'क्लीन इन',
 notasks:'अभी तक कोई कार्य नहीं है'
 }
 return(labels);
 }
 getTaskListLabels(){
 const labels = {
 water: 'पौधों को पानी में',
 place: 'खोज मापदंड दर्ज करें',
 clean: 'क्लीन इन',
 block: 'ब्लॉक',
 plots:'भूखंडों',
 Acknowledge:'स्वीकार',
 Acknowledged:'स्वीकार',
 done: 'मार्क ऐज़ डन',
 completed: 'पूरा',
 select: '- मान चुनें -'
 }
 return(labels);
 }
getTaskPlannerLabels(){
 const labels = {
select_freq:'- आवृत्ति का चयन करें -',
Filter: 'फिल्टर',
image: 'छवि',
Block: 'अवरुद्ध करें',
Category:'श्रेणी',
Description: 'वर्णन',
Assignee:'अनुदिष्ट',
Status:'स्थिति',
Open: 'ओपन',
Closed:'बंद',
NowShowing:'अब दिखा',
nores: 'कोई परिणाम नहीं मिला',
}
return(labels);
 }
 getTaskUpdateHistoryLabels(){
 const labels = {
 title: 'टास्क अपडेट हिस्ट्री',
 assign: 'टास्क असाइन किया गया',
 task_mark:'कार्य को के रूप में चिह्नित किया गया है',
 completed: 'कार्य को पूर्ण के रूप में चिह्नित किया गया है',
 updated:'कार्य को अपडेट कर दिया गया है'
 }
 return(labels);
 }
 getAddBlockLabels(){
 const labels = {
 title: 'टास्क अपडेट हिस्ट्री',
 assign: 'टास्क असाइन किया गया',
 task_mark:'कार्य को के रूप में चिह्नित किया गया है',
 completed:'कार्य को पूर्ण के रूप में चिह्नित किया गया है',
 updated: 'कार्य को अपडेट कर दिया गया है'
 }
 return(labels);
 }
 getAddLiveStockLabels(){
 const labels = {
 name:'नाम',
 gst:'जीएसटी',
 block:'ब्लॉक',
 submit:'सबमिट',
 update:'अपडेट करें',
 delete:'हटाएं',
 };
 return(labels);
 }
 getCreateNotificationLabels(){
  let labels = {
    notification: "अधिसूचना संदेश",
    priority:"प्राथमिकता",
    priority_place:"एक मान का चयन करें",
    employee:'कर्मचारियों का चयन करें',
    selectAll:'सभी का चयन करे',
    alert:'कृपया खेतों का चयन करें',
    submit:"प्रस्तुत"
  }
 return(labels)
 }

 getPriority(){
 let labels = [{name:'अर्जेंट',view:'अर्जेंट'},
 {name:'उच्च',view:'उच्च'},
 {name:'मध्यम',view:'मध्यम'},
 {name:'कम',view:'कम'}];
 return labels;
 }
}
