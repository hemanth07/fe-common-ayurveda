import { Injectable } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
@Injectable({
  providedIn: 'root'
})
export class EnglishService {

  constructor(private sg: SimpleGlobal) { }
  getDashboard(){
    if(this.sg['userdata'].role === 'MD'){
      const dashboard = [
  //     {
  //    "title": "Farm Observations",
  //    "subtitle": "Report specific findings  ",
  //    "image": "assets/images/observations.jpg",
  //    "path": "/farmeasy-observation-list"
  //  },
   {
     "title": "Task Management",
     "subtitle": " Plan, Assign & Complete Tasks/Work",
     "image": "assets/images/task.jpg",
     "path": "/task-planner" 
   },
  //  {
  //    'title': 'Reports/Analytics',
  //    'subtitle': 'Manage reports and the analysis of all farm details',
  //    'image': 'assets/images/sales_report.png',
  //    "path": "/farmeasy-reports-menu"
  //  },
   {
     "title": "Administration",
     "subtitle": "Setup the company related data",
     "image": "assets/images/in_and_out.jpg",
     "path": "/farm-details-menu"
   },
  //  {
  //    'title': 'Expenses & Purchases',
  //    'subtitle': 'Enter all daily expenses and farm purchases here',
  //    'image': 'assets/images/expense.jpg',
  //    "path": "/farmeasy-expense-manager"
  //  },
  //  {
  //    "title": "Sales and Production",
  //    "subtitle": "Manage Stock In, Stock Out, and Sales",
  //    "image": "assets/images/management.jpg",
  //    "path": '/harvest'
  //  },
  //  {
  //   "title": "Asset Management",
  //   "subtitle": "Manage Material In, Material Out",
  //   'path': '/assets-management-menu',
  //   "image": "assets/images/farmcleaning.jpg",
  //  },
  //  {
  //    "title": "Accounts/Finance",
  //    "subtitle": "Manage cash flow  ",
  //    "image": "assets/images/accounts.jpg",
  //    "path": '/account-manager-menu'
  //  }
  ];
   return(dashboard);
   } else if(this.sg['userdata'].role === 'Manager'){
    const dashboard = [
    //   {
    //   "title": "Observations",
    //   "subtitle": "Report specific findings  ",
    //   "image": "assets/images/observations.jpg",
    //   "path": "/farmeasy-observation-list"
    // },
    {
      "title": "Task Management",
      "subtitle": " Plan, Assign & Complete Tasks/Work",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    },
    // {
    //   'title': 'Reports/Analytics',
    //   'subtitle': 'Manage reports and the analysis of all farm details',
    //   'image': 'assets/images/sales_report.png',
    //   "path": "/farmeasy-reports-menu"
    // },
    {
      "title": "Info",
      "subtitle": "Manage the complete related data and its operations",
      "image": "assets/images/in_and_out.jpg",
      "path": "/farm-details-menu"
    },
    // {
    //   'title': 'Expenses & Purchases',
    //   'subtitle': 'Enter all daily expenses and farm purchases here',
    //   'image': 'assets/images/expense.jpg',
    //   "path": "/farmeasy-expense-manager"
    // },
    // {
    //   "title": "Sales and Production",
    //   "subtitle": "Manage Stock In, Stock Out, and Sales",
    //   "image": "assets/images/management.jpg",
    //   "path": '/harvest'
    // },
    // {
    //   "title": "Accounts/Finance",
    //   "subtitle": "Manage cash flow  ",
    //   "image": "assets/images/accounts.jpg",
    //   "path": '/account-manager-menu'
    // }
  ];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'corporate'){
  const dashboard = [
    {
      "title": "Task Management",
      "subtitle": " Plan, Assign & Complete Tasks/Work",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    },
   //  {
   //    'title': 'Reports/Analytics',
   //    'subtitle': 'Manage reports and the analysis of all farm details',
   //    'image': 'assets/images/sales_report.png',
   //    "path": "/farmeasy-reports-menu"
   //  },
    {
      "title": "Administration",
      "subtitle": "Setup the company related data",
      "image": "assets/images/in_and_out.jpg",
      "path": "/farm-details-menu"
    },
  // {
  //   'title': 'Expenses & Purchases',
  //   'subtitle': 'Enter all daily expenses and purchases here',
  //   'image': 'assets/images/expense.jpg',
  //   "path": "/farmeasy-expense-manager"
  // },
  // {
  //   "title": "Produce",
  //   "subtitle": "Manage Stock In, Stock Out, and Sales",
  //   "image": "assets/images/management.jpg",
  //   "path": '/harvest'
  // },
  // {
  //   "title": "Accounts/Finance",
  //   "subtitle": "Manage cash flow  ",
  //   "image": "assets/images/accounts.jpg",
  //   "path": '/account-manager-menu'
  // },
  // {
  //   'title': 'Reports/Analytics',
  //   'subtitle': 'Manage reports and the analysis of all farm details',
  //   'image': 'assets/images/sales_report.png',
  //   "path": "/farmeasy-reports-menu"
  // },
  {
    "title": "Info",
    "subtitle": "Manage the complete farm related data and its operations",
    "image": "assets/images/in_and_out.jpg",
    "path": "/farm-details-menu"
  }
];
   return(dashboard);
   } else if(this.sg['userdata'].role === 'HR'){
    const dashboard = [
    //   {
    //   "title": "Farm Observations",
    //   "subtitle": "Report specific findings  ",
    //   "image": "assets/images/observations.jpg",
    //   "path": "/farmeasy-observation-list"
    // },
    {
      "title": "Task Management",
      "subtitle": " Plan, Assign & Complete Tasks/Work",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    },
    {
      "title": "Administration",
      "subtitle": "Setup the company related data",
      "image": "assets/images/in_and_out.jpg",
      "path": "/farm-details-menu"
    },
    // {
    //   'title': 'Expenses & Purchases',
    //   'subtitle': 'Enter all daily expenses and farm purchases here',
    //   'image': 'assets/images/expense.jpg',
    //   "path": "/farmeasy-expense-manager"
    // }
  ];
   return(dashboard);
   } else if(this.sg['userdata'].role === 'Employee'){
    const dashboard = [
    //   {
    //   "title": "Farm Observations",
    //   "subtitle": "Report specific findings  ",
    //   "image": "assets/images/observations.jpg",
    //   "path": "/farmeasy-observation-list"
    // },
    {
      "title": "Task Management",
      "subtitle": " Plan, Assign & Complete Tasks/Work",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    }
  ];
     return(dashboard);
   } 
 }
  getHamburger(){
    const hamburger = {
      profile:'My Profile',
      notification:'Notifications',
      settings:'Settings',
      about:'About Ayurveda One',
      calender:'Employee Calendar',
      sync:'Sync Data',
      lagout:'Logout',
      switch:'Switch Language',
      dashboard:'Dashboard',
      support:'Support/Feedback',
      plant_history:'Plants History',
      change_pass:'Change Password',
      version:'Version',
      terms:'Date'

    };
    return(hamburger);
  }
  getOnBoarding(){
    const onboarding = [{
    'image': 'assets/images/admin.jpg',
    'heading': 'Employee Records',
    'description': 'Manage all employment records of the staff  ',
    'action': '/farmeasy-manage-labours',
    },
    {
      'image': 'assets/images/tasks.jpg',
      'heading': 'Categories Setup',
      'description': 'Setup all categories of work  ',
      'action': '/farmeasy-view-category',
    },
    // {
    //   'image': 'assets/images/farmmap.jpg',
    //   'heading': 'Blocks Setup',
    //   'description': 'Setup the blocks and partitions  ',
    //   'action': '/farmeasy-view-blocks',
    // },
    //  {
    //   'image': 'assets/images/rotavator.jpg',
    //   'heading': 'Resources Setup',
    //   'description': 'Setup all the resources put to use  ',
    //   'action': '/farmeasy-view-source',
    // },
    // {
    //   'image': 'assets/images/Farm-Base-Map.png',
    //   'heading': 'Farm Map/Layout',
    //   'description': 'Mark the farm and its layout',
    //   'action': '/farm-map',
    // }, 
   ];
    return(onboarding);
  }
  getExpense(){
    let expense = []
    if(this.sg['userdata'].role === 'supervisor'){
      expense = [
        {
        'image': 'assets/images/dailyExpense.png',
        'action': 'view-daily-expense',
        'heading': 'Daily Expenses at Farm',
        'keyword':'Daily Expenses',
        'description': 'Day to day expenditure to be logged here'
        },
        {
          'image': 'assets/images/farm-map.jpg',
          'action': 'farm-purchases-list',
          'heading': 'Farm Purchases',
          'keyword':'Farm Purchases',
          'description': 'Any purchases for the farm including any machinery / equipment purchase'
        }];
    } else {
      expense = [{
        'image': 'assets/images/dailyExpense.png',
        'action': 'view-daily-expense',
        'heading': 'Daily Expenses at Farm',
        'keyword':'Daily Expenses',
        'description': 'Day to day expenditure to be logged here'
        },
        {
          'image': 'assets/images/farm-map.jpg',
          'action': 'farm-purchases-list',
          'heading': 'Farm Purchases',
          'keyword':'Farm Purchases',
          'description': 'Any purchases for the farm including any machinery / equipment purchase'
        },
        {
          'image': 'assets/images/salary_payments.jpeg',
          'action': 'salary-advances',
          'heading': 'Salary Payments',
          'keyword': 'Salary Payments',
          'description': 'Enter all salary payments made to the employees'
        },
        {
          'image': 'assets/images/loan.jpeg',
          'action': 'farmeasy-labour-advance',
          'heading': 'Loans & Advances',
          'keyword': 'Loans & Advances',
          'description': 'Any advance given to labourers during the month'
        },
      ];
    }
    return(expense);
  }
  getReportsMenu(){
    const reports = [{
      'heading': 'Sales & Collection Report',
      'description': 'Sales in the the farm including perishable items',
      'image': './assets/images/sales_report.png',
      'action': '/sales-collections-reports'
    },
    {
      'heading': 'Expenses & Purchases Report',
      'description': 'Any purchases for the farm including any machinery / equipment purchase',
      'image': './assets/images/expense.jpg',
      'action': '/expense-purchases-reports'
    },
    {
      'heading': 'Inflow & Outflow Report',
      'description': 'Track the inflow and outflow of cash  ',
      'image': './assets/images/in_and_out.jpg',
      'action': '/inflow-outflow-reports'
    },
    {
      'heading': 'Stock Inventory Report',
      'description': 'View Stock In, Stock Out and Stock on Hand details',
      'image': './assets/images/harvesting1.jpg',
      'action': '/stock-inventory-report-menu'
    },
    {
      'heading': 'Farm Observation Reports',
      'description': 'Track all Plantation and manage them',
      'image': './assets/images/plantation_new.jpg',
      'action': '/farm-activity-report-menu'
    }
  ];
    return(reports);
  }
  getFarmActivityMenu(){
    const reports = [{
      'heading': 'Farm Observations Report',
      'keyword': 'observation-reports',
      'description': 'Track all the observations reported  ',
      'image': './assets/images/in_and_out.jpg',
      'action': '/farmeasy-all-observations'
    },
    {
      'heading': 'Watering Report',
      'keyword': 'watering-reports',
      'description': 'Monitor the watering done across the farm ',
      'image': './assets/images/watering_reports.jpg',
      'action': '/watering-reports'
    },
    {
      'heading': 'Fertigation Report',
      'keyword': 'fertigation-reports',
      'description': 'Monitor the fertigation done across the farm ',
      'image': './assets/images/fertigation.jpeg',
      'action': '/watering-reports'
    },
    // {
    //   'keyword': 'labour-work-assignment',
    //   'heading': 'Labour Work assignment',
    //   'description': 'View the work assigned to the Labourers  ',
    //   'image': './assets/images/male.png',
    //   'action': '/resource-planning'
    // },
    {
      'heading': 'Task Report',
      'keyword': 'task-reports',
      'description': 'View all tasks managed  ',
      'image': './assets/images/task.jpg',
      'action': '/task-reports'
    }
  ];
    return(reports);
  }
  getStockInventoryMenu(){
    const reports = [{
      'heading': 'Stock In',
      'description': 'Track all harvests done and manage them',
      'image': './assets/images/harvesting1.jpg',
      'action': '/stock-in'
    },
    {
      'heading': 'Stock Out',
      'description': 'Track all Plantation and manage them',
      'image': './assets/images/stock_out.png',
      'action': '/stock-out'
    },
    {
      'heading': 'Stock on Hand',
      'description': 'Sales in the the farm including perishable items',
      'image': './assets/images/stockonhand.jpg',
      'action': '/stock-in-hand'
    }
  ];
    return(reports);
  }
  getHarvestMenu(){
    const harvest=[{
      "title": "Stock In",
      "title1": "stock in",
      "subtitle": "Track all harvests done and manage them",
      "image": "assets/images/harvest-in.png",
      "path": "/harvestin"
    },
    {
      "title": "Sales",
      "title1": "sales",
      "subtitle": "Any purchases for the farm including any machinery / equipment purchase",
      "image": "assets/images/cultivator.png",
      "path": "/harvestout"
    },
    {
      "title": "Stock used and Damage",
      "title1": "stock out",
      "subtitle": "Track all harvests going out and manage them",
      "image": "assets/images/food.jpg",
      "path": "/harvestout"
    },
    
   ];
    return(harvest);
  }
    getAssestsManagementMenu(){
    const purchases=[
    // {
    //   "title": "Material Purchases In",
    //   "subtitle": "Any purchases for the farm including any machinery / equipment purchase",
    //   "image": "assets/images/cultivator.png",
    //   "keyword":"PurchasesIn",
    //   "path": "/purchasein-list"
    // },
    // {
    //   "title": "Material Purchases Out",
    //   "subtitle": "Track and manage any material going out of the stores",
    //   "image": "assets/images/dailyExpense.png",
    //   "keyword":"PurchasesOut",
    //   "path": "/purchasein-list"
    // }
   ];
    return(purchases);
  }
  getTaskManagerMenu(){
    const task = [{
      "image": 'assets/images/category.png',
      'heading': 'Assign Task by Department',
      "description": 'Assign Tasks/work by Category E.g: Finance, Marketing, Sales etc.',
      "action":'/create-task-category'
    },
    // {
    //   "image": 'assets/images/farm-map.jpg',
    //   'heading': 'Assign by Block',
    //   "description": 'Assign Tasks by blocks segregated  ',
    //   "action":'/create-task-by-block'
    // },
    {
      "image": 'assets/images/male.png',
      'heading': 'Assign Task by Employee',
      "description": 'Assign tasks based on Employee',
      "action":'/create-task-labour'
    },
    // {
    //   "image": 'assets/images/task.jpg',
    //   'heading': 'Assign by Date',
    //   "description": 'Assign tasks based on date',
    //   "action":'/create-task-by-date'
    // }
  ];
    return(task);
  }
  getFarmDetails(){
    const labels = [
      // {
      // 'image': 'assets/images/task.jpg',
      // 'action': '/farm-land-records-list',
      // 'heading': 'Land Records',
      // 'keyword': 'Land Records',
      // 'description': 'Manage all Land records and katha details'
      // },
      // {
      //   'image': 'assets/images/cultivator.png',
      //   'action': '/farm-land-records-list',
      //   'heading': 'Farm Assets',
      //   'keyword': 'Farm Assets',
      //   'description': 'Store all fixed assets and movable assets  '
      // },
      // {
      //   'image': 'assets/images/water.jpg',
      //   'action': '/farm-land-records-list',
      //   'heading': 'Farm Utility Details',
      //   'keyword': 'Farm Utility Details',
      //   'description': 'Store any EB. Water and other utility bills'
      // },
      // {
      //   'image': 'assets/images/certifying-clipart-banner-6.png',
      //   'action': '/farm-land-records-list',
      //   'heading': 'Licenses',
      //   'keyword': 'Licenses',
      //   'description': 'Any licenses and the related documentation of the farm'
      // },
      {
        'image': 'assets/images/Farm-Base-Map.png',
        'action': 'farmeasy-boarding-menu',
        'heading': 'Onboarding/Setup',
        'description': 'Manage the entire Setup'
      },
      // {
      //   'image': 'assets/images/farmmap.jpg',
      //   'action': '/farm-land-records-list',
      //   'heading': 'Miscellaneous',
      //   'keyword': 'Miscellaneous',
      //   'description': 'Any other documents that doesnt fit in above categories'
      // }
      ];
    return(labels);
  }
  getAccountsManagerLabels(){
    let labels = [
      // {
      // 'image': 'assets/images/salary_payments.jpeg',
      // 'action': 'salary-advances',
      // 'heading': 'Salary Payments',
      // 'description': 'Enter all salary payments made to the employees'
      // },
      // {
      //   'image': 'assets/images/loan.jpeg',
      //   'action': 'farmeasy-labour-advance',
      //   'heading': 'Loans & Advances',
      //   'description': 'Any advance given to labourers during the month'
      // },
      {
        "heading": "Material Purchases In",
        "description": "Any purchases for the including any machinery / equipment purchase",
        "image": "assets/images/cultivator.png",
        "keyword":"PurchasesIn",
        "action": "/purchasein-list"
      },
      {
        "heading": "Material Purchases Out",
        "description": "Track and manage any material going out of the stores",
        "image": "assets/images/dailyExpense.png",
        "keyword":"PurchasesOut",
        "action": "/purchasein-list"
      },
      {
        'image': 'assets/images/sales_report.png',
        'action': 'sales-reconciliation',
        'heading': 'Daily Reconciliation of Sales',
        "keyword":"Daily Reconciliation of Sales",
        'description': 'Reconcile daily sales figures and provide sign off',
      },
	    {
        'image': 'assets/images/in_and_out.jpg',
        'action': 'expenses-reconciliation',
        'heading': 'Daily Reconciliation of Expenses',
        "keyword": "Daily Reconciliation of Expenses",
        'description': 'Reconcile daily expenses and provide sign off',
      },
	   ];
    return(labels)
  }
  getEnterObservation(){
    let message = 'Your Observation is recorded successfully and the field supervisor has been notified of the further action that needs to be taken.';
    let labels = ['Select a Block','Enter Problem Description','Priority','Submit',message,'select File','Enter Problem Description','Block','Ok','Cancel','Select a value', 'Proof of the Observation'];
    return(labels);
  }
  getBudgetApproval(){
    const budget = ['Select Purpose', 'select category', 'Amount', 'Justification', 'Submit','Range From', 'Range To','Enter Justification'];
    return(budget);
  }
  getBudgetApprovalView(){
    const budgetview = ['Purpose', 'Product', 'Justification', 'Amount', 'Images', 'Approve', 'Reject','No Request For Budget Approval'];
    return(budgetview);
  }
  getDailyExpense(){
    const daily = ['Select Expense Category','Expense Description','Amount [INR]','Date of Expense','Justification', 'Submit', 'Enter Text'];
    return(daily);
  }
  getLabourAdvance(){
    const labAdvance = ['Select Expense Category','Labour Name','Monthly Sal','Adv paid','new Adv','Submit'];
    return(labAdvance);
  }
  getHarvestOutInvoice(){
    const invoice = ['Harvest Out - Invoice','Srini Food Park','Mallik arjuna rao Karge ,121,  Chittoor Main Road','Andhra Pradesh, Chittor','Category',
    'Subcategory', 'Rate','WellNest Natural Farms','JP Corp, Bellary Road, Sada shiva nagar',' Bangalore, Karnataka, India.', 'Registration No','Print Invoice','Total'];
    return(invoice);
  }
  getHarvestIn(){
    const harvestIn = ['Date of Receiving','Select Labour','Date of Transaction','Harvest  Type',
    'select category','Bill NO','Customer Name','Contact Person','Select Sub-Category','UOM','QTY','Rate',
    'Review Entry','Please select all fields','Description','ADD ANOTHER CATEGORY'];
    return(harvestIn);
  }
  getHarvestPrintPopup(){
    const popup = ['Bill/Invoice','#219/11,JP Corp,Bellary Road,Sadshiva nagar,Bangalore,Karnataka, India.','Category','Subcategory','UOM','QTY','Rate',
    'Note: This is an electronically generated Invoice and doesnt need signature','Print','Total'];
    return(popup);
  }
  getHarvestReviewPopup(){
    const reviewpopup = ['Confirm Review','Labourer Name:','Category Name:','SubCategory Name:','Date of Receiving:','UOM:','Quantity:','Cancel','Okay','Rate:'];
    return(reviewpopup);
  }
  getCreateEmployeeLabels(){
    const labels = {
      'heading':'Create Employee',
      'title':'Employee ID Card',
      'name':'Employee Name',
      'gender':'- Select a Gender -',
      'dob':'Date of Birth',
      'address':'Residential Address',
      'phone':'Contact Number',
      'aadhar':'Aadhar Number',
      'email':'Email ID',
      'proof':'Add ID Proof',
      'bank':'Add Bank Details',
      'language':'Language Preference',
      'details':'Employment Details',
      'doj':'Date of Joining',
      'designation':'- Designation -',
      'salary':'Monthly Salary',
      'branch':'Branch Name',
      'department':'Department',
      // 'designation':'Designation',
      'skill':'Employee skills',

      'manager':'- Manager -',
      'password':'Password',
      'block':'-select Block-',
      'submit':'submit',
      'update':'update',
      'delete':'delete',
      'found':'User Already exist',
      'known_lang':'Known Languages'
      }
    return(labels);
  }
  getViewObservationLabels(){ 
    const view = {
      'image':'Image',
      'video':'Video',
      'action':'Action History on Observation',
      'acknowledge':'Acknowledge',
      'convert':'Convert to Task'
      };
    return(view);
  }
  getLiveStockMenu(){
    const menu = [{
      'name': 'Vegetables',
      'icon': 'assets/images/carrot.svg',
      'action': '/farmeasy-livestock-view',
    },
    {
      'name': 'Fruits',
      'icon': 'assets/images/watermelon.svg',
      'action': '/farmeasy-livestock-view'
    },
    {
      'name': 'Crops',
      'icon': 'assets/images/crops.svg',
      'action': '/farmeasy-livestock-view'
    },
    {
      'name': 'Greens',
      'icon': 'assets/images/green-tea.svg',
      'action': '/farmeasy-livestock-view'
    }];;
    return(menu);
  }
  getHarvestReportLabels(){
    const reports = ['Type','From Date','To Date','report','compare'];
    return(reports);
  }
  getCreateTaskLabourLabels(){
    const labels = {
      labour:'employee',
      labour_place:'-Select Labour-',
      date:'Date',
      block:'Block',
      block_place:'- Select Block -',
      priority:'Priority',
      priority_place:'- Select Priority -',
      adhoc:'Adhoc Task',
      adhoc_place:'- Select Task -',
      select_lab_date:'Please Select Labour and Date',
     }
    return(labels);
  }
  getFrequencySelectionLabels(){
    const labels = {
      frequency:'Frequency',
      freq_place:'- Select Frequency -',
      dayof:' Day(s) of the week',
      dayofm:'Day(s) of the Month',
      start:'Start Date',
      end:'End Date',
      start_time:'Start Time',
      end_time:'End Time',

      ok:'Okay',
      cancel:'Cancel'
      }
    return(labels);
  }
  getMyTaskLabels(){
    const labels = {
      title:'My Tasks',
      place:'Search',
      water:'Water the plants in',
      Block:'Block',
      plots:'plots',
      Acknowledge:'Acknowledge',
      Acknowledged:'Acknowledged',
      done:'Mark as Done',
      completed:'completed',
      clean:'Clean in',
      notasks:'There are no tasks yet'
      }
    return(labels);
  }
  getTaskListLabels(){
    const labels = {
      water:'Water the plants in',
      place:'Enter Search criteria',
      clean:'Clean in',
      block:'Block',
      plots:'plots',
      Acknowledge:'Acknowledge',
      Acknowledged:'Acknowledged',
      done:'Mark as Done',
      completed:'completed',
      select:'-select values -'
      }
    return(labels);
  }
  getTaskPlannerLabels(){
    const labels =  {
select_freq:'- Select Frequency -',
Filter:'Filter',
image:'image',
Block:'Block',
Category:'Category',
Description:'Description',
Assignee:'Assignee',
Status:'Status',
Open:'Open',
Closed:'Closed',
NowShowing:'Now Showing',
nores:'No results found',
}
    return(labels);
  }
  getTaskUpdateHistoryLabels(){
    const labels = {
      title:'Task Update History',
      assign:'Assigned the Task to',
      task_mark:'The task has been marked as',
      completed:'The task has been marked as completed',
      updated:'The task has been updated'
      }
    return(labels);
  }
  getAddBlockLabels(){
    const labels = {
      title:'Task Update History',
      assign:'Assigned the Task to',
      task_mark:'The task has been marked as',
      completed:'The task has been marked as completed',
      updated:'The task has been updated'
      }
    return(labels);
  }
  getAddLiveStockLabels(){
    const labels = {
      name:'Name',
      gst:'GST',
      block:'Block',
      submit:'Submit',
      update:'Update',
      delete:'Delete',
      };
    return(labels);
  }
  
  getCreateNotificationLabels(){
    let labels = {
      notification: "Notification Message",
      priority:"Priority",
      priority_place:"Select a value",
      employee:'Select Employees',
      selectAll:'Select All',
      alert:'Please select fields',
      submit:"Submit"
    }
    return(labels)
  }

  getPriority(){
    let labels = [{name:'Urgent',view:'Urgent'},
                  {name:'High',view:'High'},
                  {name:'Medium',view:'Medium'},
                  {name:'Low',view:'Low'}];
    return labels;
  }
}
