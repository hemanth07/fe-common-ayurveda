import { Injectable } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { Http,Headers ,RequestOptions } from '@angular/http';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File, IWriteOptions } from "@ionic-native/file/ngx";
import { Device } from "@ionic-native/device/ngx";
import { Platform  } from '@ionic/angular';
//general
//const notification_url = 'http://104.237.2.124:3000/send';
//const invoice_url = 'http://104.237.2.124:3000/invoice';

//dev
const notification_url = 'http://104.237.2.124:3001/send';
const invoice_url = 'http://104.237.2.124:3001/invoice';
const report_url = 'http://104.237.2.124:3001/reports';

//wellnest
//const notification_url = 'http://104.237.2.124:3002/send';
// const invoice_url = 'http://104.237.2.124:3002/invoice';

@Injectable({
  providedIn: 'root'
})

export class OtherService {

    constructor(private sg: SimpleGlobal,
                private http:Http,
                private file:File,
                private device: Device,
                private fileOpener:FileOpener,
                private platform:Platform
                ) { }
                
  getColors(){
      
      let colors = [ "#000000","#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
      "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
      "#5A0007", "#809693", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
      "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
      "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
      "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
      "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
      "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
    
      "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
      "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
      "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
      "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
      "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
      "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
      "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
      "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"];

      return colors;
  }

  splitMultipleTasks(vendor_id,raw_task,date){
    const days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ];
    let temp_task_list = [];
    var today_day = days[new Date().getDay()];
    if(today_day === 'undefined') {
      today_day = "Saturday";
    }
    var dateString = new Date().toISOString().split("T")[0]
    var todayMilliSec = new Date(dateString).getTime();
    var today_date = new Date().getDate();
    var current_month = new Date().getMonth()+1;
    if(raw_task.blocks){
      for(let block of raw_task.blocks){
        if(block.plots){
          for(let plot of block.plots){
            if(plot.assignee){
              let task = {...raw_task};
              task['blocks'] = [{name:block.name,plots:[{name:plot.name,source:plot.source}]}]
              task['assignee'] = plot.assignee;
              task['log'] = true;
              task['date'] = date;
              delete task['multiple_labours'];
              delete task['start_date'];
              delete task['end_date'];
              if(task.frequency){
                    if(task.frequency.name == "Weekly" && task.frequency.daysOfWeek){
                          for(let day of task.frequency.daysOfWeek){
                             if(day.name == today_day){
                                  console.log(task.description);
                                  temp_task_list.push(task);
                             }
                          }
                    }
                    else if(task.frequency.name == "Monthly"){
                      for(let day of task.frequency.dates){
                          if(day == today_date){
                               console.log(task.description);
                               temp_task_list.push(task);
                           }
                       }
                    } 
                    else if(task.frequency.name == "Daily"){
                      console.log(task.description);
                      temp_task_list.push(task);
                    }
                    else if(task.frequency.name == "Yearly"){
                      if(task.frequency.date == today_date && task.frequency.month ==current_month)
                      {
                        console.log(task.description);
                        temp_task_list.push(task);
                      }
                   }
                   else if(task.frequency.name == "One Time"){
                    if(task.date == todayMilliSec){
                      console.log(task.description);
                      temp_task_list.push(task);
                    }
                  }
              }
           
            }
          }
        }
      }
    }
    if(temp_task_list.length>0){
      let headers = new Headers( );  
      headers.append( 'Content-Type','application/json');
     let options = new RequestOptions({
       headers: headers                
     });
      for(let task of temp_task_list){
        if(task.assignee.device_id){
          this.http.post(notification_url,{device_id:task.assignee.device_id,title:'FarmEasy',body:task.description},options).subscribe(resp=>{
            console.log(resp);
          })
        }
        firebase.database().ref(vendor_id+'/taskManager/task_table').push(task);
      }
    }
    setTimeout(()=>{
      this.createTaskLogs(vendor_id)
    },1000)
  // log.info(temp_task_list);
  // for(let task of temp_task_list){
  //   let headers = new Headers( );  
  //             headers.append( 'Content-Type','application/json');
  //            let options = new RequestOptions({
  //              headers: headers                
  //            });
  //   if(task.assignee.device_id){
  //     this.http.post(notification_url,{device_id:task.assignee.device_id,title:'FarmEasy',body:task.description},options).subscribe(resp=>{
  //       console.log(resp);
  //     })
  //   }
  //   firebase.database().ref(vendor_id+'/taskManager/task_table').push(task);
  //   }
  }

  createTaskLogs(vendor_id){
    firebase.database().ref(vendor_id+'/taskManager/task_table').orderByChild('log').equalTo(true).once('value',resp =>{
      let latest_tasks = snapshotToArray(resp);
      for(let task of latest_tasks){
        let task_log = JSON.parse(JSON.stringify(task));
        let body = {task_id:task_log.key,status:task_log.status,created:task_log.created,assignee:task_log.assignee,new:true};
        firebase.database().ref(vendor_id+'/taskManager/task_logTable').push(body);
        firebase.database().ref(vendor_id+'/taskManager/task_table/'+task.key+'/log').remove();
        firebase.database().ref(vendor_id+'/taskManager/task_table/'+task.key+'/key').remove();
      }
      });
    }

  createNewTaskFromBulkTasks(vendor_id,task,date){
      let headers = new Headers( );  
              headers.append( 'Content-Type','application/json');
              let options = new RequestOptions({
               headers: headers                
             });
    let temp_task_list = [];
    task['date'] = date; 
    task['log'] = true;
    delete task['start_date'];
    delete task['end_date'];
    temp_task_list.push(task);
    // log.info(temp_task_list);
    for(let task of temp_task_list){
      if(task.assignee.device_id){
        this.http.post(notification_url,{device_id:task.assignee.device_id,title:'FarmEasy',body:task.description},options).subscribe(resp=>{
          console.log(resp);
        })
      }
      firebase.database().ref(vendor_id+'/taskManager/task_table').push(task);
      }
    }

    sendNotification(task){
      let headers = new Headers( );  
              headers.append( 'Content-Type','application/json');
             let options = new RequestOptions({
               headers: headers                
             });
      if(task.assignee){
        if(task.assignee.device_id){
          this.http.post(notification_url,{device_id:task.assignee.device_id,title:'FarmEasy',body:task.description},options).subscribe(resp=>{
            console.log(resp);
          });
        }
      }
      if(task.device_id){
        this.http.post(notification_url,{device_id:task.device_id,title:'FarmEasy',body:task.description},options).subscribe(resp=>{
          console.log(resp);
        });
      }
    }
    getInvoice(body){
      let headers = new Headers( );  
      headers.append( 'Content-Type','application/json'); 
      let options = new RequestOptions({
       headers: headers                
     });
      return new Promise(resolve => {
        this.http.post(invoice_url,body,options).subscribe(response => {
          // response.header("Access-Control-Allow-Origin", "*");
              resolve(response);
            });
          });
    }

    getReports(body){
      let headers = new Headers( );  
      headers.append( 'Content-Type','application/json'); 
      let options = new RequestOptions({
       headers: headers             
     });
     options['responseType'] = 3; 
      return new Promise(resolve => {
        this.http.post(report_url,body,options).subscribe(response => {
          // response.header("Access-Control-Allow-Origin", "*");
              resolve(response);
            });
          });
    }

    openFile(path){
      let fileExtn=path.split('.').reverse()[0];
      let fileMIMEType=this.getMIMEtype(fileExtn);
      this.fileOpener.open(path, fileMIMEType)
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
    }
    createExcelandOpen(fileName,blob){
     let filePath 
      switch (this.device.platform) {
        case "Android":
          filePath = "file:///storage/emulated/0/FarmEasy/";
          break;
        case "iOS":
          filePath = this.file.documentsDirectory+'/Farmeasy/';
          break;
      }
      // this.platform.ready().then(() =>{
      //   if(this.platform.is('android')) {
      //     this.file.checkDir(this.file.externalRootDirectory, 'Event').then(response => {
      //       console.log('Directory exists'+response);
      //     }).catch(err => {
      //       console.log('Directory doesn\'t exist'+JSON.stringify(err));
      //       this.file.createDir(this.file.externalRootDirectory, 'Event', false).then(response => {
      //         console.log('Directory create'+response);
      //       }).catch(err => {
      //         console.log('Directory no create'+JSON.stringify(err));
      //       }); 
      //     });
      //   }
      // });
      let fileExtn=fileName.split('.').reverse()[0];
      let fileMIMEType=this.getMIMEtype(fileExtn);
      let promise = new Promise((resolve, reject)=>{
        this.file.writeFile(filePath, fileName, blob, { replace: true });
        setTimeout(()=>{
          // alert("fileCreated");
          resolve('Completed');
        },1000)
       });
      promise.then(()=>{
        this.fileOpener.showOpenWithDialog(filePath+fileName, fileMIMEType)
          .then(() => console.log('File is opened'))
          .catch(err => alert('Error openening file: ' + JSON.stringify(err)));
      });
      
    }
    getMIMEtype(extn){
      let ext=extn.toLowerCase();
      let MIMETypes={
        'txt' :'text/plain',
        'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'doc' : 'application/msword',
        'pdf' : 'application/pdf',
        'jpg' : 'image/jpeg',
        'bmp' : 'image/bmp',
        'png' : 'image/png',
        'xls' : 'application/vnd.ms-excel',
        'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'rtf' : 'application/rtf',
        'ppt' : 'application/vnd.ms-powerpoint',
        'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      }
      return MIMETypes[ext];
    }
getTaskSuggestions(){
let suggestions = [
"Block A, Plot A1 pour water daily.",
"Do clean in Block B weekly once.",
"apply fertilizer in block b once in a month.",
"gather all the crops in block D once in a year.",
"Do plantation of plants in Block E for one time.",
"prune the branches in block d for one time.",
" Solar power water source is not working.",
"Keep all the products in store room after gathering.",
"Use water source Ganga Bhavani for the tasks.",
"not getting water from Ganga Bhavani please inform for repairing the connection.",
"pack the products which is in storeroom.",
"place everything in Guest House.",
"Take Guests to Guest house to take rest.",
"Open the gate of Cowshed.",
"Keep water for each Cow.",
"Keep grass to each cow to eat.",
"Tie cows.",
"Clean cow's place.",
"Take cow dung from each cow and put it in a corner.",
"Wash the Cows with water and solution .",
"Wash the place where cows stay.",
"To protect cows and goats from insects and mosquitoes do switch on insect catcher at 6pm.",
"Paint the Cow's horns.",
"do milking from cows.",
"clean urine and dung from cows.",
"Take away cows for grazing.",
"Bring back cows from grazing.",
"Clean the goats place.",
"food to Goats .",
"Do milking from Goat .",
"Collect Goat dung and store it in a corner.",
"Take the goats for grazing .",
"Bring back goats from grazing area.",
"Take away cows for injection.",
"Use cow dung and goat dung for the farms as manure.",
"Clean block A with Rotavator.",
"Clean block b Manually.",
"with bush cutter  do block c clean.",
"Gather products from block A with hand.",
"Plant the plants by hand in block A.",
"plant the seeds through tractor in block B.",
"No current because transformer bursted.",
"Dig a place for the transformer pole.",
"Do repair the transformer.",
"Monitor the water accross the farm.",
"Monitor cleaning accross the farm.",
"Monitor harvesting accross the farm.",
"Monitor planting accross the farm.",
"Monitor Other tasks accross the farm.",
"Task Completed.",
"Task not completed due to rain.",
"Task not completed due to Power cut.",
"Task not completed due to Machine repair.",
"repair in Tractor.",
"Tractor punchure.",
"purchase 100 liters of diesel.",
"Purchase 100 liters of petrol.",
"Purchase 50 liters of Oil for motor.",
"Purchase 50 liters of Oil for Vehicles.",
"If the water is stop in Plot untie it.",
"Check the flow of water in all the plots.",
"Cover the line of mud with plastic cover & also for further plantation of seeds or plants into the soil make a hole.",
"Open the block its closed with mud,for water to enter into the block.",
"For water to stay in block close it with mud.",
"Check the water connection working properly or not.",
"check water pipe everywhere.",
"Check the blocks/plots if there is any blockage with mud or any other things.",
"plough through tractor.", 
"Plough the field with bull or Ox.",
"seedlings to plant from nursery Bring.",
"Switch on the main switch.",
"Switch on Motor.",
"Switch off the main switch.",
"Turn off the motor.",
"Milk the cows through Milking machine.",
"Milk the Goats through Milking machine.",
"Store milk of cows in can 1.",
"Store milk of goats in can 2.",
"Purchase of milk testing machine.",
"Send the milk of goat & cow seperately for testing.",
"After testing milk take it to diary.",
"Plant tomato seeds,after it grows to plant tie it with thread to a stick or top of the plant.",
"Take tomatoes from the plants after its grown up.",
"Tie the rope to cow's nose and neck.",
"Tie the rope to goats neck .",
"Remove rope of calf and let it go drink milk from its mother.",
"Remove rope of goat and let it go drink milk from its mother.",
"Count the number of cows and goats before & after taking to grazing.",
"do milking from cows and goats morning and evening.",
"Take the cow dung make it into round shape and paste it to the wall & use it after it gets dried.",
"Purchase of Soap,shampoo,brush etc for cows & goats.",
"Serve food for workers.",
"Prepare food for workers.",
"Bring tools after completing the work.",
"Keep glass & plates in guest house.",
"everyone please come to office at 9am,i will give explaination about tasks.",
"everyone please join at 6pm every saturday for the salary.",
"come to office on 05th of every month for the salary.",
"Salary will be deposited today please check your accounts.",
"Salary will be deposit tomorrow.",
"Salary will be deposited day after tomorrow.",
"Salary deposited yesterday.",
"Salary will be deposited next week.",
"collect all the foods in one bucket for cows to eat.",
"Come to weekly meeting, now stop working for now, continue work after meeting.",
"Check whether grass is there or not in cow's grass place.",
"Check whether water is there or not in cowshed.",
"Check whether cow powder is there or not in cow shed.",
"Check whether water is there or not in Goat area.",
"Check whether eatables plants is there or not in Goats shelter for goats to eat.",
"Collect the dung of cows.",
"Collect the dung of goats.",
"Sharpen all the tools.",
"Change fish tank water.",
"Do Clean the fish tank.",
" food for fishes.",
"Wear gloves while sprinkling fertilizer in the field.",
"One person should stay in the field to look after the crops at night time - Security required.",
"Change tractor wheels to Cage wheels for operation.",
"Pack fishes of different types each seperately.",
"Bring petrol to field to fill the machine.",
"Bring diesel To field to fill tractor.",
"Bring tractor oil for tractor.",
"On the lights every evening at 6 pm.",
"Off the lights every morning at 7am.",
"Put boundaries for the farm with help of stones and barbed wire.",
" Today's work is postponed due to rain.",
"It has rained today dont supply water to plants.",
"Tomorrow is holiday.",
"Place all the crops/products inside store room as it is raining.",
"Apply fertilizer to the farm in block F.",
"Remove weeds in block A.",
"Switch on the UPS or Generator.",
"Off the UPS/Generator."];
return suggestions;
}

  // split multiple tasks end

}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};














// let pdfOutput = resp['_body'];
// // using ArrayBuffer will allow you to put image inside PDF
// let buffer = new ArrayBuffer(pdfOutput.length);
// let array = new Uint8Array(buffer);
// for (var i = 0; i < pdfOutput.length; i++) {
//     array[i] = pdfOutput.charCodeAt(i);
// }
// const directory = this.file.dataDirectory ;
// const fileName = "Invoice.pdf";
// let options: IWriteOptions = { replace: true };
// this.file.checkFile(directory, fileName).then((success)=> {
//   // Writing File to Device
//   this.file.writeFile(directory,fileName,buffer, options)
//   .then((success)=> {
//     alert("File created Succesfully" + JSON.stringify(success));
//     this.fileOpener.open(this.file.dataDirectory + fileName, 'application/pdf')
//       .then(() => alert('File is opened'))
//       .catch(e => alert('Error opening file', e));
//   })
//   .catch((error)=> {
//    alert(error);
//   //  console.log("Cannot Create File " +JSON.stringify(error));
//   });
// }).catch((error)=> {
//   // Writing File to Device
//   this.file.writeFile(directory,fileName,buffer)
//   .then((success)=> {
//    // console.log("File created Succesfully" + JSON.stringify(success));
//     this.fileOpener.open(this.file.dataDirectory + fileName, 'application/pdf')
//       .then(() => console.log('File is opened'))
//       .catch(e => console.log('Error opening file', e));
//   })
//   .catch((error)=> {
//     console.log("Cannot Create File " +JSON.stringify(error));
//   });
// }).catch(function (error) {
// this.popoverController.dismiss();
// console.error('oops, something went wrong!', error);
// });
// console.log(resp);




