import { Injectable } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';

@Injectable({
  providedIn: 'root'
})
export class KanadaService {
 constructor(private sg: SimpleGlobal) { }
 getDashboard(){
  if(this.sg['userdata'].role === 'corporate'){
    const dashboard = [{
   "title": "ಕೃಷಿ ಅವಲೋಕನಗಳು",
   "subtitle": "ಜಮೀನಿನಲ್ಲಿ ನಿರ್ದಿಷ್ಟ ಆವಿಷ್ಕಾರಗಳನ್ನು ವರದಿ ಮಾಡಿ",
   "image": "assets/images/observations.jpg",
   "path": "/farmeasy-observation-list"
 },
 {
   "title": "ಕಾರ್ಯ ನಿರ್ವಹಣೆ",
   "subtitle": " ಯೋಜನೆ, ನಿಯೋಜಿಸಿ ಮತ್ತು ಸಂಪೂರ್ಣ ಕಾರ್ಯಗಳು / ಕೆಲಸ",
   "image": "assets/images/task.jpg",
   "path": "/task-planner" 
 },
 {
   'title': 'ವರದಿಗಳು / ವಿಶ್ಲೇಷಣೆ',
   'subtitle': 'ವರದಿಗಳನ್ನು ನಿರ್ವಹಿಸಿ ಮತ್ತು ಎಲ್ಲಾ ಕೃಷಿ ವಿವರಗಳ ವಿಶ್ಲೇಷಣೆ',
   'image': 'assets/images/sales_report.png',
   "path": "/farmeasy-reports-menu"
 },
 {
   "title": "ಫಾರ್ಮ್ ಮಾಹಿತಿ",
   "subtitle": "ಸಂಪೂರ್ಣ ಕೃಷಿ ಸಂಬಂಧಿತ ಡೇಟಾ ಮತ್ತು ಅದರ ಕಾರ್ಯಾಚರಣೆಗಳನ್ನು ನಿರ್ವಹಿಸಿ",
   "image": "assets/images/in_and_out.jpg",
   "path": "/farm-details-menu"
 },
 {
   'title': 'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿಗಳು',
   'subtitle': 'ಎಲ್ಲಾ ದೈನಂದಿನ ವೆಚ್ಚಗಳು ಮತ್ತು ಕೃಷಿ ಖರೀದಿಗಳನ್ನು ಇಲ್ಲಿ ನಮೂದಿಸಿ',
   'image': 'assets/images/expense.jpg',
   "path": "/farmeasy-expense-manager"
 },
 {
   "title": "ಮಾರಾಟ ಮತ್ತು ಉತ್ಪಾದನೆ",
   "subtitle": "ಸ್ಟಾಕ್ ಇನ್, ಸ್ಟಾಕ್, ಟ್ ಮತ್ತು ಮಾರಾಟವನ್ನು ನಿರ್ವಹಿಸಿ",
   "image": "assets/images/management.jpg",
   "path": '/harvest'
 },
//  {
//   "title": "Asset Management",
//   "subtitle": "Manage Material In, Material Out",
//   'path': '/assets-management-menu',
//   "image": "assets/images/farmcleaning.jpg",
//  },
 {
   "title": "ಖಾತೆಗಳು / ಹಣಕಾಸು",
   "subtitle": "ಜಮೀನಿನಲ್ಲಿ ಹಣದ ಹರಿವನ್ನು ನಿರ್ವಹಿಸಿ",
   "image": "assets/images/accounts.jpg",
   "path": '/account-manager-menu'
 }];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'manager'){
  const dashboard = [{
    "title": "ಕೃಷಿ ಅವಲೋಕನಗಳು",
    "subtitle": "ಜಮೀನಿನಲ್ಲಿ ನಿರ್ದಿಷ್ಟ ಆವಿಷ್ಕಾರಗಳನ್ನು ವರದಿ ಮಾಡಿ",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "ಕಾರ್ಯ ನಿರ್ವಹಣೆ",
    "subtitle": " ಯೋಜನೆ, ನಿಯೋಜಿಸಿ ಮತ್ತು ಸಂಪೂರ್ಣ ಕಾರ್ಯಗಳು / ಕೆಲಸ",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  },
  {
    'title': 'ವರದಿಗಳು / ವಿಶ್ಲೇಷಣೆ',
    'subtitle': 'ವರದಿಗಳನ್ನು ನಿರ್ವಹಿಸಿ ಮತ್ತು ಎಲ್ಲಾ ಕೃಷಿ ವಿವರಗಳ ವಿಶ್ಲೇಷಣೆ',
    'image': 'assets/images/sales_report.png',
    "path": "/farmeasy-reports-menu"
  },
  {
    "title": "ಫಾರ್ಮ್ ಮಾಹಿತಿ",
    "subtitle": "ಸಂಪೂರ್ಣ ಕೃಷಿ ಸಂಬಂಧಿತ ಡೇಟಾ ಮತ್ತು ಅದರ ಕಾರ್ಯಾಚರಣೆಗಳನ್ನು ನಿರ್ವಹಿಸಿ",
    "image": "assets/images/in_and_out.jpg",
    "path": "/farm-details-menu"
  },
  {
    'title': 'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿಗಳು',
    'subtitle': 'ಎಲ್ಲಾ ದೈನಂದಿನ ವೆಚ್ಚಗಳು ಮತ್ತು ಕೃಷಿ ಖರೀದಿಗಳನ್ನು ಇಲ್ಲಿ ನಮೂದಿಸಿ',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  },
  {
    "title": "ಮಾರಾಟ ಮತ್ತು ಉತ್ಪಾದನೆ",
    "subtitle": "ಸ್ಟಾಕ್ ಇನ್, ಸ್ಟಾಕ್, ಟ್ ಮತ್ತು ಮಾರಾಟವನ್ನು ನಿರ್ವಹಿಸಿ",
    "image": "assets/images/management.jpg",
    "path": '/harvest'
  },
  {
    "title": "ಖಾತೆಗಳು / ಹಣಕಾಸು",
    "subtitle": "ಜಮೀನಿನಲ್ಲಿ ಹಣದ ಹರಿವನ್ನು ನಿರ್ವಹಿಸಿ",
    "image": "assets/images/accounts.jpg",
    "path": '/account-manager-menu'
  }];
return(dashboard);
} else if(this.sg['userdata'].role === 'accounts'){
const dashboard = [
{
  'title': 'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿಗಳು',
  'subtitle': 'ಎಲ್ಲಾ ದೈನಂದಿನ ವೆಚ್ಚಗಳು ಮತ್ತು ಕೃಷಿ ಖರೀದಿಗಳನ್ನು ಇಲ್ಲಿ ನಮೂದಿಸಿ',
  'image': 'assets/images/expense.jpg',
  "path": "/farmeasy-expense-manager"
},
{
  "title": "ಕೃಷಿ ಉತ್ಪಾದನೆ",
  "subtitle": "ಸ್ಟಾಕ್ ಇನ್, ಸ್ಟಾಕ್, ಟ್ ಮತ್ತು ಮಾರಾಟವನ್ನು ನಿರ್ವಹಿಸಿ",
  "image": "assets/images/management.jpg",
  "path": '/harvest'
},
{
  "title": "ಖಾತೆಗಳು / ಹಣಕಾಸು",
  "subtitle": "ಜಮೀನಿನಲ್ಲಿ ಹಣದ ಹರಿವನ್ನು ನಿರ್ವಹಿಸಿ",
  "image": "assets/images/accounts.jpg",
  "path": '/account-manager-menu'
},
{
  'title': 'ವರದಿಗಳು / ವಿಶ್ಲೇಷಣೆ',
  'subtitle': 'ವರದಿಗಳನ್ನು ನಿರ್ವಹಿಸಿ ಮತ್ತು ಎಲ್ಲಾ ಕೃಷಿ ವಿವರಗಳ ವಿಶ್ಲೇಷಣೆ',
  'image': 'assets/images/sales_report.png',
  "path": "/farmeasy-reports-menu"
},
{
  "title": "ಫಾರ್ಮ್ ಮಾಹಿತಿ",
  "subtitle": "ಸಂಪೂರ್ಣ ಕೃಷಿ ಸಂಬಂಧಿತ ಡೇಟಾ ಮತ್ತು ಅದರ ಕಾರ್ಯಾಚರಣೆಗಳನ್ನು ನಿರ್ವಹಿಸಿ",
  "image": "assets/images/in_and_out.jpg",
  "path": "/farm-details-menu"
}];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'supervisor'){
  const dashboard = [{
    "title": "ಕೃಷಿ ಅವಲೋಕನಗಳು",
    "subtitle": "ಜಮೀನಿನಲ್ಲಿ ನಿರ್ದಿಷ್ಟ ಆವಿಷ್ಕಾರಗಳನ್ನು ವರದಿ ಮಾಡಿ",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "ಕಾರ್ಯ ನಿರ್ವಹಣೆ",
    "subtitle": " ಯೋಜನೆ, ನಿಯೋಜಿಸಿ ಮತ್ತು ಸಂಪೂರ್ಣ ಕಾರ್ಯಗಳು / ಕೆಲಸ",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  },
  {
    'title': 'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿಗಳು',
    'subtitle': 'ಎಲ್ಲಾ ದೈನಂದಿನ ವೆಚ್ಚಗಳು ಮತ್ತು ಕೃಷಿ ಖರೀದಿಗಳನ್ನು ಇಲ್ಲಿ ನಮೂದಿಸಿ',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  }
];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'employee'){
  const dashboard = [{
    "title": "ಕೃಷಿ ಅವಲೋಕನಗಳು",
    "subtitle": "ಜಮೀನಿನಲ್ಲಿ ನಿರ್ದಿಷ್ಟ ಆವಿಷ್ಕಾರಗಳನ್ನು ವರದಿ ಮಾಡಿ",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "ಕಾರ್ಯ ನಿರ್ವಹಣೆ",
    "subtitle": " ಯೋಜನೆ, ನಿಯೋಜಿಸಿ ಮತ್ತು ಸಂಪೂರ್ಣ ಕಾರ್ಯಗಳು / ಕೆಲಸ",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  }];
   return(dashboard);
 } 
}
 getHamburger(){
 const hamburger = {
 profile: 'ನನ್ನ ಪ್ರೊಫೈಲ್',
 notification:'ಅಧಿಸೂಚನೆಗಳು',
 settings: 'ಸೆಟ್ಟಿಂಗ್‌ಗಳು',
 about: 'ಫಾರ್ಮಸಿ ಬಗ್ಗೆ',
 sync: 'ಡೇಟಾವನ್ನು ಸಿಂಕ್ ಮಾಡಿ',
 lagout: 'ಲಾಗ್ out ಟ್',
 switch:'ಭಾಷೆ ಬದಲಿಸಿ',
 dashboard: 'ಡ್ಯಾಶ್‌ಬೋರ್ಡ್',
 support: 'ಬೆಂಬಲ / ಪ್ರತಿಕ್ರಿಯೆ',
 plant_history: 'ಸಸ್ಯಗಳ ಇತಿಹಾಸ',
 change_pass:'ಗುಪ್ತಪದವನ್ನು ಬದಲಿಸಿ',
 version: 'ಆವೃತ್ತಿ',
 terms:'ದಿನಾಂಕ'   
 };
 return(hamburger);
 }
getOnBoarding(){
 const onboarding = [{
 'image': 'assets/images/admin.jpg',
 'heading': 'ನೌಕರರ ದಾಖಲೆಗಳು',
 'description':'ಜಮೀನಿನಲ್ಲಿರುವ ಸಿಬ್ಬಂದಿಗಳ ಎಲ್ಲಾ ಉದ್ಯೋಗ ದಾಖಲೆಗಳನ್ನು ನಿರ್ವಹಿಸಿ',
 'action': '/farmeasy-manage-labours',
 },
 {
 'image': 'assets/images/tasks.jpg',
 'heading': 'ವರ್ಗಗಳ ಸೆಟಪ್',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ಎಲ್ಲಾ ವರ್ಗದ ಕೆಲಸಗಳನ್ನು ಹೊಂದಿಸಿ',
 'action': '/farmeasy-view-category',
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'heading': 'ನಿರ್ಬಂಧಗಳ ನಿರ್ಬಂಧ',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ಬ್ಲಾಕ್‌ಗಳು ಮತ್ತು ವಿಭಾಗಗಳನ್ನು ಹೊಂದಿಸಿ',
 'action': '/farmeasy-view-blocks',
 },
 {
 'image': 'assets/images/rotavator.jpg',
 'heading': 'ಸಂಪನ್ಮೂಲಗಳ ಸೆಟಪ್',
 'description':'ಜಮೀನಿನಲ್ಲಿ ಬಳಸಲು ಎಲ್ಲಾ ಸಂಪನ್ಮೂಲಗಳನ್ನು ಹೊಂದಿಸಿ',
 'action': '/farmeasy-view-source',
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'heading': 'ಫಾರ್ಮ್ ನಕ್ಷೆ / ವಿನ್ಯಾಸ',
 'description': 'ಕೃಷಿ ಮತ್ತು ಅದರ ವಿನ್ಯಾಸವನ್ನು ಗುರುತಿಸಿ',
 'action': '/farm-map',
 }, 
 ];
 return(onboarding);
 }
 getExpense(){
   let expense;
  if(this.sg['userdata'].role === 'supervisor'){
 expense = [{
  'image': 'assets/images/dailyExpense.png',
  'action': 'view-daily-expense',
  'heading': 'ಜಮೀನಿನಲ್ಲಿ ದೈನಂದಿನ ವೆಚ್ಚಗಳು',
  'keyword':'Daily Expenses',
  'description': 'ದಿನನಿತ್ಯದ ವೆಚ್ಚವನ್ನು ಇಲ್ಲಿ ಲಾಗ್ ಇನ್ ಮಾಡಲು'
  },
  {
  'image': 'assets/images/farm-map.jpg',
  'action': 'farm-purchases-list',
  'heading': 'ಕೃಷಿ ಖರೀದಿಗಳು',
  'keyword':'Farm Purchases',
  'description': 'ಯಾವುದೇ ಯಂತ್ರೋಪಕರಣಗಳು / ಸಲಕರಣೆಗಳ ಖರೀದಿ ಸೇರಿದಂತೆ ಜಮೀನಿಗೆ ಯಾವುದೇ ಖರೀದಿಗಳು'
  }];
  } else {
 expense = [{
  'image': 'assets/images/dailyExpense.png',
  'action': 'view-daily-expense',
  'heading': 'ಜಮೀನಿನಲ್ಲಿ ದೈನಂದಿನ ವೆಚ್ಚಗಳು',
  'keyword':'Daily Expenses',
  'description': 'ದಿನನಿತ್ಯದ ವೆಚ್ಚವನ್ನು ಇಲ್ಲಿ ಲಾಗ್ ಇನ್ ಮಾಡಲು'
  },
  {
  'image': 'assets/images/farm-map.jpg',
  'action': 'farm-purchases-list',
  'heading': 'ಕೃಷಿ ಖರೀದಿಗಳು',
  'keyword':'Farm Purchases',
  'description': 'ಯಾವುದೇ ಯಂತ್ರೋಪಕರಣಗಳು / ಸಲಕರಣೆಗಳ ಖರೀದಿ ಸೇರಿದಂತೆ ಜಮೀನಿಗೆ ಯಾವುದೇ ಖರೀದಿಗಳು'
  },
  {
  'image': 'assets/images/salary_payments.jpeg',
  'action': 'salary-advances',
  'heading': 'ಸಂಬಳ ಪಾವತಿಗಳು',
  'keyword': 'Salary Payments',
  'description': 'ನೌಕರರಿಗೆ ಮಾಡಿದ ಎಲ್ಲಾ ವೇತನ ಪಾವತಿಗಳನ್ನು ನಮೂದಿಸಿ'
  },
  {
  'image': 'assets/images/loan.jpeg',
  'action': 'farmeasy-labour-advance',
  'heading': 'ಸಾಲಗಳು ಮತ್ತು ಮುಂಗಡಗಳು',
  'keyword': 'Loans & Advances',
  'description': 'ತಿಂಗಳಲ್ಲಿ ಕಾರ್ಮಿಕರಿಗೆ ನೀಡುವ ಯಾವುದೇ ಮುಂಗಡ'
  } ];
  }
 return(expense);
 }
 getReportsMenu(){
 const reports = [{
 'heading': 'ಮಾರಾಟ ಮತ್ತು ಸಂಗ್ರಹ ವರದಿ',
 'description': 'ಹಾಳಾಗುವ ವಸ್ತುಗಳನ್ನು ಒಳಗೊಂಡಂತೆ ಜಮೀನಿನಲ್ಲಿ ಮಾರಾಟ',
 'image': './assets/images/sales_report.png',
 'action': '/sales-collections-reports'
 },
 {
 'heading':'ವೆಚ್ಚಗಳು ಮತ್ತು ಖರೀದಿಗಳ ವರದಿ',
 'description':'ಯಾವುದೇ ಯಂತ್ರೋಪಕರಣಗಳು / ಸಲಕರಣೆಗಳ ಖರೀದಿ ಸೇರಿದಂತೆ ಜಮೀನಿಗೆ ಯಾವುದೇ ಖರೀದಿಗಳು',
 'image': './assets/images/expense.jpg',
 'action': '/expense-purchases-reports'
 },
 {
 'heading':'ಒಳಹರಿವು ಮತ್ತು ಹೊರಹರಿವಿನ ವರದಿ',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ಹಣದ ಒಳಹರಿವು ಮತ್ತು ಹೊರಹರಿವನ್ನು ಟ್ರ್ಯಾಕ್ ಮಾಡಿ',
 'image': './assets/images/in_and_out.jpg',
 'action': '/inflow-outflow-reports'
 },
 {
 'heading': 'ಸ್ಟಾಕ್ ಇನ್ವೆಂಟರಿ ವರದಿ',
 'description': 'ಸ್ಟಾಕ್ ಇನ್ ವೀಕ್ಷಿಸಿ, ಸ್ಟಾಕ್ and ಟ್ ಮತ್ತು ಸ್ಟಾಕ್ ಆನ್ ಹ್ಯಾಂಡ್ ವಿವರಗಳು',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-inventory-report-menu'
 },
 {
 'heading': 'ಕೃಷಿ ವೀಕ್ಷಣಾ ವರದಿಗಳು',
 'description': 'ಜಮೀನಿನಲ್ಲಿರುವ ಎಲ್ಲಾ ತೋಟಗಳನ್ನು ಟ್ರ್ಯಾಕ್ ಮಾಡಿ ಮತ್ತು ಅವುಗಳನ್ನು ನಿರ್ವಹಿಸಿ',
 'image': './assets/images/plantation_new.jpg',
 'action': '/farm-activity-report-menu'
 }];
 return(reports);
 }
 getFarmActivityMenu(){
 const reports = [{
 'heading':'ಕೃಷಿ ಅವಲೋಕನ ವರದಿ',
 'keyword': 'observation-reports',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ವರದಿಯಾದ ಎಲ್ಲಾ ಅವಲೋಕನಗಳನ್ನು ಟ್ರ್ಯಾಕ್ ಮಾಡಿ',
 'image': './assets/images/in_and_out.jpg',
 'action': '/farmeasy-all-observations'
 },
 {
 'heading': 'ನೀರಿನ ವರದಿ',
 'keyword': 'watering-reports',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ನೀರುಹಾಕುವುದನ್ನು ಮೇಲ್ವಿಚಾರಣೆ ಮಾಡಿ',
 'image': './assets/images/watering_reports.jpg',
 'action': '/watering-reports'
 },
 {
 'heading': 'ಫಲೀಕರಣ ವರದಿ',
 'keyword': 'fertigation-reports',
 'description':'ಜಮೀನಿನಲ್ಲಿ ಮಾಡಿದ ಫಲೀಕರಣವನ್ನು ಮೇಲ್ವಿಚಾರಣೆ ಮಾಡಿ',
 'image': './assets/images/fertigation.jpeg',
 'action': '/watering-reports'
 },
//  {
//  'keyword': 'labour-work-assignment',
//  'heading':'ಕಾರ್ಮಿಕ ಕೆಲಸದ ನಿಯೋಜನೆ',
//  'description': 'ಜಮೀನಿನಲ್ಲಿರುವ ಕಾರ್ಮಿಕರಿಗೆ ನಿಯೋಜಿಸಲಾದ ಕೆಲಸವನ್ನು ವೀಕ್ಷಿಸಿ',
//  'image': './assets/images/male.png',
//  'action': '/resource-planning'
//  },
 {
 'heading': 'ಕಾರ್ಯ ವರದಿ',
 'keyword': 'task-reports',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ನಿರ್ವಹಿಸಲಾದ ಎಲ್ಲಾ ಕಾರ್ಯಗಳನ್ನು ವೀಕ್ಷಿಸಿ',
 'image': './assets/images/task.jpg',
 'action': '/task-reports'
 }
 ];
 return(reports);
 }
 getStockInventoryMenu(){
 const reports = [{
 'heading': 'ಸ್ಟಾಕ್ ಇನ್',
 'description': 'ಜಮೀನಿನಲ್ಲಿ ಮಾಡಿದ ಎಲ್ಲಾ ಸುಗ್ಗಿಯನ್ನು ಪತ್ತೆ ಮಾಡಿ ಮತ್ತು ಅವುಗಳನ್ನು ನಿರ್ವಹಿಸಿ',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-in'
 },
 {
 'heading': 'ಸ್ಟಾಕ್ Out ಟ್',
 'description': 'ಜಮೀನಿನಲ್ಲಿರುವ ಎಲ್ಲಾ ತೋಟಗಳನ್ನು ಟ್ರ್ಯಾಕ್ ಮಾಡಿ ಮತ್ತು ಅವುಗಳನ್ನು ನಿರ್ವಹಿಸಿ',
 'image': './assets/images/stock_out.png',
 'action': '/stock-out'
 },
 {
 'heading':'ಸ್ಟಾಕ್ ಆನ್ ಹ್ಯಾಂಡ್',
 'description': 'ಹಾಳಾಗುವ ವಸ್ತುಗಳನ್ನು ಒಳಗೊಂಡಂತೆ ಜಮೀನಿನಲ್ಲಿ ಮಾರಾಟ',
 'image': './assets/images/stockonhand.jpg',
 'action': '/stock-in-hand'
 }
 ];
 return(reports);
 }
 getHarvestMenu(){
 const harvest=[{
 "title": "ಸ್ಟಾಕ್ ಇನ್",
 "title1": "stock in",
 "subtitle": "ಜಮೀನಿನಲ್ಲಿ ಮಾಡಿದ ಎಲ್ಲಾ ಸುಗ್ಗಿಯನ್ನು ಪತ್ತೆ ಮಾಡಿ ಮತ್ತು ಅವುಗಳನ್ನು ನಿರ್ವಹಿಸಿ",
 "image": "assets/images/harvest-in.png",
 "path": "/harvestin"
 },
 {
 "title":"ಮಾರಾಟ",
 "title1":"sales",
 "subtitle": "ಯಾವುದೇ ಯಂತ್ರೋಪಕರಣಗಳು / ಸಲಕರಣೆಗಳ ಖರೀದಿ ಸೇರಿದಂತೆ ಜಮೀನಿಗೆ ಯಾವುದೇ ಖರೀದಿಗಳು",
 "image": "assets/images/cultivator.png",
 "path": "/harvestout"
 },
 {
  "title": "ಬಳಸಿದ ಸ್ಟಾಕ್ ಮತ್ತು ಹಾನಿ",
  "title1": "stock out",
  "subtitle": "ಜಮೀನಿನಲ್ಲಿ ಹೊರಹೋಗುವ ಎಲ್ಲಾ ಸುಗ್ಗಿಯನ್ನು ಪತ್ತೆಹಚ್ಚಿ ಮತ್ತು ಅವುಗಳನ್ನು ನಿರ್ವಹಿಸಿ",
  "image": "assets/images/food.jpg",
  "path": "/harvestout"
  }
 ];
 return(harvest);
 }
 getAssestsManagementMenu(){
 const purchases=[
 {
 "title": "ವಸ್ತು ಖರೀದಿಗಳು",
 "subtitle": "ಯಾವುದೇ ಯಂತ್ರೋಪಕರಣಗಳು / ಸಲಕರಣೆಗಳ ಖರೀದಿ ಸೇರಿದಂತೆ ಜಮೀನಿಗೆ ಯಾವುದೇ ಖರೀದಿಗಳು",
 "image": "assets/images/cultivator.png",
 "keyword":"PurchasesIn",
 "path": "/purchasein-list"
 },
 {
 "title": "ವಸ್ತು ಖರೀದಿಗಳು ಟ್", 
 "subtitle":"ಅಂಗಡಿಗಳಿಂದ ಹೊರಹೋಗುವ ಯಾವುದೇ ವಸ್ತುಗಳನ್ನು ಟ್ರ್ಯಾಕ್ ಮಾಡಿ ಮತ್ತು ನಿರ್ವಹಿಸಿ",
 "image": "assets/images/dailyExpense.png",
 "keyword":"PurchasesOut",
 "path": "/purchasein-list"
 }
 ];
 return(purchases);
 }
 getTaskManagerMenu(){
 const task = [{
 "image": 'assets/images/category.png',
 'heading':'ವರ್ಗದಿಂದ ನಿಯೋಜಿಸಿ',
 "description": 'ವರ್ಗದಿಂದ ಕಾರ್ಯಗಳನ್ನು / ಕೆಲಸವನ್ನು ನಿಯೋಜಿಸಿ ಉದಾ: ನೀರುಹಾಕುವುದು, ಸ್ವಚ್ aning ಗೊಳಿಸುವುದು, ಕೊಯ್ಲು ಇತ್ಯಾದಿ.',
 "action":'/create-task-category'
 },
 {
 "image": 'assets/images/farm-map.jpg',
 'heading':'ಬ್ಲಾಕ್‌ನಿಂದ ನಿಯೋಜಿಸಿ',
 "description": 'ಜಮೀನಿನಲ್ಲಿ ಬೇರ್ಪಡಿಸಿದ ಬ್ಲಾಕ್ಗಳಿಂದ ಕಾರ್ಯಗಳನ್ನು ನಿಯೋಜಿಸಿ',
 "action":'/create-task-by-block'
 },
 {
 "image": 'assets/images/male.png',
 'heading': 'ಕಾರ್ಮಿಕರಿಂದ ನಿಯೋಜಿಸಿ',
 "description": 'ಜಮೀನಿನಲ್ಲಿ ಕೆಲಸ ಮಾಡುವ ಕಾರ್ಮಿಕರ ಆಧಾರದ ಮೇಲೆ ಕಾರ್ಯಗಳನ್ನು ನಿಯೋಜಿಸಿ',
 "action":'/create-task-labour'
 },
 {
 "image": 'assets/images/task.jpg',
 'heading':'ದಿನಾಂಕದಂದು ನಿಯೋಜಿಸಿ',
 "description": 'ದಿನಾಂಕವನ್ನು ಆಧರಿಸಿ ಕಾರ್ಯಗಳನ್ನು ನಿಯೋಜಿಸಿ',
 "action":'/create-task-by-date'
 }
 ];
 return(task);
 }
 getFarmDetails(){
 const labels = [{
 'image': 'assets/images/task.jpg',
 'action': '/farm-land-records-list',
 'heading': 'ಲ್ಯಾಂಡ್ ರೆಕಾರ್ಡ್ಸ್',
 'keyword': 'Land Records',
 'description': 'ಎಲ್ಲಾ ಭೂ ದಾಖಲೆಗಳು ಮತ್ತು ಕಥಾ ವಿವರಗಳನ್ನು ನಿರ್ವಹಿಸಿ'
 },
 {
 'image': 'assets/images/cultivator.png',
 'action': '/farm-land-records-list',
 'heading': 'ಫಾರ್ಮ್ ಸ್ವತ್ತುಗಳು',
 'keyword': 'Farm Assets',
 'description': 'ಎಲ್ಲಾ ಸ್ಥಿರ ಸ್ವತ್ತುಗಳು ಮತ್ತು ಚಲಿಸಬಲ್ಲ ಸ್ವತ್ತುಗಳನ್ನು ಜಮೀನಿನಲ್ಲಿ ಸಂಗ್ರಹಿಸಿ'
 },
 {
 'image': 'assets/images/water.jpg',
 'action': '/farm-land-records-list',
 'heading': 'ಫಾರ್ಮ್ ಯುಟಿಲಿಟಿ ವಿವರಗಳು',
 'keyword': 'Farm Utility Details',
 'description': 'ಯಾವುದೇ ಇಬಿ ಸಂಗ್ರಹಿಸಿ. ನೀರು ಮತ್ತು ಇತರ ಉಪಯುಕ್ತತೆ ಬಿಲ್‌ಗಳು '
 },
 {
 'image': 'assets/images/certifying-clipart-banner-6.png',
 'action': '/farm-land-records-list',
 'heading': 'ಪರವಾನಗಿಗಳು',
 'keyword': 'Licenses',
 'description': 'ಯಾವುದೇ ಪರವಾನಗಿಗಳು ಮತ್ತು ಜಮೀನಿನ ಸಂಬಂಧಿತ ದಸ್ತಾವೇಜನ್ನು'
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'action': 'farmeasy-boarding-menu',
 'heading':'ಫಾರ್ಮ್ ಆನ್‌ಬೋರ್ಡಿಂಗ್ / ಸೆಟಪ್',
 'description': 'ಸಂಪೂರ್ಣ ಕೃಷಿ ವಿನ್ಯಾಸ ಮತ್ತು ನಕ್ಷೆಯನ್ನು ನಿರ್ವಹಿಸಿ'
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'action': '/farm-land-records-list',
 'heading': 'ವಿವಿಧ',
 'keyword': 'Miscellaneous',
 'description': 'ಮೇಲಿನ ವರ್ಗಗಳಿಗೆ ಹೊಂದಿಕೆಯಾಗದ ಯಾವುದೇ ದಾಖಲೆಗಳು'
 }
 ];
 return(labels);
 }
 getAccountsManagerLabels(){
 let labels = [
 // {
 // 'image': 'assets/images/salary_payments.jpeg',
 // 'action': 'salary-advances',
 // 'heading': 'Salary Payments',
 // 'description': 'Enter all salary payments made to the employees'
 // },
 // {
 // 'image': 'assets/images/loan.jpeg',
 // 'action': 'farmeasy-labour-advance',
 // 'heading': 'Loans & Advances',
 // 'description': 'Any advance given to labourers during the month'
 // },
 {
 'image': 'assets/images/sales_report.png',
 'action': 'sales-reconciliation',
 'heading': 'ಮಾರಾಟದ ದೈನಂದಿನ ಸಾಮರಸ್ಯ',
 'description': 'ದೈನಂದಿನ ಮಾರಾಟ ಅಂಕಿಅಂಶಗಳನ್ನು ಮರುಸಂಗ್ರಹಿಸಿ ಮತ್ತು ಸೈನ್ ಆಫ್ ಒದಗಿಸಿ',
 },
   {
 'image': 'assets/images/in_and_out.jpg',
 'action': 'expenses-reconciliation',
 'heading': 'ಖರ್ಚುಗಳ ದೈನಂದಿನ ಸಾಮರಸ್ಯ',
 'description': 'ದೈನಂದಿನ ಖರ್ಚುಗಳನ್ನು ಮರುಸಂಗ್ರಹಿಸಿ ಮತ್ತು ಸೈನ್ ಆಫ್ ಒದಗಿಸಿ',
 },
   ];
 return(labels)
 }
 getEnterObservation(){
 let message = 'ನಿಮ್ಮ ವೀಕ್ಷಣೆಯನ್ನು ಯಶಸ್ವಿಯಾಗಿ ದಾಖಲಿಸಲಾಗಿದೆ ಮತ್ತು ಮುಂದಿನ ಕ್ರಮಗಳ ಬಗ್ಗೆ ಕ್ಷೇತ್ರ ಮೇಲ್ವಿಚಾರಕರಿಗೆ ತಿಳಿಸಲಾಗಿದೆ.';
 let labels = ['ಒಂದು ಬ್ಲಾಕ್ ಆಯ್ಕೆಮಾಡಿ', 'ಸಮಸ್ಯೆ ವಿವರಣೆಯನ್ನು ನಮೂದಿಸಿ', 'ಆದ್ಯತೆ', 'ಸಲ್ಲಿಸಿ', message,'ಫೈಲ್ ಆಯ್ಕೆಮಾಡಿ', 'ಸಮಸ್ಯೆ ವಿವರಣೆಯನ್ನು ನಮೂದಿಸಿ', 'ನಿರ್ಬಂಧಿಸು', 'ಸರಿ', 'ರದ್ದುಮಾಡು', ' ಮೌಲ್ಯವನ್ನು ಆಯ್ಕೆಮಾಡಿ ',' ವೀಕ್ಷಣೆಯ ಪುರಾವೆ ']; 
 return(labels);
 }
 getBudgetApproval(){
 const budget = ['ಉದ್ದೇಶವನ್ನು ಆರಿಸಿ', 'ವಿಭಾಗವನ್ನು ಆರಿಸಿ', 'ಮೊತ್ತ', 'ಸಮರ್ಥನೆ', 'ಸಲ್ಲಿಸು', 'ಶ್ರೇಣಿಯಿಂದ', 'ಶ್ರೇಣಿಯಿಂದ', 'ಸಮರ್ಥನೆಯನ್ನು ನಮೂದಿಸಿ'];
 return(budget);
 }
 getBudgetApprovalView(){
 const budgetview = ['ಉದ್ದೇಶ', 'ಉತ್ಪನ್ನ', 'ಸಮರ್ಥನೆ', 'ಮೊತ್ತ', 'ಚಿತ್ರಗಳು', 'ಅನುಮೋದಿಸು', 'ತಿರಸ್ಕರಿಸಿ', 'ಬಜೆಟ್ ಅನುಮೋದನೆಗೆ ಯಾವುದೇ ವಿನಂತಿಯಿಲ್ಲ'];
 return(budgetview);
 }
 getDailyExpense(){
 const daily = ['ಖರ್ಚು ವಿಭಾಗವನ್ನು ಆರಿಸಿ', 'ಖರ್ಚು ವಿವರಣೆ', 'ಮೊತ್ತ [INR]', 'ವೆಚ್ಚದ ದಿನಾಂಕ', 'ಸಮರ್ಥನೆ', 'ಸಲ್ಲಿಸು', 'ಪಠ್ಯವನ್ನು ನಮೂದಿಸಿ'];
 return(daily);
 }
 getLabourAdvance(){
 const labAdvance = ['ಖರ್ಚು ವಿಭಾಗವನ್ನು ಆರಿಸಿ', 'ಕಾರ್ಮಿಕ ಹೆಸರು', 'ಮಾಸಿಕ ಸಾಲ್', 'ಅಡ್ ಪಾವತಿಸಿದ', 'ಹೊಸ ಅಡ್', 'ಸಲ್ಲಿಸು'];
 return(labAdvance);
 }
 getHarvestOutInvoice(){
 const invoice = ['ಹಾರ್ವೆಸ್ಟ್ --ಟ್ - ಸರಕುಪಟ್ಟಿ', 'ಶ್ರೀನಿ ಫುಡ್ ಪಾರ್ಕ್', 'ಮಲ್ಲಿಕ್ ಅರ್ಜುನ ರಾವ್ ಕಾರ್ಗೆ, 121, ಚಿತ್ತೂರು ಮುಖ್ಯ ರಸ್ತೆ', 'ಆಂಧ್ರಪ್ರದೇಶ, ಚಿತ್ತೋರ್', 'ವರ್ಗ',
     'ಉಪವರ್ಗ', 'ದರ', 'ವೆಲ್‌ನೆಸ್ಟ್ ನ್ಯಾಚುರಲ್ ಫಾರ್ಮ್ಸ್', 'ಜೆ.ಪಿ.ಕಾರ್ಪ್, ಬಳ್ಳಾರಿ ರಸ್ತೆ, ಸದಾ ಶಿವ ನಗರ', 'ಬೆಂಗಳೂರು, ಕರ್ನಾಟಕ, ಭಾರತ.', 'ನೋಂದಣಿ ಸಂಖ್ಯೆ', 'ಮುದ್ರಣ ಸರಕುಪಟ್ಟಿ', 'ಒಟ್ಟು'];
 return(invoice);
 }
 getHarvestIn(){
 const harvestIn = ['ಸ್ವೀಕರಿಸುವ ದಿನಾಂಕ', 'ಕಾರ್ಮಿಕ ಆಯ್ಕೆಮಾಡಿ', 'ವಹಿವಾಟಿನ ದಿನಾಂಕ', 'ಹಾರ್ವೆಸ್ಟ್ ಪ್ರಕಾರ',
     'ವಿಭಾಗವನ್ನು ಆರಿಸಿ', 'ಬಿಲ್ ಇಲ್ಲ', 'ಗ್ರಾಹಕರ ಹೆಸರು', 'ಸಂಪರ್ಕ ವ್ಯಕ್ತಿ', 'ಉಪ-ವರ್ಗವನ್ನು ಆರಿಸಿ', 'UOM', 'QTY', 'ದರ',
     'ವಿಮರ್ಶೆ ಪ್ರವೇಶ', 'ದಯವಿಟ್ಟು ಎಲ್ಲಾ ಕ್ಷೇತ್ರಗಳನ್ನು ಆರಿಸಿ', 'ವಿವರಣೆ', 'ಮತ್ತೊಂದು ವರ್ಗವನ್ನು ಸೇರಿಸಿ'];
 return(harvestIn);
 }
 getHarvestPrintPopup(){
 const popup = ['ಬಿಲ್ / ಇನ್‌ವಾಯ್ಸ್', '# 219/11, ಜೆಪಿ ಕಾರ್ಪ್, ಬಳ್ಳಾರಿ ರಸ್ತೆ, ಸದ್ಶಿವ ನಗರ, ಬೆಂಗಳೂರು, ಕರ್ನಾಟಕ, ಭಾರತ.', 'ವರ್ಗ', 'ಉಪವರ್ಗ', 'UOM', 'QTY', ' ದರ',
     'ಗಮನಿಸಿ: ಇದು ವಿದ್ಯುನ್ಮಾನವಾಗಿ ರಚಿಸಲಾದ ಸರಕುಪಟ್ಟಿ ಮತ್ತು ಸಹಿ ಅಗತ್ಯವಿಲ್ಲ', 'ಮುದ್ರಿಸು', 'ಒಟ್ಟು'];
 return(popup);
 }
 getHarvestReviewPopup(){
 const reviewpopup =['ವಿಮರ್ಶೆಯನ್ನು ದೃ irm ೀಕರಿಸಿ', 'ಕಾರ್ಮಿಕರ ಹೆಸರು:', 'ವರ್ಗದ ಹೆಸರು:', 'ಉಪವರ್ಗದ ಹೆಸರು:', 'ಸ್ವೀಕರಿಸುವ ದಿನಾಂಕ:', 'UOM:', 'ಪ್ರಮಾಣ:', 'ರದ್ದುಮಾಡು', 'ಸರಿ ','ದರ:'];
 return(reviewpopup);
 }
 getCreateEmployeeLabels(){
 const labels = {
 'heading': 'ಉದ್ಯೋಗಿಯನ್ನು ರಚಿಸಿ',
 'title':'ನೌಕರರ ಗುರುತಿನ ಚೀಟಿ',
 'name': 'ನೌಕರರ ಹೆಸರು',
 'gender': '- ಲಿಂಗವನ್ನು ಆರಿಸಿ -',
 'dob': 'ಹುಟ್ಟಿದ ದಿನಾಂಕ',
 'address':'ವಸತಿ ವಿಳಾಸ',
 'phone':'ಸಂಪರ್ಕ ಸಂಖ್ಯೆ',
 'aadhar': 'ಆಧಾರ್ ಸಂಖ್ಯೆ',
 'email':'ಇಮೇಲ್ ಐಡಿ',
 'proof': 'ID ಪುರಾವೆ ಸೇರಿಸಿ',
 'bank': 'ಬ್ಯಾಂಕ್ ವಿವರಗಳನ್ನು ಸೇರಿಸಿ',
 'language': 'ಭಾಷಾ ಆದ್ಯತೆ',
 'details':'ಉದ್ಯೋಗ ವಿವರಗಳು',
 'doj':'ಸೇರುವ ದಿನಾಂಕ',
 'designation': '- ಹುದ್ದೆ -',
 'salary': 'ಮಾಸಿಕ ಸಂಬಳ',
 'manager': '- ಮ್ಯಾನೇಜರ್ -',
 'password': 'ಪಾಸ್‌ವರ್ಡ್',
 'block': '- ಬ್ಲಾಕ್- ಆಯ್ಕೆಮಾಡಿ',
 'submit':'ಸಲ್ಲಿಸು',
 'update': 'ನವೀಕರಿಸಿ',
 'delete': 'ಅಳಿಸು',
 'found': 'ಬಳಕೆದಾರರು ಈಗಾಗಲೇ ಅಸ್ತಿತ್ವದಲ್ಲಿದ್ದಾರೆ',
 'known_lang': 'ತಿಳಿದಿರುವ ಭಾಷೆಗಳು'
 }
 return(labels);
 }
 getViewObservationLabels(){ 
 const view = {
 'image':'ಚಿತ್ರ',
 'video':'ವಿಡಿಯೋ',
 'action':'ಆಕ್ಷನ್ ಹಿಸ್ಟರಿ ಆನ್ ಅಬ್ಸರ್ವೇಶನ್',
 'acknowledge':'ಅಂಗೀಕರಿಸು',
 'convert': 'ಕಾರ್ಯಕ್ಕೆ ಪರಿವರ್ತಿಸಿ'
 };
 return(view);
 }
 getLiveStockMenu(){
 const menu = [{
 'name': 'ತರಕಾರಿಗಳು',
 'icon': 'assets/images/carrot.svg',
 'action': '/farmeasy-livestock-view',
 },
 {
 'name': 'ಹಣ್ಣುಗಳು',
 'icon': 'assets/images/watermelon.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'ಬೆಳೆಗಳು',
 'icon': 'assets/images/crops.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'ಗ್ರೀನ್ಸ್',
 'icon': 'assets/images/green-tea.svg',
 'action': '/farmeasy-livestock-view'
 }];
 return(menu);
 }
 getHarvestReportLabels(){
 const reports = ['ಪ್ರಕಾರ', 'ದಿನಾಂಕದಿಂದ', 'ದಿನಾಂಕದಿಂದ', 'ವರದಿ', 'ಹೋಲಿಕೆ'];
 return(reports);
 }
 getCreateTaskLabourLabels(){
 const labels = {
 labour:'ಕಾರ್ಮಿಕ',
 labour_place:'- ಕಾರ್ಮಿಕರನ್ನು ಆರಿಸಿ-',
 date:'ದಿನಾಂಕ',
 block: 'ಬ್ಲಾಕ್',
 block_place: '- ಬ್ಲಾಕ್ ಆಯ್ಕೆಮಾಡಿ -',
 priority: 'ಆದ್ಯತೆ',
 priority_place:'- ಆದ್ಯತೆಯನ್ನು ಆರಿಸಿ -',
 adhoc:'Adhoc Task',
 adhoc_place: '- ಕಾರ್ಯ ಆಯ್ಕೆಮಾಡಿ -',
 select_lab_date:'ದಯವಿಟ್ಟು ಕಾರ್ಮಿಕ ಮತ್ತು ದಿನಾಂಕವನ್ನು ಆರಿಸಿ',
 }
 return(labels);
 }
 getFrequencySelectionLabels(){
 const labels = {
 frequency: 'ಆವರ್ತನ',
 freq_place: '- ಆವರ್ತನವನ್ನು ಆರಿಸಿ -',
 dayof:'ವಾರದ ದಿನ (ಗಳು)',
 dayofm:'ತಿಂಗಳ ದಿನ (ಗಳು)',
 start: 'ಪ್ರಾರಂಭ ದಿನಾಂಕ',
 end: 'ಅಂತಿಮ ದಿನಾಂಕ',
 ok: 'ಸರಿ',
 cancel:'ರದ್ದುಮಾಡು'
 }
 return(labels);
 }
 getMyTaskLabels(){
 const labels = {
 title: 'ನನ್ನ ಕಾರ್ಯಗಳು',
 place: 'ಹುಡುಕಾಟ',
 water:'ಸಸ್ಯಗಳಿಗೆ ನೀರು ಹಾಕಿ',
 Block: 'ನಿರ್ಬಂಧಿಸು',
 plots: 'ಪ್ಲಾಟ್ಗಳು',
 Acknowledge: 'ಅಂಗೀಕರಿಸು',
 Acknowledged: 'ಅಂಗೀಕರಿಸಲಾಗಿದೆ',
 done: 'ಮುಗಿದಂತೆ ಗುರುತಿಸಿ',
 completed: 'ಪೂರ್ಣಗೊಂಡಿದೆ',
 clean: 'ಕ್ಲೀನ್ ಇನ್',
 notasks:'ಇನ್ನೂ ಯಾವುದೇ ಕಾರ್ಯಗಳಿಲ್ಲ'
 }
 return(labels);
 }
 getTaskListLabels(){
 const labels = {
 water:'ಸಸ್ಯಗಳಿಗೆ ನೀರು ಹಾಕಿ',
 place:'ಹುಡುಕಾಟ ಮಾನದಂಡಗಳನ್ನು ನಮೂದಿಸಿ',
 clean: 'ಕ್ಲೀನ್ ಇನ್',
 block: 'ಬ್ಲಾಕ್',
 plots:'ಪ್ಲಾಟ್ಗಳು',
 Acknowledge: 'ಅಂಗೀಕರಿಸು',
 Acknowledged: 'ಅಂಗೀಕರಿಸಲಾಗಿದೆ',
 done: 'ಮುಗಿದಂತೆ ಗುರುತಿಸಿ',
 completed: 'ಪೂರ್ಣಗೊಂಡಿದೆ',
 select: '- ಮೌಲ್ಯಗಳನ್ನು ಆರಿಸಿ -'
 }
 return(labels);
 }
getTaskPlannerLabels(){
 const labels = {
select_freq:'- ಆವರ್ತನವನ್ನು ಆರಿಸಿ -',
Filter: 'ಫಿಲ್ಟರ್',
image: 'ಚಿತ್ರ',
Block: 'ನಿರ್ಬಂಧಿಸು',
Category:'ವರ್ಗ',
Description:'ವಿವರಣೆ',
Assignee:'ನಿಯೋಜಕ',
Status:'ಸ್ಥಿತಿ',
Open: 'ಓಪನ್',
Closed: 'ಮುಚ್ಚಲಾಗಿದೆ',
NowShowing:'ಈಗ ತೋರಿಸಲಾಗುತ್ತಿದೆ',
nores:'ಯಾವುದೇ ಫಲಿತಾಂಶಗಳು ಕಂಡುಬಂದಿಲ್ಲ'
}
return(labels);
 }
 getTaskUpdateHistoryLabels(){
 const labels = {
 title: 'ಕಾರ್ಯ ನವೀಕರಣ ಇತಿಹಾಸ',
 assign: 'ನಿಯೋಜಿಸಲಾದ ಕಾರ್ಯ',
 task_mark:'ಕಾರ್ಯವನ್ನು ಎಂದು ಗುರುತಿಸಲಾಗಿದೆ ',
 completed: 'ಕಾರ್ಯವನ್ನು ಪೂರ್ಣಗೊಳಿಸಲಾಗಿದೆ ಎಂದು ಗುರುತಿಸಲಾಗಿದೆ',
 updated: 'ಕಾರ್ಯವನ್ನು ನವೀಕರಿಸಲಾಗಿದೆ' 
 }
 return(labels);
 }
 getAddBlockLabels(){
 const labels = {
 title:'ಕಾರ್ಯ ನವೀಕರಣ ಇತಿಹಾಸ',
 assign:'ನಿಯೋಜಿಸಲಾದ ಕಾರ್ಯ',
 task_mark:'ಕಾರ್ಯವನ್ನು ಎಂದು ಗುರುತಿಸಲಾಗಿದೆ ',
 completed:'ಕಾರ್ಯವನ್ನು ಪೂರ್ಣಗೊಳಿಸಲಾಗಿದೆ ಎಂದು ಗುರುತಿಸಲಾಗಿದೆ',
 updated: 'ಕಾರ್ಯವನ್ನು ನವೀಕರಿಸಲಾಗಿದೆ'
 }
 return(labels);
 }
 getAddLiveStockLabels(){
 const labels = {
 name:'ಹೆಸರು',
 gst:'ಜಿಎಸ್ಟಿ',
 block:'ಬ್ಲಾಕ್',
 submit:'ಸಲ್ಲಿಸು',
 update:'ನವೀಕರಿಸಿ',
 delete:'ಅಳಿಸು',
 };
 return(labels);
 }
 getCreateNotificationLabels(){
  let labels = {
    notification: "ಅಧಿಸೂಚನೆ ಸಂದೇಶ",
    priority:"ಆದ್ಯತೆ",
    priority_place:"ಮೌಲ್ಯವನ್ನು ಆಯ್ಕೆಮಾಡಿ",
    employee:'ಉದ್ಯೋಗಿಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ',
    selectAll:'ಎಲ್ಲವನ್ನು ಆರಿಸು',
    alert:'ದಯವಿಟ್ಟು ಕ್ಷೇತ್ರಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ',
    submit:"ಸಲ್ಲಿಸು"
  }

 return(labels)
 }

 getPriority(){
let labels = [{name:'Urgent',view:'ತುರ್ತು'},
                  {name:'High',view:'ಹೈ'},
                  {name:'Medium',view:'ಮಧ್ಯಮ'},
                  {name:'Low',view:'ಕಡಿಮೆ'}];
 return labels;
 }
}


