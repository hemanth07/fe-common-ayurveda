import { Injectable } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';

@Injectable({
  providedIn: 'root'
})
export class TamilService {
 constructor(private sg: SimpleGlobal) { }
 getDashboard(){
  if(this.sg['userdata'].role === 'corporate'){
    const dashboard = [{
   "title": "பண்ணை அவதானிப்புகள்",
   "subtitle": "பண்ணையில் குறிப்பிட்ட கண்டுபிடிப்புகளைப் புகாரளிக்கவும்",
   "image": "assets/images/observations.jpg",
   "path": "/farmeasy-observation-list"
 },
 {
   "title": "பணி மேலாண்மை",
   "subtitle": "திட்டம், ஒதுக்கு மற்றும் முழுமையான பணிகள் / வேலை",
   "image": "assets/images/task.jpg",
   "path": "/task-planner" 
 },
 {
   'title': 'அறிக்கைகள் / அனலிட்டிக்ஸ்',
   'subtitle': 'அறிக்கைகள் மற்றும் அனைத்து பண்ணை விவரங்களின் பகுப்பாய்வையும் நிர்வகிக்கவும்',
   'image': 'assets/images/sales_report.png',
   "path": "/farmeasy-reports-menu"
 },
 {
   "title": "பண்ணை தகவல்",
   "subtitle": "பண்ணை தொடர்பான முழுமையான தரவு மற்றும் அதன் செயல்பாடுகளை நிர்வகிக்கவும்",
   "image": "assets/images/in_and_out.jpg",
   "path": "/farm-details-menu"
 },
 {
   'title': 'செலவுகள் மற்றும் கொள்முதல்',
   'subtitle': 'தினசரி செலவுகள் மற்றும் பண்ணை கொள்முதல் அனைத்தையும் இங்கே உள்ளிடவும்',
   'image': 'assets/images/expense.jpg',
   "path": "/farmeasy-expense-manager"
 },
 {
   "title": "விற்பனை மற்றும் உற்பத்தி",
   "subtitle": "ஸ்டாக் இன், ஸ்டாக் அவுட் மற்றும் விற்பனையை நிர்வகிக்கவும்",
   "image": "assets/images/management.jpg",
   "path": '/harvest'
 },
//  {
//   "title": "Asset Management",
//   "subtitle": "Manage Material In, Material Out",
//   'path': '/assets-management-menu',
//   "image": "assets/images/farmcleaning.jpg",
//  },
 {
   "title": "கணக்குகள் / நிதி",
   "subtitle": "பண்ணையில் பணப்புழக்கத்தை நிர்வகிக்கவும்",
   "image": "assets/images/accounts.jpg",
   "path": '/account-manager-menu'
 }];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'manager'){
  const dashboard = [{
    "title": "பண்ணை அவதானிப்புகள்",
    "subtitle": "பண்ணையில் குறிப்பிட்ட கண்டுபிடிப்புகளைப் புகாரளிக்கவும்",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "பணி மேலாண்மை",
    "subtitle": "திட்டம், ஒதுக்கு மற்றும் முழுமையான பணிகள் / வேலை",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  },
  {
    'title': 'அறிக்கைகள் / அனலிட்டிக்ஸ்',
    'subtitle': 'அறிக்கைகள் மற்றும் அனைத்து பண்ணை விவரங்களின் பகுப்பாய்வையும் நிர்வகிக்கவும்',
    'image': 'assets/images/sales_report.png',
    "path": "/farmeasy-reports-menu"
  },
  {
    "title": "பண்ணை தகவல்",
    "subtitle": "பண்ணை தொடர்பான முழுமையான தரவு மற்றும் அதன் செயல்பாடுகளை நிர்வகிக்கவும்",
    "image": "assets/images/in_and_out.jpg",
    "path": "/farm-details-menu"
  },
  {
    'title': 'செலவுகள் மற்றும் கொள்முதல்',
    'subtitle': 'தினசரி செலவுகள் மற்றும் பண்ணை கொள்முதல் அனைத்தையும் இங்கே உள்ளிடவும்',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  },
  {
    "title": "விற்பனை மற்றும் உற்பத்தி",
    "subtitle": "ஸ்டாக் இன், ஸ்டாக் அவுட் மற்றும் விற்பனையை நிர்வகிக்கவும்",
    "image": "assets/images/management.jpg",
    "path": '/harvest'
  },
  {
    "title": "கணக்குகள் / நிதி",
    "subtitle": "பண்ணையில் பணப்புழக்கத்தை நிர்வகிக்கவும்",
    "image": "assets/images/accounts.jpg",
    "path": '/account-manager-menu'
  }];
return(dashboard);
} else if(this.sg['userdata'].role === 'accounts'){
const dashboard = [
{
  'title': 'செலவுகள் மற்றும் கொள்முதல்',
  'subtitle': 'தினசரி செலவுகள் மற்றும் பண்ணை கொள்முதல் அனைத்தையும் இங்கே உள்ளிடவும்',
  'image': 'assets/images/expense.jpg',
  "path": "/farmeasy-expense-manager"
},
{
  "title": "பண்ணை உற்பத்தி",
  "subtitle": "ஸ்டாக் இன், ஸ்டாக் அவுட் மற்றும் விற்பனையை நிர்வகிக்கவும்",
  "image": "assets/images/management.jpg",
  "path": '/harvest'
},
{
  "title": "கணக்குகள் / நிதி",
  "subtitle": "பண்ணையில் பணப்புழக்கத்தை நிர்வகிக்கவும்",
  "image": "assets/images/accounts.jpg",
  "path": '/account-manager-menu'
},
{
  'title': 'அறிக்கைகள் / அனலிட்டிக்ஸ்',
  'subtitle': 'அறிக்கைகள் மற்றும் அனைத்து பண்ணை விவரங்களின் பகுப்பாய்வையும் நிர்வகிக்கவும்',
  'image': 'assets/images/sales_report.png',
  "path": "/farmeasy-reports-menu"
},
{
  "title": "பண்ணை தகவல்",
  "subtitle": "பண்ணை தொடர்பான முழுமையான தரவு மற்றும் அதன் செயல்பாடுகளை நிர்வகிக்கவும்",
  "image": "assets/images/in_and_out.jpg",
  "path": "/farm-details-menu"
}];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'supervisor'){
  const dashboard = [{
    "title": "பண்ணை அவதானிப்புகள்",
    "subtitle": "பண்ணையில் குறிப்பிட்ட கண்டுபிடிப்புகளைப் புகாரளிக்கவும்",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "பணி மேலாண்மை",
    "subtitle": "திட்டம், ஒதுக்கு மற்றும் முழுமையான பணிகள் / வேலை",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  },
  {
    'title': 'செலவுகள் மற்றும் கொள்முதல்',
    'subtitle': 'தினசரி செலவுகள் மற்றும் பண்ணை கொள்முதல் அனைத்தையும் இங்கே உள்ளிடவும்',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  }
];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'employee'){
  const dashboard = [{
    "title": "பண்ணை அவதானிப்புகள்",
    "subtitle": "பண்ணையில் குறிப்பிட்ட கண்டுபிடிப்புகளைப் புகாரளிக்கவும்",
    "image": "assets/images/observations.jpg",
    "path": "/farmeasy-observation-list"
  },
  {
    "title": "பணி மேலாண்மை",
    "subtitle": "திட்டம், ஒதுக்கு மற்றும் முழுமையான பணிகள் / வேலை",
    "image": "assets/images/task.jpg",
    "path": "/task-planner" 
  }];
   return(dashboard);
 } 
}
 getHamburger(){
 const hamburger = {
 profile: 'அறிவிப்புகளை',
 notification: 'அமைப்புகள்',
 settings:'ஃபார்மேஸி பற்றி',
 about: 'தரவு ஒத்திசை',
 sync:'வெளியேறு',
 lagout: 'வெளியேறு',
 switch:'மொழி மாறு',
 dashboard:'டாஷ்போர்டு',
 support: 'ஆதரவு / கருத்து',
 plant_history:'தாவரங்களின் வரலாறு',
 change_pass:'கடவுச்சொல்லை மாற்று',
 version:'பதிப்பு',
 terms:'தேதி'
 };
 return(hamburger);
 }
     
getOnBoarding(){
 const onboarding = [{
 'image': 'assets/images/admin.jpg',
 'heading': 'பணியாளர் பதிவுகள்',
 'description': 'பண்ணையில் உள்ள ஊழியர்களின் அனைத்து வேலைவாய்ப்பு பதிவுகளையும் நிர்வகிக்கவும்',
 'action': '/farmeasy-manage-labours',
 },
 {
 'image': 'assets/images/tasks.jpg',
 'heading':'வகைகள் அமைவு',
 'description': 'பண்ணையில் அனைத்து வகை வேலைகளையும் அமைக்கவும்',
 'action': '/farmeasy-view-category',
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'heading': 'தொகுதிகள் அமைவு',
 'description': 'பண்ணையில் தொகுதிகள் மற்றும் பகிர்வுகளை அமைக்கவும்',
 'action': '/farmeasy-view-blocks',
 },
 {
 'image': 'assets/images/rotavator.jpg',
 'heading': 'வள அமைப்பு',
 'description': 'பண்ணையில் பயன்படுத்த வேண்டிய அனைத்து வளங்களையும் அமைக்கவும்',
 'action': '/farmeasy-view-source',
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'heading': 'பண்ணை வரைபடம் / தளவமைப்பு',
 'description': 'பண்ணை மற்றும் அதன் தளவமைப்பைக் குறிக்கவும்',
 'action': '/farm-map',
 }, 
 ];
 return(onboarding);
 }
 getExpense(){
  let expense;
  if(this.sg['userdata'].role === 'supervisor'){
    expense = [{
      'image': 'assets/images/dailyExpense.png',
      'action': 'view-daily-expense',
      'heading': 'பண்ணையில் தினசரி செலவுகள்',
      'keyword':'Daily Expenses',
      'description': 'இங்கே உள்நுழைய வேண்டிய அன்றாட செலவு'
      },
      {
      'image': 'assets/images/farm-map.jpg',
      'action': 'farm-purchases-list',
      'heading': 'பண்ணை கொள்முதல்',
      'keyword':'Farm Purchases',
      'description': 'எந்திரங்கள் / உபகரணங்கள் வாங்குவது உட்பட பண்ணைக்கு ஏதேனும் கொள்முதல்'
      }];
  } else {
    expense = [{
      'image': 'assets/images/dailyExpense.png',
      'action': 'view-daily-expense',
      'heading': 'பண்ணையில் தினசரி செலவுகள்',
      'keyword':'Daily Expenses',
      'description': 'இங்கே உள்நுழைய வேண்டிய அன்றாட செலவு'
      },
      {
      'image': 'assets/images/farm-map.jpg',
      'action': 'farm-purchases-list',
      'heading': 'பண்ணை கொள்முதல்',
      'keyword':'Farm Purchases',
      'description': 'எந்திரங்கள் / உபகரணங்கள் வாங்குவது உட்பட பண்ணைக்கு ஏதேனும் கொள்முதல்'
      },
      {
      'image': 'assets/images/salary_payments.jpeg',
      'action': 'salary-advances',
      'heading': 'சம்பள கொடுப்பனவுகள்',
      'keyword': 'Salary Payments',
      'description':'ஊழியர்களுக்கு செய்யப்படும் அனைத்து சம்பள கொடுப்பனவுகளையும் உள்ளிடவும்'
      },
      {
      'image': 'assets/images/loan.jpeg',
      'action': 'farmeasy-labour-advance',
      'heading': 'கடன்கள் மற்றும் முன்னேற்றங்கள்',
      'keyword': 'Loans & Advances',
      'description': 'மாதத்தில் தொழிலாளர்களுக்கு வழங்கப்படும் எந்த முன்கூட்டியே'
      },
      ];
  }

 return(expense);
 }
 getReportsMenu(){
 const reports = [{
 'heading': 'விற்பனை மற்றும் சேகரிப்பு அறிக்கை',
 'description': 'அழிந்துபோகக்கூடிய பொருட்கள் உட்பட பண்ணையில் விற்பனை',
 'image': './assets/images/sales_report.png',
 'action': '/sales-collections-reports'
 },
 {
 'heading':'செலவுகள் மற்றும் கொள்முதல் அறிக்கை',
 'description': 'எந்திரங்கள் / உபகரணங்கள் வாங்குவது உட்பட பண்ணைக்கு ஏதேனும் கொள்முதல்',
 'image': './assets/images/expense.jpg',
 'action': '/expense-purchases-reports'
 },
 {
 'heading':'வரத்து மற்றும் வெளிச்செல்லும் அறிக்கை',
 'description':'பண்ணையில் பணப்பரிமாற்றம் மற்றும் வெளியேற்றத்தைக் கண்காணிக்கவும்',
 'image': './assets/images/in_and_out.jpg',
 'action': '/inflow-outflow-reports'
 },
 {
 'heading': 'பங்கு சரக்கு அறிக்கை',
 'description': 'ஸ்டாக் இன், ஸ்டாக் அவுட் மற்றும் ஸ்டாக் ஹேண்ட் விவரங்களைக் காண்க',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-inventory-report-menu'
 },
 {
 'heading':'பண்ணை கண்காணிப்பு அறிக்கைகள்',
 'description': 'பண்ணையில் உள்ள அனைத்து தோட்டங்களையும் கண்காணித்து அவற்றை நிர்வகிக்கவும்',
 'image': './assets/images/plantation_new.jpg',
 'action': '/farm-activity-report-menu'
 }];
 return(reports);
 }
 getFarmActivityMenu(){
 const reports = [{
 'heading':'பண்ணை அவதானிப்பு அறிக்கை',
 'keyword': 'observation-reports',
 'description': 'பண்ணையில் தெரிவிக்கப்பட்ட அனைத்து அவதானிப்புகளையும் கண்காணிக்கவும்',
 'image': './assets/images/in_and_out.jpg',
 'action': '/farmeasy-all-observations'
 },
 {
 'heading': 'நீர்ப்பாசன அறிக்கை',
 'keyword': 'watering-reports',
 'description': 'பண்ணை முழுவதும் செய்யப்படும் நீர்ப்பாசனத்தைக் கண்காணிக்கவும்',
 'image': './assets/images/watering_reports.jpg',
 'action': '/watering-reports'
 },
 {
 'heading': 'கருத்தரித்தல் அறிக்கை',
 'keyword': 'fertigation-reports',
 'description':'பண்ணை முழுவதும் செய்யப்படும் கருத்தரிப்பைக் கண்காணிக்கவும்',
 'image': './assets/images/fertigation.jpeg',
 'action': '/watering-reports'
 },
//  {
//  'keyword': 'labour-work-assignment',
//  'heading': 'தொழிலாளர் பணி நியமனம்',
//  'description': 'பண்ணையில் உள்ள தொழிலாளர்களுக்கு ஒதுக்கப்பட்ட வேலையைக் காண்க',
//  'image': './assets/images/male.png',
//  'action': '/resource-planning'
//  },
 {
 'heading': 'பணி அறிக்கை',
 'keyword': 'task-reports',
 'description': 'பண்ணையில் நிர்வகிக்கப்படும் அனைத்து பணிகளையும் காண்க',
 'image': './assets/images/task.jpg',
 'action': '/task-reports'
 }
 ];
 return(reports);
 }
 getStockInventoryMenu(){
 const reports = [{
 'heading': 'ஸ்டாக் இன்',
 'description': 'பண்ணையில் செய்யப்பட்ட அனைத்து', 
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-in'
 },
 {
 'heading': 'ஸ்டாக் அவுட்',
 'description': 'பண்ணையில் உள்ள அனைத்து தோட்டங்களையும் கண்காணித்து அவற்றை நிர்வகிக்கவும்',
 'image': './assets/images/stock_out.png',
 'action': '/stock-out'
 },
 {
 'heading': 'கையிலுள்ள பங்கு',
 'description': 'அழிந்துபோகக்கூடிய பொருட்கள் உட்பட பண்ணையில் விற்பனை',
 'image': './assets/images/stockonhand.jpg',
 'action': '/stock-in-hand'
 }
 ];
 return(reports);
 }
 getHarvestMenu(){
 const harvest=[{
 "title": 'ஸ்டாக் இன்',
 "title1": 'stock in',
 "subtitle": "பண்ணையில் செய்யப்பட்ட அனைத்து அறுவடைகளையும் கண்காணித்து அவற்றை நிர்வகிக்கவும்",
 "image": "assets/images/harvest-in.png",
 "path": "/harvestin"
 },
 {
 "title": "விற்பனை",
 "title1": "sales",
 "subtitle": "எந்த இயந்திரங்கள் / உபகரணங்கள் வாங்குவது உட்பட பண்ணைக்கான எந்தவொரு கொள்முதல்",
 "image": "assets/images/cultivator.png",
 "path": "/harvestout"
 },
 {
  "title": 'பயன்படுத்தப்பட்ட பங்கு மற்றும் சேதம்',
  "title1": 'stock out',
  "subtitle":"பண்ணையில் வெளியேறும் அனைத்து அறுவடைகளையும் கண்காணித்து அவற்றை நிர்வகிக்கவும்",
  "image": "assets/images/food.jpg",
  "path": "/harvestout"
  }
 ];
 return(harvest);
 }
 getAssestsManagementMenu(){
 const purchases=[
 {
 "title":"பொருள் கொள்முதல்",
 "subtitle": "எந்த இயந்திரங்கள் / உபகரணங்கள் வாங்குவது உட்பட பண்ணைக்கான எந்தவொரு கொள்முதல்",
 "image": "assets/images/cultivator.png",
 "keyword":"PurchasesIn",
 "path": "/purchasein-list"
 },
 {
 "title":"பொருள் கொள்முதல் அவுட்",
 "subtitle": "கடைகளுக்கு வெளியே செல்லும் எந்தவொரு பொருளையும் கண்காணித்து நிர்வகிக்கவும்",
 "image": "assets/images/dailyExpense.png",
 "keyword":"PurchasesOut",
 "path": "/purchasein-list"
 }
 ];
 return(purchases);
 }
 getTaskManagerMenu(){
 const task = [{
 "image": 'assets/images/category.png',
 'heading': 'வகையால் ஒதுக்கு',
 "description": 'வகை மூலம் பணிகள் / பணிகளை ஒதுக்குதல் எ.கா: நீர்ப்பாசனம், சுத்தம் செய்தல், அறுவடை போன்றவை',
 "action":'/create-task-category'
 },
 {
 "image": 'assets/images/farm-map.jpg',
 'heading': 'தடுப்பால் ஒதுக்கு',
 "description": 'பண்ணையில் பிரிக்கப்பட்ட தொகுதிகள் மூலம் பணிகளை ஒதுக்கு',
 "action":'/create-task-by-block'
 },
 {
 "image": 'assets/images/male.png',
 'heading': 'உழைப்பால் ஒதுக்கு',
 "description": 'பண்ணையில் பணிபுரியும் தொழிலாளர்களின் அடிப்படையில் பணிகளை ஒதுக்கு',
 "action":'/create-task-labour'
 },
 {
 "image": 'assets/images/task.jpg',
 'heading':'தேதியால் ஒதுக்கு',
 "description": 'தேதியின் அடிப்படையில் பணிகளை ஒதுக்கு',
 "action":'/create-task-by-date'
 }
 ];
 return(task);
 }
 getFarmDetails(){
 const labels = [{
 'image': 'assets/images/task.jpg',
 'action': '/farm-land-records-list',
 'heading': 'நில பதிவுகள்',
 'keyword': 'Land Records',
 'description': 'அனைத்து நில பதிவுகளையும் கத விவரங்களையும் நிர்வகிக்கவும்'
 },
 {
 'image': 'assets/images/cultivator.png',
 'action': '/farm-land-records-list',
 'heading': 'பண்ணை சொத்துக்கள்',
 'keyword': 'Farm Assets',
 'description': 'அனைத்து நிலையான சொத்துக்கள் மற்றும் நகரக்கூடிய சொத்துக்களை பண்ணையில் சேமிக்கவும்'
 },
 {
 'image': 'assets/images/water.jpg',
 'action': '/farm-land-records-list',
 'heading': 'பண்ணை பயன்பாட்டு விவரங்கள்',
 'keyword': 'Farm Utility Details',
 'description': 'எந்த ஈ.பியையும் சேமிக்கவும். நீர் மற்றும் பிற பயன்பாட்டு பில்கள் '
 },
 {
 'image': 'assets/images/certifying-clipart-banner-6.png',
 'action': '/farm-land-records-list',
 'heading': 'உரிமங்கள்',
 'keyword': 'Licenses',
 'description': 'ஏதேனும் உரிமங்கள் மற்றும் பண்ணையின் தொடர்புடைய ஆவணங்கள்'
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'action': 'farmeasy-boarding-menu',
 'heading': 'பண்ணை உள்நுழைவு / அமைவு',
 'description': 'முழு பண்ணை தளவமைப்பு மற்றும் வரைபடத்தை நிர்வகிக்கவும்'
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'action': '/farm-land-records-list',
 'heading': 'இதர',
 'keyword': 'Miscellaneous',
 'description': 'மேலே உள்ள வகைகளில் பொருந்தாத வேறு எந்த ஆவணங்களும்'
 }
 ];
 return(labels);
 }
 getAccountsManagerLabels(){
 let labels = [
 // {
 // 'image': 'assets/images/salary_payments.jpeg',
 // 'action': 'salary-advances',
 // 'heading': 'Salary Payments',
 // 'description': 'Enter all salary payments made to the employees'
 // },
 // {
 // 'image': 'assets/images/loan.jpeg',
 // 'action': 'farmeasy-labour-advance',
 // 'heading': 'Loans & Advances',
 // 'description': 'Any advance given to labourers during the month'
 // },
 {
 'image': 'assets/images/sales_report.png',
 'action': 'sales-reconciliation',
 'heading': 'விற்பனையின் தினசரி நல்லிணக்கம்',
 'description': 'தினசரி விற்பனை புள்ளிவிவரங்களை மீண்டும் சரிசெய்து கையொப்பமிடுங்கள்',
 },
   {
 'image': 'assets/images/in_and_out.jpg',
 'action': 'expenses-reconciliation',
 'heading': 'செலவுகளின் தினசரி நல்லிணக்கம்',
 'description':'தினசரி செலவுகளை சரிசெய்து கையொப்பமிடுங்கள்',
 },
   ];
 return(labels)
 }
 getEnterObservation(){
 let message = 'உங்கள் அவதானிப்பு வெற்றிகரமாக பதிவுசெய்யப்பட்டு, மேற்கொண்டு எடுக்க வேண்டிய நடவடிக்கை குறித்து கள மேற்பார்வையாளருக்கு அறிவிக்கப்பட்டுள்ளது.';
 let labels = ['ஒரு தொகுதியைத் தேர்ந்தெடு', 'சிக்கல் விளக்கத்தை உள்ளிடுக', 'முன்னுரிமை', 'சமர்ப்பி', message,'கோப்பைத் தேர்ந்தெடு', 'சிக்கல் விளக்கத்தை உள்ளிடுக', 'தடு', 'சரி', 'ரத்துசெய்', ' ஒரு மதிப்பைத் தேர்ந்தெடுக்கவும் ',' அவதானிப்பின் சான்று '];
 return(labels);
 }
 getBudgetApproval(){
 const budget = ['நோக்கத்தைத் தேர்ந்தெடு', 'வகையைத் தேர்ந்தெடு', 'தொகை', 'நியாயப்படுத்துதல்', 'சமர்ப்பி', 'வரம்பிலிருந்து', 'வரம்பிலிருந்து', 'நியாயப்படுத்தலை உள்ளிடுக'];
 return(budget);
 }
 getBudgetApprovalView(){
 const budgetview = ['நோக்கம்', 'தயாரிப்பு', 'நியாயப்படுத்துதல்', 'தொகை', 'படங்கள்', 'ஒப்புதல்', 'நிராகரி', 'பட்ஜெட் ஒப்புதலுக்கான கோரிக்கை இல்லை'];
 return(budgetview);
 }
 getDailyExpense(){
 const daily = ['செலவு வகையைத் தேர்ந்தெடு', 'செலவு விவரம்', 'தொகை [INR]', 'செலவு தேதி', 'நியாயப்படுத்துதல்', 'சமர்ப்பி', 'உரையை உள்ளிடுக'];
 return(daily);
 }
 getLabourAdvance(){
 const labAdvance = ['செலவு வகையைத் தேர்ந்தெடு', 'தொழிலாளர் பெயர்', 'மாதாந்திர சால்', 'அட்வ் பணம்', 'புதிய அட்வா', 'சமர்ப்பி'];
 return(labAdvance);
 }
 getHarvestOutInvoice(){
 const invoice = ['அறுவடை அவுட் - விலைப்பட்டியல்', 'ஸ்ரினி ஃபுட் பார்க்', 'மல்லிக் அர்ஜுனா ராவ் கார்ஜ், 121, சித்தூர் மெயின் ரோடு', 'ஆந்திரா, சித்தோர்', 'வகை',
     'துணைப்பிரிவு', 'விகிதம்', 'வெல்நெஸ்ட் இயற்கை பண்ணைகள்', 'ஜே.பி. கார்ப், பெல்லாரி சாலை, சதா சிவா நகர்', 'பெங்களூர், கர்நாடகா, இந்தியா.', 'பதிவு எண்', 'அச்சு விலைப்பட்டியல்', 'மொத்தம்'];
 return(invoice);
 }
 getHarvestIn(){
 const harvestIn = ['பெறும் தேதி', 'தொழிலாளர் தேர்ந்தெடு', 'பரிவர்த்தனை தேதி', 'அறுவடை வகை',
     'தேர்ந்தெடு வகை', 'பில் எண்', 'வாடிக்கையாளர் பெயர்', 'தொடர்பு நபர்', 'துணை வகையைத் தேர்ந்தெடு', 'UOM', 'QTY', 'விகிதம்',
     'மறுஆய்வு நுழைவு', 'தயவுசெய்து எல்லா புலங்களையும் தேர்ந்தெடுக்கவும்', 'விளக்கம்', 'மற்றொரு வகையைச் சேர்'];
 return(harvestIn);
 }
 getHarvestPrintPopup(){
 const popup = ['பில் / விலைப்பட்டியல்', '# 219/11, ஜே.பி. கார்ப், பெல்லாரி சாலை, சத்ஷிவா நகர், பெங்களூர், கர்நாடகா, இந்தியா.', 'வகை', 'துணைப்பிரிவு', 'UOM', 'QTY', ' மதிப்பீடு ',
     'குறிப்பு: இது மின்னணு முறையில் உருவாக்கப்பட்ட விலைப்பட்டியல் மற்றும் கையொப்பம் தேவையில்லை', 'அச்சு', 'மொத்தம்'];
 return(popup);
 }
 getHarvestReviewPopup(){
 const reviewpopup = ['மதிப்பாய்வை உறுதிப்படுத்தவும்', 'தொழிலாளர் பெயர்:', 'வகை பெயர்:', 'துணைப்பிரிவு பெயர்:', 'பெறும் தேதி:', 'UOM:', 'அளவு:', 'ரத்துசெய்', 'சரி ',' விகிதம்: '];
 return(reviewpopup);
 }
 getCreateEmployeeLabels(){
 const labels = {
 'heading':'பணியாளரை உருவாக்கு',
 'title':'பணியாளர் அடையாள அட்டை',
 'name': 'பணியாளர் பெயர்',
 'gender':'- பாலினத்தைத் தேர்ந்தெடுக்கவும் -',
 'dob':'பிறந்த தேதி',
 'address':'வீட்டு முகவரி',
 'phone': 'தொடர்பு எண்',
 'aadhar': 'ஆதார் எண்',
 'email':'மின்னஞ்சல் ஐடி',
 'proof': 'ஐடி ஆதாரத்தைச் சேர்',
 'bank': 'வங்கி விவரங்களைச் சேர்',
 'language': 'மொழி விருப்பம்',
 'details': 'வேலைவாய்ப்பு விவரங்கள்',
 'doj': 'சேரும் தேதி',
 'designation':'- பதவி -',
 'salary':'மாத சம்பளம்',
 'manager': '- மேலாளர் -',
 'password':'கடவுச்சொல்',
 'block': '- தடுப்பு- என்பதைத் தேர்ந்தெடுக்கவும்',
 'submit':'சமர்ப்பி',
 'update':'மேம்படுத்தல்',
 'delete':'நீக்க',
 'found': 'பயனர் ஏற்கனவே இருக்கிறார்',
 'known_lang':'தெரிந்த மொழிகள்'
 }
 return(labels);
 }
 getViewObservationLabels(){ 
 const view = {
 'image':'பட',
 'video':'வீடியோ',
 'action': 'கவனிப்புக்கான செயல் வரலாறு',
 'acknowledge':'ஒப்புக்கொண்டுள்ள',
 'convert': 'பணிக்கு மாற்று'
 };
  return(view);
 }
 getLiveStockMenu(){
 const menu = [{
 'name': 'காய்கறிகள்',
 'icon': 'assets/images/carrot.svg',
 'action': '/farmeasy-livestock-view',
 },
 {
 'name': 'பழங்கள்',
 'icon': 'assets/images/watermelon.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'பயிர்கள்',
 'icon': 'assets/images/crops.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'பசுமை',
 'icon': 'assets/images/green-tea.svg',
 'action': '/farmeasy-livestock-view'
 }];
 return(menu);
 }
 getHarvestReportLabels(){
 const reports = ['வகை', 'தேதியிலிருந்து', 'தேதி வரை', 'அறிக்கை', 'ஒப்பிடு'];
 return(reports);
 }
 getCreateTaskLabourLabels(){
 const labels = {
 labour:'தொழிலாளர்',
 labour_place: '- தொழிற்கட்சியைத் தேர்ந்தெடுக்கவும்-',
 date: 'தேதி',
 block: 'பிளாக்',
 block_place:'- தடுப்பைத் தேர்ந்தெடுக்கவும் -',
 priority: 'முன்னுரிமை',
 priority_place:'- முன்னுரிமையைத் தேர்ந்தெடுக்கவும் -',
 adhoc: 'Adhoc Task',
 adhoc_place: '- பணியைத் தேர்ந்தெடுக்கவும் -',
 select_lab_date:'தயவுசெய்து உழைப்பு மற்றும் தேதியைத் தேர்ந்தெடுக்கவும்',
 }
 return(labels);
 }
 getFrequencySelectionLabels(){
 const labels = {
 frequency: 'அதிர்வெண்',
 freq_place:'- அதிர்வெண்ணைத் தேர்ந்தெடுக்கவும் -',
 dayof: 'வாரத்தின் நாள் (கள்)',
 dayofm: 'மாதத்தின் நாள் (கள்)',
 start:'தொடக்க தேதி',
 end:'இறுதி தேதி',
 ok:'சரி',
 cancel:'ரத்துசெய்'
 }
 return(labels);
 }
 getMyTaskLabels(){
 const labels = {
 title: 'எனது பணிகள்',
 place:'தேடு',
 water: 'தாவரங்களுக்கு நீர் கொடுங்கள்',
 Block: 'பிளாக்',
 plots: 'கதைக்',
 Acknowledge: 'ஒப்புக்கொண்டுள்ள',
 Acknowledged: 'ஒப்புக்கொள்ளப்பட்டது ',
 done:'முடிந்தது என குறிக்கவும்',
 completed: 'நிறைவு',
 clean: 'சுத்தம்',
 notasks: 'இதுவரை பணிகள் எதுவும் இல்லை'
 }
 return(labels);
 }
 getTaskListLabels(){
 const labels = {
 water: 'தாவரங்களுக்கு நீர் கொடுங்கள்',
 place: 'தேடல் அளவுகோல்களை உள்ளிடுக',
 clean: 'சுத்தம்',
 block:'பிளாக்',
 plots:'கதைக்',
 Acknowledge: 'ஒப்புக்கொண்டுள்ள',
 Acknowledged:' ஒப்புக்கொள்ளப்பட்டது ',
 done: 'முடிந்தது என குறிக்கவும்',
 completed: 'நிறைவு',
 select:'- மதிப்புகளைத் தேர்ந்தெடு -'
 }
 return(labels);
 }
getTaskPlannerLabels(){
 const labels = {
select_freq: '- அதிர்வெண்ணைத் தேர்ந்தெடுக்கவும் -',
Filter:'வடிகட்டி',
image: 'படத்தை',
Block: 'பிளாக்',
Category: 'வகை',
Description: 'விளக்கம்',
Assignee:'நியமிக்கப்பட்ட',
Status: 'நிலைமை',
Open:'திறந்த',
Closed: 'மூடப்பட்ட',
NowShowing: 'இப்போது காண்பிக்கிறது',
nores:'முடிவுகள் எதுவும் கிடைக்கவில்லை',
}
return(labels);
 }
 getTaskUpdateHistoryLabels(){
 const labels = {
 title: 'பணி புதுப்பிப்பு வரலாறு',
 assign:'பணி ஒதுக்கப்பட்டது',
 task_mark: 'பணி எனக் குறிக்கப்பட்டுள்ளது',
 completed: 'பணி முடிந்ததாக குறிக்கப்பட்டுள்ளது',
 updated:'பணி புதுப்பிக்கப்பட்டது'
 }
 return(labels);
 }
 getAddBlockLabels(){
 const labels = {
 title: 'பணி புதுப்பிப்பு வரலாறு',
 assign: 'பணி ஒதுக்கப்பட்டது',
 task_mark: 'பணி எனக் குறிக்கப்பட்டுள்ளது',
 completed:'பணி முடிந்ததாக குறிக்கப்பட்டுள்ளது',
 updated: 'பணி புதுப்பிக்கப்பட்டது'
 }
 return(labels);
 }
 getAddLiveStockLabels(){
 const labels = {
 name:'பெயர்',
 gst:'ஜிஎஸ்டி',
 block:'பிளாக்',
 submit:'சமர்ப்பிக்கலாம்',
 update:'அப்டேட்',
 delete:'நீக்கு',
 };
 return(labels);
 }
 getCreateNotificationLabels(){
  let labels = {
    notification: "அறிவிப்பு செய்தி",
    priority:"முன்னுரிமை",
    priority_place:"மதிப்பைத் தேர்ந்தெடுக்கவும்",
    employee:'பணியாளர்களைத் தேர்ந்தெடுக்கவும்',
    selectAll:'அனைத்தையும் தெரிவுசெய்',
    alert:'புலங்களைத் தேர்ந்தெடுக்கவும்',
    submit:"சமர்ப்பி"
  }
  return(labels)
 }

 getPriority(){
 let labels = [{name:'அவசரம்',view:'அவசரம்'},
 {name:'உயர்',view:'உயர்'},
 {name:'நடுத்தர',view:'நடுத்தர'},
 {name:'லோ',view:'லோ'}];
 return labels;
 }
}