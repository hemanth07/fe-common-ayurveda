import { Injectable } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';

@Injectable({
  providedIn: 'root'
})
export class TeluguService {

  constructor(private sg: SimpleGlobal) { }
  getDashboard(){
    if(this.sg['userdata'].role === 'corporate'){
      const dashboard = [{
     "title": "వ్యవసాయ పరిశీలనలు",
     "subtitle": "పొలంలో నిర్దిష్ట ఫలితాలను నివేదించండి",
     "image": "assets/images/observations.jpg",
     "path": "/farmeasy-observation-list"
   },
   {
     "title": "టాస్క్ మేనేజ్మెంట్",
     "subtitle": " ప్రణాళిక, కేటాయించండి & పూర్తి పనులు / పని",
     "image": "assets/images/task.jpg",
     "path": "/task-planner" 
   },
   {
     'title': 'నివేదికలు / విశ్లేషణలు',
     'subtitle': 'నివేదికలను నిర్వహించండి మరియు అన్ని వ్యవసాయ వివరాల విశ్లేషణ',
     'image': 'assets/images/sales_report.png',
     "path": "/farmeasy-reports-menu"
   },
   {
     "title": "వ్యవసాయ సమాచారం",
     "subtitle": "వ్యవసాయ సంబంధిత డేటా మరియు దాని కార్యకలాపాలను పూర్తి చేయండి",
     "image": "assets/images/in_and_out.jpg",
     "path": "/farm-details-menu"
   },
   {
     'title': 'ఖర్చులు & కొనుగోళ్లు',
     'subtitle': 'అన్ని రోజువారీ ఖర్చులు మరియు వ్యవసాయ కొనుగోళ్లను ఇక్కడ నమోదు చేయండి',
     'image': 'assets/images/expense.jpg',
     "path": "/farmeasy-expense-manager"
   },
   {
     "title": "అమ్మకాలు మరియు ఉత్పత్తి",
     "subtitle": "స్టాక్ ఇన్, స్టాక్ అవుట్ మరియు అమ్మకాలను నిర్వహించండి",
     "image": "assets/images/management.jpg",
     "path": '/harvest'
   },
  //  {
  //   "title": "Asset Management",
  //   "subtitle": "Manage Material In, Material Out",
  //   'path': '/assets-management-menu',
  //   "image": "assets/images/farmcleaning.jpg",
  //  },
   {
     "title": "ఖాతాలు / ఆర్థిక",
     "subtitle": "పొలంలో నగదు ప్రవాహాన్ని నిర్వహించండి",
     "image": "assets/images/accounts.jpg",
     "path": '/account-manager-menu'
   }];
   return(dashboard);
   } else if(this.sg['userdata'].role === 'manager'){
    const dashboard = [{
      "title": "వ్యవసాయ పరిశీలనలు",
      "subtitle": "పొలంలో నిర్దిష్ట ఫలితాలను నివేదించండి",
      "image": "assets/images/observations.jpg",
      "path": "/farmeasy-observation-list"
    },
    {
      "title": "టాస్క్ మేనేజ్మెంట్",
      "subtitle": "ప్రణాళిక, కేటాయించండి & పూర్తి పనులు / పని",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    },
    {
      'title': 'నివేదికలు / విశ్లేషణలు',
      'subtitle': 'నివేదికలను నిర్వహించండి మరియు అన్ని వ్యవసాయ వివరాల విశ్లేషణ',
      'image': 'assets/images/sales_report.png',
      "path": "/farmeasy-reports-menu"
    },
    {
      "title": "వ్యవసాయ సమాచారం",
      "subtitle": "వ్యవసాయ సంబంధిత డేటా మరియు దాని కార్యకలాపాలను పూర్తి చేయండి",
      "image": "assets/images/in_and_out.jpg",
      "path": "/farm-details-menu"
    },
    {
      'title': 'ఖర్చులు & కొనుగోళ్లు',
      'subtitle': 'అన్ని రోజువారీ ఖర్చులు మరియు వ్యవసాయ కొనుగోళ్లను ఇక్కడ నమోదు చేయండి',
      'image': 'assets/images/expense.jpg',
      "path": "/farmeasy-expense-manager"
    },
    {
      "title": "అమ్మకాలు మరియు ఉత్పత్తి",
      "subtitle": "స్టాక్ ఇన్, స్టాక్ అవుట్ మరియు అమ్మకాలను నిర్వహించండి",
      "image": "assets/images/management.jpg",
      "path": '/harvest'
    },
    {
      "title": "అకౌంట్స్ / ఫైనాన్స్",
      "subtitle": "పొలంలో నగదు ప్రవాహాన్ని నిర్వహించండి",
      "image": "assets/images/accounts.jpg",
      "path": '/account-manager-menu'
    }];
 return(dashboard);
 } else if(this.sg['userdata'].role === 'accounts'){
  const dashboard = [
  {
    'title': 'ఖర్చులు & కొనుగోళ్లు',
    'subtitle': 'అన్ని రోజువారీ ఖర్చులు మరియు వ్యవసాయ కొనుగోళ్లను ఇక్కడ నమోదు చేయండి',
    'image': 'assets/images/expense.jpg',
    "path": "/farmeasy-expense-manager"
  },
  {
    "title": "వ్యవసాయ ఉత్పత్తి",
    "subtitle": "స్టాక్ ఇన్, స్టాక్ అవుట్ మరియు అమ్మకాలను నిర్వహించండి",
    "image": "assets/images/management.jpg",
    "path": '/harvest'
  },
  {
    "title": "అకౌంట్స్ / ఫైనాన్స్",
    "subtitle": "పొలంలో నగదు ప్రవాహాన్ని నిర్వహించండి",
    "image": "assets/images/accounts.jpg",
    "path": '/account-manager-menu'
  },
  {
    'title': 'నివేదికలు / విశ్లేషణలు',
    'subtitle': 'నివేదికలను నిర్వహించండి మరియు అన్ని వ్యవసాయ వివరాల విశ్లేషణ',
    'image': 'assets/images/sales_report.png',
    "path": "/farmeasy-reports-menu"
  },
  {
    "title": "వ్యవసాయ సమాచారం",
    "subtitle": "వ్యవసాయ సంబంధిత డేటా మరియు దాని కార్యకలాపాలను పూర్తి చేయండి",
    "image": "assets/images/in_and_out.jpg",
    "path": "/farm-details-menu"
  }];
   return(dashboard);
   } else if(this.sg['userdata'].role === 'supervisor'){
    const dashboard = [{
      "title": "వ్యవసాయ పరిశీలనలు",
      "subtitle": "పొలంలో నిర్దిష్ట ఫలితాలను నివేదించండి",
      "image": "assets/images/observations.jpg",
      "path": "/farmeasy-observation-list"
    },
    {
      "title": "టాస్క్ మేనేజ్మెంట్",
      "subtitle": " ప్రణాళిక, కేటాయించండి & పూర్తి పనులు / పని",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    },
    {
      'title': 'ఖర్చులు & కొనుగోళ్లు',
      'subtitle': 'అన్ని రోజువారీ ఖర్చులు మరియు వ్యవసాయ కొనుగోళ్లను ఇక్కడ నమోదు చేయండి',
      'image': 'assets/images/expense.jpg',
      "path": "/farmeasy-expense-manager"
    }
  ];
   return(dashboard);
   } else if(this.sg['userdata'].role === 'employee'){
    const dashboard = [{
      "title": "వ్యవసాయ పరిశీలనలు",
      "subtitle": "పొలంలో నిర్దిష్ట ఫలితాలను నివేదించండి",
      "image": "assets/images/observations.jpg",
      "path": "/farmeasy-observation-list"
    },
    {
      "title": "టాస్క్ మేనేజ్మెంట్",
      "subtitle": " ప్రణాళిక, కేటాయించండి & పూర్తి పనులు / పని",
      "image": "assets/images/task.jpg",
      "path": "/task-planner" 
    }];
     return(dashboard);
   } 
 }
 getHamburger(){
 const hamburger = {
 profile:'నా ప్రొఫైల్',
 notification:'ప్రకటనలు',
 settings:'సెట్టింగ్స్',
 about:'ఫార్మేసీ గురించి',
 sync:'డేటాను సమకాలీకరించండి',
 lagout:'లాగౌట్',
 switch:'భాష మారండి',
 dashboard:'డాష్బోర్డ్',
 support:'మద్దతు / చూడు',
 plant_history:'మొక్కల చరిత్ర',
 change_pass:'పాస్వర్డ్ మార్చండి',
 version:'వెర్షన్',
 terms:'తేదీ'
 };
 return(hamburger);
 }
 getOnBoarding(){
 const onboarding = [{
 'image': 'assets/images/admin.jpg',
 'heading': 'ఎంప్లాయీ రికార్డ్స్',
 'description': 'పొలంలోని సిబ్బంది యొక్క అన్ని ఉపాధి రికార్డులను నిర్వహించండి',
 'action': '/farmeasy-manage-labours',
 },
 {
 'image': 'assets/images/tasks.jpg',
 'heading':'వర్గాల సెటప్',
 'description': 'పొలంలో అన్ని వర్గాల పనిని సెటప్ చేయండి',
 'action': '/farmeasy-view-category',
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'heading': 'బ్లాక్స్ సెటప్',
 'description': 'పొలంలో బ్లాక్‌లు మరియు విభజనలను సెటప్ చేయండి',
 'action': '/farmeasy-view-blocks',
 },
 {
 'image': 'assets/images/rotavator.jpg',
 'heading': 'వనరుల సెటప్',
 'description': 'పొలంలో ఉపయోగించడానికి ఉంచిన అన్ని వనరులను సెటప్ చేయండి',
 'action': '/farmeasy-view-source',
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'heading': 'వ్యవసాయ పటం / లేఅవుట్',
 'description': 'పొలం మరియు దాని లేఅవుట్ను గుర్తించండి',
 'action': '/farm-map',
 }, 
 ];
 return(onboarding);
 }
 getExpense(){
  let expense;
  if(this.sg['userdata'].role === 'supervisor'){
    expense = [{
      'image': 'assets/images/dailyExpense.png',
      'action': 'view-daily-expense',
      'heading': 'పొలంలో రోజువారీ ఖర్చులు',
      'keyword':'Daily Expenses',
      'description': 'రోజువారీ ఖర్చు ఇక్కడ లాగిన్ అవ్వాలి'
      },
      {
      'image': 'assets/images/farm-map.jpg',
      'action': 'farm-purchases-list',
      'heading': 'వ్యవసాయ కొనుగోళ్లు',
      'keyword':'Farm Purchases',
      'description': 'ఏదైనా యంత్రాలు / పరికరాల కొనుగోలుతో సహా పొలం కోసం ఏదైనా కొనుగోళ్లు'
      }];
  } else {
    expense = [{
      'image': 'assets/images/dailyExpense.png',
      'action': 'view-daily-expense',
      'heading': 'పొలంలో రోజువారీ ఖర్చులు',
      'keyword':'Daily Expenses',
      'description': 'రోజువారీ ఖర్చు ఇక్కడ లాగిన్ అవ్వాలి'
      },
      {
      'image': 'assets/images/farm-map.jpg',
      'action': 'farm-purchases-list',
      'heading': 'వ్యవసాయ కొనుగోళ్లు',
      'keyword':'Farm Purchases',
      'description': 'ఏదైనా యంత్రాలు / పరికరాల కొనుగోలుతో సహా పొలం కోసం ఏదైనా కొనుగోళ్లు'
      },
      {
      'image': 'assets/images/salary_payments.jpeg',
      'action': 'salary-advances',
      'heading': 'జీతం చెల్లింపులు',
      'keyword': 'Salary Payments',
      'description':'ఉద్యోగులకు చేసిన అన్ని జీతాల చెల్లింపులను నమోదు చేయండి'
      },
      {
      'image': 'assets/images/loan.jpeg',
      'action': 'farmeasy-labour-advance',
      'heading': 'రుణాలు & అడ్వాన్సులు',
      'keyword': 'Loans & Advances',
      'description': 'నెలలో కూలీలకు ఏదైనా అడ్వాన్స్ ఇస్తే'
      }];
  }
 return(expense);
 }
 getReportsMenu(){
 const reports = [{
 'heading': 'సేల్స్ & కలెక్షన్ రిపోర్ట్',
 'description': 'పాడైపోయే వస్తువులతో సహా పొలంలో అమ్మకాలు',
 'image': './assets/images/sales_report.png',
 'action': '/sales-collections-reports'
 },
 {
 'heading': 'ఖర్చులు & కొనుగోళ్ల నివేదిక',
 'description': 'ఏదైనా యంత్రాలు / పరికరాల కొనుగోలుతో సహా పొలం కోసం ఏదైనా కొనుగోళ్లు',
 'image': './assets/images/expense.jpg',
 'action': '/expense-purchases-reports'
 },
 {
 'heading': 'ఇన్‌ఫ్లో & low ట్‌ఫ్లో రిపోర్ట్',
 'description': 'పొలంలో నగదు ప్రవాహం మరియు ప్రవాహాన్ని ట్రాక్ చేయండి',
 'image': './assets/images/in_and_out.jpg',
 'action': '/inflow-outflow-reports'
 },
 {
 'heading': 'స్టాక్ ఇన్వెంటరీ రిపోర్ట్',
 'description': 'స్టాక్ ఇన్, స్టాక్ అవుట్ మరియు స్టాక్ ఆన్ హ్యాండ్ వివరాలు',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-inventory-report-menu'
 },
 {
 'heading': 'వ్యవసాయ పరిశీలన నివేదికలు',
 'description':'పొలంలో ఉన్న అన్ని తోటలను ట్రాక్ చేసి వాటిని నిర్వహించండి',
 'image': './assets/images/plantation_new.jpg',
 'action': '/farm-activity-report-menu'
 }];
 return(reports);
 }
 getFarmActivityMenu(){
 const reports = [{
 'heading': 'వ్యవసాయ పరిశీలన నివేదిక',
 'keyword': 'observation-reports',
 'description': 'పొలంలో నివేదించబడిన అన్ని పరిశీలనలను ట్రాక్ చేయండి',
 'image': './assets/images/in_and_out.jpg',
 'action': '/farmeasy-all-observations'
 },
 {
 'heading': 'నీరు త్రాగుట నివేదిక',
 'keyword': 'watering-reports',
 'description': 'పొలంలో నీరు త్రాగుట పర్యవేక్షించండి',
 'image': './assets/images/watering_reports.jpg',
 'action': '/watering-reports'
 },
 {
 'heading': 'ఫెర్టిగేషన్ రిపోర్ట్',
 'keyword': 'fertigation-reports',
 'description': 'పొలంలో చేసిన ఫలదీకరణాన్ని పర్యవేక్షించండి',
 'image': './assets/images/fertigation.jpeg',
 'action': '/watering-reports'
 },
//  {
//  'keyword': 'labour-work-assignment',
//  'heading': 'లేబర్ వర్క్ అసైన్‌మెంట్',
//  'description': 'పొలంలో కార్మికులకు కేటాయించిన పనిని చూడండి',
//  'image': './assets/images/male.png',
//  'action': '/resource-planning'
//  },
 {
 'heading': 'టాస్క్ రిపోర్ట్',
 'keyword': 'task-reports',
 'description': 'పొలంలో నిర్వహించబడే అన్ని పనులను చూడండి',
 'image': './assets/images/task.jpg',
 'action': '/task-reports'
 }
 ];
 return(reports);
 }
 getStockInventoryMenu(){
 const reports = [{
 'heading': 'స్టాక్ ఇన్',
 'description': 'పొలంలో చేసిన అన్ని పంటలను ట్రాక్ చేసి వాటిని నిర్వహించండి',
 'image': './assets/images/harvesting1.jpg',
 'action': '/stock-in'
 },
 {
 'heading': 'స్టాక్ అవుట్',
 'description': 'పొలంలో ఉన్న అన్ని తోటలను ట్రాక్ చేసి వాటిని నిర్వహించండి',
 'image': './assets/images/stock_out.png',
 'action': '/stock-out'
 },
 {
 'heading': 'స్టాక్ ఆన్ హ్యాండ్',
 'description': 'పాడైపోయే వస్తువులతో సహా పొలంలో అమ్మకాలు',
 'image': './assets/images/stockonhand.jpg',
 'action': '/stock-in-hand'
 }
 ];
 return(reports);
 }
 getHarvestMenu(){
 const harvest=[{
 "title": "స్టాక్ ఇన్",
 "title1":"stock in",
 "subtitle": "పొలంలో చేసిన అన్ని పంటలను ట్రాక్ చేసి వాటిని నిర్వహించండి",
 "image": "assets/images/harvest-in.png",
 "path": "/harvestin"
 },
 {
 "title":"అమ్మకాలు",
 "title1": "sales",
 "subtitle": "ఏదైనా యంత్రాలు / పరికరాల కొనుగోలుతో సహా పొలం కోసం ఏదైనా కొనుగోళ్లు",
 "image": "assets/images/cultivator.png",
 "path": "/harvestout"
 },
 {
  "title": 'ఉపయోగించిన స్టాక్ మరియు నష్టం',
  "title1": 'stock out',
  "subtitle": "పొలంలో బయటకు వెళ్లే అన్ని పంటలను ట్రాక్ చేసి వాటిని నిర్వహించండి",
  "image": "assets/images/food.jpg",
  "path": "/harvestout"
  }
 ];
 return(harvest);
 }
 getAssestsManagementMenu(){
 const purchases=[
 {
 "title": "మెటీరియల్ కొనుగోళ్లు",
 "subtitle": "ఏదైనా యంత్రాలు / పరికరాల కొనుగోలుతో సహా పొలం కోసం ఏదైనా కొనుగోళ్లు",
 "image": "assets/images/cultivator.png",
 "keyword":"PurchasesIn",
 "path": "/purchasein-list"
 },
 {
 "title":"మెటీరియల్ కొనుగోళ్లు ముగిసింది",
 "subtitle": "దుకాణాల నుండి బయటకు వెళ్ళే ఏదైనా పదార్థాన్ని ట్రాక్ చేయండి మరియు నిర్వహించండి",
 "image": "assets/images/dailyExpense.png",
 "keyword":"PurchasesOut",
 "path": "/purchasein-list"
 }
 ];
 return(purchases);
 }
 getTaskManagerMenu(){
 const task = [{
 "image": 'assets/images/category.png',
 'heading': 'వర్గం ప్రకారం కేటాయించండి',
 "description": 'వర్గం వారీగా పనులు / పనిని కేటాయించండి ఉదా: నీరు త్రాగుట, శుభ్రపరచడం, పంట మొదలైనవి',
 "action":'/create-task-category'
 },
 {
 "image": 'assets/images/farm-map.jpg',
 'heading': 'బ్లాక్ ద్వారా కేటాయించండి',
 "description":'పొలంలో వేరు చేయబడిన బ్లాక్‌ల ద్వారా పనులను కేటాయించండి',
 "action":'/create-task-by-block'
 },
 {
 "image": 'assets/images/male.png',
 'heading': 'లేబర్ చేత కేటాయించండి',
 "description":'పొలంలో పనిచేసే కార్మికుల ఆధారంగా విధులను కేటాయించండి',
 "action":'/create-task-labour'
 },
 {
 "image": 'assets/images/task.jpg',
 'heading': 'తేదీ ద్వారా కేటాయించండి',
 "description": 'తేదీ ఆధారంగా పనులను కేటాయించండి',
 "action":'/create-task-by-date'
 }
 ];
 return(task);
 }
 getFarmDetails(){
 const labels = [{
 'image': 'assets/images/task.jpg',
 'action': '/farm-land-records-list',
 'heading': 'ల్యాండ్ రికార్డ్స్',
 'keyword': 'Land Records',
 'description': 'అన్ని భూ రికార్డులు మరియు కథ వివరాలను నిర్వహించండి'
 },
 {
 'image': 'assets/images/cultivator.png',
 'action': '/farm-land-records-list',
 'heading': 'వ్యవసాయ ఆస్తులు',
 'keyword': 'Farm Assets',
 'description': 'అన్ని స్థిర ఆస్తులు మరియు కదిలే ఆస్తులను పొలంలో నిల్వ చేయండి'
 },
 {
 'image': 'assets/images/water.jpg',
 'action': '/farm-land-records-list',
 'heading': 'ఫార్మ్ యుటిలిటీ వివరాలు',
 'keyword': 'Farm Utility Details',
 'description':'ఏదైనా EB ని నిల్వ చేయండి. నీరు మరియు ఇతర యుటిలిటీ బిల్లులు '
 },
 {
 'image': 'assets/images/certifying-clipart-banner-6.png',
 'action': '/farm-land-records-list',
 'heading': 'లైసెన్సులు',
 'keyword': 'Licenses',
 'description': 'ఏదైనా లైసెన్సులు మరియు వ్యవసాయ సంబంధిత డాక్యుమెంటేషన్'
 },
 {
 'image': 'assets/images/Farm-Base-Map.png',
 'action': 'farmeasy-boarding-menu',
 'heading': 'ఫార్మ్ ఆన్‌బోర్డింగ్ / సెటప్',
 'description': 'మొత్తం వ్యవసాయ లేఅవుట్ మరియు మ్యాప్‌ను నిర్వహించండి'
 },
 {
 'image': 'assets/images/farmmap.jpg',
 'action': '/farm-land-records-list',
 'heading': 'ఇతరాలు',
 'keyword': 'Miscellaneous',
 'description': 'పై వర్గాలకు సరిపోని ఇతర పత్రాలు'
 }
 ];
 return(labels);
 }
 getAccountsManagerLabels(){
 let labels = [
 // {
 // 'image': 'assets/images/salary_payments.jpeg',
 // 'action': 'salary-advances',
 // 'heading': 'Salary Payments',
 // 'description': 'Enter all salary payments made to the employees'
 // },
 // {
 // 'image': 'assets/images/loan.jpeg',
 // 'action': 'farmeasy-labour-advance',
 // 'heading': 'Loans & Advances',
 // 'description': 'Any advance given to labourers during the month'
 // },
 {
 'image': 'assets/images/sales_report.png',
 'action': 'sales-reconciliation',
 'heading': 'అమ్మకాల డైలీ సయోధ్య',
 'description': 'రోజువారీ అమ్మకాల గణాంకాలను పునరుద్దరించండి మరియు సైన్ ఆఫ్ ఇవ్వండి',
 },
   {
 'image': 'assets/images/in_and_out.jpg',
 'action': 'expenses-reconciliation',
 'heading': 'ఖర్చుల రోజువారీ సయోధ్య',
 'description': 'రోజువారీ ఖర్చులను సరిచేసుకోండి మరియు సైన్ ఆఫ్ ఇవ్వండి',
 },
   ];
 return(labels)
 }
 getEnterObservation(){
 let message = 'మీ పరిశీలన విజయవంతంగా రికార్డ్ చేయబడింది మరియు తీసుకోవలసిన తదుపరి చర్య గురించి ఫీల్డ్ సూపర్‌వైజర్‌కు తెలియజేయబడుతుంది.';
 let labels = ['బ్లాక్‌ను ఎంచుకోండి', 'సమస్య వివరణను నమోదు చేయండి', 'ప్రాధాన్యత', 'సమర్పించు', message,'ఫైల్‌ను ఎంచుకోండి', 'సమస్య వివరణను నమోదు చేయండి', 'బ్లాక్', 'సరే', 'రద్దు చేయి', ' విలువను ఎంచుకోండి ',' పరిశీలన యొక్క రుజువు '];
 return(labels);
 }
 getBudgetApproval(){
 const budget = ['పర్పస్ ఎంచుకోండి', 'కేటగిరీని ఎంచుకోండి', 'మొత్తం', 'జస్టిఫికేషన్', 'సమర్పించు', 'రేంజ్ ఫ్రమ్', 'రేంజ్ టు', 'ఎంటర్ జస్టిఫికేషన్'];
 return(budget);
 }
 getBudgetApprovalView(){
 const budgetview = ['పర్పస్', 'ప్రొడక్ట్', 'జస్టిఫికేషన్', 'మొత్తం', 'ఇమేజెస్', 'అప్రూవ్', 'రిజెక్ట్', 'బడ్జెట్ ఆమోదం కోసం అభ్యర్థన లేదు'];
 return(budgetview);
 }
 getDailyExpense(){
 const daily = ['ఖర్చు వర్గాన్ని ఎంచుకోండి', 'ఖర్చు వివరణ', 'మొత్తం [INR]', 'ఖర్చు తేదీ', 'సమర్థన', 'సమర్పించు', 'వచనాన్ని నమోదు చేయండి'];
 return(daily);
 }
 getLabourAdvance(){
 const labAdvance = ['ఖర్చు వర్గాన్ని ఎంచుకోండి', 'శ్రమ పేరు', 'మంత్లీ సాల్', 'అడ్వాడ్ పెయిడ్', 'న్యూ అడ్', 'సమర్పించు'];
 return(labAdvance);
 }
 getHarvestOutInvoice(){
 const invoice = ['హార్వెస్ట్ అవుట్ - ఇన్వాయిస్', 'శ్రీని ఫుడ్ పార్క్', 'మల్లిక్ అర్జున రావు కార్గే, 121, చిత్తూరు మెయిన్ రోడ్', 'ఆంధ్రప్రదేశ్, చిత్తూరు', 'వర్గం',
     'ఉపవర్గం', 'రేటు', 'వెల్‌నెస్ట్ నేచురల్ ఫార్మ్స్', 'జెపి కార్ప్, బళ్లారి రోడ్, సదా శివ నగర్', 'బెంగళూరు, కర్ణాటక, ఇండియా.', 'రిజిస్ట్రేషన్ సంఖ్య', 'ప్రింట్ ఇన్‌వాయిస్', 'మొత్తం'];
 return(invoice);
 }
 getHarvestIn(){
 const harvestIn = ['స్వీకరించే తేదీ', 'శ్రమను ఎంచుకోండి', 'లావాదేవీల తేదీ', 'హార్వెస్ట్ రకం',
     'వర్గాన్ని ఎంచుకోండి', 'బిల్ NO', 'కస్టమర్ పేరు', 'సంప్రదింపు వ్యక్తి', 'ఉప-వర్గాన్ని ఎంచుకోండి', 'UOM', 'QTY', 'రేటు',
     'ఎంట్రీని సమీక్షించండి', 'దయచేసి అన్ని ఫీల్డ్‌లను ఎంచుకోండి', 'వివరణ', 'మరొక వర్గాన్ని జోడించండి'];
 return(harvestIn);
 }
 getHarvestPrintPopup(){
 const popup = ['బిల్ / ఇన్వాయిస్', '# 219/11, జెపి కార్ప్, బళ్లారి రోడ్, సద్శివ నగర్, బెంగళూరు, కర్ణాటక, ఇండియా.', 'వర్గం', 'ఉపవర్గం', 'UOM', 'QTY', ' రేటు ',
     'గమనిక: ఇది ఎలక్ట్రానిక్ ఉత్పత్తి చేసిన ఇన్వాయిస్ మరియు సంతకం అవసరం లేదు', 'ప్రింట్', 'టోటల్'];
 return(popup);
 }
 getHarvestReviewPopup(){
 const reviewpopup = ['సమీక్షను నిర్ధారించండి', 'కార్మికుల పేరు:', 'వర్గం పేరు:', 'ఉపవర్గం పేరు:', 'స్వీకరించే తేదీ:', 'UOM:', 'పరిమాణం:', 'రద్దు చేయి', 'సరే ',' రేటు: '];
 return(reviewpopup);
 }
 getCreateEmployeeLabels(){
 const labels = {
 'heading':'ఉద్యోగిని సృష్టించండి',
 'title':'ఎంప్లాయీ ఐడి కార్డ్',
 'name': 'ఉద్యోగి పేరు',
 'gender':'- లింగాన్ని ఎంచుకోండి -',
 'dob':'పుట్టిన తేదీ',
 'address':'నివాస చిరునామా',
 'phone': 'సంప్రదింపు సంఖ్య',
 'aadhar': 'ఆధార్ సంఖ్య',
 'email': 'ఇమెయిల్ ID',
 'proof': 'ID ప్రూఫ్‌ను జోడించు',
 'bank':'బ్యాంక్ వివరాలను జోడించండి',
 'language': 'భాష ప్రాధాన్యత',
 'details': 'ఉపాధి వివరాలు',
 'doj': 'చేరిన తేదీ',
 'designation':'- హోదా -',
 'salary':'మంత్లీ జీతం',
 'manager': '- మేనేజర్ -',
 'password': 'పాస్వర్డ్',
 'block': '- బ్లాక్- ఎంచుకోండి',
 'submit':'submit',
 'update':'నవీకరణ',
 'delete':'తొలగించు',
 'found':'వినియోగదారు ఇప్పటికే ఉన్నారు',
 'known_lang': 'తెలిసిన భాషలు'
 }
 return(labels);
 }
 getViewObservationLabels(){ 
 const view = {
 'image':'చిత్రం',
 'video':'వీడియో',
 'action':'యాక్షన్ హిస్టరీ ఆన్ అబ్జర్వేషన్',
 'acknowledge': 'అంగీకరిస్తున్నాను',
 'convert': 'టాస్క్‌గా మార్చండి'
 };
  return(view);
 }
 getLiveStockMenu(){
 const menu = [{
 'name': 'కూరగాయలు',
 'icon': 'assets/images/carrot.svg',
 'action': '/farmeasy-livestock-view',
 },
 {
 'name': 'పండ్లు',
 'icon': 'assets/images/watermelon.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'పంటలు',
 'icon': 'assets/images/crops.svg',
 'action': '/farmeasy-livestock-view'
 },
 {
 'name': 'గ్రీన్స్',
 'icon': 'assets/images/green-tea.svg',
 'action': '/farmeasy-livestock-view'
 }];
 return(menu);
 }
 getHarvestReportLabels(){
 const reports = ['రకం', 'తేదీ నుండి', 'తేదీకి', 'నివేదిక', 'పోల్చండి'];
 return(reports);
 }
 getCreateTaskLabourLabels(){
 const labels = {
 labour:'లేబర్',
 labour_place:'-లేబర్- ఎంచుకోండి, ',
 date:'తేదీ',
 block:'బ్లాక్',
 block_place:'- బ్లాక్ ఎంచుకోండి -',
 priority:'ప్రాధాన్య',
 priority_place: '- ప్రాధాన్యతను ఎంచుకోండి -',
 adhoc: 'Adhoc Task',
 adhoc_place: '- టాస్క్ ఎంచుకోండి -',
 select_lab_date: 'దయచేసి శ్రమ మరియు తేదీని ఎంచుకోండి',
 }
 return(labels);
 }
 getFrequencySelectionLabels(){
 const labels = {
 frequency: 'పౌనఃపున్యం',
 freq_place: '- ఫ్రీక్వెన్సీని ఎంచుకోండి -',
 dayof:'వారపు రోజు (లు)',
 dayofm:'నెల రోజు (లు)',
 start: 'ప్రారంభ తేదీ',
 end:'ముగింపు తేదీ',
 ok:'సరే',
 cancel:'రద్దు'
 }
 return(labels);
 }
 getMyTaskLabels(){
 const labels = {
 title: 'నా పనులు',
 place: 'శోధన',
 water: 'మొక్కలకు నీరు పెట్టండి',
 Block: 'బ్లాక్',
 plots:'ప్లాట్లు',
 Acknowledge:'గుర్తించి',
 Acknowledged: 'ఆమోదించినవి',
 done: 'పూర్తయినట్లు గుర్తించండి',
 completed:'పూర్తి',
 clean: 'క్లీన్ ఇన్',
 notasks:'ఇంకా పనులు లేవు'
 }
 return(labels);
 }
 getTaskListLabels(){
 const labels = {
 water:'మొక్కలకు నీరు పెట్టండి',
 place: 'శోధన ప్రమాణాలను నమోదు చేయండి',
 clean: 'క్లీన్ ఇన్',
 block:'బ్లాక్',
 plots:'ప్లాట్లు',
 Acknowledge:'ఆమోదించినవి',
 Acknowledged:'పూర్తయినట్లు గుర్తించండి',
 done:'పూర్తయినట్లు గుర్తించండి',
 completed:'పూర్తి',
 select: '- విలువలను ఎంచుకోండి -'
 }
 return(labels);
 }
getTaskPlannerLabels(){
 const labels = {
select_freq: '- ఫ్రీక్వెన్సీని ఎంచుకోండి -',
Filter:'వడపోత',
image:'చిత్రం',
Block:'బ్లాక్',
Category:'వర్గం',
Description:'వివరణ',
Assignee:'అప్పగింత',
Status:'హోదా',
Open:'ఓపెన్',
Closed:'ముగించబడినది',
NowShowing:'ఇప్పుడు చూపుతోంది',
nores:'ఫలితాలు కనుగొనబడలేదు',
}
return(labels);
 }
 getTaskUpdateHistoryLabels(){
 const labels = {
 title: 'టాస్క్ నవీకరణ చరిత్ర',
 assign:'అసైన్డ్ టాస్క్',
 task_mark: 'టాస్క్ ఇలా గుర్తించబడింది',
 completed: 'పని పూర్తయినట్లు గుర్తించబడింది',
 updated:'పని నవీకరించబడింది'
 }
 return(labels);
 }
 getAddBlockLabels(){
 const labels = {
 title: 'టాస్క్ నవీకరణ చరిత్ర',
 assign: 'అసైన్డ్ టాస్క్',
 task_mark: 'టాస్క్ ఇలా గుర్తించబడింది',
 completed: 'పని పూర్తయినట్లు గుర్తించబడింది',
 updated: 'పని నవీకరించబడింది'
 }
 return(labels);
 }
 getAddLiveStockLabels(){
 const labels = {
 name:'పేరు',
 gst:'జిఎస్టి',
 block:'బ్లాక్',
 submit:'సమర్పించవచ్చు',
 update:'సరిచేయబడిన',
 delete:'తొలగించు',
 };
 return(labels);
 }
 getCreateNotificationLabels(){
  let labels = {
    notification: "నోటిఫికేషన్ సందేశం",
    priority:"ప్రాధాన్యత",
    priority_place:"విలువను ఎంచుకోండి",
    employee:'ఉద్యోగులను ఎంచుకోండి',
    selectAll:'అన్ని ఎంచుకోండి',
    alert:'దయచేసి ఫీల్డ్‌లను ఎంచుకోండి',
    submit:"సమర్పించండి"
  }
 return(labels)
 }

 getPriority(){
 let labels = [{name:'అర్జెంట్',view:'అర్జెంట్'},
 {name:'హై',view:'హై'},
 {name:'మీడియం',view:'మీడియం'},
 {name:'తక్కువ',view:'తక్కువ'}];
 return labels;
 }
}