import { Location } from '@angular/common';
import { Component, OnInit, ViewChild, asNativeElements, ElementRef, NgZone } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { Platform } from '@ionic/angular';
// import { FileChooser } from '@ionic-native/file-chooser/ngx';
// import { IOSFilePicker } from '@ionic-native/file-picker';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';
import * as firebase from 'firebase';
import { LoadingController, AlertController, PopoverController} from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
@Component({
  selector: 'app-farm-land-records',
  templateUrl: './farm-land-records.page.html',
  styleUrls: ['./farm-land-records.page.scss'],
}) 
@ViewChild('fileInput')
export class FarmLandRecordsPage implements OnInit {
  headerData: any = [];
  footerData:any = [];
  category: any;
  sub_category:any;
  name: any;
  description:any;
  date:any;
  attachment: any = [];
  save_data: any = [];
  survey_no: string;
  page:any;
  value:any;
  constructor(private sg: SimpleGlobal,
    public plt: Platform,
    public camera:Camera,
    public file: File,
    public media: Media,
    public mediaCapture: MediaCapture,
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    private zone:NgZone,
    public popoverController: PopoverController,
    private location:Location) { }
  ngOnInit() {
    this.date = new Date().toString();
    this.page = this.sg['farm_details_item'];
    this.createHeader();
  }
  createHeader() {
    // color: green,orange,purple
    let title;
    if(this.page == 'Land Records'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Farm Land Records';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఫార్మ్ ల్యాండ్ రికార్డ్స్';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை நிலப் பதிவுகள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಲ್ಯಾಂಡ್ ರೆಕಾರ್ಡ್ಸ್';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'फार्म लैंड रिकॉर्ड';
      }
    } else if(this.page == 'Farm Assets'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Farm Assets';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఫార్మ్ ఆస్తులు';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை சொத்துக்கள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಸ್ವತ್ತುಗಳು';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'फार्म एसेट्स';
      }
    } else if(this.page == 'Farm Utility Details'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Farm Utility Details';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఫార్మ్ యుటిలిటీ వివరాలు';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை பயன்பாட்டு விவரங்கள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಯುಟಿಲಿಟಿ ವಿವರಗಳು';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'फार्म उपयोगिता विवरण';
      }
    } else if(this.page == 'Licenses'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Licenses';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'లైసెన్స్';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'உரிமங்கள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಪರವಾನಗಿಗಳು';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'लाइसेंस';
      }
    } else if(this.page == 'Miscellaneous'){
      this.category = 'Miscellaneous';
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Miscellaneous';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఇతరాలు';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'இதர';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ವಿವಿಧ';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'कई तरह का';
      }
    }

    this.headerData = {
      color: 'teal',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.9)',
        'leftButtonName':'Submit',
        'rightButtonName':'Add Another',
        'leftButtonColor':'teal',
        'rightButtonColor':'teal',
        };
  }
  uploadFile(fileInput:HTMLElement){
    fileInput.click();
 }

 async fileUpload(event) {
   let allow = false;
   let that = this;
   let urls;
   if(event.target.files && event.target.files[0]){
     let fileExt = event.target.files[0].name.split(".")[1];
     if(fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'docx'){
       allow = true;
     }
   }
   this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
  
  if (event.target.files && event.target.files[0] && allow) {
         const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'File'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });

    let reader = new FileReader();
    let file = event.target.files[0];
   // console.log(event.target.files[0]);
      reader.readAsDataURL(file);
      urls = {'name' : this.sg['userdata'].vendor_id+'/farmLandRecords/doc/' + file.name};
        this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/doc/' + file.name);
    firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/doc/' + file.name).put(file).then(function(snapshot) {
      setTimeout(() => {
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmLandRecords/doc/' + file.name).getDownloadURL().then(function(url) {
          const body = { 'type': 'attachment', 'url' : url  , 'path':urls};
          that.attachment.push(body);
          // alert(url);
          popover.dismiss();
        });
      }, 1000);
    });
    that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/doc/' + file.name).put(file).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
  } else if(!allow){
    const alert = await this.alertController.create({
      message: 'Please Select doc, docx and pdf extension files',
      buttons: ['OK']
    });

    await alert.present();
  }
}
cameraImage(){
 let  urls;
const options: CameraOptions = {
  quality: 20,
  destinationType: this.camera.DestinationType.DATA_URL,
  encodingType: this.camera.EncodingType.JPEG,
  mediaType: this.camera.MediaType.PICTURE
}
this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
   this.camera.getPicture(options).then(async (imageData) => {
              this.date = new Date().getTime();
                   const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });

              let that;
              that = this;
                     let img_url  = 'data:image/jpeg;base64,' + imageData;
                     fetch(img_url)
                     .then(res => res.blob())
                     .then(blob => {
                       urls = {'name' : this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date};
        this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date);
                       firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date).put(blob).then(function(snapshot) {
                         setTimeout(() => {
                           firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                            popover.dismiss();
                            const body = { 'type': 'camera', 'url' : url  , 'path': urls };
                            that.attachment.push(body);
                            });
                         }, 1000);
                       });
                       that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                    });
                   }, (err) => {
                     // Handle error
            alert(err);
       });
  }
  video(){
       let urls:any;
    const options: CaptureVideoOptions = {
      limit: 1,
      duration: 30
    };
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    this.mediaCapture.captureVideo(options)
    .then(
     async (data: MediaFile[]) => {
             const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Video'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });

        let that;
        that = this;
        const index = data[0].fullPath.lastIndexOf('/');
        const finalPath = data[0].fullPath.substr(0, index);
        this.file.readAsArrayBuffer(finalPath, data[0].name).then( async(file) => {
        const blob = new Blob([file], {type: data[0].type});
        urls = {'name' : this.sg['userdata'].vendor_id+'/farmLandRecords/videos/' + data[0].name};
        this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/videos/' + data[0].name);
        firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/videos/' + data[0].name).put(blob).then(function(snapshot) {
          firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmLandRecords/videos/' + data[0].name).getDownloadURL().then(function(url) {
            popover.dismiss();
            const body = { type: 'video', url : url,  path : urls };
            that.attachment.push(body);
          });
         });
             that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/videos/' + data[0].name).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
        });
      },
      (err: CaptureError) => console.error(err)
    );
   }
  gallery(){
    let  urls;
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const options: CameraOptions = {
             quality: 100,
             destinationType: this.camera.DestinationType.DATA_URL,
             encodingType: this.camera.EncodingType.JPEG,
             mediaType: this.camera.MediaType.PICTURE,
             sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
           }
            this.camera.getPicture(options).then(async (imageData) => {
              this.date = new Date().getTime();
                   const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });

              let that;
              that = this;
                     let img_url  = 'data:image/jpeg;base64,' + imageData;
                     fetch(img_url)
                     .then(res => res.blob())
                     .then(blob => {
                       urls = {'name' : this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date};
        this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date);
                       firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date).put(blob).then(function(snapshot) {
                         setTimeout(() => {
                           firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                            popover.dismiss();
                            const body = { 'type': 'gallery', 'url' : url  , 'path': urls };
                            that.attachment.push(body);
                            });
                         }, 1000);
                       });
                        that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmLandRecords/images/' + this.date).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                    });
                   }, (err) => {
                     // Handle error
                alert(err);
       });
  }
  //save land Records
 async validationEntry(){
    let validation = [];
    if(this.page == 'Land Records'){
      if(!this.category){
        validation.push('Category')
      }
      if(!this.name){
        validation.push('Name')
      }
      if(!this.description){
        validation.push('Description');
      }
      if(!this.survey_no){
        validation.push('Survey No')
      }
      if(this.attachment.length == 0){
        validation.push('Attachments')
      }
    } else if(this.page == 'Farm Assets'){
      if(!this.category){
        validation.push('Category')
      }
      if(!this.sub_category){
        validation.push('Sub Category')
      }
      if(!this.value){
        validation.push('Value')
      }
      if(!this.name){
        validation.push('Name')
      }
      if(!this.description){
        validation.push('Description');
      }
     if(this.attachment.length == 0){
        validation.push('Attachments')
      }
    } else if(this.page == 'Farm Utility Details'){

      if(!this.category){
        validation.push('Category')
      }
      if(!this.name){
        validation.push('Name')
      }
      if(!this.description){
        validation.push('Description');
      }
      if(this.attachment.length == 0){
        validation.push('Attachments')
      }

    } else if(this.page == 'Licenses'){

      if(!this.category){
        validation.push('Category')
      }
      if(!this.name){
        validation.push('Name')
      }
      if(!this.description){
        validation.push('Description');
      }
      if(this.attachment.length == 0){
        validation.push('Attachments')
      }

    } else if(this.page == 'Miscellaneous'){

      if(!this.category){
        validation.push('Category')
      }
      if(!this.name){
        validation.push('Name')
      }
      if(!this.description){
        validation.push('Description');
      }
      if(this.attachment.length == 0){
        validation.push('Attachments')
      }

    }

    if (validation.length != 0) {

      const alert = await this.alertController.create({
        header: "Please enter mandatory fields",
        message: validation.join(","),
        cssClass: "custom-alertDanger",
        buttons: ["OK"]
      });
      await alert.present();

    } else {
      this.save();
    }
  }
  save(){
    if(this.page == 'Land Records'){
      let that = this;
      let body = {
       category:this.category,
       name:this.name,
       description:this.description,
       attachment:this.attachment,
       survey_no:this.survey_no
     }
     this.save_data.push(body);
     let count = 0;
     for(let body of this.save_data){
       firebase.database().ref(this.sg['userdata'].vendor_id+'/farmLandRecords').push(body).then(()=>{
         count ++;
         if(that.save_data.length === count){
         that.location.back();
         }
       })
     }
    } else if(this.page == 'Farm Assets'){
      let that = this;
      let body = {
       category:this.category,
       sub_category:this.sub_category,
       name:this.name,
       value:this.value,
       description:this.description,
       attachment:this.attachment,
     }
     this.save_data.push(body);
     let count = 0;
     for(let body of this.save_data){
       firebase.database().ref(this.sg['userdata'].vendor_id+'/farmAssets').push(body).then(()=>{
         count ++;
         if(that.save_data.length === count){
         that.location.back();
         }
       })
     }
    } else if(this.page == 'Farm Utility Details'){
      let that = this;
      let body = {
       category:this.category,
       name:this.name,
       description:this.description,
       attachment:this.attachment,
     }
     this.save_data.push(body);
     let count = 0;
     for(let body of this.save_data){
       firebase.database().ref(this.sg['userdata'].vendor_id+'/farmUtilityDetails').push(body).then(()=>{
         count ++;
         if(that.save_data.length === count){
         that.location.back();
         }
       })
     }
    } else if(this.page == 'Licenses'){

      let that = this;
      let body = {
       category:this.category,
       name:this.name,
       description:this.description,
       attachment:this.attachment,
     }
     this.save_data.push(body);
     let count = 0;
     for(let body of this.save_data){
       firebase.database().ref(this.sg['userdata'].vendor_id+'/licenses').push(body).then(()=>{
         count ++;
         if(that.save_data.length === count){
         that.location.back();
         }
       })
     }
    } else if(this.page == 'Miscellaneous'){
      let that = this;
      let body = {
       category:this.category,
       name:this.name,
       description:this.description,
       attachment:this.attachment,
     }
     this.save_data.push(body);
     let count = 0;
     for(let body of this.save_data){
       firebase.database().ref(this.sg['userdata'].vendor_id+'/miscellaneous').push(body).then(()=>{
         count ++;
         if(that.save_data.length === count){
         that.location.back();
         }
       })
     }
    }


  }
  addAnother(){
    if(this.page == 'Land Records'){
      let body = {
        category:this.category,
        name:this.name,
        description:this.description,
        attachment:this.attachment,
        survey_no:this.survey_no
      }
      this.save_data.push(body);
      this.category = null;
      this.name = null;
      this.description = null;
      this.attachment = [];
      this.survey_no = null;
    } else if(this.page == 'Farm Assets'){
      let body = {
        category:this.category,
        sub_category:this.sub_category,
        name:this.name,
        value:this.value,
        description:this.description,
        attachment:this.attachment,
      }
      this.save_data.push(body);
      this.category = null;
      this.sub_category = null;
      this.name =null;
      this.value = null;
      this.description = null;
      this.attachment = [];
    }  else if(this.page == 'Farm Utility Details') {
      let body = {
        category:this.category,
        name:this.name,
        description:this.description,
        attachment:this.attachment,
      }
      this.save_data.push(body);
      this.category = null;
      this.name =null;
      this.description = null;
      this.attachment = [];
    } else if(this.page == 'Licenses'){
      let body = {
        category:this.category,
        name:this.name,
        description:this.description,
        attachment:this.attachment,
      }
      this.save_data.push(body);
      this.category = null;
      this.name =null;
      this.description = null;
      this.attachment = [];
    } else if(this.page == 'Miscellaneous'){
      let body = {
        category:this.category,
        name:this.name,
        description:this.description,
        attachment:this.attachment,
      }
      this.save_data.push(body);
      this.category = null;
      this.name =null;
      this.description = null;
      this.attachment = [];
    }
   
  }
  async presentRemoveAlert(index,url){
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure want to Remove this attachment?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
           // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.removeAttachment(index,url);
           // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  removeAttachment(index,url) {
    let that;
    that = this;
      firebase.storage().ref(this.attachment[index].path.name).delete().then(function(snapshot) {
        that.attachment.splice(index, 1);
      });
    // }
   }
}
