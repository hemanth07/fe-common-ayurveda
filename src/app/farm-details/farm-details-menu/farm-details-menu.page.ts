import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { OnInit, Component, ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { AlertController, LoadingController , NavController} from '@ionic/angular';
import { FarmeasyTranslate } from '../../translate.service';
@Component({
  selector: 'app-farm-details-menu',
  templateUrl: './farm-details-menu.page.html',
  styleUrls: ['./farm-details-menu.page.scss'],
})
export class FarmDetailsMenuPage implements OnInit {
  headerData: any = [];
  menuData: any = [];
  constructor(private sg: SimpleGlobal, private loadingCtrl: LoadingController,private navCtrl:NavController,
    private translate: FarmeasyTranslate,
    private resolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private en: EnglishService,
    private hi: HindiService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService
    ) { }
  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    // color: green,orange,purple
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Details';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'వివరాలు';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'விவரங்கள்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ವಿವರಗಳು';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'विवरण';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      button2: 'notifications',
      button2_action: '/notifications',
      notification_count: this.sg['notificationscount'],
      };
  }
 translation(){
  if(this.sg['userdata'].primary_language === 'en'){
    this.menuData  = this.en.getFarmDetails();
  } else if(this.sg['userdata'].primary_language === 'te'){
    this.menuData  = this.te.getFarmDetails();
  } else if(this.sg['userdata'].primary_language === 'ta'){
    this.menuData  = this.ta.getFarmDetails();
  } else if(this.sg['userdata'].primary_language === 'kn'){
    this.menuData  = this.kn.getFarmDetails();
  }else if(this.sg['userdata'].primary_language === 'hi'){
    this.menuData  = this.hi.getFarmDetails();
  } else {
     this.menuData  = this.en.getFarmDetails();
  }
}
selectedTask(task) {
  this.sg['farm_details_item'] = task.keyword;
  this.navCtrl.navigateForward(task.action);
  }
}
