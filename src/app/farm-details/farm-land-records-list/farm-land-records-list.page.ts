import { AlertController, LoadingController } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";
import { SimpleGlobal } from "ng2-simple-global";
import * as firebase from "firebase";
import {  DocumentViewer,  DocumentViewerOptions} from "@ionic-native/document-viewer/ngx";
import { Http, Headers, RequestOptions } from "@angular/http";
import { map } from "rxjs/operators";
import {  FileTransfer,  FileUploadOptions,  FileTransferObject} from "@ionic-native/file-transfer/ngx"; 
import { File } from "@ionic-native/file/ngx";
import { Device } from "@ionic-native/device/ngx";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { NavParams, NavController, PopoverController} from '@ionic/angular';
import { ViewDocumentComponent } from '../../components/farm-details/view-document/view-document.component';
import { InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser/ngx';
import { OtherService } from "src/app/languages/others.service";
@Component({
  selector: "app-farm-land-records-list",
  templateUrl: "./farm-land-records-list.page.html",
  styleUrls: ["./farm-land-records-list.page.scss"]
})
export class FarmLandRecordsListPage implements OnInit {
  headerData: any = [];
  record_data: any = [];
  temp_record_data: any = [];
  page:any;
  searchInput:any;
  hideSearchIcon:boolean=true;
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};
  constructor(
    private sg: SimpleGlobal,
    private alertController: AlertController,
    private document: DocumentViewer,
    private http: Http,
    private transfer: FileTransfer,
    public file: File,
    private device: Device,
    private diagnostic: Diagnostic,
    private androidPermissions: AndroidPermissions,
    private popoverController: PopoverController,
    private iab: InAppBrowser,
    private loadingCtrl:LoadingController,
    private other:OtherService
  ) {}
  ngOnInit() {
    this.page = this.sg['farm_details_item'];
    this.createHeader();
    this.getRecords();
    // alert(this.device.platform);
    if (this.device.platform == "Android") {
      this.getPermission();
    }
  }
  getPermission() {
    this.diagnostic.getPermissionAuthorizationStatus(
        this.diagnostic.permission.READ_EXTERNAL_STORAGE
      ).then(
        status => {
        //  console.log(`AuthorizationStatus`);
      //    console.log(status);
          if (status != this.diagnostic.permissionStatus.GRANTED) {
            this.diagnostic
              .requestRuntimePermission(
                this.diagnostic.permission.READ_EXTERNAL_STORAGE
              )
              .then(data => {
           //     console.log(`getCameraAuthorizationStatus`);
           //     console.log(data);
              });
          } else {
            console.log("We have the permission");
          }
        },
        statusError => {
         // console.log(`statusError`);
         // console.log(statusError);
        }
      );
    this.diagnostic
      .getPermissionAuthorizationStatus(
        this.diagnostic.permission.WRITE_EXTERNAL_STORAGE
      )
      .then(
        status => {
        //  console.log(`AuthorizationStatus`);
        //  console.log(status);
          if (status != this.diagnostic.permissionStatus.GRANTED) {
            this.diagnostic
              .requestRuntimePermission(
                this.diagnostic.permission.WRITE_EXTERNAL_STORAGE
              )
              .then(data => {
              //  console.log(`getCameraAuthorizationStatus`);
              //  console.log(data);
              });
          } else {
            //console.log("We have the permission");
          }
        },
        statusError => {
         // console.log(`statusError`);
         // console.log(statusError);
        }
      );
  }
  getItems(data){
    this.hideSearchIcon=false;
    if(data == ""){
      this.record_data = this.temp_record_data;
    } else {
    this.record_data = this.temp_record_data.filter(item => item.description.toLowerCase().includes(data.toLowerCase()));
    }
  }
  createHeader() {
    // color: green,orange,purple
    let title;
    
    if(this.page == 'Land Records'){
      if (this.sg["userdata"].primary_language === "en") {
        title = "Farm Land Records";
      } else if (this.sg["userdata"].primary_language === "te") {
        title = "ఫార్మ్ ల్యాండ్ రికార్డ్స్";
      } else if (this.sg["userdata"].primary_language === "ta") {
        title = "பண்ணை நிலப் பதிவுகள்";
      } else if (this.sg["userdata"].primary_language === "kn") {
        title = "ಫಾರ್ಮ್ ಲ್ಯಾಂಡ್ ರೆಕಾರ್ಡ್ಸ್";
      } else if (this.sg["userdata"].primary_language === "hi") {
        title = "फार्म लैंड रिकॉर्ड";
      }
    } else if(this.page == 'Farm Assets'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Farm Assets';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఫార్మ్ ఆస్తులు';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை சொத்துக்கள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಸ್ವತ್ತುಗಳು';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'फार्म एसेट्स';
      }
    } else if(this.page == 'Farm Utility Details'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Farm Utility Details';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఫార్మ్ యుటిలిటీ వివరాలు';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'பண்ணை பயன்பாட்டு விவரங்கள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಫಾರ್ಮ್ ಯುಟಿಲಿಟಿ ವಿವರಗಳು';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'फार्म उपयोगिता विवरण';
      }
    } else if(this.page == 'Licenses'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Licenses';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'లైసెన్స్';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'உரிமங்கள்';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ಪರವಾನಗಿಗಳು';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'लाइसेंस';
      }
    } else if(this.page == 'Miscellaneous'){
      if (this.sg['userdata'].primary_language === 'en') {
        title = 'Miscellaneous';
      } else if (this.sg['userdata'].primary_language === 'te'){
        title = 'ఇతరాలు';
      }else if (this.sg['userdata'].primary_language === 'ta'){
        title = 'இதர';
      }else if (this.sg['userdata'].primary_language === 'kn'){
        title = 'ವಿವಿಧ';
      } else if (this.sg['userdata'].primary_language === 'hi'){
        title = 'कई तरह का';
      }
    }
    this.headerData = {
      color: "teal",
      title: title,
      button1: "add-circle-outline",
      button1_action: "/farm-land-records",
      button2: 'home',
      button2_action: '/farm-dashboard',
      };
  }
 async getRecords() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    if(this.page == 'Land Records'){
    firebase
      .database()
      .ref(this.sg['userdata'].vendor_id+"/farmLandRecords")
      .on("value", resp => {
        this.record_data = snapshotToArray(resp);
        this.temp_record_data = this.record_data;
        loading.dismiss();
      });
    } else if(this.page == 'Farm Assets'){
      firebase
        .database()
        .ref(this.sg['userdata'].vendor_id+"/farmAssets")
        .on("value", resp => {
          this.record_data = snapshotToArray(resp);
        this.temp_record_data = this.record_data;
        loading.dismiss();

        });
      } else if(this.page == 'Farm Utility Details'){
      firebase
        .database()
        .ref(this.sg['userdata'].vendor_id+"/farmUtilityDetails")
        .on("value", resp => {
          this.record_data = snapshotToArray(resp);
        this.temp_record_data = this.record_data;
        loading.dismiss();

        });
      } else if(this.page == 'Licenses'){
        firebase
        .database()
        .ref(this.sg['userdata'].vendor_id+"/licenses")
        .on("value", resp => {
          this.record_data = snapshotToArray(resp);
        this.temp_record_data = this.record_data;
        loading.dismiss();
        });
      }else if(this.page == 'Miscellaneous'){
        firebase
        .database()
        .ref(this.sg['userdata'].vendor_id+"/miscellaneous")
        .on("value", resp => {
          this.record_data = snapshotToArray(resp);
        this.temp_record_data = this.record_data;
        loading.dismiss();
        });
      }
  }
  async deleteRecord(record) {
    let url;
    if(this.page == 'Land Records'){
      url = this.sg['userdata'].vendor_id+"/farmLandRecords/"
     } else if(this.page == 'Farm Assets'){
       url = this.sg['userdata'].vendor_id+"/farmAssets/"
     } else if(this.page == 'Farm Utility Details'){
       url = this.sg['userdata'].vendor_id+"/farmUtilityDetails/"
    } else if(this.page == 'Licenses'){
      url = this.sg['userdata'].vendor_id+"/licenses/";
     }else if(this.page == 'Miscellaneous'){
       url = this.sg['userdata'].vendor_id+"/miscellaneous/";
     }
    const alert = await this.alertController.create({
      header: "Confirm!",
      message: "Are you sure want to Delete Record...?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
           // console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Yes",
          handler: () => {
           // console.log(record);
           if(record.attachment){
            for (let data of record.attachment) {
              firebase
                .storage()
                .ref(data.path.name)
                .delete();
            }
           }
            firebase
              .database()
              .ref(url + record.key)
              .remove();
          }
        }
      ]
    });
    await alert.present();
  }
async viewDocument(data) {
  let temp = data.attachment[0].path.name;
  let url = data.attachment[0].url;
  let filename = temp.substring(temp.lastIndexOf("/") + 1);
  let type =  filename.substring(filename.lastIndexOf(".")+1);
  if(type == 'pdf'){
    //  const browser = this.iab.create(url);
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    var storageLocation = "";
    let temp = data.attachment[0].path.name;
    let url = data.attachment[0].url;
   // console.log(this.file);
    let filename = temp.substring(temp.lastIndexOf("/") + 1);
    // alert(this.device.platform);
    switch (this.device.platform) {
      case "Android":
        storageLocation = "file:///storage/emulated/0/FarmEasy/LandRecords/"+filename;
        break;
      case "iOS":
        storageLocation = this.file.documentsDirectory+filename;
        break;

    }
    const fileTransfer: FileTransferObject = this.transfer.create();
    // alert(url);
    // alert(storageLocation);
    fileTransfer.download(url, storageLocation).then(
      entry => {
       let target = "_system";
       switch (this.device.platform) {
        case "Android":
          target = "_system";
          break;
        case "iOS":
        target = "_blank";
          break;
      }
       loading.dismiss();
    const browser =  this.iab.create(storageLocation,target,this.options);
      },
      error => {
        loading.dismiss();
        // handle error
        alert(JSON.stringify(error));
      }
    );
  } else {
   //  console.log("view document");
    const popover = await this.popoverController.create({
      component: ViewDocumentComponent,
      cssClass: 'view-document-class',
      componentProps: {
        Record: data
      },
    });
    await popover.present();
  }
 
  }
  recordAudio(){

  }
  async download(data) {
    var storageLocation = "";
    let temp = data.attachment[0].path.name;
    let url = data.attachment[0].url;
  //  console.log(this.file);
    let filename = temp.substring(temp.lastIndexOf("/") + 1);
    let type =  filename.substring(filename.lastIndexOf(".")+1);
    // alert(this.device.platform);
    let that = this;
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    switch (this.device.platform) {
      case "Android":
        storageLocation = "file:///storage/emulated/0/FarmEasy/"+filename;
        break;
      case "iOS":
        storageLocation = this.file.documentsDirectory+'/FarmEasy/'+filename;
        break;
    }
    const fileTransfer: FileTransferObject = this.transfer.create();
    // alert(url);
    // alert(storageLocation);
    fileTransfer.download(url, storageLocation).then(
      entry => {
      //  console.log("download complete: " + entry.toURL());
        loading.dismiss();
        alert("downloaded");
        if(type == 'pdf'){
          that.other.openFile(storageLocation);
        }
      },
      error => {
        loading.dismiss();
        // handle error
        alert('Something Went Wrong');
        // alert(JSON.stringify(error));
      }
    );
  }

}
export const snapshotToArray = snapshot => {
  const returnArr = [];

  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
