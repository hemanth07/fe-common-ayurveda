import { FarmDetailsModule } from './../../components/farm-details/farm-details.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmLandRecordsListPage } from './farm-land-records-list.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
const routes: Routes = [
  {
    path: '',
    component: FarmLandRecordsListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    FarmDetailsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmLandRecordsListPage]
})
export class FarmLandRecordsListPageModule {}
