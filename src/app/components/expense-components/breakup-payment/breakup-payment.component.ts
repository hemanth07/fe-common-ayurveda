import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, NavParams, ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-breakup-payment',
  templateUrl: './breakup-payment.component.html',
  styleUrls: ['./breakup-payment.component.scss']
})
export class BreakupPaymentComponent implements OnInit {
  breakUp:any;
  constructor(public popoverController:PopoverController,
              public navParams: NavParams,
              public navCtrl: NavController,) { }

  ngOnInit() {
    this.breakUp = this.navParams.get("message");
    this.calaculateBalance(this.breakUp);
    console.log(this.breakUp);
  }
  calaculateBalance(recover){
    let amount = 0;
    let balance = 0;
    for(let i=0;i<recover.recovered.length;i++) {
      amount += parseInt(recover.recovered[i].amount);
      balance = recover.amount - amount;
      this.breakUp.recovered[i]['balance'] = balance;
    }
  }
  comma(number){
    return number.toLocaleString();
  }
  dateConverter(date){
      let d = new Date(date)
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      return day+"-"+months[month]+"-"+year;
    }
  cancel(){
  this.popoverController.dismiss();
  }
  ok(){
  this.popoverController.dismiss();
  }
}
