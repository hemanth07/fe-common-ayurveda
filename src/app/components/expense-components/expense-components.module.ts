import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BreakupPaymentComponent } from './breakup-payment/breakup-payment.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(),
  ],
  declarations: [BreakupPaymentComponent],
  exports: [BreakupPaymentComponent],
  entryComponents: [BreakupPaymentComponent],
})
export class ExpenseComponentsModule {}
