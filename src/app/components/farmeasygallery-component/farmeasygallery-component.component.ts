import { PopOverComponent } from './../observation-components/pop-over/pop-over.component';
import { SimpleGlobal } from 'ng2-simple-global';
import { Component, OnInit, Input, Output, EventEmitter ,NgZone} from '@angular/core';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { LoadingController, PopoverController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { ProgressBarComponent } from '../progress-bar/progress-bar.component';
import { OtherService } from '../../languages/others.service';
@Component({
  selector: 'farm-gallery-component',
  templateUrl: './farmeasygallery-component.component.html',
  styleUrls: ['./farmeasygallery-component.component.scss']
})
export class FarmeasygalleryComponentComponent implements OnInit {
  url: any = []; cemera_image_url: any;
  @Input('editDescription') editDescription;
  @Output() attchments = new EventEmitter();
  @Output() video = new EventEmitter();
  @Output() audio = new EventEmitter();
  @Output() FarmeasyAdditionalNoates = new EventEmitter();
  @Output() cameraImage = new EventEmitter();
  @Output() height = new EventEmitter();

  additionalNotes: any;
  cemera_image: any;
  videoPreview: any = [];
  date: any;
  videosList: any = [];
  cameraImagesList: any = [];
  recordList: any = [];
  attachementImageslist: any = [];
  suggestions:any = [];
  filter_suggestions:any = [];
  hide_suggestions:boolean = true;
  constructor(private storage: Storage, private file: File,
    private media: Media, private camera: Camera,
    public popoverController: PopoverController,
    private mediaCapture: MediaCapture,
    public loadingCtrl: LoadingController,
    private imagePicker: ImagePicker,
    public sg: SimpleGlobal,
    public other:OtherService,
    private zone:NgZone) {
  }

  ngOnInit() {
    // this.translation();
  if (this.editDescription) {
      this.additionalNotes = this.editDescription.description;
      console.log(this.additionalNotes);
    }
    this.suggestions = this.other.getTaskSuggestions();
    console.log(this.suggestions);

  }
  //--------------------------FILE UPLOAD START---------------------------------//

  ngOnChanges() {
    if (this.editDescription) {
      this.additionalNotes = this.editDescription.description;
      console.log(this.additionalNotes);
    }

  }
  getAdditionalData(val) {
    this.filter_suggestions = this.suggestions.filter(sugg => sugg.toLowerCase().includes(val.detail.value.toLowerCase()));
    console.log(this.filter_suggestions);
    if(val.detail.value != ""){
      if(this.filter_suggestions.length > 0){
        if(this.filter_suggestions.length == 1){
          if(this.filter_suggestions[0] == val.detail.value){
            this.hide_suggestions = true;
          } else {
            this.hide_suggestions = false;
          }
        }  else {
          this.hide_suggestions = false;
        }
      } else {
        this.hide_suggestions = true;
      }
    } else {
      this.hide_suggestions = true;
    }
 
     // console.log(val);
    this.FarmeasyAdditionalNoates.emit(val.detail.value);

  }
  pickSuggestion(sugg){
    console.log(sugg);
    if(this.filter_suggestions.length == 1){
      if(this.filter_suggestions[0] == sugg){
        this.hide_suggestions = true;
      } else {
        this.hide_suggestions = false;
      }
    } else {
      if(this.filter_suggestions.length != 0){
        this.hide_suggestions = false;
      } 
    }
    this.additionalNotes = sugg;
    this.FarmeasyAdditionalNoates.emit(sugg);
  }
  translation() {
    if (this.sg['userdata'].primary_language === 'en') {
      this.additionalNotes = 'Any additional Notes';
    } else if (this.sg['userdata'].primary_language === 'te') {
      this.additionalNotes = 'ఏదైనా అదనపు గమనికలు';
    } else if (this.sg['userdata'].primary_language === 'ta') {
      this.additionalNotes = 'எந்த கூடுதல் குறிப்புகள்';
    } else if (this.sg['userdata'].primary_language === 'kn') {
      this.additionalNotes = 'ಯಾವುದೇ ಹೆಚ್ಚುವರಿ ಟಿಪ್ಪಣಿಗಳು';
    } else if (this.sg['userdata'].primary_language === 'hi') {
      this.additionalNotes = 'कोई भी अतिरिक्त नोट्स';
    } else {
      this.additionalNotes = 'Any additional Notes';
    }
  }



  multipleImageAttachment() {

    let urls;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    this.camera.getPicture(options).then(async (imageData) => {
      this.date = new Date().getTime();
      // const loading = await this.loadingCtrl.create({
      //   spinner: 'bubbles',
      //   message: 'Please wait...',
      //   translucent: true,
      //   cssClass: 'custom-class custom-loading'
      // });
      // loading.present();
      const popover = await this.popoverController.create({
        component: ProgressBarComponent,
        translucent: true,
        backdropDismiss:false,
        cssClass:'progress-bar-popover',
        componentProps: {
         'source':'Image'
        },
      });
      await popover.present();
      popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
      });
      let that;
      that = this;
      let img_url = 'data:image/jpeg;base64,' + imageData;
      fetch(img_url)
        .then(res => res.blob())
        .then(blob => {
          urls = { 'name': this.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + this.date };
          this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + this.date);
          firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + this.date).put(blob).then(function (snapshot) {
            setTimeout(() => {
              firebase.storage().ref(urls.name).getDownloadURL().then(function (url) {
                // loading.dismiss();
                const body = { 'type': 'gallery', 'url': url, 'path': urls };
                //alert(JSON.stringify(body));
                that.attchments.emit(body);
                popover.dismiss();
              });
            }, 1000);
          });
        that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + that.date ).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
        });
    }, (err) => {
      // Handle error
      alert(err);
    });
  }
 async captureImage() {
      //  const popover = await this.popoverController.create({
      //       component: ProgressBarComponent,
      //       translucent: true,
      //       backdropDismiss:false,
      //       componentProps: {
      //    'source':'Image'
      //   },
      //     });
      // await popover.present();
      //     popover.onDidDismiss().then(resp =>{
      //       if(resp.data == 'Close'){
      //         return false;
      //       }
      // });
    let urls;
    let body = [];
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
      this.date = new Date().getTime();
      const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            cssClass:'progress-bar-popover',
            backdropDismiss:false,
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
      let that;
      that = this;
      let img_url = 'data:image/jpeg;base64,' + imageData;
      fetch(img_url)
        .then(res => res.blob())
        .then(blob => {
          urls = { 'name': this.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + this.date };
          this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + this.date);
          firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + this.date).put(blob).then(function (snapshot) {
            setTimeout(() => {
              firebase.storage().ref(urls.name).getDownloadURL().then(function (url) {
                // loading.dismiss();
                that.cameraImagesList.push({ 'type': 'camera', 'url': url, 'path': urls.name });

                that.cameraImage.emit(that.cameraImagesList);
                popover.dismiss();
              });
            }, 1000);
          });
        that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + that.date ).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
        });
    }, (err) => {
      // Handle error
      alert(JSON.stringify(err));
    });
   
  }
  captureVedio() {
    const options: CaptureVideoOptions = {
      limit: 1,
      duration: 30
    };
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    this.mediaCapture.captureVideo(options)
      .then(
       async (data: MediaFile[]) => {
          const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Video'
        },
          });
          await popover.present();
          popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
          });
          let that;
          that = this;
          const index = data[0].fullPath.lastIndexOf('/');
          const finalPath = data[0].fullPath.substr(0, index);
          this.file.readAsArrayBuffer(finalPath, data[0].name).then(async (file) => {
            const blob = new Blob([file], { type: data[0].type });
            // const loading = await this.loadingCtrl.create({
            //   spinner: 'bubbles',
            //   message: 'Please wait...',
            //   translucent: true,
            //   cssClass: 'custom-class custom-loading'
            // });
            // loading.present();
            let urls = { 'name': this.sg['userdata'].vendor_id + '/farmeasy/taskManager/videos/' + data[0].name };
          this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/videos/' + data[0].name);
            firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/videos/' + data[0].name).put(blob).then(function (snapshot) {
              firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/videos/' + data[0].name).getDownloadURL().then(function (url) {
                // loading.dismiss();
                that.videosList.push({ type: 'video', url: url, 'path': urls.name });
                // alert(JSON.stringify( that.videosList));
                that.video.emit(that.videosList);
                that.popoverController.dismiss();

              });
            });
          that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/videos/' + data[0].name).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
            that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            
            // alert(that.sg['uploadProgress']);
            });
          });
        });
      },
        (err: CaptureError) => alert(err)
      );
  }
  recordAudio() {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    let options: CaptureImageOptions = { limit: 1 }
    this.mediaCapture.captureAudio(options)
      .then(
        async (data: MediaFile[]) => {
          // alert('data' + JSON.stringify(data))
          const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Audio'
        },
          });
          await popover.present();
          popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
           });
          let that;
          that = this;
          const index = data[0].fullPath.lastIndexOf('/');
          const finalPath = data[0].fullPath.substr(0, index);
          this.file.readAsArrayBuffer(finalPath, data[0].name).then(async (file) => {
            const blob = new Blob([file], { type: data[0].type });
            // const loading = await this.loadingCtrl.create({
            //   spinner: 'bubbles',
            //   message: 'Please wait...',
            //   translucent: true,
            //   cssClass: 'custom-class custom-loading'
            // });
            // loading.present();
            let urls = { 'name': this.sg['userdata'].vendor_id + '/farmeasy/taskManager/audios/' + data[0].name };
          this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/audios/' + data[0].name);
            firebase.storage().ref(this.sg['userdata'].vendor_id + '/farmeasy/taskManager/audios/' + data[0].name).put(blob).then(function (snapshot) {
              firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/audios/' + data[0].name).getDownloadURL().then(function (url) {
                that.recordList.push({ type: 'audio', url: url, path: urls.name });
                // alert(JSON.stringify(that.recordList));
                that.audio.emit(that.recordList);
              });
            });
          that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/audios/' + data[0].name).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });

          });
        },
        (err: CaptureError) => alert('error' + JSON.stringify(err))
      );
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
