import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, } from '@angular/core';
import * as firebase from 'firebase';
import { FormsModule } from '@angular/forms';
import { Router, Route } from '@angular/router';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { LoadingController } from '@ionic/angular';
import { from } from 'rxjs';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../../translate.service';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';

@Component({
  selector: 'farmeasy-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  @Input('tasksData') tasksData;
  @Input('Data') Data;
  @Input('filteredTasksByDate') filteredTasksByDate;
  @Input('categoryName') categoryName;
  @Input('statusValue') statusValue;
  @Input('blockName') blockName;
  @Input('labourName') labourName;
  @Input('clearFiltersData') clearFiltersData;
  @Output() audio: EventEmitter<any> = new EventEmitter();
  @Output() totalTasksList: EventEmitter<any> = new EventEmitter();
  @Input('refresh_data') refresh_data;
  @Input('searchValue') searchValue;
  @Input('selectAllTasksData') selectAllTasksData;
  @Input('reports') reports;
  @Output() status: EventEmitter<any> = new EventEmitter();
  @Output() deletedTaskList: EventEmitter<any> = new EventEmitter();
  @ViewChild('infi') infi: ElementRef;
  @ViewChild('content') content;
  showCompleted: any = [];
  showAcknowledged: any = [];
  // tasksData: any = [];
  deletedTasksData: any = [];
  isTaskData: boolean = false; hideSearchIcon: boolean = true;
  increment: any = 0;
  labels: any; totalTasksData: any = [];
  count: number = 10;tasks:any=[]
  doScroll: boolean = true; check: any = [];
  reportsData:any = [];
  constructor(public router: Router, private media: Media,
    private mediaCapture: MediaCapture,
    private file: File, public loadingCtrl: LoadingController, public sg: SimpleGlobal,
    private en: EnglishService, private hi: HindiService, private ta: TamilService, private te: TeluguService,
    private kn: KanadaService, private translate: FarmeasyTranslate) {

  }

  ngOnInit() {
    // this.createSingleTasksFromMultiple();
   
    this.translation();
    // this.createSingleTasksFromMultiple();
    // this.getTotalTasksData();
    this.deletedTasksData=[];
    // firebase.database().ref('TaskManager/Task_table').orderByChild('date').endAt(1565136000000).on('value', resp => {
    //   let data = snapshotToArray(resp);
    //   console.log(data);
    // });
  }

  ngOnChanges() {
    if(this.tasksData){
      console.log(new Date().toISOString());
    }
    console.log(new Date().getTime());
    console.log(this.tasksData);
    if (this.refresh_data) {
      this.deletedTasksData=[];
    }
    if(this.selectAllTasksData){
      this.selectAllTasks();
    } else {
      this.unSelectAllTasks();
    }
  }
  selectAllTasks(){
    this.check=[];
    for(var i=0;i<this.tasksData.length;i++){
      this.check.push(true)
    }
  }
  unSelectAllTasks(){
    this.check=[];
    for(var i=0;i<this.tasksData.length;i++){
      this.check.push(false)
    }
  }
  markAsDone(val, idx) {
    let status = { "name": 'Task completed' };
    var data = { 'key': val.key, 'status': status };
    this.showCompleted[idx] = true;
    this.status.emit(data);
  }
  markAsAcknowledged(val, idx) {
    var data = { 'key': val.key, 'status': 'Acknowledged' };
    this.showAcknowledged[idx] = true;
    this.status.emit(data);
  }
  getTask(data) {
    let temp = { 'task': JSON.stringify(data) };
    this.router.navigate(['/task-detailed-view', temp]);
  }
  checked(status, data, idx) {
    if (status == true) {
      this.deletedTasksData.push(this.tasksData[idx])
      this.deletedTaskList.emit(this.deletedTasksData);
      // this.deletedTasksData=[];
    } else {
      for (var i = 0; i < this.deletedTasksData.length; i++) {
        if (data.key == this.deletedTasksData[i].key) {
          this.deletedTasksData.splice(i, 1);
          this.deletedTaskList.emit(this.deletedTasksData);
          // this.deletedTasksData=[];
        }
      }
    }
  }
  translation() {
    if (this.sg['userdata'].primary_language === 'en') {
      this.labels = this.en.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'te') {
      this.labels = this.te.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'ta') {
      this.labels = this.ta.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'kn') {
      this.labels = this.kn.getTaskListLabels();
    } else if (this.sg['userdata'].primary_language === 'hi') {
      this.labels = this.hi.getTaskListLabels();
    }
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
