import { FarmeasyTranslate } from './../../../translate.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Inject, LOCALE_ID } from '@angular/core';
import { AlertController, NavController, PopoverController } from '@ionic/angular';
import { SelectPlotsBlockComponent } from '../select-plots-block/select-plots-block.component';
import {SimpleGlobal} from 'ng2-simple-global';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';
import { CalendarComponent } from 'ionic2-calendar';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-frequency-selection',
  templateUrl: './frequency-selection.component.html',
  styleUrls: ['./frequency-selection.component.scss']
})
export class FrequencySelectionComponent implements OnInit {
  @Input() Data: any;
  // @Input() selectedCategory: any;
  @Input("selectedCategory") selectedCategory;
  @Input("selectedDates") selectedDates;
  @Input("selectedDate") selectedDate;
  @Input("selectedStartDate") selectedStartDate;
  @Input("selectedEndDate") selectedEndDate;
  @Input('editTask') editTask;
  @Output() selectedFrequency: EventEmitter<any> = new EventEmitter();
  @Output() isDateClicked: EventEmitter<any> = new EventEmitter();
  @Output() Day: EventEmitter<any> = new EventEmitter();
  daysOfWeek: any;
  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };

  minDate = new Date().toISOString();
  allEvents: any = []
  // eventSource = [];
  viewTitle;
  eventSource;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
  
  @ViewChild(CalendarComponent) myCal: CalendarComponent;
 
  selectFrequencies: any;
  category: any;
  currentDate: any;
  endDate: any;
  userSelectedFrequency: any;
  checkSelectedDay: any = [];
  frequency: any;
  selectedDates1: any;
  selectedDateMonth:any;
  selectedDateDate:any;
  activeToggle: boolean = false;
  labels: any;
  months:any = [];
  constructor(private navCtrl: NavController,
    private popoverController: PopoverController,
    private sg: SimpleGlobal,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService,
    private alertCtrl: AlertController, @Inject(LOCALE_ID) private locale: string,
    private translate:FarmeasyTranslate) { }

  ngOnInit() {
    this.selectFrequencies = this.Data;
    this.translatingData(this.Data.options);
    this.initializeData();
    this.initializeFrequency();
  }
  ngOnDestroy() {
    //this.selectFrequencies = null;
  }
  initializeData() {
    this.translation();
    var date = new Date().getTime();
    var current_date = new Date(date).toISOString().split("T")[0];
    var end_date = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString().split("T")[0];
    // var current_month = current_date.getMonth() + 1;
    // var end_month = end_date.getMonth() + 1;
    if (!this.editTask) {
     // this.currentDate = current_date.getDate() + "-" + current_month + "-" + current_date.getFullYear();
      //this.endDate = end_date.getDate() + "-" + end_month + "-" + end_date.getFullYear();
      this.currentDate = current_date;
      this.endDate = end_date;
    }
    if(this.sg['userdata'].primary_language === 'en'){
      this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.months = ["జనవరి", "ఫిబ్రవరి", "మార్", "ఏప్రిల్", "మే", "జూన్", "జూలై", "ఆగస్టు", "సెప్టెంబర్", "అక్టోబర్", "నవంబర్", "డిసెంబర్"];
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.months = ["ஜன", "பிப்ரவரி", "மார்", "ஏப்ரல்", "மே", "ஜூன்", "ஜூலை", "ஆகஸ்ட்", "செப்டம்பர்", "அக்", "நவம்பர்", "டிசம்பர்"];
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.months = ["ಜನವರಿ", "ಫೆಬ್ರವರಿ", "ಮಾರ್ಚ್", "ಎಪ್ರಿಲ್", "ಮೇ", "ಜೂನ್", "ಜುಲೈ", "ಆಗಸ್ಟ್", "ಸೆಪ್ಟೆಂಬರ್", "ಅಕ್ಟೋಬರ್", "ನವೆಂಬರ್", "ಡಿಸೆಂಬರ್"];
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.months = ["जन", "फ़रवरी", "मार्च", "अप्रैल", "मई", "जून", "जुलाई", "अगस्त", "सेप्ट", "अक्टूबर", "नोव", "दिसंबर"];
    }
  }
  initializeFrequency() {
    if (this.selectFrequencies) {
      console.log(this.selectFrequencies);
      for (var i = 0; i < this.selectFrequencies.days_of_week.length; i++) {
        this.checkSelectedDay[i] = false;
        if (this.editTask.frequency && this.editTask.frequency.daysOfWeek) {
          for (var j = 0; j < this.editTask.frequency.daysOfWeek.length; j++) {
            if (this.editTask.frequency.daysOfWeek[j].name == this.selectFrequencies.days_of_week[i].name) {
              this.checkSelectedDay[i] = true;
              this.Day.emit(this.selectFrequencies.days_of_week[i]);
            }
          }
        }
      }
    }
    if (this.editTask && this.editTask.end_date && this.editTask.start_date) {
      this.activeToggle = true;
    }
    if (this.editTask && this.editTask.frequency && this.editTask.frequency.dates) {
      if (this.editTask.frequency.dates.length > 0) {
        this.selectedDates1 = this.editTask.frequency.dates;
      }
    }
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels  = this.en.getFrequencySelectionLabels();
      } else if(this.sg['userdata'].primary_language === 'te'){
        this.labels  = this.te.getFrequencySelectionLabels();
      } else if(this.sg['userdata'].primary_language === 'ta'){
        this.labels  = this.ta.getFrequencySelectionLabels();
      } else if(this.sg['userdata'].primary_language === 'kn'){
        this.labels  = this.kn.getFrequencySelectionLabels();
      }else if(this.sg['userdata'].primary_language === 'hi'){
        this.labels  = this.hi.getFrequencySelectionLabels();
      }
  }

  selectFrequency(val) {
    console.log(val);
    this.frequency = val.detail.value;
    this.selectedFrequency.emit(val.detail.value);
  }
  selectedDay(val, idx) {
    if (this.checkSelectedDay[idx] == true) {
      this.checkSelectedDay[idx] = false;
    }
    else {
      this.checkSelectedDay[idx] = true;
    }

    this.Day.emit(val);
  }
  ngOnChanges() {
    console.log(this.selectedDates);
    this.category = this.selectedCategory;
    console.log(this.category);
    let that=this;
  

    if (this.editTask) {
      console.log(this.editTask);
      this.userSelectedFrequency = this.editTask.frequency.name;
      this.selectedFrequency.emit(this.userSelectedFrequency);
      var startDate = new Date(this.editTask.start_date).toISOString().split("T")[0];
      var endDate = new Date(this.editTask.end_date).toISOString().split("T")[0];
      // var m1 = startDate.getMonth() + 1;
      // var m2 = endDate.getMonth() + 1;
      // this.currentDate = startDate.getFullYear() + '-' + m1 + '-' + startDate.getDate();
      // this.endDate = endDate.getFullYear() + '-' + m2 + '-' + endDate.getDate();
       this.currentDate = startDate;
      this.endDate = endDate;
      this.initializeFrequency();

    }
    if (that.selectedStartDate) {
      that.currentDate = that.selectedStartDate.split("-").reverse().join("-");
      if(this.frequency == 'One Time'){
      that.endDate = that.selectedStartDate.split("-").reverse().join("-");
      }
    }
    if (that.selectedEndDate) {
      that.endDate = that.selectedEndDate.split("-").reverse().join("-");
   
    }
    if (this.selectedDates.length>0) {
      this.selectedDates1 = this.selectedDates;
    }
    if(this.selectedDate){
      console.log(this.selectedDate);
      let date = this.selectedDate.split("-");
      this.selectedDateDate = date[0];
      this.selectedDateMonth = parseInt(date[1])-1;
    }

  }
  dateConverter(date){
    if(date){
      let d = new Date(date);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      if(isNaN(day)){
        return "DD-Mon-YYYY";
      } else {
      return day+"-"+months[month]+"-"+year;
      }
    }
  }

  openStartEndDatePopover(val) {
    console.log(val);
    let temp = { 'name': val }
    //if(val.detail.checked){
    this.isDateClicked.emit(temp);
    // }

  }
  translatingData(data){
    console.log(data);
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        name:item.name,
      }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.selectFrequencies.options[i]['view'] = data['name'];
        });
      } else {
        this.selectFrequencies.options[i]['view'] = menu[i]['name'];
       }
    }
  }
 
  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }
 
  // Create the right event format and reload source
  addEvent() {
    let eventCopy = {
      title: this.event.title,
      startTime:  new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay,
      desc: this.event.desc
    }
 
    if (eventCopy.allDay) {
      let start = eventCopy.startTime;
      let end = eventCopy.endTime;
 
      eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
      eventCopy.endTime = new Date(Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), end.getUTCDate() + 1));
    }
 
    this.eventSource.push(eventCopy);
    this.myCal.loadEvents();
    this.resetEvent();
  }
  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }
   
  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }
   
  // Change between month/week/day
  changeMode(mode) {
    this.calendar.mode = mode;
  }
   
  // Focus today
  today() {
    this.calendar.currentDate = new Date();
  }
   
  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
   
  // Calendar event was clicked
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    let start = formatDate(event.startTime, 'medium', this.locale);
    let end = formatDate(event.endTime, 'medium', this.locale);
   
    const alert = await this.alertCtrl.create({
      header: event.title,
      subHeader: event.desc,
      message: 'From: ' + start + '<br><br>To: ' + end,
      buttons: ['OK']
    });
    alert.present();
  }
   
  // Time slot was clicked
  onTimeSelected(ev) {
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());
  }
  // getEndDate(){
  //   this.isDateClicked.emit("endDate");
  // }
}
