import { Component, OnInit,Input,Output, EventEmitter} from '@angular/core';
import { NavParams,NavController,PopoverController,ModalController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { from } from 'rxjs';
import {SimpleGlobal} from 'ng2-simple-global';
@Component({
  selector: 'app-select-plots-block',
  templateUrl: './select-plots-block.component.html',
  styleUrls: ['./select-plots-block.component.scss']
})
export class SelectPlotsBlockComponent implements OnInit {
  @Input() Data: any;
  @Output() selected_plots: EventEmitter<any>  = new EventEmitter();
  selectedBlock:any;
  selectedCategory:any;
  plots:any=[];
  checkImage:any=[];
  activeDate:any=[];
  selectedPlotsList:any=[];
  isSelectedBlocks:boolean=true;
  year:any;
	month:any;
	day:any;
	hours:any;
	minutes:any;
  alphaDay:any;
  selectedDate:any;
  selectedDates:any=[];
  totalblocks:any=[];
  unSelectedBlocks:any=[];
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  labels: any;
  private BlockEmitter: EventEmitter< any >;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private popoverController: PopoverController,private sg: SimpleGlobal,
    private storage: Storage,public modalController: ModalController) { }

  ngOnInit() {
    this.selectedBlock = this.navParams.get('blockName');  
    this.selectedCategory = this.navParams.get('category');
    this.totalblocks = this.navParams.get('totalBlocks');
    this.BlockEmitter = this.navParams.get('theEmitter');
    console.log(this.selectedBlock);
    this.initializeData();
    this.date = new Date();
    this.translation();
  }
  
  initializeData(){
    this.date = new Date();
    if(this.selectedBlock){
     let index=0;
      for(var i=0;i<this.selectedBlock.no_of_plots;i++){
        index=i+1;
        this.plots.push({'name':this.selectedBlock.name+index,"isSelectedPlot":false});
        this.checkImage[i]=false;
      }
      console.log(this.plots);
    }
    console.log(this.selectedBlock);
    if(this.selectedBlock.plots){
      for(var i=0;i<this.plots.length;i++){
        for(var j=0;j<this.selectedBlock.plots.length;j++){
          if(this.plots[i].name==this.selectedBlock.plots[j].name){
            if(this.selectedBlock.plots[j].isSelectedPlot==true){
              this.plots[i].isSelectedPlot=true;
              this.checkImage[i]=true;
              this.isSelectedBlocks=false;
            }
          }
        }
      }
     
    }
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
  this.labels= {
    selectplot:' Select Plots in Block',
    ok:'Ok',
    cancel:'cancel',
    select:'select'
    };
   } else if(this.sg['userdata'].primary_language === 'te'){
  this.labels= {
    selectplot:'బ్లాక్లో ప్లాట్లు ఎంచుకోండి',
    ok: "అలాగే",
    cancel: "రద్దు",
    select:'ఎంచుకోండి'
  }
   } else if(this.sg['userdata'].primary_language === 'ta'){
  this.labels= {
    selectplot:'பிளாக் உள்ள பிளாட்ஸைத் தேர்ந்தெடுக்கவும்',
    ok: "சரி",
    cancel: "ரத்து",
    select:'தேர்வு'
  };
   } else if(this.sg['userdata'].primary_language === 'kn'){
  this.labels= {
    selectplot:'ಬ್ಲಾಕ್ನಲ್ಲಿನ ಪ್ಲಾಟ್ಗಳು ಆಯ್ಕೆಮಾಡಿ',
    ok: "ಸರಿ",
    cancel: "ರದ್ದುಗೊಳಿಸಲು",
    select:'ಆಯ್ಕೆಮಾಡಿ'
  };
   } else if(this.sg['userdata'].primary_language === 'hi'){
  this.labels= {
    selectplot:'ब्लॉक में भूखंडों का चयन करें',
    ok: "ठीक",
    cancel: "रद्द करना",
    select:'चुनते हैं'
  };
   }
}
  getBlock(index){
    
    console.log(this.checkImage);
    for(var i=0;i<this.plots.length;i++){
      if(i==index){
        if(this.checkImage[i]==true){
          this.checkImage[i]=false;
          this.plots[i].isSelectedPlot=false;
          for(let j=0;j<this.selectedPlotsList.length;j++){
            if(this.plots[i].name  == this.selectedPlotsList[j].name){
              this.selectedPlotsList.splice(j,1);
            }
          }
        }
        else{
          this.checkImage[i]=true;
          this.plots[i].isSelectedPlot=true;
          this.selectedPlotsList.push(this.plots[i]);
          // this.BlockEmitter.emit({plot:this.plots[i].name,selected:this.selectedPlotsList,blockName:this.selectedBlock.name});
          //this.plots['blockName']=this.selectedBlock.name;
        }
        if(this.selectedPlotsList.length>=1){
          this.isSelectedBlocks=false;
        }
      }
      
    }
    console.log(this.selectedPlotsList);
    console.log(this.checkImage);
  }
  blockPopOverCancel(){
    this.popoverController.dismiss();
  }
  blockPopOverOk(){
   var plots:any=[];
    console.log(this.plots);
    for(var i=0;i<this.plots.length;i++){
      if(this.plots[i].isSelectedPlot==true){
        plots.push(this.plots[i]);
      }
    }
    plots['blockName']=this.selectedBlock.name;
    console.log(plots);
    this.popoverController.dismiss(plots); 
  }
  
  
}
