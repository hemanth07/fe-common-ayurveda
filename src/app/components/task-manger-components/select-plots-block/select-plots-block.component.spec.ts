import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPlotsBlockComponent } from './select-plots-block.component';

describe('SelectPlotsBlockComponent', () => {
  let component: SelectPlotsBlockComponent;
  let fixture: ComponentFixture<SelectPlotsBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPlotsBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPlotsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
