import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, PopoverController } from '@ionic/angular';
import {SimpleGlobal} from 'ng2-simple-global';

@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss']
})
export class SelectDateComponent implements OnInit {
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  selectedDate: any = []
  selectedDatePopOver: any;
  showCurrentDateBackground: any = [];
  showLastDateBackground: any = [];
  showNextDateBackground: any = [];
  isDateSelected: any = [];
  cancelDate: any = [];
  endDate: any;
  showDate: boolean = false;
  selectedDateType: any;
  startEndDate: boolean = false;
  StartOrEndDate: any;
  todayDate: any;
  showOk: boolean = true;
  dateAfterOneYear: any;
  presentDate: any;
  dates:any;
  labels: any;
  blockpastdates:any = [];
  month:any;
  year:any;
  startDateFormate: string;
  EndDateFormate: string;
  rand_date:string;
  constructor(public navCtrl: NavController,private sg: SimpleGlobal,
     public navParams: NavParams, private popoverController: PopoverController) { }

  ngOnInit() {
    this.translation();
    this.selectedDate = [];
    this.selectedDatePopOver = this.navParams.get('date');
    var today_date = this.navParams.get('startDate');
    var end_date1 = this.navParams.get('endDate');
    this.dates = this.navParams.get('dates'); 
    
    var d1 = new Date(today_date);
    var d2 = new Date(end_date1);
    var m1 = d1.getMonth() + 1;
    var m2 = d2.getMonth() + 1;
    this.todayDate =  d1.getDate() + '-' + m1 + '-' + d1.getFullYear() ;
    this.startDateFormate = d1.getFullYear() + '-' + m1 + '-' + d1.getDate();
    this.endDate = d2.getDate() + '-' + m2 + '-' + d2.getFullYear() ;
    this.EndDateFormate = d2.getFullYear() + '-' + m2 + '-' + d2.getDate();
    this.date = new Date();
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (var j = 0; j <= thisNumOfDays; j++) {
      this.blockpastdates.push(false);
    }
    this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    var current_date = new Date();
    var end_date = new Date(new Date().setFullYear(new Date().getFullYear() + 1))
    var current_month = current_date.getMonth() + 1;
    var end_month = end_date.getMonth() + 1;
    this.presentDate = current_date.getFullYear() + "-" + current_month + "-" + current_date.getDate();
    this.dateAfterOneYear = end_date.getFullYear() + "-" + end_month + "-" + end_date.getDate();
    if (!today_date) {
      this.todayDate = current_date.getDate() + "-" + current_month + "-" + current_date.getFullYear();
      this.startDateFormate=this.presentDate;
    }
    if (!end_date1) {
      this.endDate = this.dateAfterOneYear;
    }
    console.log(this.todayDate);
    console.log(this.endDate);

    if (this.selectedDatePopOver && this.selectedDatePopOver.name) {
      this.showDate = true;
      this.startEndDate = false;
      this.StartOrEndDate = this.selectedDatePopOver.name;
      this.getDaysOfMonth();
    }
    else if (this.selectedDatePopOver.id == '1') {
      this.showDate = false;
      this.startEndDate = true;
    }
  }
  translation(){
      if(this.sg['userdata'].primary_language === 'en'){
    this.labels= {
      StartDate:'Start Date',
      EndDate:'End Date',
      ok:'OK',
      cancel:'cancel'
      };
     } else if(this.sg['userdata'].primary_language === 'te'){
    this.labels= {
      StartDate: "ప్రారంబపు తేది",
      EndDate: "ఆఖరి తేది",
      ok: "అలాగే",
      cancel: "రద్దు"
    }
     } else if(this.sg['userdata'].primary_language === 'ta'){
    this.labels= {
      StartDate: "தொடக்கத் தேதி",
      EndDate: "கடைசி தேதி",
      ok: "சரி",
      cancel: "ரத்து"
    };
     } else if(this.sg['userdata'].primary_language === 'kn'){
    this.labels= {
      StartDate: "ಪ್ರಾರಂಭ ದಿನಾಂಕ",
      EndDate: "ಅಂತಿಮ ದಿನಾಂಕ",
      ok: "ಸರಿ",
      cancel: "ರದ್ದುಗೊಳಿಸಲು"
    };
     } else if(this.sg['userdata'].primary_language === 'hi'){
    this.labels= {
      StartDate: "आरंभ करने की तिथि",
      EndDate: "अंतिम तिथि",
      ok: "ठीक",
      cancel: "रद्द करना"
    };
     }
  }
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.month = this.monthNames[new Date().getMonth()]
    this.year =  new Date().getFullYear();
    this.currentYear = this.date.getFullYear();

    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
      this.showLastDateBackground[i] = false;

    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (var j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j + 1);
      this.showCurrentDateBackground[j] = false;
      //this.isDateSelected[j]['isDateSelected']=false;
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    // var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var k = 0; k < (6 - lastDayThisMonth); k++) {
      this.daysInNextMonth.push(k + 1);
      this.showNextDateBackground[k + 1] = false;
    }
    var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (var l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
        this.daysInNextMonth.push(l);
        // this.showBackground[l]=false;
      }
    }
    console.log(this.daysInNextMonth);
    console.log(this.daysInThisMonth);
    console.log(this.daysInLastMonth);
    this.preFillDates();

  }
  preFillDates() {
    if (this.selectedDatePopOver.name == 'Start Date') {
      if (this.startDateFormate) {
        this.date.getMonth() + 1
        var d1 = new Date(this.startDateFormate);
        for (var i = 0; i < this.daysInThisMonth.length; i++) {
          if(this.currentDate>this.daysInThisMonth[i] && this.currentMonth === this.month){
            this.blockpastdates[i]=true;
           }
           else{
             this.blockpastdates[i]=false;
           }
          if (this.daysInThisMonth[i] == d1.getDate() && this.date.getMonth() + 1 == d1.getMonth() + 1 &&
            d1.getFullYear() == this.date.getFullYear()) {
            this.showCurrentDateBackground[i] = true;
            this.showOk=false;
          }
        }
        console.log(this.showCurrentDateBackground);

        // var m1=d1.getMonth()+1;

        // this.todayDate=d1.getFullYear()+'-'+m1+'-'+d1.getDate();
      }
    }
    if (this.selectedDatePopOver.name == 'End Date') {
      if (this.endDate) {
        this.date.getMonth() + 1
        var d1 = new Date(this.endDate);
        for (var i = 0; i < this.daysInThisMonth.length; i++) {
          if(this.currentDate>this.daysInThisMonth[i] && this.currentMonth === this.month){
            this.blockpastdates[i]=true;
           }
           else{
             this.blockpastdates[i]=false;
           }
          if (this.daysInThisMonth[i] == d1.getDate() && this.date.getMonth() + 1 == d1.getMonth() + 1 &&
            d1.getFullYear() == this.date.getFullYear()) {
            this.showCurrentDateBackground[i] = true;
            this.showOk=false;
          }
        }
        console.log(this.showCurrentDateBackground);

      }
    }
    if (this.selectedDatePopOver.name == 'Select Date(s)') {
      if (this.dates) {
        var d1 = new Date(this.endDate);
        for (var i = 0; i < this.daysInThisMonth.length; i++) {
          for(var j=0;j<this.dates.length;j++){
            if (this.daysInThisMonth[i] == this.dates[j]){
              this.showCurrentDateBackground[i] = true;
              this.showOk=false;
          }
          }
        }
        console.log(this.showCurrentDateBackground);
      }
    }
    if (this.selectedDatePopOver.name == 'Select Date') {
      if (this.dates) {
        // var d1 = new Date(this.endDate);
        for (var i = 0; i < this.daysInThisMonth.length; i++) {
          for(var j=0;j<this.dates.length;j++){
            if (this.daysInThisMonth[i] == this.dates[j]){
              this.showCurrentDateBackground[i] = true;
              this.showOk=false;
          }
          }
        }
        console.log(this.showCurrentDateBackground);
      }
    }
  }
  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
  }
  getThisMonthDate(day, month, year, idx) {
    // if(day <10 ){
    //   day = '0'+day;
    // }

    if(this.blockpastdates[idx] === false){
      console.log(this.daysInThisMonth)
      var month1: any;
      for (var i = 0; i < this.monthNames.length; i++) {
        if (this.monthNames[i] == month) {
          month1 = i + 1;
  
        }
      }
      if (this.selectedDatePopOver.name == 'Start Date' || this.selectedDatePopOver.name == 'End Date') {
        for (var i = 0; i < this.daysInThisMonth.length; i++) {
          if (i == idx) {
            if (this.showCurrentDateBackground[i] == false) {
              this.showCurrentDateBackground[i] = true;
              for (var j = 0; j < this.monthNames.length; j++) {
                if (this.monthNames[j] == month) {
                  month1 = j + 1;
                  // if(month1<10){
                  //   month1 = '0'+month1;
                  // }
                  // if(this.daysInThisMonth[i]<10){
                  //   this.daysInThisMonth[i] = '0'+this.daysInThisMonth[i];
                  // }
                  this.selectedDate =this.checkZero(day)+ "-" +this.checkZero(month1)+ "-" + year;
                  this.startDateFormate= year + "-" + this.checkZero(month1) + "-" + this.checkZero(day)
                }
              }
              this.showOk = false;
            }
            else {
              this.showCurrentDateBackground[i] = false;
              this.selectedDate = '';
              this.showOk = true;
            }
  
          }
          else {
            this.showCurrentDateBackground[i] = false;
          }
        }
      }
      else if (this.selectedDatePopOver.name == 'Select Date(s)') {
        if (this.showCurrentDateBackground[idx] == true) {
          this.showCurrentDateBackground[idx] = false;
          this.showOk = true;
          let date = day + month + year
          let index = this.selectedDate.indexOf(date);
          this.selectedDate.splice(index,1);
        }
        else {
          this.showCurrentDateBackground[idx] = true;
          this.showOk = false;
          this.selectedDate.push(day + month + year);
        }
        if (this.selectedDate.length >= 1) {
          this.showOk = false;
        }
      }
      else if (this.selectedDatePopOver.name == 'Select Date') {
        for (var i = 0; i < this.daysInThisMonth.length; i++) {
          if (i == idx) {
            if (this.showCurrentDateBackground[i] == false) {
              this.showCurrentDateBackground[i] = true;
              for (var j = 0; j < this.monthNames.length; j++) {
                if (this.monthNames[j] == month) {
                  month1 = j + 1;
                  this.rand_date = day+ "-" +month1+ "-" + year;
                }
              }
              this.showOk = false;
            }
            else {
              this.showCurrentDateBackground[i] = false;
              this.rand_date = undefined;
              this.showOk = true;
            }
          }
          else {
            this.showCurrentDateBackground[i] = false;
          }
        }
      }
      if (this.selectedDatePopOver.name == 'Start Date') {
        if (this.selectedDate) {
          this.todayDate = this.selectedDate;
        }
        else {
          this.todayDate = this.presentDate;
        }
      }
      else if (this.selectedDatePopOver.name == 'End Date') {
        if (this.selectedDate) {
          this.endDate = this.selectedDate;
  
        }
        else {
          this.endDate = this.dateAfterOneYear
        }
  
      }
      console.log(this.selectedDate);
    }
  }
  checkZero(number){
    return  number<10?'0'+number:number
  }
  openDatePopover(val) {
    this.selectedDateType = val;
    if (val == 'startDate') {
      this.showDate = true;
      this.StartOrEndDate = 'Select Start Date';
      this.getDaysOfMonth();
    }
    else if (val == 'endDate') {
      this.showDate = true;
      this.StartOrEndDate = 'Select End Date';
      this.getDaysOfMonth();
    }
  }
  popOverDateOk() {
    var val: any;
    console.log(this.selectedDate);
    if (this.selectedDatePopOver.name == 'Start Date') {

      val = { 'startDate': this.todayDate, 'name': 'Start Date','dateFormate':this.startDateFormate };
    }
    if (this.selectedDatePopOver.name == 'End Date') {

      val = {  'endDate': this.endDate, 'name': 'End Date','dateFormate':this.startDateFormate };
    }
    if (this.selectedDatePopOver.name =='Select Date(s)') {
      //this.endDate = this.selectedDate;
      this.selectedDate = [];
      var month1: any;
      for (var i = 0; i < this.daysInThisMonth.length; i++) {
        if (this.showCurrentDateBackground[i] == true) {
          for (var j = 0; j < this.monthNames.length; j++) {
            if (this.monthNames[j] == this.currentMonth) {
              month1 = j + 1;
              if(month1<10){
                month1 = '0'+month1;
              }
              // if(this.daysInThisMonth[i]<10){
              //   this.daysInThisMonth[i] = '0'+this.daysInThisMonth[i];
              // }
              this.selectedDate.push(this.currentYear + "-" + this.checkZero(month1) + "-" + this.checkZero(this.daysInThisMonth[i]));
            }
          }
        }
      }
      console.log(this.selectedDate);
      val = { 'name': 'date', 'date': this.selectedDate };
    }
    if (this.selectedDatePopOver.name =='Select Date') {
      val = { name:'Select Date', date:this.rand_date}
    }
    this.popoverController.dismiss(val);
  }
  popOverDateCancel() {
    this.popoverController.dismiss();
  }

}
