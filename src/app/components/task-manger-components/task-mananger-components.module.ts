import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlockSelectionComponent } from './block-selection/block-selection.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { FrequencySelectionComponent } from './frequency-selection/frequency-selection.component';
import { SelectPlotsBlockComponent } from './select-plots-block/select-plots-block.component';
import { SelectSourcePopOverComponent } from './select-source-pop-over/select-source-pop-over.component';
import { SelectDateComponent } from './select-date/select-date.component';
import { TaskListComponent } from './task-list/task-list.component';
import { FromAndToDateComponent } from './from-and-to-date/from-and-to-date.component';
import { TaskPlannerComponent } from './task-planner/task-planner.component';
import { TaskViewComponent } from './task-view/task-view.component';
import { TaskUpdateHistoryComponent } from './task-update-history/task-update-history.component';
import { CreateTaskLabourComponent } from './create-task-labour/create-task-labour.component';
import { PopupTaskPlannerDeleteComponent } from './popup-task-planner-delete/popup-task-planner-delete.component'
import { PopupTaskPlannerUpdateComponent } from './popup-task-planner-update/popup-task-planner-update.component';
import { PopupTaskPlannerFilterComponent } from './popup-task-planner-filter/popup-task-planner-filter.component'
 import{HarvestcomponentsModule} from '../harvestcomponents/harvestcomponents.module';
import { NgCalendarModule } from 'ionic2-calendar';



@NgModule({
  declarations: [BlockSelectionComponent,
    FrequencySelectionComponent,
    SelectPlotsBlockComponent,
    SelectSourcePopOverComponent,
    SelectDateComponent,
    TaskListComponent,
    FromAndToDateComponent,
    TaskPlannerComponent,
    TaskViewComponent,
    TaskUpdateHistoryComponent,
    CreateTaskLabourComponent,
    PopupTaskPlannerDeleteComponent,PopupTaskPlannerUpdateComponent, PopupTaskPlannerFilterComponent
    ],
    entryComponents:[],
  imports: [
    CommonModule,HarvestcomponentsModule,NgCalendarModule, FormsModule, IonicModule.forRoot()
  ],
  exports: [BlockSelectionComponent, FrequencySelectionComponent, SelectPlotsBlockComponent,
     SelectSourcePopOverComponent, SelectDateComponent, TaskListComponent,
    FromAndToDateComponent, TaskPlannerComponent, TaskViewComponent, TaskUpdateHistoryComponent,
    CreateTaskLabourComponent,PopupTaskPlannerDeleteComponent,PopupTaskPlannerUpdateComponent,PopupTaskPlannerFilterComponent
  ],
  
})
export class TaskManagerComponentsModule { }
