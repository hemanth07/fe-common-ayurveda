import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskUpdateHistoryComponent } from './task-update-history.component';

describe('TaskUpdateHistoryComponent', () => {
  let component: TaskUpdateHistoryComponent;
  let fixture: ComponentFixture<TaskUpdateHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskUpdateHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskUpdateHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
