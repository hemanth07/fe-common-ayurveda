import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as firebase from 'firebase';
import {SimpleGlobal} from 'ng2-simple-global';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';
import { FarmeasyTranslate } from 'src/app/translate.service';
@Component({
	selector: 'app-task-update-history',
	templateUrl: './task-update-history.component.html',
	styleUrls: ['./task-update-history.component.scss']
})
export class TaskUpdateHistoryComponent implements OnInit {
	@Input('taskData') taskData;
	updateHistoryData: any = [];
	labels: any;
	constructor(
		private sg: SimpleGlobal,
		private en: EnglishService,
		private ta:TamilService,
		private te:TeluguService,
		private hi:HindiService,
		private kn:KanadaService,
		private translate:FarmeasyTranslate
	) { }

	ngOnInit() {
		this.translation();
		firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable').orderByChild('task_id').equalTo(this.taskData.key).on('value', resp => {
			this.updateHistoryData = snapshotToArray(resp);
			this.translating(this.updateHistoryData);
			console.log(this.updateHistoryData);

			// var data = [];
			// data = snapshotToArray(resp);
			// console.log(data);
			// if (data.length > 0) {
			// 	console.log(data);
			// 	this.updateHistoryData = data.filter(
			// 		m => m.task_id == this.taskData.key
			// 	);
			// 	console.log(this.updateHistoryData);
			// }
		});
	}
	translation(){
		if(this.sg['userdata'].primary_language === 'en'){
		  this.labels  = this.en.getTaskUpdateHistoryLabels();
		  } else if(this.sg['userdata'].primary_language === 'te'){
			this.labels  = this.te.getTaskUpdateHistoryLabels();
		  } else if(this.sg['userdata'].primary_language === 'ta'){
			this.labels  = this.ta.getTaskUpdateHistoryLabels();
		  } else if(this.sg['userdata'].primary_language === 'kn'){
			this.labels  = this.kn.getTaskUpdateHistoryLabels();
		  } else if(this.sg['userdata'].primary_language === 'hi'){
			this.labels  = this.hi.getTaskUpdateHistoryLabels();
		  }
	  }

	  translating(data) {
		let temp = data
		let menu = [];
		let status;

		for (let item of data) {
			if(item.status.name == "Acknowledge"){
				status = "Task assigned to "+item.assignee.name
			} else {
				status = item.status.name
			}
		  let obj = {
			status: status,
		  }
		  menu.push(obj)
		}
		for (let i = 0; i < menu.length; i++) {
		  if (this.sg['userdata'].primary_language !== 'en') {
			this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
			  this.updateHistoryData[i]['status']['name'] = data['status'];
			});
		  } else {
			this.updateHistoryData[i]['status']['name'] = menu[i]['status'];
		  }
		}
	  }
	
	covert12HoursFormate(events) {
		var month: any;
		var date_format = '12';
		//	for(var i=0;i<events.length;i++){
		var dateTime = events;
		var result: any;
		var hour: any;
		var minutes: any;
		var sec: any;
		var d = new Date(dateTime);

		hour = d.getHours();
		minutes = d.getMinutes();
		sec = d.getSeconds();
		result = hour;
		var ext = '';

		if (date_format == '12') {
			if (hour > 12) {
				ext = 'pm';
				hour = (hour - 12);

				if (hour < 10) {
					result = "0" + hour;
				} else if (hour == 12) {
					hour = "00";
					ext = 'am';
				}
			}
			else if (hour < 12) {
				result = ((hour < 10) ? "0" + hour : hour);
				ext = 'am';
			} else if (hour == 12) {
				ext = 'pm';
			}
		}

		if (minutes < 10) {
			minutes = "0" + minutes;
		}

		var time = result + ":" + minutes + ":" + sec + ' ' + ext;
		var months = [];
		var weekday = [];
		if(this.sg['userdata'].primary_language === 'en'){
				months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
				weekday = ["Sunday", "Monday", "Tuesday", "Wednesday","Thursday", "Friday", "Saturday"]
			} else if(this.sg['userdata'].primary_language === 'te'){
				months = ["జనవరి", "ఫిబ్రవరి", "మార్", "ఏప్రిల్", "మే", "జూన్", "జూలై", "ఆగస్టు", "సెప్టెంబర్", "అక్టోబర్", "నవంబర్", "డిసెంబర్"];
				weekday = ["ఆదివారం","సోమవారం", "మంగళవారం","బుధవారం","గురువారం","శుక్రవారం","శనివారం"]
			} else if(this.sg['userdata'].primary_language === 'ta'){
				months = ["ஜன", "பிப்ரவரி", "மார்", "ஏப்ரல்", "மே", "ஜூன்", "ஜூலை", "ஆகஸ்ட்", "செப்டம்பர்", "அக்", "நவம்பர்", "டிசம்பர்"];
				weekday = ["ஞாயிறு","திங்கள்","செவ்வாய்","புதன்","வியாழன்","வெள்ளி","சனி"]
			} else if(this.sg['userdata'].primary_language === 'kn'){
				months = ["ಜನವರಿ", "ಫೆಬ್ರವರಿ", "ಮಾರ್ಚ್", "ಎಪ್ರಿಲ್", "ಮೇ", "ಜೂನ್", "ಜುಲೈ", "ಆಗಸ್ಟ್", "ಸೆಪ್ಟೆಂಬರ್", "ಅಕ್ಟೋಬರ್", "ನವೆಂಬರ್", "ಡಿಸೆಂಬರ್"];
				weekday = ["ಭಾನುವಾರ","ಸೋಮವಾರ","ಮಂಗಳವಾರ"," ಬುಧವಾರ ","ಗುರುವಾರ"," ಶುಕ್ರವಾರ"," ಶನಿವಾರ"]
			} else if(this.sg['userdata'].primary_language === 'hi'){
				months = ["जन", "फ़रवरी", "मार्च", "अप्रैल", "मई", "जून", "जुलाई", "अगस्त", "सेप्ट", "अक्टूबर", "नोव", "दिसंबर"];
				weekday = ["रविवार"," सोमवार"," मंगलवार"," बुधवार"," गुरुवार"," शुक्रवार"," शनिवार"]
			}

		month = months[d.getMonth()];
		var da = d.getDate();
		var yr = d.getFullYear();
		var date = da + "-" + month + "-" + yr;
		var n = weekday[d.getDay()];
		//}

		return n + "," + " " + date + " " + time;
	}


}
export const snapshotToArray = snapshot => {
	let returnArr = [];

	snapshot.forEach(childSnapshot => {
		let item = childSnapshot.val();
		item.key = childSnapshot.key;
		returnArr.push(item);
	});

	return returnArr;
};
