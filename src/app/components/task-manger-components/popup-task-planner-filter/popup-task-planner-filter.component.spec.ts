import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupTaskPlannerFilterComponent } from './popup-task-planner-filter.component';

describe('PopupTaskPlannerFilterComponent', () => {
  let component: PopupTaskPlannerFilterComponent;
  let fixture: ComponentFixture<PopupTaskPlannerFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupTaskPlannerFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupTaskPlannerFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
