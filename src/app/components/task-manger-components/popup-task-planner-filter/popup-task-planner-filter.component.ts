import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {NavParams,NavController, PopoverController,AlertController,LoadingController} from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import * as firebase from 'firebase';
import { SimpleGlobal } from "ng2-simple-global";
import { CalendercomponentComponent } from '../../harvestcomponents/calendercomponent/calendercomponent.component'
import { FarmeasyTranslate } from 'src/app/translate.service';
@Component({
  selector: 'app-popup-task-planner-filter',
  templateUrl: './popup-task-planner-filter.component.html',
  styleUrls: ['./popup-task-planner-filter.component.scss']
})
export class PopupTaskPlannerFilterComponent implements OnInit {
  inputData:any
  fromOrTodate:any;
  toDate:any;
  fromDate:any;
  blockName:any;
  labourName:any;
  statusValue:any;
  categoryValue:any;
  typeValue:any;
  labels:any= {};
  constructor(
    private popoverController: PopoverController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingController:LoadingController,
    private translate: FarmeasyTranslate,
    public sg:SimpleGlobal) { }

  ngOnInit() {
    this.inputData = this.navParams.get('data');
    console.log(this.inputData);
    if(this.inputData){
      this.translating(this.inputData);
    }
    if(this.inputData.selected){
      this.blockName = this.inputData.selected.block;
      this.categoryValue = this.inputData.selected.category;
      this.labourName = this.inputData.selected.labour;
      this.statusValue = this.inputData.selected.status;
      this.fromDate = this.inputData.selected.fromDate?this.inputData.selected.fromDate:"DD-Mon-YY";
      this.toDate =  this.inputData.selected.toDate?this.inputData.selected.toDate:"DD-Mon-YY";
    }
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels['title'] = "Specify Filter Criteria";
      this.labels['employee'] = "Labour";
      this.labels['labour_place'] = "Select Labour";
      this.labels['block'] = "Block";
      this.labels['block_place'] = "Select Block"
      this.labels['status'] = "Status";
      this.labels['status_place'] = "Select Status"
      this.labels['category'] = "Category";
      this.labels['category_place'] = "Select Category"
      this.labels['from'] = "From"
      this.labels['to'] = "To"
      this.labels['filter'] = "Filter"
      this.labels['clear'] = "Clear all Filters"
      this.labels['open'] = "Open"
      this.labels['close'] = "Closed"
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.labels['title'] = "ఫిల్టర్ ప్రమాణాలను పేర్కొనండి";
      this.labels['labour'] = "కార్మిక";
      this.labels['labour_place'] = "శ్రమను ఎంచుకోండి";
      this.labels['block'] = "బ్లాక్";
      this.labels['block_place'] = "బ్లాక్ ఎంచుకోండి"
      this.labels['status'] = "స్థితి";
      this.labels['status_place'] = "స్థితిని ఎంచుకోండి"
      this.labels['category'] = "వర్గం";
      this.labels['category_place'] = "వర్గం ఎంచుకోండి"
      this.labels['from'] = "నుండి"
      this.labels['to'] = "టు"
      this.labels['filter'] = "వడపోత"
      this.labels['clear'] = "అన్ని ఫిల్టర్‌లను క్లియర్ చేయండి"
      this.labels['open'] = "ఓపెన్"
      this.labels['close'] = "క్లోజ్డ్"
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.labels['title'] = "வடிகட்டி அளவுகோல்களைக் குறிப்பிடவும்";
      this.labels['labour'] = "தொழிலாளர்";
      this.labels['labour_place'] = "உழைப்பைத் தேர்ந்தெடுக்கவும்";
      this.labels['block'] = "பிளாக்";
      this.labels['block_place'] = "தடுப்பு என்பதைத் தேர்ந்தெடுக்கவும்"
      this.labels['status'] = "நிலைமை";
      this.labels['status_place'] = "நிலையைத் தேர்ந்தெடுக்கவும்"
      this.labels['category'] = "வகை";
      this.labels['category_place'] = "பிரிவை தேர்வு செய்க"
      this.labels['from'] = "இருந்து"
      this.labels['to'] = "செய்ய"
      this.labels['filter'] = "வடிகட்டி"
      this.labels['clear'] = "எல்லா வடிப்பான்களையும் அழிக்கவும்"
      this.labels['open'] = "திறந்த"
      this.labels['close'] = "மூடப்பட்ட"
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.labels['title'] = "ಫಿಲ್ಟರ್ ಮಾನದಂಡವನ್ನು ನಿರ್ದಿಷ್ಟಪಡಿಸಿ";
      this.labels['labour'] = "ಕಾರ್ಮಿಕ";
      this.labels['labour_place'] = "ಕಾರ್ಮಿಕ ಆಯ್ಕೆಮಾಡಿ";
      this.labels['block'] = "ನಿರ್ಬಂಧಿಸಿ";
      this.labels['block_place'] = "ಬ್ಲಾಕ್ ಆಯ್ಕೆಮಾಡಿ"
      this.labels['status'] = "ಸ್ಥಿತಿ";
      this.labels['status_place'] = "ಸ್ಥಿತಿ ಆಯ್ಕೆಮಾಡಿ"
      this.labels['category'] = "ವರ್ಗ";
      this.labels['category_place'] = "ವರ್ಗವನ್ನು ಆಯ್ಕೆಮಾಡಿ"
      this.labels['from'] = "ಇಂದ"
      this.labels['to'] = "ಗೆ"
      this.labels['filter'] = "ಫಿಲ್ಟರ್ ಮಾಡಿ"
      this.labels['clear'] = "ಎಲ್ಲಾ ಫಿಲ್ಟರ್‌ಗಳನ್ನು ತೆರವುಗೊಳಿಸಿ"
      this.labels['open'] = "ತೆರೆಯಿರಿ"
      this.labels['close'] = "ಮುಚ್ಚಲಾಗಿದೆ"
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.labels['title'] = "फ़िल्टर मानदंड निर्दिष्ट करें";
      this.labels['labour'] = "श्रम";
      this.labels['labour_place'] = "श्रम का चयन करें";
      this.labels['block'] = "खंड";
      this.labels['block_place'] = "ब्लॉक का चयन करें"
      this.labels['status'] = "स्थिति";
      this.labels['status_place'] = "स्थिति का चयन करें"
      this.labels['category'] = "वर्ग";
      this.labels['category_place'] = "श्रेणी का चयन करें"
      this.labels['from'] = "से"
      this.labels['to'] = "सेवा"
      this.labels['filter'] = "फ़िल्टर"
      this.labels['clear'] = "सभी फ़िल्टर साफ़ करें"
      this.labels['open'] = "खुला"
      this.labels['close'] = "बन्द है"
    }
    console.log(this.labels);
    
  }
  translating(data) {
    let temp = data
    let labours = [];
    let category = [];
    for (let item of this.inputData.users) {
      let obj = {
        name_label: item.name,
      }
      labours.push(obj)
    }
    for (let i = 0; i < labours.length; i++) {
      if (this.sg['userdata'].primary_language !== 'en') {
        this.translate.translateObject(labours[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.inputData.users[i]['name_label'] = data['name_label'];
        });
      } else {
        this.inputData.users[i]['name_label'] = labours[i]['name_label'];

      }
    }
    for (let item of this.inputData.categories) {
      let obj = {
        name_label: item.name,
      }
      category.push(obj)
    }
    for (let i = 0; i < category.length; i++) {
      if (this.sg['userdata'].primary_language !== 'en') {
        this.translate.translateObject(category[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.inputData.categories[i]['name_label'] = data['name_label'];
        });
      } else {
        this.inputData.categories[i]['name_label'] = category[i]['name_label'];

      }
    }
  }
  getblock(event){
    this.blockName = event.detail.value;
  }
  user(event){
    this.labourName = event.detail.value;
  }
  getstatus(event){
    this.statusValue = event.detail.value;
  }
  getType(event){
    this.typeValue = event.detail.value;
  }
  getCategory(event){
    this.categoryValue = event.detail.value;
  }
  CapitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
  async openDate(val) {
    this.fromOrTodate = val;
    let popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popover_class",
      componentProps: {'previous': true},
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getSelectedDate(detail.data);
    });
    await popover.present();
  }
  getSelectedDate(val) {
    console.log(this.fromOrTodate);
    if (this.fromOrTodate == 'toDate') {
      this.toDate = val;
      console.log(this.toDate);
    }
    if (this.fromOrTodate == 'fromDate') {
      this.fromDate = val;
      console.log(this.fromDate);
    }
  }
    dateConverter(date){
    if(date){
      let d = new Date(date);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      if(isNaN(day)){
        return "DD-Mon-YYYY";
      } else {
      return day+"-"+months[month]+"-"+year;
      }
    } else {
      return "DD-Mon-YYYY";
    }
  }
  closePopup(){
    this.popoverController.dismiss();
  }
  filter(){
    let data = {
                labour:this.labourName?this.labourName:null,
                block:this.blockName?this.blockName:null,
                status:this.statusValue?this.statusValue:null,
                category:this.categoryValue?this.categoryValue.toLowerCase():null,
                fromDate:this.fromDate? new Date(this.fromDate).getTime() : null,
                toDate: this.toDate ? new Date(this.toDate).getTime() : null,
                }
    this.popoverController.dismiss(data);
  }
  clearFilter(){
    this.popoverController.dismiss('clear');
  }
}
