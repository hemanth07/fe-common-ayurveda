import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavController, AlertController,ModalController, LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';

declare var window:any ;
@Component({
  selector: 'farmeasy-task-planner',
  templateUrl: './task-planner.component.html',
  styleUrls: ['./task-planner.component.scss']
})
export class TaskPlannerComponent implements OnInit {
  @Output() totalTasksList: EventEmitter<any> = new EventEmitter();
  @Input('tasksFilterByDate') tasksFilterByDate;
  @Output() cleanFilters: EventEmitter<any> = new EventEmitter();
  @Output() selectAllTasks: EventEmitter<any> = new EventEmitter();
  @Output() selectedBlock: EventEmitter<any> = new EventEmitter();
  @Output() selectedCategory: EventEmitter<any> = new EventEmitter();
  @Output() selectedUser: EventEmitter<any> = new EventEmitter();
  @Output() selectedStatus: EventEmitter<any> = new EventEmitter();
  @Input('users') users;
  @Input('blocks') blocks;
  @Input('categories') categories;
  // tasksData: any = [];
  // isTaskData: boolean = false;
  showCompleted: any = [];
  showAcknowledged: any = [];
  clear:boolean = false;
  isLaboursList:boolean=false;
  totalTasksData: any = [];
  length: any = 0;
  dataIsThere:boolean=false;
  noData:boolean=true;
  labels: any;
  all:any;labourName:any;
  block:any;
  category:any;
  labour:any;
  status:any;
  constructor(public alertController: AlertController,
    public router: Router,public modelController: ModalController,
    private sg: SimpleGlobal,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.labourName="Labour";
    // this.intitializeTasksData();
    this.translation();
    // console.log(this.tasksData);

    // firebase.database().ref('lovs/blocks').once('value', resp => {
    //   var data = [];
    //   data = snapshotToArray(resp);
    //   this.blocks = data;
    // });
    // firebase.database().ref('lovs/categories').once('value', resp => {
    //   var data = [];
    //   data = snapshotToArray(resp);
    //   this.categories = data;
    // });
    // firebase.database().ref('users/').once('value', resp => {
    //   var data = [];
    //   data = snapshotToArray(resp);
    //   data.filter(data => {
    //     if (data.role == 'labour')
    //       this.users.push(data);

    //   })
    // });

  }
  selectName(name){
    this.isLaboursList=false;
    this.labourName = name.name
    let temp = { "name": name.name, "phone": name.phone, "profile_url": name.profile_url, "role": name.role,
    'device_id':name.device_id, 'primary_language':name.primary_language}
    console.log(temp)
    this.selectedUser.emit(this.labourName)
    // this.selectedLabour.emit(temp);
    console.log(temp)
  }

  selectAll(data){
    this.selectAllTasks.emit(data.detail.checked);
  }
  clearData(){
    // this.clear = !this.clear;
    this.block = null;
    this.category = null;
    this.labour = null;
    this.status = null;
    this.labourName = 'Labour'
    this.cleanFilters.emit(true);
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
        this.labels  = this.en.getTaskPlannerLabels();
      } else if(this.sg['userdata'].primary_language === 'te'){
        this.labels  = this.te.getTaskPlannerLabels();
      } else if(this.sg['userdata'].primary_language === 'ta'){
        this.labels  = this.ta.getTaskPlannerLabels();
      } else if(this.sg['userdata'].primary_language === 'kn'){
        this.labels  = this.kn.getTaskPlannerLabels();
      } else if(this.sg['userdata'].primary_language === 'hi'){
        this.labels  = this.hi.getTaskPlannerLabels();
      }
  }
  getBlock(val) {
    console.log(val);
    this.selectedBlock.emit(val.detail.value);

  }
  getCategory(val) {
    console.log(val.detail.value);
    // this.selectedCategory = val.detail.value;
    this.selectedCategory.emit(val.detail.value)

  }
  getUser(val) {
    console.log(val.detail.value);

    this.selectedUser.emit(val.detail.value)
  }
  getStatus(val) {
    console.log(val.detail.value);
    if (val.detail.value == 'Open') {
      this.selectedStatus.emit('Open')
    }
    if (val.detail.value == 'WIP') {
      this.selectedStatus.emit('Acknowledged')
    }
    if (val.detail.value == 'Closed') {
      this.selectedStatus.emit('Closed')
    }
  }
  showLaboursList(){
    //this.isLaboursList=true;
    document.getElementById("interestOverlay").style.display = 'block';
   // document.getElementById("interestOverlay").style.display = 'none';  
  }
  off() {
    document.getElementById("interestOverlay").style.display = "none";
  }
//   ngOnInit() {
//     this.viewCtrl.didEnter.subscribe(() => {
//        console.log('Component active');
//     });
//  }
//  async intitializeTasksData() {
//     this.tasksData = [];
//     let that = this;
//       const loading = await this.loadingController.create({
//       spinner: 'bubbles',
//       message: 'Please wait...',
//       translucent: true,
//       cssClass: 'custom-class custom-loading'
//     });
//     loading.present();
//     firebase.database().ref('TaskManager/Task_table/').on('value', resp => {
//       this.totalTasksData = [];
//       this.tasksData = [];
//       var data = [];
//       data = snapshotToArray(resp);
//       if (data.length > 0) {
//         this.isTaskData = true;
//         console.log(data);
//         data.filter(data => {
//           if (data.task_type) {
//             this.tasksData.push(data);
//             this.showCompleted = false;
//             this.showAcknowledged = false;
//           }
//         })
//         if (this.tasksData.length > 0) {
//           this.tasksData.reverse();
//           that.initializeData();
//         }
//       }
//       loading.dismiss();
//     });
//   }
 
  // initializeData() {
  //   for (var i = 0; i < this.tasksData.length; i++) {
  //     if (i < 15) {
  //       this.totalTasksData.push(this.tasksData[i]);
  //       this.length = this.totalTasksData.length;
  //     }
  //   }
  //   if(this.tasksData.length>15){
  //     this.dataIsThere=false;
  //   }
  //   else{
  //     this.dataIsThere=true;
  //   }
  //   console.log(this.totalTasksData);
  //   this.totalTasksList.emit(this.totalTasksData);
  // }
  // loadMore() {
  //   if (this.tasksData.length > 15) {
  //     if (this.length < this.tasksData.length) {
  //       this.totalTasksData = [];
  //       for (var i = this.length; i < this.tasksData.length; i++) {
  //         if (i < this.length + 15) {
  //           this.totalTasksData.push(this.tasksData[i]);
  //         }
  //         else {
  //           break;
  //         }
  //       }
  //       this.length = this.length + this.totalTasksData.length;
  //     }
  //     else {
  //       //alert('no data');
  //     }
      
  //   }
  //   else {
  //     //alert('no data');
  //   }
  //   if( this.length>=this.tasksData.length){
  //     this.dataIsThere=true;
  //     this.noData=false;
  //   }
  //   else{
  //     this.dataIsThere=false;
  //     this.noData=false;
  //   }
  //   if(this.length <= 0){
  //     this.noData=true;
  //   }
  // }
  // loadPreviousData() {
  //   this.length = this.length - this.totalTasksData.length;

  //  // if (this.length > 0) {
  //     if (this.length - 15 > 0) {
  //       this.totalTasksData = [];
  //       for (var i = (this.length - 15); i < this.length; i++) {
  //         this.totalTasksData.push(this.tasksData[i]);
  //       }
  //       this.noData=false;
  //       this.dataIsThere=false;
  //     }
  //     else {
  //       this.totalTasksData = [];
  //       for (var i = 0; i < this.length; i++) {
  //         this.totalTasksData.push(this.tasksData[i]);
  //       }
  //       this.length = 15;
  //     this.dataIsThere=false;
  //     this.noData=true;
  //     }
  //    // this.noData=false;
  //   // }
  //   // else {
  //   //   this.length = 15;
  //   //   this.dataIsThere=false;
  //   //   this.noData=true;
  //   //   alert('no data');
  //   // }
  //   if( this.length>=this.tasksData.length){
  //     this.dataIsThere=true;
      
  //   }
  //  }


  // filterData() {

  //   if (this.selectedCategory) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.category && role.category.name == this.selectedCategory);
  //   }
  //   if (this.selectedStatus) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus);
  //   }
  //   if (this.selectedBlock) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //   }
  //   if (this.selectedUser) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.assignee.name == this.selectedUser);
  //   }
  //   if (this.selectedBlock && this.selectedCategory) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.category && role.category.name == this.selectedCategory && role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //   }
  //   if (this.selectedBlock && this.selectedStatus) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus && role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //   }
  //   if (this.selectedBlock && this.selectedUser) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.assignee.name == this.selectedUser && role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //   }
  //   if (this.selectedStatus && this.selectedCategory) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus && role.category && role.category.name == this.selectedCategory);
  //   }
  //   if (this.selectedUser && this.selectedCategory) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.assignee.name == this.selectedUser && role.category && role.category.name == this.selectedCategory);
  //   }
  //   if (this.selectedStatus && this.selectedUser) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus && role.category && role.category.name == this.selectedUser);
  //   }
  //   if (this.selectedStatus && this.selectedCategory && this.selectedBlock) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus && role.category && role.category.name == this.selectedCategory && role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //     console.log(this.tasksData);
  //   }
  //   if (this.selectedStatus && this.selectedCategory && this.selectedUser) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus && role.category && role.category.name == this.selectedCategory && role.assignee.name == this.selectedUser);
  //     console.log(this.tasksData);
  //   }
  //   if (this.selectedStatus && this.selectedUser && this.selectedBlock) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus &&  role.assignee && role.assignee.name == this.selectedUser && role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //     console.log(this.tasksData);
  //   }
  //   if (this.selectedStatus && this.selectedCategory && this.selectedUser && this.selectedBlock) {
  //     this.initializeData();
  //     this.totalTasksData = this.tasksData.filter(role => role.status == this.selectedStatus && role.category && role.category.name == this.selectedCategory && role.assignee.name == this.selectedUser && role.blocks.find(group => this.selectedBlock.includes(group.name)));
  //     console.log(this.tasksData);
  //   }
  //   this.totalTasksList.emit(this.totalTasksData);
  // }
  // async deleteSelectedData(val) {
  //   const alert = await this.alertController.create({
  //     header: 'Confirm!',
  //     message: 'Are you sure  want to delete',
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         cssClass: 'secondary',
  //         handler: () => {
  //           console.log('Confirm Cancel: blah');
  //         }
  //       }, {
  //         text: 'Okay',
  //         handler: () => {
  //           console.log('Confirm Okay');
  //           var playersRef = firebase.database().ref("TaskManager/Task_table/" + val.key).remove();
  //           // this.intitializeTasksData();
  //           console.log(playersRef);
  //         }
  //       }
  //     ]
  //   });
  //   await alert.present();
  // }
  // editSelectedData(val) {
  //   console.log(val);
  //   let temp = { 'data': JSON.stringify(val) };
  //   console.log(temp);
  //   if (val.task_type == 'Laboures') {
  //     this.router.navigate(['/create-task-labour', temp]);
  //   }
  //   if (val.task_type == 'Date Wise') {
  //     this.router.navigate(['/create-task-by-date', temp]);
  //   }
  //   if (val.task_type == 'Block') {
  //     this.router.navigate(['/create-task-by-block', temp]);
  //   }
  //   if (val.task_type == 'Categories') {
  //     this.router.navigate(['/create-task-category', temp]);
  //   }
  //   if(val.task_type =="Adhoc"){
  //     this.router.navigate(['/create-task-adhoc', temp]);
  //   }
  // }

  // window.onclick = function(event) {
  //   document.getElementById("interestOverlay").style.display = 'none'; 
  // }

    // window.onclick=function(event){
    //   document.getElementById("interestOverlay").style.display = 'none';
    // }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
