import { Firebase } from '@ionic-native/firebase';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {NavParams,NavController, PopoverController,AlertController,LoadingController} from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import * as firebase from 'firebase';
import { SimpleGlobal } from "ng2-simple-global";

@Component({
  selector: 'app-popup-task-planner-delete',
  templateUrl: './popup-task-planner-delete.component.html',
  styleUrls: ['./popup-task-planner-delete.component.scss'],
})
export class PopupTaskPlannerDeleteComponent implements OnInit {
  delteTasksCount:any;
  deleteTasks:any = [];
  labels:any = {};
  constructor(private popoverController: PopoverController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingController:LoadingController,
    public sg:SimpleGlobal) { 
    this.deleteTasks = this.navParams.get('count');
    console.log(this.deleteTasks);
    this.delteTasksCount = this.deleteTasks.length;
  }

  ngOnInit() {
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels['title'] = "Confirmation for Deletion of Tasks";
      this.labels['message'] = " You have chosen "+this.delteTasksCount+" Tasks for deletion. Please confirm if wish to delete the selected tasks?";
      this.labels['ok'] = "ok";
      this.labels['cancel'] = "cancel";
      
    } else if(this.sg['userdata'].primary_language === 'te'){

      this.labels['title'] = "పనుల తొలగింపుకు నిర్ధారణ";
      this.labels['message'] = " మీరు ఎంచుకున్నారు "+this.delteTasksCount+" తొలగించడానికి విధులు. ఎంచుకున్న పనులను తొలగించాలనుకుంటే దయచేసి నిర్ధారించండి?";
      this.labels['ok'] = "అలాగే";
      this.labels['cancel'] = "రద్దు చేయండి";

    } else if(this.sg['userdata'].primary_language === 'ta'){

      this.labels['title'] = "பணிகளை நீக்குவதற்கான உறுதிப்படுத்தல்";
      this.labels['message'] = " நீங்கள் தேர்ந்தெடுத்தீர்கள் "+this.delteTasksCount+" நீக்குவதற்கான பணிகள். தேர்ந்தெடுக்கப்பட்ட பணிகளை நீக்க விரும்பினால் உறுதிப்படுத்தவும்?";
      this.labels['ok'] = "சரி";
      this.labels['cancel'] = "ரத்து";

    } else if(this.sg['userdata'].primary_language === 'kn'){

      this.labels['title'] = "ಕಾರ್ಯಗಳನ್ನು ಅಳಿಸಲು ದೃ ir ೀಕರಣ";
      this.labels['message'] = " ನೀವು ಆಯ್ಕೆ ಮಾಡಿದ್ದೀರಿ "+this.delteTasksCount+" ಅಳಿಸುವ ಕಾರ್ಯಗಳು. ಆಯ್ದ ಕಾರ್ಯಗಳನ್ನು ಅಳಿಸಲು ಬಯಸಿದರೆ ದಯವಿಟ್ಟು ದೃ irm ೀಕರಿಸಿ?";
      this.labels['ok'] = "ಸರಿ";
      this.labels['cancel'] = "ರದ್ದುಮಾಡಿ";

    } else if(this.sg['userdata'].primary_language === 'hi'){

      this.labels['title'] = "टास्क के हटने की पुष्टि";
      this.labels['message'] = "तुमने पसंद किया "+this.delteTasksCount+" हटाने के लिए कार्य। कृपया चयनित कार्यों को हटाने के लिए पुष्टि करें ?";
      this.labels['ok'] = "ठीक";
      this.labels['cancel'] = "रद्द करना";

    }
  }
 blockPopOverCancel(){
    this.popoverController.dismiss('cancel');
  }
  async blockPopOverOk(){
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    for(let key of this.deleteTasks){
      firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table/'+key).remove();
    }
    this.deleteTasks=[];
    loading.dismiss();
    this.popoverController.dismiss('update');
  }
}
