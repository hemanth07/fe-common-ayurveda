import { Component, OnInit, Input, Output, EventEmitter,NgZone } from '@angular/core';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { LoadingController,AlertController , PopoverController } from '@ionic/angular';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import {SimpleGlobal} from 'ng2-simple-global';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';
import { ProgressBarComponent } from './../../progress-bar/progress-bar.component';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {
  @Output() status: EventEmitter<any>  =  new EventEmitter();
  @Output() updateTask: EventEmitter<any>  =  new EventEmitter();
  @Input('taskData') getTask;
  showCompleted:boolean=false;
  showAcknowledged:any=[];
  isTaskData:boolean=false;
  taskData:any;
  taskStatus:any;
  captureImage:any=[];
  attachedImages:any=[];
  labels: any;
  date:any;
  userTask:any;
  statusArr:any = [];
  placeholder:string;
  constructor(private storage: Storage, private file: File,
    private media: Media,private camera: Camera,
    private mediaCapture: MediaCapture,
    public loadingCtrl: LoadingController,
    private imagePicker: ImagePicker,private alertController: AlertController,
    private sg: SimpleGlobal,
 private en: EnglishService,
 private hi: HindiService,
 private ta:TamilService,
 private te:TeluguService,
 private kn:KanadaService,
 public popoverController: PopoverController,
 private zone:NgZone) { }

  ngOnInit() {
    this.translation();
    console.log(this.getTask.key);
    this.taskData = this.task(this.getTask);
    // let that=this;
    
    // firebase.database().ref('TaskManager/Task_table/'+this.getTask.key).on('value', resp => {
    //   var data=[];
    //   // data= snapshotToArray(resp);
    //   data = [...resp.val()];
    //   console.log(data);
    //   if(data.length>0){
    //     // var temp:any;
    //   //  temp=data.filter(role => role.key == this.getTask.key);
    //    this.taskData=this.task(data[0]);
    //   console.log(this.taskData);
    // }
    // });
  }
task(data){
  let today_date = new Date();
  var current_month = today_date.getMonth() + 1;

  let currentDate = today_date.getFullYear() + '-' + current_month + "-" + today_date.getDate();

  var one_day = 1000 * 60 * 60 * 24;
  var date1 = new Date(currentDate);
  var date1_ms = date1.getTime();
  var date2_ms = data.date;
  var difference_ms = date2_ms - date1_ms;
  difference_ms = difference_ms / 1000;
  var seconds = Math.floor(difference_ms % 60);
  difference_ms = difference_ms / 60;
  var minutes = Math.floor(difference_ms % 60);
  difference_ms = difference_ms / 60;
  var hours = Math.floor(difference_ms % 24);
  var days = Math.floor(difference_ms / 24);
  if (days > 0) {
    if (days > 1) {

      data["dueDate"]= 'Due by ' + days + ' Days';
      data['color']="green";
      return data;

    }
    if (days == 1) {

      data["dueDate"]= 'Due tomorrow';
      data['color']="green";
      return data;
    }
  }
  if (days == 0) {

    data["dueDate"]= 'Due today';
    data['color']="green";
    return data;
  }
  if (days < 0) {
    if (days == -1) {
      data['color']="red";
      data["dueDate"]= 'Overdue by ' + Math.abs(days) + ' Day';
      return data;
    }
    data['color']="red";
    data["dueDate"]= 'Overdue by ' + Math.abs(days) + ' Days';
    return data;
  }
}
  daysbetweenDates(date){
    //this.dateColor='';
   
    let today_date = new Date();
    var current_month=today_date.getMonth()+1;
   
    let currentDate=today_date.getFullYear()+'-'+current_month+"-"+today_date.getDate();

    var one_day=1000*60*60*24;
    var date1 = new Date(currentDate);
    var date1_ms = date1.getTime();
    var date2_ms = date;
    var difference_ms = date2_ms - date1_ms;
    difference_ms = difference_ms/1000;
    var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var hours = Math.floor(difference_ms % 24);  
    var days = Math.floor(difference_ms/24);
    if ( days > 0) {
      if(days>1){
        
        return 'Due by '+ days + ' Days';
       
        
      }
      if(days==1){
       
        return 'Due tomorrow';
      }
    }
    if(days==0){
    
      return 'Due today';
    }
    if(days<0){
      if(days==-1){
      
      return 'Overdue by '+ Math.abs(days) + ' Day';
      }
      
      return 'Overdue by '+ Math.abs(days) + ' Days';
    }
  
    // var diffDays =days + ' days, ' + hours + ' hours, ' + minutes + ' minutes, and ' + seconds + ' seconds';
    // document.getElementById("demo").innerHTML = diffDays;
  }
   translation(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels  = this.en.getTaskListLabels();
      this.placeholder = " - select value - "
      this.statusArr = [
        {value:"Task completed",name:"Task completed"},
        {value:"Rain - Task Not complete",name:"Rain - Task Not complete"},
        {value:"Power cut - Task Not complete",name:"Power cut - Task Not complete"},
        {value:"Machinery repair - Task Not Complete",name:"Machinery repair - Task Not Complete"},
      ]
      } else if(this.sg['userdata'].primary_language === 'te'){
      this.placeholder = " - ఎంచుకోండి - "
      this.labels  = this.te.getTaskListLabels();
        this.statusArr = [
          {value:"Task completed",name:"టాస్క్ పూర్తయింది"},
          {value:"Rain - Task Not complete",name:"వర్షం - టాస్క్ పూర్తి కాలేదు"},
          {value:"Power cut - Task Not complete",name:"పవర్ కట్ - టాస్క్ పూర్తి కాలేదు"},
          {value:"Machinery repair - Task Not Complete",name:"యంత్రాల మరమ్మత్తు - పని పూర్తి కాలేదు"},
        ]
      } else if(this.sg['userdata'].primary_language === 'ta'){
      this.placeholder = " - தேர்ந்தெடு - "
      this.labels  = this.ta.getTaskListLabels();
        this.statusArr = [
          {value:"Task completed",name:"பணி முடிந்தது"},
          {value:"Rain - Task Not complete",name:"மழை - பணி முடிக்கப்படவில்லை"},
          {value:"Power cut - Task Not complete",name:"பவர் கட் - பணி முடிக்கப்படவில்லை"},
          {value:"Machinery repair - Task Not Complete",name:"இயந்திர பழுது - பணி முடிக்கப்படவில்லை"},
        ]
      } else if(this.sg['userdata'].primary_language === 'kn'){
      this.placeholder = " - ಆಯ್ಕೆಮಾಡಿ - "
      this.labels  = this.kn.getTaskListLabels();
        this.statusArr = [
          {value:"Task completed",name:"ಕಾರ್ಯ ಪೂರ್ಣಗೊಂಡಿದೆ"},
          {value:"Rain - Task Not complete",name:"ಮಳೆ - ಕಾರ್ಯ ಪೂರ್ಣಗೊಂಡಿಲ್ಲ"},
          {value:"Power cut - Task Not complete",name:"ಪವರ್ ಕಟ್ - ಕಾರ್ಯ ಪೂರ್ಣಗೊಂಡಿಲ್ಲ"},
          {value:"Machinery repair - Task Not Complete",name:"ಯಂತ್ರೋಪಕರಣಗಳ ದುರಸ್ತಿ - ಕಾರ್ಯ ಪೂರ್ಣಗೊಂಡಿಲ್ಲ"},
        ]
      } else if(this.sg['userdata'].primary_language === 'hi'){
      this.placeholder = " - चुनते हैं - "
      this.labels  = this.hi.getTaskListLabels();
        this.statusArr = [
          {value:"Task completed",name:"कार्य संपूर्ण हुआ"},
          {value:"Rain - Task Not complete",name:"वर्षा - कार्य पूर्ण नहीं"},
          {value:"Power cut - Task Not complete",name:"बिजली कटौती - कार्य पूरा नहीं"},
          {value:"Machinery repair - Task Not Complete",name:"मशीनरी मरम्मत - कार्य पूरा नहीं"},
        ]
      }
  }
  colors(date){
    let today_date = new Date();
    var current_month=today_date.getMonth()+1;
   
    let currentDate=today_date.getFullYear()+'-'+current_month+"-"+today_date.getDate();

    var one_day=1000*60*60*24;
    var date1 = new Date(currentDate);
    var date1_ms = date1.getTime();
    var date2_ms = date;
    var difference_ms = date2_ms - date1_ms;
    difference_ms = difference_ms/1000;
    var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var hours = Math.floor(difference_ms % 24);  
    var days = Math.floor(difference_ms/24);
    if ( days > 0) {
      if(days>1){
        //this.coloredImage='./assets/images/green.svg';
        
        return true; 
      }
      if(days==1){
        //this.coloredImage='./assets/images/green.svg';
        return true;
      }
    }
    if(days==0){
      //this.coloredImage='./assets/images/green.svg';
      return true;
    }
    if(days<0){
      //this.coloredImage='./assets/images/red.svg'
      return false;
    }
  }
  coloredImage(date){
  let today_date = new Date();
    var current_month=today_date.getMonth()+1;
   
    let currentDate=today_date.getFullYear()+'-'+current_month+"-"+today_date.getDate();

    var one_day=1000*60*60*24;
    var date1 = new Date(currentDate);
    var date1_ms = date1.getTime();
    var date2_ms = date;
    var difference_ms = date2_ms - date1_ms;
    difference_ms = difference_ms/1000;
    var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60; 
    var hours = Math.floor(difference_ms % 24);  
    var days = Math.floor(difference_ms/24);
    if ( days > 0) {
      if(days>1){
        
        
        return 'https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2FTaskManager%2Fgreen.svg?alt=media&token=f758fc2c-b63d-4844-b246-02d3ccb8f403'; 
      }
      if(days==1){
      
        return 'https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2FTaskManager%2Fgreen.svg?alt=media&token=f758fc2c-b63d-4844-b246-02d3ccb8f403';
      }
    }
    if(days==0){
      
      return 'https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2FTaskManager%2Fgreen.svg?alt=media&token=f758fc2c-b63d-4844-b246-02d3ccb8f403';
    }
    if(days<0){
      
      return 'https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2FTaskManager%2Fred.svg?alt=media&token=2884c0ec-7e58-4e8c-9196-fac7036be14a';
    }
  }
  cameraImage() {
    // const options: CaptureImageOptions = {
    //   limit: 1
    // };
    // this.mediaCapture.captureImage(options)
    //   .then(
    //     (data: MediaFile[]) => {
    //       let that;
    //       that = this;
    //       const index = data[0].fullPath.lastIndexOf('/');
    //       const finalPath = data[0].fullPath.substr(0, index);
    //       this.file.readAsArrayBuffer(finalPath, data[0].name).then(async (file) => {
    //         const blob = new Blob([file], { type: data[0].type });
    //         const loading = await this.loadingCtrl.create({
    //           spinner: 'bubbles',
    //           message: 'Please wait...',
    //           translucent: true,
    //           cssClass: 'custom-class custom-loading'
    //         });
    //         loading.present();
    //         firebase.storage().ref('farmeasy/TaskManager/images/' + data[0].name).put(blob).then(function (snapshot) {
    //           // alert('Uploaded a blob or file!');
    //           firebase.storage().ref('farmeasy/TaskManager/images/' + data[0].name).getDownloadURL().then(function (url) {
    //             loading.dismiss();
    //             alert(url);
    //            let body = { type: 'camera', url: url, path: 'farmeasy/TaskManager/images/' + data[0].name };
    //             this.captureImage.push(body);
    //             //this.attchments.emit(that.image_data);

    //             alert(JSON.stringify(that.captureImage));

    //           });
    //         });
    //       });
    //       console.log(data);
    //     },
    //     (err: CaptureError) => console.error(err)
    //   );


      let  urls;
      let body=[];
      this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const options: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
               this.date = new Date().getTime();
      const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
               let that;
               that = this;
                      let img_url  = 'data:image/jpeg;base64,' + imageData;
                      fetch(img_url)
                      .then(res => res.blob())
                      .then(blob => {
                        urls = {'name' : this.sg['userdata'].vendor_id+'/farmeasy/taskManager/images/' + this.date};
          this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/images/' + this.date);
                        firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/images/' + this.date).put(blob).then(function(snapshot) {
                          setTimeout(() => {
                            firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                             popover.dismiss();
                            //  loading.dismiss();
                             that.captureImage.push({ 'type': 'camera', 'url' : url  , 'path': urls.name });
                             //alert(JSON.stringify(that.captureImage));
                             
                             });
                          }, 1000);
                        });
                            that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + that.date ).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                     });
                    }, (err) => {
                      // Handle error
             alert(err);
        });
  }

  markAsDone(val){
    let status={"name":'Task completed'};
    var data = { 'key': this.getTask.key, 'status':status };
    
    this.showCompleted=true;
    this.status.emit(data);
  }
  markAsAcknowledged(val,idx){
    var data={'key':this.getTask.key,'status':'Acknowledged'};
    this.showAcknowledged[idx]=true;
    this.status.emit(data);
  }
  getTaskStatus(val){
    console.log(val);
    this.taskStatus=val.detail.value;
  }
  // cameraImage(){
  //   this.captureImage='';
  // }
  attachedImage(){
    

    // const proof_address = [];
    // const urls = [];
    // const imageData = '';
    // const images = [];
    // const options = {
    //   quality: 20,
    //   outputType: 1,
    //   maximumImagesCount: 10
    // };
    // this.imagePicker.getPictures(options).then(async results => {
    //   let that;
    //   that = this;
    //   if (results && results.length > 0 && results !== 'OK') {
    //     const loading = await this.loadingCtrl.create({
    //       spinner: 'bubbles',
    //       message: 'Please wait...',
    //       translucent: true,
    //       cssClass: 'custom-class custom-loading'
    //     });
    //     loading.present();
    //     const len = results.length - 1;
    //     for (let i = 0; i < results.length; i++) {
    //       this.date = new Date().getTime();
    //       const img_url = 'data:image/png;base64,' + results[i];
    //       fetch(img_url)
    //         .then(res => res.blob())
    //         .then(blob => {
    //           urls.push({ 'name': 'farmeasy/TaskManager/images/' + this.date + i });
    //           firebase.storage().ref('farmeasy/TaskManager/images/' + this.date + i).put(blob).then(function (snapshot) {
    //             setTimeout(() => {
    //               firebase.storage().ref(urls[i].name).getDownloadURL().then(function (url) {
    //                 // alert(url);
    //                 if (i === len) {
    //                   loading.dismiss();
    //                 }
    //                 const body = { 'type': 'gallery', 'url': url, 'path': urls };
    //                 // that.address.emit(body);
    //                 that.attachedImages.push(body);
    //                 //this.attchments.emit(that.image_data);

    //                 alert(JSON.stringify(that.image_data));
    //               });
    //             }, 1000);
    //           });
    //         });
    //     }
    //   }
    // });



    let  urls;
    const options: CameraOptions = {
             quality: 100,
             destinationType: this.camera.DestinationType.DATA_URL,
             encodingType: this.camera.EncodingType.JPEG,
             mediaType: this.camera.MediaType.PICTURE,
             sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
             }
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    this.camera.getPicture(options).then(async (imageData) => {
               this.date = new Date().getTime();
      const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
               let that;
               that = this;
                      let img_url  = 'data:image/jpeg;base64,' + imageData;
                      fetch(img_url)
                      .then(res => res.blob())
                      .then(blob => {
                        urls = {'name' : this.sg['userdata'].vendor_id+'/farmeasy/taskManager/images/' + this.date};
          this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/images/' + this.date);
                        firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/images/' + this.date).put(blob).then(function(snapshot) {
                          setTimeout(() => {
                            firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                             that.captureImage.push({ 'type': 'camera', 'url' : url  , 'path': urls.name });
                             popover.dismiss();
                             //alert(JSON.stringify(that.captureImage));
                             
                             });
                          }, 1000);
                        });
                            that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id + '/farmeasy/taskManager/images/' + that.date ).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                     });
                    }, (err) => {
                      // Handle error
             alert(err);
        });
  }
  async closeImage(val,idx){
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure want to delete this'+ val +'attachment?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.removeAttachment(val,idx);
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  
  removeAttachment(val,idx){
      let that=this;
      if(val=='camera'){
        for(var i=0;i<this.captureImage.length;i++){
          if(i==idx){
            firebase.storage().ref(that.captureImage[i].path).delete()
            that.captureImage.splice(idx, 1);
          }
        }
        
      }
      if(val=='attachment'){
        //alert(JSON.stringify(this.attachedImages));
        //alert(JSON.stringify(idx));
        for(var i=0;i<this.attachedImages.length;i++){
          if(i==idx){
            firebase.storage().ref(that.attachedImages[i].path.name).delete()
            that.attachedImages.splice(idx, 1);
          }
        }
      
      }
    }
  async submitTaskDetails(){
    if(this.taskStatus != 'null' && this.taskStatus != '' && this.taskStatus){
      let temp={'name':this.taskStatus,'cameraImage':this.captureImage,
      'attachedImage':this.attachedImages}
      if(this.taskStatus=='Task completed'){
        this.showCompleted=true;
      } else {
      this.showCompleted=false;
      }
      //this.taskData.status.name=this.taskStatus
      this.taskData.status=this.taskStatus
     
      this.updateTask.emit({"status":temp,'key':this.taskData.key});
      this.taskStatus='';
      this.captureImage=[];
      this.attachedImages=[];
      this.userTask='null';
    } else {
      const alert = await this.alertController.create({
        header: 'Alert',
        message: 'please select status',
        buttons: ['OK']
      });
  
      await alert.present();
    }
 

  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
