import { FarmeasyTranslate } from './../../../translate.service';
import { Component, OnInit } from '@angular/core';
import { NavParams,NavController,PopoverController,ModalController,AlertController} from '@ionic/angular';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
@Component({
  selector: 'app-select-source-pop-over',
  templateUrl: './select-source-pop-over.component.html',
  styleUrls: ['./select-source-pop-over.component.scss']
})
export class SelectSourcePopOverComponent implements OnInit {
  selectedCategories:any=[];
  checkImage:any=[];
  categories:any=[];
  isSelectedSource:boolean=true;
  selectedSource:any=[];
  labels:any;finalData:any;
  sourceSegment:any;
  usersData:any=[];assignee:any;
  laboursData:any=[];selectedLabour:any=[];
  isSelectedLabour:boolean=true;
  selected_values :any = []
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private popoverController: PopoverController,
    public modalController: ModalController,
    private sg: SimpleGlobal,
    private translate:FarmeasyTranslate,
    private alertController:AlertController) { }

  ngOnInit() {
    this.sourceSegment='source';
    this.selectedCategories = this.navParams.get('category');
    this.usersData = this.navParams.get('labours');
    this.selected_values = this.navParams.get('selected_values');
    console.log(this.selected_values);
    console.log(this.usersData);
    console.log(this.selectedCategories);
    this.translation();
    this.translationMethod(this.selectedCategories);
    this.translationMethodData(this.selectedCategories.data);
    this.initializeData();

    // firebase.database().ref('users/').on('value', resp => {
    //   let data = snapshotToArray(resp);
  
    //   this.usersData = data.filter(data => data.role == 'labour')
  
      if(this.usersData){
        this.getLaboursWithBlockResponseble();
      }
      // if(this.selected_values){
      //   this.prefillData(this.selected_values);
      // }
    // });
   
  }
  initializeData(){
    let sources;
    if(this.selected_values.source){
    sources = this.selected_values.source.data;
    }
    if(this.selectedCategories){
        for(var i=0;i<this.selectedCategories.data.length;i++){
          if(sources){
            for(let source of sources){
              if(this.selectedCategories.data[i]['source_code'] == source.source_code){
                this.selectedCategories.data[i]['isSelected']= true;
                this.checkImage[i]= true;
                this.isSelectedSource=true;
              }
            }
          } else{
            this.checkImage[i]=false;
            this.selectedCategories.data[i]['isSelected']=false;
          }
        }
    }
    
      // if(this.selectedCategories.categoryName=='Cleaning'){
      //   for(var i=0;i<this.selectedCategories.source.length;i++){
      //     this.selectedCategories.source[i]['isSourceSelected']=false;
      //   }
      // }
    // }
  }
  getLaboursWithBlockResponseble() {
    this.laboursData = this.usersData;
    let temp: any = [];
    let farmeasy_id;
    if(this.selected_values.assignee){
      farmeasy_id = this.selected_values.assignee.farmeasy_id;
    }
    for (var i = 0; i < this.laboursData.length; i++) {
      if (this.laboursData[i].block_responsible) {
        for (var j = 0; j < this.laboursData[i].block_responsible.length; j++) {
          if (this.laboursData[i].block_responsible[j] == this.selectedCategories.block) {
            temp.push(this.laboursData[i]);
            if(this.laboursData[i].farmeasy_id == farmeasy_id){
              this.selectedLabour.push(true);
              this.isSelectedLabour = true;
            } else {
              this.selectedLabour.push(false);
            }
          }
        }
      }
    }
    this.usersData = temp;
    this.translationLaboursNames(this.usersData);
  }
  // prefillData(selected_values){
  //   let sources = selected_values.source.data;
  //   let labour_id = selected_values.assignee.farmeasy_id;
  //   for(var i=0;i<this.selectedCategories.data.length;i++){
  //     for(let source of sources){
  //       if(this.selectedCategories.data[i]['source_code'] == source.source_code){
  //         this.selectedCategories.data[i]['isSelected']= true;
  //       }
  //     }
  //   }

  // }
  getWaterSource(idx1,idx2){
    if(this.selectedCategories.categoryName=='Watering'){
      for(var i=0;i<this.selectedCategories.source.length;i++){
        if(idx1==i){
          for(var j=0;j<this.selectedCategories.source[i].source.length;j++){
            if(idx2==j){
              if(this.selectedCategories.source[i].source[j].isSourceSelected==true){
                this.selectedCategories.source[i].source[j].isSourceSelected=false;
              }
              else{
                this.isSelectedSource=false;
                this.selectedCategories.source[i].source[j].isSourceSelected=true;
                this.selectedSource.push(this.selectedCategories.source[i].source[j])
                this.categories={"blockName":this.selectedCategories.name,"plotName":this.selectedCategories.plotName,
                "source":{name:this.selectedCategories.selectedSource,"data":this.selectedSource}};
              
              }
            }
          }
        }
      }
    }
  
  }
  selectName(data,index){
    for(var i=0;i<this.selectedLabour.length;i++){
      if(i==index){
        let device_id:any;
        this.selectedLabour[i]=true;
        if (this.usersData[i].device_id) {
          device_id = this.usersData[i].device_id
        }
        this.assignee = {
          'name': this.usersData[i].name, 'phone': this.usersData[i].phone, 'profile_url': this.usersData[i].profile_url, 'role': this.usersData[i].role,
          'primary_language': this.usersData[i].primary_language, 'device_id': device_id?device_id:null,'farmeasy_id':this.usersData[i].farmeasy_id
        }
        this.isSelectedLabour=false;
      }
      else this.selectedLabour[i]=false;
    }
    if(this.finalData.source){
      this.finalData['labour']=this.assignee;
    }else{
      this.finalData={'blockName':this.selectedCategories.block, 'plotName':this.selectedCategories.plot,'labour':this.assignee}
    }
  }
  async laboursPopOverOk(){
    let source = this.checkImage.some( item => item == true);
    let labour = this.selectedLabour.some( item => item == true);
    let message = "";
    if(!source){
      message ="Please Select "+this.selectedCategories.source_name_label;
    }
    if(!labour){
      message ="Please Select Labour";
    }
    if(!source && !labour){
      message ="Please Select Labour and "+this.selectedCategories.source_name_label;
    }
    if(source && labour){
    this.popoverController.dismiss(this.finalData); 
    } else {
      const alert = await this.alertController.create({
        header: 'Error',
        message: message,
        buttons: ['OK']
      });
      await alert.present();
    }
  }
  selectSegment(value){
    this.sourceSegment=value;
  }
  translation(){
     if(this.sg['userdata'].primary_language === 'en'){
  this.labels= {
    select:'Select',
    ok:'OK',
    cancel:'cancel',
    labour: 'Labours'
    };
   } else if(this.sg['userdata'].primary_language === 'te'){
  this.labels= {
    select:'ఎంచుకోండి',
    ok: "అలాగే",
    cancel: "రద్దు",
    labour: 'లేబర్'

  }
   } else if(this.sg['userdata'].primary_language === 'ta'){
  this.labels= {
    select:'தேர்வு',
    ok: "சரி",
    cancel: "ரத்து",
    labour: 'உழைப்பின்'

  };
   } else if(this.sg['userdata'].primary_language === 'kn'){
  this.labels= {
    select:'ಆಯ್ಕೆಮಾಡಿ',
    ok: "ಸರಿ",
    cancel: "ರದ್ದುಗೊಳಿಸಲು",
    labour: 'ಕಾರ್ಮಿಕರು'

  };
   } else if(this.sg['userdata'].primary_language === 'hi'){
  this.labels= {
    select:'चुनते हैं',
     ok: "ठीक",
    cancel: "रद्द करना",
    labour: 'मजदूरों'

  };
   }
}
translationMethod(data){
   let obj = {
      source_name_label:data.source_name,
      }
    if(this.sg['userdata'].primary_language !== 'en'){
      this.translate.translateObject(obj,'en',this.sg['userdata'].primary_language).then(data => {
        this.selectedCategories['source_name_label'] = data['source_name_label'];
        });
    } else {
      this.selectedCategories['source_name_label'] = obj['source_name_label'];
    }
}
translationMethodData(data){
  let temp = data
  let menu = [];
  for(let item of data){
    let obj = {
      name_label:item.name,
      }
    menu.push(obj)
  }
  for(let i=0;i<menu.length;i++){
    if(this.sg['userdata'].primary_language !== 'en'){
      this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
        this.selectedCategories.data[i]['name_label'] = data['name_label'];
        
        });
    } else {
      this.selectedCategories.data[i]['name_label']  = menu[i]['name_label'];
     
    }
  }
}
translationLaboursNames(data){
  let temp = data
  let menu = [];
  for(let item of data){
    let obj = {
      name_label:item.name,
      }
    menu.push(obj)
  }
  for(let i=0;i<menu.length;i++){
    if(this.sg['userdata'].primary_language !== 'en'){
      this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
        this.usersData[i]['name_label'] = data['name_label'];
        });
    } else {
      this.usersData[i]['name_label']  = menu[i]['name_label'];
    }
  }
}
  getSource(idx1){  
    if(this.checkImage[idx1]==true){
      this.checkImage[idx1]=false;
      this.selectedCategories.data[idx1].isSelected=false;
    }
    else{
      this.checkImage[idx1]=true;
      this.selectedCategories.data[idx1].isSelected=true;
      this.selectedSource.push(this.selectedCategories.data[idx1]);
    }
    if(this.selectedSource.length>=1){
      this.isSelectedSource=false;
    }
    let source:any;
    let temp:any=[];
    for(var i=0;i<this.selectedCategories.data.length;i++){
      if(this.selectedCategories.data[i].isSelected==true){
        if(this.selectedCategories.id=="1"){
          // temp.push({'name':this.selectedCategories.data[i].name});
          temp.push(this.selectedCategories.data[i]);
        }
        else{
          temp.push(this.selectedCategories.data[i]); 
          // temp.push({"source_code":this.selectedCategories.data[i].source_code,'name':this.selectedCategories.data[i].name});
        }
        source={'name':this.selectedCategories.source_name,'data':temp};
        if(this.assignee){
          this.finalData={'blockName':this.selectedCategories.block, 'plotName':this.selectedCategories.plot,'source':source,'labour':this.assignee}
        }else{
          this.finalData={'blockName':this.selectedCategories.block, 'plotName':this.selectedCategories.plot,'source':source}

        }
      }
    }
  }
  sourcePopOverCancel(){
    this.popoverController.dismiss();
  }
  // sourcePopOverOk(){

  //   this.popoverController.dismiss(this.finalData); 
  // }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
