import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSourcePopOverComponent } from './select-source-pop-over.component';

describe('SelectSourcePopOverComponent', () => {
  let component: SelectSourcePopOverComponent;
  let fixture: ComponentFixture<SelectSourcePopOverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSourcePopOverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSourcePopOverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
