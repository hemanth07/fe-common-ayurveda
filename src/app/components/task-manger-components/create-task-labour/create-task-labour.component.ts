import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavController, PopoverController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import {SimpleGlobal} from 'ng2-simple-global';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';
import { FarmeasyTranslate } from 'src/app/translate.service';
@Component({
  selector: 'farmeasy-create-task-labour',
  templateUrl: './create-task-labour.component.html',
  styleUrls: ['./create-task-labour.component.scss']
})
export class CreateTaskLabourComponent implements OnInit {
 // @Input('date') toDate;
  @Input('editTask') editTask;
  @Input('adhoc') adhoc;
  @Input('type') type;
  @Input('selectedDate') selectedDate1;
  @Input('showBlock') showBlock;
  @Input('laboursData') laboursData;
  @Input('blocks') blocks;
  @Output() selectedLabour: EventEmitter<any> = new EventEmitter();
  @Output() selectedBlock: EventEmitter<any> = new EventEmitter();
  @Output() selectedPriority: EventEmitter<any> = new EventEmitter();
  @Output() openDatePopOver: EventEmitter<any> = new EventEmitter();
  @Output() getSelectedDate: EventEmitter<any> = new EventEmitter();
  @Output() selectedTask: EventEmitter<any> = new EventEmitter();
  @Input('isLabour_date') isLabour_date;
  // blocks: any;
  blockName: any;
  userTask: any;
  labourName: any = '- select labour -';
  // laboursData: any = [];
  priority: any;
  selectedDate: any = new Date().toISOString().split("T")[0];
  showErrorMsg: any;
  toDate:any;
  labels: any;
  ok:any;isLaboursList:boolean=false;
  cancel:any;
  no_labours:any;
  priorityArr = [];
  adhocArr = [];
  constructor(private navCtrl: NavController,
    private route: ActivatedRoute,
    private popoverController: PopoverController,
    private sg: SimpleGlobal,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService,
    private translate:FarmeasyTranslate) { }

  ngOnInit() {
    
    this.translation();
    this.toDate = "DD-MON-YYYY";
    let that = this;
    // firebase.database().ref('lovs/blocks').on('value', resp => {
    //   let blocks = snapshotToArray(resp);
    //   this.blocks = blocks;
    //  // console.log(blocks);
    // });
    // let usersData = firebase.database().ref('users/');
    // usersData.once("value", function (Data) {
    //   let data1 = [];
    //   let temp = snapshotToArray(Data);
    //   that.sg['block'] = that.showBlock;
    //   if(that.showBlock == false){
    //     data1 = temp.filter(labour => {
    //     let blocks = [];
    //     blocks = labour.block_responsible;
    //     if(blocks){
    //     if (blocks.includes(that.editTask.block)) { return true; }
    //     } else {
    //       return false;
    //     }
    //     });
    //   } else {
    //     data1 = temp;
    //   }
    //   if (data1.length > 0) {
    //     that.getLaboursData(data1);
    //   }
    // });
    if(this.editTask && typeof this.editTask.save_time !== 'undefined') {
      this.labourName = this.editTask.labour?this.editTask.labour.name:'';
      this.selectedLabour.emit(this.editTask.labour);
      this.blockName = this.editTask.block;
      this.selectedBlock.emit(this.blockName);
      this.priority = this.editTask.priority;
      this.selectedPriority.emit(this.priority);
    }
  }
  ngOnChanges() {
    if (this.editTask && typeof this.editTask.save_time == 'undefined') {
      console.log(this.editTask);
      this.labourName = this.editTask.assignee.name;
      this.selectedLabour.emit(this.editTask.assignee);
      this.blockName = this.editTask.blocks[0].name;
      this.selectedBlock.emit(this.blockName);
      this.priority = this.editTask.priority;
      this.selectedPriority.emit(this.priority);
      this.userTask = this.editTask.adhoc_task;
      this.selectedTask.emit(this.userTask);
      console.log(this.userTask)
      if (!this.adhoc) {
        var d = new Date(this.editTask.end_date);
        console.log(d.getDate());
        console.log(d.getMonth() + 1);
        var month = d.getMonth() + 1;
        console.log(d.getFullYear());
        //this.toDate=d.getDate()+"-"+month+"-"+d.getFullYear();
        this.selectedDate = d.getDate() + "-" + month + "-" + d.getFullYear();
        this.toDate = d.getDate() + "-" + month + "-" + d.getFullYear();
        console.log(this.selectedDate);
        //this.getSelectedDate.emit(d.getFullYear() + "-" + month + '-' + d.getDate())
      }
    } 
    if (this.isLabour_date) {
      this.showErrorMsg = true;
    }
    if(this.selectedDate1){
      this.selectedDate=this.selectedDate1.split("-").reverse().join("-");
    }
    if(this.laboursData){
      this.translationLabours(this.laboursData);
    }

  }
  translationLabours(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        name_label:item.name,
      }
      menu.push(obj);
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i], 'en',this.sg['userdata'].primary_language).then(data => {
          this.laboursData[i]['name_label'] = data['name_label'];
          });
      } else {
        this.laboursData[i]['name_label'] = menu[i]['name_label'];
      }
    }
  }
  ngOnDestroy() {
    this.blocks = [];
    this.laboursData = [];
    this.editTask = [];
  }
  showLaboursList(){
    this.isLaboursList=!this.isLaboursList;
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels  = this.en.getCreateTaskLabourLabels();
      this.ok = "Okay";
      this.cancel = "Cancel";
      this.no_labours = "There is no labour";
      this.labourName = '- select labour -';
      this.priorityArr = [
        {value:"Urgent", name:"Urgent"},
        {value:"High", name:"High"},
        {value:"Medium", name:"Medium"},
        {value:"Low", name:"Low"},
      ]
      this.adhocArr = [
        {vlaue:"Soil Test",name:"Soil Test"},
        {vlaue:"Chemical Residual Test",name:"Chemical Residual Test"},
        {vlaue:"Water Level Test",name:"Water Level Test"},
        {vlaue:"Store Cleanliness Test",name:"Store Cleanliness Test"},
      ]
      } else if(this.sg['userdata'].primary_language === 'te'){
        this.labels  = this.te.getCreateTaskLabourLabels();
        this.ok = "సరే";
        this.cancel = "రద్దు";
        this.no_labours = "లేబర్స్ లేరు";
        this.labourName = '- లేబర్స్ ఎంచుకోండి -';
        this.priorityArr = [
          {value:"Urgent", name:"అర్జంట్"},
          {value:"High", name:"అధిక"},
          {value:"Medium", name:"మీడియం"},
          {value:"Low", name:"తక్కువ"},
        ]
        this.adhocArr = [
          {vlaue:"Soil Test",name:"నేల పరీక్ష"},
          {vlaue:"Chemical Residual Test",name:"రసాయన అవశేష పరీక్ష"},
          {vlaue:"Water Level Test",name:"నీటి స్థాయి పరీక్ష"},
          {vlaue:"Store Cleanliness Test",name:"స్టోర్ శుభ్రత పరీక్ష"},
        ]

      } else if(this.sg['userdata'].primary_language === 'ta'){
        this.labels  = this.ta.getCreateTaskLabourLabels();
        this.ok = "சரி";
        this.cancel = "ரத்து";
        this.no_labours = "உழைப்பு இல்லை";
        this.labourName = '- லேபர்களைத் தேர்ந்தெடுக்கவும் -';
        this.priorityArr = [
          {value:"Urgent", name:"அவசர"},
          {value:"High", name:"உயர்"},
          {value:"Medium", name:"நடுத்தர"},
          {value:"Low", name:"குறைந்த"},
        ]
        this.adhocArr = [
          {vlaue:"Soil Test",name:"மண் சோதனை"},
          {vlaue:"Chemical Residual Test",name:"வேதியியல் எஞ்சிய சோதனை"},
          {vlaue:"Water Level Test",name:"நீர் நிலை சோதனை"},
          {vlaue:"Store Cleanliness Test",name:"கடை சுத்தம் சோதனை"},
        ]

      } else if(this.sg['userdata'].primary_language === 'kn'){
        this.labels  = this.kn.getCreateTaskLabourLabels();
        this.ok = "ಸರಿ";
        this.cancel = "ರದ್ದುಮಾಡಿ";
        this.no_labours = "ಶ್ರಮವಿಲ್ಲ";
        this.labourName = '- ಲೇಬರ್ಗಳನ್ನು ಆಯ್ಕೆಮಾಡಿ -';
        this.priorityArr = [
          {value:"Urgent", name:"ತುರ್ತು"},
          {value:"High", name:"ಹೆಚ್ಚು"},
          {value:"Medium", name:"ಮಾಧ್ಯಮ"},
          {value:"Low", name:"ಕಡಿಮೆ"},
        ]
        this.adhocArr = [
          {vlaue:"Soil Test",name:"ಮಣ್ಣಿನ ಪರೀಕ್ಷೆ"},
          {vlaue:"Chemical Residual Test",name:"ರಾಸಾಯನಿಕ ಉಳಿಕೆ ಪರೀಕ್ಷೆ"},
          {vlaue:"Water Level Test",name:"ನೀರಿನ ಮಟ್ಟದ ಪರೀಕ್ಷೆ"},
          {vlaue:"Store Cleanliness Test",name:"ಅಂಗಡಿ ಸ್ವಚ್ l ತೆ ಪರೀಕ್ಷೆ"},
        ]
      } else if(this.sg['userdata'].primary_language === 'hi'){
        this.labels  = this.hi.getCreateTaskLabourLabels();
        this.ok = "ठीक है";
        this.cancel = "रद्द करना";
        this.no_labours = "कोई श्रम नहीं है";
        this.labourName = '- मजदूरों का चयन करें -';
        this.priorityArr = [
          {value:"Urgent", name:"अति आवश्यक"},
          {value:"High", name:"उच्च"},
          {value:"Medium", name:"मध्यम"},
          {value:"Low", name:"कम"},
        ]
        this.adhocArr = [
          {vlaue:"Soil Test",name:"मृदा परीक्षण"},
          {vlaue:"Chemical Residual Test",name:"रासायनिक अवशिष्ट परीक्षण"},
          {vlaue:"Water Level Test",name:"जल स्तर का परीक्षण"},
          {vlaue:"Store Cleanliness Test",name:"स्टोर स्वच्छता परीक्षण"},
        ]

      }
  }
  getLaboursData(data) {
    console.log(data);
    this.laboursData = data.filter(user =>
      user.role == 'employee');
    console.log(this.laboursData)
  }
  getToDate() {
    this.openDatePopOver.emit();
  }
  selectName(name){
    this.isLaboursList=false;
    this.labourName = name.name_label
    let temp = { "name": name.name, "phone": name.phone, "profile_url": name.profile_url, "role": name.role,'farmeasy_id':name.farmeasy_id,
    'device_id':name.device_id?name.device_id:null, 'primary_language':name.primary_language}
    console.log(temp)
    this.selectedLabour.emit(temp);
    console.log(temp)
  }
  getLabour(val) {
    let device_id='null';
    console.log(val.detail.value);
    let data = this.laboursData.filter(user =>
      user.name == val.detail.value);
      if(data[0].device_id){
        device_id=data[0].device_id;
      }
    let temp = { "name": data[0].name, "phone": data[0].phone, "profile_url": data[0].profile_url, "role": data[0].role,'farmeasy_id':data[0].farmeasy_id,
    'device_id':device_id?device_id:null, 'primary_language':data[0].primary_language}
    console.log(temp)
    this.selectedLabour.emit(temp);
    console.log(temp)
  }
  getBlock(val) {
    this.selectedBlock.emit(val.detail.value);
  }
  getPriorty(val) {
    this.selectedPriority.emit(val.detail.value);
  }
  getTask(val) {
    console.log(val);
    this.selectedTask.emit(val.detail.value);
  }
  dateConverter(date){
    if(date){
      let d = new Date(date);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      if(isNaN(day)){
        return "DD-Mon-YYYY";
      } else {
      return day+"-"+months[month]+"-"+year;
      }
    }
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
