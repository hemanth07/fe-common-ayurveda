import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTaskLabourComponent } from './create-task-labour.component';

describe('CreateTaskLabourComponent', () => {
  let component: CreateTaskLabourComponent;
  let fixture: ComponentFixture<CreateTaskLabourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTaskLabourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTaskLabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
