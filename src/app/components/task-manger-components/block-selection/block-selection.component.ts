import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Storage } from '@ionic/storage';
import {SimpleGlobal} from 'ng2-simple-global';
import { NavController, PopoverController,AlertController} from '@ionic/angular';
import { SelectSourcePopOverComponent } from '../select-source-pop-over/select-source-pop-over.component'
import { OverlayEventDetail } from '@ionic/core';

@Component({
  selector: 'app-block-selection',
  templateUrl: './block-selection.component.html',
  styleUrls: ['./block-selection.component.scss']
})
export class BlockSelectionComponent implements OnInit {
  @Input() Data: any;
  @Input('totalData') totalData;
  @Input('finalData') finalData;  
  @Input('selectedblocksWithCategory') blockWithCategory;
  @Input('arrow') arrow;
  @Input('flags') flags;
  @Input('viewPlot') viewPlot;
  @Output() selected_block: EventEmitter<any>  = new EventEmitter();
  @Output() selected_blockWithPlotName: EventEmitter<any>  = new EventEmitter();
  blocks:any;
  selected_plots:any;
  blockNames:any;
  taskByCategory:any=[];
  showBlocks:any= [];
  label: string;
  selectedblocksWithCategory:any=[];
  selectedSource:any=[];
  selectedBlockFlag = [];
  constructor(private storage: Storage, private sg: SimpleGlobal,private popoverController: PopoverController) { 
  //   this.storage.get('selected_plots')
  // .then(
  //   data => console.log(data),
  //   error => console.error(error)
  // );
  }

  ngOnInit() {
    
    this.blocks = this.Data;
    // console.log(this.arrow);
   
   console.log(this.blocks);
   //this.blockNames=this.blocks[0].block;
   this.translation();
  // console.log(this.selected_plots);
     }
  ngOnDestroy(){
    this.blocks=[];
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
     this.label = 'Block Selection';
      } else if(this.sg['userdata'].primary_language === 'te'){
     this.label = 'బ్లాక్ ఎంపిక';
      } else if(this.sg['userdata'].primary_language === 'ta'){
     this.label = 'பிளாக் தேர்வு';
      } else if(this.sg['userdata'].primary_language === 'kn'){
     this.label = 'ಬ್ಲಾಕ್ ಆಯ್ಕೆ';
      }else if(this.sg['userdata'].primary_language === 'hi'){
     this.label = 'ब्लॉक चयन';
      }
  }
  ngOnChanges(){
 // console.log(this.blocks);
  let that=this;
  if(this.totalData){
    that.blocks = that.totalData;
   }
  if(this.flags){
      this.selectedBlockFlag = this.flags;
      this.viewPlot = this.selectedBlockFlag.includes(true);
    }
  // this.blocks.filter(block => {if(block.plots.length>1)
  //   this.showBlocks=true;})
   
  
 
  if(this.finalData && this.finalData.length>0){
  
    that.blocks = that.finalData;
    //this.blocks=this.finalData
  }
  //console.log(this.blocks);
  //console.log(this.finalData);
  if(this.blockWithCategory){
    this.selectedblocksWithCategory=this.blockWithCategory
  }
  }
  selectedBlock(val){
    this.selected_block.emit(val);
  }
  // async getplot(plot,block){
  //   //let val:any=[];
  //   //this.blocks = this.totalData;
  //   if(plot.name){
  //     let source1:any=[];
  //     if(this.selectedblocksWithCategory){
  //       for (var i = 0; i < this.selectedblocksWithCategory.sources.length; i++) {
  //         if (this.selectedblocksWithCategory.sources[i].id == this.selectedblocksWithCategory.categoryId) {
  //           source1 = this.selectedblocksWithCategory.sources[i];
  //         }
  //       }

  //     }
  //   // val={'plot':plot.name,'block':block.name}
  //   // console.log(val);

  //   source1['plot'] = plot.name;
  //   source1['block'] = block.name;
  //   console.log(source1);
  //   if (source1) {
  //     let popover = await this.popoverController.create({
  //       component: SelectSourcePopOverComponent,

  //       cssClass: "popover_class",
  //       componentProps: {
  //         category: source1,
  //         blockName: this.selectedBlock,
  //        // totalBlocks: this.categoriesData.blocks,
  //         popoverType:"source"
  //       },

  //     });
      

  //     popover.onDidDismiss().then((detail: OverlayEventDetail) => {
  //       this.getWaterSource(detail.data);
  //     });
  //     await popover.present();
  //   }
  //  // this.selected_blockWithPlotName.emit(val);
  //   this.blocks = this.totalData;
  //   }
  // }
  getplot(plot,block){
    if(plot.isSelectedPlot){
      let val:any=[];
      val={'plot':plot.name,'block':block.name}
      this.selected_blockWithPlotName.emit(val);
    }
  }
  // getWaterSource(val){
  //  // console.log(val);
  //   this.selectedSource=val;
  //   this.selected_blockWithPlotName.emit(val);
  // }

}
