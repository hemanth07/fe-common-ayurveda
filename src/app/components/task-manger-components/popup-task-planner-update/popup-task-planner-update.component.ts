import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { NavParams, NavController, PopoverController, LoadingController } from '@ionic/angular';
import { SimpleGlobal } from "ng2-simple-global";

@Component({
  selector: 'app-popup-task-planner-update',
  templateUrl: './popup-task-planner-update.component.html',
  styleUrls: ['./popup-task-planner-update.component.scss'],
})
export class PopupTaskPlannerUpdateComponent implements OnInit {
  usersData:any=[];taskData:any;
  selectedLabour:any=null;
  selectedPriority:any=null;
  labels:any = {}
  constructor( private popoverController: PopoverController,
               private navParams:NavParams,
               public sg:SimpleGlobal,
               private loadingController:LoadingController) {
    this.taskData = this.navParams.get('taskData');
    console.log(this.taskData);
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').on('value', resp => {
      this.usersData = snapshotToArray(resp);
  
      // this.usersData = data.filter(data => data.role == 'labour')
  
      //this.usersData['page'] = 'createTaskByCategory';
    });
  }

  ngOnInit() {
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels['title'] = "Mass Update";
      this.labels['subtitle'] = "Please select the fields you wish to update";
      this.labels['field'] = "Field";
      this.labels['value'] = "New Value";
      this.labels['labour'] = "Labour";
      this.labels['priority'] = "Priority";
      this.labels['update'] = "Update";
      this.labels['cancel'] = "Cancel";
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.labels['title'] = "మాస్ నవీకరణ";
      this.labels['subtitle'] = "దయచేసి మీరు నవీకరించాలనుకుంటున్న ఫీల్డ్‌లను ఎంచుకోండి";
      this.labels['field'] = "ఫీల్డ్";
      this.labels['value'] = "క్రొత్త విలువ";
      this.labels['labour'] = "లేబర్";
      this.labels['update'] = "నవీకరణ";
      this.labels['cancel'] = "రద్దు చేయండి";
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.labels['title'] = "வெகுஜன புதுப்பிப்பு";
      this.labels['subtitle'] = "நீங்கள் புதுப்பிக்க விரும்பும் புலங்களைத் தேர்ந்தெடுக்கவும்";
      this.labels['field'] = "களம்";
      this.labels['value'] = "புதிய மதிப்பு";
      this.labels['labour'] = "தொழிலாளர்";
      this.labels['update'] = "புதுப்பிக்கப்பட்டது";
      this.labels['cancel'] = "ரத்து";
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.labels['title'] = "ಸಾಮೂಹಿಕ ನವೀಕರಣ";
      this.labels['subtitle'] = "ದಯವಿಟ್ಟು ನೀವು ನವೀಕರಿಸಲು ಬಯಸುವ ಕ್ಷೇತ್ರಗಳನ್ನು ಆಯ್ಕೆ ಮಾಡಿ";
      this.labels['field'] = "ಕ್ಷೇತ್ರ";
      this.labels['value'] = "ಹೊಸ ಮೌಲ್ಯ";
      this.labels['labour'] = "ಕಾರ್ಮಿಕ";
      this.labels['update'] = "ನವೀಕರಿಸಿ";
      this.labels['cancel'] = "ರದ್ದುಮಾಡಿ";
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.labels['title'] = "मास अपडेट";
      this.labels['subtitle'] = "कृपया उन फ़ील्ड्स का चयन करें जिन्हें आप अपडेट करना चाहते हैं";
      this.labels['field'] = "मैदान";
      this.labels['value'] = "नया मूल्य";
      this.labels['labour'] = "श्रम";
      this.labels['update'] = "अपडेट करें";
      this.labels['cancel'] = "रद्द करना";
    }
  }

  async update(){
  const loading = await this.loadingController.create({
    spinner: 'bubbles',
    message: 'Please wait...',
    translucent: true,
    cssClass: 'custom-class custom-loading'
  });
  loading.present();
    if(this.taskData.length>0){
      for(var i=0;i<this.taskData.length;i++){
        if(this.selectedLabour){
          firebase.database().ref(this.sg['userdata'].vendor_id+"/taskManager/task_table/"+this.taskData[i].key).update({'assignee':this.selectedLabour});
        }
        if(this.taskData[i].priority && this.selectedPriority){
          firebase.database().ref(this.sg['userdata'].vendor_id+"/taskManager/task_table/"+this.taskData[i].key).update({'priority':this.selectedPriority });
        }
      }  
    }
    this.taskData=[];
    loading.dismiss();
    this.popoverController.dismiss('update');
  }
  cancel(){
    this.popoverController.dismiss('cancel');
    this.taskData=[];
  } 
  // taskUpdate(data){
  //   if(this.taskData.length>0){
  //     for(var i=0;i<this.taskData.length;i++){
  //       if(this.selectedLabour){
  //         firebase.database().ref("TaskManager/Task_table/"+this.taskData[i].key).update({'assignee':this.selectedLabour});
  //       }
  //       if(this.taskData[i].priority && this.selectedPriority){
  //         firebase.database().ref("TaskManager/Task_table/"+this.taskData[i].key).update({'priority':this.selectedPriority });
  //       }
  //     }  
      
  //     console.log(data)
  //   }
    
  // }
  selectLabour(event){
    console.log(event);
    let labour = event.detail.value;
    this.selectedLabour = {device_id:labour.device_id?labour.device_id:null,
                           farmeasy_id:labour.farmeasy_id,
                           name:labour.name,
                           phone:labour.phone,
                           primary_language:labour.primary_language,
                           profile_url:labour.profile_url,
                           role:labour.role}
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
