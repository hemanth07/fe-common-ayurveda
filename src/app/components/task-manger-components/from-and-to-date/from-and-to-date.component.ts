import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-from-and-to-date',
  templateUrl: './from-and-to-date.component.html',
  styleUrls: ['./from-and-to-date.component.scss']
})
export class FromAndToDateComponent implements OnInit {
  @Output() isDateClicked: EventEmitter<any>  = new EventEmitter();
  @Output() search_data: EventEmitter<any>  = new EventEmitter();
  @Input('fromDate') fromDate;
  @Input('toDate') toDate;
  @Input('color') color;
  @Input('clearFiltersData') clearFiltersData;

  constructor() { }

  ngOnInit() {
    this.fromDate="DD-Mon-YY";
    this.toDate="DD-Mon-YY";
  }
  ngOnChanges(){
    if(this.clearFiltersData){
      this.fromDate="DD-Mon-YY";
      this.toDate="DD-Mon-YY";
    }
  }
  getToDate(){
    
    this.isDateClicked.emit('toDate');
  }
  getFromDate(){
    console.log('fromDate');
    this.isDateClicked.emit('fromDate');
  }
  searchData(){
    this.search_data.emit();
  }
  dateConverter(date){
    if(date){
      let d = new Date(date);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      if(isNaN(day)){
        return "DD-Mon-YYYY";
      } else {
      return day+"-"+months[month]+"-"+year;
      }
    }
  }
} 
