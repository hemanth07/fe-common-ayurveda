import { Component, OnInit,NgZone } from '@angular/core';
import { NavParams, NavController, PopoverController} from '@ionic/angular';
import { ChangeDetectorRef } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';

@Component({
  selector: 'app-add-to-group',
  templateUrl: './add-to-group.component.html',
  styleUrls: ['./add-to-group.component.scss']
})
export class AddToGroupComponent implements OnInit {
  search:any = ""
  existingList:any = [];
  usersList:any = [];
  tempUserList:any = [];
  selectedUsers:any = [];
  constructor(private popoverController: PopoverController,
              public navParams: NavParams,
              private zone: NgZone,
              private changeRef: ChangeDetectorRef,
              public sg:SimpleGlobal) { }
  ngOnInit() {
   this.existingList = this.navParams.get('existingList');
   for(let i=0 ;i< this.existingList.list.length ;i++){
    this.existingList.list[i]['select'] = false;
    this.existingList.list[i]['index'] = i;
    }
   this.usersList = JSON.parse(JSON.stringify(this.existingList.list));
   this.tempUserList = JSON.parse(JSON.stringify(this.existingList.list));
   console.log(this.existingList);
  }
  
  listenSearch(event){
    console.log(event.detail.value);
    this.usersList =JSON.parse(JSON.stringify(this.tempUserList.filter(item => item.name.toLowerCase().includes(event.detail.value.toLowerCase()))));
  }
  removeCapsule(capsule){
    let id = capsule.id;
    for(let i=0 ;i<this.usersList.length;i++){
      if(this.usersList[i].farmeasy_id == id){
        if(this.usersList[i].select){
          this.usersList[i].select = false;
          }
        }
    }
    for(let j=0;j<this.tempUserList.length;j++){
      if(this.tempUserList[j].farmeasy_id == id){
        if(this.tempUserList[j].select){
          this.tempUserList[j].select = false;
          }
        }
      }
      for(let k=0;k<this.selectedUsers.length;k++){
        if(this.selectedUsers[k].id == id){
          this.selectedUsers.splice(k,1);
        }
      }
  }
  addToGroup(user,idx){
    console.log(user);
      console.log(this.usersList[idx]['select']);
      if(this.usersList[idx].select){
        this.usersList[idx].select = false;
      } else{
        this.usersList[idx].select = true;
      }
      console.log(this.usersList[idx]['select']);
      let id = user.farmeasy_id;
      let data = [...this.tempUserList]
      for(let i=0; i<data.length; i++){
        if(this.tempUserList[i].farmeasy_id == id){
          if(this.tempUserList[i].select){
          this.tempUserList[i].select = false;
          for(let j=0;j<this.selectedUsers.length;j++){
              if(this.selectedUsers[j].id == id){
                this.selectedUsers.splice(j,1);
              }
            }
          } else {
            this.tempUserList[i].select = true;
            this.selectedUsers.push({id:this.tempUserList[i].farmeasy_id,
                                     name:this.tempUserList[i].name,
                                     profile_url:this.tempUserList[i].profile_url,
                                     role:this.tempUserList[i].role});
          }
        }
      }
      console.log(this.selectedUsers);
  }
  close(){
    this.popoverController.dismiss();
  }
  done(){
    this.popoverController.dismiss(this.selectedUsers);
  }
}
