import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, PopoverController} from '@ionic/angular';
import { ChangeDetectorRef } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.scss']
})
export class GroupDetailsComponent implements OnInit {
group:any;
members:any;
labour:boolean = false;
  constructor(public navParams: NavParams,
    public navCtrl: NavController,
    private sg: SimpleGlobal,
    public popoverCtrl: PopoverController,
    ) { }

  ngOnInit() {
    this.group = this.navParams.get('group');
    if(this.group.group_icon == ""){
      this.group.group_icon = "https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2Fdefault.jpeg?alt=media&token=07684228-cd2b-4b0b-9a8c-7fbf4560e766";
    }
    this.members = Object.keys(this.group.members);
    if(this.sg['userdata'].role != 'employee'){
     this.labour = false;
    } else {
      this.labour = true;

    }
    console.log(this.group);
  }
  info(){
    this.popoverCtrl.dismiss();
    const data = {group: JSON.stringify(this.group)};
    this.navCtrl.navigateForward(['/edit-group-description',data]);
  }

}
