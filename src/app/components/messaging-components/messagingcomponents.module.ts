import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { AddToGroupComponent } from './add-to-group/add-to-group.component';
import { ChatMenuOptionsComponent } from './chat-menu-options/chat-menu-options.component';
import { GroupDetailsComponent } from './group-details/group-details.component';
@NgModule({
  declarations: [AddToGroupComponent, ChatMenuOptionsComponent, GroupDetailsComponent],
  imports: [
    CommonModule,IonicModule.forRoot(),FormsModule
  ],
  exports:[AddToGroupComponent,ChatMenuOptionsComponent,GroupDetailsComponent],
  entryComponents:[AddToGroupComponent,ChatMenuOptionsComponent,GroupDetailsComponent]
})
export class MessagingcomponentsModule { }
