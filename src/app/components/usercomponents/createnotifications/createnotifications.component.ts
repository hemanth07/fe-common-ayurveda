import { Component, OnInit, NgZone } from '@angular/core';
import { NavController,ToastController,PopoverController} from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { LoadingController } from '@ionic/angular';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { Location } from '@angular/common';
import { ProgressBarComponent } from './../../progress-bar/progress-bar.component';
@Component({
  selector: 'createnotifications',
  templateUrl: './createnotifications.component.html',
  styleUrls: ['./createnotifications.component.scss']
})
export class CreatenotificationsComponent implements OnInit {
  farmeasy_ref = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/');
  // blocks: any = ["Block A", "Block B", "Block C", "Block D", "Block E", "Block F"];
  // categories: any = [{ "name": "watering" }, { "name": "cleaning" }, { "name": "fertigation" }, { "name": "harvesting" }, { "name": "plantation" }, { "name": "adhoc Task" }];
  prioritys = ["Urgent", "High", "Medium", "Low"];
  // Labours = ["Urgent", "High", "Medium", "Low"];
  Users: any;
  blocks:any=[];
  categories:any=[];
  selected_user: any = [];
  finalusers: any = [];
  selectedblock: any;
  selectedcategoty: any;
  selectedpriority: any;
  selectedLabour: any;
  description: any;
  errormessage: boolean = true;
  user_data: any = {};
  date: any;
  notification_body = [];
  not_key: any;
  image_date: any;
  image_data: any='';
  images_data:any='';
  video:any='';
  Labourers: any;
  checkedstatus: boolean = false;
  SelectAll: any;page_status: any;
  labels:any = {};
  sendStatus: boolean = false;
 constructor(private storage: Storage,
 public toastController:ToastController,
 public navCtrl: NavController,
 private file: File,
 private media: Media,
 private mediaCapture: MediaCapture,
 public loadingCtrl: LoadingController,
 private imagePicker: ImagePicker,
 private router: Router,
 private route: ActivatedRoute,
 private camera: Camera,
 public sg: SimpleGlobal,
 private en: EnglishService,
 private hi: HindiService,
 private ta:TamilService,
 private te:TeluguService,
 private kn:KanadaService,
 private zone:NgZone,
 public popoverController: PopoverController,
 private location:Location) { }

  ngOnInit() {
    this.translation();
    this.date = Date.now();
    this.image_date = new Date().toString();
    this.storage.get('farmeasy_userdata').then((val) => {
      console.log('Your age is', val);
      this.user_data = val;
      if (this.user_data != "undefined") {
        firebase.database().ref(this.sg['userdata'].vendor_id+'/users/').on('value', resp => {
          let users = snapshotToArray(resp);
          this.Users = users;
          this.Labourers = users;
          for (let i = 0; i < this.Users.length; i++) {
            this.selected_user[i] = false;
            if (this.Users[i].phone == this.user_data.phone) {
              this.Users.splice(i, 1);
            }
          }
        });
      //  console.log(JSON.stringify(this.Users));
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').on('value', resp => {
          let blocks = snapshotToArray(resp);
          this.blocks = blocks;
          console.log(blocks);
          //that.editData();
        });
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').on('value', resp => {
          let categories = snapshotToArray(resp);
          this.categories = categories;
         // console.log(categories);
        });
      }
    });

    let sub = this.route.params.subscribe(params => {
      this.page_status =JSON.parse(params["page_status"]); 
     // console.log(this.page_status);
 });

  }
translation(){
  let data;
    if(this.sg['userdata'].primary_language === 'en'){
        data  = this.en.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'te'){
        data  = this.te.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'ta'){
        data  = this.ta.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'kn'){
        data  = this.kn.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'hi'){
        data  = this.hi.getCreateNotificationLabels();
      }
    this.labels['block'] = data[0];
    this.labels['category'] = data[1];
    this.labels['priority'] = data[2];
    this.labels['labour'] = data[3];
    this.labels['select_emp'] = data[4];
    this.labels['select_all'] = data[5];
    this.labels['select_field'] = data[6];
    this.labels['send'] = data[7];
}

  selectuser(id) {
    for (let i = 0; i < this.Users.length; i++) {
      if (i == id) {
        if (this.selected_user && this.selected_user.length > 0) {
          for (let j = 0; j < this.selected_user.length; j++) {
            if (j == id) {
              if (this.selected_user[j] == true) {
                this.selected_user[id] = false;
                if (this.finalusers && this.finalusers.length) {
                  for (let s = 0; s < this.finalusers.length; s++) {
                    if (this.finalusers[s].name == this.Users[i].name) {
                      this.finalusers.splice(s, 1);
                    }
                  }
                }
              }
              else {
                this.selected_user[id] = true;
                this.finalusers.push({ "name": this.Users[i].name, "key": this.Users[i].key });
              }
            }
          }
        }
        this.SelectAll = this.selected_user.every(function (item: any) {
          return item == true;
        })
       // console.log(this.finalusers);
      }
    }
  }

  SelectAllUsers() {
    this.finalusers = [];
    for (var i = 0; i < this.Users.length; i++) {
      this.selected_user[i] = true;
      this.selected_user[i] = this.SelectAll;
      if (this.selected_user[i] == true) {
        this.finalusers.push({ "name": this.Users[i].name, "key": this.Users[i].key });
      }
    }
   // console.log(this.finalusers);
  }
  descriptionListen(){
    this.sendStatus = false;
  }
  priorityListen(){
    this.sendStatus = false;
  }

  sendnotification() {
    if (this.finalusers && this.finalusers.length > 0 && this.selectedpriority  && this.description) {
      this.errormessage = true;
      this.sendStatus = true;
      for (let i = 0; i < this.finalusers.length; i++) {
        let user_name = this.finalusers[i].name;
        this.notifications(this.finalusers[i].key);
      }
      this.senderMessage();
    }
    else {
      this.errormessage = false;
    }
  }
  senderMessage(){
    let media={'camera':this.image_data,'attachments':this.images_data,'video':this.video}
    let body: any;
    let that = this;
    body = {
      "Priority": this.selectedpriority,
      "notiification_description": this.description,
      "createdat": this.date,
      "sendername": this.user_data.name,
      "senderimage": this.user_data.profile_url,
      "senderkey":this.user_data.key,
      "role":this.user_data.role,
      "media":media,
      "sender":true,
      "read":true,
    };
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' +this.sg['userdata'].key + '/notifications').push(body);
  }

  //------------------------------------Send Notification----------------------------------------//

  notifications(key) {
    let media={'camera':this.image_data,'attachments':this.images_data,'video':this.video}
    let body: any;
    let user_key = key;
    let that = this;
    body = {
      "Priority": this.selectedpriority,
      "notiification_description": this.description,
      "createdat": this.date,
      "sendername": this.user_data.name,
      "senderimage": this.user_data.profile_url,
      "senderkey":this.user_data.key,
      "role":this.user_data.role,
      "media":media,
      "status": "Acknowledge",
      "notification_status": "Mark as Done",
    };
    let newData = firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + user_key + '/notifications').push(body).then(async() =>{
      const toast = await this.toastController.create({
        message: 'Notification Sent Successfully',
        duration: 2000
      });
      toast.present();
    this.location.back();

    });
    // let usersRef = firebase.database().ref("users/" + user_key + "/notifications");
    // usersRef.once("value", function (Data) {
    //   let data = snapshotToArray(Data);
    //  // console.log(data);
    //  // console.log(data[data.length - 1].key);
    //   that.not_key = data[data.length - 1].key;
    //   that.notificationslog(user_key, that.not_key, body);
    // });
  }

  //------------------------------------Send Notification-Log----------------------------------------//

  // notificationslog(key, not_key, body) {
  //   body["notification_key"] = not_key;
  //   let newData = firebase.database().ref('users/' + key + '/notifications_log').push(body);
  //   let data={"page_status":JSON.stringify(this.page_status)}
  //   // this.location.back();
  //   this.navCtrl.navigateForward(['/notifications',data]);
  // }

  multipleImageAttachment() {
    let  urls;
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const options: CameraOptions = {
             quality: 100,
             destinationType: this.camera.DestinationType.DATA_URL,
             encodingType: this.camera.EncodingType.JPEG,
             mediaType: this.camera.MediaType.PICTURE,
             sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
           }
            this.camera.getPicture(options).then(async (imageData) => {
              this.date = new Date().getTime();
                const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
              let that;
              that = this;
                     let img_url  = 'data:image/jpeg;base64,' + imageData;
                     fetch(img_url)
                     .then(res => res.blob())
                     .then(blob => {
          this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/images/' + this.date);
                       urls = {'name' : this.sg['userdata'].vendor_id+'/notifications/images/' + this.date};
                       firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/images/' + this.date).put(blob).then(function(snapshot) {
                         setTimeout(() => {
                           firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                            popover.dismiss();
                            const body = { 'type': 'gallery', 'url' : url  , 'path': urls };
                            that.image_data = body;
                                });
                         }, 1000);
                       });
                               that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/images/' + this.date).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                    });
                   }, (err) => {
                     // Handle error
                alert(err);
       });
  }
  captureImage() {
    let  urls;
    const options: CameraOptions = {
      quality: 10,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
       this.camera.getPicture(options).then(async (imageData) => {
                  this.date = new Date().getTime();
                 const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });

                  let that;
                  that = this;
                         let img_url  = 'data:image/jpeg;base64,' + imageData;
                         fetch(img_url)
                         .then(res => res.blob())
                         .then(blob => {
          this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/images/' + this.date);
                           urls = {'name' : this.sg['userdata'].vendor_id+'/notifications/images/' + this.date};
                           firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/images/' + this.date).put(blob).then(function(snapshot) {
                             setTimeout(() => {
                               firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                                popover.dismiss();
                                const body = { 'type': 'camera', 'url' : url  , 'path': urls };
                                that.image_data = body;
                                });
                             }, 1000);
                           });

                                that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/images/' + this.date).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
                        });
                       }, (err) => {
                         // Handle error
                alert(err);
           });
  }
  captureVedio() {
    let urls:any;
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const options: CaptureVideoOptions = {
      limit: 1,
      duration: 30
    };
    this.mediaCapture.captureVideo(options)
    .then(
      (data: MediaFile[]) => {
        let that;
        that = this;
        const index = data[0].fullPath.lastIndexOf('/');
        let finalPath = data[0].fullPath.substr(0, index);
        if(finalPath.substring(0, 7) !== 'file://'){
        finalPath = 'file://'+finalPath;
        }
        this.file.readAsArrayBuffer(finalPath, data[0].name).then( async(file) => {
        const blob = new Blob([file], {type: data[0].type});
     const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Video'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
        urls = {'name' : this.sg['userdata'].vendor_id+'/notifications/videos/' + data[0].name};
        this.sg['currentFile'] =  firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/videos/' + data[0].name);
        firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/videos/' + data[0].name).put(blob).then(function(snapshot) {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/videos/' + data[0].name).getDownloadURL().then(function(url) {
            popover.dismiss();
            const body = { type: 'video', url : url,  path : urls };
            that.video = body;
          });
         });
         that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/notifications/videos/' + data[0].name).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
        });
      },
      (err: CaptureError) => console.error(err)
    );
  }
  presentRemoveAlert(data,type){
    if(type == 'camera'){
      firebase.storage().ref(data.path.name).delete();
      this.image_data = {};
    }
    else{
      firebase.storage().ref(data.path.name).delete();
      this.video = {};
    }
 }


}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
