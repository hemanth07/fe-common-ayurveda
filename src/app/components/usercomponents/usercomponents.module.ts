import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsereditprofileComponent } from './usereditprofile/usereditprofile.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HttpModule } from '@angular/http';

import { NotificationsComponent } from './notifications/notifications.component';
import { CreatenotificationsComponent } from './createnotifications/createnotifications.component';
import { ViewnotificationComponent } from './viewnotification/viewnotification.component';
import { SendtopicpopoverComponent } from './sendtopicpopover/sendtopicpopover.component';

import { TranslatePipe } from "../../translate.pipe";


@NgModule({
  declarations: [TranslatePipe,UsereditprofileComponent, NotificationsComponent,CreatenotificationsComponent, ViewnotificationComponent, SendtopicpopoverComponent],
  imports: [FormsModule,
    CommonModule, IonicModule.forRoot(),HttpModule
  ],
  exports: [UsereditprofileComponent,NotificationsComponent,CreatenotificationsComponent,ViewnotificationComponent],
  entryComponents: [SendtopicpopoverComponent
  ],
})
export class UsercomponentsModule { }
