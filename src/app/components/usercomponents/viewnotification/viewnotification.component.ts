import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from './../../../translate.service';
import { Component, OnInit,Input } from '@angular/core';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'viewnotification',
  templateUrl: './viewnotification.component.html',
  styleUrls: ['./viewnotification.component.scss']
})
export class ViewnotificationComponent implements OnInit {
  @Input("notificationkey") notificationkey;

  notifications_data:any={};
  senderimage:any;
  status:any;
  notification_status:any;
  notiification_description:any;
  createdat:any;
  enddate:any;
  sendername:any;
  Category:any;
  Labour:any;
  priority:any;
  role:any;
  remarks: any;
  userdata: any = [];
  // priority: any;
  constructor(private storage: Storage,
    private translate:FarmeasyTranslate,
    public sg:SimpleGlobal) { }

  ngOnInit() {
    this.initilisedata();
    this.translationData([this.notificationkey]);
          // alert(JSON.stringify(this.notificationkey))

  }
  
  initilisedata(){
    let that=this;
    // that.storage.get('farmeasy_userdata').then((val) => {
    //      console.log('Your age is', val);
    //     if(val){
    //      let that = this;
    //      let usersRef = firebase.database().ref("users/"+val.key+"/notifications");
    //      usersRef.once("value", function (Data) {
    //     let data=snapshotToArray(Data);
    //     for(let i=0;i<data.length;i++){
    //     if(data[i].key == that.notificationkey){
    //       that. translationData(data[i]);
    //       console.log(data[i].notification_key);
    //       console.log(that.notificationkey);
    //       that.notifications_data=data[i];
    //       that.senderimage=data[i].senderimage;
    //       that.status=data[i].status;
    //       that.notification_status=data[i].notification_status;
    //       that.notiification_description=data[i].notiification_description;
    //       that.createdat=that.timeSince(data[i].createdat);
    //       that.sendername=data[i].sendername;
    //       that.Category=data[i].Category;
    //       that.Labour=data[i].Labour;
    //       that.Priority=data[i].Priority;
    //     }
    //     }
    //     });
    //     }
    //    });

    }

    timeTill(date) {
      let todaydate:any;
      todaydate=new Date();
      let seconds = Math.floor((todaydate - date) / 1000);
      let interval = Math.floor(seconds / 31536000);
      if (interval > 1) {
        return interval + "y ago";
      }
      interval = Math.floor(seconds / 2592000);
      if (interval > 1) {
        return interval + "m ago";
      }
      interval = Math.floor(seconds / 86400);
      if (interval > 1) {
        return interval + "d ago";
      }
      interval = Math.floor(seconds / 3600);
      if (interval > 1) {
        return interval + "h ago";
      }
      interval = Math.floor(seconds / 60);
      if (interval > 1) {
        return interval +"m ago";
      }
      return Math.floor(seconds) + " seconds";
    } 
  timeSince(date) {
    let todaydate:any;
    todaydate=new Date();
    let seconds = Math.floor((todaydate - date) / 1000);
    let interval = Math.floor(seconds / 31536000);
    if (interval > 1) {
      return interval + "y ago";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + "m ago";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + "d ago";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + "h ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval +"m ago";
    }
    return Math.floor(seconds) + " seconds";
  }
  translationData(data){
    // console.log(data);
          // alert(JSON.stringify(data))

    this.userdata = data
    let menu = [];
    for(let item of data){
      // alert(JSON.stringify(item.priority))
          this.senderimage=item.senderimage;
          this.status=item.status_label;
          this.notification_status=item.notification_status_label;
          this.notiification_description=item.notiification_description;
          this.remarks = item.data.remarks;
          this.priority=item.priority,
               
          this.sendername = item.sendername,
          this.enddate=item.enddate;
          this.createdat=item.createdat;
      let obj = {
        Category:item.Category,
        Labour:item.Labour,
        Priority:item.Priority,
        sendername:item.sendername,
        role:item.role
       }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.sendername=data['sendername'];
          this.Category=data['Category'];
          this.Labour=data['Labour'];
          // this.priority=data['priority'];
          this.role=data['role'];
          });
      } else {
        this.sendername=menu[i].sendername;
          this.Category=menu[i].Category;
          this.Labour=menu[i].Labour;
          // this.priority=menu[i].priority;
          this.role=menu[i].role;
      }
    }
  }




}




export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};

