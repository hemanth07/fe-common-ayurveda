import { FarmeasyTranslate } from './../../../translate.service';
import { Component, OnInit, Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import * as firebase from 'firebase';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PopoverController,LoadingController } from '@ionic/angular';
import { SendtopicpopoverComponent } from '../../../components/usercomponents/sendtopicpopover/sendtopicpopover.component';
import { NavigationExtras } from '@angular/router';
@Component({
  selector: 'appnotifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  user_notifications: any;
  my_notifications: any;
  notifications_data: any;
  error_message: any;
  date: any;
  add_label:any;
  user_data:any;
  constructor(private storage: Storage, private sg: SimpleGlobal, private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation:FarmeasyTranslate,
    private loading:LoadingController) {
    this.date = Date.now();
  }

  ngOnInit() {
    if(this.sg['userdata'].primary_language === 'en'){
        this.add_label = "Add";
        this.error_message = "No Notifications";
      } else if(this.sg['userdata'].primary_language === 'te'){
        this.add_label = "చేర్చు";
        this.error_message = "నోటిఫికేషన్ లేదు";
      } else if(this.sg['userdata'].primary_language === 'ta'){
        this.add_label = "கூட்டு";
        this.error_message = "அறிவிப்பு இல்லை";
      } else if(this.sg['userdata'].primary_language === 'kn'){
        this.add_label = "ಸೇರಿಸಿ";
        this.error_message = "ಸೂಚನೆ ಇಲ್ಲ";
      } else if(this.sg['userdata'].primary_language === 'hi'){
        this.add_label = "जोड़ना"
        this.error_message = "कोई अधिसूचना नहीं";
      }
    this.initilisedata();

  }
  //-------------------------------Retrieveing Of Notifications-----------------------------------//
  async initilisedata() {
    let that = this;
    const loading = await this.loading.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    that.storage.get('farmeasy_userdata').then((val) => {
      console.log('Your age is', val);
      this.user_data = val;
      if (val) {
        let that = this;
        let usersRef = firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications");
        usersRef.on("value", function (Data) {
          let data = snapshotToArray(Data);
          that.notificationdata(data);
          loading.dismiss();
        });
      }
    });
  }

  notificationdata(data) {
    this.notifications_data = data.reverse();
    for (let i = 0; i < this.notifications_data.length; i++) {
      this.notifications_data[i].createdat = this.timeSince(this.notifications_data[i].createdat);
    }
    this.translate(this.notifications_data);
    }
  translate(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj;
      if(item.status && item.notification_status){
        obj = {
          description:item.notiification_description,
          status_label:item.status,
          notification_status_label:item.notification_status
         }
      }
      else {
           obj ={
            description:item.notiification_description,
           }
      }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translation.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          if(menu[i].status_label && menu[i].notification_status_label){
            this.notifications_data[i].notiification_description = data['description'];
            this.notifications_data[i]['status_label'] = data['status_label'];
            this.notifications_data[i]['notification_status_label'] = data['notification_status_label'];
          }
          else {
            this.notifications_data[i].notiification_description = data['description'];
          }
        });
      } else {
        if(menu[i].status_label && menu[i].notification_status_label){
          this.notifications_data[i].notiification_description = menu[i]['description'];
          this.notifications_data[i]['status_label'] = menu[i]['status_label'];
          this.notifications_data[i]['notification_status_label'] = menu[i]['notification_status_label'];
        }
        else {
          this.notifications_data[i].notiification_description = menu[i]['description'];
        }
      }
    }
  }

  //-------------------------------Time Stamp For Notification Time-----------------------------------//


  timeSince(date) {
    let todaydate: any;
    todaydate = new Date();
    let seconds = Math.floor((todaydate - date) / 1000);
    let interval = Math.floor(seconds / 31536000);
    if (interval > 1) {
      return interval + "y ago";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + "m ago";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + "d ago";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + "h ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + "m ago";
    }
    return Math.floor(seconds) + " seconds";
  }

  //-------------------------------Update Notification-----------------------------------//

  updatestatus(data) {
    if (data.key) {
      let that = this;
      that.storage.get('farmeasy_userdata').then((val) => {
        if (val) {
          firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications" + "/" + data.key).update({ 'status': 'Acknowledged', 'createdat': that.date, 'read':true });
        }
      });
    }
  }
  //-------------------------------Update Notification Status-----------------------------------//
  notification_status(data) {
    if (data.key) {
      let that = this;
      that.storage.get('farmeasy_userdata').then((val) => {
        if (val) {
          firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications" + "/" + data.key).update({ 'notification_status': 'Completed', 'createdat': that.date, 'read':true });
        }
      });
    }
  }


  //-------------------------------View Notifications-----------------------------------//
  Viewnotification(notification_key) {
    this.storage.get('farmeasy_userdata').then((val) => {
      if (val) {
        firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications" + "/" +notification_key.key).update({ 'read':true});
      }
    });
    let data = { "notification_key": JSON.stringify(notification_key) };
    this.navCtrl.navigateForward(['/viewnotification', data]);
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};

