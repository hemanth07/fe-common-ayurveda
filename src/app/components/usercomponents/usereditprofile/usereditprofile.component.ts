import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import * as firebase from 'firebase';
import { FormsModule } from '@angular/forms';
import { FarmeasyTranslate } from '../../../translate.service';
import { AlertController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-usereditprofile',
  templateUrl: './usereditprofile.component.html',
  styleUrls: ['./usereditprofile.component.scss']
})
export class UsereditprofileComponent implements OnInit {
  user_deails: boolean = false;defaultHref='';
  editableuser_deails: boolean = true;
  label: any = {};
  user_data: any = {};
  user_name: any;
  user_number: any;
  // b: any;

  user_role: any;
  user_languages: any;
  languages_known:any;
  user_profilepic: any;
  user_dob: any;
  user_address: any;
  user_gender: any;
  url: any;
  newuser_profilepic: any;
  user_blocks: any;
  user_salary:any;
  user_join_date:any;
  user_day_sal:any;
  user_categories:any;
  categoriesSelectFlag:any = [];
  categoriesData:any=[];
  @Output() updateuserdeatils = new EventEmitter();
  user_branch: any;
  user_skills: any;
  addDiseasesFlag: boolean = false;
  addDiseasesFlag1: boolean = true;
  user_email: any;
  constructor(public navCtrl: NavController, 
    private storage: Storage,
    public sg: SimpleGlobal, 
    public alertController: AlertController,
    public translate: FarmeasyTranslate) {
    // this.editableuser_deails=true;
    setTimeout(() => {
      this.storage.get('farmeasy_userdata').then((val) => {
        console.log('Your age is', val);
        this.user_data = val;
        this.user_name = this.user_data.name;
        this.user_number = this.user_data.phone;
        this.user_role = this.user_data.role;
        this.user_dob = this.user_data.dob;
        this.user_address = this.user_data.address;
        this.user_gender = this.user_data.gender;
        this.user_languages = this.user_data.languages;
        this.user_profilepic = this.user_data.profile_url;
        this.user_branch = this.user_data.branch;
        this.user_skills = this.user_data.skills;
        this.user_email = this.user_data.email;

        this.user_blocks = this.user_data.block_responsible;
        this.user_salary = parseInt(this.user_data.salary);
        this.user_join_date = this.user_data.doj;
        this.user_categories = this.user_data.categories;
        this.userDataPrepration();
        this.getCategories();
        this.translation(this.user_data );
      });
    }, 500);
  }
  userDataPrepration(){
    let skils_known = []
    for(let lang of this.user_languages){
      if(lang === 'en'){
        skils_known.push('Writting');
      } else if(lang === 'te'){
        skils_known.push('Foodball');
      } else if(lang === 'ta'){
        skils_known.push('Cricket');
      } else if(lang === 'Kn'){
        skils_known.push('Carrom');
      }  else if(lang === 'Hi'){
        skils_known.push('Mucic');
      }
    }
    let languages_known = []
    for(let lang of this.user_languages){
      if(lang === 'en'){
        languages_known.push('English');
      } else if(lang === 'te'){
        languages_known.push('Telugu');
      } else if(lang === 'ta'){
        languages_known.push('Tamil');
      } else if(lang === 'Kn'){
        languages_known.push('Kannada');
      }  else if(lang === 'Hi'){
        languages_known.push('Hindi');
      }
    
    }
    
    this.languages_known = languages_known.join(" , ");
    this.user_day_sal = Math.round((this.user_salary/30)).toLocaleString();
    this.user_salary = this.user_salary.toLocaleString();

  }
  ngOnInit() {
  }
  chooseFile() {
    document.getElementById('image').click();
  }
  farmeasyfileupload(event) {
    let user_profilepic: any;
    let storageRef = firebase.storage().ref();
    if (event) {
      let selectedFile = event.target.files[0];
      let path = '/files/' + Date.now() + `${selectedFile.name}`;
      let iRef = storageRef.child(path);
      iRef.put(selectedFile).then(() => {
        iRef.getDownloadURL().then(url => this.newuser_profilepic = url)
        this.user_profilepic = this.newuser_profilepic;
      });

    }
  }
  getCategories(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').on('value', resp => {
      this.categoriesSelectFlag = [];
      let categoriesData = snapshotToArray(resp);
      for(let category of categoriesData){
        this.categoriesSelectFlag.push(false);
      }
      console.log(categoriesData);
    for(let i=0;i<categoriesData.length;i++){
      for(let category of this.user_categories){
        if(categoriesData[i].id === category){
          this.categoriesData.push(categoriesData[i]);
          this.categoriesSelectFlag[i] = true;
        }
      }
    }
      });
  }
  dateConverter(date){
    let d = new Date(date)
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = []
    if(this.sg['userdata'].primary_language === 'en'){
      months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
       } else if(this.sg['userdata'].primary_language === 'te'){
        months = ['జనవరి', 'ఫిబ్రవరి', 'మార్చి', 'ఏప్రిల్', 'మే', 'జూన్', 'జూలై', 'ఆగస్టు', 'సెప్టెంబర్', 'అక్టోబర్', 'నవంబర్', 'డిసెంబర్'];
       } else if(this.sg['userdata'].primary_language === 'ta'){
        months = ['ஜனவரி', 'பிப்ரவரி', 'மார்ச்', 'ஏப்ரல்', 'மே', 'ஜூன்', 'ஜூலை', 'ஆகஸ்ட்', 'செப்டம்பர்', 'அக்டோபர்', 'நவம்பர்', 'டிசம்பர்'];
       } else if(this.sg['userdata'].primary_language === 'kn'){
        months = ['ಜನವರಿ', 'ಫೆಬ್ರವರಿ', 'ಮಾರ್ಚ್', 'ಏಪ್ರಿಲ್', 'ಮೇ', 'ಜೂನ್', 'ಜುಲೈ', 'ಆಗಸ್ಟ್', 'ಸೆಪ್ಟೆಂಬರ್', 'ಅಕ್ಟೋಬರ್', 'ನವೆಂಬರ್', 'ಡಿಸೆಂಬರ್']
       } else if(this.sg['userdata'].primary_language === 'hi'){
        months = ["जनवर","फरवरी","मार्च","अप्रैल","मई","जून","जुलाई","अगस्त","सितंबर","अक्टूबर","नवंबर","दिसंबर"];
       } 
     
    return months[month]+" "+year;
  }
  Updateprofile() {
    this.user_deails = true;
    this.editableuser_deails = false;
  }
  calender(){
  this.navCtrl.navigateForward('/employee-calendar')

}
  Done() {
    let that = this;
    this.user_name = this.user_data.name;
    this.user_role = this.user_data.role;
    this.user_languages = this.user_data.languagesuage;
    this.user_profilepic = this.user_data.profile_url;
    this.user_branch = this.user_data.branch;
    this.user_email = this.user_data.email;

   // console.log(that.user_number);
    var query = firebase.database().ref(this.sg['userdata'].vendor_id+'/users/').orderByChild("phone").equalTo(that.user_number);
    query.once("child_added", function (snapshot) {
      if (that.user_name) {
        snapshot.ref.update({ name: that.user_name })
      }
      if (that.user_role) {
        snapshot.ref.update({ role: that.user_role })
      }
      if (that.user_profilepic) {
        snapshot.ref.update({ profile_url: that.user_profilepic })
      }
      if (that.user_languages) {
        snapshot.ref.update({ languagesuage: that.user_languages })
      }
      if (that.user_branch) {
        snapshot.ref.update({ branch: that.user_branch })
      }
      if (that.user_email) {
        snapshot.ref.update({ email: that.user_email })
      }


    });
    that.reviewalert();
  }
  translation(data){
    let temp = data
    let menu = [];
      let obj = {
        // address:data.address,
        gender:data.gender,
        name:data.name,
        role:data.role,
        language: this.languages_known
      }
      menu.push(obj)
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          // this.label['address'] = data['address'];
          this.label['gender'] = data['gender'];
          this.label['name'] = data['name'];
          this.label['role'] = data['role'];
          this.label['language'] = data['language'];
          this.label['branch'] = data['branch'];
          this.label['email'] = data['email'];



          });
      } else {
          this.label['address'] = menu[i]['address'];
          this.label['gender'] = menu[i]['gender'];
          this.label['name'] = menu[i]['name'];
          this.label['role'] = menu[i]['role'];
          this.label['language'] = menu[i]['language'];
          this.label['branch'] = menu[i]['branch'];
          this.label['email'] = menu[i]['email'];


      }
    }
    if(this.sg['userdata'].primary_language === 'en'){
      this.label['blocks_label'] = 'Allotted Blocks';
      this.label['resp'] = 'Responsibilities';
      this.label['block'] = 'Block';
      this.label['compensation'] = 'Compensation';
      this.label['day'] = 'per day';
      this.label['month'] = 'per month';
      this.label['since'] = 'Employed Since';

      this.label['Watering'] = 'Watering';
      this.label['Cleaning'] = 'Cleaning';
      this.label['Fertigation'] = 'Fertigation';
      this.label['Plantation'] = 'Plantation';
      this.label['Other_Tasks'] = 'Other Tasks';
       } else if(this.sg['userdata'].primary_language === 'te'){
        this.label['blocks_label'] = 'కేటాయించిన బ్లాక్స్';
        this.label['resp'] = 'బాధ్యతలు';
        this.label['block'] = 'బ్లాక్';
        this.label['compensation'] = 'పరిహారం';
        this.label['day'] = 'రోజుకు';
        this.label['month'] = 'నెలకు';
        this.label['since'] = 'అప్పటి నుండి ఉద్యోగం';

        this.label['Watering'] = 'నీళ్ళు';
        this.label['Cleaning'] = 'శుభ్రపరచడం';
        this.label['Fertigation'] = 'ఫెర్టిగేషన్';
        this.label['Plantation'] = 'ప్లాంటేషన్';
        this.label['Other_Tasks'] = 'ఇతర పనులు';

       } else if(this.sg['userdata'].primary_language === 'ta'){
        this.label['blocks_label'] = 'ஒதுக்கப்பட்ட தொகுதிகள்';
        this.label['resp'] = 'பொறுப்புகள்';
        this.label['block'] = 'பிளாக்';
        this.label['compensation'] = 'இழப்பீடு';
        this.label['day'] = 'ஒரு நாளைக்கு';
        this.label['month'] = 'மாதத்திற்கு';
        this.label['since'] = 'முதல் பணியாளர்';

        this.label['Watering'] = 'தண்ணீர்';
        this.label['Cleaning'] = 'கிளீனிங்';
        this.label['Fertigation'] = 'உரப்பாசன';
        this.label['Plantation'] = 'தோட்ட';
        this.label['Other_Tasks'] = 'பிற பணிகள்';

       } else if(this.sg['userdata'].primary_language === 'kn'){
        this.label['blocks_label'] = 'ನಿಗದಿಪಡಿಸಿದ ನಿರ್ಬಂಧಗಳು';
        this.label['resp'] = 'ಜವಾಬ್ದಾರಿಗಳನ್ನು';
        this.label['block'] = 'ನಿರ್ಬಂಧಿಸಿ';
        this.label['compensation'] = 'ಪರಿಹಾರ';
        this.label['day'] = 'ದಿನಕ್ಕೆ';
        this.label['month'] = 'ಪ್ರತಿ ತಿಂಗಳು';
        this.label['since'] = 'ರಿಂದ ಉದ್ಯೋಗ';

        this.label['Watering'] = 'ನೀರುಹಾಕುವುದು';
        this.label['Cleaning'] = 'ಸ್ವಚ್ .ಗೊಳಿಸುವಿಕೆ';
        this.label['Fertigation'] = 'ಫಲೀಕರಣ';
        this.label['Plantation'] = 'ನೆಡುತೋಪು';
        this.label['Other_Tasks'] = 'ಇತರ ಕಾರ್ಯಗಳು';

       } else if(this.sg['userdata'].primary_language === 'hi'){
        this.label['blocks_label'] = 'आवंटित ब्लॉक';
        this.label['resp'] = 'जिम्मेदारियों';
        this.label['block'] = 'खंड';
        this.label['compensation'] = 'नुकसान भरपाई';
        this.label['day'] = 'प्रति दिन';
        this.label['month'] = 'प्रति माह';
        this.label['since'] = 'कब से कार्यरत है';

        this.label['Watering'] = 'पानी';
        this.label['Cleaning'] = 'सफाई';
        this.label['Fertigation'] = 'फर्टिगेशन';
        this.label['Plantation'] = 'वृक्षारोपण';
        this.label['Other_Tasks'] = 'अन्य कार्य';

       } else {
        this.label['blocks_label'] = 'Allotted Blocks';
        this.label['resp'] = 'Responsibilities';
        this.label['block'] = 'Block';
        this.label['compensation'] = 'Compensation';
        this.label['day'] = 'per day';
        this.label['month'] = 'per month';
        this.label['since'] = 'Employed Since';

        this.label['Watering'] = 'Watering';
        this.label['Cleaning'] = 'Cleaning';
        this.label['Fertigation'] = 'Fertigation';
        this.label['Plantation'] = 'Plantation';
        this.label['Other_Tasks'] = 'Other Tasks';

      }

  console.log(this.label);
  }

  async reviewalert() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Your Profile is Sucessfully',
      buttons: [
        {
          text: 'OKay',

          cssClass: 'secondary',
          handler: (blah) => {
            this.user_deails = false;
            this.editableuser_deails = true;
          }
        }
      ]
    });
    await alert.present();
  }
  seepic(){
    this.addDiseasesFlag = true;
    this.addDiseasesFlag1 =false
  }
  previewOff(){
    this.addDiseasesFlag = false;
    this.addDiseasesFlag1 =true

  }
  backto_dashboard() {
    this.navCtrl.navigateRoot('/farm-dashboard');

  }
  nofication_count() {
    let data = { "page_status": JSON.stringify("dashboard") }
    this.navCtrl.navigateForward(['/notifications', data]);
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};