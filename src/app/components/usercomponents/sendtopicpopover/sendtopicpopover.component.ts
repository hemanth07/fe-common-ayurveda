import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import {  PopoverController} from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global'
import { Http, Headers, RequestOptions } from '@angular/http';
import { shareReplay } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-sendtopicpopover',
  templateUrl: './sendtopicpopover.component.html',
  styleUrls: ['./sendtopicpopover.component.scss']
})
export class SendtopicpopoverComponent implements OnInit {
  Users:any;
  description:any;
  selectedtopic:any;
  labels:any = {};
  constructor(private http: Http,private popoverController: PopoverController,public sg:SimpleGlobal) { }

  ngOnInit() {
    if(this.sg['userdata'].primary_language === 'en'){
       this.labels['topic'] = "Add Topic";
       this.labels['description'] = "Description";
       this.labels['send'] = "Send";
      } else if(this.sg['userdata'].primary_language === 'te'){
       this.labels['topic'] = "విషయాన్ని జోడించు";
       this.labels['description'] = "వివరణ";
       this.labels['send'] = "పంపండి";
      } else if(this.sg['userdata'].primary_language === 'ta'){
       this.labels['topic'] = "தலைப்பு சேர்க்கவும்";
       this.labels['description'] = "விளக்கம்";
       this.labels['send'] = "அனுப்புக";
      } else if(this.sg['userdata'].primary_language === 'kn'){
       this.labels['topic'] = "ವಿಷಯ ಸೇರಿಸಿ";
       this.labels['description'] = "ವಿವರಣೆ";
       this.labels['send'] = "ಕಳುಹಿಸು";
      } else if(this.sg['userdata'].primary_language === 'hi'){
        this.labels['topic'] = "विषय जोड़ें";
       this.labels['description'] = "विवरण";
       this.labels['send'] = "भेजना";
      }
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').on('value', resp => {
      let that = this;
      const data = snapshotToArray(resp);
    //  console.log(data);
     // console.log(data.length);
      if (data) {
          that.Users = data
       //   console.log(that.Users);
      }
  });
  }
  sendtopic(){
  //  console.log(this.description)
  //  console.log(this.selectedtopic)

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
 
      let body = {
   "data":"sample message","topic":"labour"
      }
      let options = new RequestOptions({
        headers: headers
      });
      this.http.post("http://104.237.2.124:8091/farmeasy/sendtotopic", body, options)
        .pipe(map(res => res.json()))
        .subscribe(data => {
      
          this.popoverController.dismiss();
        },
        err => {
          this.popoverController.dismiss();

        });



       



  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};


