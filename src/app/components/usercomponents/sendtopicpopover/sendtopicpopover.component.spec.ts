import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendtopicpopoverComponent } from './sendtopicpopover.component';

describe('SendtopicpopoverComponent', () => {
  let component: SendtopicpopoverComponent;
  let fixture: ComponentFixture<SendtopicpopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendtopicpopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendtopicpopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
