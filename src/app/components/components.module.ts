import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormEasyLoginComponent } from './farm-easy-login/farm-easy-login.component';
import { IonicModule } from '@ionic/angular';
import { FarmeasygalleryComponentComponent } from './farmeasygallery-component/farmeasygallery-component.component';
import { FarmeasymodulesComponent } from './farmeasymodules/farmeasymodules.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { FarmeasyForgotPasswordComponent } from './farmeasy-forgot-password/farmeasy-forgot-password.component';
import { ContactComponent } from './contact/contact.component';
@NgModule({
  declarations: [FormEasyLoginComponent, FarmeasygalleryComponentComponent, FarmeasymodulesComponent, ProgressBarComponent, FarmeasyForgotPasswordComponent, ContactComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot(),
  ],
  entryComponents:[ProgressBarComponent,FarmeasyForgotPasswordComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports: [FormEasyLoginComponent, FarmeasymodulesComponent,FormsModule,
            FarmeasygalleryComponentComponent,ProgressBarComponent,FarmeasyForgotPasswordComponent]
})
export class ComponentsModule { }