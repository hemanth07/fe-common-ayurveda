import { FarmeasyTranslate } from '../../../translate.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as firebase from 'firebase';
import { NavController, PopoverController,NavParams,Platform } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import * as  jsPDF from 'jspdf';
import domtoimage from 'dom-to-image';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';

import { KanadaService } from '../../../languages/kanada.service';
import { TeluguService } from '../../../languages/telugu.service';
import { TamilService } from '../../../languages/tamil.service';
import { EnglishService } from '../../../languages/english.service';
import { HindiService } from '../../../languages/hindi.service';
import { ActivatedRoute } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-sales-invoice-download',
  templateUrl: './sales-invoice-download.component.html',
  styleUrls: ['./sales-invoice-download.component.scss']
})
export class SalesInvoiceDownloadComponent implements OnInit {
  date:any;n
  harvest_outdata:any={};
  total:any;
  finaltotol:any;
  total_in_words:any;
  countinwords:any;
  labels: any = [];
  amount:number = 0;
  headerData: any;
  @ViewChild('invoice') invoice;
  constructor( public navCtrl: NavController,
    private popoverController: PopoverController,
    public navParams: NavParams,
    private sg: SimpleGlobal,
    private route: ActivatedRoute,
    private translate: FarmeasyTranslate,
    private file: File,
    private fileOpener: FileOpener,
    private en: EnglishService,
    private hi: HindiService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService,
    private platform:Platform,
    private socialSharing:SocialSharing) {
    this.sg['select_harvesttype'] = "Harvest Out";
    let monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    this.date=new Date().getFullYear()+"-"+monthNames[(new Date().getMonth()+1)]+"-"+new Date().getDate();
    this.initilisedata();
    this.translation();
}

ngOnInit(){

}
initilisedata() {
  this.harvest_outdata = JSON.parse(this.navParams.get("receipt"));
  console.log(this.harvest_outdata);
   this.calaculateRate(this.harvest_outdata.record);
}
translation(){
  if(this.sg['userdata'].primary_language === 'en'){
    this.labels  = this.en.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'te'){
    this.labels  = this.te.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'ta'){
    this.labels  = this.ta.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'kn'){
    this.labels  = this.kn.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'hi'){
    this.labels  = this.hi.getHarvestOutInvoice();
  } else {
     this.labels  = this.en.getHarvestOutInvoice();
  }
}
calaculateRate(data){
  for(let record of data){
    this.amount += record.Rate;
  }
}
cancel(){
  this.popoverController.dismiss()
}

    

    toWords(s) {
      let  th = ['', ' thousand', ' million', ' billion', ' trillion', ' quadrillion', ' quintillion'];
      let dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
      let tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
      let tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

      s = s.toString();
      s = s.replace(/[\, ]/g, '');
      if (s != parseFloat(s)) return 'not a number';
      var x = s.indexOf('.');
      if (x == -1) x = s.length;
      if (x > 15) return 'too big';
      var n = s.split('');
      this.countinwords = '';
      var sk = 0;
      for (var i = 0; i < x; i++) {
          if ((x - i) % 3 == 2) {
              if (n[i] == '1') {
                this.countinwords  += tn[Number(n[i + 1])] + ' ';
                  i++;
                  sk = 1;
              } else if (n[i] != 0) {
                this.countinwords  += tw[n[i] - 2] + ' ';
                  sk = 1;
              }
          } else if (n[i] != 0) {
            this.countinwords  += dg[n[i]] + ' ';
              if ((x - i) % 3 == 0)  this.countinwords  += 'hundred ';
              sk = 1;
          }
          if ((x - i) % 3 == 1) {
              if (sk)  this.countinwords  += th[(x - i - 1) / 3] + ' ';
              sk = 0;
          }
      }
     return  this.countinwords.replace(/\s+/g, ' ');
  }
  shareVia() {
    if(this.platform.is('ios')) {
      this.socialSharing.share('“Invoice Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
      .then(()=>{
         // alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    } else {
      this.socialSharing.share('“Invoice Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
      .then(()=>{
     //     alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    }
  }
  Print() {
    // this.presentLoading('Creating PDF file...');
    const div = document.getElementById("Invoice");
    const options = { background: "white", width: div.clientWidth, height: div.clientHeight };
    domtoimage.toPng(div, options).then((dataUrl)=> {
      // Initialize JSPDF
      var doc = new jsPDF("p","mm","a4");
      // Add image Url to PDF
      doc.addImage(dataUrl, 'PNG', 20, 20, 240, 180);
  
      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (var i = 0; i < pdfOutput.length; i++) {
          array[i] = pdfOutput.charCodeAt(i);
      }
      // This is where the PDF file will stored , you can change it as you like
      // for more information please visit https://ionicframework.com/docs/native/file/
      const directory = this.file.dataDirectory ;
      const fileName = "Harvestoutvoice.pdf";
      let options: IWriteOptions = { replace: true };
  
      this.file.checkFile(directory, fileName).then((success)=> {
        // Writing File to Device
        this.file.writeFile(directory,fileName,buffer, options)
        .then((success)=> {
         this.popoverController.dismiss();
         this.navCtrl.navigateRoot('/farm-dashboard');
  
          console.log("File created Succesfully" + JSON.stringify(success));
          this.fileOpener.open(this.file.dataDirectory + fileName, 'application/pdf')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
        })
        .catch((error)=> {
         this.popoverController.dismiss();
         this.navCtrl.navigateRoot('/farm-dashboard');
  
        //  console.log("Cannot Create File " +JSON.stringify(error));
        });
      })
      .catch((error)=> {
        // Writing File to Device
        this.file.writeFile(directory,fileName,buffer)
        .then((success)=> {
         this.popoverController.dismiss();
         this.navCtrl.navigateRoot('/farm-dashboard');
         // console.log("File created Succesfully" + JSON.stringify(success));
          this.fileOpener.open(this.file.dataDirectory + fileName, 'application/pdf')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
        })
        .catch((error)=> {
         this.popoverController.dismiss();
         this.navCtrl.navigateRoot('/farm-dashboard');
          console.log("Cannot Create File " +JSON.stringify(error));
        });
      });
    })
    .catch(function (error) {
     this.popoverController.dismiss();
     this.navCtrl.navigateRoot('/farm-dashboard');
      console.error('oops, something went wrong!', error);
    });
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};