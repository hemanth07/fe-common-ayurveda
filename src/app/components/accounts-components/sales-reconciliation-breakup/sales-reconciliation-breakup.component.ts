import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, NavParams,Platform,AlertController} from '@ionic/angular';
import * as firebase from 'firebase';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SimpleGlobal } from "ng2-simple-global";

@Component({
  selector: 'app-sales-reconciliation-breakup',
  templateUrl: './sales-reconciliation-breakup.component.html',
  styleUrls: ['./sales-reconciliation-breakup.component.scss']
})
export class SalesReconciliationBreakupComponent implements OnInit {
  receipt:any;
  amount:number;
  payments:any = [];
  zero_balance:boolean = false;
  constructor(public navParams: NavParams,
              public popoverController:PopoverController,
              private socialSharing:SocialSharing,
              public alertController: AlertController,
              public sg:SimpleGlobal,
              public platform:Platform) { }

  ngOnInit() {
    this.receipt = JSON.parse(this.navParams.get("receipt"));
    this.balanceCalaculate(this.receipt.record);
    console.log(this.receipt);
  }
  share(){
    if(this.platform.is('ios')) {
      this.socialSharing.share('“Invoice Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
      .then(()=>{
         // alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    } else {
      this.socialSharing.share('“Invoice Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
      .then(()=>{
     //     alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    }
  }
  cancel(){
    this.popoverController.dismiss();
  }
  async save(){
    if(this.amount>0 && this.amount<=this.receipt.total_balance){
      let date = new Date().toISOString().split('T')[0];
      firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out/'+this.receipt.key+'/payments').push({date:date,amount:this.amount});
      if(this.receipt['total_balance']- this.amount <= 0 ){
        this.zero_balance = true;
        firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out/'+this.receipt.key).update({payment_status:'Paid'});
      }
      this.cancel();
    } else {
      const alert = await this.alertController.create({
      header: 'Error',
      subHeader: 'Please enter proper amount',
      buttons: ['OK']
    });
    await alert.present();
    }
  }
  balanceCalaculate(data){
    let amount = 0;
    for(let item of data){
        amount = amount + item.Rate
    }
    this.receipt['total_amount'] = amount;
    if(this.receipt.payments){
    this.convertObjectToArray(this.receipt.payments);
    } else {
      this.receipt['total_payment'] = 0;
      this.receipt['total_balance'] = amount;
    }
  }
  convertObjectToArray(obj){
    let array1 = Object.entries(obj);
   let original_array = [];
    for(let array2 of array1){
        let obj1 = array2[1];
        obj1['key'] = array2[0]
           original_array.push(obj1);
     }
   console.log(original_array);
   this.prepareTable(original_array);
   return original_array;
  }
  prepareTable(data){
    let amount = 0;
    let balance = 0;
    for(let i=0;i<data.length;i++) {
      amount += parseInt(data[i].amount);
      balance = this.receipt['total_amount'] - amount;
      let temp = { date:data[i].date,receipt:amount,balance:balance}
      this.payments.push(temp);
    }
    if(balance <= 0){
      this.zero_balance = true;
    }
    this.receipt['total_payment'] = amount;
    this.receipt['total_balance'] = balance;
    console.log(this.receipt);
    console.log(this.payments);
  }
  calaculateAmount(data){
    let amount = 0;
    for(let item of data){
        amount = amount + item.Rate
    }
    return amount.toLocaleString();
  }
  dateConverter(date){
    let d = new Date(date)
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    this.receipt['date'] =  day+"-"+months[month]+"-"+year;
    return day+"-"+months[month]+"-"+year;
  }


}
