import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SalesReceiptComponent } from './sales-receipt/sales-receipt.component';
import { HttpModule } from '@angular/http';
import { SalesInvoiceDownloadComponent } from './sales-invoice-download/sales-invoice-download.component';
import { SalesReconciliationBreakupComponent } from './sales-reconciliation-breakup/sales-reconciliation-breakup.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(),
  ],
  declarations: [SalesReceiptComponent, SalesInvoiceDownloadComponent, SalesReconciliationBreakupComponent,
    
     ],
  exports: [SalesReceiptComponent,SalesInvoiceDownloadComponent,SalesReconciliationBreakupComponent
    
    ],
  entryComponents: [
    SalesReceiptComponent,
    SalesInvoiceDownloadComponent,
    SalesReconciliationBreakupComponent
  ],
})
export class AccountsComponentsModule {}
