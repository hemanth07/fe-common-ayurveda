import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, NavParams,Platform} from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-sales-receipt',
  templateUrl: './sales-receipt.component.html',
  styleUrls: ['./sales-receipt.component.scss']
})
export class SalesReceiptComponent implements OnInit {
  receipt:any;
  message:any;
  constructor(public popoverController:PopoverController,
              public navParams: NavParams,
              public platform:Platform,
              public socialSharing:SocialSharing) { }

  ngOnInit() {
    this.receipt = JSON.parse(this.navParams.get("receipt"));
    console.log(this.receipt);
    this.calaculateAmount(this.receipt.record);
    this.dateConverter(this.receipt.Date_of_Transaction);
    this.message = "We confirm the receipt of Rs "+this.receipt['amount']+" from "+ this.receipt.customer_name +" towards the pending dues from the invoice on "+ this.receipt.date+" via online transfer."
  }
  cancel(){
    this.popoverController.dismiss()
  }
  calaculateAmount(data){
    let amount = 0;
    for(let item of data){
        amount = amount + item.Rate
    }
    this.receipt['amount'] = amount.toLocaleString();
    return amount.toLocaleString();
  }

  dateConverter(date){
    let d = new Date(date)
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    this.receipt['date'] =  day+"-"+months[month]+"-"+year;
    return day+"-"+months[month]+"-"+year;
  }

  shareVia() {
    if(this.platform.is('ios')) {
      this.socialSharing.share(this.message,null,null,null)
      .then(()=>{
         // alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    } else {
      this.socialSharing.share(this.message,null,null,null)
      .then(()=>{
     //     alert('Done');
      }).catch((err)=>{
        alert('error :'+JSON.stringify(err));
      });
    }
   
    }
}
