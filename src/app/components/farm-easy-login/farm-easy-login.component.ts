import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, AlertController,PopoverController, LoadingController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TamilService } from './../../languages/tamil.service';
import { TeluguService } from './../../languages/telugu.service';
import { EnglishService } from './../../languages/english.service';
import { HindiService } from './../../languages/hindi.service';
import { Router } from '@angular/router';
import { FarmeasyForgotPasswordComponent } from '../farmeasy-forgot-password/farmeasy-forgot-password.component';
import { OtherService } from 'src/app/languages/others.service';
@Component({
  selector: 'farm-easy-login',
  templateUrl: './farm-easy-login.component.html',
  styleUrls: ['./farm-easy-login.component.scss']
})
export class FormEasyLoginComponent {
  Farm_password; 
  private currentColor: string
  usernumber: any;
  usernum_errormessage: boolean = true;
  userpassword: any;
  profile_pic: any;
  user_number_errmsg: boolean = true;
  users: any;
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";

  constructor(private sg: SimpleGlobal,
    public translate: FarmeasyTranslate,
    public navCtrl: NavController,
    public alertController: AlertController,
    public toastController: ToastController,
    private popoverController: PopoverController,
    private storage: Storage,
    private router:Router,
    private menu: MenuController,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService,
    private hi:HindiService,
    private other:OtherService,
    private loading:LoadingController) {
    this.storage.get('user_profilepic').then((val) => {
      this.profile_pic = val;
    });
    this.storage.remove("farmeasy_userdata");
    }

    //---------------------------- Users List--------------------------------//

  ngOnInit() {
    this.menu.swipeEnable(false, 'menu_close');
    // firebase.database().ref('users/').on('value', resp => {
    //   this.users = [];
    //   this.users = snapshotToArray(resp);
    // });
  }

  //---------------------------- Chnage Event For Number --------------------------------//

  onchnageusernumber(event) {
   // console.log(event);
    this.usernumber = event.target.value;
    var re = new RegExp("^((\\+91-?)|0)?[0-9]{10}$");
    if (re.test(this.usernumber)) {
   //   console.log("Valid");
      this.usernum_errormessage = true;
    }
    else {
     // console.log("Invalid");
      this.usernum_errormessage = false;
    }
  }

    //---------------------------- Chnage Event For Password --------------------------------//

  onchnageuserpassword(event) {
    // console.log(event);
    this.userpassword = event.target.value;
  }


    //---------------------------- Navigation To Dashboard --------------------------------//


 async Farmdashboard() {
    const loading = await this.loading.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    let that = this;
    if (typeof that.usernumber == 'undefined' && typeof that.userpassword == 'undefined') {
      that.user_number_errmsg = false;
      loading.dismiss();
    }
    else {
      if (that.usernum_errormessage == true && that.userpassword) {
        that.user_number_errmsg = true;
        firebase.database().ref('wellnest/users').orderByChild('phone').equalTo(this.usernumber).once('value', resp => {
          let users = snapshotToArray(resp);
          this.users = users.filter(users => users.phone == this.usernumber && users.password == this.userpassword);
          loading.dismiss();
          if (this.users && this.users.length == 1) {
          this.validation(this.users[0].farmeasy_id);
          } else{
          this.confirmation();
          }
        });
       
      } else {
        loading.dismiss();
      }
    }
  }
  validation(farmeasy_id){
    if (this.users && this.users.length == 1) {
      firebase.database().ref(this.users[0].vendor_id+"/users").orderByChild('farmeasy_id').equalTo(farmeasy_id).once('value', async resp => {
      let users = snapshotToArray(resp);
      if (users[0].primary_language !== 'en') {
        let payload = { name : users[0].name, address:users[0].address }
        await this.translate.translateObject(payload,'en',users[0].primary_language).then(data => {
          users[0].name = data['name'];
          users[0].address = data['address'];
        });
      } 
      this.storage.set('farmeasy_userdata', users[0]);
      this.sg["userdata"] = users[0];
      console.log(this.sg["userdata"]);
      let hamburger;
      let dashboard;
      if(this.sg['userdata'].primary_language === 'en'){
        hamburger = this.en.getHamburger();
        dashboard = this.en.getDashboard();
      } else if(this.sg['userdata'].primary_language === 'te'){
        hamburger = this.te.getHamburger();
        dashboard = this.te.getDashboard();
      } else if(this.sg['userdata'].primary_language === 'ta'){
        hamburger = this.ta.getHamburger();
        dashboard = this.ta.getDashboard();
      } else if(this.sg['userdata'].primary_language === 'kn'){
        hamburger = this.kn.getHamburger();
        dashboard = this.kn.getDashboard();
      } else if(this.sg['userdata'].primary_language === 'hi'){
        hamburger = this.hi.getHamburger();
        dashboard = this.hi.getDashboard();
      } else {
        hamburger = this.en.getHamburger();
        dashboard = this.en.getDashboard();
      }
      this.storage.set('hamburger', hamburger);
      this.storage.set('dashboard', dashboard);
      this.sg["hamburger"] = hamburger;
      this.sg["dashboard"] = dashboard;
     // console.log(this.sg['hamburger']);
      this.initilisedata();
      this.storage.set('user_profilepic', users[0].profile_url);
      this.storage.get('user_profilepic').then((val) => {
        this.profile_pic = val;
      });
    });
   }
  }
     //---------------------------- User Notification Count --------------------------------//


  initilisedata() {
    if (this.sg["userdata"] && this.sg["userdata"].key) {
      firebase.database().ref(this.sg["userdata"].vendor_id+"/users/" + this.sg["userdata"].key + "/notifications").on('value', resp => {
        let that = this;
        const data = snapshotToArray(resp);
       // console.log(data);
       // console.log(data.length);
        if (data && data.length) {
          let temp_len = data.filter(item => item.read !== true);
          this.sg['notificationscount'] = temp_len.length;
          }
      });
      this.router.navigateByUrl('/farm-dashboard');
      // this.navCtrl.navigateRoot('/my-tasks');
      //  this.navCtrl.navigateRoot('/farm-dashboard');
    }
  }

 async forgotPassword(){
         const popover = await this.popoverController.create({
        component: FarmeasyForgotPasswordComponent,
        translucent: true,
        backdropDismiss:false,
        cssClass: "popover_class",
      });
      await popover.present();
  }


  async confirmation() {
    const alert = await this.alertController.create({
      message: 'Credentials are not matched',
      buttons: ['OK']
    });
    await alert.present();
  }
}


export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};

