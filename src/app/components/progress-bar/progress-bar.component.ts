import { Component, OnInit } from '@angular/core';
import { PopoverController,NavParams } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  source:any;
  constructor(public popoverController: PopoverController,
              public sg: SimpleGlobal,
              private navParams:NavParams) { }

  ngOnInit() {
  this.source = this.navParams.get('source');
  console.log(this.source);
  }

  closePopup(){
    this.popoverController.dismiss("Close");
  }
  bytesToSize(bytes) {
    // bytes = parseInt(bytes);
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = Math.floor(Math.log(bytes) / Math.log(1024));
   return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
}

}
