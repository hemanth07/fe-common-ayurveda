import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-view-document',
  templateUrl: './view-document.component.html',
  styleUrls: ['./view-document.component.scss']
})
export class ViewDocumentComponent implements OnInit {
  Records: any = [];
  isImage: any = [];
  constructor(private popoverController: PopoverController, public navParams: NavParams) { }

  ngOnInit() {
    this.Records = this.navParams.get('Record');
    console.log(this.Records);
    if(this.Records.attachment){
      for(let attachment of this.Records.attachment){
        let file_name = attachment.path.name.substring(attachment.path.name.lastIndexOf("/")+1);
        let ext = file_name.split(".")[1];
        if(ext == 'jpeg' || ext == 'png' || ext == 'jpg' || ext == 'svg' || ext == undefined){
          this.isImage.push(true);
        } else {
          this.isImage.push(false);
        }
      }
    }
  }
  sourcePopOverClose() {
    this.popoverController.dismiss();
  }
}
