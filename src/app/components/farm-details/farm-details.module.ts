import { ViewDocumentComponent } from './view-document/view-document.component';
import { CommonModule } from '@angular/common';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [ViewDocumentComponent],
  imports: [
    CommonModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports:[ViewDocumentComponent],
  entryComponents : [ViewDocumentComponent]
})
export class FarmDetailsModule { }
