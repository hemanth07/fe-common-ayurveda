import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ReviewpopoverComponent } from './reviewpopover/reviewpopover.component';
import { CalendercomponentComponent } from './calendercomponent/calendercomponent.component';
import { FormsModule } from '@angular/forms';
import { PrintPopupComponent } from './print-popup/print-popup.component';

@NgModule({
declarations: [ ReviewpopoverComponent, CalendercomponentComponent, PrintPopupComponent],
imports: [
CommonModule,IonicModule.forRoot(),FormsModule,
],
exports: [ReviewpopoverComponent,CalendercomponentComponent,PrintPopupComponent],
entryComponents: [ReviewpopoverComponent,CalendercomponentComponent,PrintPopupComponent]
})
export class HarvestcomponentsModule { }
