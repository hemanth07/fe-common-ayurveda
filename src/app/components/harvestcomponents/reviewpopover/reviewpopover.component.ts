import { Component, OnInit } from "@angular/core";
import { NavParams, PopoverController, ModalController, NavController, ToastController} from "@ionic/angular";
import * as firebase from "firebase";
import { AlertController } from "@ionic/angular";
import { SimpleGlobal } from "ng2-simple-global";
import {Location} from '@angular/common';
import { KanadaService } from "../../../languages/kanada.service";
import { TeluguService } from "../../../languages/telugu.service";
import { TamilService } from "../../../languages/tamil.service";
import { EnglishService } from "../../../languages/english.service";
import { HindiService } from "../../../languages/hindi.service";
@Component({
  selector: "app-reviewpopover",
  templateUrl: "./reviewpopover.component.html",
  styleUrls: ["./reviewpopover.component.scss"]
})
export class ReviewpopoverComponent implements OnInit {
  harvest_data: any = {};
  harvest_outdata: any = {};
  type: any;
  my_lang: string;
  labels: any = [];
  farmeasy_har_in:any;
  farmeasy_hat_out:any;
  farmeasy_purchase_in:any;

  constructor(
    public navParams: NavParams,
    private popoverController: PopoverController,
    public modalController: ModalController,
    public alertController: AlertController,
    public navCtrl: NavController,
    public sg: SimpleGlobal,
    private en: EnglishService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService,
    private hi: HindiService,
    private location: Location,
    public toastController: ToastController
  ) {
    this.farmeasy_har_in = firebase.database().ref(this.sg['userdata'].vendor_id+"/harvest/harvest_in");
    this.farmeasy_hat_out = firebase.database().ref(this.sg['userdata'].vendor_id+"/harvest/harvest_out");
    this.farmeasy_purchase_in = firebase.database().ref(this.sg['userdata'].vendor_id+"/harvest/purchase_in");
    this.harvest_data = this.navParams.get("harvest_data");
   // console.log(this.harvest_data);
    this.type = this.navParams.get("type");
   // console.log(this.type);
    if (this.sg["userdata"].primary_language !== "en") {
      this.my_lang = this.sg["userdata"].primary_language;
    }
  }

  ngOnInit() {
    this.translation();
  }
  translation() {
    if (this.sg["userdata"].primary_language === "en") {
      this.labels = this.en.getHarvestReviewPopup();
    } else if (this.sg["userdata"].primary_language === "te") {
      this.labels = this.te.getHarvestReviewPopup();
    } else if (this.sg["userdata"].primary_language === "ta") {
      this.labels = this.ta.getHarvestReviewPopup();
    } else if (this.sg["userdata"].primary_language === "kn") {
      this.labels = this.kn.getHarvestReviewPopup();
    } else if (this.sg["userdata"].primary_language === "hi") {
      this.labels = this.hi.getHarvestReviewPopup();
    } else {
      this.labels = this.en.getHarvestReviewPopup();
    }
  }
  requestcancel() {
    this.location.back();
    this.popoverController.dismiss();
  }

  requestok() {
    let network: any;
    let that = this;
    var connectedRef = firebase.database().ref(".info/connected");
    connectedRef.on("value", function(snap) {
     // console.log(snap.val());

      network = snap.val();
      if (network == true) {
        that.networkonline();
      } else {
        that.networkoffline();
      }
    });
  }
  networkonline() {
    if (this.type == "Harvest In") {
      let newData = this.farmeasy_har_in.push(this.harvest_data);
    }
    if (this.type == "Harvest Out") {
      let newData = this.farmeasy_hat_out.push(this.harvest_data);
    }
    if (this.type == "Purchase In") {
      let newData = this.farmeasy_purchase_in.push(this.harvest_data);
    }
    this.popoverController.dismiss();
    this.reviewalert();
  }
  networkoffline() {
    
    this.popoverController.dismiss();
   
  }
  async reviewalert() {
    const alert = await this.alertController.create({
      header: "Confirm!",
      message: "Review is Sucessfull",
      buttons: [
        {
          text: "Submit",
          cssClass: "secondary",
          handler: blah => {
            if (this.type == "Harvest Out") {
              this.presentToast("Harvested Out");
              this.location.back();
             // this.navCtrl.navigateForward("/harvestout-voice");
            }
            if (this.type == "Harvest In") {
              this.presentToast("Harvested In");
             this.location.back();
            }
            if (this.type == "Purchase In") {
              this.presentToast("Purchase In");
             this.location.back();
            }
          }
        }
      ]
    });
    await alert.present();
  }
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message +" Successful",
      duration: 2000
    });
    toast.present();
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
