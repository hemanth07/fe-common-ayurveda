import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from '@angular/router';
import { Component,Input, OnInit } from '@angular/core';
import { NavController,NavParams, AlertController, PopoverController, } from '@ionic/angular';
@Component({
  selector: 'app-calendercomponent',
  templateUrl: './calendercomponent.component.html',
  styleUrls: ['./calendercomponent.component.scss']
})
export class CalendercomponentComponent implements OnInit {
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  eventList: any;
  selectedEvent: any;
  isSelected: any;
  selectedDate:any;
  showOk:boolean=true;
  showCurrentDateBackground: any = [];
  datss:any;
  today_date:any;
  blockpastdates: any = [];
  month:string;
  year:any;
  previousflag:boolean;
  // @Input("harvesttype") harvesttype;

  constructor(private alertCtrl: AlertController, public navParams: NavParams,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private sg: SimpleGlobal,
    private route:ActivatedRoute) { }

  ngOnInit() {
   this.today_date = this.navParams.get('startDate');
   this.previousflag = this.navParams.get("previous");
   this.datss="nummm";
   this.date = new Date();
   this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
   this.getDaysOfMonth();

  }
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.month = this.monthNames[new Date().getMonth()]
    this.currentYear = this.date.getFullYear();
    this.year = new Date().getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (var j = 0; j < thisNumOfDays; j++) {
      this.blockpastdates[j]=false;
      this.daysInThisMonth.push(j + 1);
      this.showCurrentDateBackground[j] = false;
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    // var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var k = 0; k < (6 - lastDayThisMonth); k++) {
      this.daysInNextMonth.push(k + 1);
    }
    var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (var l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
        this.daysInNextMonth.push(l);
      }
    }
    this.preFillDate();
  }
  preFillDate(){
  if (this.today_date) {
      var d1 = new Date(this.today_date);
      for (var i = 0; i < this.daysInThisMonth.length; i++) {
        if(this.currentDate>this.daysInThisMonth[i] && this.currentMonth === this.month){
           this.blockpastdates[i]=true;
          }
          else{
            this.blockpastdates[i]=false;
          }
        if (this.daysInThisMonth[i] == d1.getDate() && this.date.getMonth() + 1 == d1.getMonth() + 1 &&
          d1.getFullYear() == this.date.getFullYear()) {
          this.showCurrentDateBackground[i] = true;
          this.showOk=false;
        }
      }
     // console.log(this.showCurrentDateBackground);
  }
}

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
  }
  selectDate(day,idx,month,year) {
    if(this.blockpastdates[idx] === false){
      var month1:any;
      var thisDate2 = this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day;
     // console.log(thisDate2);
      for (var i = 0; i < this.daysInThisMonth.length; i++) {
        if (i == idx) {
          if (this.showCurrentDateBackground[i] == false) {
            this.showCurrentDateBackground[i] = true;
            for (var j = 0; j < this.monthNames.length; j++) {
              if (this.monthNames[j] == month) {
                month1 = j + 1;
                this.selectedDate =year  + "-" + this.checkZero(month1) + "-" + this.checkZero(day);
              }
            }
            this.showOk = false;
          }
          else {
            this.showCurrentDateBackground[i] = false;
            this.selectedDate = '';
            this.showOk = true;
          }
        }
        else {
          this.showCurrentDateBackground[i] = false;
        }
      }
    }
    // if (thisDate2) {
    //   this.popoverController.dismiss(thisDate2)
    // }
  }
  checkZero(number){
    return  number<10?'0'+number:number
  }
  popOverDateOk(){
      //this.endDate = this.selectedDate;
      this.selectedDate=[];
      var month1: any;
      for(var i=0;i<this.daysInThisMonth.length;i++){
        if(this.showCurrentDateBackground[i]==true){
        for (var j = 0; j < this.monthNames.length; j++) {
          if (this.monthNames[j] == this.currentMonth) {
            month1 = j + 1;
            // if(month1<10){
            //   month1 = '0'+month1;
            // }
            // if(this.daysInThisMonth[i]<10){
            //   this.daysInThisMonth[i] = '0'+this.daysInThisMonth[i];
            // }
              this.selectedDate=  this.currentYear + "-" + this.checkZero(month1) + "-" +  this.checkZero(this.daysInThisMonth[i]);
          }
        }
      }
    }
   // console.log(this.selectedDate);
  
    this.popoverController.dismiss( this.selectedDate);
  }
  popOverDateCancel(){
    this.popoverController.dismiss();
  }
}
