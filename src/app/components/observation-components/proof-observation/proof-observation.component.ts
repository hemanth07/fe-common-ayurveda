import { Component, OnInit, Output, EventEmitter, NgZone } from '@angular/core';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import * as firebase from 'firebase';
import { LoadingController,PopoverController } from '@ionic/angular';
import { ImagePicker,   ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {SimpleGlobal} from 'ng2-simple-global';
import { ProgressBarComponent } from './../../progress-bar/progress-bar.component';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'farmeasy-proof-observation',
  templateUrl: './proof-observation.component.html',
  styleUrls: ['./proof-observation.component.scss']
})
export class ProofObservationComponent implements OnInit {
  date: any = [];
  location:any;
  @Output() address: EventEmitter<any> = new EventEmitter();
  constructor(public file: File,
    public media: Media,
    public mediaCapture: MediaCapture,
    public loadingCtrl: LoadingController,
    public popoverController: PopoverController,
    private camera: Camera,
    public imagePicker: ImagePicker,
    private geolocation: Geolocation,
    public plt: Platform,
    private zone:NgZone,
    public sg:SimpleGlobal) { }

  ngOnInit() {
    this.date = new Date().toString();
  }
  getLangLatValues(){
    this.geolocation.getCurrentPosition().then((resp) => {
        let location = {latitude:resp.coords.latitude,longitude:resp.coords.longitude}
        this.location = location
        }).catch((error) => {
         alert('Error getting location' + error);
       });
    }
 async captureImage() {
  this.getLangLatValues();
  let  urls;
const options: CameraOptions = {
  quality: 20,
  destinationType: this.camera.DestinationType.DATA_URL,
  encodingType: this.camera.EncodingType.JPEG,
  mediaType: this.camera.MediaType.PICTURE
}
this.sg['uploadProgress'] = 1
this.sg['transferBytes'] = 0
this.sg['totalBytes'] = 0
    //           const popover = await this.popoverController.create({
    //         component: ProgressBarComponent,
    //         translucent: true,
    //         backdropDismiss:false,
    //         cssClass:'progress-bar-popover',
    //         componentProps: {
    //      'source':'Image'
    //     },
    //       });
    //   await popover.present();
    //  popover.onDidDismiss().then(resp =>{
    //         if(resp.data == 'Close'){
    //           this.sg['currentFile'].cancel();
    //           return true;
    //         }
    //  });
   this.camera.getPicture(options).then(async (imageData) => {
            let location = this.getLangLatValues();
              this.date = new Date().getTime();
               const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Image'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
              // const loading = await this.loadingCtrl.create({
              //   spinner: 'bubbles',
              //   message: 'Please wait...',
              //   translucent: true,
              //   cssClass: 'custom-class custom-loading'
              // });
              // loading.present();
              let that;
              that = this;
              // alert(imageData);
                     let img_url  = 'data:image/jpeg;base64,' + imageData;
                     fetch(img_url)
                     .then(res => res.blob())
                     .then(blob => {
                       urls = {'name' : this.sg['userdata'].vendor_id+'/observations/images/' + this.date};
          this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/observations/images/' + this.date);
                       firebase.storage().ref(this.sg['userdata'].vendor_id+'/observations/images/' + this.date).put(blob).then(function(snapshot) {
                         setTimeout(() => {
                           firebase.storage().ref(urls.name).getDownloadURL().then(function(url) {
                            // loading.dismiss();
                            const body = { 'type': 'camera', 'url' : url  , 'path': urls,location:that.location?that.location:null};
                            that.address.emit(body);
                            popover.dismiss();
                            img_url = null;
                            });
                         }, 1000);
                       });
       that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id+'/observations/images/' + that.date ).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });

                    });
                   }, (err) => {
                     // Handle error
            alert(err);
       });

  }
   captureVedio() {
    this.getLangLatValues();
    let urls:any;
    const options: CaptureVideoOptions = {
      limit: 1,
      duration: 30
    };
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    this.mediaCapture.captureVideo(options)
    .then(
     async (data: MediaFile[]) => {
         const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
         'source':'Video'
        },
          });
      await popover.present();
     popover.onDidDismiss().then(resp =>{
            if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
            }
     });
        let that;
        that = this;
        const index = data[0].fullPath.lastIndexOf('/');
        let finalPath = data[0].fullPath.substr(0, index);
        if(finalPath.substring(0, 7) !== 'file://'){
        finalPath = 'file://'+finalPath;
        }
        this.file.readAsArrayBuffer(finalPath, data[0].name).then( async(file) => {
        let blob = new Blob([file], {type: data[0].type});
        // const loading = await this.loadingCtrl.create({
        //   spinner: 'bubbles',
        //   message: 'Please wait...',
        //   translucent: true,
        //   cssClass: 'custom-class custom-loading'
        // });
        // loading.present();
        urls = {'name' : that.sg['userdata'].vendor_id+'/observations/videos/' + data[0].name};
          this.sg['currentFile'] = firebase.storage().ref(that.sg['userdata'].vendor_id+'/observations/videos/' + data[0].name);
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/observations/videos/' + data[0].name).put(blob).then(function(snapshot) {
          firebase.storage().ref(that.sg['userdata'].vendor_id+'/observations/videos/' + data[0].name).getDownloadURL().then(function(url) {
            // loading.dismiss();
            const body = { type: 'video', url : url,  path : urls,location:that.location?that.location:null};
            that.address.emit(body);
            popover.dismiss();
            blob = null;
          });
         });
         that.zone.run(() => {
          firebase.storage().ref(that.sg['userdata'].vendor_id+'/observations/videos/' + data[0].name).put(blob).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
        });
      },
      (err: CaptureError) => console.error(err)
    );
   }
}
