import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
import { Router, Route, NavigationEnd } from "@angular/router";
import { SimpleGlobal } from "ng2-simple-global";
import { FarmeasyTranslate } from "../../../translate.service";
import { FormsModule } from "@angular/forms";
import { Platform, NavController } from '@ionic/angular';
import { IonRouterOutlet } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: "farmeasy-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @Input() Data: any;
  @Input("back") back_path;
  @Output() delete: EventEmitter<string>  = new EventEmitter();
  @Output() update: EventEmitter<string>  = new EventEmitter();
  @Output() filter: EventEmitter<string>  = new EventEmitter();
  @Output() detailInfo: EventEmitter<string>  = new EventEmitter();
  @Output() avatar: EventEmitter<string>  = new EventEmitter();
  defaultHref = "";
  button: any = "clock";
  showbutton1: boolean = true;
  date:String;
  twobuttonsflag:boolean = false;
  onebuttonflag:boolean = false;
  nobuttonsflag:boolean = false;
  private routerEvents: any;
  private previousUrl: string;
  private currentUrl: string;
  public canGoBack: boolean;
  constructor(
    private navCtrl: NavController,
    public router: Router,
    private ionRouterOutlet: IonRouterOutlet,
    public sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    public platform: Platform,
    public location:Location
  ) {
    }

  ngOnInit() {
   this.createDateString();
   if(this.Data.button1_action && this.Data.button2_action){
    this.twobuttonsflag = true;
    this.onebuttonflag = false;
    this.nobuttonsflag = false;
   } else {
    if(this.Data.button1_action || this.Data.button2_action){
    this.twobuttonsflag = false;
    this.onebuttonflag = true;
    this.nobuttonsflag = false;
    } else {
    this.twobuttonsflag = false;
    this.onebuttonflag = false;
    this.nobuttonsflag = true;
    }
   }
   if (this.Data.title == "Task List" || this.Data.title == "పని జాబితా" || this.Data.title == "பணி பட்டியல்" || this.Data.title == "ಕಾರ್ಯ ಪಟ್ಟಿ") {
      this.canGoBack = true;
      this.previousUrl = "/farm-dashboard";
    }
    this.canGoBack = this.ionRouterOutlet.canGoBack();
    this.currentUrl = this.router.url;
    this.routerEvents = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;

      }
    });
    // console.log(this.ionRouterOutlet);
    // console.log(this.canGoBack);
    // console.log(this.currentUrl);
    // console.log(this.previousUrl);
    // console.log(this.currentUrl);
    if (this.Data.title == "My Tasks") {
      if (this.sg["userdata"].role == "labour") {
        this.showbutton1 = false;
      }
    }
    this.defaultHref = this.Data.back;
    // console.log(this.previousUrl);
    // console.log(this.currentUrl);


  }
  OpenTitle(){
    if(this.sg['userdata'].role != 'employee'){
      if(this.Data.avatar){
        this.detailInfo.emit();
      }
    }
  }
  groupInfo(){
    this.avatar.emit();
  }
  ngOnChanges() {
    this.defaultHref = this.Data.back;
  }
  ngOnDestroy() {
    if(this.routerEvents){
    this.routerEvents.unsubscribe();
    }
  }
  ionViewDidEnter() {
    this.defaultHref = this.Data.back;
  }
  back() {
    this.location.back();
    // this.navCtrl.back();
    // if(this.Data.header){
    //   const body = { item: JSON.stringify({name:this.Data.header})};
    //   this.navCtrl.navigateBack([this.Data.back,body]);
    // // this.router.navigate([this.Data.back,body], { skipLocationChange: true });
    // } else {
    //    this.navCtrl.navigateBack([this.Data.back]);
    // // this.router.navigate([this.Data.back], { skipLocationChange: true });
    // }
    //this.navCtrl.pop()
  }
  firstButton() {
    let data: any;
    if(this.Data.button1 === 'more'){
      if(this.Data.avatar){
        this.detailInfo.emit();
      }
    }
    if (this.Data.title == "My Tasks") {
      // data = { path: JSON.stringify("/my-tasks") };
      // this.navCtrl.navigateForward([this.Data.button1_action], data);
      // this.router.navigate([this.Data.button1_action, data], {
      //   preserveFragment: true
      // });
    } else if (this.Data.title == "Task List") {

      data = { path: JSON.stringify("/task-list") };
      this.navCtrl.navigateForward([this.Data.button1_action], data);
      // this.router.navigate([this.Data.button1_action, data], {
      //   preserveFragment: true
      // });
    } else if (this.Data.title == "Task Planner") {
      data = { path: JSON.stringify("/task-planner") };
      //this.delete.emit('delete');
      this.navCtrl.navigateForward([this.Data.button1_action], data);
      // this.router.navigate([this.Data.button1_action, data], {
      //   preserveFragment: true
      // });
    } else if (this.Data.present === 'farmeasy-livestock-view') {
      data = { data: JSON.stringify(this.Data) };
      this.navCtrl.navigateForward([this.Data.button1_action, data]);
      // this.router.navigate([this.Data.button1_action, data], {
      //   preserveFragment: true
      // });
    } else {
      if(this.Data.button1 === 'home'){
        this.router.navigateByUrl(this.Data.button1_action);
      } else {
        this.navCtrl.navigateForward([this.Data.button1_action]);
      }
      // this.router.navigate([this.Data.button1_action], {
      //   preserveFragment: true
      // });
    }
  }
  secondButton() {
    const content = { previous: this.Data.present };
    const data = { page_status: JSON.stringify(content) };
    if(this.Data.button2 === 'home'){
      this.router.navigateByUrl(this.Data.button2_action);
    } else {
    this.navCtrl.navigateForward([this.Data.button2_action, data]);
    }
  }
  thirdButton(){
    if(this.Data.button3_action){
      this.navCtrl.navigateForward([this.Data.button3_action]);
    }
    this.delete.emit('delete');
  }
  fourthButton(){
    this.update.emit('update');
  }
  fifthButton(){
    this.filter.emit('filter');
  }
  createDateString(){
    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let fulldate = new Date();
    let day = fulldate.getDay();
    let month = fulldate.getMonth();
    let year = fulldate.getFullYear();
    let date = fulldate.getDate()
    let date_suf = this.ordinal_suffix_of(date);
    this.date = days[day]+','+date_suf+' '+months[month]+' '+year;
   // console.log(this.date);
    // console.log(fulldate,days[day],months[month],year,date,date_suf);
  }
  ordinal_suffix_of(i) {
    let j = i%10;
    let k = i%100;
    if (j==1 && k!=11) {
        return i + "st";
    }
    if (j==2 && k!=12) {
        return i + "nd";
    }
    if (j==3 && k!=13) {
        return i + "rd";
    }
    return i + "th";
}

}