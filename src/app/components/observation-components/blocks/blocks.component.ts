import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'farmeasy-blocks',
  templateUrl: './blocks.component.html',
  styleUrls: ['./blocks.component.scss']
})
export class BlocksComponent implements OnInit {
  @Input() Data: any;
  @Output() block: EventEmitter<any>  = new EventEmitter();
  blocks: any = [];
  blockActive: any = [];
  ngOnInit() {
    this.blocks = this.Data;
  }
  constructor() { }
  Initalize() {
  for (let i = 0; i < this.blocks.length; i++) {
    this.blockActive[i] = false;
  }
  }
  selectBlock(index) {
    for (let i = 0; i < this.blocks.length; i++) {
      if (i === index) {
        this.blockActive[i] = true;
        this.block.emit(this.blocks[i]);
      } else {
      this.blockActive[i] = false;
      }
    }
  }
}
