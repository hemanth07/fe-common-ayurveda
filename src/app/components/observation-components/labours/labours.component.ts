import { FarmeasyTranslate } from './../../../translate.service';
import { KanadaService } from './../../../languages/kanada.service';
import { TamilService } from './../../../languages/tamil.service';
import { HindiService } from './../../../languages/hindi.service';
import { TeluguService } from './../../../languages/telugu.service';
import { EnglishService } from 'src/app/languages/english.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
@Component({
  selector: 'farmeasy-labours',
  templateUrl: './labours.component.html',
  styleUrls: ['./labours.component.scss']
})
export class LaboursComponent implements OnInit {
  @Input() Data: any;
  @Input('editTask') editTask: any;
  @Input('selected') selected: any;
  @Input('fromBlock') fromBlock:any;
  @Output() labour: EventEmitter<any> = new EventEmitter();
  @Output() selectedPriority: EventEmitter<any> = new EventEmitter();
  labourActive: any = [];
  priority:any;
  priority_label:any;
  priority_list:any = [];
  ok:any;
  cancel:any;
  no_labour:any;
  constructor(public sg:SimpleGlobal,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService,
    private translate:FarmeasyTranslate) {
     
    }
  ngOnInit() {
    setTimeout(()=>{
      this.translation(this.Data);
    },500);
      if(this.sg['userdata'].primary_language === 'en'){
      this.no_labour = "There are no labours in this blocks";
      this.priority_label = 'Indicate Priority';
      this.ok="Ok";
      this.cancel="Cancel"
      this.priority_list = this.en.getPriority();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.no_labour = "ఈ బ్లాకులలో లేబర్స్ లేరు";
      this.priority_label = 'ప్రాధాన్యత సూచించండి';
      this.priority_list = this.te.getPriority();
      this.ok="సరే";
      this.cancel="రద్దు";
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.no_labour = "இந்த தொகுதிகளில் லேபர்கள் இல்லை";
      this.priority_label = 'முன்னுரிமை குறிப்பிடு';
      this.priority_list = this.ta.getPriority();
      this.ok="சரி";
      this.cancel="ரத்து";
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.no_labour = "ಈ ಬ್ಲಾಕ್ಗಳಲ್ಲಿ ಯಾವುದೇ ಲೇಬರ್ಗಳಿಲ್ಲ";
      this.priority_label = 'ಆದ್ಯತೆ ಸೂಚಿಸಿ';
      this.priority_list = this.kn.getPriority();
      this.ok="ಸರಿ";
      this.cancel="ರದ್ದುಮಾಡಿ";
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.no_labour = "इस ब्लॉक में कोई मजदूर नहीं हैं";
      this.priority_label = 'प्राथमिकता को इंगित करें';
      this.priority_list = this.hi.getPriority();
      this.ok="ठीक है";
      this.cancel="रद्द करना";
    } else {
      this.no_labour = "There are no labours in this blocks";
      this.priority_label = 'Indicate Priority';
      this.priority_list = this.en.getPriority();
      this.ok="Ok";
      this.cancel="Cancel";
    }
  }
  translation(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        name_label:item.name,
      }
      menu.push(obj);
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i], 'en',this.sg['userdata'].primary_language).then(data => {
          this.Data[i]['name_label'] = data['name_label'];
          });
      } else {
        this.Data[i]['name_label'] = menu[i]['name_label'];
      }
    }
  }
  ngOnChanges(){
    if(this.editTask) {
     // console.log(this.Data);
      this.priority = this.editTask.indicate_priority;
      this.InitaliseData();
    }
    if(this.Data.length != 0){
      if(this.selected) {
     //   console.log(this.Data);
     //   console.log(this.selected);
        for(let idx in this.Data){
          if(this.Data[idx].phone === this.selected.labour.phone){
            this.selectLabour(parseInt(idx));
          }
        }
      }
      this.translation(this.Data);
    }
  }
  selectLabour(index) {
    for (let i = 0; i < this.Data.length; i++) {
      if (i === index) {
        this.labourActive[i] = true;
        this.labour.emit(this.Data[i]);
      } else {
        this.labourActive[i] = false;
      }
    }  
  }
  InitaliseData() {
    for (let i = 0; i < this.Data.length; i++) {
      this.labourActive[i] = false;
      this.Data[i].name=this.Data[i].name.text(this.Data[i].name.text().substr(0, 1));
      if (this.editTask) {
        if (this.Data[i].phone == this.editTask.assignee.phone) {
          this.labourActive[i] = true;
          this.labour.emit(this.Data[i]);
        }
      }
    }
    
  }
  getPriorty(val){
   // console.log(val);
    this.selectedPriority.emit(val.detail.value);
  }
  displayName(longText){
    return longText.text(longText.text().substr(0, 10));
  }

}
