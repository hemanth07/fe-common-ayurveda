import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-pop-over',
  templateUrl: './pop-over.component.html',
  styleUrls: ['./pop-over.component.scss']
})
export class PopOverComponent implements OnInit {
  message: string;
  constructor(private popoverController: PopoverController, public navParams: NavParams) { }

  ngOnInit() {
    this.message = this.navParams.get('message');
  }
  sourcePopOverOk() {
    this.popoverController.dismiss();
  }
}
