import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ObservationListComponent } from './observation-list/observation-list.component';
import { HeaderComponent } from './header/header.component';
import { LaboursComponent } from './labours/labours.component';
import { CategoriesComponent } from './categories/categories.component';
import { BlocksComponent } from './blocks/blocks.component';
import { ProofObservationComponent } from './proof-observation/proof-observation.component';
import { PopOverComponent } from './pop-over/pop-over.component';
import { HttpModule } from '@angular/http';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(),
  ],
  declarations: [ObservationListComponent,
     HeaderComponent,
     LaboursComponent,
     CategoriesComponent,
     BlocksComponent,
     ProofObservationComponent,
     PopOverComponent,
     FooterComponent
     ],
  exports: [ObservationListComponent,
    HeaderComponent,
    LaboursComponent,
    CategoriesComponent,
    BlocksComponent ,
    ProofObservationComponent,
    PopOverComponent,
    FooterComponent
    ],
  entryComponents: [
     HeaderComponent,
     LaboursComponent,
     CategoriesComponent,
     BlocksComponent,
     ProofObservationComponent,
     PopOverComponent,
     FooterComponent
  ],
})
export class ObservationComponentsModule {}
