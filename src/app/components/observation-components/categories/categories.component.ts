import { FarmeasyTranslate } from './../../../translate.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'farmeasy-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @Input() Data: any;
  @Output() category: EventEmitter<any>  = new EventEmitter();
  @Input('editTask') editTask;
  categoryActive: any = [];
  categories: any = [];
  constructor(public sg:SimpleGlobal,
  private translate:FarmeasyTranslate
    ) { }
  ngOnInit() {
    this.categories = this.Data;
    this.translation(this.Data);
    this.InitaliseData();
  }
  ngOnChanges(){
    if(this.editTask){
      this.InitaliseData();
    }
  }
  translation(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj = {
        name:item.name,
        }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.categories[i].name = data['name'];
           });
      } else {
        this.categories[i].name = menu[i]['name'];
      }
    }
  }
  
  selectCategory(index) {
    for (let i = 0; i < this.categories.length; i ++) {
      if (i === index) {
        this.categoryActive[i] = true;
        this.category.emit(this.categories[i]);
      } else {
        this.categoryActive[i] = false;
      }
    }
  }
  InitaliseData() {
    for (let i = 0; i < this.categories.length; i ++) {
      this.categoryActive[i] = false;
      if(this.editTask && this.editTask.category){
        if(this.categories[i].id==this.editTask.category.id){
          this.categoryActive[i] = true;
          this.category.emit(this.categories[i]);
        }
      }
    }
  }

}
