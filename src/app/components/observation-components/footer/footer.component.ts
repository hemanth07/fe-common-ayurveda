import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
@Component({
  selector: 'farmeasy-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() Data: any;
  @Input() isEnableLeft:any;
  @Input() isEnableRight:any;
  @Input() isEnableMiddle:any;
  @Output() leftButtonAction: EventEmitter<string>  = new EventEmitter();
  @Output() middleButtonAction: EventEmitter<string>  = new EventEmitter();
  @Output() rightButtonAction: EventEmitter<string>  = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log(this.Data);
  }
  leftButton(){
    this.leftButtonAction.emit('leftButtonAction');
  }
  middleButton(){
    this.middleButtonAction.emit('middleButtonAction');
  }
  rightButton(){
    this.rightButtonAction.emit('middleButtonAction');
  }

}