import { KanadaService } from './../../languages/kanada.service';
import { TamilService } from './../../languages/tamil.service';
import { TeluguService } from './../../languages/telugu.service';
import { EnglishService } from './../../languages/english.service';
import { HindiService } from './../../languages/hindi.service';
import { LoadingController } from '@ionic/angular';
import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { Storage } from '@ionic/storage';
import { FarmeasyTranslate } from '../../translate.service';
import { count } from 'rxjs/operators';
@Component({
  selector: 'app-farmeasymodules',
  templateUrl: './farmeasymodules.component.html',
  styleUrls: ['./farmeasymodules.component.scss']
})
export class FarmeasymodulesComponent implements OnInit {
  Farm_Modules: any = [];
  temp_Farm_Modules: any = [];
  userData: any = [];
  showlabour: boolean;
  showadmin: boolean;
  showsupervisor: boolean;
  adminButton: string = 'All Observations';
  taskmanager: string = 'Task Manager';
  @Output() Farmeasymoduleslist = new EventEmitter();
  constructor(public storage: Storage,  public translate: FarmeasyTranslate,
    private sg: SimpleGlobal, private loadingController: LoadingController,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService) {}
  ngOnInit() {
    this.getRoles();
  
   }
    getRoles() {
    this.storage.get('farmeasy_userdata').then((val) => {
      this.userData = val;
      console.log(this.userData);
      if (this.userData.role === 'corporate') {
        this.showadmin = true;
      }
      if (this.userData.role === 'supervisor') {
        this.showsupervisor = true;
      }
      if (this.userData.role === 'employee') {
        this.showlabour = true;
      }
    });
    if(this.sg['userdata'].primary_language === 'en'){
      this.Farm_Modules = this.en.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.Farm_Modules = this.te.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.Farm_Modules = this.ta.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.Farm_Modules = this.kn.getDashboard();
    }else if(this.sg['userdata'].primary_language === 'hi'){
      this.Farm_Modules = this.hi.getDashboard();
    } else {
      this.Farm_Modules = this.en.getDashboard();
    }
  }

  select_module(path) {
   this.Farmeasymoduleslist.emit(path);
  }
}
