import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, AlertController,PopoverController, LoadingController, ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { SimpleGlobal } from 'ng2-simple-global';

@Component({
  selector: 'app-farmeasy-forgot-password',
  templateUrl: './farmeasy-forgot-password.component.html',
  styleUrls: ['./farmeasy-forgot-password.component.scss']
})
export class FarmeasyForgotPasswordComponent implements OnInit {
  mobile:number;
  otp:number;
  dontShowSend:boolean = true;
  showOpt:boolean = false;
  otperror:boolean = false;
  public recaptchaVerifier:firebase.auth.RecaptchaVerifier;
  constructor(
     public navCtrl: NavController,
     private sg: SimpleGlobal,
     private popoverController: PopoverController,
     public alertController: AlertController,
     public toastController: ToastController,
     private loading:LoadingController
     ) { 
       firebase.auth().languageCode = 'en';
     }

  ngOnInit() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }
  popOverDateOk() {
    this.popoverController.dismiss();
  }
  popOverDateCancel() {
    this.popoverController.dismiss();
  }
async checkNumberInDB(){
   const loading = await this.loading.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
   firebase.database().ref('farmeasy_users').orderByChild('phone').equalTo(this.mobile.toString()).once('value', resp =>{
     let user = snapshotToArray(resp);
     console.log(user);
     loading.dismiss();
     if(user.length >= 1){
       this.sg['forgotUserData'] = user[0];
       this.showOpt = true;
       this.sendOTPToMobileNumber(this.mobile);
     } else {
       this.userNotExistingAlert();
     }
   }).catch((err)=>{
     console.error(err);
   });
  }
  async userNotExistingAlert() {
    const alert = await this.alertController.create({
      message: 'User details is not existing in farmeasy, Please contact admin for more details',
      buttons: ['OK']
    });
    await alert.present();
  }
  listMobileNumber(){
   if(this.mobile.toString().length != 10){
     this.dontShowSend = true;
   }else {
     this.dontShowSend = false;
   }
  }
  listenOTP(){
    this.otperror = false;
  }
sendOTPToMobileNumber(phone){
   const appVerifier = this.recaptchaVerifier;
  const phoneNumberString = "+91" + phone;

  firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
    .then( confirmationResult => {
      this.sg['confirmationResult'] = confirmationResult;
      this.presentToast('OPT sent to mobile number');
      // SMS sent. Prompt user to type the code from the message, then sign the
      // user in with confirmationResult.confirm(code).
    })
  .catch(function (error) {
    console.error("SMS not sent", error);
  });
}
verifyNumber(){
  let that = this;
     this.sg['confirmationResult'].confirm(this.otp.toString())
            .then(function (result) {
               console.log(result.user);
               that.popoverController.dismiss();
               that.showPassword();
              // ...
            }).catch(function (error) {
              console.log(error);
              that.otperror = true;
              // User couldn't sign in (bad verification code?)
              // ...
            });
}
async showPassword(){
      const alert = await this.alertController.create({
      header: 'Password',
      backdropDismiss:false,
      message: 'Your Password is <strong>'+this.sg['forgotUserData'].password+'</strong> please login with your mobile number and this password',
      buttons: ['OK']
      });
      await alert.present();
}


async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
