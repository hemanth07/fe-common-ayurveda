import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmeasyForgotPasswordComponent } from './farmeasy-forgot-password.component';

describe('FarmeasyForgotPasswordComponent', () => {
  let component: FarmeasyForgotPasswordComponent;
  let fixture: ComponentFixture<FarmeasyForgotPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmeasyForgotPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmeasyForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
