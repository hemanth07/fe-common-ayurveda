import { Component, OnInit } from '@angular/core';
import { SimpleGlobal } from "ng2-simple-global";
import { NavController } from "@ionic/angular";
import { Router, Route, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-error',
  templateUrl: './error.page.html',
  styleUrls: ['./error.page.scss'],
})
export class ErrorPage implements OnInit {

  constructor( public sg: SimpleGlobal,
                public navCtrl:NavController,
                public router:Router) { }

  ngOnInit() {
  }
  retry(){
    this.router.navigateByUrl('/farmeasysignin');
    // this.navCtrl.navigateRoot('/farm-dashboard');
  }

}

