import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
//import { HelloComponent } from './hello.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FarmeasysigninPage } from './farmeasysignin.page';

@NgModule({
  imports: [
    CommonModule,
    
    IonicModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: FarmeasysigninPage
      }
    ]),ComponentsModule
  ],
  declarations: [FarmeasysigninPage ],

})
export class FarmeasysigninPageModule {}
