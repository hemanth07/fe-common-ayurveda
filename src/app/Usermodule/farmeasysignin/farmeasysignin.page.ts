import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { SimpleGlobal } from 'ng2-simple-global';

@Component({
  selector: 'app-farmeasysignin',
  templateUrl: 'farmeasysignin.page.html',
  styleUrls: ['farmeasysignin.page.scss'],
})
export class FarmeasysigninPage  {

  constructor(private sg: SimpleGlobal, public navCtrl: NavController, public alertController: AlertController, private storage: Storage, private menu: MenuController) {
   
  }

}