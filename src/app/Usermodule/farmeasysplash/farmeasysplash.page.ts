import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import * as firebase from 'firebase';
@Component({
  selector: 'app-farmeasysplash',
  templateUrl: './farmeasysplash.page.html',
  styleUrls: ['./farmeasysplash.page.scss'],
})
export class FarmeasysplashPage implements OnInit {
  constructor(private sg: SimpleGlobal, public navCtrl: NavController, private menu: MenuController) {
   }
  ngOnInit() {
    // firebase.database().ref('wellnest/users').once('value',resp =>{
    //   let data  = snapshotToArray(resp);
    //   for(let record of data){
    //    console.log(record.name+"-"+record.phone+"-"+record.password+"-"+record.role);
    //   }
    // });
   }
  farm_signin() {
    this.navCtrl.navigateRoot('/farmeasysignin');
  }
}

export const snapshotToArray = snapshot => {

  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};



// Cloud Express-1111111111-wnf-corporate
// Dan Singh Bist-8217670501-wnf-supervisor
// Chellaiah-7893020561-wnf-labour
// Ambika-7893020562-wnf-labour
// Lalitha-8940917679-wnf-labour
// Madavan-7702572691-wnf-labour
// Sudhakar-9908342451-wnf-labour
// Chinnaiah-8309096199-wnf-labour
// Jamuna-9676141096-wnf-labour
// Sounderraj-9676720162-wnf-labour
// Vijayan-8464912381-wnf-labour
// Pattabhi-9493177026-wnf-labour
// Bhargav-8310100920-12345-corporate
// Chandra shekar-9902504444-wnf-corporate
// Sharan-9900051404-wnf-corporate
// Surendra Rao-9972438029-wnf-corporate
// vinay ojha-9980035340-wnf-corporate
// Sathesh-9845533775-ces-corporate
// Ramesh-9019256889-ces-corporate
// Raghu-9047675382-wnf-supervisor
// Vandana-9876543210-12345-corporate
// Suresh-9740742526-ces-corporate
// Shravan-9160682009-123-corporate
// munnaswami-9676141096-wnf-labour