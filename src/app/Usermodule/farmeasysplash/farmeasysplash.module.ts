import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasysplashPage } from './farmeasysplash.page';

const routes: Routes = [
  {
    path: '',
    component: FarmeasysplashPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasysplashPage]
})
export class FarmeasysplashPageModule {}
