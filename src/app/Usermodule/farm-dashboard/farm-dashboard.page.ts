import { Router  } from '@angular/router';
import { Component, OnInit, ViewChild } from "@angular/core";
import { PopoverController, Platform, LoadingController, AlertController} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { SimpleGlobal } from "ng2-simple-global";
import { NavController } from "@ionic/angular";
import * as firebase from "firebase";
import { Firebase } from "@ionic-native/firebase/ngx";
import { AppVersion } from '@ionic-native/app-version/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { KanadaService } from '../../languages/kanada.service';
import { TamilService } from '../../languages/tamil.service';
import { TeluguService } from '../../languages/telugu.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
@Component({
  selector: "app-farm-dashboard",
  templateUrl: "./farm-dashboard.page.html",
  styleUrls: ["./farm-dashboard.page.scss"]
})
export class FarmDashboardPage implements OnInit {
  defaultHref: any = "";
  date: any;
  showUpdateApp: boolean;
  current_appversion:any;
  Farm_Modules: any = [];
  temp_Farm_Modules: any = [];
  userData: any = [];
  showlabour: boolean;
  showadmin: boolean;
  showsupervisor: boolean;
  adminButton: string = 'All Observations';
  taskmanager: string = 'Task Manager';
  isDeveloper:boolean = false;
  subscription:any;
  constructor(
    private firebase1: Firebase,
    private popoverController: PopoverController,
    public loadingController: LoadingController,
    private alertController: AlertController,
    public navCtrl: NavController,
    public sg: SimpleGlobal,
    private appVersion: AppVersion,
    private storage: Storage,
    private platform: Platform,
    private iab: InAppBrowser,
    private en: EnglishService,
    private ta:TamilService,
    private hi:HindiService,
    private te:TeluguService,
    private kn:KanadaService,
    public router:Router
  ) {
   
    this.createDateString();
    this.storage.get("farmeasy_userdata").then(val => {
      if (val) {
        this.sg["userdata"] = val;
        this.firebase1.getToken().then(token => {
          // alert(token);
          if (token) {
            firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key).update({ device_id: token }).then(resp => {
              //  alert(resp);
              });
          }
         // alert(token);
        }) // save the token server-side and use it to push notifications to this device
          .catch(error => console.log(error));
      }
    });
    // firebase.database().ref('devList').on('value',resp =>{
    //   let developingList = resp.val();
    //   if(developingList.includes(this.sg['userdata'].farmeasy_id)){
    //     this.isDeveloper = true;
    //     console.log(this.isDeveloper);
    //   }
    // });
    
  }
  ngOnInit() {
   
  }

  ionViewDidEnter(){
    this.getVersion();
    this.getRoles();
    this.subscription = this.platform.backButton.subscribe(async()=>{
      const alert = await this.alertController.create({
        header: 'Confirm!',
        message: 'Are you sure want to close <strong>Ayurveda one</strong>',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
            console.log('Confirm Okay');
            navigator['app'].exitApp();
            }
          }
        ]
      });
  
      await alert.present();
  });
  }
ionViewWillLeave(){
    this.subscription.unsubscribe();
}

  getRoles() {
    this.storage.get('farmeasy_userdata').then((val) => {
      this.userData = val;
      console.log(this.userData);
      if (this.userData.role === 'corporate') {
        this.showadmin = true;
      }
      if (this.userData.role === 'supervisor') {
        this.showsupervisor = true;
      }
      if (this.userData.role === 'employee') {
        this.showlabour = true;
      }
    });
    if(this.sg['userdata'].primary_language === 'en'){
      this.Farm_Modules = this.en.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.Farm_Modules = this.te.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.Farm_Modules = this.ta.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.Farm_Modules = this.kn.getDashboard();
    }else if(this.sg['userdata'].primary_language === 'hi'){
      this.Farm_Modules = this.hi.getDashboard();
    } else {
      this.Farm_Modules = this.en.getDashboard();
    }
  }
  async selectModule(data) {
    this.sg['farm_details_item'] = data.keyword;
    // const loading = await this.loadingController.create({
    //   spinner: 'bubbles',
    //   message: 'Loading '+data.title,
    //   translucent: true,
    // });
    // await loading.present();
    // this.router.navigate([data.path]);
    this.navCtrl.navigateForward(data.path)
    // .then(()=>{
    //   loading.dismiss();
    // });
    // let data = { backPath: JSON.stringify("/farm-dashboard") };
    
  }
  goBack() {
    // this.navCtrl.goBack();
    this.navCtrl.navigateBack("/my-tasks");
  }
  openMessageList(){
    this.navCtrl.navigateForward('/chat-list');
  }
  nofication_count() {
    let data = { page_status: JSON.stringify("dashboard") };
    this.navCtrl.navigateForward(["/notifications", data]);
  }
 openProfile(){
  this.navCtrl.navigateForward('/userprofile');
 }
  remaindMeLater() {
    var currentDate = new Date();
    this.storage.set('app_update_reminder_frequency', currentDate);

    this.showUpdateApp = false;
  }
  updateNow() {
    this.showUpdateApp = false;
    this.storage.remove('app_update_reminder_frequency');
    if (this.platform.is('ios')) {
      // const browser = this.iab.create('https://tinyurl.com/y2qzgmuf', '_system', { location: 'no' });
    }
    else {
       const browser = this.iab.create('https://play.google.com/store/apps/details?id=io.app.farmeasy', '_system', { location: 'no' });
    }
  }
  checkVersion() {
    let latest_version: any;
    this.appVersion.getVersionNumber().then((version) => {
      this.current_appversion = version;
      //alert(version);
      if (this.platform.is('ios')) {
        firebase.database().ref('version/ios').on('value',resp =>{
          latest_version =resp.val();
        //  alert("latest"+latest_version);
          if (latest_version && latest_version > version) {
            this.showUpdateApp = true;
          }
        })
      } else {
        firebase.database().ref('version/android').on('value',resp =>{
          latest_version = resp.val();
        //  alert("latest"+latest_version);
          if (latest_version && latest_version > version) {
            this.showUpdateApp = true;
          }
        })
      }
    });
  }
  getVersion() {
    this.storage.get('app_update_reminder_frequency').then((val) => {
      if (!val) {
        this.checkVersion();
      }
      else {
        let user_date: any = new Date(val);
        let today_date: any = new Date();
        let diff_date = today_date - user_date;
        let num_years = diff_date / 31536000000;
        let num_months = (diff_date % 31536000000) / 2628000000;
        let years = Math.floor(num_years);
        let month = Math.floor(num_months);
        var one_day = 1000 * 60 * 60 * 24;
        var date1_ms = today_date.getTime();
        var date2_ms = user_date.getTime();
        var difference_ms = date1_ms - date2_ms;
        difference_ms = difference_ms / 1000;
        var seconds = Math.floor(difference_ms % 60);
        difference_ms = difference_ms / 60;
        var minutes = Math.floor(difference_ms % 60);
        difference_ms = difference_ms / 60;
        var hours = Math.floor(difference_ms % 24);
        var days = Math.floor(difference_ms / 24);
        if (days >= 2) {
          this.checkVersion();
        }
      }
    });
  }
  createDateString() {
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    let days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ];
    let fulldate = new Date();
    let day = fulldate.getDay();
    let month = fulldate.getMonth();
    let year = fulldate.getFullYear();
    let date = fulldate.getDate();
    let date_suf = this.ordinal_suffix_of(date);
    this.date = days[day] + "," + date_suf + " " + months[month] + " " + year;
   
  }
  ordinal_suffix_of(i) {
    let j = i % 10;
    let k = i % 100;
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
