import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
// import { UsercomponentsModule } from '../../components/usercomponents/usercomponents.module';

import { IonicModule } from '@ionic/angular';
import { FarmDashboardPage } from './farm-dashboard.page';
const routes: Routes = [
  {
    path: '',
    component: FarmDashboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // UsercomponentsModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [FarmDashboardPage]
})
export class FarmDashboardPageModule {}
