import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
// import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  headerData:any;
  constructor(private sg: SimpleGlobal,
    private socialSharing: SocialSharing) { }

  ngOnInit() {
    this.getTitle();
  }
    getTitle(){
      let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'About Ayurveda one';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'Ayurveda one ಬಗ್ಗೆ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'Ayurveda one பற்றி';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'Ayurveda one గురించి';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'Ayurveda one के बारे में';
    }
    this.headerData = {
      color: 'green',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
  }
  sShare(){
    var options = {
      message: 'Ravi Sent feed', // not supported on some apps (Facebook, Instagram)
      subject: 'Sent local feeds', // fi. for email
      url: 'https://ionicframework.com/docs/native/social-sharing',
    };
    this.socialSharing.shareWithOptions(options)
  }
  shareViaEmail() {
    this.socialSharing.canShareViaEmail().then(() => {
      var options = {
        message: 'Ravi Sent feed', // not supported on some apps (Facebook, Instagram)
        subject: 'Sent local feeds', // fi. for email
        url: 'https://ionicframework.com/docs/native/social-sharing',
      };
      // this.platform.ready().then(() => {
        this.socialSharing.shareViaEmail('Body', 'Subject', ['info@cloudexpresssolutions.com'])
      // });
    }).catch((err) => {
      alert('Email not available')
    })
  }

}
