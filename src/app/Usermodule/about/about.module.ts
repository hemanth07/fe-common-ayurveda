import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
// import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
// import { SocialSharing } fr/om '@ionic-native/social-sharing/ngx';

import { AboutPage } from './about.page';
import { ObservationComponentsModule } from './../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: AboutPage
  }
];

@NgModule({
  imports: [
    ObservationComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    // SocialSharing,

    RouterModule.forChild(routes)
  ],

  declarations: [AboutPage]
})
export class AboutPageModule {}
