import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { SimpleGlobal } from 'ng2-simple-global';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ToastController } from '@ionic/angular';
import { snapshotToArray } from './../../languages/others.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  headerData: any;
  feedbackForm: any = FormGroup;

  submitAttempt: boolean = false;
  constructor(private sg: SimpleGlobal,
    public form: FormBuilder,
    public location: Location,
    private socialSharing: SocialSharing,
    public toastController: ToastController) {
    this.feedbackForm = form.group({
      feature: ['', Validators.required],
      feedback: ['', Validators.required]
    });
  }

  ngOnInit() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Supports'
    } else if (this.sg['userdata'].primary_language === 'te') {
      title = 'అభిప్రాయం'
    } else if (this.sg['userdata'].primary_language === 'ta') {
      title = 'பின்னூட்டம்'
    } else if (this.sg['userdata'].primary_language === 'kn') {
      title = 'ಪ್ರತಿಕ್ರಿಯೆ'
    } else if (this.sg['userdata'].primary_language === 'hi') {
      title = 'प्रतिपुष्टि'
    }
    this.headerData = {
      color: 'green',
      title: title,
      button2: 'home',
      button2_action: '/farm-dashboard',
    };
    firebase.database().ref('wellnest/taskManager/task_table').orderByChild('log').equalTo(true).once('value', resp => {
      let latest_tasks = snapshotToArray(resp);
      for (let task of latest_tasks) {
        if (task.status) {

        } else {
          firebase.database().ref('wellnest/taskManager/task_table/' + task.key).remove();
        }
      }
    });

  }
  shareViaEmai() {
    this.submitAttempt = true;
    if (this.feedbackForm.valid) {
      let data = this.feedbackForm.value
      alert(JSON.stringify(data.feature))
    }
  }

  shareViaEmail() {
    this.submitAttempt = true;

    this.socialSharing.canShareViaEmail().then(() => {
      if (this.feedbackForm.valid) {
        let data = this.feedbackForm.value
        var subject = data.feature;
        var body = data.feedback;
        // this.platform.ready().then(() => {
        this.socialSharing.shareViaEmail(body, subject, ['info@cloudexpresssolutions.com'])
      }
    }).catch((err) => {
      alert('Email not available')
    })

  }

  submit() {

    this.submitAttempt = true;

    if (this.feedbackForm.valid) {
      let data = this.feedbackForm.value
      data['id'] = this.sg['userdata'].farmeasy_id;
      data['name'] = this.sg['userdata'].name;
      data['phone'] = this.sg['userdata'].phone;
      console.log(data);
      firebase.database().ref(this.sg['userdata'].vendor_id + '/feedback').push(data);
      this.location.back();
      this.presentToast();

    }
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Feedback sent',
      duration: 2000
    });
    toast.present();
  }

}
