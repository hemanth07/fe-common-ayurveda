import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SimpleGlobal } from 'ng2-simple-global';
import { ActivatedRoute } from "@angular/router";
import { FarmeasyTranslate } from '../../translate.service';
import * as firebase from 'firebase';
import { PopoverController,LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  user_notifications: any;
  my_notifications: any;
  notifications_data: any;
  notifications_data_receive: any;
  notifications_data_send: any;
  error_message: any;
  date: any;
  add_label:any;
  user_data:any;
  page_status: any;
  title: string;
  headerData: any;
  show_receive:boolean=true;
  show_send:boolean=false;
  loader:any;
  labels:any = {}
  labourflag:boolean = false;
  wait:string;
  constructor(private storage: Storage, 
    private sg: SimpleGlobal,
    private router: Router,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translation:FarmeasyTranslate,
    private loading:LoadingController,
    private route: ActivatedRoute
    ) {
    this.date = Date.now();
  }



  ngOnInit() {
    this.getTitle();
    if(this.sg['userdata'].role === 'employee'){
      this.labourflag = true;
    } else {
      this.labourflag = false;
    }
      if(this.sg['userdata'].primary_language === 'en'){
        this.add_label = "Add";
        this.wait = "Please wait ..."
        this.error_message = "No Notifications";
        this.labels['receive'] = "Received"
        this.labels['send'] = "Sent"
        this.labels['acknowledge'] = "Acknowledge"
        this.labels['acknowledged'] = "Acknowledged"
      } else if(this.sg['userdata'].primary_language === 'te'){
    this.wait = "దయచేసి వేచి ఉండండి ..."
        this.add_label = "చేర్చు";
        this.error_message = "నోటిఫికేషన్ లేదు";
        this.labels['receive'] = "అందుకుంది"
        this.labels['send'] = "పంపిన"
        this.labels['acknowledge'] = "గుర్తించి"
        this.labels['acknowledged'] = "తెలియజేసారు"
      } else if(this.sg['userdata'].primary_language === 'ta'){
    this.wait = "தயவுசெய்து காத்திருங்கள் ..."
        this.add_label = "கூட்டு";
        this.error_message = "அறிவிப்பு இல்லை";
        this.labels['receive'] = "பெறப்பட்டது"
        this.labels['send'] = "அனுப்பப்பட்டது"
        this.labels['acknowledge'] = "ஒப்புக்கொண்டுள்ள"
        this.labels['acknowledged'] = "ஒப்புக்கொள்ளப்பட்டது"
      } else if(this.sg['userdata'].primary_language === 'kn'){
    this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
        this.add_label = "ಸೇರಿಸಿ";
        this.error_message = "ಸೂಚನೆ ಇಲ್ಲ";
        this.labels['receive'] = "ಸ್ವೀಕರಿಸಲಾಗಿದೆ"
        this.labels['send'] = "ಕಳುಹಿಸಲಾಗಿದೆ"
        this.labels['acknowledge'] = "ಅಂಗೀಕರಿಸಿ"
        this.labels['acknowledged'] = "ಅಂಗೀಕರಿಸಲಾಗಿದೆ"
      } else if(this.sg['userdata'].primary_language === 'hi'){
        this.add_label = "जोड़ना"
    this.wait = "कृपया प्रतीक्षा करें ..."
        this.error_message = "कोई अधिसूचना नहीं";
        this.labels['receive'] = "प्राप्त किया"
        this.labels['send'] = "भेज दिया"
        this.labels['acknowledge'] = "स्वीकार करते हैं"
        this.labels['acknowledged'] = "स्वीकार किया"
      }
    this.initilisedata();

  }
  getTitle(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.title = 'Notification';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.title = 'ಅಧಿಸೂಚನೆ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.title = 'அறிவித்தல்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.title = 'నోటిఫికేషన్';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.title = 'अधिसूचना';
    }
    if(this.sg['userdata'].role != 'employee'){
      this.headerData = {
        color: 'green',
        title: this.title,
        button1: 'add-circle-outline',
        button1_action: '/create-notifications',
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    } else {
      this.headerData = {
        color: 'green',
        title: this.title,
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    }

  }

  //-------------------------------Retrieveing Of Notifications-----------------------------------//
  async initilisedata() {
    let that = this;
    this.loader = await this.loading.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    this.loader.present();
    that.storage.get('farmeasy_userdata').then((val) => {
      console.log('Your age is', val);
      this.user_data = val;
      if (val) {
        let that = this;
        let usersRef = firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications");
        usersRef.on("value", function (Data) {
          let data = snapshotToArray(Data);
          // alert(JSON.stringify(data))
          that.notificationdata(data);
         
        });
      }
    });
  }

  segmentChanged(val) {
    if(val == 'receive'){
      this.show_receive = true;
      this.show_send = false;
    } else {
      this.show_receive = false;
      this.show_send = true;
    }
  }

  notificationdata(data) {
    this.notifications_data = data.reverse();
    for (let i = 0; i < this.notifications_data.length; i++) {
      this.notifications_data[i].createdat = this.timeSince(this.notifications_data[i].createdat);
    }
    this.translate(this.notifications_data);
  }
  
  translate(data){
    let temp = data
    let menu = [];
    for(let item of data){
      let obj;
      if(item.status && item.notification_status){
        obj = {
          description:item.notiification_description,
          // status_label:item.status,
          // notification_status_label:item.notification_status
         }
      }
      else {
           obj ={
            description:item.notiification_description,
           }
      }
      menu.push(obj)
    }
    for(let i=0;i<menu.length;i++){
      if(this.sg['userdata'].primary_language !== 'en'){
        this.translation.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          if(menu[i].description){
            this.notifications_data[i].notiification_description = data['description'];
            // this.notifications_data[i]['status_label'] = data['status_label'];
            // this.notifications_data[i]['notification_status_label'] = data['notification_status_label'];
          }
          else {
            this.notifications_data[i].notiification_description = data['description'];
          }
        });
      } else {
        if(menu[i].description){
          this.notifications_data[i].notiification_description = menu[i]['description'];
          // this.notifications_data[i]['status_label'] = menu[i]['status_label'];
          // this.notifications_data[i]['notification_status_label'] = menu[i]['notification_status_label'];
        }
        else {
          this.notifications_data[i].notiification_description = menu[i]['description'];
        }
      }
    }
    setTimeout(()=>{
      this.notifications_data_send = [];
      this.notifications_data_receive = [];
      for(let notification of  this.notifications_data){
        if(notification.sender){
          this.notifications_data_send.push(notification);
        }else {
          this.notifications_data_receive.push(notification);
        }
      }
      this.loader.dismiss();
     },2000);

  }
  async loadData(event) {
    console.log(event);
    
    setTimeout(() => {
      console.log('Done');
      event.target.complete();
      }, 500);
  }

  //-------------------------------Time Stamp For Notification Time-----------------------------------//
  timeSince(date) {
    let todaydate: any;
    todaydate = new Date();
    let seconds = Math.floor((todaydate - date) / 1000);
    let interval = Math.floor(seconds / 31536000);
    let intervalType;
  if (interval >= 1) {
    intervalType = 'y ago';
  } else {
    interval = Math.floor(seconds / 2592000);
    if (interval >= 1) {
      intervalType = 'm ago';
    } else {
      interval = Math.floor(seconds / 86400);
      if (interval >= 1) {
        intervalType = 'd ago';
      } else {
        interval = Math.floor(seconds / 3600);
        if (interval >= 1) {
          intervalType = "h ago";
        } else {
          interval = Math.floor(seconds / 60);
          if (interval >= 1) {
            intervalType = "m ago";
          } else {
            interval = seconds;
            return 'Just Now';
          }
        }
      }
    }
  }
  return interval + '' + intervalType;
  }

  //-------------------------------Update Notification-----------------------------------//
  updatestatus(data) {
    if (data.key) {
      let that = this;
      that.storage.get('farmeasy_userdata').then((val) => {
        if (val) {
          firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications" + "/" + data.key).update({ 'status': 'Acknowledged', 'createdat': that.date, 'read':true });
        }
      });
    }
  }

  //-------------------------------Update Notification Status-----------------------------------//
  notification_status(data) {
    if (data.key) {
      let that = this;
      that.storage.get('farmeasy_userdata').then((val) => {
        if (val) {
          firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications" + "/" + data.key).update({ 'notification_status': 'Completed','status': 'Acknowledged', 'createdat': that.date, 'read':true });
        }
      });
    }
  }

  //-------------------------------View Notifications-----------------------------------//
  Viewnotification(notification_key) {
    this.storage.get('farmeasy_userdata').then((val) => {
      if (val) {
        firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + val.key + "/notifications" + "/" +notification_key.key).update({ 'read':true});
      }
    });
    let data = { "notification_key": JSON.stringify(notification_key) };
    this.navCtrl.navigateForward(['/viewnotification', data]);
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};

