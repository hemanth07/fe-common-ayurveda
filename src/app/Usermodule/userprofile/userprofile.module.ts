import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UsercomponentsModule } from '../../components/usercomponents/usercomponents.module';

import { IonicModule } from '@ionic/angular';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { UserprofilePage } from './userprofile.page';

const routes: Routes = [
  {
    path: '',
    component: UserprofilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,UsercomponentsModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserprofilePage]
})
export class UserprofilePageModule {}
