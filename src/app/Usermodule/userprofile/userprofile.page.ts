import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import {SimpleGlobal} from 'ng2-simple-global';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.page.html',
  styleUrls: ['./userprofile.page.scss'],
})
export class UserprofilePage implements OnInit {
  headerData:any;
  constructor(private router: Router,public navCtrl: NavController,private sg: SimpleGlobal) { 
   // console.log(this.sg["editableuser_deails"]);
  }

  ngOnInit() {
    this.createHeader();
  }
  backto_dashboard(){
    this.navCtrl.navigateRoot('/farm-dashboard');
  }
    createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'My Profile';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'నా జీవన వివరణ';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'என் சுயவிவரம்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಸ್ವ ಭೂಮಿಕೆ';
    }else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'मेरी प्रोफाइल';
    }
    this.headerData = {
      color: 'green',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      button2: 'notifications',
      button2_action: '/notifications',
      notification_count: this.sg['notificationscount'],
       };
   }
 
}
