import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasysignupPage } from './farmeasysignup.page';

const routes: Routes = [
  {
    path: '',
    component: FarmeasysignupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasysignupPage]
})
export class FarmeasysignupPageModule {}
