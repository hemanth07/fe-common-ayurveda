import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from './../../languages/english.service';
import { HindiService } from './../../languages/hindi.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { snapshotToArray } from './../../languages/others.service';
import { FarmeasyTranslate } from 'src/app/translate.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  currentpass: String;
  newpass: String;
  confirmpass: String;
  changepasswordForm: any = FormGroup
  submitAttempt: boolean = false;
  httpOptions = {};
  error1: string;
  error2: string;
  errorflag: boolean = false;
  errorflag1: boolean = false;
  headerData: any;
  footerData: any;
  labels: any;
  filePath: any = [];
  tables:any = [];
  weeksTable:any = [];
  constructor(
    public navCtrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
    private sg: SimpleGlobal,
    private storage: Storage,
    public toastController: ToastController,
    public loadingCtrl: LoadingController,
    private en: EnglishService,
    private hi: HindiService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService,
    private location: Location,
    public form: FormBuilder,
    private translate : FarmeasyTranslate
  ) {
    this.changepasswordForm = form.group({
      currentpass: ['', Validators.required],
      newpass: ['', Validators.required],
      confirmpass: ['', Validators.required]
    });

  }

  ngOnInit() {
    // this.filePath = [];
    this.createHeaderFooter();
  }
  createHeaderFooter() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Change Password';
      this.labels = {
        current: "Current Password",
        new: 'New Password',
        confirm: 'Confirm Password',
        error1: 'Current Password is Invalid',
        error2: "Confirm Password is Invalid",
        currenterror: "Please Enter Current Password",
        newerror: "Please Enter New Password",
        confirmerror: "Please Enter Confirm Password"
      }

    } else if (this.sg['userdata'].primary_language === 'kn') {
      title = 'ಗುಪ್ತಪದವನ್ನು ಬದಲಿಸಿ';
      this.labels = {
        current: "ಪ್ರಸ್ತುತ ಪಾಸ್‌ವರ್ಡ್",
        new: 'ಹೊಸ ಪಾಸ್‌ವರ್ಡ್',
        confirm: 'ಪಾಸ್ವರ್ಡ್ ದೃಢೀಕರಿಸಿ',
        error1: 'ಪ್ರಸ್ತುತ ಪಾಸ್‌ವರ್ಡ್ ಅಮಾನ್ಯವಾಗಿದೆ',
        error2: "ಪಾಸ್ವರ್ಡ್ ಅಮಾನ್ಯವಾಗಿದೆ ಎಂದು ಖಚಿತಪಡಿಸಿ",
        currenterror: "ದಯವಿಟ್ಟು ಪ್ರಸ್ತುತ ಪಾಸ್‌ವರ್ಡ್ ಅನ್ನು ನಮೂದಿಸಿ",
        newerror: "ದಯವಿಟ್ಟು ಹೊಸ ಪಾಸ್‌ವರ್ಡ್ ನಮೂದಿಸಿ",
        confirmerror: "ಪಾಸ್ವರ್ಡ್ ಖಚಿತಪಡಿಸಿ ದಯವಿಟ್ಟು ನಮೂದಿಸಿ"
      }
    } else if (this.sg['userdata'].primary_language === 'ta') {
      title = 'கடவுச்சொல்லை மாற்று';
      this.labels = {
        current: "தற்போதைய கடவுச்சொல்",
        new: 'புதிய கடவுச்சொல்',
        confirm: 'கடவுச்சொல்லை உறுதிப்படுத்தவும்',
        error1: 'தற்போதைய கடவுச்சொல் தவறானது',
        error2: "கடவுச்சொல் தவறானது என்பதை உறுதிப்படுத்தவும்",
        currenterror: "தற்போதைய கடவுச்சொல்லை உள்ளிடவும்",
        newerror: "புதிய கடவுச்சொல்லை உள்ளிடவும்",
        confirmerror: "கடவுச்சொல்லை உறுதிப்படுத்தவும்"
      }
    } else if (this.sg['userdata'].primary_language === 'te') {
      title = 'పాస్వర్డ్ మార్చండి';
      this.labels = {
        current: "ప్రస్తుత పాస్‌వర్డ్",
        new: 'క్రొత్త పాస్‌వర్డ్',
        confirm: 'పాస్‌వర్డ్‌ను నిర్ధారించండి',
        error1: 'ప్రస్తుత పాస్‌వర్డ్ చెల్లదు',
        error2: "పాస్‌వర్డ్ చెల్లదని నిర్ధారించండి",
        currenterror: "దయచేసి ప్రస్తుత పాస్‌వర్డ్‌ను నమోదు చేయండి",
        newerror: "దయచేసి క్రొత్త పాస్‌వర్డ్‌ను నమోదు చేయండి",
        confirmerror: "దయచేసి పాస్‌వర్డ్‌ను నిర్ధారించండి"
      }
    } else if (this.sg['userdata'].primary_language === 'hi') {
      title = 'पासवर्ड बदलें';
      this.labels = {
        current: "करंट पासवर्ड",
        new: 'नया पासवर्ड',
        confirm: 'पासवर्ड की पुष्टि करें',
        error1: 'वर्तमान पासवर्ड अमान्य है',
        error2: "पासवर्ड की पुष्टि करें अमान्य है",
        currenterror: "कृपया वर्तमान पासवर्ड दर्ज करें",
        newerror: "कृपया नया पासवर्ड दर्ज करें",
        confirmerror: "कृपया पासवर्ड की पुष्टि करें"
      }
    }
    this.headerData = {
      color: 'green',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
    };
    this.footerData = {
      'footerColor': 'rgba(255, 255, 255, 0.9)',
      'middleButtonName': 'Submit',
      'middleButtonColor': 'green',
    };

// this.translating();
  }
  
  translating() {
    let obj = {
      date:"Date of Transaction",
      bill_no:"Bill No:",
      vendor_name:"Vendor Name",
      description:"Description",
      uom:"UOM",
      qty:"QYT",
      total:"Total",
      add_bill:"Add Bill",
      review:"Review",
      back:"Back",
      save:"Save",
      reconcile:"Reconcile",
      fixed_assets:"Add To Fixed Asset",
      added_fixed_assets:"Added to fixed Asset"
     }
    this.translate.translateObject(obj,'en','te').then(data => {
      console.log(JSON.stringify(data));
    });
    this.translate.translateObject(obj,'en','ta').then(data => {
      console.log(JSON.stringify(data));
    });
    this.translate.translateObject(obj,'en','kn').then(data => {
      console.log(JSON.stringify(data));
    });
    this.translate.translateObject(obj,'en','hi').then(data => {
      console.log(JSON.stringify(data));
    });
  }

  checkpassword() {
    // if(this.sg['userdata'].password == this.currentpass){
    //     if(this.newpass == this.confirmpass){
    this.changePassword();
    //   }
    //   else{
    //     this.errorflag = true;
    //     this.error2 = "Confirm Password is Invalid"
    //   }
    // }
    // else{
    //   this.errorflag1 = true;
    //   this.error1 = "Current Password is Invalid"
    // }
  }


  changePassword() {
    this.submitAttempt = true;

    if (this.changepasswordForm.valid) {



      if (this.sg['userdata'].password == this.currentpass) {
        if ((this.sg['userdata'].password == this.currentpass) && this.currentpass != this.newpass) {
          if (this.newpass == this.confirmpass) {
            this.sg['userdata'].password = this.confirmpass;
            if (this.sg['userdata'].key) {
              firebase.database().ref('farmeasy_users').orderByChild('farmeasy_id').equalTo(this.sg['userdata'].farmeasy_id).once('value', resp => {
                let data = snapshotToArray(resp);
                console.log(data);
                firebase.database().ref('farmeasy_users/' + data[0].key).update({ password: this.confirmpass });
              });
              firebase.database().ref(this.sg['userdata'].vendor_id + '/users/' + this.sg['userdata'].key).update({ password: this.confirmpass }).then(() => {
                this.storage.get('farmeasy_userdata').then((val) => {
                  let temp = val;
                  temp.password = this.confirmpass;
                  this.storage.set('farmeasy_userdata', temp);
                  this.presentToast('Password Changed Please login with new password');
                  this.Logout_app();
                  // this.location.back();
                });
              });
            }
          } else {
            this.errorflag = true;
            this.error2 = "Confirm Password is Invalid"
          }
        } else {
          this.presentToast('Current Password and changed password is same');
        }
      } else {
        this.errorflag1 = true;
        this.error1 = "Current Password is Invalid"
      }
    }
  }

  Logout_app() {
    firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + this.sg["userdata"].key).update({device_id:null});
    this.storage.remove("farmeasy_userdata");
    this.sg["userdata"] = '';
    this.storage.get('farmeasy_userdata').then((val) => {
      console.log('Your age is', val);
    });
    this.sg['notificationscount'] = '';
    // this.router.navigateByUrl('/farmeasysplash');
    this.router.navigateByUrl('/farmeasysignin');
  }
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
  clearerror() {
    this.errorflag = false;
    this.errorflag1 = false;
    this.error1 = null;
    this.error2 = null;
  }


}
