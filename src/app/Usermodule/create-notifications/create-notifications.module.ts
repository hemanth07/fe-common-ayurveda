import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UsercomponentsModule } from '../../components/usercomponents/usercomponents.module';

import { IonicModule } from '@ionic/angular';

import { CreateNotificationsPage } from './create-notifications.page';
import { ObservationComponentsModule } from './../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: CreateNotificationsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,UsercomponentsModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreateNotificationsPage]
})
export class CreateNotificationsPageModule {}
