import { Component, OnInit } from '@angular/core';
import { NavController,ToastController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from './../../languages/english.service';
import { HindiService } from './../../languages/hindi.service';
import { OtherService } from './../../languages/others.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { Location } from '@angular/common';
import { FarmeasyTranslate } from 'src/app/translate.service';
@Component({
  selector: 'app-create-notifications',
  templateUrl: './create-notifications.page.html',
  styleUrls: ['./create-notifications.page.scss'],
})
export class CreateNotificationsPage implements OnInit {
  page_status:any;
  title: string;
  headerData: any;
  footerData: any;
  farmeasy_ref = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/');
  prioritys :any = [];
  Users: any;
  blocks:any=[];
  categories:any=[];
  selected_user: any = [];
  finalusers: any = [];
  selectedblock: any;
  selectedcategoty: any;
  selectedpriority: any;
  selectedLabour: any;
  description: any;
  errormessage: boolean = true;
  user_data: any = {};
  date: any;
  notification_body = [];
  not_key: any;
  image_date: any;
  image_data: any='';
  images_data:any='';
  video:any='';
  Labourers: any;
  checkedstatus: boolean = false;
  SelectAll: any;
  labels:any = {};
  sendStatus: boolean = false;
  constructor( public navCtrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
    private sg: SimpleGlobal,
    private storage: Storage,
 public toastController:ToastController,
 public alertController: AlertController,
 public loadingCtrl: LoadingController,
 private en: EnglishService,
 private hi: HindiService,
 private ta:TamilService,
 private te:TeluguService,
 private kn:KanadaService,
 private other:OtherService,
 private translate:FarmeasyTranslate,
 private location:Location
    ) { }
   ngOnInit() {
    this.translation();
    this.date = Date.now();
    this.image_date = new Date().toString();
    this.storage.get('farmeasy_userdata').then((val) => {
      console.log('Your age is', val);
      this.user_data = val;
      if (this.user_data != "undefined") {
        firebase.database().ref(this.sg['userdata'].vendor_id+'/users/').on('value', resp => {
          let users = snapshotToArray(resp);
          this.Users = users;
          this.Labourers = users;
          for (let i = 0; i < this.Users.length; i++) {
            this.selected_user[i] = false;
            if (this.Users[i].phone == this.user_data.phone) {
              this.Users.splice(i, 1);
            }
          }
          this.translationUsers();
        });
      //  console.log(JSON.stringify(this.Users));
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').on('value', resp => {
          let blocks = snapshotToArray(resp);
          this.blocks = blocks;
          console.log(blocks);
          //that.editData();
        });
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').on('value', resp => {
          let categories = snapshotToArray(resp);
          this.categories = categories;
         // console.log(categories);
        });
      }
    });

    let sub = this.route.params.subscribe(params => {
      this.page_status =JSON.parse(params["page_status"]); 
     // console.log(this.page_status);
 });
    this.createHeader();
  }
translationUsers(){
  let labours = [];
  for (let item of this.Users) {
    let obj = {
      name_label: item.name,
    }
    labours.push(obj)
  }
  for (let i = 0; i < labours.length; i++) {
    if (this.sg['userdata'].primary_language !== 'en') {
      this.translate.translateObject(labours[i],'en',this.sg['userdata'].primary_language).then(data => {
        this.Users[i]['name_label'] = data['name_label'];
      });
    } else {
      this.Users[i]['name_label'] = labours[i]['name_label'];

    }
  }
}
 
  createHeader(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.title = 'Create New notification';
      this.prioritys = [
        {value:"Urgent", name:"Urgent"},
        {value:"High", name:"High"},
        {value:"Medium", name:"Medium"},
        {value:"Low", name:"Low"},
      ]
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.title = 'ಹೊಸ ಪ್ರಕಟಣೆ ರಚಿಸಿ';
      this.prioritys =  [
        {value:"Urgent", name:"ತುರ್ತು"},
        {value:"High", name:"ಹೆಚ್ಚು"},
        {value:"Medium", name:"ಮಾಧ್ಯಮ"},
        {value:"Low", name:"ಕಡಿಮೆ"},
      ]
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.title = 'புதிய அறிவிப்பை உருவாக்கவும்';
      this.prioritys = [
        {value:"Urgent", name:"அவசர"},
        {value:"High", name:"உயர்"},
        {value:"Medium", name:"நடுத்தர"},
        {value:"Low", name:"குறைந்த"},
      ]
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.title = 'క్రొత్త నోటిఫికేషన్ సృష్టించు';
      this.prioritys = [
        {value:"Urgent", name:"అర్జంట్"},
        {value:"High", name:"అధిక"},
        {value:"Medium", name:"మీడియం"},
        {value:"Low", name:"తక్కువ"},
      ]
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.title = 'नई अधिसूचना बनाएँ';
      this.prioritys = [
        {value:"Urgent", name:"अति आवश्यक"},
        {value:"High", name:"उच्च"},
        {value:"Medium", name:"मध्यम"},
        {value:"Low", name:"कम"},
      ]
    }
    this.headerData = {
      color: 'green',
      title: this.title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };

      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.9)',
        'middleButtonName':this.labels.submit,
        'middleButtonColor':'green',
        };
  }
translation(){
  let data;
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels = this.en.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'te'){
      this.labels = this.te.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'ta'){
      this.labels = this.ta.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'kn'){
      this.labels = this.kn.getCreateNotificationLabels();
      } else if(this.sg['userdata'].primary_language === 'hi'){
      this.labels = this.hi.getCreateNotificationLabels();
      }

}

  selectuser(id) {
    for (let i = 0; i < this.Users.length; i++) {
      if (i == id) {
        if (this.selected_user && this.selected_user.length > 0) {
          for (let j = 0; j < this.selected_user.length; j++) {
            if (j == id) {
              if (this.selected_user[j] == true) {
                this.selected_user[id] = false;
                if (this.finalusers && this.finalusers.length) {
                  for (let s = 0; s < this.finalusers.length; s++) {
                    if (this.finalusers[s].name == this.Users[i].name) {
                      this.finalusers.splice(s, 1);
                    }
                  }
                }
              }
              else {
                this.selected_user[id] = true;
                this.finalusers.push({ "name": this.Users[i].name, "key": this.Users[i].key,device_id:this.Users[i].device_id });
              }
            }
          }
        }
        this.SelectAll = this.selected_user.every(function (item: any) {
          return item == true;
        })
       // console.log(this.finalusers);
      }
    }
  }

  SelectAllUsers() {
    this.finalusers = [];
    for (var i = 0; i < this.Users.length; i++) {
      this.selected_user[i] = true;
      this.selected_user[i] = this.SelectAll;
      if (this.selected_user[i] == true) {
        this.finalusers.push({ "name": this.Users[i].name, "key": this.Users[i].key ,device_id:this.Users[i].device_id});
      }
    }
   // console.log(this.finalusers);
  }
  descriptionListen(){
    this.sendStatus = false;
  }
  priorityListen(){
    this.sendStatus = false;
  }
 async sendnotification() {
    if (this.finalusers && this.finalusers.length > 0 && this.selectedpriority  && this.description) {
      this.errormessage = true;
      this.sendStatus = true;
      for (let i = 0; i < this.finalusers.length; i++) {
        let user_name = this.finalusers[i].name;
        this.notifications(this.finalusers[i].key);
        let body = {device_id:this.finalusers[i].device_id,description:this.description}
        this.other.sendNotification(body);
        if(i == this.finalusers.length-1){
          this.backFromCreation();
        }
      }
      this.senderMessage();
    }
    else {
      const alert = await this.alertController.create({
      subHeader: 'Error',
      message: this.labels.alert,
      buttons: ['OK']
    });
    await alert.present();
      // this.errormessage = false;
    }
  }
 async backFromCreation(){
    const toast = await this.toastController.create({
        message: 'Notification Sent Successfully',
        duration: 2000
      });
      toast.present();
    this.location.back();
  }
  senderMessage(){
    let media={'camera':this.image_data,'attachments':this.images_data,'video':this.video}
    let body: any;
    let that = this;
    body = {
      "Priority": this.selectedpriority,
      "notiification_description": this.description,
      "createdat": this.date,
      "sendername": this.user_data.name,
      "senderimage": this.user_data.profile_url,
      "senderkey":this.user_data.key,
      "media":media,
      "sender":true,
      "read":true,
    };
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' +this.sg['userdata'].key + '/notifications').push(body);
  }

  //------------------------------------Send Notification----------------------------------------//

  notifications(key) {
    let media={'camera':this.image_data,'attachments':this.images_data,'video':this.video}
    let body: any;
    let user_key = key;
    let that = this;
    body = {
      "Priority": this.selectedpriority,
      "notiification_description": this.description,
      "createdat": this.date,
      "sendername": this.user_data.name,
      "senderimage": this.user_data.profile_url,
      "senderkey":this.user_data.key,
      "media":media,
      "status": "Acknowledge",
      "notification_status": "Mark as Done",
    };
    let newData = firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + user_key + '/notifications').push(body).then(async() =>{
      

    });
  }
 presentRemoveAlert(data,type){
    if(type == 'camera'){
      firebase.storage().ref(data.path.name).delete();
      this.image_data = {};
    }
    else{
      firebase.storage().ref(data.path.name).delete();
      this.video = {};
    }
 }

 
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
