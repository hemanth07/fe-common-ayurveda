import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UsercomponentsModule } from '../../components/usercomponents/usercomponents.module';

import { IonicModule } from '@ionic/angular';

import { ViewnotificationPage } from './viewnotification.page';
import { ObservationComponentsModule } from './../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: ViewnotificationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,UsercomponentsModule,
    IonicModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewnotificationPage]
})
export class ViewnotificationPageModule {}
