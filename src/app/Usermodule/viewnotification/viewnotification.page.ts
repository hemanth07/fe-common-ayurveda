import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import {SimpleGlobal} from 'ng2-simple-global';
@Component({
  selector: 'app-viewnotification',
  templateUrl: './viewnotification.page.html',
  styleUrls: ['./viewnotification.page.scss'],
})
export class ViewnotificationPage implements OnInit {
  key: any;
  page_status:any;
  title:any;
  headerData:any;
  constructor(private router: Router,private route: ActivatedRoute,
    public navCtrl: NavController,
    public sg:SimpleGlobal) { 

    }

  ngOnInit() {
     let sub = this.route.params.subscribe(params => {
      this.key =JSON.parse(params["notification_key"]); 
      // this.page_status =JSON.parse(params["page_status"]); 
     // console.log(this.key);
 });
     if(this.sg['userdata'].primary_language === 'en'){
      this.title = 'View Notification';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.title = 'ಅಧಿಸೂಚನೆ ವೀಕ್ಷಿಸಿ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.title = 'அறிவிப்பைக் காட்டு';
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.title = 'నోటిఫికేషన్ను వీక్షించండి';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.title = 'अधिसूचना देखें';
    }
    this.headerData = {
      color: 'green',
      title: this.title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
  }
  //   backto_dashboard(){
  //   let data={"page_status":JSON.stringify(this.page_status)}
  //   this.navCtrl.navigateForward(['/notifications',data]);
  // }
}
