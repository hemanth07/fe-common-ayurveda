import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';

@Component({
  selector: 'app-assets-management-menu',
  templateUrl: './assets-management-menu.page.html',
  styleUrls: ['./assets-management-menu.page.scss'],
})
export class AssetsManagementMenuPage implements OnInit {
  Harvest_data: any = [];
  id: any;
  title :string;
  my_lang: string;
  headerData: any;
  constructor(private router: Router,
    public navCtrl: NavController, private route: ActivatedRoute,
    public sg: SimpleGlobal, private translate:FarmeasyTranslate,
    private en: EnglishService,
    private hi: HindiService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService
   ) {
  }

  ngOnInit() {
    this.translation();
    this.route.params.subscribe(params => {
      this.id = params;
    });
  }

  selected_harvest(data) {
    this.sg["type"] = data.keyword;
    this.navCtrl.navigateForward(data.path);
  }

  translation(){
      if(this.sg['userdata'].primary_language === 'en'){
        this.Harvest_data  = this.en.getAssestsManagementMenu();
        this.title = 'Asset Management';
      } else if(this.sg['userdata'].primary_language === 'te'){
        this.Harvest_data  = this.te.getAssestsManagementMenu();
        this.title = 'ఆస్తి నిర్వహణ';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        this.Harvest_data  = this.ta.getAssestsManagementMenu();
        this.title = 'சொத்து மேலாண்மை';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        this.Harvest_data  = this.kn.getAssestsManagementMenu();
        this.title = 'ಆಸ್ತಿ ನಿರ್ವಹಣೆ';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        this.Harvest_data  = this.hi.getAssestsManagementMenu();
        this.title = 'परिसंपत्ति प्रबंधन';
      } else {
         this.Harvest_data  = this.en.getAssestsManagementMenu();
         this.title = 'Asset Management';
      }
   
   
    this.headerData = {
      title: this.title,
      color: "pink",
      button1: 'home',
      button1_action: '/farm-dashboard',
    };
  }


}
