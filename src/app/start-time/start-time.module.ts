import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StartTimePage } from './start-time.page';
import { NgCalendarModule } from 'ionic2-calendar';
import { ContactComponent } from '../components/contact/contact.component';
import { SelectDateComponent } from '../components/task-manger-components/select-date/select-date.component';
import { SelectPlotsBlockComponent } from '../components/task-manger-components/select-plots-block/select-plots-block.component';
import { SelectSourcePopOverComponent } from '../components/task-manger-components/select-source-pop-over/select-source-pop-over.component';
import { ObservationComponentsModule } from '../components/observation-components/observation-components.module';
import { TaskManagerComponentsModule } from '../components/task-manger-components/task-mananger-components.module';
import { UsercomponentsModule } from '../components/usercomponents/usercomponents.module';
import { ComponentsModule } from '../components/components.module';

const routes: Routes = [
  {
    path: '',
    component: StartTimePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    UsercomponentsModule,
    ComponentsModule,
    ReactiveFormsModule,
    TaskManagerComponentsModule,
    NgCalendarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StartTimePage],
  entryComponents: [SelectPlotsBlockComponent,SelectSourcePopOverComponent,SelectDateComponent,ContactComponent
  ]
})
export class StartTimePageModule {}
