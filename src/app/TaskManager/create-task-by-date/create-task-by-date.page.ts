import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { NavController, PopoverController, AlertController, ToastController } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import { OverlayEventDetail } from '@ionic/core';
import { CalendercomponentComponent } from './../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { OtherService } from '../../languages/others.service';

@Component({
  selector: 'app-create-task-by-date',
  templateUrl: './create-task-by-date.page.html',
  styleUrls: ['./create-task-by-date.page.scss'],
})
export class CreateTaskByDatePage implements OnInit {
  taskManager_Task_table = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table');
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable');
  headerData: any;
  laboursData:any = [];
  blocks:any = [];
  discription: any = null;
  date: any;
  block: any = [];
  labour: any;
  priority: any; 
  taskByDate: any = [];
  task_logData: any = [];
  today_date: any;
  date_in_ms: any;
  toDate: any;
  selectedDateInTime: any;
  footerData:any;
  editTask: any;
  count: any = 0;
  count1: any = 0;
  isLabour: boolean = false;
  isDate: boolean = false;
  audio: any;
  selectedDate: any;
  video: any;
  camera: any;
  attachments: any = [];
  labels: any;
  showBlock = true;
  isCreatedTask:any;
  isdraft:any;
  success_msg:string;
  constructor(private navCtrl: NavController, private popoverController: PopoverController, private location: Location,
    private sg: SimpleGlobal, private route: ActivatedRoute, private alertController: AlertController,
    public toastController: ToastController,public other:OtherService) { }

  ngOnInit() {
    let date_string = new Date().toISOString().split("T")[0]
    let today_date = new Date(date_string);
    this.selectedDateInTime = today_date.getTime();
    this.toDate = "DD-MON-YYYY";
    this.editTask = JSON.parse(this.route.snapshot.paramMap.get('data'));
    console.log(this.editTask);
    let data
    if (this.sg['userdata'].primary_language === 'en') {
     this.success_msg  = "Success"
     data = 'Create Task - By Date';
      this.labels = { labourerrmsg: 'Please Select Labour', create: 'Create Task', dateerrmsg: 'Please Select Date',description:'Task Description'}
    } else if (this.sg['userdata'].primary_language === 'te') {
     this.success_msg  = "విజయం"
     data = 'టాక్సు సృష్టించు - లేబర్ ద్వారా';
      this.labels = { labourerrmsg: 'దయచేసి లేబర్ మరియు తేదీని ఎంచుకోండి', create: 'పనిని సృష్టించండి', dateerrmsg: 'దయచేసి  తేదీని ఎంచుకోండి',description:'టాస్క్ వివరణ'}
    } else if (this.sg['userdata'].primary_language === 'ta') {
     this.success_msg  = "வெற்றி"
     data = 'பணியை உருவாக்குதல் - தொழில் மூலம்';
      this.labels = { labourerrmsg: 'தொழில் மற்றும் தேதி தேர்ந்தெடுக்கவும்', create: 'பணி உருவாக்க' ,description:'பணி விளக்கம்'}
    } else if (this.sg['userdata'].primary_language === 'kn') {
     this.success_msg  = "ಯಶಸ್ಸು"
     data = 'ಕಾರ್ಯವನ್ನು ರಚಿಸಿ - ಕಾರ್ಮಿಕರಿಂದ';
      this.labels = { labourerrmsg: 'ದಯವಿಟ್ಟು ಕಾರ್ಮಿಕ ಮತ್ತು ದಿನಾಂಕವನ್ನು ಆಯ್ಕೆಮಾಡಿ', create: 'ಕೆಲಸವನ್ನು ರಚಿಸಿ',description:'ಕಾರ್ಯ ವಿವರಣೆ' }
    } else if (this.sg['userdata'].primary_language === 'hi') {
     this.success_msg  = "सफलता"
     data = 'कार्य बनाएँ - श्रम द्वारा';
      this.labels = { labourerrmsg: 'कृपया श्रम और तिथि का चयन करें', create: 'कार्य बनाएँ',description:'कार्य विवरण'}
    }
    this.headerData = {
      'title': data, 'back': '/tm-mainmenu',button1: 'home',button1_action: '/farm-dashboard',
      'color': 'green'
    };
    // this.footerData={'submit':'CREATE TASK','draft':'SAVE DRAFT','submitColor':'green',
    // 'footerColor':'rgba(255,255,255,0.9)','submit_right':'22px','draftColor':'brown' ,'submitDissable':true}
    // this.footerData= {
    //   'footerColor':'rgba(255,255,255,0.9)',
    //   'leftButtonName':'CREATE TASK',
    //   'rightButtonName':'SAVE DRAFT',
    //   'leftButtonColor':'green',
    //   'rightButtonColor':'grey'
    //   }
      this.footerData= {
      'footerColor':'rgba(255,255,255,0.9)',
      'leftButtonName':'CREATE TASK',
      'leftButtonColor':'green',
      }

      this.today_date = new Date(date_string);
      this.date_in_ms = this.today_date.getTime();
    if (this.editTask) {
      this.assignMedia();
    }
    else {
      this.taskByDate['media'] = {};
    }

  }

  ionViewDidEnter(){
    let that = this;
    // firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').on('value', resp => {
    //   this.blocks = snapshotToArray(resp);
    // });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').on("value", function (Data) {
      that.laboursData = snapshotToArray(Data);
      console.log(that.laboursData);
     });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.success_msg,
      duration: 2000
    });
    toast.present();
  }
  assignMedia() {
    if (this.editTask && this.editTask.media) {
      this.taskByDate['media'] = this.editTask.media;
    }
    this.selectedDateInTime = this.editTask.end_date;
  }
  getAdditionalNotes(val) {
    this.discription = val;
  }
  getRecordedAudio(val) {
    console.log(val);

    this.audio = val;
    this.taskByDate.media['audio'] = val;
    //alert(this.taskByDate.media.audio.url);
  }
  getVideo(val) {
    this.video = val;
    this.taskByDate.media['video'] = val;
    //alert(this.taskByDate.media.video.url);
  }
  getCameraImage(val) {
    this.camera = val;
    this.taskByDate.media['camera'] = val;
    //alert(this.taskByDate.media.camera.url);
  }
  getAttachments(val) {
    this.attachments.push(val);
    this.taskByDate.media['attachments'] = [];
    this.taskByDate.media['attachments'] = this.attachments;
    //alert(this.taskByDate.media.attachments.url);
  }
  getDate(val) {
    this.date = val;
  }
  getBlock(val) {
    this.block = [];
    this.block.push({ "name": val });
  }

  getLabour(val) {
    console.log(val);
    if (val) {
      this.labour = val;
      this.isLabour = false;
    }
    else this.isLabour = true;
  }
  // getPriority(val) {
  //   this.priority = val;
  // }
  async openDateCalender() {
    const popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popover_class",
      componentProps: {
        'startDate': this.toDate
      },

    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getSelectedDate(detail.data);
    });
    await popover.present();
  }
  getSelectedDate(val) {
    if (val) {
      let today_date = new Date(val);
      let year = today_date.getFullYear()
      let month = today_date.getMonth() + 1;
      let date = today_date.getDate();
      console.log(val);
      console.log(year);
      this.toDate = year + "-" + month + "-" + date;
      this.selectedDateInTime = today_date.getTime();
      this.selectedDate = date + '-' + month + '-' + year;
      this.isDate = false;
    }
    else {
      this.isDate = true;
    }

  }
  getPriority(val) {
    this.priority = val;
  }
  async closeImage(val,idx){
    let message;
    let header;
    let ok;
    let cancel;
    if(this.sg['userdata'].primary_language === 'en'){
      header = "Confirm!"
      message  = "Are you sure want to delete this "
      ok = "Okay"
      cancel = "Cancel"
     } else if(this.sg['userdata'].primary_language === 'te'){
      header = "కన్ఫర్మ్!"
      message  = "మీరు దీన్ని ఖచ్చితంగా తొలగించాలనుకుంటున్నారా "
      cancel = "కాదు"
      ok = "సరే"
     } else if(this.sg['userdata'].primary_language === 'ta'){
      header = "உறுதிசெய்!"
      message  = "இதை நிச்சயமாக நீக்க விரும்புகிறீர்களா "
      cancel = "இல்லை"
      ok = "சரி"
     } else if(this.sg['userdata'].primary_language === 'kn'){
      header = "ಖಚಿತಪಡಿಸಿ!"
      message  = "ಇದನ್ನು ಅಳಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ "
      cancel = "ಇಲ್ಲ"
      ok = "ಸರಿ"
     } else if(this.sg['userdata'].primary_language === 'hi'){
      header = "पुष्टि करें!"
      message  = "क्या आप वाकई इसे हटाना चाहते हैं "
      cancel = "नहीं"
      ok = "ठीक है"
     }
    const alert = await this.alertController.create({
      header: header,
      message: message+ val +'?',
      buttons: [
        {
          text: cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: ok,
          handler: () => {
            this.removeAttachment(val,idx);
            console.log('Confirm Okay');
          }
        }
      ]
    });
  
    await alert.present();
  }
  removeAttachment(val, idx) {
    let that = this;
    if (val == 'audio') {
      for (var i = 0; i < this.taskByDate.media.audio.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByDate.media.audio[i].path).delete()
          that.taskByDate.media.audio.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef.update(that.taskByDate.media);
          }


        }
      }

    }
    if (val == 'video') {
      for (var i = 0; i < this.taskByDate.media.video.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByDate.media.video[i].path).delete()
          that.taskByDate.media.video.splice(idx, 1);
          if (that.editTask) {
            var playersRef1 = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef1.update(that.taskByDate.media);
          }
        }
      }

    }
    if (val == 'camera') {
      for (var i = 0; i < this.taskByDate.media.camera.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByDate.media.camera[i].path).delete()
          that.taskByDate.media.camera.splice(idx, 1);
          if (that.editTask) {
            var playersRef2 = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef2.update(that.taskByDate.media);
          }
        }

      }
    }
    if (val == 'attachments') {
      for (var i = 0; i < this.taskByDate.media.attachments.length; i++) {
        if (i == idx) {
          this.taskByDate.media.attachments[i] = null;

          firebase.storage().ref(that.taskByDate.media.attachments[i].path.name).delete()
          that.taskByDate.media.attachments.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef.update(that.taskByDate.media);
          }
        }
      }
      console.log(this.taskByDate.media.attachments);
    }
  }
  async createTask(val) {
    let labour;
    // let block;
    let priority;
    let date;
    let error_label;
    let select;
    let okay;
    if(this.sg['userdata'].primary_language === 'en'){
      labour = "Labour";
      // block = "Block";
      priority = "Priority";
      date = "Date"
      error_label = "Error";
      select = "Select "
      okay = "Okay"
     } else if(this.sg['userdata'].primary_language === 'te'){
      labour = "లేబర్";
      // block = "బ్లాక్";
      priority = "ప్రాధాన్యత";
      date = "తేదీ"
      error_label = "లోపం";
      select = "ఎంచుకోండి "
      okay = "సరే"
     } else if(this.sg['userdata'].primary_language === 'ta'){
      labour = "தொழிலாளர்";
      // block = "பிளாக்";
      priority = "முன்னுரிமை";
      date = "தேதி"
      error_label = "பிழை";
      select = "தேர்வு "
      okay = "சரி"
     } else if(this.sg['userdata'].primary_language === 'kn'){
      labour = "ಕಾರ್ಮಿಕ";
      // block = "ಬ್ಲಾಕ್";
      priority = "ಆದ್ಯತೆ";
      date = "ದಿನಾಂಕ"
      error_label = "ದೋಷ";
      select = "ಆಯ್ಕೆ ಮಾಡಿ "
      okay = "ಸರಿ"
     } else if(this.sg['userdata'].primary_language === 'hi'){
      labour = "श्रम";
      // block = "खंड";
      priority = "प्राथमिकता";
      date = "दिनांक"
      error_label = "त्रुटि";
      select = "चुनते हैं "
      okay = "ठीक है"
     }
    let error = [];
   if(!this.labour){
      error.push(labour);
   }
  //  if(this.block.length<=0){
  //      error.push(block);
  //  } 
    if(!this.priority){
       error.push(priority);
   } 
    if(!this.selectedDateInTime){
       error.push(date);
   }
    
   if(error.length == 0){
    let message;
    let header;
    let ok;
    let cancel;
    if(this.sg['userdata'].primary_language === 'en'){
      header = "Confirm!"
      message  = "Are you sure want to create task without description"
      ok = "Okay"
      cancel = "Cancel"
     } else if(this.sg['userdata'].primary_language === 'te'){
      header = "కన్ఫర్మ్!"
      message  = "మీరు ఖచ్చితంగా వివరణ లేకుండా పనిని సృష్టించాలనుకుంటున్నారా"
      cancel = "కాదు"
      ok = "సరే"
     } else if(this.sg['userdata'].primary_language === 'ta'){
      header = "உறுதிசெய்!"
      message  = "விளக்கம் இல்லாமல் பணியை உருவாக்க விரும்புகிறீர்களா?"
      cancel = "இல்லை"
      ok = "சரி"
     } else if(this.sg['userdata'].primary_language === 'kn'){
      header = "ಖಚಿತಪಡಿಸಿ!"
      message  = "ವಿವರಣೆಯಿಲ್ಲದೆ ಕಾರ್ಯವನ್ನು ರಚಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ?"
      cancel = "ಇಲ್ಲ"
      ok = "ಸರಿ"
     } else if(this.sg['userdata'].primary_language === 'hi'){
      header = "पुष्टि करें!"
      message  = "क्या आप वाकई वर्णन के बिना कार्य बनाना चाहते हैं"
      cancel = "नहीं"
      ok = "ठीक है"
     }
    if(!this.discription){
      const alert = await this.alertController.create({
        header: header,
        message:message,
        buttons: [
          {
            text: cancel,
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: ok,
            handler: () => {
              this.submitTask(val);
            }
          }
        ]
      });
      await alert.present();
    }else{
      this.submitTask(val);
    }
   } else {
      const alert = await this.alertController.create({
      header: error_label,
      message: select+error.join(","),
      buttons: [okay]
    });
    await alert.present();
   }
  //   if(val=='rightButtonAction' && (this.labour || this.block || this.priority || this.discription)){
  //     //this.isdraft=false;
  //     this.submitTask(val);
  //   }
  //   if( val=='leftButtonAction' && this.labour ){
     
  //     this.submitTask(val);
  // }else{
  //     if(val=='leftButtonAction' && !this.labour){
  //     this.isLabour=true;
  //     }
  //     if(val=='leftButtonAction' ){
  //       this.isDate=true;
  //     }
      
  //   }
  }
  submitTask(val){
      
    let that = this;
    this.taskByDate["assignee"] = this.labour ? this.labour : null;
    // this.taskByDate["blocks"] = this.block;
    this.taskByDate["priority"] = this.priority;
    // this.taskByDate["end_date"] = this.selectedDateInTime;
    this.taskByDate["start_date"] = this.selectedDateInTime;
    this.taskByDate["date"] = this.selectedDateInTime;
    this.taskByDate["description"] = this.discription;
    this.taskByDate['category'] = { "name": '' };
    if (!this.editTask) {
      if(val=='leftButtonAction'){
      this.taskByDate['status'] = { name: 'Acknowledge' };
      }else if(val=='rightButtonAction'){
        this.taskByDate['status'] = { name: 'Draft' };
      }
      this.taskByDate['task_type'] = 'Date Wise';
      this.taskByDate['created'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      console.log(this.taskByDate);

      var connectedRef = firebase.database().ref(".info/connected");
      connectedRef.on("value", function (snap) {
        if (snap.val() === true) {
          that.createTaskInOnline();

        } else {
          that.createTaskInOffLine();
        }
      });
    }
    // if (this.editTask) {
    //   this.taskByDate['status'] = this.editTask.status;
    //   this.taskByDate['task_type'] = this.editTask.task_type;
    //   this.taskByDate['created'] = this.editTask.created;
    //   this.taskByDate['updated'] = {
    //     "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
    //     'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
    //   };
    //   // this.taskByDate['updated_date'] = this.date_in_ms;
    //   var connectedRefEdit = firebase.database().ref(".info/connected");
    //   connectedRefEdit.on("value", function (snap) {
    //     if (snap.val() === true) {
    //       that.updateTaskInOnline();

    //     } else {
    //       that.createTaskInOffLine();
    //     }
    //   });

    // }
  }
  createTaskInOffLine() {
    this.count1 = this.count1 + 1;
    if (this.count <= 1) {
      this.location.back();
      // this.navCtrl.navigateBack('/task-list');
    }
  }
  createTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let totalTask_tableData: any;
      let task_tableLength: any;

      let newData = this.taskManager_Task_table.push();
      newData.set(this.taskByDate);
      let hours = new Date().getHours();
      if(this.taskByDate['date'] == this.date_in_ms && hours>=6){
        this.other.sendNotification(this.taskByDate);
      }
      // let usersRef = firebase.database().ref('TaskManager/Task_table');
      this.taskManager_Task_table.limitToLast(1).once('value',resp =>{
        totalTask_tableData = snapshotToArray(resp)[0];
        console.log(totalTask_tableData);
        if (totalTask_tableData) {
          that.Task_logTable(totalTask_tableData);
        }
      });
      // this.taskManager_Task_table.once("value", function (Data) {
      //   totalTask_tableData = snapshotToArray(Data);
      //   console.log(totalTask_tableData);

      //   task_tableLength = totalTask_tableData.length - 1;
      //   console.log(task_tableLength);
        
      // });
    }
  }
  updateTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let usersRef1 = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table/' + this.editTask.key);
      usersRef1.set(this.taskByDate);
      if (usersRef1) {
        that.Task_logTable(this.editTask);
      }
    }

  }
  Task_logTable(totalTask_tableData) {
    /* pushing data to Task_logTable */
    if (!this.editTask) {
      console.log(totalTask_tableData[totalTask_tableData.length - 1]);
      this.task_logData['created'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      // this.task_logData['task_id'] = totalTask_tableData[totalTask_tableData.length - 1].key;
      this.task_logData['task_id'] = totalTask_tableData.key;
      this.task_logData['status'] = { 'name': 'Acknowledge' };
      if(this.taskByDate.assignee){
      this.task_logData['assignee'] = this.taskByDate.assignee;
      }else{
        this.task_logData['assignee']=null
      }

      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.presentToast();
      this.location.back();
      //setTimeout(() => { this.location.back(); this.location.back(); }, 1);
    }
    if (this.editTask) {
      this.task_logData['updated'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      this.task_logData['task_id'] = this.editTask.key;
      this.task_logData['status'] = this.editTask.status;
      this.task_logData['assignee'] = this.editTask.assignee;
      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.presentToast();
      this.location.back();
      //setTimeout(() => { this.location.back(); this.location.back(); }, 1);
    }
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};