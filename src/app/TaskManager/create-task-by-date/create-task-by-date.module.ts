import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CreateTaskByDatePage } from './create-task-by-date.page';
import {ComponentsModule} from '../../components/components.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import{CalendercomponentComponent} from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import {HarvestcomponentsModule} from '../../components/harvestcomponents/harvestcomponents.module'

const routes: Routes = [
  {
    path: '',
    component: CreateTaskByDatePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TaskManagerComponentsModule,
    ObservationComponentsModule,
    HarvestcomponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreateTaskByDatePage],
  entryComponents: [CalendercomponentComponent]
})
export class CreateTaskByDatePageModule {}
