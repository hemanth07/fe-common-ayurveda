import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, AlertController, ToastController,LoadingController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import * as firebase from 'firebase';
import { SelectPlotsBlockComponent } from '../../components/task-manger-components/select-plots-block/select-plots-block.component';
import { SimpleGlobal } from 'ng2-simple-global';
import { SelectDateComponent } from '../../components/task-manger-components/select-date/select-date.component';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { OtherService } from '../../languages/others.service';
@Component({
  selector: 'app-create-task-by-block',
  templateUrl: './create-task-by-block.page.html',
  styleUrls: ['./create-task-by-block.page.scss'],
})
export class CreateTaskByBLockPage implements OnInit {
  taskManager_Task_table = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/bulk_Tasks');
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable');
  headerData: any = [];
  categoriesData: any = [];
  selectedBlock: any = [];
  task_by_category: any = [];
  taskByBlock: any = [];
  blocksWithPlots: any = [];
  plots: any = [];
  categoryByPlots: any = [];
  usersData: any = [];
  today_date: any;
  date_in_ms: any;
  task_logData: any = [];
  currentDate: any;
  endDate: any;
  editTask: any;
  assignee: any;
  start_date: any;footerData:any;
  end_date: any;
  isDateSelected: boolean = false;
  activeToggle: boolean = false;
  isdateOpen: any = [];
  count: any = 0;
  count1: any = 0;
  audio: any;
  presentDate: any;
  dateAfterOneYear: any;
  fromBlock: any = 'createBlock';
  selectedStartDate: any;
  selectedEndDate: any;
  video: any;
  camera: any;
  attachments: any = [];
  labels: any;
  selected_blocks: any;
  laboursData: any = [];
  isCreatedTask:any;
  isdraft:any;
  selectedBlockFlag:any = [];
  viewPlot:boolean = false;
  AllLaboursData:any = [];
  wait:string;
  success_msg:string;
  description_label:string;
  constructor(private navCtrl: NavController, private popoverController: PopoverController, private location: Location,
    private sg: SimpleGlobal, private route: ActivatedRoute, private alertController: AlertController,
    public toastController: ToastController,private loadingController:LoadingController,
    public other:OtherService) { }

  ngOnInit() {

    //this.isdateOpen=0;
    this.editTask = JSON.parse(this.route.snapshot.paramMap.get('data'));

    console.log(this.editTask);
    let data;
    if (this.sg['userdata'].primary_language === 'en') {
      data = 'Create Task - By Block';
      this.wait = "Please Wait ..."
      this.success_msg = "Success"
      this.description_label = "Task Description"
      this.labels = {
        StartDate: 'Start Date',
        EndDate: 'End Date',
        create: 'Create Task',
        any_additional:'Any additional Notes'
      };
    } else if (this.sg['userdata'].primary_language === 'te') {
    this.wait = "దయచేసి వేచి ఉండండి ..."
      this.description_label = "టాస్క్ వివరణ"
      this.success_msg = "విజయం";
    data = 'టాస్క్ సృష్టించు - బ్లాక్ ద్వారా';
      this.labels = {
        StartDate: "ప్రారంబపు తేది",
        EndDate: "ఆఖరి తేది",
        create: 'టాస్క్ సృష్టించండి',
        any_additional:'ఏదైనా అదనపు గమనికలు'
      }
    } else if (this.sg['userdata'].primary_language === 'ta') {
      data = 'பணி உருவாக்கு - பிளாக் மூலம்';  
      this.success_msg = "வெற்றி"
      this.description_label = "பணி விளக்கம்"
      this.wait = "தயவுசெய்து காத்திருங்கள் ..."
      this.labels = {
        StartDate: "தொடக்கத் தேதி",
        EndDate: "கடைசி தேதி",
        create: 'பணி உருவாக்கவும்',
        any_additional:'எந்த கூடுதல் குறிப்புகள்'
      };
    } else if (this.sg['userdata'].primary_language === 'kn') {
      data = 'ಟಾಸ್ಕ್ ರಚಿಸಿ - ಬ್ಲಾಕ್ ಮೂಲಕ';
      this.success_msg = "ಯಶಸ್ಸು"
      this.description_label = "ಕಾರ್ಯ ವಿವರಣೆ"
      this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
      this.labels = {
        StartDate: "ಪ್ರಾರಂಭ ದಿನಾಂಕ",
        EndDate: "ಅಂತಿಮ ದಿನಾಂಕ",
        create: "ಟಾಸ್ಕ್ ರಚಿಸಿ",
        any_additional:'ಯಾವುದೇ ಹೆಚ್ಚುವರಿ ಟಿಪ್ಪಣಿಗಳು'
      };
    } else if (this.sg['userdata'].primary_language === 'hi') {
      data = 'कार्य बनाएँ - ब्लॉक द्वारा';
      this.description_label = "कार्य विवरण"
      this.success_msg = "सफलता"
      this.wait = "कृपया प्रतीक्षा करें ..."
      this.labels = {
        StartDate: "आरंभ करने की तिथि",
        EndDate: "अंतिम तिथि",
        create: "कार्य बनाएँ",
        any_additional:'कोई भी अतिरिक्त नोट्स'
      };
    }
    this.headerData = { 'title': data,button1: 'home',button1_action: '/farm-dashboard', 'back': '/tm-mainmenu', 'color': 'green' };
    // this.footerData={'submit':'CREATE TASK','draft':'SAVE DRAFT','submitColor':'green',
    // 'footerColor':'rgba(255,255,255,0.9)','submit_right':'22px','draftColor':'brown' ,'submitDissable':true}
    // this.footerData= {
    //   'footerColor':'rgba(255,255,255,0.9)',
    //   'leftButtonName':'CREATE TASK',
    //   'rightButtonName':'SAVE DRAFT',
    //   'leftButtonColor':'green',
    //   'rightButtonColor':'grey'
    //   }
       this.footerData= {
      'footerColor':'rgba(255,255,255,0.9)',
      'leftButtonName':'CREATE TASK',
      'leftButtonColor':'green',
      }
  }


 async ionViewDidEnter(){
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();

    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').on('value', resp => {
      let blocks = snapshotToArray(resp);
      this.categoriesData['blocks'] = blocks;
      console.log(blocks);
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users/').on('value', resp => {
      let data = snapshotToArray(resp);
      this.AllLaboursData = data.filter(data => data.role == 'employee');
      this.usersData = data.filter(data => data.role == 'employee')

      this.usersData['page'] = 'createTaskByBlock';
    });
    console.log(this.usersData);
    this.today_date = new Date();
    let datestring = new Date().toISOString().split("T")[0];
    this.date_in_ms = new Date(datestring).getTime();
    var end_date = new Date(new Date().setFullYear(new Date().getFullYear() + 1))
    var current_month = this.today_date.getMonth() + 1;
    var end_month = end_date.getMonth() + 1;
    this.presentDate =  this.today_date.getDate()  + '-' + current_month + "-" + this.today_date.getFullYear();
    var st = this.today_date.getFullYear() + '-' + current_month + "-" + this.today_date.getDate();
    var endDate = end_date.getFullYear() + "-" + end_month + '-' + end_date.getDate();
    this.dateAfterOneYear = end_date.getDate()  + "-" + end_month + '-' + end_date.getFullYear();
    this.currentDate = this.today_date;
    this.endDate = new Date(end_date);
    if (this.editTask && this.categoriesData) {
      this.getPlots(this.editTask.blocks);
      if (this.editTask.start_date || this.editTask.end_date) {
        this.displayEditDate();
      }
    }
    else {
      this.taskByBlock['media'] = [''];
    }
    this.taskByBlock['start_date'] = this.date_in_ms;
    this.taskByBlock['end_date'] = new Date(endDate).getTime();
    setTimeout(()=>{
      loading.dismiss();
    },1000)
  }



  getLaboursWithBlockResponseble() {
    this.laboursData = this.AllLaboursData;
    let temp: any = [];
    for (var i = 0; i < this.laboursData.length; i++) {
      if (this.laboursData[i].block_responsible) {
        for (var j = 0; j < this.laboursData[i].block_responsible.length; j++) {
          if (this.laboursData[i].block_responsible[j] == this.selected_blocks.blockName) {
            temp.push(this.laboursData[i]);
          }
        }
      }
    }
    this.usersData = temp;
  }
  
  async presentToast() {
    const toast = await this.toastController.create({
      message: this.success_msg,
      duration: 2000
    });
    toast.present();
  }

  displayEditDate() {
    let currentDate = new Date(this.editTask.start_date);
    let endDate = new Date(this.editTask.end_date);
    var current_month = currentDate.getMonth() + 1;
    this.presentDate = currentDate.getFullYear() + '-' + current_month + "-" + currentDate.getDate();
    let end_month = endDate.getMonth() + 1;
    if (this.editTask && this.editTask.media) {
      this.taskByBlock['media'] = this.editTask.media;
    }
  }
  getBlock(val) {
    console.log(val);
    this.selectedBlock = val;
    this.task_by_category['blocks'] = { 'name': this.selectedBlock.name };
    //console.log( this.taskByBlock);
    this.presentPopover();

  }
  async presentPopover() {

    const popover = await this.popoverController.create({
      component: SelectPlotsBlockComponent,
      cssClass: "popover_class",
      componentProps: {
        blockName: this.selectedBlock,
        totalBlocks: this.categoriesData.blocks
      },

    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getPlots(detail.data);
    });
    await popover.present();
  }

  getPlots(val) {
    console.log(val);
    this.selected_blocks = val;
    console.log(this.categoriesData.blocks);
    let data: any = [];
    let data1: any = [];
    let temp: any = 0;
    if (val) {
      if (!this.editTask) {
          for (var i = 0; i < val.length; i++) {
          for (var j = 0; j < this.categoriesData.blocks.length; j++) {
            if (val.blockName == this.categoriesData.blocks[j].name) {
              data.push(val[i]);
              data1.push({ 'name': val[i].name })
              this.categoriesData.blocks[j]['plots'] = data;
            } else {
              this.categoriesData.blocks[j]['plots'] = [];
            }
          }
        }
        if(val.length == 0){
          for (var j = 0; j < this.categoriesData.blocks.length; j++) {
            if (val.blockName == this.categoriesData.blocks[j].name) {
              this.categoriesData.blocks[j]['plots'] = [];
            }
          }
        }
        this.blocksWithPlots = [{ 'name': val.blockName, 'plots': data1 }];
        this.taskByBlock['blocks'] = this.blocksWithPlots;
        // for (var i = 0; i < val.length; i++) {
        //   for (var j = 0; j < this.categoriesData.blocks.length; j++) {
        //     if (val.blockName == this.categoriesData.blocks[j].name) {
        //       data.push(val[i]);
        //       data1.push({ 'name': val[i].name })
        //       this.categoriesData.blocks[j]['plots'] = data;
        //     }
        //   }
        // }
        // this.blocksWithPlots.push({ 'name': val.blockName, 'plots': data1 })
        // this.plots.push({ 'name': val.blockName, 'plots': data });
        // this.taskByBlock['blocks'] = this.blocksWithPlots;
      }
      if (this.editTask && val.blockName) {
        for (var i = 0; i < val.length; i++) {
          for (var j = 0; j < this.categoriesData.blocks.length; j++) {
            if (val.blockName == this.categoriesData.blocks[j].name) {
              data.push(val[i]);
              data1.push({ 'name': val[i].name })
              this.categoriesData.blocks[j]['plots'] = data;
            } else {
              this.categoriesData.blocks[j]['plots'] = [];
            }
          }
        }
        this.blocksWithPlots = [{ 'name': val.blockName, 'plots': data1 }];
        this.plots.push({ 'name': val.blockName, 'plots': data });
        this.taskByBlock['blocks'] = this.blocksWithPlots;
      }
      if (this.editTask && !val.blockName) {
        let blockName: any = [];
        for (var i = 0; i < val.length; i++) {
          data1 = [];
          data = [];
          for (var j = 0; j < val[i].plots.length; j++) {
            for (var k = 0; k < this.categoriesData.blocks.length; k++) {
              if (val[i].name == this.categoriesData.blocks[k].name) {
                data.push({ "name": val[i].plots[j].name, "isSelectedPlot": true });
                data1.push(val[i].plots[j]);
                this.categoriesData.blocks[k]['plots'] = data;
              } else {
                this.categoriesData.blocks[j]['plots'] = [];
              }
            }
          }
          blockName = [{ "name": val[i].name, 'plots': data1 }];
        }
        console.log(blockName);
        this.blocksWithPlots = blockName;
        this.plots.push({ 'name': val.blockName, 'plots': data });
        this.taskByBlock['blocks'] = this.blocksWithPlots;
      }
      for (var i = 0; i < this.categoriesData.blocks.length; i++) {
        if (this.categoriesData.blocks[i].plots) {
          if (this.categoriesData.blocks[i].plots.length > temp) {
            temp = this.categoriesData.blocks[i].plots.length;
          }
        }
      }
      for (var i = 0; i < this.categoriesData.blocks.length; i++) {
        var tempPlots: any = [];
        for (var j = 0; j < temp;) {
          if (this.categoriesData.blocks[i].plots) {
            if (this.categoriesData.blocks[i].plots[j] && this.categoriesData.blocks[i].plots[j].isSelectedPlot) {
              j++;
            }
            else {
              this.categoriesData.blocks[i].plots[j] = { "name": '', 'isSelectedPlot': false }
              j++;
            }
          }
          else {
            tempPlots.push({ "name": '', 'isSelectedPlot': false });
            j++;
            this.categoriesData.blocks[i]['plots'] = tempPlots;
          }
        }
      }
      console.log(this.taskByBlock);
      console.log(this.categoriesData.blocks);
      this.categoryByPlots = this.categoriesData.blocks
      console.log(this.categoryByPlots);
      if(this.categoryByPlots){
      for(let i=0;i<this.categoryByPlots.length;i++){
        this.selectedBlockFlag[i] = false;
        if(this.categoryByPlots[i].plots){
          for(let plot of this.categoryByPlots[i].plots){
            if(plot.isSelectedPlot == true){
             this.selectedBlockFlag[i] = true;
            } 
          }
        } else {
          this.selectedBlockFlag[i] = false;
        }
      }
      this.viewPlot = this.selectedBlockFlag.includes(true);
     }
    }
    this.getLaboursWithBlockResponseble();
  }
  async openStartEndDatePopover(data) {
    //this.isdateOpen= this.isdateOpen+1;
    let val = { 'name': data };
    if (!this.editTask) {
      const popover = await this.popoverController.create({
        component: SelectDateComponent,
        cssClass: "datePopover_class",
        componentProps: {
          date: val,
          startDate: this.selectedStartDate,
          endDate: this.selectedEndDate
        },
      });
      popover.onDidDismiss().then((detail: OverlayEventDetail) => {
        this.getDates(detail.data);
      });
      await popover.present();
    }
    if (this.editTask) {
      this.isdateOpen = this.isdateOpen + 1;
      if (this.isdateOpen >= 1) {

        const popover = await this.popoverController.create({
          component: SelectDateComponent,
          cssClass: "datePopover_class",
          componentProps: {
            date: val,
            startDate: this.selectedStartDate,
            endDate: this.selectedEndDate
          },
        });
        popover.onDidDismiss().then((detail: OverlayEventDetail) => {
          this.getDates(detail.data);
        });
        await popover.present();
      }
    }
  }
  // getDates(val) {
  //   console.log(val);
  //   var startDate: any;
  //   var endDate: any;
  //   var frequencyWithDays: any = [];
  //   if (val) {
  //     if (val.name == 'Start Date') {
  //       var d = new Date(val.startDate);
  //       if (this.taskByBlock.start_date) {
  //         this.taskByBlock.start_date = d.getTime();
  //         this.selectedStartDate = val.startDate;
  //         this.presentDate = val.startDate;
  //       }
  //     }
  //     if (val.name == 'End Date') {
  //       var d1 = new Date(val.endDate);
  //       if (this.taskByBlock.end_date) {
  //         this.taskByBlock.end_date = d1.getTime();
  //         this.selectedEndDate = val.endDate;
  //         this.dateAfterOneYear = val.endDate;
  //       }
  //     }
  //     console.log(this.taskByBlock);
  //   }
  // }
  getDates(val) {
    console.log(val);
    var startDate: any;
    var endDate: any;
    let that = this;

    var frequencyWithDays: any = [];
    if (val) {
      if (val.name == 'Start Date') {
        var d = new Date(val.dateFormate);
        if (this.taskByBlock.start_date) {
          this.taskByBlock.start_date = d.getTime();
          that.selectedStartDate = val.dateFormate;
          this.presentDate = val.startDate;

        }
      }
      if (val.name == 'End Date') {
        var d1 = new Date(val.dateFormate);
        if (this.taskByBlock.end_date) {
          this.taskByBlock.end_date = d1.getTime();
          that.selectedEndDate = val.dateFormate;
          this.dateAfterOneYear = val.endDate;

        }
      }
    }
  }
  getDate(val) {
    console.log(val);
    var startDate: any;
    var endDate: any;
    var frequencyWithDays: any = [];
    if (val) {
      this.isDateSelected = true;
      if (val.name == 'startEndDate') {
        var d = new Date(val.startDate);
        this.taskByBlock['start_date'] = d.getTime();
        startDate = val.date;
        var d1 = new Date(val.endDate);
        this.taskByBlock['end_date'] = d1.getTime();
        endDate = val.endDate;
      }
      console.log(this.taskByBlock);
    }
    else {
      this.isDateSelected = false;
    }


  }
  getFromGallery(val) {
    console.log(val);
  }

  getPriority(val) {
    console.log(val);

    this.taskByBlock['indicate_priority'] = val;
  }
  getLabourData(val) {
    console.log(val);
    let temp: any = [];
     this.assignee = {
      'name': val.name, 'phone': val.phone, 'profile_url': val.profile_url, 'role': val.role,'farmeasy_id':val.farmeasy_id,
      'primary_language': val.primary_language, 'device_id': val.device_id?val.device_id:null
    }
    this.taskByBlock['assignee'] = this.assignee;
  }
  getAdditionalNotes(val) {
    console.log(val);
    this.taskByBlock['description'] = val;
  }
  getRecordedAudio(val) {
    console.log(val);
    this.audio = val.url;
    this.taskByBlock.media['audio'] = val;
    //alert( this.taskByBlock.media.audio.url);
  }
  getVideo(val) {
    this.video = val;
    this.taskByBlock.media['video'] = val;
    //alert( this.taskByBlock.media.video.url);
  }
  getCameraImage(val) {
    this.camera = val;
    this.taskByBlock.media['camera'] = val;
    //alert( this.taskByBlock.media.camera.url);
  }
  getAttachments(val) {
    this.attachments.push(val);
    this.taskByBlock.media['attachments'] = [];
    this.taskByBlock.media['attachments'] = this.attachments;

  }
 async createTask(val) {
  let labour;
  let block;
  let priority;
  let date;
  let error_label;
  let select;
  let okay;
  if(this.sg['userdata'].primary_language === 'en'){
    labour = "Labour";
    block = "Block";
    priority = "Priority";
    date = "Proper Dates"
    error_label = "Error";
    select = "Select "
    okay = "Okay"
   } else if(this.sg['userdata'].primary_language === 'te'){
    labour = "లేబర్";
    block = "బ్లాక్";
    priority = "ప్రాధాన్యత";
    date = "సరైన తేదీలు"
    error_label = "లోపం";
    select = "ఎంచుకోండి "
    okay = "సరే"
   } else if(this.sg['userdata'].primary_language === 'ta'){
    labour = "தொழிலாளர்";
    block = "பிளாக்";
    priority = "முன்னுரிமை";
    date = "சரியான தேதிகள்"
    error_label = "பிழை";
    select = "தேர்வு "
    okay = "சரி"
   } else if(this.sg['userdata'].primary_language === 'kn'){
    labour = "ಕಾರ್ಮಿಕ";
    block = "ಬ್ಲಾಕ್";
    priority = "ಆದ್ಯತೆ";
    date = "ಸರಿಯಾದ ದಿನಾಂಕಗಳು"
    error_label = "ದೋಷ";
    select = "ಆಯ್ಕೆ ಮಾಡಿ "
    okay = "ಸರಿ"
   } else if(this.sg['userdata'].primary_language === 'hi'){
    labour = "श्रम";
    block = "खंड";
    priority = "प्राथमिकता";
    date = "उचित तिथियां"
    error_label = "त्रुटि";
    select = "चुनते हैं "
    okay = "ठीक है"
   }
    let error = [];
    if(this.taskByBlock){
      if(!this.taskByBlock['indicate_priority']){
        error.push(priority);
      }
      if(!this.taskByBlock['assignee']){
        error.push(labour);
      }
      // if(!this.taskByBlock['description']){
      //   error.push("Description");
      // }
      if(!this.taskByBlock['blocks']){
        error.push(block)
      }
      if(this.taskByBlock.start_date>this.taskByBlock.end_date){
        error.push(date);
      }
    }
    if(error.length==0){
      let message;
      let header;
      let ok;
      let cancel;
      if(this.sg['userdata'].primary_language === 'en'){
        header = "Confirm!"
        message  = "Are you sure want to create task without description"
        ok = "Okay"
        cancel = "Cancel"
       } else if(this.sg['userdata'].primary_language === 'te'){
        header = "కన్ఫర్మ్!"
        message  = "మీరు ఖచ్చితంగా వివరణ లేకుండా పనిని సృష్టించాలనుకుంటున్నారా"
        cancel = "కాదు"
        ok = "సరే"
       } else if(this.sg['userdata'].primary_language === 'ta'){
        header = "உறுதிசெய்!"
        message  = "விளக்கம் இல்லாமல் பணியை உருவாக்க விரும்புகிறீர்களா?"
        cancel = "இல்லை"
        ok = "சரி"
       } else if(this.sg['userdata'].primary_language === 'kn'){
        header = "ಖಚಿತಪಡಿಸಿ!"
        message  = "ವಿವರಣೆಯಿಲ್ಲದೆ ಕಾರ್ಯವನ್ನು ರಚಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ?"
        cancel = "ಇಲ್ಲ"
        ok = "ಸರಿ"
       } else if(this.sg['userdata'].primary_language === 'hi'){
        header = "पुष्टि करें!"
        message  = "क्या आप वाकई वर्णन के बिना कार्य बनाना चाहते हैं"
        cancel = "नहीं"
        ok = "ठीक है"
       }
       if(!this.taskByBlock['description']){
        const alert = await this.alertController.create({
          header: header,
          message: message,
          buttons: [
            {
              text: cancel,
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('Confirm Cancel: blah');
              }
            }, {
              text: ok,
              handler: () => {
                this.createTaskAfterErrorChecks(val);
              }
            }
          ]
        });
        await alert.present();
      } else {
        this.createTaskAfterErrorChecks(val);
      }
    }
    else{
      const alert = await this.alertController.create({
      header: error_label,
      message: select+error.join(","),
      buttons: [okay]
    });
    await alert.present();
    }
  }

createTaskAfterErrorChecks(val){
      
  if (!this.taskByBlock.start_date) {
    this.taskByBlock['start_date'] = this.currentDate.getTime();
  }
  if (!this.taskByBlock.end_date) {
    this.taskByBlock['end_date'] = this.endDate.getTime()
  }
  this.taskByBlock['category'] = { "name": '' };
  let that = this;

  if (!this.editTask) {
    this.taskByBlock['task_type'] = 'Block';
    if(val=='leftButtonAction'){
    this.taskByBlock['status'] = {'name':'Acknowledge'};
    }else if(val=='rightButtonAction'){
      this.taskByBlock['status'] =null
    }
    this.taskByBlock['created'] = {
      "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
      'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
    };

    console.log(this.taskByBlock);
    var connectedRef = firebase.database().ref(".info/connected");
    connectedRef.on("value", function (snap) {
      if (snap.val() === true) {
        that.createTaskInOnline();

      } else {
        that.createTaskInOffLine();
      }
    });

  }
  // if (this.editTask) {
  //   this.taskByBlock['task_type'] = this.editTask.task_type;
  //   this.taskByBlock['status'] = this.editTask.status;
  //   this.taskByBlock['updated'] = {
  //     "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
  //     'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
  //   };
  //   this.taskByBlock['created'] = this.editTask.created;
  //   var connectedRefEdit = firebase.database().ref(".info/connected");
  //   connectedRefEdit.on("value", function (snap) {
  //     if (snap.val() === true) {
  //       that.updateTaskInOnline();

  //     } else {
  //       that.createTaskInOffLine();
  //     }
  //   });
  //   }
}


  async closeImage(val, idx) {
    let message;
    let header;
    let ok;
    let cancel;
    if(this.sg['userdata'].primary_language === 'en'){
      header = "Confirm!"
      message  = "Are you sure want to delete this "
      ok = "Okay"
      cancel = "Cancel"
     } else if(this.sg['userdata'].primary_language === 'te'){
      header = "కన్ఫర్మ్!"
      message  = "మీరు దీన్ని ఖచ్చితంగా తొలగించాలనుకుంటున్నారా "
      cancel = "కాదు"
      ok = "సరే"
     } else if(this.sg['userdata'].primary_language === 'ta'){
      header = "உறுதிசெய்!"
      message  = "இதை நிச்சயமாக நீக்க விரும்புகிறீர்களா "
      cancel = "இல்லை"
      ok = "சரி"
     } else if(this.sg['userdata'].primary_language === 'kn'){
      header = "ಖಚಿತಪಡಿಸಿ!"
      message  = "ಇದನ್ನು ಅಳಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ "
      cancel = "ಇಲ್ಲ"
      ok = "ಸರಿ"
     } else if(this.sg['userdata'].primary_language === 'hi'){
      header = "पुष्टि करें!"
      message  = "क्या आप वाकई इसे हटाना चाहते हैं "
      cancel = "नहीं"
      ok = "ठीक है"
     }
    const alert = await this.alertController.create({
      header: header,
      message: message + val + '?',
      buttons: [
        {
          text: cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: ok,
          handler: () => {
            this.removeAttachment(val, idx);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  removeAttachment(val, idx) {
    let that = this;
    if (val == 'audio') {
      for (var i = 0; i < this.taskByBlock.media.audio.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByBlock.media.audio[i].path).delete()
          that.taskByBlock.media.audio.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/bulk_Tasks/" + that.editTask.key + '/media');
            playersRef.update(that.taskByBlock.media);
          }


        }
      }

    }
    if (val == 'video') {
      for (var i = 0; i < this.taskByBlock.media.video.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByBlock.media.video[i].path).delete()
          that.taskByBlock.media.video.splice(idx, 1);
          if (that.editTask) {
            var playersRef1 = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/bulk_Tasks/" + that.editTask.key + '/media');
            playersRef1.update(that.taskByBlock.media);
          }
        }
      }

    }
    if (val == 'camera') {
      for (var i = 0; i < this.taskByBlock.media.camera.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByBlock.media.camera[i].path).delete()
          that.taskByBlock.media.camera.splice(idx, 1);
          if (that.editTask) {
            var playersRef2 = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/bulk_Tasks/" + that.editTask.key + '/media');
            playersRef2.update(that.taskByBlock.media);
          }
        }

      }
    }
    if (val == 'attachments') {
      for (var i = 0; i < this.taskByBlock.media.attachments.length; i++) {
        if (i == idx) {
          this.taskByBlock.media.attachments[i] = null;

          firebase.storage().ref(that.taskByBlock.media.attachments[i].path.name).delete()
          that.taskByBlock.media.attachments.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/bulk_Tasks/" + that.editTask.key + '/media');
            playersRef.update(that.taskByBlock.media);
          }
        }
      }
      console.log(this.taskByBlock.media.attachments);
    }
  }

  createTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      //alert(JSON.stringify(that.taskByBlock))
      let newData = this.taskManager_Task_table.push();
      newData.set(this.taskByBlock);
      let hours = new Date().getHours();
      if(this.taskByBlock['start_date'] == this.date_in_ms && hours>=6){
        this.other.createNewTaskFromBulkTasks(this.sg['userdata'].vendor_id,this.taskByBlock,this.date_in_ms);
      }
      this.presentToast();
      this.location.back();
      /* getting taskTable data */
      let totalTask_tableData: any;
      let task_tableLength: any;
      let usersRef = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/bulk_Tasks');
      // usersRef.once("value", function (Data) {
      //   totalTask_tableData = snapshotToArray(Data);
      //   console.log(totalTask_tableData);
      //   //console.log(data[data.length-1].key);
      //   task_tableLength = totalTask_tableData.length - 1;
      //   console.log(task_tableLength);
      //   if (totalTask_tableData) {
      //     that.Task_logTable(totalTask_tableData);
      //   }
      // });
    }

  }
  createTaskInOffLine() {
    this.count1 = this.count1 + 1;
    if (this.count <= 1) {
      this.location.back();
      // this.navCtrl.navigateBack('/task-list');
    }
  }
  updateTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let usersRef = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/bulk_Tasks/' + this.editTask.key);
      usersRef.set(this.taskByBlock);
      this.presentToast();
      this.location.back();
      if (usersRef) {
        // that.Task_logTable(this.editTask);
      }
    }
  }
      dateConverter(date){
        if(date){
              let date_split =  date.split("-");
        if(date_split[1]< 10 && date_split[1].length<2){
          date_split[1] = '0'+date_split[1];
        }
        let tempdate = date_split.reverse().join("-");
      let d = new Date(tempdate)
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      return day+"-"+months[month]+"-"+year;
        }
    }
  Task_logTable(totalTask_tableData) {
    //   /* pushing data to Task_logTable */ 
    if (!this.editTask) {
      console.log(totalTask_tableData[totalTask_tableData.length - 1]);
      this.task_logData['task_id'] = totalTask_tableData[totalTask_tableData.length - 1].key;
      this.task_logData['status'] = { 'name': 'Acknowledge' };
      this.task_logData['created'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      this.task_logData['assignee'] = this.assignee;

      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);

      this.categoriesData = [];
      this.presentToast();
      this.location.back();
      //setTimeout(() => {  this.location.back(); this.location.back();},1);
    }
    if (this.editTask) {
      this.task_logData['task_id'] = totalTask_tableData.key;
      this.task_logData['status'] = totalTask_tableData.status;
      this.task_logData['assignee'] = this.assignee;
      this.task_logData['updated'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.categoriesData = [];
      this.presentToast();
      this.location.back();
    }
  }
  backToTmMainMenu() {
    this.location.back();
    // this.navCtrl.navigateForward('/task-list');
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
