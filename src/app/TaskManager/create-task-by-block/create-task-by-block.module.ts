import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import {ComponentsModule} from '../../components/components.module';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { CreateTaskByBLockPage } from './create-task-by-block.page';
import { SelectPlotsBlockComponent } from '../../components/task-manger-components/select-plots-block/select-plots-block.component';
import {SelectDateComponent} from '../../components/task-manger-components/select-date/select-date.component'

import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: CreateTaskByBLockPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ObservationComponentsModule,
    TaskManagerComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreateTaskByBLockPage],
  entryComponents: [SelectPlotsBlockComponent,SelectDateComponent]
})
export class CreateTaskByBLockPageModule {}
