import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { OverlayEventDetail } from '@ionic/core';
import { NavController, PopoverController, AlertController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { SelectDateComponent } from '../../components/task-manger-components/select-date/select-date.component';
import { SimpleGlobal } from 'ng2-simple-global';
import { Location } from '@angular/common';
import { OtherService } from '../../languages/others.service';
@Component({
  selector: 'app-create-task-adhoc',
  templateUrl: './create-task-adhoc.page.html',
  styleUrls: ['./create-task-adhoc.page.scss'],
})
export class CreateTaskAdhocPage implements OnInit {
  taskManager_Task_table = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table');
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable');
  headerData: any;
  frequency: any;
  laboursData:any = [];
  blocks:any = [];
  selectedCategory: any;
  editTask: any;
  taskByAdhoc: any = [];
  isdateOpen: boolean = false;
  selectedDates: any = [];
  block: any = [];
  labour: any = '';
  priority: any = '';
  task: any = '';
  description: any = '';
  selectedFrequency: any = '';
  audio: any;
  today_date: any;
  date_in_ms: any;
  isLabour: boolean = false;
  count1: any = 0;
  count: any = 0;
  task_logData: any = [];
  dateAfterOnerYear: any;
  selectedEndDate: any;
  selectedStartDate: any;
  selectedDates1: any;
  start_date: any;footerData:any;
  end_date: any;
  camera: any;
  attachments: any = [];
  video: any;
  create_label: string;
  labels: any;
  isCreatedTask:any;
  isdraft:any;
  success_msg:string;
  description_label:string;
  constructor(private route: ActivatedRoute, private navCtrl: NavController, private sg: SimpleGlobal, private location: Location,
    private popoverController: PopoverController, private alertController: AlertController,
    public toastController: ToastController,public other:OtherService) { }

  ngOnInit() {

    this.editTask = '';
    this.editTask = JSON.parse(this.route.snapshot.paramMap.get('data'));
    if (!this.editTask) {
      this.selectedCategory = JSON.parse(this.route.snapshot.paramMap.get('category'));
      this.taskByAdhoc['media'] = {};
    } else {
      this.selectedCategory = this.editTask.category;
      this.taskByAdhoc = this.editTask;

    }
    console.log(this.selectedCategory);
    let data;
    if (this.sg['userdata'].primary_language === 'en') {
      data = 'Create Task - By Adhoc';
      this.create_label = 'Create Task';
      this.success_msg = "Success"
      this.description_label = "Task Description"
      this.labels = { labourerrmsg: 'Please Select Labour', create: 'Create Task' }
    } else if (this.sg['userdata'].primary_language === 'te') {
      data = 'టాస్క్ సృష్టించు - Adhoc ద్వారా';
      this.create_label = 'టాస్క్ సృష్టించండి';
      this.description_label = "టాస్క్ వివరణ"
      this.success_msg = "విజయం";
      this.labels = { labourerrmsg: 'దయచేసి లేబర్ ఎంచుకోండి', create: 'పనిని సృష్టించండి' }
    } else if (this.sg['userdata'].primary_language === 'ta') {
      data = 'பணி உருவாக்க - Adhoc மூலம்';
      this.success_msg = "வெற்றி"
      this.description_label = "பணி விளக்கம்"
      this.create_label = 'பணி உருவாக்கவும்';
      this.labels = { labourerrmsg: 'தொழில் மற்றும் தேதி தேர்ந்தெடுக்கவும்', create: 'பணி உருவாக்க' }
    } else if (this.sg['userdata'].primary_language === 'kn') {
      data = 'ಟಾಸ್ಕ್ ರಚಿಸಿ - Adhoc ಮೂಲಕ';
      this.create_label = 'ಟಾಸ್ಕ್ ರಚಿಸಿ';
      this.success_msg = "ಯಶಸ್ಸು"
      this.description_label = "ಕಾರ್ಯ ವಿವರಣೆ"
      this.labels = { labourerrmsg: 'ದಯವಿಟ್ಟು ಕಾರ್ಮಿಕ ಮತ್ತು ದಿನಾಂಕವನ್ನು ಆಯ್ಕೆಮಾಡಿ', create: 'ಕೆಲಸವನ್ನು ರಚಿಸಿ' }
    } else if (this.sg['userdata'].primary_language === 'hi') {
      data = 'कार्य बनाएँ - Adhoc द्वारा';
      this.description_label = "कार्य विवरण"
      this.success_msg = "सफलता"
      this.create_label = 'कार्य बनाएँ';
      this.labels = { labourerrmsg: 'कृपया श्रम और तिथि का चयन करें', create: 'कार्य बनाएँ' }
    }

    this.headerData = { 'title': data,
                        button1: 'home',
                        button1_action: '/farm-dashboard',
                        'back': '/tm-mainmenu', 
                        'color': 'green' };
    // this.footerData={'submit':'CREATE TASK','draft':'SAVE DRAFT','submitColor':'green',
    // 'footerColor':'rgba(255,255,255,0.9)','submit_right':'22px','draftColor':'brown' ,'submitDissable':true}
    this.footerData= {
      'footerColor':'rgba(255,255,255,0.9)',
      'leftButtonName':'CREATE TASK',
      'rightButtonName':'SAVE DRAFT',
      'leftButtonColor':'green',
      'rightButtonColor':'grey'
      }

    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/').on('value', resp => {
      let frequency = snapshotToArray(resp);
      this.frequency = frequency[0].frequency;
      console.log(this.frequency);
    });
    let date_string = new Date().toISOString().split("T")[0];
    this.today_date = new Date();
    this.date_in_ms = new Date(date_string).getTime();
    this.taskByAdhoc['date']= this.date_in_ms;
    if (!this.editTask) {
      var end_date = new Date(new Date().setFullYear(new Date().getFullYear() + 1))
      var end_month = end_date.getMonth() + 1;
      this.dateAfterOnerYear = end_date.getFullYear() + "-" + end_month + '-' + end_date.getDate();
      // this.taskByAdhoc['start_date'] = this.date_in_ms;
      // this.taskByAdhoc['end_date'] = new Date(this.dateAfterOnerYear).getTime();
    }

  }
  ionViewDidEnter(){
    let that = this;
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').on('value', resp => {
      this.blocks = snapshotToArray(resp);
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').orderByChild('role').equalTo('labour').once("value", function (Data) {
      that.laboursData = snapshotToArray(Data);
      console.log(that.laboursData);
     });
  }
  getBlock(val) {
    this.block = [];
    console.log(val);
    this.block.push({ "name": val });
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: this.success_msg,
      duration: 2000
    });
    toast.present();
  }
  getLabour(val) {
    console.log(val);
    this.labour = val;
    if (this.labour) {
      this.isLabour = false;
    }
    else {
      this.isLabour = true;
    }
  }
  getPriority(val) {
    console.log(val);
    this.priority = val;
  }
  selectedTask(val) {
    console.log(val);
    this.task = val;
  }
  getAdditionalNotes(val) {
    console.log(val);
    this.description = val;
  }
  getRecordedAudio(val) {
    console.log(val);
    this.audio = val.url;
    this.taskByAdhoc.media['audio'] = val;
    //alert( this.taskByAdhoc.media.audio.url)
  }
  getVideo(val) {
    this.video = val;
    this.taskByAdhoc.media['video'] = val;
    //alert( this.taskByAdhoc.media.video.url)
  }
  getCameraImage(val) {
    this.camera = val;
    this.taskByAdhoc.media['camera'] = val;
    //alert( this.taskByAdhoc.media.camera.url)
  }
  getAttachments(val) {
    this.attachments.push(val);
    this.taskByAdhoc.media['attachments'] = [];
    this.taskByAdhoc.media['attachments'] = this.attachments;

  }
  async openDaysOfMonthPopOver(val) {
    //this.isdateOpen= this.isdateOpen+1;
    if (!this.editTask) {
      const popover = await this.popoverController.create({
        component: SelectDateComponent,
        cssClass: "datePopover_class",
        componentProps: {
          date: val,
          startDate: this.selectedStartDate,
          endDate: this.selectedEndDate,
          dates: this.selectedDates
        },
      });
      popover.onDidDismiss().then((detail: OverlayEventDetail) => {
        this.getDates(detail.data);
      });
      await popover.present();
    }
    if (this.editTask) {
      // this.isdateOpen = this.isdateOpen + 1;
      // if (this.isdateOpen >= 1) {

      const popover = await this.popoverController.create({
        component: SelectDateComponent,
        cssClass: "datePopover_class",
        componentProps: {
          date: val,
          startDate: this.editTask.start_date,
          endDate: this.editTask.end_date,
          dates: this.editTask.frequency.dates

        },
      });
      popover.onDidDismiss().then((detail: OverlayEventDetail) => {
        this.getDates(detail.data);
      });
      await popover.present();
      // }
    }
  }

  // async openDaysOfMonthPopOver(val) {
  //   //this.isdateOpen= this.isdateOpen+1;
  //   if (!this.editTask) {
  //     const popover = await this.popoverController.create({
  //       component: SelectDateComponent,
  //       cssClass: "datePopover_class",
  //       componentProps: {
  //         date: val,
  //         startDate: this.selectedStartDate,
  //         endDate: this.selectedEndDate,
  //         dates:this.selectedDates
  //       },
  //     });
  //     popover.onDidDismiss().then((detail: OverlayEventDetail) => {
  //       this.getDates(detail.data);
  //     });
  //     await popover.present();
  //   }
  //   if (this.editTask) {
  //     this.isdateOpen = this.isdateOpen + 1;
  //     if (this.isdateOpen >= 1) {

  //       const popover = await this.popoverController.create({
  //         component: SelectDateComponent,
  //         cssClass: "datePopover_class",
  //         componentProps: {
  //           date: val,
  //           startDate: this.start_date,
  //           endDate: this.end_date,
  //           dates:this.editTask.frequency.dates

  //         },
  //       });
  //       popover.onDidDismiss().then((detail: OverlayEventDetail) => {
  //         this.getDates(detail.data);
  //       });
  //       await popover.present();
  //     }
  //   }
  // }
  // getDates(val) {
  //   console.log(val);
  //   var startDate: any;
  //   var endDate: any;
  //   var frequencyWithDays: any = [];
  //   if (val) {
  //     if (val.name == 'StartDate') {
  //       var d = new Date(val.startDate);
  //       if (this.taskByCategory.start_date) {
  //         this.taskByCategory.start_date = d.getTime();
  //         this.selectedStartDate = val.startDate;
  //       }
  //     }
  //     if (val.name == 'EndDate') {
  //       var d1 = new Date(val.endDate);
  //       if (this.taskByCategory.end_date) {
  //         this.taskByCategory.end_date = d1.getTime();
  //         this.selectedEndDate = val.endDate;
  //       }
  //     }
  //     if (val.name == 'date') {
  //       for (var i = 0; i < val.date.length; i++) {
  //         var d = new Date(val.date[i]);
  //         this.selectedDates.push(d.getDate());
  //       }
  //       if (this.selectedDates.length > 0) {

  //         this.taskByCategory.frequency['dates'] = this.selectedDates;
  //       }
  //       console.log(this.selectedDates);
  //     }

  //     console.log(this.taskByCategory);

  //   }
  // }

  // getDates(val) {
  //   console.log(val);
  //   var startDate: any;
  //   var endDate: any;
  //   var frequencyWithDays: any = [];
  //   if (val) {
  //     if (val.name == 'date') {
  //       for (var i = 0; i < val.date.length; i++) {
  //         var d = new Date(val.date[i]);
  //         this.selectedDates.push(d.getDate());
  //       }

  //     }
  //     console.log(this.selectedDates);

  //     console.log(this.taskByAdhoc);
  //   }
  // }
  getDates(val) {
    console.log(val);
    var startDate: any;
    var endDate: any;
    var frequencyWithDays: any = [];
    if (val) {
      this.isdateOpen = true;
      if (val.name == 'Start Date') {
        let temp = val.startDate.split('-');
        let temp_date = temp[1]+"-"+temp[0]+"-"+temp[2];
        var d = new Date(temp_date);
        if (this.taskByAdhoc.start_date) {
          this.taskByAdhoc.start_date = d.getTime();
          this.selectedStartDate = val.startDate;
        }
      }
      if (val.name == 'End Date') {
        let temp = val.endDate.split('-');
        let temp_date = temp[1]+"-"+temp[0]+"-"+temp[2];
        var d1 = new Date(temp_date);
        if (this.taskByAdhoc.end_date) {
          this.taskByAdhoc.end_date = d1.getTime();
          this.selectedEndDate = val.endDate;
        }
      }
      if (val.name == 'date') {

        for (var i = 0; i < val.date.length; i++) {
          var d = new Date(val.date[i]);
          this.selectedDates.push(d.getDate());
        }
        this.selectedDates1 = this.selectedDates;

      }
      console.log(this.selectedDates);
      console.log(this.taskByAdhoc);
      if (this.selectedDates) {
        frequencyWithDays = {
          'name': this.selectedFrequency, 'dates': this.selectedDates
        }
        this.taskByAdhoc.frequency = frequencyWithDays;
      }
    }
  }
  getFrequency(val) {
    console.log(val);
    this.selectedFrequency = val;
  }
  getDay(val) {
    console.log(val);
  }
  async closeImage(val, idx) {
    let message;
    let header;
    let ok;
    let cancel;
    if(this.sg['userdata'].primary_language === 'en'){
      header = "Confirm!"
      message  = "Are you sure want to delete this "
      ok = "Okay"
      cancel = "Cancel"
     } else if(this.sg['userdata'].primary_language === 'te'){
      header = "కన్ఫర్మ్!"
      message  = "మీరు దీన్ని ఖచ్చితంగా తొలగించాలనుకుంటున్నారా "
      cancel = "కాదు"
      ok = "సరే"
     } else if(this.sg['userdata'].primary_language === 'ta'){
      header = "உறுதிசெய்!"
      message  = "இதை நிச்சயமாக நீக்க விரும்புகிறீர்களா "
      cancel = "இல்லை"
      ok = "சரி"
     } else if(this.sg['userdata'].primary_language === 'kn'){
      header = "ಖಚಿತಪಡಿಸಿ!"
      message  = "ಇದನ್ನು ಅಳಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ "
      cancel = "ಇಲ್ಲ"
      ok = "ಸರಿ"
     } else if(this.sg['userdata'].primary_language === 'hi'){
      header = "पुष्टि करें!"
      message  = "क्या आप वाकई इसे हटाना चाहते हैं "
      cancel = "नहीं"
      ok = "ठीक है"
     }
    const alert = await this.alertController.create({
      header: header,
      message: message + val + '?',
      buttons: [
        {
          text: cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: ok,
          handler: () => {
            this.removeAttachment(val, idx);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  removeAttachment(val, idx) {
    let that = this;
    if (val == 'audio') {
      for (var i = 0; i < this.taskByAdhoc.media.audio.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByAdhoc.media.audio[i].path).delete()
          that.taskByAdhoc.media.audio.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef.update(that.taskByAdhoc.media);
          }


        }
      }

    }
    if (val == 'video') {
      for (var i = 0; i < this.taskByAdhoc.media.video.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByAdhoc.media.video[i].path).delete()
          that.taskByAdhoc.media.video.splice(idx, 1);
          if (that.editTask) {
            var playersRef1 = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef1.update(that.taskByAdhoc.media);
          }
        }
      }

    }
    if (val == 'camera') {
      for (var i = 0; i < this.taskByAdhoc.media.camera.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByAdhoc.media.camera[i].path).delete()
          that.taskByAdhoc.media.camera.splice(idx, 1);
          if (that.editTask) {
            var playersRef2 = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef2.update(that.taskByAdhoc.media);
          }
        }

      }
    }
    if (val == 'attachments') {
      for (var i = 0; i < this.taskByAdhoc.media.attachments.length; i++) {
        if (i == idx) {
          this.taskByAdhoc.media.attachments[i] = null;

          firebase.storage().ref(that.taskByAdhoc.media.attachments[i].path.name).delete()
          that.taskByAdhoc.media.attachments.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id+"/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef.update(that.taskByAdhoc.media);
          }
        }
      }
      console.log(this.taskByAdhoc.media.attachments);
    }
  }


  createTask(val) {
    if (this.labour) {
      let that = this;
      this.taskByAdhoc['blocks'] = this.block;
      this.taskByAdhoc["assignee"] = this.labour;
      this.taskByAdhoc['priority'] = this.priority;
      this.taskByAdhoc['description'] = this.description;
      this.taskByAdhoc['adhoc_task'] = this.task;
      console.log(this.taskByAdhoc);

      if (!this.editTask) {
        if(val=='leftButtonAction'){
          this.taskByAdhoc['status'] = {'name':'Acknowledge'};
        }else if(val=='rightButtonAction'){
          this.taskByAdhoc['status'] ={'name':'Draft'};;
        }
        this.taskByAdhoc['frequency'] = { 'name': this.selectedFrequency, 'dates': this.selectedDates }
        this.taskByAdhoc['task_type'] = 'Adhoc';
        this.taskByAdhoc['category'] = this.selectedCategory;
        this.taskByAdhoc['created'] = {
          "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
          'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
        };
        console.log(this.taskByAdhoc);

        var connectedRef = firebase.database().ref(".info/connected");
        connectedRef.on("value", function (snap) {
          if (snap.val() === true) {
            that.createTaskInOnline();

          } else {
            that.createTaskInOffLine();
          }
        });
      }
      if (this.editTask) {
        this.taskByAdhoc['status'] = this.editTask.status;
        this.taskByAdhoc['task_type'] = this.editTask.task_type;
        // this.taskByAdhoc['created'] = this.editTask.created;
        this.taskByAdhoc['category'] = this.editTask.category;
        this.taskByAdhoc.frequency.name = this.selectedFrequency;
        this.taskByAdhoc['updated'] = {
          "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
          'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
        };
        // this.taskByAdhoc['updated_date'] = this.date_in_ms;
        var connectedRefEdit = firebase.database().ref(".info/connected");
        connectedRefEdit.on("value", function (snap) {
          if (snap.val() === true) {
            that.updateTaskInOnline();

          } else {
            that.createTaskInOffLine();
          }
        });

      }
    }
    else {
      this.isLabour = true;

    }
  }

  createTaskInOffLine() {
    this.count1 = this.count1 + 1;
    if (this.count <= 1) {
      this.location.back();
      // this.navCtrl.navigateBack('/task-list');
    }
  }

  createTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let totalTask_tableData: any;
      let task_tableLength: any;

      let newData = this.taskManager_Task_table.push();
      newData.set(this.taskByAdhoc);
      let hours = new Date().getHours();
      if(this.taskByAdhoc['date'] == this.date_in_ms && hours>=6){
        this.other.sendNotification(this.taskByAdhoc);
      }
      // let usersRef = firebase.database().ref('TaskManager/Task_table');
      this.taskManager_Task_table.once("value", function (Data) {
        totalTask_tableData = snapshotToArray(Data);
        console.log(totalTask_tableData);

        task_tableLength = totalTask_tableData.length - 1;
        console.log(task_tableLength);
        if (totalTask_tableData) {
          that.Task_logTable(totalTask_tableData);
        }
      });
    }
  }
  updateTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let usersRef1 = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table/' + this.editTask.key);
      usersRef1.set(this.taskByAdhoc);
      if (usersRef1) {
        that.Task_logTable(this.editTask);
      }
    }

  }

  Task_logTable(totalTask_tableData) {
    /* pushing data to Task_logTable */
    if (!this.editTask) {
      console.log(totalTask_tableData[totalTask_tableData.length - 1]);
      this.task_logData['created'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      this.task_logData['task_id'] = totalTask_tableData[totalTask_tableData.length - 1].key;
      this.task_logData['status'] = { 'name': 'Acknowledge' };
      if(this.taskByAdhoc.assignee){
      this.task_logData['assignee'] = this.taskByAdhoc.assignee;
      }else{
        this.task_logData['assignee']=null;
      }
      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.presentToast();
      this.location.back();
      //setTimeout(() => {  this.location.back(); this.location.back();  },10);
    }
    if (this.editTask) {
      this.task_logData['updated'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      this.task_logData['task_id'] = this.editTask.key;
      this.task_logData['status'] = this.editTask.status;
      this.task_logData['assignee'] = this.editTask.assignee;
      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.presentToast();
      this.location.back();
    }
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
