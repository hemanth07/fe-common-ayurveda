import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import {ComponentsModule} from '../../components/components.module';
import {SelectDateComponent} from '../../components/task-manger-components/select-date/select-date.component'
import { IonicModule} from '@ionic/angular';
import { CreateTaskAdhocPage } from './create-task-adhoc.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: CreateTaskAdhocPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TaskManagerComponentsModule,
    ComponentsModule,
    IonicModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreateTaskAdhocPage],
  entryComponents: [SelectDateComponent]
})
export class CreateTaskAdhocPageModule {}
