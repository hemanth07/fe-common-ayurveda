import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import {ComponentsModule} from '../../components/components.module';
import { CreateTaskLabourPage } from './create-task-labour.page';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import{CalendercomponentComponent} from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import {HarvestcomponentsModule} from '../../components/harvestcomponents/harvestcomponents.module'
import { NgCalendarModule } from 'ionic2-calendar';
import { UsercomponentsModule } from 'src/app/components/usercomponents/usercomponents.module';
import { ContactComponent } from 'src/app/components/contact/contact.component';
import { SelectDateComponent } from 'src/app/components/task-manger-components/select-date/select-date.component';
import { SelectPlotsBlockComponent } from 'src/app/components/task-manger-components/select-plots-block/select-plots-block.component';
import { SelectSourcePopOverComponent } from 'src/app/components/task-manger-components/select-source-pop-over/select-source-pop-over.component';
const routes: Routes = [
  {
    path: '',
    component: CreateTaskLabourPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ObservationComponentsModule,
    UsercomponentsModule,
    NgCalendarModule,
    TaskManagerComponentsModule,
    HarvestcomponentsModule,
    
    RouterModule.forChild(routes)
  ],
  declarations: [CreateTaskLabourPage],
  entryComponents: [CalendercomponentComponent,
    SelectPlotsBlockComponent,SelectSourcePopOverComponent,SelectDateComponent,ContactComponent
  ]
})
export class CreateTaskLabourPageModule {}
