import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { SimpleGlobal } from 'ng2-simple-global';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate, Location } from '@angular/common';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-task-detailed-view',
  templateUrl: './task-detailed-view.page.html',
  styleUrls: ['./task-detailed-view.page.scss'],
})
export class TaskDetailedViewPage implements OnInit {
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable');
  task_logData:any=[];
  headerData: any;
  task:any;
  updateHistoryData: any = [];
  name: any;
  creator: any;
  priorty: any;
  end_date: any;
  start_date: any;
  key: string;
  assignee_key: string;
  constructor(private sg: SimpleGlobal,private route: ActivatedRoute,
    @Inject(LOCALE_ID) private locale: string,
    public toastController: ToastController,
    public router: Router
    ) { }

  ngOnInit() {
    this.task =JSON.parse(this.route.snapshot.paramMap.get('task'));
    this.name = this.task.description;
this.creator = this.task.created.name;
this.priorty = this.task.priority;
this.key=this.task.key
this.assignee_key=this.task.created.key;
this.end_date = formatDate(this.task.start_date, 'medium', this.locale);
this.start_date = formatDate(this.task.end_date, 'medium', this.locale);

// alert(JSON.stringify(this.assignee_key))

// this.start_date=this.task.start_date

    debugger
    // console.log(th/is.task.end_date);
   
   let data
     if(this.sg['userdata'].primary_language === 'en'){
     data = 'Update Task';
    } else if(this.sg['userdata'].primary_language === 'te'){
     data = 'టాస్క్ అప్డేట్ చేయండి';
    } else if(this.sg['userdata'].primary_language === 'ta'){
     data = 'பணி மேம்படுத்தல்';
    } else if(this.sg['userdata'].primary_language === 'kn'){
     data = 'ಅಪ್ಡೇಟ್ ಕಾರ್ಯ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
     data = 'टास्क अपडेट करें';
    }
    this.headerData = {
      'title': data, 'back': '/task-list', 'color': 'green',button1: 'home',button1_action: '/farm-dashboard'
      };
  }
  updateStatus(val){
    console.log(val);
    var today_date = new Date();
    var date_in_ms = today_date.getTime();
    firebase.database().ref(this.sg['userdata'].vendor_id+"/taskManager/task_table/"+val.key)
    .update({'status': val.status,'updated_date':date_in_ms});

    this.task_logData['updated'] = { "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone, 
    'role': this.sg["userdata"].role,'profile_url': this.sg["userdata"].profile_url,'date': date_in_ms};
    this.task_logData['task_id'] = val.key;
    this.task_logData['status'] = val.status;
   
    let logData = this.taskManager_Task_log.push();
    logData.set(this.task_logData);
  }
  updateTask(val){
    console.log(val);
    console.log(val);
    var today_date = new Date();
    var date_in_ms = today_date.getTime();
    firebase.database().ref(this.sg['userdata'].vendor_id+"/taskManager/task_table/"+val.key)
    .update({'status': val.status,'updated_date':date_in_ms});

    this.task_logData['updated'] = { "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone, 
    'role': this.sg["userdata"].role,'profile_url': this.sg["userdata"].profile_url,'date': date_in_ms};
    this.task_logData['task_id'] = val.key;
    this.task_logData['status'] = val.status;
   
    let logData = this.taskManager_Task_log.push();
    logData.set(this.task_logData);
  }


  async submitRead(){ 
    let eventCopy = {
      senderimage:this.sg['userdata'].profile_url ,
      notification_status: "Mark as Done",
      notiification_description:  this.name,
      senderkey:this.sg['userdata'].key,
      sendername: this.sg['userdata'].name,
      phone: this.sg['userdata'].phone,
      status:  "Acknowledge",   
      priority:this.priorty,  
      enddate:this.end_date,
      createdat:this.start_date,      
     read:true,
    }
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + this.assignee_key +'/notifications/').push(eventCopy);
    // this.location.back(); 
    this.router.navigateByUrl('/farm-dashboard');
  
  // }
    const toast = await this.toastController.create({
      message: 'Message was Updated',
      duration: 2000
    });
    toast.present();        
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
