import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';

import { IonicModule } from '@ionic/angular';

import { TaskDetailedViewPage } from './task-detailed-view.page';

const routes: Routes = [
  {
    path: '',
    component: TaskDetailedViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    TaskManagerComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TaskDetailedViewPage]
})
export class TaskDetailedViewPageModule {}
