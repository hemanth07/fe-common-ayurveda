import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { IonicModule } from '@ionic/angular';

import { TmMainmenuPage } from './tm-mainmenu.page';

const routes: Routes = [
  {
    path: '',
    component: TmMainmenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TmMainmenuPage]
})
export class TmMainmenuPageModule {}
