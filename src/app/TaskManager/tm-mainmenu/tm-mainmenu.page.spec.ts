import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmMainmenuPage } from './tm-mainmenu.page';

describe('TmMainmenuPage', () => {
  let component: TmMainmenuPage;
  let fixture: ComponentFixture<TmMainmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmMainmenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmMainmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
