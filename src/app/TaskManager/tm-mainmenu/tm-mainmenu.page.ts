import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { ActivatedRoute } from '@angular/router';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';

@Component({
  selector: 'app-tm-mainmenu',
  templateUrl: './tm-mainmenu.page.html',
  styleUrls: ['./tm-mainmenu.page.scss'],
})
export class TmMainmenuPage implements OnInit {
  mainMenus:any;
  lovs:any=[];
  title:string;
  defaultHref='';
  back:any;
  headerData:any;
  constructor(private router: Router, public navCtrl: NavController, public sg: SimpleGlobal,
     private translate: FarmeasyTranslate,private route: ActivatedRoute,
     private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService) { }
  ngOnInit() {
    // let sub1 = this.route.params.subscribe(params => {
    //   this.back = JSON.parse(params["page_status"]);
    //   console.log(this.back);
    // });
    
    // this.defaultHref = this.back;
  
    this.translation();
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.mainMenus  = this.en.getTaskManagerMenu();
      this.title = 'Create Task';
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.mainMenus  = this.te.getTaskManagerMenu();
      this.title = 'టాస్క్ సృష్టించండి';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.mainMenus  = this.ta.getTaskManagerMenu();
      this.title = 'பணியை உருவாக்கவும்';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.mainMenus  = this.kn.getTaskManagerMenu();
      this.title = 'ಕಾರ್ಯವನ್ನು ರಚಿಸಿ';
    }else if(this.sg['userdata'].primary_language === 'hi'){
      this.mainMenus  = this.hi.getTaskManagerMenu();
      this.title = 'कार्य बनाएँ';
    }  else {
       this.mainMenus  = this.en.getTaskManagerMenu();
       this.title = 'Create Task';
    }
    this.headerData = {
      'title': this.title,'color': 'green',button1: 'home',button1_action: '/farm-dashboard'
      };
  }
  ionViewDidEnter() {
   
  }
  selectedTask(task){
      this.navCtrl.navigateForward(task.action);
  }
  backTODashboard(){
    this.navCtrl.navigateRoot('/farm-dashboard');
  }
  nofication_count(){
    let data={"page_status":JSON.stringify("tm-mainmenu")}
    this.navCtrl.navigateForward(['/notifications',data]);
  }

}
