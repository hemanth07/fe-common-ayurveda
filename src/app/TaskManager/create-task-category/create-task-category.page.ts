import { Component, OnInit, OnDestroy, ViewChild, Inject, LOCALE_ID } from '@angular/core';
import { NavController, PopoverController, AlertController, ToastController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import { SelectPlotsBlockComponent } from '../../components/task-manger-components/select-plots-block/select-plots-block.component';
import { SelectSourcePopOverComponent } from '../../components/task-manger-components/select-source-pop-over/select-source-pop-over.component'
import { SelectDateComponent } from '../../components/task-manger-components/select-date/select-date.component';
import * as firebase from 'firebase';
import { SimpleGlobal } from 'ng2-simple-global';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Router, Route } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { formatDate, Location } from '@angular/common';
import { OtherService } from './../../languages/others.service';
import { CalendarComponent } from 'ionic2-calendar';

@Component({
  selector: 'app-create-task-category',
  templateUrl: './create-task-category.page.html',
  styleUrls: ['./create-task-category.page.scss'],
})
export class CreateTaskCategoryPage implements OnInit {
  taskManager_Task_table = firebase.database().ref(this.sg['userdata'].vendor_id + '/taskManager/task_table');
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id + '/taskManager/task_logTable');
  categories: any;
  categoriesData: any = [];
  blocks: any;
  selectFrequency: any;
  selectedFrequency: any;
  selectedDay: any;
  selectedBlock: any;
  selectedCategory: any;
  selected_plots: any;
  isCreatedTask: boolean = true;
  category: any = [];
  waterSource: any = [];
  days: any = []
  taskByCategory: any = [];
  task_by_category: any = [];
  plots: any = [];
  source: any = [];
  cleaningSource: any = [];
  lovs: any = [];
  finalData: any = [];
  categoryByPlots: any = [];
  //startEndDates: any;
  selectedStartDate: any;
  selectedEndDate: any;
  selectedDates: any = [];
  blocksWithPlots: any = [];
  displayData: boolean = false;
  currentDate: any;
  endDate: any;
  today_date: any;
  date_in_ms: any;
  task_logData: any = [];
  totalLaboursData: any = [];
  additionalNotes: any;
  showAdditionalNotes: boolean = false;
  editTask: any;
  isdateOpen: any = 0;
  start_date: any;
  end_date: any;
  defaultHref = '';
  count: any = 0;
  count1: any = 0;
  audios: any = [];
  videos: any = [];
  cameraImages: any = [];
  attachments: any = [];
  labels: any;
  selectedblocksWithCategory: any = [];
  selectedStartDateFormate: any;
  selectedEndDateFormate: any;
  headerData: any;
  allUser: any = [];
  usersData: any = []; footerData: any;
  assignee: { 'name': any; 'phone': any; 'profile_url': any; 'role': any; 'primary_language': any; 'device_id': string;'key': string, };
  laboursData: any;
  selected_blocks: any;
  isdraft: any;
  selectedBlockFlag: any = [];
  viewPlot: boolean = false;
  wait: string;
  task_created_label: string;
  success_label: string;
  saved_draft_label: string;
  yearDate: string;
  priority: any;
  usersrole: any=[];
  role: any[];
  allrole: any;
  allname: any = [];
  key: any;
  event = {
    // title: '',
    // desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };

  minDate = new Date().toISOString();
  allEvents: any = []
  eventSource = [];
  viewTitle;
  // eventSource;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
  @ViewChild(CalendarComponent) myCal: CalendarComponent;
  tempData: any = [];

  constructor(private navCtrl: NavController, private popoverController: PopoverController,
    @Inject(LOCALE_ID) private locale: string,
    private sg: SimpleGlobal, private route: ActivatedRoute, public loadingCtrl: LoadingController,
    public router: Router, private alertController: AlertController, private location: Location,
    public toastController: ToastController, private http: Http, private other: OtherService) {
  }

  ngOnInit() {
    this.resetEvent();

    this.getLabels();
    this.editTask = JSON.parse(this.route.snapshot.paramMap.get('data'));
    let date_string = new Date().toISOString().split("T")[0];
    this.today_date = new Date(date_string);
    this.date_in_ms = this.today_date.getTime();

    let that = this;
    if (!this.editData) {
      this.taskByCategory['media'] = [];
      this.taskByCategory.media['audio'] = [''];
      this.taskByCategory.media['video'] = [''];
      this.taskByCategory.media['camera'] = [''];
      this.taskByCategory.media['attachments'] = [''];

    }
  }
  ngOnDestroy() {

  }
  ionViewDidEnter() {
    let that = this;
    firebase.database().ref(this.sg['userdata'].vendor_id + '/lovs/blocks').on('value', resp => {
      let blocks = snapshotToArray(resp);
      this.categoriesData['blocks'] = blocks;
      console.log(blocks);
      that.editData();
    });
    firebase.database().ref(this.sg['userdata'].vendor_id + '/lovs/categories').on('value', resp => {
      let categories = snapshotToArray(resp);
      this.categoriesData['categories'] = categories;
      console.log(categories);
    });
    firebase.database().ref(this.sg['userdata'].vendor_id + '/lovs/').on('value', resp => {
      let frequency = snapshotToArray(resp);
      this.categoriesData['frequency'] = frequency[0].frequency;
      // this.categoriesData['frequency'] = frequency;
      console.log(frequency);
    });
    firebase.database().ref(this.sg['userdata'].vendor_id + '/lovs/sources').on('value', resp => {
      let sources = snapshotToArray(resp);
      this.categoriesData['sources'] = sources;
      console.log(sources);
      this.displayData = true;
    });

    console.log(this.categoriesData);
    this.taskByCategory['frequency'] = { 'name': '' };
    this.taskByCategory['media'] = {};
    this.setStartDate_endDate();
    
    
    firebase.database().ref(this.sg['userdata'].vendor_id + '/taskManager/task_table').on('value', resp => {
      let data = snapshotToArray(resp);
      for(let all of data){
          this.allname =all.assignee.name;
          this.allrole =all.start_date ;
          // alert(JSON.stringify(this.allname))
      }
  // alert(JSON.stringify(this.allname))

    });

  }
  getLabels() {

    if (this.sg['userdata'].primary_language === 'en') {
      this.wait = "Please Wait ..."
      this.task_created_label = 'Task Created'
      this.success_label = "Success"
      this.saved_draft_label = "Task saved in draft"
      this.labels = { title: "Create Task - By Category", create: "Create Task", description: 'Task Description' }
    } else if (this.sg['userdata'].primary_language === 'te') {
      this.wait = "దయచేసి వేచి ఉండండి ..."
      this.task_created_label = 'టాస్క్ సృష్టించబడింది'
      this.success_label = "విజయం"
      this.saved_draft_label = "టాస్క్ డ్రాఫ్ట్‌లో సేవ్ చేయబడింది"
      this.labels = { title: "టాస్క్ సృష్టించు - వర్గం ద్వారా", create: "టాస్క్ సృష్టించండి", description: 'టాస్క్ వివరణ' }
    } else if (this.sg['userdata'].primary_language === 'ta') {
      this.success_label = "வெற்றி"
      this.wait = "தயவுசெய்து காத்திருங்கள் ..."
      this.task_created_label = 'பணி உருவாக்கப்பட்டது'
      this.saved_draft_label = "பணி வரைவில் சேமிக்கப்பட்டது"
      this.labels = { title: "பணி உருவாக்க - வகை மூலம்", create: "பணி உருவாக்கவும்", description: 'பணி விளக்கம்' }
    } else if (this.sg['userdata'].primary_language === 'kn') {
      this.success_label = "ಯಶಸ್ಸು"
      this.wait = "ದಯಮಾಡಿ ನಿರೀಕ್ಷಿಸಿ ..."
      this.task_created_label = 'ಕಾರ್ಯವನ್ನು ರಚಿಸಲಾಗಿದೆ'
      this.saved_draft_label = "ಕಾರ್ಯವನ್ನು ಡ್ರಾಫ್ಟ್‌ನಲ್ಲಿ ಉಳಿಸಲಾಗಿದೆ"
      this.labels = { title: "ಟಾಸ್ಕ್ ರಚಿಸಿ - ವರ್ಗದಿಂದ", create: "ಟಾಸ್ಕ್ ರಚಿಸಿ", description: 'ಕಾರ್ಯ ವಿವರಣೆ' }
    } else if (this.sg['userdata'].primary_language === 'hi') {
      this.wait = "कृपया प्रतीक्षा करें ..."
      this.success_label = "सफलता"
      this.task_created_label = 'टास्क बनाया गया'
      this.saved_draft_label = "ड्राफ्ट में टास्क बचा"
      this.labels = { title: "कार्य बनाएँ - श्रेणी के द्वारा", create: "कार्य बनाएँ", description: 'कार्य विवरण' }
    }
    this.headerData = { 'title': this.labels.title, button1: 'home', button1_action: '/farm-dashboard', 'back': '/tm-mainmenu', 'color': 'green' };
    // this.footerData= {
    //   'footerColor':'rgba(255,255,255,0.9)',
    //   'leftButtonName':'CREATE TASK',
    //   'rightButtonName':'SAVE DRAFT',
    //   'leftButtonColor':'green',
    //   'rightButtonColor':'grey'
    //   }
    this.footerData = {
      'footerColor': 'rgba(255,255,255,0.9)',
      'leftButtonName': 'CREATE TASK',
      'leftButtonColor': 'green'
    }

  }
  editData() {
    if (this.editTask && this.categoriesData && this.categoriesData.blocks) {
      console.log(this.editTask);
      // this.getPlots(this.editTask.blocks);
      // this.getWaterSource(this.editTask.blocks);
      if (this.editTask.start_date || this.editTask.end_date) {
        //this.activeToggle = true;
        this.start_date = this.editTask.start_date;
        this.end_date = this.editTask.end_date
      }
    }
    if (this.editTask && this.editTask.media) {
      this.taskByCategory['media'] = this.editTask.media;
    }

  }
  setStartDate_endDate() {
    var end_date = new Date(new Date().setFullYear(new Date().getFullYear() + 1))
    var current_month = this.today_date.getMonth() + 1;
    var end_month = end_date.getMonth() + 1;
    var st = this.today_date.getFullYear() + '-' + current_month + "-" + this.today_date.getDate();
    var endDate = end_date.getFullYear() + "-" + end_month + '-' + end_date.getDate();
    this.currentDate = new Date(st);
    this.endDate = new Date(end_date);
    this.taskByCategory['start_date'] = this.date_in_ms;
    this.taskByCategory['end_date'] = this.endDate.getTime();

    if (this.editTask) {
      this.taskByCategory.start_date = this.editTask.start_date;
      this.taskByCategory.end_date = this.editTask.end_date;
      if (this.editTask.frequency.dates) {
        this.taskByCategory.frequency['dates'] = this.editTask.frequency.dates;
      }

    }
  }
  async getcategory(val) {

    //  if (val.id != '6') {
    this.selectedCategory = val;
    // alert(JSON.stringify(this.selectedCategory.name));
    // searchInventory
    firebase.database().ref(this.sg['userdata'].vendor_id + '/users/').on('value', resp => {
      let data = snapshotToArray(resp);
      this.allUser = data
      this.tempData =data
      // for ( let i of this.allUser){
      //   this.tempData = i.department;
      //         // alert(JSON.stringify(this.tempData)) 

      // }
      // var tempData  = this.usersData.department
      debugger

      this.usersData = this.tempData.filter(department =>  department.department == this.selectedCategory.name);

      // alert(JSON.stringify(this.usersData)) 

      this.usersData['page'] = 'createTaskByCategory';

    });
    var category = { "name": this.selectedCategory.name, 'image': this.selectedCategory.image }
    this.taskByCategory['category'] = this.selectedCategory;
    // alert(JSON.stringify(this.category));

    // }
    // if (val.id == '6') {
    //   let temp = { 'category': JSON.stringify(val) };
    //   console.log(temp);
    //   this.router.navigate(['/create-task-adhoc', temp]);
    // }
  }

  getFrequency(val) {
    this.selectedFrequency = val;
    console.log(this.selectedFrequency);
    let frequencyWithDays: any = [];
    this.showAdditionalNotes = true;
    this.taskByCategory.frequency.name = this.selectedFrequency;

  }
  getDay(val) {
    this.selectedDay = val;
    let index = null;
    this.days.filter((item, idx) => {
      if (item.id == val.id) {
        index = idx;
      }
    });
    if (index !== null) {
      this.days.splice(index, 1);
    } else {
      this.days.push(val);
    }
    console.log(this.days);
    let frequencyWithDays: any = [];
    // frequencyWithDays = {
    //   'name': this.selectedFrequency, 'daysOfWeek': this.days,
    //   'start_date': this.currentDate, 'end_date': this.endDate
    // }
    this.taskByCategory.frequency['daysOfWeek'] = this.days;
    this.showAdditionalNotes = true;
    console.log(this.taskByCategory);
  }
  getBlock(val) {
    console.log(val);
    this.selectedBlock = val;
    this.task_by_category['blocks'] = { 'name': this.selectedBlock.name };
    console.log(this.taskByCategory);
    this.presentPopover();
  }
  async presentPopover() {

    const popover = await this.popoverController.create({
      component: SelectPlotsBlockComponent,
      cssClass: "popover_class",
      componentProps: {
        blockName: this.selectedBlock,
        totalBlocks: this.categoriesData.blocks
      },
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getPlots(detail.data);
    });
    await popover.present();
  }

  async openDaysOfMonthPopOver(val) {
    //this.isdateOpen= this.isdateOpen+1;
    if (!this.editTask) {
      const popover = await this.popoverController.create({
        component: SelectDateComponent,
        cssClass: "datePopover_class",
        componentProps: {
          date: val,
          startDate: this.selectedStartDate,
          endDate: this.selectedEndDate,
          dates: this.selectedDates
        },
      });
      popover.onDidDismiss().then((detail: OverlayEventDetail) => {
        this.getDates(detail.data);
      });
      await popover.present();
    }
    if (this.editTask) {
      this.isdateOpen = this.isdateOpen + 1;
      if (this.isdateOpen >= 1) {

        const popover = await this.popoverController.create({
          component: SelectDateComponent,
          cssClass: "datePopover_class",
          componentProps: {
            date: val,
            startDate: this.start_date,
            endDate: this.end_date,
            dates: this.editTask.frequency.dates

          },
        });
        popover.onDidDismiss().then((detail: OverlayEventDetail) => {
          this.getDates(detail.data);
        });
        await popover.present();
      }
    }
    this.showAdditionalNotes = true;
  }
  getPriority(val) {
    this.priority = val;
  }
  getDates(val) {
    console.log(val);
    var startDate: any;
    var endDate: any;
    let that = this;

    var frequencyWithDays: any = [];
    if (val) {
      if (val.name == 'Start Date') {
        var d = new Date(val.dateFormate);
        if (this.taskByCategory.start_date) {
          this.taskByCategory.start_date = d.getTime();
          if (this.taskByCategory.frequency.name == 'One Time') {
            this.taskByCategory.end_date = d.getTime();
            that.selectedEndDate = val.dateFormate;
            that.selectedEndDateFormate = val.endDate;
          }
          that.selectedStartDate = val.dateFormate;
          that.selectedStartDateFormate = val.startDate;

        }
      }
      if (val.name == 'End Date') {
        var d1 = new Date(val.dateFormate);
        if (this.taskByCategory.end_date) {
          this.taskByCategory.end_date = d1.getTime();
          that.selectedEndDate = val.dateFormate;
          that.selectedEndDateFormate = val.endDate;

        }
      }
      if (val.name == 'Select Date') {
        if (val.date) {
          this.yearDate = val.date;
          this.taskByCategory.frequency['date'] = this.yearDate.split("-")[0];
          this.taskByCategory.frequency['month'] = this.yearDate.split("-")[1];
        }
      }
      if (val.name == 'date') {
        this.selectedDates = [];
        for (var i = 0; i < val.date.length; i++) {
          var d = new Date(val.date[i]);
          this.selectedDates.push(d.getDate());
        }
        if (this.selectedDates.length > 0) {

          this.taskByCategory.frequency['dates'] = this.selectedDates;
        }
        console.log(this.selectedDates);
      }

      console.log(this.taskByCategory);

    }
  }
  backToTmMainMenu() {
    this.navCtrl.navigateRoot('/tm-mainmenu');
  }
  getPlots(val) {
    console.log(val);
    this.selected_blocks = val;
    console.log(this.categoriesData.blocks);
    this.blocksWithPlots;
    let data: any = [];
    let data1: any = [];
    let temp: any = 0;
    if (val) {
      if (!this.editTask) {
        for (var i = 0; i < val.length; i++) {
          for (var j = 0; j < this.categoriesData.blocks.length; j++) {
            if (val.blockName == this.categoriesData.blocks[j].name) {
              data.push(val[i]);
              data1.push({ 'name': val[i].name })
              this.categoriesData.blocks[j]['plots'] = data;
            }
          }
        }
        if (val.length == 0) {
          for (var j = 0; j < this.categoriesData.blocks.length; j++) {
            if (val.blockName == this.categoriesData.blocks[j].name) {
              this.categoriesData.blocks[j]['plots'] = [];
            }
          }
        }
        this.blocksWithPlots = { 'name': val.blockName, 'plots': data1 };
        // this.plots.push({ 'name': val.blockName, 'plots': data });
        let count = 0;
        if (this.taskByCategory['blocks']) {
          for (let block = 0; block < this.taskByCategory['blocks'].length; block++) {
            if (this.taskByCategory['blocks'][block].name == this.blocksWithPlots.name) {
              this.taskByCategory['blocks'][block] = this.blocksWithPlots;
              count++;
            }
          }
        } else {
          this.taskByCategory['blocks'] = [];
          this.taskByCategory['blocks'].push(this.blocksWithPlots);
          count++;
        }

        if (count == 0) {
          this.taskByCategory['blocks'].push(this.blocksWithPlots);
        }

      }
      if (this.editTask && val.blockName) {
        for (var i = 0; i < val.length; i++) {
          for (var j = 0; j < this.categoriesData.blocks.length; j++) {
            if (val.blockName == this.categoriesData.blocks[j].name) {
              data.push(val[i]);
              data1.push({ 'name': val[i].name })
              this.categoriesData.blocks[j]['plots'] = data;
            }
          }
        }
        this.blocksWithPlots = { 'name': val.blockName, 'plots': data1 };
        // this.plots.push({ 'name': val.blockName, 'plots': data });
        // this.taskByCategory['blocks'] = this.blocksWithPlots;
        let count = 0;
        if (this.taskByCategory['blocks']) {
          for (let block = 0; block < this.taskByCategory['blocks'].length; block++) {
            if (this.taskByCategory['blocks'][block].name == this.blocksWithPlots.name) {
              this.taskByCategory['blocks'][block] = this.blocksWithPlots;
              count++;
            }
          }
        } else {
          this.taskByCategory['blocks'] = [];
          this.taskByCategory['blocks'].push(this.blocksWithPlots);
          count++;
        }

        if (count == 0) {
          this.taskByCategory['blocks'].push(this.blocksWithPlots);
        }
      }
      if (this.editTask && !val.blockName) {
        let blockName: any = [];
        for (var i = 0; i < val.length; i++) {
          data1 = [];
          data = [];
          for (var j = 0; j < val[i].plots.length; j++) {
            for (var k = 0; k < this.categoriesData.blocks.length; k++) {
              if (val[i].name == this.categoriesData.blocks[k].name) {
                data.push({ "name": val[i].plots[j].name, "isSelectedPlot": true });
                data1.push(val[i].plots[j]);
                this.categoriesData.blocks[k]['plots'] = data;
              } else {
                this.categoriesData.blocks[k]['plots'] = [];
              }
            }
          }
          blockName.push({ "name": val[i].name, 'plots': data1 });
        }
        console.log(blockName);
        this.blocksWithPlots = blockName;
        // this.plots.push({ 'name': val.blockName, 'plots': data });
        this.taskByCategory['blocks'] = this.blocksWithPlots;
      }
      for (var i = 0; i < this.categoriesData.blocks.length; i++) {
        if (this.categoriesData.blocks[i].plots) {
          if (this.categoriesData.blocks[i].plots.length > 0) {
            for (let plot = 0; plot < this.categoriesData.blocks[i].plots.length; plot++) {
              if (this.categoriesData.blocks[i].plots[plot].name == "") {
                this.categoriesData.blocks[i].plots.splice(plot, 1)
              }
            }
          }
        }
      }
      for (var i = 0; i < this.categoriesData.blocks.length; i++) {
        if (this.categoriesData.blocks[i].plots) {
          if (this.categoriesData.blocks[i].plots.length > temp) {
            temp = this.categoriesData.blocks[i].plots.length;
          }
        }
      }
      for (var i = 0; i < this.categoriesData.blocks.length; i++) {
        var tempPlots: any = [];
        for (var j = 0; j < temp;) {
          if (this.categoriesData.blocks[i].plots) {
            if (this.categoriesData.blocks[i].plots[j] && this.categoriesData.blocks[i].plots[j].isSelectedPlot) {
              j++;
            }
            else {
              this.categoriesData.blocks[i].plots[j] = { "name": '', 'isSelectedPlot': false }
              j++;
            }
          }
          else {
            tempPlots.push({ "name": '', 'isSelectedPlot': false });
            j++;
            this.categoriesData.blocks[i]['plots'] = tempPlots;
          }
        }
      }
      console.log(this.taskByCategory);
      console.log(this.categoriesData.blocks);
      this.categoryByPlots = this.categoriesData.blocks;
      this.selectedblocksWithCategory = this.categoriesData;
      if (this.selectedCategory) {
        this.selectedblocksWithCategory['categoryId'] = this.selectedCategory.id;
      }
      console.log(this.categoryByPlots);
      if (this.categoryByPlots) {
        for (let i = 0; i < this.categoryByPlots.length; i++) {
          this.selectedBlockFlag[i] = false;
          if (this.categoryByPlots[i].plots) {
            for (let plot of this.categoryByPlots[i].plots) {
              if (plot.isSelectedPlot == true) {
                this.selectedBlockFlag[i] = true;
              }
            }
          } else {
            this.selectedBlockFlag[i] = false;
          }
        }
        this.viewPlot = this.selectedBlockFlag.includes(true);
      }
    }

  }
  getselected_blockWithPlotName(val) {
    console.log(val);
    this.selectSourcePopOver(val);
  }
  async selectSourcePopOver(val) {
    let source1: any = []
    let selected_source_labour = [];
    if (val) {
      for (let block of this.taskByCategory.blocks) {
        if (block.name == val.block) {
          for (let plot of block.plots) {
            if (plot.name == val.plot) {
              selected_source_labour = plot;
            }
          }
        }
      }
    }
    if (val) {
      for (var i = 0; i < this.categoriesData.sources.length; i++) {
        if (this.categoriesData.sources[i].id == this.selectedCategory.id) {
          source1 = this.categoriesData.sources[i];
        }
      }


      source1['plot'] = val.plot;
      source1['block'] = val.block;
      console.log(source1);
      if (source1) {
        let popover = await this.popoverController.create({
          component: SelectSourcePopOverComponent,
          cssClass: "popover_class",
          componentProps: {
            category: source1,
            labours: this.usersData,
            selected_values: selected_source_labour,
          },

        });

        popover.onDidDismiss().then((detail: OverlayEventDetail) => {
          this.getWaterSource(detail.data);
          console.log(detail.data);
        });
        await popover.present();
      }
    }
  }
  getWaterSource(val) {
    console.log(val);
    console.log(this.taskByCategory);
    if (val) {
      if (!this.editTask) {

        this.showAdditionalNotes = true;
        for (var i = 0; i < this.categoryByPlots.length; i++) {
          if (this.categoryByPlots[i].name == val.blockName) {
            for (var j = 0; j < this.categoryByPlots[i].plots.length; j++) {
              if (this.categoryByPlots[i].plots[j].name == val.plotName) {
                /* Plots selection */
                // this.categoryByPlots[i].plots[j]['source'] = val.source;

                /* Plots With Labour Selection */
                if (val.source) {
                  this.categoryByPlots[i].plots[j]['source'] = val.source;
                } if (val.labour) {
                  this.categoryByPlots[i].plots[j]['assignee'] = val.labour;
                }
              }
            }
          }
        }
        for (var i = 0; i < this.taskByCategory.blocks.length; i++) {
          if (this.taskByCategory.blocks[i].name == val.blockName) {
            for (var j = 0; j < this.taskByCategory.blocks[i].plots.length; j++) {
              if (this.taskByCategory.blocks[i].plots[j].name == val.plotName) {
                /* Plots selection */
                // this.taskByCategory.blocks[i].plots[j]['source'] = val.source;

                /* Plots With Labour Selection */
                if (val.source) {
                  this.taskByCategory.blocks[i].plots[j]['source'] = val.source;
                } if (val.labour) {
                  this.taskByCategory.blocks[i].plots[j]['assignee'] = val.labour;
                }
              }
            }
          }
        }
      }
      if (this.editTask && val.blockName) {
        this.showAdditionalNotes = true;
        for (var i = 0; i < this.categoryByPlots.length; i++) {
          if (this.categoryByPlots[i].name == val.blockName) {
            for (var j = 0; j < this.categoryByPlots[i].plots.length; j++) {
              if (this.categoryByPlots[i].plots[j].name == val.plotName) {
                this.categoryByPlots[i].plots[j]['source'] = val.source;
              }
            }
          }
        }
        for (var i = 0; i < this.taskByCategory.blocks.length; i++) {
          if (this.taskByCategory.blocks[i].name == val.blockName) {
            for (var j = 0; j < this.taskByCategory.blocks[i].plots.length; j++) {
              if (this.taskByCategory.blocks[i].plots[j].name == val.plotName) {
                this.taskByCategory.blocks[i].plots[j]['source'] = val.source;
              }
            }
          }
        }
      }
      if (this.editTask && !val.blockName) {
        this.showAdditionalNotes = true;

        for (var i = 0; i < this.categoryByPlots.length; i++) {
          for (var j = 0; j < this.editTask.blocks.length; j++) {
            if (this.categoryByPlots[i].name == this.editTask.blocks[j].name) {
              for (var k = 0; k < this.categoryByPlots[i].plots.length; k++) {
                for (var l = 0; l < this.editTask.blocks[j].plots.length; l++) {
                  if (this.categoryByPlots[i].plots[k].name == this.editTask.blocks[j].plots[l].name) {
                    this.categoryByPlots[i].plots[k]['source'] = this.editTask.blocks[j].plots[l].source;
                  }
                }

              }
            }
          }

        }
        for (var i = 0; i < this.taskByCategory.blocks.length; i++) {
          for (var j = 0; j < this.editTask.blocks.length; j++) {
            if (this.taskByCategory.blocks[i].name == this.editTask.blocks[j].name) {
              for (var k = 0; k < this.categoryByPlots[i].plots.length; k++) {
                for (var l = 0; l < this.editTask.blocks[j].plots.length; l++) {
                  if (this.taskByCategory.blocks[i].plots[k].name == this.editTask.blocks[j].plots[l].name) {
                    if (this.editTask.blocks[j].plots[l].source) {
                      this.taskByCategory.blocks[i].plots[k]['source'] = this.editTask.blocks[j].plots[l].source;
                    }
                  }
                }

              }
            }
          }

        }
      }

    }
    console.log(this.categoryByPlots);
    // this.getLaboursWithBlockResponseble();
    this.finalData = this.categoryByPlots;
    console.log(this.finalData);

    console.log(this.taskByCategory);
    this.isCreatedTask = false;
    // let that = this;
    // let assine: any;
    // let usersData = firebase.database().ref('users/');
    // usersData.once("value", function (Data) {
    //   let data1 = snapshotToArray(Data);
    //   if (data1.length > 0) {
    //     that.getLaboursData(data1);

    //   }
    // });

  }
  getLabourData(val) {
    console.log(val);
    let temp: any = [];
    let device_id = 'null';
    if (val.device_id) {
      device_id = val.device_id
    }
    this.assignee = {
      'name': val.name,  'phone': val.phone, 'profile_url': val.profile_url, 'role': val.role,
      'primary_language': val.primary_language, 'device_id': device_id,'key': val.key,
    }
    this.key=val.key

    // alert(JSON.stringify(this.key))
    this.taskByCategory['assignee'] = this.assignee;
  }
  getLaboursWithBlockResponseble() {
    this.laboursData = this.usersData;
    let temp: any = [];
    for (var i = 0; i < this.laboursData.length; i++) {
      if (this.laboursData[i].block_responsible) {
        for (var j = 0; j < this.laboursData[i].block_responsible.length; j++) {
          if (this.laboursData[i].block_responsible[j] == this.selected_blocks.blockName) {
            temp.push(this.laboursData[i]);
          }
        }
      }
    }
    this.usersData = temp;
  }


  getAdditionalNotes(val) {
    console.log(val);
    this.additionalNotes = val;
    // if(val.length>0){
    // this.isCreatedTask = false;
    // }
    // if(val.length == 0){
    // this.isCreatedTask = true;
    // }
    this.taskByCategory['description'] = val;
  }
  getRecordedAudio(val) {
    console.log(val);
    this.audios = val;
    // alert(JSON.stringify(val));
    //alert(JSON.stringify(this.taskByCategory.media.audio));
    this.taskByCategory.media['audio'] = this.audios;
    // alert(JSON.stringify(this.taskByCategory));

  }
  getVideo(val) {
    let that = this;
    this.videos = val;

    // alert(JSON.stringify(this.videos));

    this.taskByCategory.media['video'] = this.videos;


    // alert(JSON.stringify(that.taskByCategory.media.video));
  }
  getCameraImage(val) {

    this.cameraImages = val;
    this.taskByCategory.media['camera'] = this.cameraImages;

    // alert(this.taskByCategory.media.camera);
  }
  getAttachments(val) {
    this.attachments.push(val);
    this.taskByCategory.media['attachments'] = []
    this.taskByCategory.media['attachments'] = this.attachments;
  }

  // getAdditionalNotes(val) {
  //   console.log(val);
  //   this.additionalNotes = val;
  //   this.taskByCategory['description'] = this.additionalNotes;

  // }
  // getRecordedAudio(val) {
  //   alert(val);
  //   this.audio = val.url;
  //   this.taskByCategory.media['audio'] = val;
  //   alert(this.taskByCategory.media.audio.url);
  // }
  // getVideo(val) {
  //   this.video = val;
  //   this.taskByCategory.media['video'] = val;
  //   alert(this.taskByCategory.media.video.url);
  // }
  // getCameraImage(val) {
  //   this.camera = val;
  //   this.taskByCategory.media['camera'] = val;
  //   alert(this.taskByCategory.media.camera.url);
  // }
  // getAttachments(val) {
  //   this.attachments = val;
  //   this.taskByCategory.media['attachments'] = val;
  //   alert(this.taskByCategory.media.attachments.url);
  // }

  async closeImage(val, idx) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure want to delete this ' + val + ' attachment?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.removeAttachment(val, idx);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  removeAttachment(val, idx) {
    let that = this;
    if (val == 'audio') {
      for (var i = 0; i < this.taskByCategory.media.audio.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByCategory.media.audio[i].path).delete()
          that.taskByCategory.media.audio.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id + "/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef.update(that.taskByCategory.media);
          }


        }
      }

    }
    if (val == 'video') {
      for (var i = 0; i < this.taskByCategory.media.video.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByCategory.media.video[i].path).delete()
          that.taskByCategory.media.video.splice(idx, 1);
          if (that.editTask) {
            var playersRef1 = firebase.database().ref(that.sg['userdata'].vendor_id + "/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef1.update(that.taskByCategory.media);
          }
        }
      }

    }
    if (val == 'camera') {
      for (var i = 0; i < this.taskByCategory.media.camera.length; i++) {
        if (i == idx) {
          firebase.storage().ref(that.taskByCategory.media.camera[i].path).delete()
          that.taskByCategory.media.camera.splice(idx, 1);
          if (that.editTask) {
            var playersRef2 = firebase.database().ref(that.sg['userdata'].vendor_id + "/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef2.update(that.taskByCategory.media);
          }
        }

      }
    }
    if (val == 'attachments') {
      for (var i = 0; i < this.taskByCategory.media.attachments.length; i++) {
        if (i == idx) {
          this.taskByCategory.media.attachments[i] = null;

          firebase.storage().ref(that.taskByCategory.media.attachments[i].path.name).delete()
          that.taskByCategory.media.attachments.splice(idx, 1);
          if (that.editTask) {
            var playersRef = firebase.database().ref(that.sg['userdata'].vendor_id + "/taskManager/task_table/" + that.editTask.key + '/media');
            playersRef.update(that.taskByCategory.media);
          }
        }
      }
      console.log(this.taskByCategory.media.attachments);
    }

  }
  async createTask(val) {
    debugger
    let dates;
    let days;
    let proper;
    let priority;

    let error_label;
    let select;
    let okay;
    if (this.sg['userdata'].primary_language === 'en') {
      dates = "Select Dates ";
      days = "Select Days";
      proper = "Select Proper Dates";
      priority = " Select Priority";
      error_label = "Warning";
      select = "Select "
      okay = "Okay"
    } else if (this.sg['userdata'].primary_language === 'te') {
      dates = "తేదీలు";
      days = "రోజులు";
      proper = "సరైన తేదీలు"
      priority = "ప్రాధాన్యత";

      error_label = "లోపం";
      select = "ఎంచుకోండి "
      okay = "సరే"
    } else if (this.sg['userdata'].primary_language === 'ta') {
      dates = "தேதிகள்";
      days = "நாட்கள்";
      proper = "சரியான தேதிகள்"
      priority = "முன்னுரிமை";

      error_label = "பிழை";
      select = "தேர்வு "
      okay = "சரி"
    } else if (this.sg['userdata'].primary_language === 'kn') {
      dates = "ದಿನಾಂಕಗಳು";
      days = "ದಿನಗಳು";
      priority = "ಆದ್ಯತೆ";

      proper = "ಸರಿಯಾದ ದಿನಾಂಕಗಳು"
      error_label = "ದೋಷ";
      select = "ಆಯ್ಕೆ ಮಾಡಿ "
      okay = "ಸರಿ"
    } else if (this.sg['userdata'].primary_language === 'hi') {
      dates = "खजूर";
      days = "दिन";
      priority = "प्राथमिकता";

      proper = "उचित तिथियां"
      error_label = "त्रुटि";
      select = "चुनते हैं "
      okay = "ठीक है"
    }
    let error = [];
    if (this.taskByCategory.frequency.name == 'Monthly') {
      if (this.selectedDates.length == 0) {
        error.push("Select Month");
      }
    } else if (this.taskByCategory.frequency.name == 'Weekly') {
      if (this.days.length == 0) {
        error.push("Select Month");
      }
    }
    if (new Date(this.event.startTime) > new Date(this.event.endTime)) {
      error.push("Select Proper Date");
    }
    if (!this.priority) {
      error.push(priority);
    }
    // if (this.taskByCategory['assignee'].name == this.allname) {
    //   debugger
    //   error.push("You alredy given task");
    // }
    if (new Date(this.event.startTime) == this.allrole) {
        if(this.taskByCategory['assignee'].name == this.allname){
          error.push("He/She is engaged in task");

        }
    }

    if (error.length == 0) {
      let message;
      let header;
      let ok;
      let cancel;
      if (this.sg['userdata'].primary_language === 'en') {
        header = "Confirm!"
        message = "Are you sure want to create task without description"
        ok = "Okay"
        cancel = "Cancel"
      } else if (this.sg['userdata'].primary_language === 'te') {
        header = "కన్ఫర్మ్!"
        message = "మీరు ఖచ్చితంగా వివరణ లేకుండా పనిని సృష్టించాలనుకుంటున్నారా"
        cancel = "కాదు"
        ok = "సరే"
      } else if (this.sg['userdata'].primary_language === 'ta') {
        header = "உறுதிசெய்!"
        message = "விளக்கம் இல்லாமல் பணியை உருவாக்க விரும்புகிறீர்களா?"
        cancel = "இல்லை"
        ok = "சரி"
      } else if (this.sg['userdata'].primary_language === 'kn') {
        header = "ಖಚಿತಪಡಿಸಿ!"
        message = "ವಿವರಣೆಯಿಲ್ಲದೆ ಕಾರ್ಯವನ್ನು ರಚಿಸಲು ನೀವು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ?"
        cancel = "ಇಲ್ಲ"
        ok = "ಸರಿ"
      } else if (this.sg['userdata'].primary_language === 'hi') {
        header = "पुष्टि करें!"
        message = "क्या आप वाकई वर्णन के बिना कार्य बनाना चाहते हैं"
        cancel = "नहीं"
        ok = "ठीक है"
      }
      let obj ={
        messase :"You got new Task",
        priority:this.priority,
        senderimage:this.sg["userdata"].profile_url,
        sendername: this.sg["userdata"].name,
        senderkey: this.sg["userdata"].key,
        status :'Acknowledge',
        
        notiification_description: this.taskByCategory['description']
        }
        firebase.database().ref(this.sg['userdata'].vendor_id+'/users/'+ this.key +'/notifications/').push(obj);
        
      if(!this.taskByCategory['description']){
        const alert = await this.alertController.create({
          header: header,
          message: message,
          buttons: [
            {
              text: cancel,
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('Confirm Cancel: blah');
              }
            }, {
              text: ok,
              handler: () => {
                this.addEvent(val);
              }
            }
          ]
        });
        await alert.present();
      }else{
        this.addEvent(val);
      }
     } else {
      const alert = await this.alertController.create({
        header: error_label,
        message: select+error.join(","),
        buttons: [okay]
      });
      await alert.present();
     }
  }
  submitTask(val) {
    let that = this;

let obj ={
messase :"You got new Task",
priority:this.priority,
senderimage:this.sg["userdata"].profile_url,
sendername: this.sg["userdata"].name,
senderkey: this.sg["userdata"].key,
status :'Acknowledge',

notiification_description: this.taskByCategory['description']
}
firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + this.key +'/notifications/').push(obj);


    this.taskByCategory["priority"] = this.priority;
  
    console.log(this.taskByCategory);
    if (!this.editTask) {
      this.taskByCategory['created'] = {
        "name": this.sg["userdata"].name,"key": this.sg["userdata"].key, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, "date": this.date_in_ms, profile_url: this.sg["userdata"].profile_url
      };
      // this.taskByCategory[" start_date"] = new Date(this.event.startTime);

      // this.taskByCategory["end_date"] = new Date(this.event.endTime);
  
      this.taskByCategory['task_type'] = 'Categories';
      this.taskByCategory['multiple_labours'] = true;
      this.taskByCategory['bulk'] = true;
      if (val == 'leftButtonAction') {
        this.taskByCategory['status'] = { 'name': 'Acknowledge' };
      }
      if (val == 'rightButtonAction') {
        this.taskByCategory['status'] = { 'name': 'Draft' };
      }
      var connectedRef = firebase.database().ref(".info/connected");
      connectedRef.on("value", function (snap) {
        if (snap.val() === true) {
          that.createTaskInOnline();

        } else {
          that.createTaskInOffLine();
        }
      });
    }
    if (this.editTask) {
      let that = this;
      this.taskByCategory['updated'] = {
        "name": this.sg["userdata"].name,
        "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role,
        "date": this.date_in_ms
      };
      this.taskByCategory['multiple_labours'] = true;
      this.taskByCategory['task_type'] = this.editTask.task_type;

      this.taskByCategory['status'] = this.editTask.status;

      this.taskByCategory['created'] = this.editTask.created;
      this.taskByCategory['assignee'] = this.editTask.assignee;
      var connectedRefEdit = firebase.database().ref(".info/connected");
      connectedRefEdit.on("value", function (snap) {
        if (snap.val() === true) {
          that.updateTaskInOnline();

        } else {
          that.createTaskInOffLine();
        }
      });
    }
  }

  createTaskInOffLine() {
    this.count1 = this.count1 + 1;
    if (this.count <= 1) {
      this.location.back();
      // this.navCtrl.navigateBack('/task-list');
    }
  }
  async presentToast() {
    if (this.taskByCategory.status.name == 'Draft') {
      const toast = await this.toastController.create({
        message: this.saved_draft_label,
        duration: 2000
      });
      toast.present();
    } else if (this.taskByCategory.status.name != 'Draft') {
      const toast = await this.toastController.create({
        message: this.task_created_label,
        duration: 2000
      });
      toast.present();
    }

  }
  async presentSuccessToast() {
    const toast = await this.toastController.create({
      message: this.success_label,
      duration: 2000
    });
    toast.present();
  }

  createTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let newData = this.taskManager_Task_table.push();
      newData.set(this.taskByCategory);
      let hours = new Date().getHours();
      if (this.taskByCategory['start_date'] == this.date_in_ms && hours >= 6) {
        this.other.splitMultipleTasks(this.sg['userdata'].vendor_id, this.taskByCategory, this.date_in_ms);
      }
      this.categoriesData = [];
      /* getting taskTable data */
      let totalTask_tableData: any;
      let task_tableLength: any;
      this.presentSuccessToast();
      // this.other.createSingleTasksFromMultiple();
      this.location.back();
      // let usersRef = firebase.database().ref('TaskManager/Bulk_Tasks');
      // usersRef.once("value", function (Data) {
      //   totalTask_tableData = snapshotToArray(Data);
      //   console.log(totalTask_tableData);
      //   //console.log(data[data.length-1].key);
      //   task_tableLength = totalTask_tableData.length - 1;
      //   console.log(task_tableLength);
      //   if (totalTask_tableData) {
      //     that.Task_logTable(totalTask_tableData);
      //   }

      // });
    }
  }
  updateTaskInOnline() {
    this.count = this.count + 1;
    if (this.count <= 1) {
      let that = this;
      let newData = firebase.database().ref(this.sg['userdata'].vendor_id + '/taskManager/task_table/' + this.editTask.key);
      newData.set(this.taskByCategory);
      this.presentSuccessToast();
      this.location.back();
      this.categoriesData = [];
      /* getting taskTable data */

      if (newData) {
        // that.Task_logTable(this.editTask);
      }
    }
  }
  getLaboursData(totalUserData) {
    let device_id = 'null';
    if (totalUserData.length > 0) {

      let assine: any;
      if (this.taskByCategory.blocks.length > 0) {
        let firstBlock = this.taskByCategory.blocks[0];
        let totalLaboursData: any = [];
        for (var i = 0; i < totalUserData.length; i++) {
          if (totalUserData[i].role == 'employee') {
            totalLaboursData.push(totalUserData[i]);
          }
        }
        for (var j = 0; j < totalLaboursData.length; j++) {
          if (totalLaboursData[j].block_responsible) {
            for (var k = 0; k < totalLaboursData[j].block_responsible.length; k++) {
              if (totalLaboursData[j].block_responsible[k] == firstBlock.name) {
                assine = totalLaboursData[j];
              }
            }
          }
        }
      }
      console.log(this.totalLaboursData)

      console.log(assine);
      if (assine.deviceid) {
        device_id = assine.deviceid;
      }
      this.taskByCategory['assignee'] = {
        'name': assine.name, 'phone': assine.phone, 'role': assine.role, 'farmeasy_id': assine.farmeasy_id,
        'profile_url': assine.profile_url, 'primary_language': assine.primary_language, 'device_id': device_id
      };

    }
    console.log(this.taskByCategory);

  }
  async Task_logTable(totalTask_tableData) {
    //   /* pushing data to Task_logTable */ 
    this.presentToast();
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: this.wait,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    if (!this.editTask) {
      console.log(totalTask_tableData[totalTask_tableData.length - 1]);
      this.task_logData['created'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      this.task_logData['task_id'] = totalTask_tableData[totalTask_tableData.length - 1].key;
      this.task_logData['status'] = { 'name': 'Acknowledge' };

      if (this.taskByCategory.assignee) {
        this.task_logData['assignee'] = this.taskByCategory.assignee;
      } else {
        this.task_logData['assignee'] = null;
      }
      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.categoriesData = [];
      loading.dismiss();
      this.presentSuccessToast();
      // let url="http://104.237.2.124:8091/farmeasy/split";
      //  this.http.get(url).pipe(map(res => res.json()))
      //  .subscribe(response => {
      //    console.log(response);
      // },
      // err => {
      // console.log(err);
      // });
      this.location.back();
    }
    if (this.editTask) {
      this.task_logData['updated'] = {
        "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, 'profile_url': this.sg["userdata"].profile_url, "date": this.date_in_ms
      };
      this.task_logData['task_id'] = totalTask_tableData.key;
      this.task_logData['status'] = totalTask_tableData.status;
      this.task_logData['assignee'] = totalTask_tableData.assignee;
      let logData = this.taskManager_Task_log.push();
      logData.set(this.task_logData);
      this.categoriesData = [];
      this.presentSuccessToast();
      loading.dismiss();
      //   let url="http://104.237.2.124:8091/farmeasy/split";
      //   this.http.get(url).pipe(map(res => res.json()))
      //   .subscribe(response => {
      //     console.log(response);
      //  },
      //  err => {
      //     console.log(err);
      //  });
      this.location.back();
    }
  }
  
  resetEvent() {
    this.event = {
      // title: '',
      // desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }
 
  // Create the right event format and reload source
  addEvent(val) {
    // alert(JSON.stringify(val))
    let eventCopy = {
      created :  {
        "name": this.sg["userdata"].name,"key": this.sg["userdata"].key, "phone": this.sg["userdata"].phone,
        'role': this.sg["userdata"].role, "date": this.date_in_ms, profile_url: this.sg["userdata"].profile_url
      },
      description: this.taskByCategory['description'],
      assignee :this.assignee,
      category : this.selectedCategory,
      frequency: { 'name' : this.selectedFrequency },
      start_date:  new Date(this.event.startTime),
      end_date: new Date(this.event.endTime),
      priority : this.priority,
      task_type : 'Categories',
      status : { 'name': 'Acknowledge' },
      multiple_labours : true,
      bulk : false,
    }   
 
    this.http.post('https://ayur-central-task-manager-default-rtdb.firebaseio.com/wellnest/taskManager/task_table.json', eventCopy).subscribe(responseData => {
    console.log(responseData);
    this.presentSuccessToast();
    // this.location.back();
    this.router.navigateByUrl('/notifications');

    });
  }
   


 
// Time slot was clicked
onTimeSelected(ev) {
  let selected = new Date(ev.selectedTime);
  this.event.startTime = selected.toISOString();
  selected.setHours(selected.getHours() + 1);
  this.event.endTime = (selected.toISOString());
}
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};