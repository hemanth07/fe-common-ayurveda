import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {ComponentsModule} from '../../components/components.module';
import { IonicModule } from '@ionic/angular';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { CreateTaskCategoryPage } from './create-task-category.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { SelectPlotsBlockComponent } from '../../components/task-manger-components/select-plots-block/select-plots-block.component';
import {SelectSourcePopOverComponent} from '../../components/task-manger-components/select-source-pop-over/select-source-pop-over.component';
import {SelectDateComponent} from '../../components/task-manger-components/select-date/select-date.component'
import { from } from 'rxjs';
import { NgCalendarModule } from 'ionic2-calendar';
import { UsercomponentsModule } from 'src/app/components/usercomponents/usercomponents.module';
import { ContactComponent } from 'src/app/components/contact/contact.component';
// import { ObservationComponentsModule } from '../components/observation-components/observation-components.module';
// import { UsercomponentsModule } from '../components/usercomponents/usercomponents.module';

const routes: Routes = [
  {
    path: '',
    component: CreateTaskCategoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,

    ObservationComponentsModule,
    FormsModule,UsercomponentsModule,
    IonicModule,NgCalendarModule,
    TaskManagerComponentsModule,
    RouterModule.forChild(routes)
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
  declarations: [CreateTaskCategoryPage],
  entryComponents: [SelectPlotsBlockComponent,SelectSourcePopOverComponent,SelectDateComponent,ContactComponent
    ],
})
export class CreateTaskCategoryPageModule {}
