import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskPlannerPage } from './task-planner.page';

describe('TaskPlannerPage', () => {
  let component: TaskPlannerPage;
  let fixture: ComponentFixture<TaskPlannerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskPlannerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskPlannerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
