import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { NavController } from '@ionic/angular';
import { SimpleGlobal} from 'ng2-simple-global';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.page.html',
  styleUrls: ['./task-list.page.scss'],
})
export class TaskListPage implements OnInit {
  taskManager_Task_log = firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_logTable');
  taskList:any=[];
  task_logData:any=[];
  count:any=0;
  headerData:any;
  back_path:any;
  defaultHref:any;
  back:any;

  constructor(public navCtrl: NavController,
              private sg: SimpleGlobal,
              private route: ActivatedRoute) { }

  ngOnInit() {
    let sub = this.route.params.subscribe(params => {
      this.back_path = JSON.parse(params["backPath"]);
      console.log(this.back_path);
    });
  let data
     if(this.sg['userdata'].primary_language === 'en'){
     data = 'Task List';
    } else if(this.sg['userdata'].primary_language === 'te'){
     data = 'పని జాబితా';
    } else if(this.sg['userdata'].primary_language === 'ta'){
     data = 'பணி பட்டியல்';
    } else if(this.sg['userdata'].primary_language === 'kn'){
     data = 'ಕಾರ್ಯ ಪಟ್ಟಿ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
     data = 'कार्य सूची';
    }   
    this.headerData = {
      'title': data, 'back': '/farm-dashboard', 'color': 'green',
      'button2': 'notifications', 'button1': 'add-circle-outline', 'notification_count': this.sg["notificationscount"],
      'button1_action':'/tm-mainmenu','button2_action':'/notifications'
    };
  }
  ionViewDidEnter() {
  
    this.defaultHref = '/farm-dashboard';
   
   }
  ionViewWillEnter(){
    this.count=this.count+1;
    //if(this.count>1){
      console.log(this.count);
   // } 
  }
   
  backToCreateTask(){
    this.navCtrl.navigateRoot('/create-task-category');
  }
  updateStatus(val){
    console.log(val);
    var today_date = new Date();
    var date_in_ms = today_date.getTime();
    firebase.database().ref(this.sg['userdata'].vendor_id+"/taskManager/task_table/"+val.key)
    .update({'status': val.status,'updated_date':date_in_ms});

    this.task_logData['updated'] = { "name": this.sg["userdata"].name, "phone": this.sg["userdata"].phone, 
    'role': this.sg["userdata"].role,'profile_url': this.sg["userdata"].profile_url,'date': date_in_ms};
    this.task_logData['task_id'] = val.key;
    this.task_logData['status'] = val.status;
   
    let logData = this.taskManager_Task_log.push();
    logData.set(this.task_logData);
  }
  nofication_count(){
    let data={"page_status":JSON.stringify("tm-mainmenu")}
    this.navCtrl.navigateForward(['/notifications',data]);
  }

  gotoForward(){
    this.navCtrl.navigateForward('/tm-mainmenu');
  }

}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
