import { Pipe, PipeTransform } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {
constructor(private http: Http){

}
  transform(value: any, args?: any): any {
    if(args != 'en'){
      let temp = 'q=' + value + '&';
      const url = 'https://translation.googleapis.com/language/translate/v2?' + temp
  + 'key=AIzaSyBLtAUorsS2O4yBOvhge00bkffAiUl8pag&target=' + args + '&format=text&source=en&model=base';
return new Promise(resolve => {
  this.http.get(url).pipe(map(res => res.json()))
    .subscribe(response => {
       console.log(response['data'].translations[0].translatedText);
      resolve(response['data'].translations[0].translatedText);
      // return response['data'].translations[0].translatedText;
    }, err => {
      //  console.log('something went wrong');
    });
});
    } else {
      return new Promise(resolve =>{
        resolve(value);
      });
    }

  }

}
