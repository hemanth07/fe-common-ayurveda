import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { CalendarComponent } from 'ionic2-calendar';
import { SimpleGlobal } from 'ng2-simple-global';
import { FirebaseServiceService } from '../firebase-service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-company-calendar',
  templateUrl: './company-calendar.page.html',
  styleUrls: ['./company-calendar.page.scss'],
})
export class CompanyCalendarPage implements OnInit {
  headerData: any;
  event = {
    title: '',
    desc: '',
    creator:'',
    startTime: '',
    endTime: '',
    priorty:'',
    key:'',
    allDay: false
  };

  minDate = new Date().toISOString();
  allEvents: any = []
  // eventSource = [];
  viewTitle;
  eventSource;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  @ViewChild(CalendarComponent) myCal: CalendarComponent;
  addDiseasesFlag: boolean=false;
  header: any;
  description: any;
  addDiseasesFlag1: boolean = true;
  start_time: string;
  end_time: any;
  feedbackForm : any = FormGroup;
  userdata: any;
  creator: any;
  priorty: any;
  key: any;
  // acceptTerms
  constructor(public service: FirebaseServiceService,
    private alertCtrl: AlertController,
    private sg: SimpleGlobal,
    public form: FormBuilder,
    public location:Location,
    public toastController: ToastController,
    private http: HttpClient, @Inject(LOCALE_ID) private locale: string) {
      this.feedbackForm = form.group({
        acceptTerms: [false, Validators.requiredTrue],
        noted: [false, Validators.requiredTrue],

        remarks : ['', Validators.required] }); 
  }
  ngOnInit() {
    // alert(JSON.stringify(this.sg['userdata'])) 

    this.service.getDataFromFirebase(this.sg['userdata'].vendor_id+'/taskManager/task_table').then(data => {
      this.allEvents = data;
      // alert(JSON.stringify(this.allEvents))
      // alert(JSON.stringify(this.allEvents))

      this.allEvents = this.allEvents.filter(name =>  name.assignee.name == this.sg['userdata'].name);
// 
      // console.log(this.allEvents)
      this.eventSource = this.createRandomEvents();
    })
    this.createHeader();

    this.resetEvent();
  }
  createHeader() { 
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Employee Calendar';
    } else if (this.sg['userdata'].primary_language === 'te') {
      title = 'నా జీవన వివరణ';
    } else if (this.sg['userdata'].primary_language === 'ta') {
      title = 'என் சுயவிவரம்';
    } else if (this.sg['userdata'].primary_language === 'kn') {
      title = 'ಸ್ವ ಭೂಮಿಕೆ';
    } else if (this.sg['userdata'].primary_language === 'hi') {
      title = 'मेरी प्रोफाइल';
    }
    this.headerData = {
      color: 'green',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard',
      button2: 'notifications',
      button2_action: '/notifications',
      notification_count: this.sg['notificationscount'],
    };
  }
  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      priorty:'',
      key:'',
      creator :'',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }
  // Create the right event format and reload source
  addEvent() {
    debugger
    let eventCopy = {
      title: this.event.title,
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay,
      desc: this.event.desc
    }
    console.log(eventCopy)
    if (eventCopy.allDay) {
      let start = eventCopy.startTime;
      let end = eventCopy.endTime;
      eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
      eventCopy.endTime = new Date(Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), end.getUTCDate() + 1));
    }
    console.log(eventCopy)
    debugger
    this.http.post('https://ayurveda-one-dev-default-rtdb.firebaseio.com/posts.json', eventCopy).subscribe(responseData => {
      console.log(responseData);
      this.allEvents.push(eventCopy);
      this.loadEvents();
      this.resetEvent();
    });

  }
  loadEvents() {
    this.eventSource = this.createRandomEvents();
  }
  createRandomEvents() {

    var events = [];
    if (this.allEvents && this.allEvents.length > 0) {
      debugger

      var startTime;
      var endTime;
      var title;
      var allday;
      var desc;
      var creator
      var priorty
var key
      for (var i = 0; i < this.allEvents.length; i++) {
        var date = new Date(this.allEvents[i].start_date);
        var indiaTime = new Date(date).toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
        startTime = new Date(indiaTime);
        var date1 = new Date(this.allEvents[i].end_date);
        var indiaTime1 = new Date(date1).toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
        endTime = new Date(indiaTime1);
        title = this.allEvents[i].assignee.name;
        desc = this.allEvents[i].description;
        allday = this.allEvents[i].bulk;
        creator = this.allEvents[i].created.name +"  "+this.allEvents[i].created.role;
        priorty = this.allEvents[i].priority;
        key= this.allEvents[i].created.key
        let obj = {
          'title': title,
          'startTime': startTime,
          'endTime': endTime,
          'allDay': allday,
          'desc': desc,
          'creator': creator,
          'priorty': priorty,
          'key':key

        }
        events.push(obj);
      }
    }
    return events;
  }
  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }

  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }

  // Change between month/week/day
  changeMode(mode) {
    this.calendar.mode = mode;
  }

  // Focus today
  today() {
    this.calendar.currentDate = new Date();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
  previewOff(){
    this.addDiseasesFlag=false;
this.addDiseasesFlag1 = true
  }
  // Calendar event was clicked
  async onEventSelected(event) {

    this.addDiseasesFlag=true;
    this.addDiseasesFlag1 = false
    this.header=event.title; 
    this.description = event.desc
    this.creator = event.creator
    this.priorty = event.priorty
    this.key = event.key
    let start = formatDate(event.startTime, 'medium', this.locale);
    let end = formatDate(event.endTime, 'medium', this.locale);
    this.start_time=start;
    this.end_time=end;

    // Use Angular date pipe for conversion
    // let start = formatDate(event.startTime, 'medium', this.locale);
    // let end = formatDate(event.endTime, 'medium', this.locale);

    // const alert = await this.alertCtrl.create({
    //   header: event.title,
    //   subHeader: event.desc,
    //   message: 'From: ' + start + '<br><br>To: ' + end,
    //   buttons: ['OK']
    // });
    // alert.present();
  }

  // Time slot was clicked
  onTimeSelected(ev) {
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());
  }

  submit(){ 
    if(this.feedbackForm.valid){
      let data = this.feedbackForm.value
      data['id'] = this.sg['userdata'].farmeasy_id;
      data['name'] = this.sg['userdata'].name;
      data['phone'] = this.sg['userdata'].phone;
      console.log(data);
      firebase.database().ref(this.sg['userdata'].vendor_id+'/feedback').push(data);
      this.location.back();
      this.presentToast();
    }    
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Feedback sent',
      duration: 2000
    });
    toast.present();
  }
  async submitRead(){ 
    if(this.feedbackForm.valid){   
    let eventCopy = {
      data : this.feedbackForm.value,
      senderimage:this.sg['userdata'].profile_url ,
      notification_status: "Mark as Done",
      notiification_description:  this.description,
      senderkey:this.sg['userdata'].key,
      sendername: this.sg['userdata'].name,
      phone: this.sg['userdata'].phone,
      status:  "Acknowledge",   
      Priority:this.priorty,  
      enddate:this.end_time,
      createdat:this.start_time,
     read:true,
    }
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + this.key +'/notifications/').push(eventCopy);
    this.location.back();
  }
    const toast = await this.toastController.create({
      message: 'Message was Readead',
      duration: 2000
    });
    toast.present();        
  }
  async submitNoted(){    
    let eventCopy = {
      id: this.sg['userdata'].farmeasy_id,
      name: this.sg['userdata'].name,
      phone: this.sg['userdata'].phone,
      message: "Message was Noted",     
    }
    // firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table/' + this.sg["userdata"].key).update(eventCopy);
    this.location.back();
    const toast = await this.toastController.create({
      message: 'Message was Noted',
      duration: 2000
    });
    toast.present();        
  }
}

export const snapshotToArray = snapshot => {
  const returnArr = [];

  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};