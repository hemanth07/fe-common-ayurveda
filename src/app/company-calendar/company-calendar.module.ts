import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CompanyCalendarPage } from './company-calendar.page';
import { NgCalendarModule  } from 'ionic2-calendar';
// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
// import { Routes, RouterModule } from '@angular/router';
import { UsercomponentsModule } from '../components/usercomponents/usercomponents.module';

// import { IonicModule } from '@ionic/angular';
import { ObservationComponentsModule } from '../components/observation-components/observation-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: CompanyCalendarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    UsercomponentsModule,    
    IonicModule,
    NgCalendarModule,ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CompanyCalendarPage]
})
export class CompanyCalendarPageModule {}
