import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCalendarPage } from './company-calendar.page';

describe('CompanyCalendarPage', () => {
  let component: CompanyCalendarPage;
  let fixture: ComponentFixture<CompanyCalendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCalendarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCalendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
