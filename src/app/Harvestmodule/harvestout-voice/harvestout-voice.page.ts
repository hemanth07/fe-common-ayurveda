import { FarmeasyTranslate } from './../../translate.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as firebase from 'firebase';
import { NavController, PopoverController } from '@ionic/angular';
import { SimpleGlobal } from 'ng2-simple-global';
import { PrintPopupComponent } from '../../components/harvestcomponents/print-popup/print-popup.component';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { ActivatedRoute } from '@angular/router';

// declare var cordova:any;

@Component({
  selector: 'app-harvestout-voice',
  templateUrl: './harvestout-voice.page.html',
  styleUrls: ['./harvestout-voice.page.scss'],
})
export class HarvestoutVoicePage implements OnInit {
  date:any;n
  harvest_outdata:any={};
  total:any;
  finaltotol:any;
  total_in_words:any;
  countinwords:any;
  labels: any = [];
  amount:number = 0;
  headerData: any;
  @ViewChild('invoice') invoice;
  constructor( public navCtrl: NavController,
    private popoverController: PopoverController,
    private sg: SimpleGlobal,
    private route: ActivatedRoute,
    private translate: FarmeasyTranslate,
    private en: EnglishService,
    private hi: HindiService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService) {
    this.sg['select_harvesttype'] = "Harvest Out";
  let monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    this.date=new Date().getFullYear()+"-"+monthNames[(new Date().getMonth()+1)]+"-"+new Date().getDate();
    this.initilisedata();
    this.translation();
}

initilisedata() {
  this.route.params.subscribe(params => {
      this.harvest_outdata = JSON.parse( params['invoice']);
      console.log(this.harvest_outdata);
       this.calaculateRate(this.harvest_outdata.record);
    });
}
translation(){
  if(this.sg['userdata'].primary_language === 'en'){
    this.labels  = this.en.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'te'){
    this.labels  = this.te.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'ta'){
    this.labels  = this.ta.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'kn'){
    this.labels  = this.kn.getHarvestOutInvoice();
  } else if(this.sg['userdata'].primary_language === 'hi'){
    this.labels  = this.hi.getHarvestOutInvoice();
  } else {
     this.labels  = this.en.getHarvestOutInvoice();
  }
  this.headerData = {
        color: 'pink',
        title: this.labels[0],
        button1: 'notifications',
        button1_action: '/notifications',
        notification_count: this.sg['notificationscount'],
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
}
calaculateRate(data){
  for(let record of data){
    this.amount += record.Rate;
  }
  
}
backto_dashboard() {
  this.navCtrl.navigateRoot('/harvestin');
}

    async Print() {
      let data = this.invoice.nativeElement.innerHTML;
      console.log(this.invoice.nativeElement.innerHTML);
      // document.addEventListener('deviceready', () => {
      //   // console.log('DEVICE READY FIRED AFTER', (Date.now() - before), 'ms');

      //   cordova.plugins.pdf.htmlToPDF({
      //           data: data,
      //           documentSize: "A4",
      //           landscape: "portrait",
      //           type: "base64"
      //       },
      //       (sucess) => alert(sucess),
      //       (error) => alert(JSON.stringify(error)));
      // });
      
      const popover = await this.popoverController.create({
          component: PrintPopupComponent,
          cssClass: "harvestvoice-popup",
      });
      // popover.onDidDismiss().then((data) => {
      //     console.log(data);
        
      // });
      await popover.present();

  }

    toWords(s) {

      let  th = ['', ' thousand', ' million', ' billion', ' trillion', ' quadrillion', ' quintillion'];
      let dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
      let tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
      let tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

      s = s.toString();
      s = s.replace(/[\, ]/g, '');
      if (s != parseFloat(s)) return 'not a number';
      var x = s.indexOf('.');
      if (x == -1) x = s.length;
      if (x > 15) return 'too big';
      var n = s.split('');
      this.countinwords = '';
      var sk = 0;
      for (var i = 0; i < x; i++) {
          if ((x - i) % 3 == 2) {
              if (n[i] == '1') {
                this.countinwords  += tn[Number(n[i + 1])] + ' ';
                  i++;
                  sk = 1;
              } else if (n[i] != 0) {
                this.countinwords  += tw[n[i] - 2] + ' ';
                  sk = 1;
              }
          } else if (n[i] != 0) {
            this.countinwords  += dg[n[i]] + ' ';
              if ((x - i) % 3 == 0)  this.countinwords  += 'hundred ';
              sk = 1;
          }
          if ((x - i) % 3 == 1) {
              if (sk)  this.countinwords  += th[(x - i - 1) / 3] + ' ';
              sk = 0;
          }
      }
      // if (x != s.length) {
      //     var y = s.length;
      //     this.countinwords += 'point ';
      //     for (var i = x + 1; i < y; i++) this.countinwords += dg[n[i]] + ' ';
      // }
      return  this.countinwords.replace(/\s+/g, ' ');
  }

  ngOnInit() {
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};

