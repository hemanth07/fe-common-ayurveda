import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
import { PrintPopupComponent } from '../../components/harvestcomponents/print-popup/print-popup.component';
import { IonicModule } from '@ionic/angular';
import { HarvestoutVoicePage } from './harvestout-voice.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: HarvestoutVoicePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,HarvestcomponentsModule,
    ObservationComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HarvestoutVoicePage],
  entryComponents: [PrintPopupComponent
  ],
})
export class HarvestoutVoicePageModule {}
