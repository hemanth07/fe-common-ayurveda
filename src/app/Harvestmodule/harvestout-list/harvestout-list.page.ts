import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { AlertController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-harvestout-list',
  templateUrl: './harvestout-list.page.html',
  styleUrls: ['./harvestout-list.page.scss'],
})
export class HarvestoutListPage implements OnInit {
  headerData: any;
  HarvestList:any = [];
  constructor(private sg: SimpleGlobal,
  public alertController: AlertController,
    private navCtrl :NavController) { }

  ngOnInit() {
    this.createHeader();
    this.getHarvestoutList();
  }
   createHeader() {
     this.sg['select_harvesttype'] = "Harvest Out"
    // color: green,orange
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Harvest Out';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಹಾರ್ವೆಸ್ಟ್ ಔಟ್';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'அறுவடை';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'హార్వెస్ట్ అవుట్';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'हार्वेस्ट आउट';
    }
    this.headerData = {
        color: 'pink',
        title: title,
        button1: 'add-circle-outline',
        button1_action: '/harvestout',
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
  }
   dateConverter(date){
    let temp = date.split('-');
    return (temp[2]+"-"+temp[1]+"-"+temp[0]);
  }
  formSentance(item,customer){
    let sentence = [];
    for(let card of item){
      let cat = card.category;
      let sub = card.subcategory;
      let uom = card.UOM;
      let quant = card.Quantity;
      let rate = card.Rate
      if(uom == "Kg"){
        sentence.push(quant+uom+" "+sub);
      } else if(uom == "No"){
        sentence.push(quant+" "+sub);
      }else if(uom == "Ltr"){
        sentence.push(quant+uom+" "+sub);
      }
    }
    var c = sentence.join(",");
    var idx = c.lastIndexOf(",");
    let last;
    if(idx !== -1){
       last =  c.substr(0,idx)+' and '+c.substr(idx + 1);
    } else {
      last = c;
    }
    return ("Harvested out "+last+" for "+customer);
   }
 
  getHarvestoutList(){
      firebase.database().ref(this.sg['userdata'].vendor_id+"/harvest/harvest_out").on('value',resp =>{
        let temp = snapshotToArray(resp);
        this.HarvestList = temp;
        for(let i=0;i<temp.length;i++){
          this.HarvestList[i]['date'] = this.dateConverter(temp[i].Date_of_Transaction);
          this.HarvestList[i]['sentence'] = this.formSentance(temp[i].record,temp[i].customer_name);
        }
        this.HarvestList.reverse();
        console.log(this.HarvestList);
      });
  }
  printinvoice(invoice){
    const data = {invoice: JSON.stringify(invoice)};
      this.navCtrl.navigateForward(['harvestout-voice', data]);
  }
    async comformationPopUp(invoice){
      const alert = await this.alertController.create({
        header: 'Confirm!',
        message: 'Are you sure want to print Invoice',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: () => {
              this.printinvoice(invoice)
            }
          }
        ]
      });
  
      await alert.present();
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};