import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
@Component({
  selector: 'app-harvestin-list',
  templateUrl: './harvestin-list.page.html',
  styleUrls: ['./harvestin-list.page.scss'],
})
export class HarvestinListPage implements OnInit {
  headerData: any;
  HarvestList: any = []
  constructor(private sg: SimpleGlobal) { }

  ngOnInit() {
    this.createHeader();
    this.getHarvestinList();
  }
   createHeader() {
     this.sg['select_harvesttype'] = "Harvest In"
    // color: green,orange
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Harvest In at Stores';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಅಂಗಡಿಗಳಲ್ಲಿ ಕೊಯ್ಲು ಮಾಡಿ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'கடைகளில் அறுவடை';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'స్టోర్లలో హార్వెస్ట్';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'भंडार में फसल';
    }
    this.headerData = {
        color: 'pink',
        title: title, 
        button1: 'add-circle-outline',
        button1_action: '/harvestin',
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
  }
  getHarvestinList(){
      firebase.database().ref(this.sg['userdata'].vendor_id+"/harvest/harvest_in").on('value',resp =>{
        let temp = snapshotToArray(resp);
        this.HarvestList = temp;
        for(let i=0;i<temp.length;i++){
          this.HarvestList[i]['date'] = this.dateConverter(temp[i].Date_of_Receiving);
          this.HarvestList[i]['sentence'] = this.formSentance(temp[i].record,this.createDateString(temp[i].Date_of_Receiving));
        }
        this.HarvestList.reverse();
        console.log(this.HarvestList);
      });
  }
    createDateString(full_date) {
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    let days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ];
    let fulldate = new Date(full_date);
    let day = fulldate.getDay();
    let month = fulldate.getMonth();
    let year = fulldate.getFullYear();
    let date = fulldate.getDate();
    let date_suf = this.ordinal_suffix_of(date);
    return (date_suf + " " + months[month] + " " + year);
    // console.log(this.date);
    // console.log(fulldate,days[day],months[month],year,date,date_suf);
  }
  ordinal_suffix_of(i) {
    let j = i % 10;
    let k = i % 100;
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  }
  dateConverter(date){
    let temp = date.split('-');
    return (temp[2]+"-"+temp[1]+"-"+temp[0]);
  }
formSentance(item,date){
    let sentence = [];
    for(let card of item){
      let cat = card.category;
      let sub = card.subcategory;
      let uom = card.UOM;
      let quant = card.Quantity;
      if(uom == "Kg"){
        sentence.push(quant+uom+" "+sub);
      } else if(uom == "No"){
        sentence.push(quant+" "+sub);
      }else if(uom == "Ltr"){
        sentence.push(quant+uom+" "+sub);
      }
    }
    var c = sentence.join(",");
    var idx = c.lastIndexOf(",");
    let last;
    if(idx !== -1){
       last =  c.substr(0,idx)+' and '+c.substr(idx + 1);
    } else {
      last = c;
    }
    return ("Harvested "+last+" on "+date);
   }

}


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};