import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';

@Component({
  selector: 'app-harvest',
  templateUrl: './harvest.page.html',
  styleUrls: ['./harvest.page.scss'],
 })
export class HarvestPage implements OnInit {
  Harvest_data: any = [];
  id: any;
  title :string;
  my_lang: string;
  headerData: any;
  constructor(private router: Router,
    public navCtrl: NavController, private route: ActivatedRoute,
    public sg: SimpleGlobal, private translate:FarmeasyTranslate,
    private en: EnglishService,
    private hi: HindiService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService
   ) {
  }

  ngOnInit() {
    this.translation();
   // console.log(this.router);
    this.route.params.subscribe(params => {
      this.id = params;
   //   console.log(this.id);
    });
  }

  selected_harvest(title,title1, path) {
    this.sg['select_harvesttype'] = title1;
    this.sg['select_harvesttype_title'] = title;
    this.navCtrl.navigateForward(path);
  }

  notifications() {
    let data = { "page_status": JSON.stringify("harvest") }
    this.navCtrl.navigateForward(['/notifications', data]);
  }

  backto_dashboard() {
    this.navCtrl.navigateRoot('/farm-dashboard');
  }

  translation(){
     if(this.sg['userdata'].primary_language === 'en'){
      this.Harvest_data  = this.en.getHarvestMenu();
      this.title = 'Sales and Production';
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.Harvest_data  = this.te.getHarvestMenu();
      this.title = 'అమ్మకాలు మరియు ఉత్పత్తి';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.Harvest_data  = this.ta.getHarvestMenu();
      this.title = 'விற்பனை மற்றும் உற்பத்தி';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.Harvest_data  = this.kn.getHarvestMenu();
      this.title = 'ಮಾರಾಟ ಮತ್ತು ಉತ್ಪಾದನೆ';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.Harvest_data  = this.hi.getHarvestMenu();
      this.title = 'बिक्री और उत्पादन';
    } else {
       this.Harvest_data  = this.en.getHarvestMenu();
       this.title = 'Sales and Production';
    }
    
   
    this.headerData = {
      title: this.title,
      back: '/farm-dashboard',
      color: "pink",
      button1: 'home',
      button1_action: '/farm-dashboard',
    };
  }

}