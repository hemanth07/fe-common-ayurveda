import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
//import { ComponentsModule } from '../components/components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { ReviewpopoverComponent } from '../../components/harvestcomponents/reviewpopover/reviewpopover.component';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

import { IonicModule } from '@ionic/angular';

import { HarvestinPage } from './harvestin.page';
const routes: Routes = [
  {
    path: '',
    component: HarvestinPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,HarvestcomponentsModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HarvestinPage],
  entryComponents: [ReviewpopoverComponent,CalendercomponentComponent
  ],
})
export class HarvestinPageModule {}
