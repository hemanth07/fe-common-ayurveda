import { HeaderComponent } from './../../components/observation-components/header/header.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, PopoverController, LoadingController,ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { ReviewpopoverComponent } from '../../components/harvestcomponents/reviewpopover/reviewpopover.component';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { KanadaService } from "./../../languages/kanada.service";
import { TeluguService } from "./../../languages/telugu.service";
import { TamilService } from "./../../languages/tamil.service";
import { EnglishService } from "./../../languages/english.service";
import { HindiService } from "./../../languages/hindi.service";
import { Location } from '@angular/common';
@Component({
  selector: 'app-harvestin',
  templateUrl: './harvestin.page.html',
  styleUrls: ['./harvestin.page.scss'],
})
export class HarvestinPage implements OnInit {
  harvesttype: any;
  my_lang: string;
  headerData: any;
  harvest_griddata = [];
  harvest_categories: any;
  categories: any;
  tempsubcats: any = [];
  user_role = [];
  har_shorttype: any;
  harvest_labourer: any;
  harvest_labourer_name:any = '- Select Labour -';
  harvest_category: any;
  harvest_subcategory: any;
  harvest_uom: any;
  harvest_quantity: any;
  harvest_out_bill: number;
  appName = "Ionic App";
  dataInicial: any;
  maxDate: string;
  labourer_name: any;
  har_category: any;
  har_uom: any;
  har_subcategory: any;
  har_quantity: any;
  harvest_recevingdate: any;
  error_message: boolean = true;
  Customer_Name: any;
  Contact_Person: any;
  product_rate: any;
  labels: any = [];
  Labourerslist: any;
  harvest_type: any;
  blocks:any = [];
  select_block:any;
  add_observation:boolean;
  remarks:any;
  view_mode: boolean = false;
  show_sub_cat:boolean = false;
  allrecords: any = [];
  isLaboursList:any;
  harvest_types = [
    { name: "Sales - Retail", short_name: "SR" },
    { name: " Sales - Wholesale", short_name: "SW" },
    { name: "Damage", short_name: "D" },
    { name: "Internal Consumption", short_name: "IC" }
  ];
  constructor(private router: Router, public sg: SimpleGlobal,
    public navCtrl: NavController, public alertController: AlertController,
    private popoverController: PopoverController,
    private translate: FarmeasyTranslate,
    public loadingController: LoadingController,
    private en: EnglishService,
    private hi: HindiService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService,
    public toastController: ToastController,
    private location:Location) {
    this.my_lang = this.sg['userdata'].primary_language ;

    this.headerData = {
      title: this.sg["select_harvesttype_title"],
      button1: 'home',
      button1_action: '/farm-dashboard',
      color: "pink"
    };

  }
  ngOnInit() {
    this.dataInicial = new Date().toJSON().split("T")[0];
    console.log(this.dataInicial);
    this.harvestdata();
    this.initilisedata();
    this.harvest_out_bill = 0;
    this.harvestoutdata();
    this.translation();
    this.getBlocks();
    }
  initilisedata() {
    firebase
      .database()
      .ref(this.sg['userdata'].vendor_id+"/users")
      .on("value", resp => {
        let that = this;
        const data = snapshotToArray(resp);
        if (data) {
          that.Labourerslist = data.filter(data => data.role === "labour");
        }
      });
  }
  translation() {
    if (this.sg["userdata"].primary_language === "en") {
      this.labels = this.en.getHarvestIn();
    } else if (this.sg["userdata"].primary_language === "te") {
      this.labels = this.te.getHarvestIn();
    } else if (this.sg["userdata"].primary_language === "ta") {
      this.labels = this.ta.getHarvestIn();
    } else if (this.sg["userdata"].primary_language === "kn") {
      this.labels = this.kn.getHarvestIn();
    } else if (this.sg["userdata"].primary_language === "hi") {
      this.labels = this.hi.getHarvestIn();
    } else {
      this.labels = this.en.getHarvestIn();
    }
  }
  showLaboursList(){
    this.isLaboursList=!this.isLaboursList;
  }
  selectName(name){
    this.isLaboursList=false;
  }
  getLabour(val) {
    let device_id='null';
    console.log(val);
    // console.log(val.detail.value);
    // let data = this.Labourerslist.filter(user =>
    //   user.name == val.detail.value);
    //   if(data[0].device_id){
    //     device_id=data[0].device_id;
    //   }
    this.harvest_labourer_name = val.name;
    let temp = { "name": val.name, "phone": val.phone, "profile_url": val.profile_url, "role": val.role,
    'device_id':device_id, 'primary_language':val.primary_language}
    console.log(temp)
    this.isLaboursList=false;
    this.harvest_labourer = temp;
  }
  harvestoutdata() {
    firebase
      .database()
      .ref(this.sg['userdata'].vendor_id+"/harvest/harvest_out")
      .on("value", resp => {
        let that = this;
        const data = snapshotToArray(resp);
        if (data && data.length > 0) {
          this.harvest_out_bill = data[data.length - 1].Bill_no;
          this.harvest_out_bill += 1;
          this.har_shorttype = data[data.length - 1].short_name;
        }
      });
  }
  getBlocks() {
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp => {
      const data = snapshotToArray(resp);
      this.blocks = data;
    });
 }
  ngOnChanges() {
  }
  Selecttype(type) {
    if (type) {
      this.har_shorttype = type.split(",")[1];
    }
  }
  SelectCategory(val) {
    console.log(val);
    if(val != ""){
      for (let i = 0; i < this.harvest_griddata.length; i++) {
        if (this.harvest_griddata[i]["subcat_val"].length == 0 && this.harvest_griddata[i]['uom_val'].length == 0  && this.harvest_griddata[i]['qty'].length == 0) {
          this.harvest_griddata.splice(i, 1);
        }
      }
      let harvest_griddata = [];
      this.show_sub_cat = true;
      this.categories = val;
      for (let j = 0; j < this.harvest_categories.length; j++) {
        if (
          this.categories == this.harvest_categories[j]["cat_name"]
        ) {
          harvest_griddata.push({
            categories: this.categories,
            subcategories: this.harvest_categories[j].subcat_names,
            uom: this.harvest_categories[j].Uom,
            subcat_val: "",
            uom_val: "",
            qty: ""
          });
        }
      }
      this.harvest_griddata.push(harvest_griddata[0]);
    }
  }

  validateValue(val, idx) {
    let harvest_griddata = [];
    if (val.subcat_val.length > 0 && val.uom_val.length > 0 && val.qty > 0) {
      if (!this.harvest_griddata[idx + 1]) {
        for (let j = 0; j < this.harvest_categories.length; j++) {
          if (
            this.categories == this.harvest_categories[j]["cat_name"] ) {
            harvest_griddata.push({
              categories: this.categories,
              subcategories: this.harvest_categories[j].subcat_names,
              uom: this.harvest_categories[j].Uom,
              subcat_val: "",
              uom_val: "",
              qty: ""
            });
            this.harvest_griddata.push(harvest_griddata[0]);
          }
        }
      }
    }
  }
  
  dateConverter(date){
    if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  }
  async addNewCatergory() {
    // this.show_sub_cat = false;
    for (let data of this.harvest_griddata) {
      if (
        data.subcat_val.length > 0 &&
        data.uom_val.length > 0 &&
        data.qty > 0
      ) {
        this.allrecords.push({
          category: data.categories,
          subcategory: data.subcat_val,
          UOM: data.uom_val,
          Quantity: data.qty
        });
      }
    }
    this.harvest_griddata= [];
    this.categories = null;
    this.harvest_category = null;
  }

  //------------------------------------ Calendar Popup -----------------------------------------//

  async open() {
    const popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popoverclass"
    });
    popover.onDidDismiss().then(data => {
     // console.log(data);
      this.dataInicial = data.data;
      console.log(this.dataInicial);
    });
    await popover.present();
  }

  //------------------------------------ Review Popup -----------------------------------------//

  async harvest_in_validation() {
    let validationerros = [];
    if(this.harvest_griddata.length>1){
    for (let i = 0; i < this.harvest_griddata.length; i++) {
        if (this.harvest_griddata[i]["subcat_val"].length == 0 && this.harvest_griddata[i]['uom_val'].length == 0  && this.harvest_griddata[i]['qty'].length == 0) {
          this.harvest_griddata.splice(i, 1);
        } else if(this.harvest_griddata[i]['uom_val'].length == 0 && this.harvest_griddata[i]['qty'].length == 0) {
          validationerros.push("UOM");
        } else if(this.harvest_griddata[i]['qty'].length == 0) {
          validationerros.push("Quantity");
        }
      }
    } else {
      validationerros.push("Sub Category");
    }
    if (this.harvest_griddata.length == 0) {
      validationerros.push("Fill Records");
    }
      if (!this.dataInicial) {
        validationerros.push("Date of Receiving");
      }
      if (!this.harvest_labourer) {
        validationerros.push("labourer name");
      }
    if (validationerros.length != 0) {
      const alert = await this.alertController.create({
        header: "Please enter mandatory fields",
        message: validationerros.join(","),
        cssClass: "custom-alertDanger",
        buttons: ["OK"]
      });
      await alert.present();
    } else {
       this.review();
    }
  }
  save(){
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let mon = new Date().getMonth()+1;
     let year = new Date(this.dataInicial).getFullYear().toString().substr(2, 2);
    if(mon == 12){
      mon = 1;
      year = (new Date(this.dataInicial).getFullYear()+1).toString().substr(2, 2);
    }
    let date = new Date(this.dataInicial).getDate();
    if(date < 10){
      date = parseInt('0'+date);
    }
    let month = months[mon];
    let key = date+"-"+month+"-"+year;
    let today_millisec = new Date(this.dataInicial).getTime();



    let harvest_data = {};
    let newwork_status = navigator.onLine;
    let record = [];
    for (let data of this.harvest_griddata) {
      if (
        data.subcat_val.length > 0 &&
        data.uom_val.length > 0 &&
        data.qty > 0
      ) {
        record.push({
          category: data.categories,
          subcategory: data.subcat_val,
          UOM: data.uom_val,
          Quantity: data.qty
        });
      }
    }
    if(this.allrecords.length>0){
      for(let data of this.allrecords){
        record.push(data);
      }
    }
      harvest_data = {
        Date_of_Receiving: this.dataInicial,
        labourer_name: this.harvest_labourer,
        block :this.select_block,
        remark: this.remarks? this.remarks : null,
        record: record,
        created_by:{name:this.sg['userdata'].name,
                   phone:this.sg['userdata'].phone,
                   primary_language:this.sg['userdata'].primary_language,
                   role:this.sg['userdata'].role,
                   profile_url:this.sg['userdata'].profile_url
                   }
      };
      console.log(harvest_data);
      for(let item of record ){
        firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/stockIn/'+key+'/'+item.category).push({subcategory:item.subcategory,UOM:item.UOM,Quantity:item.Quantity,date:today_millisec});
      }
      firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_in').push(harvest_data).then(async ()=>{
        const toast = await this.toastController.create({
          message: 'Success',
          duration: 2000
        });
        toast.present();
        this.location.back();
      });
      
  }
  createObservation(){
    location
  }
  review() {
    this.view_mode = true;
  }
  back(){
    this.view_mode = false;
  }
  harvestdata() {
      firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/harvest").on('value',resp =>{
        this.harvest_categories = snapshotToArray(resp);
        // console.log(JSON.stringify(this.harvest_categories));
      });
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
