import { HeaderComponent } from './../../components/observation-components/header/header.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, PopoverController, AlertController, LoadingController, } from '@ionic/angular';
import { Router } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';
import { FormsModule } from "@angular/forms";
import * as firebase from "firebase";
import { KanadaService } from "./../../languages/kanada.service";
import { TeluguService } from "./../../languages/telugu.service";
import { TamilService } from "./../../languages/tamil.service";
import { EnglishService } from "./../../languages/english.service";
import { HindiService } from "./../../languages/hindi.service";
import { ReviewpopoverComponent } from "./../../components/harvestcomponents/reviewpopover/reviewpopover.component";
import { CalendercomponentComponent } from "./../../components/harvestcomponents/calendercomponent/calendercomponent.component";
import { ToastController } from "@ionic/angular";
import { Location } from '@angular/common';

@Component({
  selector: 'app-harvestout',
  templateUrl: './harvestout.page.html',
  styleUrls: ['./harvestout.page.scss'],
})
export class HarvestoutPage implements OnInit {
  harvesttype: any;
  my_lang: string;
  headerData: any;
  harvest_griddata = [];
  harvest_categories: any;
  categories: any;
  tempsubcats: any = [];
  user_role = [];
  har_shorttype: any;
  harvest_labourer: any;
  harvest_category: any;
  harvest_subcategory: any;
  harvest_uom: any;
  harvest_quantity: any;
  harvest_out_bill: number;
  appName = "Ionic App";
  dataInicial: any;
  maxDate: string;
  labourer_name: any;
  har_category: any;
  har_uom: any;
  har_subcategory: any;
  har_quantity: any;
  harvest_recevingdate: any;
  error_message: boolean = true;
  Customer_Name: any;
  Contact_Person: any;
  product_rate: any;
  labels: any = [];
  Labourerslist: any;
  harvest_type: any;
  blocks:any = [];
  select_block:any;
  add_observation:boolean;
  remarks:any;
  allrecords: any = [];
  view_mode: boolean = false;
  harvest_types_stockOut = [
    { name: "Damage", short_name: "D" },
    { name: "Internal Consumption", short_name: "IC" }
  ];
  harvest_types_sales = [
    { name: "Sales - Retail", short_name: "SR" },
    { name: " Sales - Wholesale", short_name: "SW" },
  ];
  constructor(private router: Router,
    public sg: SimpleGlobal,
    public navCtrl: NavController,
    private popoverController: PopoverController,
    private translate: FarmeasyTranslate,
    private resolver: ComponentFactoryResolver,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private en: EnglishService,
    private hi: HindiService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService,
    public toastController: ToastController,
    public location:Location
    ) {
      this.createHeader();
   }
  createHeader() {
    
   // color: green,orange
   let title;
   if(this.sg['select_harvesttype'] == 'stock out'){
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Stock used and Damage';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಬಳಸಿದ ಸ್ಟಾಕ್ ಮತ್ತು ಹಾನಿ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'பயன்படுத்தப்பட்ட பங்கு மற்றும் சேதம்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'ఉపయోగించిన స్టాక్ మరియు నష్టం';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'स्टॉक का इस्तेमाल किया और नुकसान';
    }
   } else if(this.sg['select_harvesttype'] == 'sales') {
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Sales';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಮಾರಾಟ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'விற்பனை';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'అమ్మకాలు';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'बिक्री';
    }
   }
   this.headerData = {
       color: 'pink',
       title: title,
       button1: 'home',
       button1_action: '/farm-dashboard',
       };
 }
 ngOnInit() {
  this.dataInicial = new Date().toJSON().split("T")[0];
  // console.log(this.dataInicial);
  this.harvestdata();
  this.initilisedata();
  this.harvest_out_bill = 0;
  this.harvestoutdata();
  this.translation();
  this.getBlocks();
}

//------------------------------------users List-----------------------------------------//

initilisedata() {
  firebase
    .database()
    .ref(this.sg['userdata'].vendor_id+"/users")
    .on("value", resp => {
      let that = this;
      const data = snapshotToArray(resp);
     // console.log(data);
     // console.log(data.length);
      if (data) {
        that.Labourerslist = data.filter(data => data.role === "labour");
      //  console.log(that.Labourerslist);
      }
    });
}
translation() {
  if (this.sg["userdata"].primary_language === "en") {
    this.labels = this.en.getHarvestIn();
  } else if (this.sg["userdata"].primary_language === "te") {
    this.labels = this.te.getHarvestIn();
  } else if (this.sg["userdata"].primary_language === "ta") {
    this.labels = this.ta.getHarvestIn();
  } else if (this.sg["userdata"].primary_language === "kn") {
    this.labels = this.kn.getHarvestIn();
  } else if (this.sg["userdata"].primary_language === "hi") {
    this.labels = this.hi.getHarvestIn();
  } else {
    this.labels = this.en.getHarvestIn();
  }
}
dateConverter(date){
  if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
}
harvestoutdata() {
  firebase
    .database()
    .ref(this.sg['userdata'].vendor_id+"/harvest/harvest_out")
    .on("value", resp => {
      let that = this;
      const data = snapshotToArray(resp);
      if (data && data.length > 0) {
        this.harvest_out_bill = data[data.length - 1].Bill_no;
        this.harvest_out_bill += 1;
        this.har_shorttype = data[data.length - 1].short_name;
      }
    });
}
getBlocks() {
  firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp => {
    const data = snapshotToArray(resp);
    this.blocks = data;
  });
}
ngOnChanges() {
}
Selecttype(type) {
  if (type) {
    this.har_shorttype = type.split(",")[1];
  }
}

SelectCategory(val) {
  console.log(val);
  if(val != ""){
    for (let i = 0; i < this.harvest_griddata.length; i++) {
      if (this.harvest_griddata[i]["subcat_val"].length == 0 && this.harvest_griddata[i]['uom_val'].length == 0  && this.harvest_griddata[i]['qty'].length == 0 && this.harvest_griddata[i]['rate'].length == 0) {
        this.harvest_griddata.splice(i, 1);
      }
    }
    let harvest_griddata = [];
    this.categories = val;
    for (let j = 0; j < this.harvest_categories.length; j++) {
      if (
        this.categories == this.harvest_categories[j]["cat_name"]
      ) {
        harvest_griddata.push({
          categories: this.categories,
          subcategories: this.harvest_categories[j].subcat_names,
          uom: this.harvest_categories[j].Uom,
          subcat_val: "",
          uom_val: "",
          qty: "",
          rate: ""
        });
      }
    }
    this.harvest_griddata.push(harvest_griddata[0]);
  }
}

validateValue(val, idx) {
  let harvest_griddata = [];
  if (val.subcat_val.length > 0 && val.uom_val.length > 0 && val.qty > 0) {
    if (!this.harvest_griddata[idx + 1]) {
      for (let j = 0; j < this.harvest_categories.length; j++) {
         if (this.categories == this.harvest_categories[j]["cat_name"] && val.rate > 0 ) {
        //  console.log(this.harvest_categories.subcat_names);
          harvest_griddata.push({
            categories: this.categories,
            subcategories: this.harvest_categories[j].subcat_names,
            uom: this.harvest_categories[j].Uom,
            subcat_val: "",
            uom_val: "",
            qty: "",
            rate: ""
          });
          this.harvest_griddata.push(harvest_griddata[0]);
        }
      }
    }
  }
}
addNewCatergory() {
  for (let data of this.harvest_griddata) {
    if (
      data.subcat_val.length > 0 &&
      data.uom_val.length > 0 &&
      data.qty > 0
    ) {
      this.allrecords.push({
        category: data.categories,
        subcategory: data.subcat_val,
        UOM: data.uom_val,
        Quantity: data.qty,
        Rate: data.rate
      });
    }
  }
  this.harvest_griddata= [];
  this.categories = null;
  this.harvest_category = null;
}

//------------------------------------ Calendar Popup -----------------------------------------//

async open() {
  const popover = await this.popoverController.create({
    component: CalendercomponentComponent,
    cssClass: "popoverclass"
  });
  popover.onDidDismiss().then(data => {
   // console.log(data);
    this.dataInicial = data.data;
  });
  await popover.present();
}

//------------------------------------ Review Popup -----------------------------------------//

async harvest_in_validation() {
  let validationerros = [];

  if(this.harvest_griddata.length>1){
    for (let i = 0; i < this.harvest_griddata.length; i++) {
      if (this.harvest_griddata[i]["subcat_val"].length == 0 && this.harvest_griddata[i]['uom_val'].length == 0  && this.harvest_griddata[i]['qty'].length == 0 && this.harvest_griddata[i]['rate'].length == 0) {
        this.harvest_griddata.splice(i, 1);
      } else if(this.harvest_griddata[i]['uom_val'].length == 0  && this.harvest_griddata[i]['qty'].length == 0 && this.harvest_griddata[i]['rate'].length == 0) {
        validationerros.push("UOM,Rate,Quantity");
      } else if( this.harvest_griddata[i]['qty'].length == 0 && this.harvest_griddata[i]['rate'].length == 0){
        validationerros.push("Rate,Quantity");
      } else if(this.harvest_griddata[i]['rate'].length == 0){
        validationerros.push("Quantity");
      }
      }
    } else {
      validationerros.push("Sub Category");
    }





  if (this.harvest_griddata.length == 0) {
    validationerros.push("Fill Records");
  }

    if (!this.Customer_Name) {
      validationerros.push("Customer Name");
    }
    if (!this.Contact_Person) {
      validationerros.push("Contact Person");
    }
     if (!this.harvest_type) {
    validationerros.push("Harvest Type");
    }
  if (validationerros.length != 0) {
    const alert = await this.alertController.create({
      header: "Please enter mandatory fields",
      message: validationerros.join(","),
      cssClass: "custom-alertDanger",
      buttons: ["OK"]
    });
    await alert.present();
  } else {
    this.review();
  }
}
save(){

  let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  let mon = new Date().getMonth()+1;
   let year = new Date(this.dataInicial).getFullYear().toString().substr(2, 2);
  if(mon == 12){
    mon = 1;
    year = (new Date(this.dataInicial).getFullYear()+1).toString().substr(2, 2);
  }
  let date = new Date(this.dataInicial).getDate();
  if(date < 10){
    date = parseInt('0'+date);
  }
  let month = months[mon];
  let key = date+"-"+month+"-"+year;
  let today_millisec = new Date(this.dataInicial).getTime();


  let harvest_data = {};
  let newwork_status = navigator.onLine;
  let record = [];
  for (let data of this.harvest_griddata) {
    if (
      data.subcat_val.length > 0 &&
      data.uom_val.length > 0 &&
      data.qty > 0 &&
      data.rate > 0
    ) {
      record.push({
        category: data.categories,
        subcategory: data.subcat_val,
        UOM: data.uom_val,
        Quantity: data.qty,
        Rate: data.rate
      });
    }
  }
  if(this.allrecords.length>0){
    for(let data of this.allrecords){
      record.push(data);
    }
  }
  harvest_data = {
    payment_status:"Not Paid",
    Date_of_Transaction: this.dataInicial,
    Bill_no: this.harvest_out_bill,
    type: this.harvest_type.split(",")[0],
    short_name: this.harvest_type.split(",")[1],
    customer_name:this.Customer_Name,
    contact_person:this.Contact_Person,
    record: record,
    created_by:{name:this.sg['userdata'].name,
                 phone:this.sg['userdata'].phone,
                 primary_language:this.sg['userdata'].primary_language,
                 role:this.sg['userdata'].role,
                 profile_url:this.sg['userdata'].profile_url
                 }
  };
  for(let item of record ){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/stockOut/'+key+'/'+item.category).push({subcategory:item.subcategory,UOM:item.UOM,Quantity:item.Quantity,date:today_millisec});
  }
    console.log(harvest_data);
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out').push(harvest_data).then(async ()=>{
      const toast = await this.toastController.create({
        message: 'Success',
        duration: 2000
      });
      toast.present();
      this.location.back();
    });
    
}
review() {
  this.view_mode = true;
}
back(){
  this.view_mode = false;
}
harvestdata() {
    firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/harvest").on('value',resp =>{
      this.harvest_categories = snapshotToArray(resp);
    });
  }
}
export const snapshotToArray = snapshot => {
let returnArr = [];
snapshot.forEach(childSnapshot => {
  let item = childSnapshot.val();
  item.key = childSnapshot.key;
  returnArr.push(item);
});
return returnArr;
};
