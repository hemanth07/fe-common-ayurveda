import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HarvestoutPage } from './harvestout.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';

const routes: Routes = [
  {
    path: '',
    component: HarvestoutPage
  }
];

@NgModule({
  imports: [
    ObservationComponentsModule,
    TaskManagerComponentsModule,
    HarvestcomponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HarvestoutPage]
})
export class HarvestoutPageModule {}
