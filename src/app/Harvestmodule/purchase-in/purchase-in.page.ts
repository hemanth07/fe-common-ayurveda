import { Component, OnInit } from "@angular/core";
import { SimpleGlobal } from "ng2-simple-global";
import { Location } from '@angular/common';
import { FarmeasyTranslate } from './../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { HindiService } from './../../languages/hindi.service';
import { EnglishService } from './../../languages/english.service';
import { CalendercomponentComponent } from './../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { PopoverController,AlertController,ToastController } from '@ionic/angular';
import { ReviewpopoverComponent } from './../../components/harvestcomponents/reviewpopover/reviewpopover.component';
import * as firebase from 'firebase';

@Component({
  selector: "app-purchase-in",
  templateUrl: "./purchase-in.page.html",
  styleUrls: ["./purchase-in.page.scss"]
})
export class PurchaseInPage implements OnInit {
  headerData: any;
  labels: any = [];
  dataInTime: any;
  uomList: any = [];
  purchase_categories: any = [];
  selectedCategory: any;
  description: any;
  purchase_category: any=[];
  purchaseIn_description: any=[];
  purchase_quantity: any=[];
  purchase_uom: any=[];
  purchase_rate: any = [];
  rate:any;
  uom: any;
  quantity: any;
  subCategory:any=[{name:'',uom:'',quantity:''}];
  subCategories:any=[];
  bill_no:any;
  vendor_name:any;
  total:number;
  view_mode:boolean = false;
  constructor(public sg: SimpleGlobal,
    public location:Location,
    private popoverController: PopoverController,
    public alertController:AlertController,
    public toastController:ToastController,
    public translate: FarmeasyTranslate,
    private en: EnglishService,
    private ta: TamilService,
    private te: TeluguService,
    private hi: HindiService,
    private kn: KanadaService) {}

  ngOnInit() {
      // color: green,orange
      let title;

      if(this.sg["type"] == 'PurchasesIn'){
        if(this.sg['userdata'].primary_language === 'en'){
          title = 'Purchase In';
        } else if(this.sg['userdata'].primary_language === 'kn'){
          title = 'ರಲ್ಲಿ ಖರೀದಿಸಿ';
        } else if(this.sg['userdata'].primary_language === 'ta'){
          title = 'இல் வாங்கவும்';
        } else if(this.sg['userdata'].primary_language === 'te'){
          title = 'లో కొనండి';
        } else if(this.sg['userdata'].primary_language === 'hi'){
          title = 'में खरीद';
        }
      } else if(this.sg["type"] == 'PurchasesOut'){
        if(this.sg['userdata'].primary_language === 'en'){
          title = 'Purchase Out';
        } else if(this.sg['userdata'].primary_language === 'kn'){
          title = 'ಖರೀದಿಸಿ';
        } else if(this.sg['userdata'].primary_language === 'ta'){
          title = 'வாங்கவும்';
        } else if(this.sg['userdata'].primary_language === 'te'){
          title = 'కొనుగోలు చేయండి';
        } else if(this.sg['userdata'].primary_language === 'hi'){
          title = 'खरीद बाहर';
        }
      }



      
      this.headerData = {
          color: 'pink',
          title: title,
          button1: 'home',
          button1_action: '/farm-dashboard',
          };
          this.subCategories=this.subCategory;
          var current_date = new Date();
          var current_month:any;
          current_month= current_date.getMonth() + 1;
          if(current_month<10){
            current_month='0'+current_month;
          }
          this.dataInTime = current_date.getFullYear() + "-" + current_month + "-" + current_date.getDate()
          this.translation();
          this.harvestdata();
    }
    translation() {
      if (this.sg['userdata'].primary_language === 'en') {
        this.labels = this.en.getHarvestIn();
      } else if (this.sg['userdata'].primary_language === 'te') {
        this.labels = this.te.getHarvestIn();
      } else if (this.sg['userdata'].primary_language === 'ta') {
        this.labels = this.ta.getHarvestIn();
      } else if (this.sg['userdata'].primary_language === 'kn') {
        this.labels = this.kn.getHarvestIn();
      }else if (this.sg['userdata'].primary_language === 'hi') {
        this.labels = this.hi.getHarvestIn();
      } else {
        this.labels = this.en.getHarvestIn();
      }
     // console.log(this.labels);
    }
    async openDate() {
      const popover = await this.popoverController.create({
        component: CalendercomponentComponent,
        cssClass: "popoverclass",
      });
      popover.onDidDismiss().then((data) => {
      //  console.log(data);
        this.dataInTime = data.data;
      });
      await popover.present();
  
    }
    add_another_category(){
      this.subCategories.push({name:'',uom:'',quantity:'',rate:''})
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
    }
    async review(){
      let validationerros = [];
      for (let i = 0; i < this.subCategories.length; i++) {
        if (this.subCategories[i]["name"].length == 0) {
          this.subCategories.splice(i, 1);
        }
      }
      if(this.subCategories.length == 0){
        this.add_another_category();
      }
      if (this.subCategories.length == 0) {
        validationerros.push("Fill Records");
       }
        if (!this.bill_no) {
          validationerros.push("Bill No");
        }
        if (!this.vendor_name) {
          validationerros.push("Vendor Name");
        }
        if(!this.dataInTime){
          validationerros.push("Date");
        }
      if (validationerros.length != 0) {
        const alert = await this.alertController.create({
          header: "Please enter mandatory fields",
          message: validationerros.join(","),
          cssClass: "custom-alertDanger",
          buttons: ["OK"]
        });
        await alert.present();
      } else {
        this.view_mode = true;
      }
    }
    back(){
      this.view_mode = false;
    }
    addAnotherRow(idx){
      if(this.subCategories[idx].name.length>0 && this.subCategories[idx].uom.length>0 && this.subCategories[idx].quantity != 0  && this.subCategories[idx].rate != 0 ){
        {
          if(!this.subCategories[idx+1]){
          this.add_another_category();
          }
        }
      }
    }
    harvestdata() {
      firebase.database().ref(this.sg['userdata'].vendor_id+"/lovs/purchasein").on('value',resp => {
        this.purchase_categories = snapshotToArray(resp);
      });
      // this.purchase_categories
      //   = [{
      //     "cat_name": "Vegetables",
      //     "cat_id": 0,
  
      //   }, {
      //     "cat_name": "Fruits",
      //     "cat_id": 1,
  
  
      //   }, {
      //     "cat_name": "Crops",
      //     "cat_id": 2,
  
  
      //   }, {
      //     "cat_name": "Greens",
      //     "cat_id": 3,
  
      //   }]
      this.uomList = [{ name: 'Kg' }, { name: 'No' }, { name: 'Ltr' }]
    }
    SelectCategory(val) {
      this.selectedCategory = val;
      
    //  console.log(this.SelectCategory);
    }
    getDescription(val,idx) {
      this.description = val.detail.value;
      this.subCategories[idx].name=this.description;
      this.addAnotherRow(idx);
    }
    selectUom(val,idx) {
      this.uom = val;
      this.subCategories[idx].uom=val;
      this.addAnotherRow(idx);
  
    }
    getQuantity(val,idx) {
      this.quantity = val.detail.value;
      this.subCategories[idx].quantity= this.quantity;
      this.addAnotherRow(idx);
  
    }
    getRate(val,idx) {
      this.rate = val.detail.value;
      this.subCategories[idx].rate= this.rate;
      this.addAnotherRow(idx);
      this.TotalAmount();
    }
    TotalAmount(){
      let count = 0
      for(let item of this.purchase_rate){
          count = count + item;
      }
      this.total = count;
    }
    async save() {
      let harvest_data = {};
      harvest_data = {
        "Date_of_transaction": this.dataInTime,
        "category": this.selectedCategory,
        "subcategory": this.subCategories,
        bill_no:this.bill_no,
        total:this.total,
        vendor:this.vendor_name,
        created_at: new Date().getTime(),
        created_by:{name:this.sg['userdata'].name,
                     phone:this.sg['userdata'].phone,
                     primary_language:this.sg['userdata'].primary_language,
                     role:this.sg['userdata'].role,
                     profile_url:this.sg['userdata'].profile_url
                     }
        
      };
      if(this.sg["type"] == 'PurchasesIn'){
        firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/purchase_in').push(harvest_data).then(async ()=>{
          const toast = await this.toastController.create({
            message: 'Success',
            duration: 2000
          });
          toast.present();
          this.location.back();
        });
      }
      else if(this.sg["type"] == 'PurchasesOut'){
        firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/purchase_out').push(harvest_data).then(async ()=>{
          const toast = await this.toastController.create({
            message: 'Success',
            duration: 2000
          });
          toast.present();
          this.location.back();
        });
      }
    }
  }
  
  export const snapshotToArray = snapshot => {
    let returnArr = [];
    snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });
    return returnArr;
  };
  