import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PurchaseinListPage } from './purchasein-list.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';

const routes: Routes = [
  {
    path: '',
    component: PurchaseinListPage
  }
];

@NgModule({
  imports: [
    ObservationComponentsModule,
    TaskManagerComponentsModule,
    HarvestcomponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PurchaseinListPage]
})
export class PurchaseinListPageModule {}
