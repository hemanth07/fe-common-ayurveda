import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import { NavController, LoadingController, PopoverController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';


@Component({
  selector: 'app-purchasein-list',
  templateUrl: './purchasein-list.page.html',
  styleUrls: ['./purchasein-list.page.scss'],
})
export class PurchaseinListPage implements OnInit {
  headerData: any;
  fromOrTodate:any;
  toDate:any;
  fromDate:any;
  color:any = '#FF4081';
  PurchaseInList:any = []
  TempPurchaseInList:any = []
  constructor(
             private sg: SimpleGlobal,
              private popoverController:PopoverController,
              public navCtrl: NavController,
              public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.createHeader();
    this.getPurchaseInList();
  }
   createHeader() {
    // color: green,orange
    let title;
    if(this.sg["type"] == 'PurchasesIn'){
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Purchase In List';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಖರೀದಿ ಪಟ್ಟಿ';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'கொள்முதல் பட்டியல்';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'కొనుగోలు జాబితా';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'खरीद सूची';
      }
    } else if(this.sg["type"] == 'PurchasesOut'){
      if(this.sg['userdata'].primary_language === 'en'){
        title = 'Purchase Out List';
      } else if(this.sg['userdata'].primary_language === 'kn'){
        title = 'ಪಟ್ಟಿಯನ್ನು ಖರೀದಿಸಿ';
      } else if(this.sg['userdata'].primary_language === 'ta'){
        title = 'பட்டியல் வாங்கவும்';
      } else if(this.sg['userdata'].primary_language === 'te'){
        title = 'జాబితాను కొనుగోలు చేయండి';
      } else if(this.sg['userdata'].primary_language === 'hi'){
        title = 'खरीद सूची';
      }
    }
    
    this.headerData = {
        color: 'pink',
        title: title,
        button1: 'add-circle-outline',
        button1_action: '/purchase-in',
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
  }
 async getPurchaseInList(){
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    if(this.sg["type"] == 'PurchasesIn'){
      firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/purchase_in').on('value',resp=>{
        let temp = snapshotToArray(resp);
       // temp.map((item,i)=>temp[i].date = temp[i].date.split("-").reverse().join("-"));
        temp.map((item,i)=> { temp[i]['time'] = new Date(temp[i].Date_of_transaction).getTime()});
        // temp.map((item,i)=> { temp[i]['total'] = parseInt(temp[i]['total']).toLocaleString();});
        temp.reverse();
          this.PurchaseInList = temp;
          this.TempPurchaseInList = temp;
          console.log(this.PurchaseInList);
          loading.dismiss();
      });
    } else if(this.sg["type"] == 'PurchasesOut'){
      firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/purchase_out').on('value',resp=>{
        let temp = snapshotToArray(resp);
       // temp.map((item,i)=>temp[i].date = temp[i].date.split("-").reverse().join("-"));
        temp.map((item,i)=> { temp[i]['time'] = new Date(temp[i].Date_of_transaction).getTime()});
        // temp.map((item,i)=> { temp[i]['total'] = parseInt(temp[i]['total']).toLocaleString();});
        temp.reverse();
          this.PurchaseInList = temp;
          this.TempPurchaseInList = temp;
          console.log(this.PurchaseInList);
          loading.dismiss();
      });
    }
    
     
  }
  dateConverter(date){
     if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
  }
  async openDate(val) {
    this.fromOrTodate = val;
    let popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popover_class",
      componentProps: {'previous': true},
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getSelectedDate(detail.data);
    });
    await popover.present();
  }
  getSelectedDate(val) {
    console.log(this.fromOrTodate);
    if (this.fromOrTodate == 'toDate') {
      this.toDate = val;
      console.log(this.toDate);
    }
    if (this.fromOrTodate == 'fromDate') {
      this.fromDate = val;
      console.log(this.fromDate);
    }
  }

  searchData(val) {
   if(this.toDate && this.fromDate){
     let toDate = new Date(this.toDate).getTime();
     let fromDate = new Date(this.fromDate).getTime();
     this.PurchaseInList = this.TempPurchaseInList.filter((item)=> item.time>=fromDate && item.time<=toDate);
   }
 }
 commaSeparater(amount){
   if(amount>999){
  return amount.toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
  } else {
    return amount;
  }
}
  formSentance(item,cat){
    let sentence = [];
    for(let card of item){
      let name = card.name;
      let uom = card.uom;
      let quant = card.quantity;
      if(uom == "Kg"){
        sentence.push(quant+uom+" "+name);
      } else if(uom == "No"){
        sentence.push(quant+" "+name);
      }else if(uom == "Ltr"){
        sentence.push(quant+uom+" "+name);
      }
    }
    var c = sentence.join(",");
    var idx = c.lastIndexOf(",");
    let last;
    if(idx !== -1){
       last =  c.substr(0,idx)+' and '+c.substr(idx + 1);
    } else {
      last = c;
    }
    return ("Purchased "+last)
   }

}


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};