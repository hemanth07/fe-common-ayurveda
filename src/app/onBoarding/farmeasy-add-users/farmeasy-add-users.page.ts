import { Location } from '@angular/common';
import { EnglishService } from './../../languages/english.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { HindiService } from './../../languages/hindi.service';
import { Component, OnInit ,NgZone} from '@angular/core';
import { AlertController, LoadingController, NavController, PopoverController} from '@ionic/angular';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-farmeasy-add-users',
  templateUrl: './farmeasy-add-users.page.html',
  styleUrls: ['./farmeasy-add-users.page.scss'],
})
export class FarmeasyAddUsersPage implements OnInit {
  user: any = [];
  users: any = [];
  Data: any = [];
  headerData: any = [];
  footerData:any = [];
  name: string;
  phone: string;
  aadhar:string;
  password: string;
  language: string;
  role: string;
  blocks: any = [];
  blocksData: any = [];
  profile: string;
  id: string;
  bank: string;
  updateflag: boolean;
  languages_support: any = [];
  lang_code: string;
  known_lang: any = [];
  gender: string;
  dob: string;
  doj: string;
  res_address: string;
  email: string;
  languages:any = ['Tamil','Kanada','Telugu','Malayalam','Hindi','English'];
  preference_lang = [];
  primary_lang = [];
  salary: string;
  branch:any;
  department:any;
  manage: string;
  labels: any = [];
  date:any;
  blockSelectFlag:any = []
  categoriesData:any = []
  categories:any = [];
  categoriesSelectFlag:any = [];
  nextFarmeasyId:number = 1;
  farmeasy_id:number;
  constructor( private alertController: AlertController,
                private route: ActivatedRoute,
                private navCtrl: NavController,
                public router: Router,
                private loadingCtrl: LoadingController,
                private sg: SimpleGlobal,
                private translate: FarmeasyTranslate,
                private popoverController: PopoverController,
                private zone:NgZone,
                private location:Location,
                private en: EnglishService,
                private hi: HindiService,
                private ta:TamilService,
                private te:TeluguService,
                private kn:KanadaService) { }

  ngOnInit() {
    this.createHeader();
    this.getUsers();
    this.getBlocks();
    this.LoadLanguages();
    this.languagesprepare();
    this.getLabels();
    this.getCategories();
    this.route.params.subscribe(params => {
      this.user = JSON.parse( params['user']);
      console.log(this.user)
      this.updateflag = true;
      this.name = this.user.name;
      this.password = this.user.password;
      this.phone = this.user.phone;
      this.aadhar = this.user.aadhar;
      this.language = this.user.primary_language;
      this.profile = this.user.profile_url;
      this.role = this.user.role;
      this.blocks = this.user.block_responsible;
      this.categories = this.user.categories?this.user.categories:[],
      this.known_lang = this.user.languages;
      this.email = this.user.email;
      this.dob = this.user.dob;
      this.doj = this.user.doj;
      this.id = this.user.id_proof;
      this.bank = this.user.bank;
      this.salary = this.user.salary;
      this.branch = this.user.branch;
      this.department = this.user.department;

      this.res_address = this.user.address;
      this.manage = this.user.manage;
      this.gender = this.user.gender;
      this.farmeasy_id = this.user.farmeasy_id;
      this.prefill();
      if (this.user) {
        this.headerDataEdit();
      }
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').limitToLast(1).once('value',resp =>{
      let data = snapshotToArray(resp)[0];
      if(data.farmeasy_id){
        this.nextFarmeasyId = parseInt(data.farmeasy_id.split("-")[1])+1;
      }
      console.log(this.nextFarmeasyId);
      console.log(data);
    });
    this.date = new Date().toISOString().substring(0,10);
  }
  getLabels(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.labels  = this.en.getCreateEmployeeLabels();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.labels  = this.te.getCreateEmployeeLabels();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.labels  = this.ta.getCreateEmployeeLabels();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.labels  = this.kn.getCreateEmployeeLabels();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.labels  = this.hi.getCreateEmployeeLabels();
    } else {
       this.labels  = this.en.getCreateEmployeeLabels();
    }
  }
  headerDataEdit() {
     let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Edit Employee';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'ఉద్యోగిని సవరించండి';
    } else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பணியாளரைத் திருத்தவும்';
    } else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ನೌಕರರನ್ನು ಸಂಪಾದಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'कर्मचारी को संपादित करें';
    }
    this.headerData = {
          color: 'green',
          title: title,
          button2: 'home',
          button2_action: '/farm-dashboard',
          back: '/farmeasy-manage-labours'
    };
    this.footerData ={
      'footerColor':'rgba(255, 255, 255, 1.0)',
      'leftButtonName':'Update',
      'rightButtonName':'Delete',
      'leftButtonColor':'green',
      'rightButtonColor':'green',
      };
  }
  languagesprepare(){
    // ['Tamil','Kanada','Telugu','Malayalam','Hindi','English']
    this.preference_lang = [{name:'Tamil',value:false,code:'ta'},
    {name:'Kanada',value:false,code:'kn'},
    {name:'English',value:false,code:'en'},
    {name:'Telugu',value:false,code:'te'},
    {name:'Malayalam',value:false,code:'ml'},
    {name:'Hindi',value:false,code:'hi'}
   ]
   this.primary_lang = [{name:'Tamil',value:false,code:'ta'},
   {name:'Kanada',value:false,code:'kn'},
   {name:'English',value:false,code:'en'},
   {name:'Telugu',value:false,code:'te'},
   {name:'Malayalam',value:false,code:'ml'},
   {name:'Hindi',value:false,code:'hi'}]
  }
  createHeader() {
      let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Create Employee';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'ఉద్యోగిని సృష్టించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பணியாளரை உருவாக்குங்கள்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಉದ್ಯೋಗಿಗಳನ್ನು ರಚಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'कर्मचारी बनाएँ';
    }
     this.headerData = {
          color: 'green',
          title: title,
          back: '/farmeasy-manage-labours',
          button1: 'home',
          button1_action: '/farm-dashboard'
          };
    this.footerData ={
        'footerColor':'rgba(255, 255, 255, 1.0)',
        'middleButtonName':'Submit',
        'middleButtonColor':'green',
        };
  }
  LoadLanguages(){
    this.languages_support =[{
      'name': 'English',
      'code': 'en'
    },
    {
      'name': 'Telugu',
      'code': 'te'
    },
    {
      'name': 'Tamil',
      'code': 'ta'
    },
    {
      'name': 'Kannada',
      'code': 'kn'
    },
    {
      'name': 'Hindi',
      'code': 'hi'
    },
    {
      'name': 'Malayalam',
      'code': 'ml'
    },
    {
      'name': 'Marathi',
      'code': 'mr'
    },
    {
      'name': 'Bengali',
      'code': 'bn'
    },
    {
      'name': 'Gujarati',
      'code': 'gu'
    },
    {
      'name': 'Punjabi',
      'code': 'pa'
    },
    {
      'name': 'Nepali',
      'code': 'ne'
    },
    {
      'name': 'Sindhi',
      'code': 'sd'
    },
    {
      'name': 'Urdu',
      'code': 'ur'
    }
  ];
  }

  bindLangCode(data){
    //console.log(data.target.value);
    let temp = this.languages_support.filter(item => item.name === data.target.value);
    this.language = temp[0].code;
    this.lang_code = temp[0].name;
    //console.log(temp);
  }
  prefill(){
    for(let i=0;i<this.preference_lang.length;i++){
      for(let data of this.known_lang){
        if(this.preference_lang[i].code === data){
          this.preference_lang[i].value = true;
        }
      }
    }
    for( let i=0;i<this.primary_lang.length;i++){
      if(this.primary_lang[i].code === this.language){
        this.primary_lang[i].value = true;
      } else {
        this.primary_lang[i].value = false;
      }
    }
    setTimeout(()=>{
          for(let i=0;i<this.categoriesData.length;i++){
      for(let category of this.categories){
        if(this.categoriesData[i].id === category){
          this.categoriesSelectFlag[i] = true;
        }
      }
    }
    for(let i=0;i<this.blocksData.length;i++){
      for(let block of this.blocks){
        if(this.blocksData[i] === block){
          this.blockSelectFlag[i] = true;
        }
      }
    }
    },1000);

    
  }
  selectedLang(){
   // console.log(this.preference_lang);
    this.known_lang = [];
    for(let i=0;i<this.preference_lang.length;i++){
      if(this.preference_lang[i].value === true){
        this.known_lang.push(this.preference_lang[i].code);
      }
    }
  }
  selectprimarylang(value,idx){
    for( let i=0;i<this.primary_lang.length;i++){
      if(i === idx){
        this.primary_lang[i].value = value;
        if(value){
          this.language = this.primary_lang[i].code;
        }
      }
      else{
        this.primary_lang[i].value = false;
      }
    }
   // console.log(this.primary_lang);
  }
  async pickDate(id) {
       const popover = await this.popoverController.create({
            component: CalendercomponentComponent,
            cssClass: 'popoverclass',
            componentProps: {'previous': true},
        });
        popover.onDidDismiss().then((data) => {
         //   console.log(data);
            if(id === 'dob'){
              this.dob = data.data;
            }
            if(id === 'doj'){
              this.doj = data.data;
            }
          
        });
        await popover.present();
    }
  async deleteUser() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are You Sure Want to Delete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + this.user.key).remove().then(() => {
              this.location.back();
              // this.navCtrl.navigateBack(['/farmeasy-manage-labours']);
              // this.router.navigate(['/farmeasy-manage-labours']);
            });
          }
        }
      ]
    });
    await alert.present();
  }
  async getUsers() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').once('value', resp => {
      this.Data = snapshotToArray(resp);
      loading.dismiss();
    });
  }
  getBlocks() {
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp => {
      const data = snapshotToArray(resp);
      let temp = []
      this.blockSelectFlag = [];
      for(let item of data){
        temp.push(item.name);
        this.blockSelectFlag.push(false);
      }
      this.blocksData = temp;
      this.prefill();
    });
  }
  getCategories(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').on('value', resp => {
      this.categoriesSelectFlag = [];
      this.categoriesData = snapshotToArray(resp);
      for(let category of this.categoriesData){
        this.categoriesSelectFlag.push(false);
      }
      console.log(this.categoriesData);
      });
  }
  selectCategory(idx,category){
    if(this.categoriesSelectFlag[idx] == false){
      this.categoriesSelectFlag[idx] = true;
      this.categories.push(category.id);
    } else {
      this.categoriesSelectFlag[idx] = false;
      for(let i=0; i<this.categories.length; i++)
      {
        if(this.categories[i] == category.id){
           this.categories.splice(i,1);
        }
      }
    }
    console.log(this.categories);
  }
  selectBlock(idx,block){
    this.blockSelectFlag[idx] = !this.blockSelectFlag[idx];
    let index = this.blocks.indexOf(block);
    if(index == -1){
      this.blocks.push(block);
    } else {
      this.blocks.splice(index,1);
    }
    console.log(this.blocks)
  }
  chooseFile(id) {
    document.getElementById(id).click();
  }
  async upload(event,id) {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
      const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
                 'source':'Image'
             },
            });
            await popover.present();
            popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
              }
            });
      this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/Users/images/' + data.name);
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/Users/images/' + data.name).put(data).then(function(snapshot) {
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/Users/images/' +  data.name).getDownloadURL().then(function(url) {
        popover.dismiss();
        if(id === 'ID'){
          that.id = url;
        }
        if(id === 'profile'){
          that.profile = url;
        }
        if(id === 'bank'){
          that.bank = url;
        }
        });
      });
      that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/Users/images/' + data.name).put(data).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
    } else {
      const alert = await this.alertController.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
  }
  submit() {
    let that;
    that = this;
    let defaulturl="https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2Fdefault.jpeg?alt=media&token=07684228-cd2b-4b0b-9a8c-7fbf4560e766";
   const usersBody = { name: this.name,
      password: this.password,
      phone: this.phone.toString(),
      aadhar:this.aadhar?this.aadhar.toString():null,
      email:this.email ? this.email : null,
      dob:this.dob ? this.dob : null,
      gender: this.gender ? this.gender : null,
      address:this.res_address ? this.res_address : null,
      id_proof: this.id ? this.id : null,
      bank:this.bank ? this.bank : null,
      doj:this.doj?this.doj:null,
      salary: this.salary?this.salary:null,
      branch: this.branch?this.branch:null,
      department: this.department?this.department:null,

      manage:this.manage?this.manage:null,
      primary_language: this.language,
      languages: this.known_lang,
      profile_url: this.profile?this.profile:defaulturl,
      role: this.role,
      categories:this.categories,
      farmeasy_id:this.sg['userdata'].vendor_id+'-'+this.nextFarmeasyId,
      vendor_id : this.sg['userdata'].vendor_id,
      block_responsible: this.role !== 'employee'&& this.role !== 'supervisor'  ? this.blocksData : this.blocks,};
    const salaryBody = {
      name: this.name,
      phone: this.phone.toString(),
      profile_url: this.profile?this.profile:defaulturl,
      role: this.role,
      salary: this.salary?this.salary:100,
      farmeasy_id:this.sg['userdata'].vendor_id+'-'+this.nextFarmeasyId,
    }
    const farmeasy_usersBody = {
      farmeasy_id:this.sg['userdata'].vendor_id+"-"+this.nextFarmeasyId,
      password: this.password,
      phone: this.phone.toString(),
      vendor_id : this.sg['userdata'].vendor_id
    }
      firebase.database().ref(this.sg['userdata'].vendor_id+'/users').push(usersBody).then(function(snapshot) {
        firebase.database().ref(that.sg['userdata'].vendor_id+'/salary').push(salaryBody).then(function(snapshot) {
          firebase.database().ref('farmeasy_users').push(farmeasy_usersBody).then(function(snapshot) {
              that.location.back();
           });
        });
        // that.router.navigate(['/farmeasy-manage-labours']);
      });
  }
  update() {
    let that;
    that = this;
    let defaulturl="https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/farmeasy%2Femployees%2Fdefault.jpeg?alt=media&token=07684228-cd2b-4b0b-9a8c-7fbf4560e766";
    const usersBody = { name: this.name,
      password: this.password,
      phone: this.phone.toString(),
      aadhar:this.aadhar?this.aadhar.toString():null,
      email:this.email ? this.email : null,
      dob:this.dob ? this.dob : null,
      gender: this.gender ? this.gender : null,
      address:this.res_address ? this.res_address : null,
      id_proof: this.id ? this.id : null,
      bank:this.bank ? this.bank : null,
      doj:this.doj?this.doj:null,
      salary: this.salary?this.salary:null,
      branch:this.branch?this.branch:null,
      department:this.department?this.department:null,

      manage:this.manage?this.manage:null,
      primary_language: this.language,
      languages: this.known_lang,
      profile_url: this.profile?this.profile:defaulturl,
      role: this.role,
      categories:this.categories,
      block_responsible: this.role !== 'employee' && this.role !== 'supervisor' ? this.blocksData : this.blocks,};
      const salaryBody = {
        name: this.name,
        phone: this.phone.toString(),
        profile_url: this.profile?this.profile:defaulturl,
        role: this.role,
        salary: this.salary?this.salary:100,
        farmeasy_id:this.farmeasy_id
      }
      const farmeasy_usersBody = {
        password: this.password,
        phone: this.phone.toString(),
      }
      firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + this.user.key).update(usersBody).then(function(snapshot) {
          firebase.database().ref(that.sg['userdata'].vendor_id+'/salary').orderByChild('farmeasy_id').equalTo(that.farmeasy_id).once('value',resp=>{
            let salary = snapshotToArray(resp)[0];
            console.log(salary);
            firebase.database().ref(that.sg['userdata'].vendor_id+'/salary/'+salary.key).update(salaryBody).then(()=>{
              firebase.database().ref('farmeasy_users').orderByChild('farmeasy_id').equalTo(that.farmeasy_id).once('value',resp =>{
                let user =  snapshotToArray(resp)[0];
                firebase.database().ref('farmeasy_users/'+user.key).update(farmeasy_usersBody).then(()=>{
                  that.location.back();
                });
              });
            })
        });
        // that.router.navigate(['/farmeasy-manage-labours']);
      });
  }
 async validateData() {
    let error = [];
    if (this.phone) {
      if (this.phone.toString().length !== 10) {
        error.push('Phone number Must contains 10 Digits');
      }
    }
    if (!this.phone) {
      error.push('Mobile Number');
    }
    if (this.users.length > 0) {
      error.push('User is Already Exists');
    }
    if (!this.name) {
      error.push('username');
    }
    if (!this.password) {
      error.push('password');
    }
    if (!this.language) {
      error.push('Language');
    }
    if (!this.role) {
      error.push('Role');
    }
    // if (!this.profile) {
    //   error.push('profile pic');
    // }
    if(!this.gender){
      error.push('Gender')
    }
    // if(!this.dob){
    //   error.push('Date of Birth')
    // }
    // if(!this.doj){
    //   error.push('Date of Joining');
    // }
    // if(!this.aadhar){
    //   error.push('Aadhar Number');
    // }
    if (error.length !== 0) {
      const alert = await this.alertController.create({
        subHeader: 'Please fill manditory fields',
        message: error.join(','),
        buttons: ['OK']
      });
      return await alert.present();
  }  else {
    if (this.updateflag) {
        this.update();
    } else {
      this.submit();
    }
  }
}
  validateNumber(data) {
    this.users = this.Data.filter(user => {
      if (user.phone.includes(this.phone)) {
        return true;
      }
  });
}
dateConverterDateOfJoining(date){
  if(date){
    let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
  } else {
    return 'Date of Join';
  }
}
dateConverterDateOfBirth(date){
  if(date){
    let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
  } else {
    return 'Date of Birth';
  }
}
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
