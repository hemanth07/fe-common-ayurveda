import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasyAddUsersPage } from './farmeasy-add-users.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { HarvestcomponentsModule } from '../../components/harvestcomponents/harvestcomponents.module';
const routes: Routes = [
  {
    path: '',
    component: FarmeasyAddUsersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    HarvestcomponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyAddUsersPage]
})
export class FarmeasyAddUsersPageModule {}
