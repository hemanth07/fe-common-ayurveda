import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmMapPage } from './farm-map.page';

describe('FarmMapPage', () => {
  let component: FarmMapPage;
  let fixture: ComponentFixture<FarmMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmMapPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
