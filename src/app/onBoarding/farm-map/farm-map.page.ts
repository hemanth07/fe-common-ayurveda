import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farm-map',
  templateUrl: './farm-map.page.html',
  styleUrls: ['./farm-map.page.scss'],
})
export class FarmMapPage implements OnInit {
  headerData: any = [];

  constructor() { }

  ngOnInit() {
    this.headerData = {
      color: 'teal',
      title: "Farm Map/Layout",
      button1: 'home',
      button1_action: '/farm-dashboard'
      };
  }

}
