import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-view-category',
  templateUrl: './farmeasy-view-category.page.html',
  styleUrls: ['./farmeasy-view-category.page.scss'],
})
export class FarmeasyViewCategoryPage implements OnInit {
  headerData: any = [];
  categoryData: any = [];
  constructor( private loadingCtrl: LoadingController,
                private navCtrl: NavController,
                public router: Router,
                private sg: SimpleGlobal,
                private translate: FarmeasyTranslate) { }

  ngOnInit() {
    this.createHeader();
    this.getCategory();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Manage Categories';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'వర్గం నిర్వహించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'வகைகள் நிர்வகி';
    } else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ವರ್ಗಗಳನ್ನು ನಿರ್ವಹಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'श्रेणी व्यवस्थित करें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button1: 'add-circle-outline',
      button1_action: 'farmeasy-add-category',
      button2: 'home',
      button2_action: '/farm-dashboard',
      };
  }
  async getCategory() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').on('value', resp => {
      const data = snapshotToArray(resp);
      this.categoryData = data;
      loading.dismiss();
    });
  }
  selectCategory(data) {
    const body = { category: JSON.stringify(data)};
    this.navCtrl.navigateBack(['/farmeasy-add-category/', body]);
    //  this.router.navigate(['/farmeasy-add-category/', body], { replaceUrl: true});
  }

}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};