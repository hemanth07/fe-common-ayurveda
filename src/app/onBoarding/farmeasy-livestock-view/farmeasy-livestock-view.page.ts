import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Router, Route } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import { NavController } from '@ionic/angular';
import { IonRouterOutlet } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-livestock-view',
  templateUrl: './farmeasy-livestock-view.page.html',
  styleUrls: ['./farmeasy-livestock-view.page.scss'],
})
export class FarmeasyLivestockViewPage implements OnInit {
  item: any = {};
  title: string;
  headerData: any = {};
  stock_data: any = [];
  nodatafound:any;
  constructor(
    private route: ActivatedRoute,
    private navCtrl:NavController,
    private ionRouterOutlet: IonRouterOutlet,
    public router: Router,
    private sg: SimpleGlobal,
    private translate: FarmeasyTranslate
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.item = JSON.parse( params['item']);
      this.title = this.item.name;
      this.createHeader(this.item);
    });
    if(this.sg['userdata'].primary_language === 'te'){
      this.nodatafound = 'డేటా కనుగొనబడలేదు';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.nodatafound = 'வேறு தகவல்கள் இல்லை';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.nodatafound = 'ಯಾವುದೇ ಡೇಟಾ ಕಂಡುಬಂದಿಲ್ಲ';
    } else if(this.sg['userdata'].primary_language === 'en'){
      this.nodatafound = 'No Data Found';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.nodatafound = 'कोई डेटा नहीं मिला';
    } 
  }
  createHeader(item) {
    // color: green,orange,purple
    if (this.sg['userdata'].primary_language !== 'en') {
      this.translate.translate(item.name, 'en', this.sg['userdata'].primary_language).then(data => {
        this.headerData = {
          color: 'teal',
          title: data,
          button1: 'add-circle-outline',
          button1_action: 'farmeasy-livestock-add',
          button2: 'home',
         button2_action: '/farm-dashboard',
         
          };
      });
      } else {
      this.headerData = {
        color: 'teal',
        title: item.name,
        button1: 'add-circle-outline',
        button1_action: 'farmeasy-livestock-add', 
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
     }
    this.getLovsData(item.name);
  }
  getLovsData(key){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/livestocks/'+key).on('value', resp => {
      const data = snapshotToArray(resp);
      this.stock_data = data;
     // console.log(this.stock_data);
    });
  }
  selectStock(stock){
    let temp = stock;
    temp['title'] = this.title;
    const body = { stock: JSON.stringify(temp)};
     this.navCtrl.navigateForward(['/farmeasy-livestock-add/', body]);
    //  this.router.navigate(['/farmeasy-livestock-add/', body], { replaceUrl: true});
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};

