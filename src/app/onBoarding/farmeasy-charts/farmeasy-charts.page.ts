import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { AlertController, LoadingController } from '@ionic/angular';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { FarmeasyTranslate } from '../../translate.service';

@Component({
  selector: 'app-farmeasy-charts',
  templateUrl: './farmeasy-charts.page.html',
  styleUrls: ['./farmeasy-charts.page.scss'],
})
export class FarmeasyChartsPage implements OnInit {
  headerData: any = [];
  public pieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [ ['Users', 'count']],
    options: {'title': 'Users', 'pieSliceText': 'label', 'is3D': true},
  };
  public observationpieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [ ['Observation', 'block']],
    options: {'title': 'Observation', 'pieSliceText': 'label', 'is3D': true},
  };
  public prioritypieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [ ['observation', 'priority']],
    options: {'title': 'Priority', 'pieSliceText': 'label', 'is3D': true},
  };
  public taskspieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [ ['Category', 'Count']],
    options: {'title': 'Category', 'pieSliceText': 'label', 'is3D': true},
  };
  constructor(private sg: SimpleGlobal, private loadingCtrl: LoadingController, private translate: FarmeasyTranslate) { }

  ngOnInit() {
    this.createHeader();
    this.getData();
    this.getCategories();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Charts';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'చార్ట్లు';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'விளக்கப்படங்கள்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಪಟ್ಟಿಯಲ್ಲಿ';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button2: 'home',
        button2_action: '/farm-dashboard',
     
      };
   }
   async getData() {
    let that;
    that = this;
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').once('value', resp => {
      const users = ['employee', 'admin', 'supervisor'];
     // console.log(users);
      const data = snapshotToArray(resp);
      for (const label of users) {
        let temp = [];
        temp = data.filter(item => {if (item.role === label) {return true; }});
        this.pieChart.dataTable.push([label, temp.length]);
      }
   //   console.log(this.pieChart);
    });
    firebase.database().ref(this.sg['userdata'].vendor_id+'/observations').once('value', resp => {
        const observation = snapshotToArray(resp);
        const priorities = [ 'High', 'Urgent', 'Medium', 'Low'];
        for (const priority of priorities) {
          let temp = [];
          temp = observation.filter(item => {if (item.proprity === priority) {return true; }});
          this.prioritypieChart.dataTable.push([priority, temp.length]);
        }
        firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp1 => {
          const data = snapshotToArray(resp1);
          for (const block of data) {
            let temp3 = [];
             temp3 = observation.filter(item => {if (item.block === block.name) {return true; }});
             if(temp3.length !== 0){
              this.observationpieChart.dataTable.push([block.name, temp3.length]);
             }
           }
        });
      loading.dismiss();
    });
  }
   getCategories(){
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories').once('value', resp => {
      firebase.database().ref(this.sg['userdata'].vendor_id+'/taskManager/task_table').once('value', resp1 => {
        const data = snapshotToArray(resp);
        const Tasks = snapshotToArray(resp1);
        for (const category of data) {
          let temp = [];
          temp = Tasks.filter(item => {if (item.category.name === category.name) {return true; }});
          if (temp.length !== 0) {
            this.taskspieChart.dataTable.push([category.name, temp.length]);
           }
        }
      });
    //  console.log(this.taskspieChart);
    });
  }
}

export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};