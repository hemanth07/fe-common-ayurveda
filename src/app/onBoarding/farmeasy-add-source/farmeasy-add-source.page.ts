import { Component, OnInit, NgZone } from '@angular/core';
import { AlertController, LoadingController, NavController, PopoverController} from '@ionic/angular';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Location } from '@angular/common';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-farmeasy-add-source',
  templateUrl: './farmeasy-add-source.page.html',
  styleUrls: ['./farmeasy-add-source.page.scss'],
})
export class FarmeasyAddSourcePage implements OnInit {
  headerData: any = [];
  footerData: any = [];
  source: any = [];
  name: string;
  type: string;
  source_img: string = "https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/Users%2Fimages%2Fplaceholder_user.png?alt=media&token=43431add-b616-4bd2-9462-6ba03e60d85e";
  source_data: any = [];
  updateflag: boolean;
  idx: any;
  constructor(private alertController: AlertController,
              private route: ActivatedRoute,
              private navCtrl: NavController,
              public router: Router,
              private loadingCtrl: LoadingController,
              private location:Location,
              private sg: SimpleGlobal,
              private zone:NgZone,
              public popoverController: PopoverController,
              private translate: FarmeasyTranslate) { }

  ngOnInit() {
    this.createHeader();
    this.createSourceData();
    this.route.params.subscribe(params => {
      this.source = JSON.parse( params['source']);
      this.name = this.source.source_name;
      this.type = this.source.type;
      this.source_img = this.source.image;
      this.source_data = this.source.data;
      this.updateflag = true;
      this.headerDataEdit();
    });
  }
  headerDataEdit() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Edit Source';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'మూలాన్ని సవరించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'மூலத்தைத் திருத்து';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಮೂಲವನ್ನು ಸಂಪಾದಿಸಿ';
    }else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'स्रोत संपादित करें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      back: '/farmeasy-view-source',
      button1: 'home',
      button1_action: '/farm-dashboard'
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.9)',
        'leftButtonName':'Update',
        'rightButtonName':'Delete',
        'leftButtonColor':'teal',
        'rightButtonColor':'teal',
        };
   }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Add Source';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'మూల జోడించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'மூலத்தைச் சேர்க்கவும்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಮೂಲ ಸೇರಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'स्रोत जोड़ें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      back: '/farmeasy-view-category',
      button1: 'home',
      button1_action: '/farm-dashboard'
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.8)',
        'middleButtonName':'Submit',
        'middleButtonColor':'teal',
        };
    
   }
  createSourceData() {
    const body = { name: '', image: 'https://firebasestorage.googleapis.com/v0/b/farmeasy-2b7e4.appspot.com/o/Users%2Fimages%2Fplaceholder_user.png?alt=media&token=43431add-b616-4bd2-9462-6ba03e60d85e', source_code: ''};
    this.source_data.push(body);
  }
  chooseFile() {
    document.getElementById('image').click();
  }
  async upload(event) {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
            const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
                 'source':'Image'
             },
            });
            await popover.present();
            popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
              }
            });
      this.sg['currentFile'] =firebase.storage().ref(this.sg['userdata'].vendor_id+'/source/images/' + data.name);
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/source/images/' + data.name).put(data).then(function(snapshot) {
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/source/images/' +  data.name).getDownloadURL().then(function(url) {
        popover.dismiss();
        that.source_img = url;
        // alert(url);
        });
      });
      that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/source/images/' + data.name).put(data).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
    } else {
      const alert = await this.alertController.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
  }
  chooseFile1(idx) {
    this.idx = idx;
    document.getElementById('image1').click();
  }
  async upload1(event, idx) {
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
      const loading = await this.loadingCtrl.create({
        spinner: 'bubbles',
        message: 'Please wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/source/images/' + data.name).put(data).then(function(snapshot) {
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/source/images/' +  data.name).getDownloadURL().then(function(url) {
        loading.dismiss();
        that.source_data[that.idx]['image'] = url;
        });
      });
    } else {
      const alert = await this.alertController.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
  }
  watch(idx) {
    if (idx === this.source_data.length-1) {
      if (this.source_data[idx]['name'] !== '' && this.source_data[idx]['source_code'] !== '') {
        this.createSourceData();
      }
    }
  }
 async validateData() {
    let error = [];
    if (!this.name) {
      error.push('Name');
    }
    if (!this.type) {
      error.push('Type');
    }
    if (error.length !== 0) {
      const alert = await this.alertController.create({
        subHeader: 'Please fill manditory fields',
        message: error.join(','),
        buttons: ['OK']
      });
      return await alert.present();
  }  else {
    if (this.updateflag) {
        this.update();
    } else {
      this.submit();
    }
  }
}
  submit() {
    let that;
    that = this;
    for (let i = 0; i < this.source_data.length; i++ ) {
      if (this.source_data[i].name.length === 0) {
        this.source_data.splice(i, 1);
      }
    }
    const body = { source_name: this.name,
      type: this.type,
      image: this.source_img,
      data: this.source_data
      };
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/sources/').push(body).then(function(snapshot) {
        that.location.back();
        // that.router.navigate(['/farmeasy-view-source']);
      });
  }
  update() {
    let that;
    that = this;
    for (let i = 0; i < this.source_data.length; i++ ) {
      if (this.source_data[i].name.length === 0) {
        this.source_data.splice(i, 1);
      }
    }
    const body = { source_name: this.name,
      type: this.type,
      image: this.source_img,
      data: this.source_data
      };
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/sources/' + this.source.key).update(body).then(function(snapshot) {
        that.location.back();
        // that.router.navigate(['/farmeasy-view-source']);
      });
  }
  async deleteUser() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are You Sure Want to Delete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/sources/' + this.source.key).remove().then(() => {
              this.location.back();
              // this.navCtrl.navigateBack(['/farmeasy-view-source']);
              // this.router.navigate(['/farmeasy-view-source']);
            });
          }
        }
      ]
    });
    await alert.present();
  }


}
