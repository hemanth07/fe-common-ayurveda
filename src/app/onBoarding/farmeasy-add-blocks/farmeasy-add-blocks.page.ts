import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-farmeasy-add-blocks',
  templateUrl: './farmeasy-add-blocks.page.html',
  styleUrls: ['./farmeasy-add-blocks.page.scss'],
})
export class FarmeasyAddBlocksPage implements OnInit {
  blocksData: any = [];
  headerData: any = [];
  footerData:any = [];
  Data: any = [];
  name: string;
  plots: string;
  color: string;
  block: any = [];
  updateflag: boolean;
  subDivisions: any = [];
  watering: any = [];
  cleaning: any = [];
  fertigation: any = [];
  sub_div_len: number;
  disable: boolean = true;
  constructor(  private alertController: AlertController,
                private route: ActivatedRoute,
                private navCtrl: NavController,
                public router: Router,
                private loadingCtrl: LoadingController,
                private sg: SimpleGlobal,
                private translate: FarmeasyTranslate,
                private location: Location,
                public toastController: ToastController) { }

  ngOnInit() {
    this.getBlocks();
    this.route.params.subscribe(params => {
      if(params['block']){
        this.block = JSON.parse(params['block']);
        this.name = this.block.name;
        this.plots = this.block.no_of_plots;
        this.color = this.block.color;
        this.updateflag = true;
        this.subDivisions = this.block.sub_division?this.block.sub_division : [];
        if(this.block.sub_division){
          for(let data of this.block.sub_division){
            this.watering.push(data.watering);
            this.cleaning.push(data.cleaning);
            this.fertigation.push(data.fertigation);
          }
        }
        this.headerDataEdit();
      } else {
        this.createHeader();
      }
      });
  }
  prepareSubDivision(){
    this.subDivisions.push({name:'',flag:false});
    this.watering.push({source1:'',source2:''});
    this.cleaning.push({source1:'',source2:''});
    this.fertigation.push({source1:'',source2:''});
    this.sub_div_len = this.subDivisions.length;
  }
  openAccordian(idx){
    this.subDivisions[idx].flag = this.subDivisions[idx].flag ? false : true;
  }
  addRow(idx){
   
    if(idx === this.sub_div_len-1){
      this.prepareSubDivision();
    }
  }
  headerDataEdit() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Edit Block';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'బ్లాక్ను సవరించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பிளாக் ஐத் திருத்துக';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ನಿರ್ಬಂಧಿಸು ಸಂಪಾದಿಸಿ';
    }else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'ब्लॉक संपादित करें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      back: '/farmeasy-view-blocks',
      button1: 'home',
      button1_action: '/farm-dashboard'
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.9)',
        'leftButtonName':'Update',
        'rightButtonName':'Delete',
        'leftButtonColor':'teal',
        'rightButtonColor':'teal',
        };
      this.prepareSubDivision();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Add Block';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'బ్లాక్ జోడించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'தொகுதி சேர்க்க';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಬ್ಲಾಕ್ ಸೇರಿಸಿ';
    }else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'ब्लॉक जोड़ें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      back: '/farmeasy-view-blocks',
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.8)',
        'middleButtonName':'Submit',
        'middleButtonColor':'teal',
        };
     this.prepareSubDivision();
 }
  getBlocks() {
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/frequency').once('value', resp => {
      this.Data = snapshotToArray(resp);
      this.blocksData = this.Data;
    });
  }
  validate(){
    if(this.name && this.plots && this.color){
      this.disable = false;
    }
  }
 async validateData() {
    let error = [];
    if (!this.name) {
      error.push('Block Name');
    }
    if (!this.plots) {
      error.push('No of Plots');
    }
    if (!this.color) {
      error.push('Color');
    }
    if (error.length !== 0) {
      const alert = await this.alertController.create({
        subHeader: 'Please fill manditory fields',
        message: error.join(','),
        buttons: ['OK']
      });
      return await alert.present();
  } else {
    if (this.updateflag) {
      this.update();
       } else {
         this.submit();
       }
     }
  }
  async deleteDiv(idx){
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are You Sure Want to Delete this Division',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.subDivisions.splice(idx,1);
            this.watering.splice(idx,1);
            this.cleaning.splice(idx,1);
            this.fertigation.splice(idx,1);
            this.sub_div_len = this.subDivisions.length;
          }
        }
      ]
    });
    await alert.present();
  }
  submit() {
    for( let i = 0; i < this.subDivisions.length; i++){
    this.subDivisions[i].flag = false;
    this.subDivisions[i].cleaning = this.cleaning[i];
    this.subDivisions[i].watering = this.watering[i];
    this.subDivisions[i].fertigation = this.fertigation[i];
   }
   this.subDivisions = this.subDivisions.filter(item => item.name !== '');
    const body = { color : this.color,
      no_of_plots: this.plots,
      name: this.name,
      sub_division: this.subDivisions};
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').push(body).then(async () => {
      const toast = await this.toastController.create({
        message: 'New Block Saved',
        duration: 2000
      });
      toast.present();
      this.location.back();
    });
  }
  update() {
  for( let i = 0; i < this.subDivisions.length; i++){
    this.subDivisions[i].flag = false;
    this.subDivisions[i].cleaning = this.cleaning[i];
    this.subDivisions[i].watering = this.watering[i];
    this.subDivisions[i].fertigation = this.fertigation[i];
   }
   this.subDivisions = this.subDivisions.filter(item => item.name !== '');
    const body = { color : this.color,
      no_of_plots: this.plots,
      name: this.name,
      sub_division: this.subDivisions};
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks/' + this.block.key).update(body).then(async () => {
      const toast = await this.toastController.create({
        message: 'Block Updated',
        duration: 2000
      });
      toast.present();
      this.location.back();
    });
  }
  async deleteBlock() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are You Sure Want to Delete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks/' + this.block.key).remove().then(async () => {
              const toast = await this.toastController.create({
                message: 'Block Deleted',
                duration: 2000
              });
              toast.present();
              this.location.back();
            });
          }
        }
      ]
    });
    await alert.present();
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};