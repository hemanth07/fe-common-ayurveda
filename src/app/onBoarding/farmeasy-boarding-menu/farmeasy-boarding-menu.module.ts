import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FarmeasyBoardingMenuPage } from './farmeasy-boarding-menu.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
const routes: Routes = [
  {
    path: '',
    component: FarmeasyBoardingMenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmeasyBoardingMenuPage]
})
export class FarmeasyBoardingMenuPageModule {}
