import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { OnInit, Component, ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { AlertController, LoadingController , NavController} from '@ionic/angular';
import { FarmeasyTranslate } from '../../translate.service';
@Component({
  selector: 'app-farmeasy-boarding-menu',
  templateUrl: './farmeasy-boarding-menu.page.html',
  styleUrls: ['./farmeasy-boarding-menu.page.scss'],
})
export class FarmeasyBoardingMenuPage implements OnInit {
  headerData: any = [];
  menuData: any = [];
  constructor(private sg: SimpleGlobal, private loadingCtrl: LoadingController,private navCtrl:NavController,
    private translate: FarmeasyTranslate,
    private resolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private en: EnglishService,
    private hi: HindiService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService
    ) { }

  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    // color: green,orange,purple
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Onboarding/Setup';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'ఆన్‌బోర్డింగ్ / సెటప్';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'உள்நுழைவு / அமைப்பு';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಆನ್‌ಬೋರ್ಡಿಂಗ್ / ಸೆಟಪ್';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'ऑनबोर्डिंग / सेटअप';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard'
      };
  }
 translation(){
  if(this.sg['userdata'].primary_language === 'en'){
    this.menuData  = this.en.getOnBoarding();
  } else if(this.sg['userdata'].primary_language === 'te'){
    this.menuData  = this.te.getOnBoarding();
  } else if(this.sg['userdata'].primary_language === 'ta'){
    this.menuData  = this.ta.getOnBoarding();
  } else if(this.sg['userdata'].primary_language === 'kn'){
    this.menuData  = this.kn.getOnBoarding();
  } else if(this.sg['userdata'].primary_language === 'hi'){
    this.menuData  = this.hi.getOnBoarding();
  } else {
     this.menuData  = this.en.getOnBoarding();
  }
}
selectedTask(task) {
  this.navCtrl.navigateForward(task.action);
  }
  getBoardingMenu(data) {
   // console.log(data);
  }
}

