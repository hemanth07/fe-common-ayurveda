import { Component, OnInit } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Router, Route } from '@angular/router';
import { NavController } from '@ionic/angular';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
@Component({
  selector: 'app-farmeasy-livestock-menu',
  templateUrl: './farmeasy-livestock-menu.page.html',
  styleUrls: ['./farmeasy-livestock-menu.page.scss'],
})
export class FarmeasyLivestockMenuPage implements OnInit {
  headerData: any = {};
  list: any = [];
  constructor(
              public router: Router,
              private navCtrl:NavController,
              private sg: SimpleGlobal,
              private translate: FarmeasyTranslate,
              private en: EnglishService,
              private ta:TamilService,
              private te:TeluguService,
              private kn:KanadaService,
              private hi:HindiService
  ) { }

  ngOnInit() {
    this.createHeader();
    this.createList();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Live Stock';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'లైవ్ స్టాక్';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'நேரடி பங்கு';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಜಾನುವಾರು';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'लाइव स्टॉक';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button2: 'home',
      button2_action: '/farm-dashboard',
      };
   }
  createList() {
    if(this.sg['userdata'].primary_language === 'en'){
       this.list  = this.en.getLiveStockMenu();
    } else if(this.sg['userdata'].primary_language === 'te'){
       this.list  = this.te.getLiveStockMenu();
    } else if(this.sg['userdata'].primary_language === 'ta'){
       this.list  = this.ta.getLiveStockMenu();
    } else if(this.sg['userdata'].primary_language === 'kn'){
       this.list  = this.kn.getLiveStockMenu();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      this.list  = this.hi.getLiveStockMenu();
   } else {
        this.list  = this.en.getLiveStockMenu();
    }
 }
  naviagation(data){
    const body = { item: JSON.stringify(data)};
     this.navCtrl.navigateForward([data.action, body]);
    // this.router.navigate([data.action, body]);
  }

}
