import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { AlertController, LoadingController, NavController, PopoverController} from '@ionic/angular';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';

@Component({
  selector: 'app-farmeasy-livestock-add',
  templateUrl: './farmeasy-livestock-add.page.html',
  styleUrls: ['./farmeasy-livestock-add.page.scss'],
})
export class FarmeasyLivestockAddPage implements OnInit {
  headerData: any = [];
  address: string;
  name: string;
  gst: string;
  blocks: any = [];
  category: any = [];
  stock: any = [];
  blocksData: any = [];
  updateflag: boolean;
  item: any = {};
  title: string;
  constructor(
    private alertController: AlertController,
    private route: ActivatedRoute,
    public router: Router,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private location: Location,
    private zone:NgZone,
    public popoverController: PopoverController,
  ) { }

  ngOnInit() {
    this.getBlocks();
      this.route.params.subscribe(params => {
      this.stock = JSON.parse( params['stock']);
      this.name = this.stock.name;
      this.address = this.stock.image;
      this.gst = this.stock.GST;
      this.blocks = this.stock.block;
      this.title = this.stock.title;
      this.updateflag = true;
      this.headerDataEdit(this.title);
    });
    this.route.params.subscribe(params => {
      this.item = JSON.parse( params['data']);
      this.title = this.item.title;
     // console.log(this.item);
      this.createHeader(this.item.title);
     });
  }
  headerDataEdit(title) {
      if (this.sg['userdata'].primary_language !== 'en') {
      this.translate.translate('Edit '+title, 'en', this.sg['userdata'].primary_language).then(data => {
        this.headerData = {
          color: 'teal',
          title: data,
          back: '/farmeasy-livestock-menu',
          header: title,
          button2: 'home',
        button2_action: '/farm-dashboard',
          };
      });
    } else {
      this.headerData = {
        color: 'teal',
        title: 'Edit '+title,
        back: '/farmeasy-livestock-menu',
        header: title,
        button2: 'home',
        button2_action: '/farm-dashboard',
        };
    }
  }
  createHeader(item) {
    // color: green,orange,purple
    if (this.sg['userdata'].primary_language !== 'en') {
      this.translate.translate('Add '+item, 'en', this.sg['userdata'].primary_language).then(data => {
        this.headerData = {
          color: 'green',
          title: data,
          back: '/farmeasy-livestock-menu',
          header: item
          };
      });
    } else {
      this.headerData = {
        color: 'green',
        title: 'Add '+item,
        back: '/farmeasy-livestock-menu',
        header: item
        };
    }
  }
  getBlocks() {
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').once('value', resp => {
      const data = snapshotToArray(resp);
      let temp = []
      for(let item of data){
        temp.push(item.name);
      }
      this.blocksData = temp;
    });
  }

  chooseFile() {
    document.getElementById('image').click();
  }
  async upload(event) {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
            const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
                 'source':'Image'
             },
            });
            await popover.present();
            popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
                this.sg['currentFile'].cancel();
                return true;
              }
            });
      this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/livestock/' + data.name);
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/livestock/' + data.name).put(data).then(function(snapshot) {
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmeasy/livestock/' +  data.name).getDownloadURL().then(function(url) {
        popover.dismiss();
        that.address = url;
        alert(url);
        });
      });
      that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/livestock/' + data.name).put(data).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });

    } else {
      const alert = await this.alertController.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
  }
  submit() {
    let that;
    that = this;
    const body = { name: this.name,
                   image: this.address,
                   GST:this.gst,
                   block:this.blocks
                  };
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/livestocks/'+this.title).push(body).then(function(snapshot) {
      const body = { item: JSON.stringify({name: that.title})};
      that.location.back();
      // that.navCtrl.navigateForward(['/farmeasy-livestock-menu',body]);
      
    });
  }
  update() {
    let that;
    that = this;
    const body = { 
      name: this.name,
      image: this.address,
      GST:this.gst,
      block:this.blocks
      };
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/livestocks/'+this.title+'/' + this.stock.key).update(body).then(function(snapshot) {
        const body = { item: JSON.stringify({name: that.title})};
        that.location.back();
        // that.navCtrl.navigateForward(['/farmeasy-livestock-menu',body]);
      });
  }
  async deleteCategory() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are You Sure Want to Delete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/livestocks/'+this.title+'/' + this.stock.key).remove().then(() => {
               const body = { item: JSON.stringify({name: this.title})};
               this.location.back();
              //  this.navCtrl.navigateForward(['/farmeasy-livestock-menu',body]);
            });
          }
        }
      ]
    });
    await alert.present();
  }
 async validateData() {
   let error= [];
    if (!this.name) {
      error.push('username');
    }
    if (!this.address) {
      error.push('Image');
    }
    if (!this.gst) {
      error.push('GST');
    }
    if (!this.blocks) {
      error.push('Block');
    }
    if (error.length !== 0) {
      const alert = await this.alertController.create({
        subHeader: 'Please fill manditory fields',
        message: error.join(','),
        buttons: ['OK']
      });
      return await alert.present();
  }  else {
    if (this.updateflag) {
        this.update();
    } else {
      this.submit();
    }
  }
}

}

export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
