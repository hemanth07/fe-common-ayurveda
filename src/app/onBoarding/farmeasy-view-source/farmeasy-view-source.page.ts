import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-view-source',
  templateUrl: './farmeasy-view-source.page.html',
  styleUrls: ['./farmeasy-view-source.page.scss'],
})
export class FarmeasyViewSourcePage implements OnInit {
  headerData: any = [];
  sources: any = [];
  constructor( private loadingCtrl: LoadingController,
              private navCtrl:NavController,
              public router: Router,
              private sg: SimpleGlobal,
              private translate: FarmeasyTranslate) { }

  ngOnInit() {
    this.createHeader();
    this.getCategory();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Manage Sources';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'సోర్సెస్ నిర్వహించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'ஆதாரங்களை நிர்வகி';
    } else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಮೂಲಗಳನ್ನು ನಿರ್ವಹಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'स्रोत प्रबंधित करें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button1: 'add-circle-outline',
      button1_action: 'farmeasy-add-source',
      button2: 'home',
      button2_action: '/farm-dashboard',
      
      };
  }
  async getCategory() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/sources').on('value', resp => {
      const data = snapshotToArray(resp);
      this.sources = data;
      console.log(this.sources);
      loading.dismiss();
    });
  }
  getSource(source) {
    const body = { source:  JSON.stringify(source)};
    this.navCtrl.navigateBack(['/farmeasy-add-source', body]);
    // this.router.navigate(['/farmeasy-add-source', body], { replaceUrl: true});
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
