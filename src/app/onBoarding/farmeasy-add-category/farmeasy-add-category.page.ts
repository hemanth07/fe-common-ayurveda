import { Component, OnInit, NgZone } from '@angular/core';
import { AlertController, LoadingController, NavController, PopoverController} from '@ionic/angular';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { Location } from '@angular/common';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar.component';


@Component({
  selector: 'app-farmeasy-add-category',
  templateUrl: './farmeasy-add-category.page.html',
  styleUrls: ['./farmeasy-add-category.page.scss'],
})
export class FarmeasyAddCategoryPage implements OnInit {
  headerData: any = [];
  footerData: any = [];
  address: string;
  name: string;
  category: any = [];
  categoryData: any = [];
  updateflag: boolean;
  disable:boolean = true;
  constructor(private alertController: AlertController,
    private route: ActivatedRoute,
    public router: Router,
    private navCtrl: NavController,
    private location: Location,
    private loadingCtrl: LoadingController,
    private sg: SimpleGlobal,
    private translate: FarmeasyTranslate,
    private zone:NgZone,
    public popoverController: PopoverController) { }

  ngOnInit() {
    this.createHeader();
    this.route.params.subscribe(params => {
      this.categoryData = JSON.parse( params['category']);
      this.name = this.categoryData.name;
      this.address = this.categoryData.image;
      this.updateflag = true;
      this.headerDataEdit();
    });
  }
  headerDataEdit() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Edit Category';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'వర్గాన్ని సవరించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பகுதியைத் திருத்துக';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ವರ್ಗ ಸಂಪಾದಿಸಿ';
    }else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'श्रेणी संपादित करें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      back: '/farmeasy-view-category',
      button1: 'home',
      button1_action: '/farm-dashboard'
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.9)',
        'leftButtonName':'Update',
        'rightButtonName':'Delete',
        'leftButtonColor':'teal',
        'rightButtonColor':'teal',
        };
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Add Category';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'వర్గాన్ని జోడించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பகுதியைச் சேர்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ವರ್ಗ ಸೇರಿಸಿ';
    }else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'कैटेगरी जोड़े';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      back: '/farmeasy-view-category',
      button1: 'home',
      button1_action: '/farm-dashboard',
      };
      this.footerData ={
        'footerColor':'rgba(255, 255, 255, 0.8)',
        'middleButtonName':'Submit',
        'middleButtonColor':'teal',
        };
  }
  chooseFile() {
    document.getElementById('image').click();
  }
  async upload(event) {
    this.sg['uploadProgress'] = 1
    this.sg['transferBytes'] = 0
    this.sg['totalBytes'] = 0
    const data = event.target.files[0];
    if (data.type === 'image/jpeg' || data.type === 'image/png' || data.type === 'image/jpg') {
      let that;
      that = this;
      const popover = await this.popoverController.create({
            component: ProgressBarComponent,
            translucent: true,
            backdropDismiss:false,
            cssClass:'progress-bar-popover',
            componentProps: {
                 'source':'Image'
             },
            });
            await popover.present();
            popover.onDidDismiss().then(resp =>{
              if(resp.data == 'Close'){
              this.sg['currentFile'].cancel();
              return true;
              }
            });
      this.sg['currentFile'] = firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/' + data.name);
      firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/' + data.name).put(data).then(function(snapshot) {
        firebase.storage().ref(that.sg['userdata'].vendor_id+'/farmeasy/taskManager/' +  data.name).getDownloadURL().then(function(url) {
        popover.dismiss();
        that.address = url;
        that.validate();
        });
      });
      that.zone.run(() => {
          firebase.storage().ref(this.sg['userdata'].vendor_id+'/farmeasy/taskManager/' + data.name).put(data).on('state_changed', (snapshot) => {
            that.sg['uploadProgress'] = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
             that.sg['transferBytes'] = snapshot.bytesTransferred;
            that.sg['totalBytes'] = snapshot.totalBytes;
            // alert(that.sg['uploadProgress']);
            });
          });
    } else {
      const alert = await this.alertController.create({
        message: 'Please Select Image',
        buttons: ['OK']
      });
      return await alert.present();
    }
    this.validate();
  }
  submit() {
    let that;
    that = this;
    const body = { name: this.name,
                   image: this.address,
                  };
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories/').push(body).then(function(snapshot) {
      that.location.back();
      // that.router.navigate(['/farmeasy-view-category']);
      });
  }
  update() {
    let that;
    that = this;
    const body = { name: this.name,
      image: this.address,
      };
      firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories/' + this.categoryData.key).update(body).then(function(snapshot) {
        that.location.back();
        // that.router.navigate(['/farmeasy-view-category']);
      });
  }
  validate(){
    if(this.name && this.address.length>0){
      this.disable = false;
    }
  }
  async deleteCategory() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are You Sure Want to Delete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/categories/' + this.categoryData.key).remove().then(() => {
              this.location.back();
              // this.navCtrl.navigateBack(['/farmeasy-view-category']);
            });
          }
        }
      ]
    });
    await alert.present();
  }
 async validateData() {
   let error= [];
    if (!this.name) {
      error.push('username');
    }
    if (!this.address) {
      error.push('profile pic');
    }
    if (error.length !== 0) {
      const alert = await this.alertController.create({
        subHeader: 'Please fill manditory fields',
        message: error.join(','),
        buttons: ['OK']
      });
      return await alert.present();
  }  else {
    if (this.updateflag) {
        this.update();
    } else {
      this.submit();
    }
  }
}

}
