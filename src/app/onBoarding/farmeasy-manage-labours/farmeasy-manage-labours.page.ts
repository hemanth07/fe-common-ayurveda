import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-farmeasy-manage-labours',
  templateUrl: './farmeasy-manage-labours.page.html',
  styleUrls: ['./farmeasy-manage-labours.page.scss'],
})
export class FarmeasyManageLaboursPage implements OnInit {
  headerData: any = [];
  admin: any = [];
  supervisor: any = [];
  labours: any = [];
  adminLabel: string;
  supervisorLabel: string;
  labourLabel: string;
  Employee_data: any = [];
  Employee_data_temp: any = [];
  showlistflag: boolean;
  searchkeys: string;
  labels: any = {};
  constructor( private loadingCtrl: LoadingController,
              private navCtrl:NavController,
              public router: Router,
              private sg: SimpleGlobal,
              private translate: FarmeasyTranslate) { }

  ngOnInit() {
    this.createHeader();
    this.getusers();
    this.showlistflag = true;
  }
  createHeader() {
    // color: green,orange,purple
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Employee List';
      this.labels['search'] = 'Filter Employees';
      this.labels['list'] = 'Switch to List View ';
      this.labels['grid'] = 'Switch to Grid View';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'ఉద్యోగుల జాబితా';
      this.labels['search'] = 'ఉద్యోగుల ఫిల్టర్';
      this.labels['list'] = 'జాబితా వీక్షణకు మారండి';
      this.labels['grid'] = 'గ్రిడ్ వీక్షణకు మారండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பணியாளர் பட்டியல்';
      this.labels['search'] = 'வடிகட்டி ஊழியர்கள்';
      this.labels['list'] = 'பட்டியல் காட்சிக்கு மாறவும்';
      this.labels['grid'] = 'கிரிட் பார்வைக்கு மாறவும்';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ನೌಕರರ ಪಟ್ಟಿ';
      this.labels['search'] = 'ಫಿಲ್ಟರ್ ನೌಕರರು';
      this.labels['list'] = 'ಪಟ್ಟಿ ವೀಕ್ಷಣೆಗೆ ಬದಲಿಸಿ';
      this.labels['grid'] = 'ಗ್ರಿಡ್ ವೀಕ್ಷಣೆಗೆ ಬದಲಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'कर्मचारी सूची';
      this.labels['search'] = 'कर्मचारियों को फ़िल्टर करें';
      this.labels['list'] = 'सूची दृश्य पर स्विच करें';
      this.labels['grid'] = 'ग्रिड दृश्य पर स्विच करें';
    }
    this.headerData = {
      color: 'green',
      title: title,
      button1: 'add-circle-outline',
      button1_action: 'farmeasy-add-users',
      button2: 'home',
      button2_action: '/farm-dashboard',
      };
   }
  selectLabour(data) {
     const body = { user: JSON.stringify(data)};
     this.navCtrl.navigateForward(['/farmeasy-add-users/', body]);
    // this.router.navigate(['/farmeasy-add-users/', body]);
  }
 async getusers() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/users').on('value', resp => {
      const data = snapshotToArray(resp);
      this.Employee_data_temp = data;
      this.Employee_data = data;
      this.labours = data.filter(item => {if (item.role === 'employee') {return true; }});
      this.admin = data.filter(item => {if (this.sg['userdata'].role === 'corporate' || this.sg['userdata'].role === 'accounts' || this.sg['userdata'].role === 'manager') {return true; }});
      this.supervisor = data.filter(item => {if (item.role === 'supervisor') {return true; }});
      loading.dismiss();
    });
  }
  showgrid(){
    this.showlistflag = false;
  }
  showlist(){
    this.showlistflag = true;
  }
  search(){
    this.Employee_data = this.Employee_data_temp.filter(emp => {
      if (emp.name.toUpperCase().includes(this.searchkeys.toUpperCase())){
        return true;
      }
    });
    if(this.searchkeys == ''){
      this.Employee_data = this.Employee_data_temp;
    }
  }
  clear(){
     this.Employee_data = this.Employee_data_temp;
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
