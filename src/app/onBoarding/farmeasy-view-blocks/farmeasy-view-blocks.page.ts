import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-farmeasy-view-blocks',
  templateUrl: './farmeasy-view-blocks.page.html',
  styleUrls: ['./farmeasy-view-blocks.page.scss'],
})
export class FarmeasyViewBlocksPage implements OnInit {
  headerData: any = [];
  blocksData: any = [];
  constructor(
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    public router: Router,
    private sg: SimpleGlobal,
    private translate: FarmeasyTranslate) { }

  ngOnInit() {
    this.createHeader();
    this.getBlocks();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Manage Blocks';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'బ్లాక్స్ నిర్వహించండి';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'பிளாக்ஸ் நிர்வகி';
    } else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ನಿರ್ಬಂಧಗಳನ್ನು ನಿರ್ವಹಿಸಿ';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'ब्लॉक प्रबंधित करें';
    }
    this.headerData = {
      color: 'teal',
      title: title,
      button1: 'add-circle-outline',
      button1_action: 'farmeasy-add-blocks',
      button2: 'home',
      button2_action: '/farm-dashboard',
     
      };
   }
  selectblock(block) {
    const body = { block: JSON.stringify(block)};
     this.navCtrl.navigateBack(['/farmeasy-add-blocks/', body]);
    //  this.router.navigate(['/farmeasy-add-blocks/', body], { replaceUrl : true});
  }
  async getBlocks() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/lovs/blocks').on('value', resp => {
      const data = snapshotToArray(resp);
      this.blocksData = data;
      loading.dismiss();
    });
  }
}

export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
