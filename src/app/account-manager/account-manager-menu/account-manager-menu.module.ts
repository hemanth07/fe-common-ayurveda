import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountManagerMenuPage } from './account-manager-menu.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';

const routes: Routes = [
  {
    path: '',
    component: AccountManagerMenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservationComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccountManagerMenuPage]
})
export class AccountManagerMenuPageModule {}
