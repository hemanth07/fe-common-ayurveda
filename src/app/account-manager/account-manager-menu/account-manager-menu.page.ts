import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { OnInit, Component, ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';
import {SimpleGlobal} from 'ng2-simple-global';
import * as firebase from 'firebase';
import { AlertController, LoadingController , NavController} from '@ionic/angular';
import { FarmeasyTranslate } from '../../translate.service';

@Component({
  selector: 'app-account-manager-menu',
  templateUrl: './account-manager-menu.page.html',
  styleUrls: ['./account-manager-menu.page.scss'],
})
export class AccountManagerMenuPage implements OnInit {
  headerData: any = [];
  menuData: any = [];
  constructor(private sg: SimpleGlobal, private loadingCtrl: LoadingController,private navCtrl:NavController,
    private translate: FarmeasyTranslate,
    private resolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private en: EnglishService,
    private hi: HindiService,
    private ta: TamilService,
    private te: TeluguService,
    private kn: KanadaService
    ) { }
  ngOnInit() {
    this.translation();
    this.createHeader();
  }
  createHeader() {
    // color: green,orange,purple
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Accounts/Finance';
    } else if (this.sg['userdata'].primary_language === 'te'){
      title = 'అకౌంట్స్ / ఫైనాన్స్';
    }else if (this.sg['userdata'].primary_language === 'ta'){
      title = 'கணக்குகள் / நிதி';
    }else if (this.sg['userdata'].primary_language === 'kn'){
      title = 'ಖಾತೆಗಳು / ಹಣಕಾಸು';
    } else if (this.sg['userdata'].primary_language === 'hi'){
      title = 'लेखा / वित्त';
    }
    this.headerData = {
      color: 'blue',
      title: title,
      button1: 'home',
      button1_action: '/farm-dashboard'      
      };
  }
 translation(){
  if(this.sg['userdata'].primary_language === 'en'){
    this.menuData  = this.en.getAccountsManagerLabels();
  } else if(this.sg['userdata'].primary_language === 'te'){
    this.menuData  = this.te.getAccountsManagerLabels();
  } else if(this.sg['userdata'].primary_language === 'ta'){
    this.menuData  = this.ta.getAccountsManagerLabels();
  } else if(this.sg['userdata'].primary_language === 'kn'){
    this.menuData  = this.kn.getAccountsManagerLabels();
  }else if(this.sg['userdata'].primary_language === 'hi'){
    this.menuData  = this.hi.getAccountsManagerLabels();
  } else {
     this.menuData  = this.en.getAccountsManagerLabels();
  }
}
selectedTask(task) {
  this.sg["type"] = task.keyword;
  this.navCtrl.navigateForward(task.action);
  }
}
