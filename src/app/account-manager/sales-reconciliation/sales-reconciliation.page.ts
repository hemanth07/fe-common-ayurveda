import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavController,ToastController,LoadingController, AlertController,Platform } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { OtherService } from '../../languages/others.service';
import { PopoverController } from '@ionic/angular';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component'
import { OverlayEventDetail } from '@ionic/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SalesReceiptComponent } from '../../components/accounts-components/sales-receipt/sales-receipt.component';
import { SalesInvoiceDownloadComponent } from '../../components/accounts-components/sales-invoice-download/sales-invoice-download.component';
import { SalesReconciliationBreakupComponent } from '../../components/accounts-components/sales-reconciliation-breakup/sales-reconciliation-breakup.component';
import { File, IWriteOptions } from "@ionic-native/file/ngx";
import { FileOpener } from '@ionic-native/file-opener/ngx';
import {  FileTransfer,  FileUploadOptions,  FileTransferObject} from "@ionic-native/file-transfer/ngx";
import { Device } from "@ionic-native/device/ngx";

@Component({
  selector: 'app-sales-reconciliation',
  templateUrl: './sales-reconciliation.page.html',
  styleUrls: ['./sales-reconciliation.page.scss'],
})
export class SalesReconciliationPage implements OnInit {
  headerData:any;
  fromOrTodate:any;
  toDate:any;
  fromDate:any;
  color:any = '#2174E1';
  salesList:any = [];
  TempSalesList:any = [];
  count:number = 10;
  previousLen:number = 0;
  displaySalesList:any = []
  constructor(public navCtrl: NavController,
    private popoverController:PopoverController,
    public loadingCtrl: LoadingController,
    public router: Router,
    private sg: SimpleGlobal,
    private location:Location,
    private translate:FarmeasyTranslate,
    private en: EnglishService,
    private hi: HindiService,
    private ta:TamilService,
    private te:TeluguService,
    private kn:KanadaService,
    public toastController: ToastController,
    public alertController:AlertController,
    private socialSharing:SocialSharing,
    private other: OtherService,
    private platform:Platform,
    private file:File,
    private fileOpener:FileOpener,
    private transfer: FileTransfer,
    private device: Device,
    ) { }

  ngOnInit() {
    this.createHeader();
    this.getSalesList();
  }
  createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Sales Reconciliation';
    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಮಾರಾಟ ಸಾಮರಸ್ಯ';
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'விற்பனை நல்லிணக்கம்';
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'అమ్మకాల సయోధ్య';
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'बिक्री सुलह';
    }
    this.headerData = {
          color: 'blue',
          title: title,
          button1: 'home',
          button1_action: '/farm-dashboard'
    };
  }
  async openDate(val) {
    this.fromOrTodate = val;
    let popover = await this.popoverController.create({
      component: CalendercomponentComponent,
      cssClass: "popover_class",
      componentProps: {'previous': true},
    });
    popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      this.getSelectedDate(detail.data);
    });
    await popover.present();
  }
  getSelectedDate(val) {
    console.log(this.fromOrTodate);
    if (this.fromOrTodate == 'toDate') {
      this.toDate = val;
      console.log(this.toDate);
    }
    if (this.fromOrTodate == 'fromDate') {
      this.fromDate = val;
      console.log(this.fromDate);
    }
  }
  searchData(val) {
    if(this.toDate && this.fromDate){
      let toDate = new Date(this.toDate).getTime();
      let fromDate = new Date(this.fromDate).getTime();
      this.salesList = this.TempSalesList.filter((item)=> item.time>=fromDate && item.time<=toDate);
      this.displaySalesList = [];
      this.count = this.salesList.length<10?this.salesList.length:10;
      for(let i=0;i<this.count;i++){
        this.displaySalesList.push(this.salesList[i]);
      }
    }
  }

  async getSalesList(){
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    firebase.database().ref(this.sg['userdata'].vendor_id+'/harvest/harvest_out').on('value',resp=>{
      this.displaySalesList = [];
        let temp = snapshotToArray(resp).reverse();
        temp.map((item,i)=> { temp[i]['time'] = new Date(temp[i].Date_of_Transaction).getTime()});
        this.salesList = temp;
        this.TempSalesList = this.salesList;
        this.count = this.salesList.length<10?this.salesList.length:10;
        for(let i=0;i<this.count;i++){
          this.displaySalesList.push(this.salesList[i]);
        }
        loading.dismiss();
      });
    }
    async loadData(event) {
      const loading = await this.loadingCtrl.create({
       spinner: 'bubbles',
       message: 'Please wait...',
       translucent: true,
       cssClass: 'custom-class custom-loading'
     });
     await loading.present();
     this.previousLen = this.count;
     this.count = this.count+10;
     if(this.salesList.length<=this.count){
       this.count = this.salesList.length;
     }
     if(this.previousLen<this.count){
       for(let i=this.previousLen ; i< this.count;i++){
       this.displaySalesList.push(this.salesList[i]);
       }
     }
     // console.log(this.displayTaskData);
      setTimeout(() => {
         loading.dismiss();
       }, 1000);
     setTimeout(() => {
       console.log('Done');
       event.target.complete();
       }, 500);
   }
    calaculateAmount(data){
      let amount = 0;
      for(let item of data){
          amount = amount + item.Rate
      }
      return amount.toLocaleString();
    }
    dateConverter(date){
      if(date){
        let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
    } else {
      return 'DD-Mon-YYYY';
    }
    }
   async printinvoice(invoice){
    console.log(invoice);
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
      //   let popover = await this.popoverController.create({
      //   component: SalesInvoiceDownloadComponent,
      //   cssClass: "harvestvoice-popup",
      //   componentProps: {'receipt': JSON.stringify(invoice)},
      // });
      // popover.onDidDismiss().then((detail: OverlayEventDetail) => {
      //   console.log(detail);
      // });
      // await popover.present();
      for(let i=0;i<invoice.record.length;i++){
        invoice.record[i]['Price'] = parseInt(invoice.record[i]['Quantity']) * parseInt(invoice.record[i]['Rate']);
      }
      let body = {
        "customer": {"date":this.dateConverter(invoice.Date_of_Transaction),"bill_no":"#"+invoice.short_name+"-"+invoice.Bill_no,"name" : "Srini Food Park", "address_line1":"Mallikarjunarao Karge, 121,Chittoor Main Road","address_line2":"Andhra Pradesh, Chittor"},
        "items": invoice.record
      };
     this.other.getInvoice(body).then(resp =>{
      let that = this;
        console.log(resp);
        let storageLocation
        let filename = invoice.short_name+"-"+invoice.Bill_no+".pdf";
    firebase.storage().ref('invoice.pdf').putString(resp['_body'],'base64').then(()=>{
      firebase.storage().ref('invoice.pdf').getDownloadURL().then(function (url) {
        switch (that.device.platform) {
          case "Android":
            storageLocation = "file:///storage/emulated/0/Download/Reports/"+filename;
            break;
          case "iOS":
            storageLocation = that.file.documentsDirectory+filename;
            break;
        }
        console.log(url);
        const fileTransfer: FileTransferObject = that.transfer.create();
        // alert(url);
        // alert(storageLocation);
        fileTransfer.download(url, storageLocation).then(
          entry => {
            // alert("downloaded");
            loading.dismiss();
            that.other.openFile(storageLocation);
          },
          error => {
            loading.dismiss();
            alert('Something Went Wrong, Please check permissions and try again');
          }
        );
      });
  });
  console.log(resp);
  });
}
      async comformationPopUp(invoice){
        const alert = await this.alertController.create({
          header: 'Confirm!',
          message: 'Are you sure want to print Invoice',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('Confirm Cancel: blah');
              }
            }, {
              text: 'Okay',
              handler: () => {
                this.printinvoice(invoice)
              }
            }
          ]
        });
    
        await alert.present();
    }
    async presentAlertPrompt() {
      const alert = await this.alertController.create({
        header: 'Create Event',
        inputs: [
          {
            name: 'event_name',
            type: 'text',
            placeholder: 'Event Name'
          },
          {
            name: 'description',
            type: 'text',
            placeholder: 'Description'
          },
          {
            name: 'date',
            type: 'date',
            placeholder: 'Date'
          },
        ],
        buttons: [
          {
            text: 'Open Calendar',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Calendar');
              this.openCalendar();
            }
          },{
            text: 'Create Event',
            handler: (data) => {
              console.log('Confirm Ok');
              console.log(data);
              this.createEvent(data.event_name,data.description,data.date);
            }
          }
        ]
      });
  
      await alert.present();
    }
    openCalendar(){
      let date = new Date();
      // this.calendar.openCalendar(date);
    }
    createEvent(eventname,description,date){
      // this.calendar.createEvent(eventname,'Cloud Express Solutions',description,date,date);
    } 
    shareVia() {
      if(this.platform.is('ios')) {
        this.socialSharing.share('“Invoice Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
        .then(()=>{
           // alert('Done');
        }).catch((err)=>{
          alert('error :'+JSON.stringify(err));
        });
      } else {
        this.socialSharing.share('“Invoice Shared from FarmEasy” Click here to download the app', null,null,'URL will be soon')
        .then(()=>{
       //     alert('Done');
        }).catch((err)=>{
          alert('error :'+JSON.stringify(err));
        });
      }
     
      }
     async openReceipt(card){
        let popover = await this.popoverController.create({
          component: SalesReceiptComponent,
          cssClass: "popover_class",
          componentProps: {'receipt': JSON.stringify(card)},
        });
        popover.onDidDismiss().then((detail: OverlayEventDetail) => {
          console.log(detail);
        });
        await popover.present();
      }
      async cash(card){
        let popover = await this.popoverController.create({
          component: SalesReconciliationBreakupComponent,
          cssClass: "popover_class",
          componentProps: {'receipt': JSON.stringify(card)},
        });
        popover.onDidDismiss().then((detail: OverlayEventDetail) => {
          console.log(detail);
        });
        await popover.present();
      }

}


export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};
