import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SalesReconciliationPage } from './sales-reconciliation.page';
import { ObservationComponentsModule } from '../../components/observation-components/observation-components.module';
import { TaskManagerComponentsModule } from '../../components/task-manger-components/task-mananger-components.module';
import { HarvestcomponentsModule} from '../../components/harvestcomponents/harvestcomponents.module';
import { AccountsComponentsModule } from '../../components/accounts-components/accounts-components.module';
const routes: Routes = [
  {
    path: '',
    component: SalesReconciliationPage
  }
];

@NgModule({
  imports: [
    ObservationComponentsModule,
    TaskManagerComponentsModule,
    AccountsComponentsModule,
    HarvestcomponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SalesReconciliationPage]
})
export class SalesReconciliationPageModule {}
