import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavController,ToastController,PopoverController,LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router, Route } from '@angular/router';
import {SimpleGlobal} from 'ng2-simple-global';
import { FarmeasyTranslate } from '../../translate.service';
import { KanadaService } from './../../languages/kanada.service';
import { TeluguService } from './../../languages/telugu.service';
import { TamilService } from './../../languages/tamil.service';
import { EnglishService } from '../../languages/english.service';
import { HindiService } from '../../languages/hindi.service';
import { CalendercomponentComponent } from '../../components/harvestcomponents/calendercomponent/calendercomponent.component';

@Component({
  selector: 'app-salary-advances',
  templateUrl: './salary-advances.page.html',
  styleUrls: ['./salary-advances.page.scss'],
})
export class SalaryAdvancesPage implements OnInit {
  headerData: any = [];
  labourData: any = [];
  labourNames: any = [];
  category: string = "Salary Payment";
  expense_category: string;
  labourname:string;
  month_sal:string;
  adv:string;
  new_adv:string;
  submit_btn:string;
  date:any;
  month:any;
  year:any;
  labels:any;
  months:any=   [{name:'January',code:'Jan'},
                {name:'February',code:'Feb'},
                {name:'March',code:'Mar'},
                {name:'April',code:'Apr'},
                {name:'May',code:'May'},
                {name:'June',code:'Jun'},
                {name:'July',code:'Jul'},
                {name:'August',code:'Aug'},
                {name:'September',code:'Sep'},
                {name:'October',code:'Oct'},
                {name:'November',code:'Nov'},
                {name:'December',code:'Dec'}];

  constructor(public navCtrl: NavController,
              public router: Router,
              private sg: SimpleGlobal,
              private location:Location,
              private translate:FarmeasyTranslate,
              private popoverController: PopoverController,
              private loadingCtrl: LoadingController,
              private en: EnglishService,
              private hi: HindiService,
              private ta:TamilService,
              private te:TeluguService,
              private kn:KanadaService,
              public toastController: ToastController) {
                  this.createHeader();
                  this.createlabours();
              }
  ngOnInit() {
    this.createHeader();
    this.translations();
    this.createlabours();
  }
 async createlabours() {
   this.date = new Date().toISOString().split("T")[0];
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    this.labourData = [];
    firebase.database().ref(this.sg['userdata'].vendor_id+'/salary').once('value', resp => {
      this.labourNames = snapshotToArray(resp);
      this.labourNames.map((item,idx)=>{
        if(item.role === "supervisor" || item.role === 'employee'){
          let loan = [];
          if(item.loan){
            loan = this.convertObjectToArray(item.loan);
          } else {
            loan = [];
          }
          let temp =  {labourPhone:item.phone,labour_loan:loan,labourName:item.name,profile_url:item.profile_url,role:item.role, monthlySal: item.salary,recover:'',deduction:'',net_paid:'', key:item.key};
          this.labourData.push(temp);
        }
      });
      this.labourData = this.labourData.filter((thing, index, self) =>
  index === self.findIndex((t) => (
    t.labourPhone === thing.labourPhone && t.labourName === thing.labourName
  ))
)
this.translating(this.labourData);
loading.dismiss();
    });
   }
     convertObjectToArray(obj){
    let array1 = Object.entries(obj);
   let original_array = [];
    for(let array2 of array1){
        let obj1 = array2[1];
        obj1['key'] = array2[0]
        original_array.push(obj1);
    }
   return original_array;
  }
   async pickDate() {
    const popover = await this.popoverController.create({
         component: CalendercomponentComponent,
         cssClass: 'popoverclass',
         componentProps: {'previous': true},
     });
     popover.onDidDismiss().then((data) => {
       //  console.log(data);
          this.date = data.data;
     });
     await popover.present();
}
   createHeader() {
    let title;
    if(this.sg['userdata'].primary_language === 'en'){
      title = 'Salary Advances';
      this.labels = {
        salary_payment:"Salary Payment",
        date_of_payment:"Date of Payment",
        month:"Month",
        year:"Year",
        placeholder:" - Select -",
        labour_name:"Labour Name",
        monthly_sal:"Monthly Sal",
        recovered:"Recovered",
        deductions:"Deductions",
        net_paid:"Net Paid"
      }
      this.months = [{name:'January',code:'Jan'},
      {name:'February',code:'Feb'},
      {name:'March',code:'Mar'},
      {name:'April',code:'Apr'},
      {name:'May',code:'May'},
      {name:'June',code:'Jun'},
      {name:'July',code:'Jul'},
      {name:'August',code:'Aug'},
      {name:'September',code:'Sep'},
      {name:'October',code:'Oct'},
      {name:'November',code:'Nov'},
      {name:'December',code:'Dec'}];

    } else if(this.sg['userdata'].primary_language === 'kn'){
      title = 'ಸಂಬಳ ಮುಂಗಡ';
      this.labels = {
        "salary_payment": "ಸಂಬಳ ಪಾವತಿ",
        "date_of_payment": "ಪಾವತಿ ದಿನಾಂಕ",
        "month": "ತಿಂಗಳು",
        "year": "ವರ್ಷ",
        "placeholder": " - ಆಯ್ಕೆ ಮಾಡಿ -",
        "labour_name": "ಲೇಬರ್ ಹೆಸರು",
        "monthly_sal": "ಮಾಸಿಕ ಸಾಲ್",
        "recovered": "ಚೇತರಿಸಿಕೊಂಡ",
        "deductions": "ಕಳೆಯುವಿಕೆಗಳು",
        "net_paid": "ನೆಟ್ ಪಾವತಿಸಿದ"
      };
      this.months = [{name:'ಜನವರಿ',code:'Jan'},
      {name:'ಫೆಬ್ರವರಿ',code:'Feb'},
      {name:'ಮಾರ್ಚ್',code:'Mar'},
      {name:'ಏಪ್ರಿಲ್',code:'Apr'},
      {name:'ಮೇ',code:'May'},
      {name:'ಜೂನ್',code:'Jun'},
      {name:'ಜುಲೈ',code:'Jul'},
      {name:'ಆಗಸ್ಟ್',code:'Aug'},
      {name:'ಸೆಪ್ಟೆಂಬರ್',code:'Sep'},
      {name:'ಅಕ್ಟೋಬರ್',code:'Oct'},
      {name:'ನವೆಂಬರ್',code:'Nov'},
      {name:'ಡಿಸೆಂಬರ್',code:'Dec'}];
    } else if(this.sg['userdata'].primary_language === 'ta'){
      title = 'சம்பள முன்னேற்றம்';
      this.labels ={
        "salary_payment": "சம்பளம் கொடுப்பனவு",
        "date_of_payment": "கொடுப்பனவு தேதி",
        "month": "மாதம்",
        "year": "ஆண்டு",
        "placeholder": " - தேர்வு -",
        "labour_name": "தொழிலாளர் பெயர்",
        "monthly_sal": "மாதாந்திர சால்",
        "recovered": "மீட்கப்பட்ட",
        "deductions": "விலக்கிற்கு",
        "net_paid": "நிகர பணம்"
      };
      this.months = [{name:'ஜனவரி',code:'Jan'},
      {name:'பிப்ரவரி',code:'Feb'},
      {name:'மார்ச்',code:'Mar'},
      {name:'ஏப்ரல்',code:'Apr'},
      {name:'மே',code:'May'},
      {name:'ஜூன்',code:'Jun'},
      {name:'ஜூலை',code:'Jul'},
      {name:'ஆகஸ்ட்',code:'Aug'},
      {name:'செப்டம்பர்',code:'Sep'},
      {name:'அக்டோபர்',code:'Oct'},
      {name:'நவம்பர்',code:'Nov'},
      {name:'டிசம்பர்',code:'Dec'}];
    } else if(this.sg['userdata'].primary_language === 'te'){
      title = 'జీతం అడ్వాన్సెస్';
      this.labels = {
        "salary_payment": "జీతం చెల్లింపు",
        "date_of_payment": "చెల్లింపు తేదీ",
        "month": "నెల",
        "year": "ఇయర్",
        "placeholder": " - ఎంచుకోండి -",
        "labour_name": "లేబర్ పేరు",
        "monthly_sal": "మంత్లీ సాల్",
        "recovered": "కోలుకున్న",
        "deductions": "తగ్గింపులకు",
        "net_paid": "నికర చెల్లింపు"
      };
      this.months = [{name:'జనవరి',code:'Jan'},
      {name:'ఫిబ్రవరి',code:'Feb'},
      {name:'మార్చి',code:'Mar'},
      {name:'ఏప్రిల్',code:'Apr'},
      {name:'మే',code:'May'},
      {name:'జూన్',code:'Jun'},
      {name:'జూలై',code:'Jul'},
      {name:'ఆగస్టు',code:'Aug'},
      {name:'సెప్టెంబర్',code:'Sep'},
      {name:'అక్టోబర్',code:'Oct'},
      {name:'నవంబర్',code:'Nov'},
      {name:'డిసెంబర్',code:'Dec'}];
    } else if(this.sg['userdata'].primary_language === 'hi'){
      title = 'वेतन अग्रिम';
      this.labels = {
        "salary_payment": "तनख्वाह का भुगतान",
        "date_of_payment": "भुगतान की तिथि",
        "month": "महीना",
        "year": "साल",
        "placeholder": " - चुनते हैं -",
        "labour_name": "श्रम नाम",
        "monthly_sal": "मासिक साल",
        "recovered": "बरामद",
        "deductions": "कटौती",
        "net_paid": "नेट भुगतान किया"
      }
      this.months = [{name:'जनवरी',code:'Jan'},
      {name:'फरवरी',code:'Feb'},
      {name:'मार्च',code:'Mar'},
      {name:'अप्रैल',code:'Apr'},
      {name:'मई',code:'May'},
      {name:'जून',code:'Jun'},
      {name:'जुलाई',code:'Jul'},
      {name:'अगस्त',code:'Aug'},
      {name:'सितंबर',code:'Sep'},
      {name:'अक्टूबर',code:'Oct'},
      {name:'नवंबर',code:'Nov'},
      {name:'दिसंबर',code:'Dec'}];
    }
    this.headerData = {
          color: 'blue',
          title: title,
          button1: 'home',
          button1_action: '/farm-dashboard'
           };
  }
  translations() {
    let data;
    if(this.sg['userdata'].primary_language === 'en'){
      data  = this.en.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'te'){
      data  = this.te.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      data  = this.ta.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      data  = this.kn.getLabourAdvance();
    } else if(this.sg['userdata'].primary_language === 'hi'){
      data  = this.hi.getLabourAdvance();
    } else {
      data  = this.en.getLabourAdvance();
    }
      this.expense_category = data[0];
      this.labourname = data[1];
      this.month_sal = data[2];
      this.adv = data[3];
      this.new_adv = data[4];
      this.submit_btn = data[5];
  }
  // addsomeMore(data, idx) {
  //   if(data.detail.value.name){
  //     this.labourData[idx].labourName = data.detail.value.name;
  //     this.labourData[idx].monthlySal = data.detail.value.salary;
  //     this.labourData[idx].advPaid = data.detail.value.advance_sal;
  //     this.labourData[idx].labourPhone = data.detail.value.phone;
  //     this.labourData[idx].key = data.detail.value.key;
  //   }
  //   if (idx >= this.labourData.length-1) {
  //     const body = {labourPhone:'', labourName: '', monthlySal:0,recover:'',deduction:'',net_paid:'', key:''};
  //     this.labourData.push(body);
  //   }
  // }
  translating(data) {
    let temp = data
    let menu = [];
    for (let item of data) {
      let obj = {
        name: item.labourName,
        role: item.role
      }
      menu.push(obj)
    }
    for (let i = 0; i < menu.length; i++) {
      if (this.sg['userdata'].primary_language !== 'en') {
        this.translate.translateObject(menu[i],'en',this.sg['userdata'].primary_language).then(data => {
          this.labourData[i]['labourName'] = data['name'];
          this.labourData[i]['role'] = data['role']
        });
      } else {
        this.labourData[i]['labourName']= menu[i]['name'];
        this.labourData[i]['role'] = menu[i]['role'];
      }
    }
  }

  storeSalary(data, idx) {
    this.labourData[idx].monthlySal = data.target.value;
  }
  recovered(data, idx) {
    this.labourData[idx].recover = data.target.value;
    let salary = this.labourData[idx].monthlySal;
    let recover = this.labourData[idx].recover;
    let deduction;
    if(this.labourData[idx].deduction){
      deduction = this.labourData[idx].deduction;
    } else {
      deduction = 0
    }
   this.labourData[idx].net_paid = parseInt(salary) - (parseInt(recover) + parseInt(deduction));

  }
  dateConverter(date){
    let d = new Date(date);
    let day = d.getDate();
    let month = d.getMonth();
    let year = d.getFullYear();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return day+"-"+months[month]+"-"+year;
  }
  deductions(data, idx) {
    this.labourData[idx].deduction = data.target.value;
    let salary = this.labourData[idx].monthlySal;
    let recover;
     if(this.labourData[idx].recover){
      recover = this.labourData[idx].recover;
    } else {
      recover = 0
    }
    let deduction;
    if(this.labourData[idx].deduction){
      deduction = this.labourData[idx].deduction;
    } else {
      deduction = 0
    }
    this.labourData[idx].net_paid = parseInt(salary) - (parseInt(recover) + parseInt(deduction));
  }
  netPaid(data, idx) {
    this.labourData[idx].net_paid = data.target.value;
  }
  
  async submit() {
    let errors = []
    // let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    // let month = months[new Date().getMonth()];
    // let year = new Date().getFullYear().toString().substr(2, 2);
    let year = this.year.toString().substr(2, 2);
    let month = this.month;
    let key = month+"-"+year;
    let date = new Date(this.date).toISOString().split("T")[0];
    const temp = this.labourData.filter(item => { if(item.labourName.length>1 && item.net_paid>=0){return true;}});
    this.labourData = temp;
    let total_amount = 0;
    for(let labour of this.labourData){
      if(labour.net_paid >0){
      total_amount = total_amount + parseInt(labour.net_paid);
      }
    }
    console.log(this.labourData);
    firebase.database().ref(this.sg['userdata'].vendor_id+'/reports/salary/'+key).push({date:new Date().getTime(),amount:total_amount,given_by:this.sg['userdata'].farmeasy_id});
   for(let labour of this.labourData){
     if(labour.labour_loan.length>0){
     let loan_key =  labour.labour_loan[labour.labour_loan.length-1].key
     let loan_amount = labour.labour_loan[labour.labour_loan.length-1]
     let balance = loan_amount.balance - labour.recover
     firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+labour.key+'/loan/'+loan_key).update({balance:balance});
     firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+labour.key+'/loan/'+loan_key+'/recovered').push({date:date,amount:labour.recover});
     }
     firebase.database().ref(this.sg['userdata'].vendor_id+'/salary/'+labour.key+'/money_paid/'+key).update({date:date,recovered:labour.recover,deduction:labour.deduction,net_paid:labour.net_paid})
   }
    if(this.labourData.length>0){
      const toast = await this.toastController.create({
        message: 'Labours Advances Updated',
        duration: 2000
      });
      toast.present();
       this.location.back();
    } else {
      this.createlabours()
    }
  }
}
export const snapshotToArray = snapshot => {
  const returnArr = [];
  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
  };