import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SimpleGlobal } from 'ng2-simple-global';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FarmeasyTranslate {
  data: string;
  translate_url:string = "http://104.237.2.124:3001/translate";
  // translate_url:string = "http://localhost:3000/translate";
  constructor(private http: Http, private sg: SimpleGlobal) { }

  reverieTranslateObject(body, target) {
    let target_lang;
    if (target == 'te') {
      target_lang = 'telugu';
    } else if (target == 'hi') {
      target_lang = 'hindi';
    } else if (target == 'ta') {
      target_lang = 'tamil';
    } else if (target == 'kn') {
      target_lang = 'kannada';
    }
    let keysname = Object.keys(body);
    let bodyData = []
    let arrayItem;
    let data;
    for (let item of keysname) {
      arrayItem = body[item] ? body[item] : "";

      bodyData.push(arrayItem);
    }
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({
      headers: headers
    });
    const reverie_url = "https://api-gw.revup.reverieinc.com/apiman-gateway/Farmeasy/localization/1.0?source_lang=english&target_lang=" + target_lang + "&domain=3&mt_context=generic_english_proper"
    return new Promise(resolve => {
      this.http.post(this.translate_url, {data:{data: bodyData},url:reverie_url}, options).pipe(map(res => res.json()))
        .subscribe(response => {
          let temp1;
          let temp: string = '';
          for (let i = 0; i < keysname.length; i++) {

            if (i == keysname.length - 1) {
              temp1 = '"' + keysname[i] + '":"' + response['responseList'][i].outString + '"';
            } else {
              temp1 = '"' + keysname[i] + '":"' + response['responseList'][i].outString + '",';
            }
            temp = temp + temp1;
          }
          data = JSON.parse('{' + temp + '}');
          // console.log(JSON.stringify(data));
          resolve(data);
        }, err => {
          resolve(body);
          //  console.log('something went wrong');
        });
    });
  }
  getDataFromFirebase(path) {
    return new Promise((resolve, reject) => {
      firebase.database().ref(path).once('value', resp => {
        let data = snapshotToArray(resp);
        resolve(data);
      }).catch(err => 
        {
          reject(err)
        });
    })
  }
  translateObject(body, source, target) {
    let data;
    let bodyData: string = '';
    let keysname = Object.keys(body);
    for (let item of keysname) {
      let temp1 = body[item];
      let temp = 'q=' + temp1 + '&';
      bodyData = bodyData + temp;
    }
    const url = 'https://translation.googleapis.com/language/translate/v2?' + bodyData
      + 'key=AIzaSyBLtAUorsS2O4yBOvhge00bkffAiUl8pag&target=' + target + '&format=text&source=' + source + '&model=base';
    return new Promise(resolve => {
      this.http.get(url).pipe(map(res => res.json()))
        .subscribe(response => {
          let temp: string = '';
          for (let i = 0; i < keysname.length; i++) {
            let temp1;
            if (i == keysname.length - 1) {
              temp1 = '"' + keysname[i] + '":"' + response['data'].translations[i].translatedText + '"';
            } else {
              temp1 = '"' + keysname[i] + '":"' + response['data'].translations[i].translatedText + '",';
            }
            temp = temp + temp1;
          }
          data = JSON.parse('{' + temp + '}');
          // console.log(JSON.stringify(data));
          resolve(data);
        }, err => {
          //  console.log('something went wrong');
        });
    });
  }
  translate(body, source, target) {
    let data;
    const url = 'https://translation.googleapis.com/language/translate/v2?q=' + body
      + '&key=AIzaSyBLtAUorsS2O4yBOvhge00bkffAiUl8pag&target=' + target + '&format=text&source=' + source + '&model=base';
    return new Promise(resolve => {
      this.http.get(url).pipe(map(res => res.json()))
        .subscribe(data => {
          resolve(data['data'].translations[0].translatedText);
        }, err => {
          resolve(body);
        });
    });
  }
  translateArray(body, source, target) {
    let bodyData: string = '';
    for (let i = 0; i < body.length; i++) {
      let temp1 = body[i];
      let temp = 'q=' + temp1 + '&';
      bodyData = bodyData + temp;
    }
    const url = 'https://translation.googleapis.com/language/translate/v2?' + bodyData
      + 'key=AIzaSyBLtAUorsS2O4yBOvhge00bkffAiUl8pag&target=' + target + '&format=text&source=' + source + '&model=base';
    return new Promise(resolve => {
      this.http.get(url).pipe(map(res => res.json()))
        .subscribe(response => {
          let data = response['data'].translations;
          let temp: any = [];
          for (const item of data) {
            temp.push(item.translatedText);
          }
          //console.log(JSON.stringify(temp));
          resolve(temp);
        });
    });
  }

  // reverieTranslateObject(body, target) {
  //   let target_lang;
  //   if (target == 'te') {
  //     target_lang = 'telugu';
  //   } else if (target == 'hi') {
  //     target_lang = 'hindi';
  //   } else if (target == 'ta') {
  //     target_lang = 'tamil';
  //   } else if (target == 'kn') {
  //     target_lang = 'kannada';
  //   }
  //   let keysname = Object.keys(body);
  //   let bodyData = []
  //   let arrayItem;
  //   let data;
  //   for (let item of keysname) {
  //     arrayItem = body[item] ? body[item] : "";

  //     bodyData.push(arrayItem);
  //   }
  //   let headers = new Headers();
  //   headers.append('REV-API-KEY', '63d0e8dcdd21f0d926b58aa4f5453b64');
  //   headers.append('REV-APP-ID', 'com.farmeasy');
  //   headers.append('Content-Type', 'application/json');
  //   headers.append('Cache-Control', 'no-cache');
  //   headers.append('Access-Control-Allow-Origin', '*');
  //   headers.append('Access-Control-Allow-Methods', 'POST');
  //   let options = new RequestOptions({
  //     headers: headers
  //   });
  //   const url = "https://api-gw.revup.reverieinc.com/apiman-gateway/Farmeasy/localization/1.0?source_lang=english&target_lang=" + target_lang + "&domain=3&mt_context=generic_english_proper"
  //   return new Promise(resolve => {
  //     this.http.post(url, { data: bodyData }, options).pipe(map(res => res.json()))
  //       .subscribe(response => {
  //         let temp1;
  //         let temp: string = '';
  //         for (let i = 0; i < keysname.length; i++) {

  //           if (i == keysname.length - 1) {
  //             temp1 = '"' + keysname[i] + '":"' + response['responseList'][i].outString + '"';
  //           } else {
  //             temp1 = '"' + keysname[i] + '":"' + response['responseList'][i].outString + '",';
  //           }
  //           temp = temp + temp1;
  //         }
  //         data = JSON.parse('{' + temp + '}');
  //         // console.log(JSON.stringify(data));
  //         resolve(data);
  //       }, err => {
  //         resolve(body);
  //         //  console.log('something went wrong');
  //       });
  //   });
  // }

  getMenu() {
    return this.sg['menu'];
  }

  
}
export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
