import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EmployeeCalendarPage } from './employee-calendar.page';
import { NgCalendarModule } from 'ionic2-calendar';
import { ObservationComponentsModule } from '../components/observation-components/observation-components.module';
import { UsercomponentsModule } from '../components/usercomponents/usercomponents.module';

const routes: Routes = [
  {
    path: '',
    component: EmployeeCalendarPage
  }
];

@NgModule({
  imports: [
    
    CommonModule,
    FormsModule,
    IonicModule,ObservationComponentsModule,UsercomponentsModule,NgCalendarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EmployeeCalendarPage]
})
export class EmployeeCalendarPageModule {}
