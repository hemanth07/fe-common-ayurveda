import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CalendarComponent } from 'ionic2-calendar';
import { SimpleGlobal } from 'ng2-simple-global';
import { FirebaseServiceService } from '../firebase-service.service';

@Component({
  selector: 'app-employee-calendar',
  templateUrl: './employee-calendar.page.html',
  styleUrls: ['./employee-calendar.page.scss'],
})
export class EmployeeCalendarPage implements OnInit {

 
  headerData: any;
  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };

  minDate = new Date().toISOString();
  allEvents: any = []
  // eventSource = [];
  viewTitle;
  eventSource;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  constructor(public service: FirebaseServiceService,
    private alertCtrl: AlertController,
    private sg: SimpleGlobal,
    private http: HttpClient, @Inject(LOCALE_ID) private locale: string) {
  }
  ngOnInit() {


    this.service.getDataFromFirebase(this.sg['userdata'].vendor_id+'/taskManager/task_table').then(data => {

      this.allEvents = data;
            // this.allEvents = data.filter(data => data.role == 'Employee')
            // this.allEvents = this.allEvents.filter(role => role.category && role.category.name == data.category);
            this.allEvents = this.allEvents.filter(role => role.assignee.role == 'Manager' && role.assignee.name == this.sg['userdata'].name);

      // alert(JSON.stringify(this.allEvents))
      console.log(this.allEvents)
      this.eventSource = this.createRandomEvents();
    })
    this.createHeader();

    this.resetEvent();
  }
  createHeader() {
    let title;
    if (this.sg['userdata'].primary_language === 'en') {
      title = 'Company Calendar';
    } else if (this.sg['userdata'].primary_language === 'te') {
      title = 'నా జీవన వివరణ';
    } else if (this.sg['userdata'].primary_language === 'ta') {
      title = 'என் சுயவிவரம்';
    } else if (this.sg['userdata'].primary_language === 'kn') {
      title = 'ಸ್ವ ಭೂಮಿಕೆ';
    } else if (this.sg['userdata'].primary_language === 'hi') {
      title = 'मेरी प्रोफाइल';
    }
    this.headerData = {
      color: 'green',
      title: this.sg['userdata'].name + " Calendar",
      button1: 'home',
      button1_action: '/farm-dashboard',
      button2: 'notifications',
      button2_action: '/notifications',
      notification_count: this.sg['notificationscount'],
    };
  }
  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }
  // Create the right event format and reload source
  addEvent() {
    debugger
    let eventCopy = {
      title: this.event.title,
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay,
      desc: this.event.desc
    }
    console.log(eventCopy)
    if (eventCopy.allDay) {
      let start = eventCopy.startTime;
      let end = eventCopy.endTime;
      eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
      eventCopy.endTime = new Date(Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), end.getUTCDate() + 1));
    }
    console.log(eventCopy)
    debugger
    this.http.post('https://ayurveda-one-dev-default-rtdb.firebaseio.com/posts.json', eventCopy).subscribe(responseData => {
      console.log(responseData);
      this.allEvents.push(eventCopy);
      this.loadEvents();
      this.resetEvent();
    });

  }
  loadEvents() {
    this.eventSource = this.createRandomEvents();
  }
  createRandomEvents() {

    var events = [];
    if (this.allEvents && this.allEvents.length > 0) {
      debugger

      var startTime;
      var endTime;
      var title;
      var allday;
      for (var i = 0; i < this.allEvents.length; i++) {
        var date = new Date(this.allEvents[i].start_date);
        var indiaTime = new Date(date).toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
        startTime = new Date(indiaTime);
        var date1 = new Date(this.allEvents[i].end_date);
        var indiaTime1 = new Date(date1).toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
        endTime = new Date(indiaTime1);
        // title = this.allEvents[i].assignee.name + ":" + this.allEvents[i].description;
        title = this.allEvents[i].created.name +  this.allEvents[i].created.role+ ":" + this.allEvents[i].description;

        allday = this.allEvents[i].bulk;

        let obj = {
          'title': title,
          'startTime': startTime,
          'endTime': endTime,
          'allDay': allday,

        }
        events.push(obj);
      }
    }
    return events;
  }
  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }

  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }

  // Change between month/week/day
  changeMode(mode) {
    this.calendar.mode = mode;
  }

  // Focus today
  today() {
    this.calendar.currentDate = new Date();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  // Calendar event was clicked
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    let start = formatDate(event.startTime, 'medium', this.locale);
    let end = formatDate(event.endTime, 'medium', this.locale);

    const alert = await this.alertCtrl.create({
      header: event.title,
      subHeader: event.desc,
      message: 'From: ' + start + '<br><br>To: ' + end,
      buttons: ['OK']
    });
    alert.present();
  }

  // Time slot was clicked
  onTimeSelected(ev) {
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());
  }
}

export const snapshotToArray = snapshot => {
  const returnArr = [];

  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};