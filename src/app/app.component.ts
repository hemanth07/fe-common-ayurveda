import { Component , ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import * as firebase from 'firebase';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';
import { FarmeasyTranslate } from './translate.service';
import { ToastController } from '@ionic/angular';
import { FarmeasymodulesComponent } from './components/farmeasymodules/farmeasymodules.component';
import { KanadaService } from './languages/kanada.service';
import { TamilService } from './languages/tamil.service';
import { TeluguService } from './languages/telugu.service';
import { EnglishService } from './languages/english.service';
import { HindiService } from './languages/hindi.service';
import { OnInit, ViewContainerRef, ComponentFactoryResolver  } from '@angular/core';

// import { MyTaskComponent } from './components/task-manger-components/my-task/my-task.component';

// Dev
// const config = {
//   apiKey: "AIzaSyCthk8-JT9iPiXm92RVs0l9hABipP4ZyN8",
//   authDomain: "ayurveda-one-dev.firebaseapp.com",
//   databaseURL: "https://ayurveda-one-dev-default-rtdb.firebaseio.com",
//   projectId: "ayurveda-one-dev",
//   storageBucket: "ayurveda-one-dev.appspot.com",
//   messagingSenderId: "378603982453",
//   appId: "1:378603982453:web:2dd85a25cd2a29655ff23c",
//   measurementId: "G-GZ1QS2VXWK"
// };
const config = {
  apiKey: "AIzaSyCckfzwx9m7G669y_iT1LbYQihQrVslbTU",
  authDomain: "ayur-central-task-manager.firebaseapp.com",
  databaseURL: "https://ayur-central-task-manager-default-rtdb.firebaseio.com",
  projectId: "ayur-central-task-manager",
  storageBucket: "ayur-central-task-manager.appspot.com",
  messagingSenderId: "637863155745",
  appId: "1:637863155745:web:2e025f1071b204dedf2b60",
  measurementId: "${config.measurementId}"
};
// Dev
// const config = {
//   apiKey: "AIzaSyCthk8-JT9iPiXm92RVs0l9hABipP4ZyN8",
//   authDomain: "ayurveda-one-dev.firebaseapp.com",
//   databaseURL: "https://ayurveda-one-dev-default-rtdb.firebaseio.com",
//   projectId: "ayurveda-one-dev",
//   storageBucket: "ayurveda-one-dev.appspot.com",
//   messagingSenderId: "378603982453",
//   appId: "1:378603982453:web:2dd85a25cd2a29655ff23c",
//   measurementId: "G-GZ1QS2VXWK"
// };
// const db= "D";

const db= "D";



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  user_data: any;
  notificationscounts: any;
  menuItem: any = [];
  user_lang: string;
  languages_support: any = [];
  count: number;
  iOS:boolean = false;
  android:boolean = false;
  @ViewChild('language') language;
  @ViewChild('mymenu') mymenu;
  constructor(
    private platform: Platform,public navCtrl: NavController,
    private splashScreen: SplashScreen, public sg: SimpleGlobal,
    private statusBar: StatusBar, private storage: Storage,
    private router: Router, public translate: FarmeasyTranslate,
    public alertController: AlertController, private http: Http,
    public toastController: ToastController,
    private en: EnglishService,
    private ta:TamilService,
    private te:TeluguService,
    private hi:HindiService,
    private kn:KanadaService,
    private resolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private firebase: Firebase
   ){
    
    this.menuItem = {
      profile:'My Profile',
      notification:'Notifications',
      settings:'Settings',
      about:'About WNF',
      sync:'Sync Data',
      lagout:'Logout',
      switch:'Switch Language',
      dashboard:'Dashboard',
      support:'Support/Feedback'
    }
    this.initializeApp();
    this.LoadLanguages();
  }
  initializeApp() {
    // firebase.database().ref("wellnest/taskManager").remove()
    // firebase.database().ref("wellnest/observations").remove()
    // firebase.database().ref("wellnest/reports").remove()
    // firebase.database().ref("wellnest/expenses").remove()
    // firebase.database().ref("wellnest/messaging").remove()
    // firebase.database().ref("wellnest/chatroom").remove()
    this.platform.ready().then(() => {
      if(this.platform.is('ios')){
        this.sg['platform'] = 'ios';
        this.iOS = true;
      } else {
        this.sg['platform'] = 'android';
        this.android = true;
      }
      // this.router.navigateByUrl('error');
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.storage.get('farmeasy_userdata').then((val) => {
        console.log('Your age is', val);
        if (!val) {
          // this.router.navigateByUrl('/farmeasysplash');
          this.router.navigateByUrl('/farmeasysignin');
        } else {
          this.storage.get('hamburger').then((menu) =>{
            this.sg["hamburger"] = menu;
           // console.log(this.sg["hamburger"]);
          });
          this.sg["userdata"] = val;
          this.initilisedata();
        }
      });
      firebase.initializeApp(config);
      if (this.platform.is('ios')) {
        this.firebase.grantPermission();
      }
    });
 
  }

// status(){
//   let that = this;
//     var connectedRef = firebase.database().ref(".info/connected");
//     connectedRef.on("value", function(snap) {
//       if (snap.val() === true) {
//        that.presentToast('Online');
//       } else {
//         that.presentToast('Offline');
//       }
//     });
// }

async presentToast(data) {
  const toast = await this.toastController.create({
    message: data,
    duration: 2000,
    cssClass: 'normalToast'
  });
  toast.present();
}
async syncData() {
  const toast = await this.toastController.create({
    message: 'Coming Soon...',
    duration: 2000,
    position : "middle",
    cssClass:"my-custom-class"
  });
  toast.present();
}
plants_history(){
  this.router.navigateByUrl('/plants-hystory');
}
about_Farmeasy(){
  
  this.navCtrl.navigateForward('/about')
}
  initilisedata() {
    
     if (this.sg["userdata"] && this.sg["userdata"].key) {
  
      firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + this.sg["userdata"].key + "/notifications").on('value', resp => {
        //let that = this;
        const data = snapshotToArray(resp);

        //console.log(data);
       // console.log(data.length);
        if (data && data.length) {
          let temp_len = data.filter(item => item.read !== true);
          this.sg['notificationscount'] = temp_len.length;
        //  console.log(this.sg['notificationscounts']);
          //this.router.navigateByUrl('/farm-dashboard');
        }
      });
      this.user_lang = this.sg['userdata'].primary_language;
      this.translation();
      // this.router.navigateByUrl('/notifications');

      // this.router.navigateByUrl('/task-planner');
      // this.router.navigateByUrl('/start-time');
      this.router.navigateByUrl('/farm-dashboard');
      // this.router.navigateByUrl('/employee-calendar');


     }
    firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + this.sg["userdata"].key+"/role").on('value',resp =>{
      let data = resp.val();
      this.storage.get('farmeasy_userdata').then((val) => {
        val['role'] = data;
       // console.log(val);
      this.sg["userdata"]['role'] = data;
      this.storage.set('farmeasy_userdata', val);
      this.translation();
      });
    });
      firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + this.sg["userdata"].key+"/primary_language").on('value',resp =>{
      let data = resp.val();
      this.storage.get('farmeasy_userdata').then((val) => {
        val['primary_language'] = data;
       // console.log(val);
      this.sg["userdata"]['primary_language'] = data;
      this.storage.set('farmeasy_userdata', val);
      this.translation();
        });
      });
  }
  translation(){
    if(this.sg['userdata'].primary_language === 'en'){
      this.sg["hamburger"] = this.en.getHamburger();
      this.sg["dashboard"] = this.en.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'te'){
      this.sg["hamburger"] = this.te.getHamburger();
      this.sg["dashboard"] = this.te.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'ta'){
      this.sg["hamburger"] = this.ta.getHamburger();
      this.sg["dashboard"] = this.ta.getDashboard();
    } else if(this.sg['userdata'].primary_language === 'kn'){
      this.sg["dashboard"] = this.kn.getDashboard();
      this.sg["hamburger"] = this.kn.getHamburger();
    }else if(this.sg['userdata'].primary_language === 'hi'){
      this.sg["dashboard"] = this.hi.getDashboard();
      this.sg["hamburger"] = this.hi.getHamburger();
    } else {
      this.sg["dashboard"] = this.en.getDashboard();
      this.sg["hamburger"] = this.en.getHamburger();
    }
    this.storage.set('hamburger', this.sg["hamburger"]);
    this.storage.set('dashboard', this.sg["dashboard"]);
  }
  changeLanuage(){
    this.language.open();
   // console.log('change lang');
    }
  changePassword(){
    this.navCtrl.navigateForward('/change-password');
    // console.log("Change Password");
  }
  bindLang(){
    // console.log(this.user_lang);
     this.sg['userdata'].primary_language = this.user_lang;
     if(this.sg['userdata'].key){
      firebase.database().ref(this.sg['userdata'].vendor_id+'/users/' + this.sg['userdata'].key).update({primary_language: this.user_lang});
      this.translation();
      this.storage.get('farmeasy_userdata').then((val) => {
        let temp = val;
        temp.primary_language = this.user_lang;
        this.storage.set('farmeasy_userdata', temp);
       // console.log(val);
      });
     }
     this.mymenu.close();
   }
  LoadLanguages(){
    this.languages_support =[{
      'name': 'English',
      'code': 'en'
    },
    {
      'name': 'Telugu-తెలుగు',
      'code': 'te'
    },
    {
      'name': 'Tamil-தமிழ்',
      'code': 'ta'
    },
    {
      'name': 'Kannada-ಕನ್ನಡ',
      'code': 'kn'
    },
    {
      'name': 'Hindi-हिंदी',
      'code': 'hi'
    },
   ];
  }
  gotonotofications() {
    let data = { "page_status": JSON.stringify("dashboard") }
    this.navCtrl.navigateForward(['/notifications', data]); 
  }
  openFeedback(){
    this.navCtrl.navigateForward('/feedback');
  }
  Logout_app() {
    firebase.database().ref(this.sg['userdata'].vendor_id+"/users/" + this.sg["userdata"].key).update({device_id:null});
    this.storage.remove("farmeasy_userdata");
    this.sg["userdata"] = '';
    this.storage.get('farmeasy_userdata').then((val) => {
      console.log('Your age is', val);
    });
    this.sg['notificationscount'] = '';
    // this.router.navigateByUrl('/farmeasysplash');
    this.router.navigateByUrl('/farmeasysignin');
  }
  gotoprofile() {
        // this.router.navigateByUrl('/start-time');

    this.router.navigateByUrl('/userprofile');
  }
  calender(){
        this.router.navigateByUrl('/start-time');

    // this.router.navigateByUrl('/company-calendar');

    // this.router.navigateByUrl('/error');

  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};